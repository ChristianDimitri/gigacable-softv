Imports System.Data
Imports System.Data.OleDb



Public Class DataSetConfiguration

    Private Const CONNECTION_STRING As String = "Provider=sqloledb;Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=NewSofTv;Persist Security Info=True;User ID=sa;Password=06011975"
    Private Const QUERY_STRING As String = "SELECT * FROM Calles"
    Private Const DATATABLE_NAME As String = "Calles"
    Private Const DIRECTORY_FILE_PATH As String = "C:\sistemas\sofTV\sofTV\"

    Public Shared ReadOnly Property CustomerDataSet() As DataSet
        Get
            Dim myDataSet As NewSofTvDataSet = New NewSofTvDataSet()
            Dim myOleDbConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
            Dim myOleDbDataAdapter As OleDbDataAdapter = New OleDbDataAdapter(QUERY_STRING, myOleDbConnection)
            myOleDbDataAdapter.Fill(myDataSet, DATATABLE_NAME)
            Return myDataSet
        End Get
    End Property

End Class
