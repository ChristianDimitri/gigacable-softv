Imports System.Data.SqlClient
Imports System.Text
Public Class BrwPreguntas

    Private Sub MuestraPreguntas(ByVal Pregunta As String, ByVal IDTipoRespuesta As Integer, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraPreguntas ")
        strSQL.Append("'" & Pregunta & "', ")
        strSQL.Append(CStr(IDTipoRespuesta) & ", ")
        strSQL.Append(CStr(Op))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            DataGridViewPregunta.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub ConTiposRespuestas(ByVal IDTipoRespuesta As Integer, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC ConTiposRespuestas ")
        strSQL.Append(CStr(IDTipoRespuesta) & ", ")
        strSQL.Append(CStr(Op))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            ComboBoxTipoRespuesta.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub


    Private Sub TextBoxPregunta_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxPregunta.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If TextBoxPregunta.Text.Length = 0 Then
                MuestraPreguntas(String.Empty, 0, 0)
                Exit Sub
            End If
            MuestraPreguntas(TextBoxPregunta.Text, 0, 1)
        End If
    End Sub

    Private Sub ButtonBusPregunta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonBusPregunta.Click
        If TextBoxPregunta.Text.Length = 0 Then
            MuestraPreguntas(String.Empty, 0, 0)
            Exit Sub
        End If
        MuestraPreguntas(TextBoxPregunta.Text, 0, 1)
    End Sub

    Private Sub ButtonBusTipoRespuesta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBusTipoRespuesta.Click
        MuestraPreguntas(String.Empty, ComboBoxTipoRespuesta.SelectedValue, 2)
    End Sub

    Private Sub ButtonNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonNuevo.Click
        eOpcion = "N"
        eIDPregunta = 0
        FrmPreguntas.Show()
    End Sub

    Private Sub ButtonConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonConsultar.Click
        If DataGridViewPregunta.RowCount = 0 Then
            MsgBox("Selecciona una Pregunta.", MsgBoxStyle.Information)
            Exit Sub
        End If
        eOpcion = "C"
        eIDPregunta = DataGridViewPregunta.SelectedCells.Item(0).Value
        FrmPreguntas.Show()
    End Sub

    Private Sub ButtonModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonModificar.Click
        If DataGridViewPregunta.RowCount = 0 Then
            MsgBox("Selecciona una Pregunta.", MsgBoxStyle.Information)
            Exit Sub
        End If
        eOpcion = "M"
        eIDPregunta = DataGridViewPregunta.SelectedCells.Item(0).Value
        FrmPreguntas.Show()
    End Sub

    Private Sub ButtonSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSalir.Click
        Me.Close()
    End Sub

    Private Sub BrwPreguntas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If eBnd = True Then
            eBnd = False
            MuestraPreguntas(String.Empty, 0, 0)
        End If
    End Sub

    Private Sub BrwPreguntas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        MuestraPreguntas(String.Empty, 0, 0)
        ConTiposRespuestas(0, 0)
    End Sub


End Class
