<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGenerales_Digital
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim IpServerLabel As System.Windows.Forms.Label
        Dim PuertoServerLabel As System.Windows.Forms.Label
        Dim Nombre_ConexionLabel As System.Windows.Forms.Label
        Dim Num_dia_habilitarLabel As System.Windows.Forms.Label
        Dim Paq_defaultLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmGenerales_Digital))
        Me.CONGenerales_DigitalBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.CONGenerales_DigitalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEDGAR = New sofTV.DataSetEDGAR
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.CONGenerales_DigitalBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.Clv_IdTextBox = New System.Windows.Forms.TextBox
        Me.IpServerTextBox = New System.Windows.Forms.TextBox
        Me.PuertoServerTextBox = New System.Windows.Forms.TextBox
        Me.Nombre_ConexionTextBox = New System.Windows.Forms.TextBox
        Me.ToolStripContainer1 = New System.Windows.Forms.ToolStripContainer
        Me.Paq_defaultTextBox = New System.Windows.Forms.TextBox
        Me.Num_dia_habilitarTextBox = New System.Windows.Forms.TextBox
        Me.Button2 = New System.Windows.Forms.Button
        Me.CONGenerales_DigitalTableAdapter = New sofTV.DataSetEDGARTableAdapters.CONGenerales_DigitalTableAdapter
        IpServerLabel = New System.Windows.Forms.Label
        PuertoServerLabel = New System.Windows.Forms.Label
        Nombre_ConexionLabel = New System.Windows.Forms.Label
        Num_dia_habilitarLabel = New System.Windows.Forms.Label
        Paq_defaultLabel = New System.Windows.Forms.Label
        CType(Me.CONGenerales_DigitalBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONGenerales_DigitalBindingNavigator.SuspendLayout()
        CType(Me.CONGenerales_DigitalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStripContainer1.ContentPanel.SuspendLayout()
        Me.ToolStripContainer1.TopToolStripPanel.SuspendLayout()
        Me.ToolStripContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'IpServerLabel
        '
        IpServerLabel.AutoSize = True
        IpServerLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        IpServerLabel.ForeColor = System.Drawing.Color.LightSlateGray
        IpServerLabel.Location = New System.Drawing.Point(219, 38)
        IpServerLabel.Name = "IpServerLabel"
        IpServerLabel.Size = New System.Drawing.Size(68, 15)
        IpServerLabel.TabIndex = 4
        IpServerLabel.Text = "Ip Server:"
        IpServerLabel.UseWaitCursor = True
        '
        'PuertoServerLabel
        '
        PuertoServerLabel.AutoSize = True
        PuertoServerLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PuertoServerLabel.ForeColor = System.Drawing.Color.LightSlateGray
        PuertoServerLabel.Location = New System.Drawing.Point(189, 65)
        PuertoServerLabel.Name = "PuertoServerLabel"
        PuertoServerLabel.Size = New System.Drawing.Size(98, 15)
        PuertoServerLabel.TabIndex = 6
        PuertoServerLabel.Text = "Puerto Server:"
        PuertoServerLabel.UseWaitCursor = True
        '
        'Nombre_ConexionLabel
        '
        Nombre_ConexionLabel.AutoSize = True
        Nombre_ConexionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Nombre_ConexionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Nombre_ConexionLabel.Location = New System.Drawing.Point(161, 92)
        Nombre_ConexionLabel.Name = "Nombre_ConexionLabel"
        Nombre_ConexionLabel.Size = New System.Drawing.Size(126, 15)
        Nombre_ConexionLabel.TabIndex = 8
        Nombre_ConexionLabel.Text = "Nombre Conexion:"
        Nombre_ConexionLabel.UseWaitCursor = True
        '
        'Num_dia_habilitarLabel
        '
        Num_dia_habilitarLabel.AutoSize = True
        Num_dia_habilitarLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Num_dia_habilitarLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Num_dia_habilitarLabel.Location = New System.Drawing.Point(18, 119)
        Num_dia_habilitarLabel.Name = "Num_dia_habilitarLabel"
        Num_dia_habilitarLabel.Size = New System.Drawing.Size(269, 15)
        Num_dia_habilitarLabel.TabIndex = 300
        Num_dia_habilitarLabel.Text = "Numero de Dìas de Prueba de Paquetes:"
        Num_dia_habilitarLabel.UseWaitCursor = True
        '
        'Paq_defaultLabel
        '
        Paq_defaultLabel.AutoSize = True
        Paq_defaultLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Paq_defaultLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Paq_defaultLabel.Location = New System.Drawing.Point(51, 146)
        Paq_defaultLabel.Name = "Paq_defaultLabel"
        Paq_defaultLabel.Size = New System.Drawing.Size(236, 15)
        Paq_defaultLabel.TabIndex = 301
        Paq_defaultLabel.Text = "Paquete(s) Asignado(s) por Default:"
        Paq_defaultLabel.UseWaitCursor = True
        '
        'CONGenerales_DigitalBindingNavigator
        '
        Me.CONGenerales_DigitalBindingNavigator.AddNewItem = Nothing
        Me.CONGenerales_DigitalBindingNavigator.BindingSource = Me.CONGenerales_DigitalBindingSource
        Me.CONGenerales_DigitalBindingNavigator.CountItem = Nothing
        Me.CONGenerales_DigitalBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONGenerales_DigitalBindingNavigator.Dock = System.Windows.Forms.DockStyle.None
        Me.CONGenerales_DigitalBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.CONGenerales_DigitalBindingNavigatorSaveItem})
        Me.CONGenerales_DigitalBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONGenerales_DigitalBindingNavigator.MoveFirstItem = Nothing
        Me.CONGenerales_DigitalBindingNavigator.MoveLastItem = Nothing
        Me.CONGenerales_DigitalBindingNavigator.MoveNextItem = Nothing
        Me.CONGenerales_DigitalBindingNavigator.MovePreviousItem = Nothing
        Me.CONGenerales_DigitalBindingNavigator.Name = "CONGenerales_DigitalBindingNavigator"
        Me.CONGenerales_DigitalBindingNavigator.PositionItem = Nothing
        Me.CONGenerales_DigitalBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONGenerales_DigitalBindingNavigator.Size = New System.Drawing.Size(208, 25)
        Me.CONGenerales_DigitalBindingNavigator.TabIndex = 5
        Me.CONGenerales_DigitalBindingNavigator.TabStop = True
        Me.CONGenerales_DigitalBindingNavigator.Text = "BindingNavigator1"
        '
        'CONGenerales_DigitalBindingSource
        '
        Me.CONGenerales_DigitalBindingSource.DataMember = "CONGenerales_Digital"
        Me.CONGenerales_DigitalBindingSource.DataSource = Me.DataSetEDGAR
        '
        'DataSetEDGAR
        '
        Me.DataSetEDGAR.DataSetName = "DataSetEDGAR"
        Me.DataSetEDGAR.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(77, 22)
        Me.BindingNavigatorDeleteItem.Text = "&Eliminar"
        '
        'CONGenerales_DigitalBindingNavigatorSaveItem
        '
        Me.CONGenerales_DigitalBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONGenerales_DigitalBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONGenerales_DigitalBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONGenerales_DigitalBindingNavigatorSaveItem.Name = "CONGenerales_DigitalBindingNavigatorSaveItem"
        Me.CONGenerales_DigitalBindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 22)
        Me.CONGenerales_DigitalBindingNavigatorSaveItem.Text = "&Guardar datos"
        '
        'Clv_IdTextBox
        '
        Me.Clv_IdTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_IdTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_IdTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGenerales_DigitalBindingSource, "Clv_Id", True))
        Me.Clv_IdTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_IdTextBox.Location = New System.Drawing.Point(137, 18)
        Me.Clv_IdTextBox.Name = "Clv_IdTextBox"
        Me.Clv_IdTextBox.Size = New System.Drawing.Size(163, 13)
        Me.Clv_IdTextBox.TabIndex = 300
        Me.Clv_IdTextBox.TabStop = False
        Me.Clv_IdTextBox.Text = "1"
        Me.Clv_IdTextBox.UseWaitCursor = True
        '
        'IpServerTextBox
        '
        Me.IpServerTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.IpServerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGenerales_DigitalBindingSource, "IpServer", True))
        Me.IpServerTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IpServerTextBox.Location = New System.Drawing.Point(293, 36)
        Me.IpServerTextBox.MaxLength = 15
        Me.IpServerTextBox.Name = "IpServerTextBox"
        Me.IpServerTextBox.Size = New System.Drawing.Size(152, 21)
        Me.IpServerTextBox.TabIndex = 0
        Me.IpServerTextBox.UseWaitCursor = True
        '
        'PuertoServerTextBox
        '
        Me.PuertoServerTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PuertoServerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGenerales_DigitalBindingSource, "PuertoServer", True))
        Me.PuertoServerTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PuertoServerTextBox.Location = New System.Drawing.Point(293, 63)
        Me.PuertoServerTextBox.MaxLength = 10
        Me.PuertoServerTextBox.Name = "PuertoServerTextBox"
        Me.PuertoServerTextBox.Size = New System.Drawing.Size(100, 21)
        Me.PuertoServerTextBox.TabIndex = 1
        Me.PuertoServerTextBox.UseWaitCursor = True
        '
        'Nombre_ConexionTextBox
        '
        Me.Nombre_ConexionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Nombre_ConexionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGenerales_DigitalBindingSource, "Nombre_Conexion", True))
        Me.Nombre_ConexionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Nombre_ConexionTextBox.Location = New System.Drawing.Point(293, 90)
        Me.Nombre_ConexionTextBox.MaxLength = 25
        Me.Nombre_ConexionTextBox.Name = "Nombre_ConexionTextBox"
        Me.Nombre_ConexionTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Nombre_ConexionTextBox.TabIndex = 2
        Me.Nombre_ConexionTextBox.UseWaitCursor = True
        '
        'ToolStripContainer1
        '
        '
        'ToolStripContainer1.ContentPanel
        '
        Me.ToolStripContainer1.ContentPanel.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Paq_defaultLabel)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.Paq_defaultTextBox)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Num_dia_habilitarLabel)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.Num_dia_habilitarTextBox)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.Clv_IdTextBox)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.Nombre_ConexionTextBox)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Nombre_ConexionLabel)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(IpServerLabel)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.PuertoServerTextBox)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.IpServerTextBox)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(PuertoServerLabel)
        Me.ToolStripContainer1.ContentPanel.Size = New System.Drawing.Size(475, 180)
        Me.ToolStripContainer1.Location = New System.Drawing.Point(12, 12)
        Me.ToolStripContainer1.Name = "ToolStripContainer1"
        Me.ToolStripContainer1.Size = New System.Drawing.Size(475, 205)
        Me.ToolStripContainer1.TabIndex = 10
        Me.ToolStripContainer1.TabStop = False
        Me.ToolStripContainer1.Text = "ToolStripContainer1"
        '
        'ToolStripContainer1.TopToolStripPanel
        '
        Me.ToolStripContainer1.TopToolStripPanel.Controls.Add(Me.CONGenerales_DigitalBindingNavigator)
        Me.ToolStripContainer1.TopToolStripPanel.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        '
        'Paq_defaultTextBox
        '
        Me.Paq_defaultTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Paq_defaultTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGenerales_DigitalBindingSource, "paq_default", True))
        Me.Paq_defaultTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Paq_defaultTextBox.Location = New System.Drawing.Point(293, 144)
        Me.Paq_defaultTextBox.Name = "Paq_defaultTextBox"
        Me.Paq_defaultTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Paq_defaultTextBox.TabIndex = 4
        Me.Paq_defaultTextBox.UseWaitCursor = True
        '
        'Num_dia_habilitarTextBox
        '
        Me.Num_dia_habilitarTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Num_dia_habilitarTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGenerales_DigitalBindingSource, "num_dia_habilitar", True))
        Me.Num_dia_habilitarTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Num_dia_habilitarTextBox.Location = New System.Drawing.Point(293, 117)
        Me.Num_dia_habilitarTextBox.Name = "Num_dia_habilitarTextBox"
        Me.Num_dia_habilitarTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Num_dia_habilitarTextBox.TabIndex = 3
        Me.Num_dia_habilitarTextBox.UseWaitCursor = True
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(345, 223)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(142, 29)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = "&CERRAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'CONGenerales_DigitalTableAdapter
        '
        Me.CONGenerales_DigitalTableAdapter.ClearBeforeFill = True
        '
        'FrmGenerales_Digital
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Gainsboro
        Me.ClientSize = New System.Drawing.Size(499, 269)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.ToolStripContainer1)
        Me.Name = "FrmGenerales_Digital"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Generales de Digital"
        CType(Me.CONGenerales_DigitalBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONGenerales_DigitalBindingNavigator.ResumeLayout(False)
        Me.CONGenerales_DigitalBindingNavigator.PerformLayout()
        CType(Me.CONGenerales_DigitalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStripContainer1.ContentPanel.ResumeLayout(False)
        Me.ToolStripContainer1.ContentPanel.PerformLayout()
        Me.ToolStripContainer1.TopToolStripPanel.ResumeLayout(False)
        Me.ToolStripContainer1.TopToolStripPanel.PerformLayout()
        Me.ToolStripContainer1.ResumeLayout(False)
        Me.ToolStripContainer1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataSetEDGAR As sofTV.DataSetEDGAR
    Friend WithEvents CONGenerales_DigitalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONGenerales_DigitalTableAdapter As sofTV.DataSetEDGARTableAdapters.CONGenerales_DigitalTableAdapter
    Friend WithEvents CONGenerales_DigitalBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONGenerales_DigitalBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Clv_IdTextBox As System.Windows.Forms.TextBox
    Friend WithEvents IpServerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PuertoServerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Nombre_ConexionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ToolStripContainer1 As System.Windows.Forms.ToolStripContainer
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Paq_defaultTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Num_dia_habilitarTextBox As System.Windows.Forms.TextBox
End Class
