﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwCatalogoMarcas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim nombreMarcaLbl As System.Windows.Forms.Label
        Dim idMarcaLbl As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.laUltimoClienteAsignado = New System.Windows.Forms.Label()
        Me.laUltimaFechaLiberacion = New System.Windows.Forms.Label()
        Me.laUltimaFechaAsignacion = New System.Windows.Forms.Label()
        Me.laStatus = New System.Windows.Forms.Label()
        Me.elIdMarcaLbl = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.laMarcaLbl = New System.Windows.Forms.Label()
        Me.marcaTxt = New System.Windows.Forms.TextBox()
        Me.idMarcaTxt = New System.Windows.Forms.TextBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.gridMarcas = New System.Windows.Forms.DataGridView()
        Me.IdMarca = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Marca = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bSalir = New System.Windows.Forms.Button()
        Me.bModificar = New System.Windows.Forms.Button()
        Me.bConsultar = New System.Windows.Forms.Button()
        Me.bNuevo = New System.Windows.Forms.Button()
        Me.btnBuscarPorNombre = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        nombreMarcaLbl = New System.Windows.Forms.Label()
        idMarcaLbl = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.gridMarcas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'nombreMarcaLbl
        '
        nombreMarcaLbl.AutoSize = True
        nombreMarcaLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        nombreMarcaLbl.ForeColor = System.Drawing.Color.White
        nombreMarcaLbl.Location = New System.Drawing.Point(15, 59)
        nombreMarcaLbl.Name = "nombreMarcaLbl"
        nombreMarcaLbl.Size = New System.Drawing.Size(51, 15)
        nombreMarcaLbl.TabIndex = 3
        nombreMarcaLbl.Text = "Marca:"
        '
        'idMarcaLbl
        '
        idMarcaLbl.AutoSize = True
        idMarcaLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        idMarcaLbl.ForeColor = System.Drawing.Color.White
        idMarcaLbl.Location = New System.Drawing.Point(15, 30)
        idMarcaLbl.Name = "idMarcaLbl"
        idMarcaLbl.Size = New System.Drawing.Size(27, 15)
        idMarcaLbl.TabIndex = 1
        idMarcaLbl.Text = "Id :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(Me.laUltimoClienteAsignado)
        Me.Panel1.Controls.Add(Me.laUltimaFechaLiberacion)
        Me.Panel1.Controls.Add(Me.laUltimaFechaAsignacion)
        Me.Panel1.Controls.Add(Me.laStatus)
        Me.Panel1.Controls.Add(Me.elIdMarcaLbl)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(nombreMarcaLbl)
        Me.Panel1.Controls.Add(Me.laMarcaLbl)
        Me.Panel1.Controls.Add(idMarcaLbl)
        Me.Panel1.Location = New System.Drawing.Point(19, 368)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(239, 127)
        Me.Panel1.TabIndex = 16
        '
        'laUltimoClienteAsignado
        '
        Me.laUltimoClienteAsignado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.laUltimoClienteAsignado.Location = New System.Drawing.Point(27, 265)
        Me.laUltimoClienteAsignado.Name = "laUltimoClienteAsignado"
        Me.laUltimoClienteAsignado.Size = New System.Drawing.Size(202, 23)
        Me.laUltimoClienteAsignado.TabIndex = 28
        '
        'laUltimaFechaLiberacion
        '
        Me.laUltimaFechaLiberacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.laUltimaFechaLiberacion.Location = New System.Drawing.Point(27, 217)
        Me.laUltimaFechaLiberacion.Name = "laUltimaFechaLiberacion"
        Me.laUltimaFechaLiberacion.Size = New System.Drawing.Size(202, 23)
        Me.laUltimaFechaLiberacion.TabIndex = 26
        '
        'laUltimaFechaAsignacion
        '
        Me.laUltimaFechaAsignacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.laUltimaFechaAsignacion.Location = New System.Drawing.Point(24, 169)
        Me.laUltimaFechaAsignacion.Name = "laUltimaFechaAsignacion"
        Me.laUltimaFechaAsignacion.Size = New System.Drawing.Size(215, 23)
        Me.laUltimaFechaAsignacion.TabIndex = 24
        '
        'laStatus
        '
        Me.laStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.laStatus.Location = New System.Drawing.Point(24, 121)
        Me.laStatus.Name = "laStatus"
        Me.laStatus.Size = New System.Drawing.Size(148, 23)
        Me.laStatus.TabIndex = 20
        '
        'elIdMarcaLbl
        '
        Me.elIdMarcaLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elIdMarcaLbl.Location = New System.Drawing.Point(74, 32)
        Me.elIdMarcaLbl.Name = "elIdMarcaLbl"
        Me.elIdMarcaLbl.Size = New System.Drawing.Size(100, 23)
        Me.elIdMarcaLbl.TabIndex = 20
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(9, 7)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(146, 18)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Datos de la Marca"
        '
        'laMarcaLbl
        '
        Me.laMarcaLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.laMarcaLbl.Location = New System.Drawing.Point(21, 79)
        Me.laMarcaLbl.Name = "laMarcaLbl"
        Me.laMarcaLbl.Size = New System.Drawing.Size(210, 20)
        Me.laMarcaLbl.TabIndex = 22
        '
        'marcaTxt
        '
        Me.marcaTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.marcaTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.marcaTxt.Location = New System.Drawing.Point(19, 104)
        Me.marcaTxt.Name = "marcaTxt"
        Me.marcaTxt.Size = New System.Drawing.Size(239, 21)
        Me.marcaTxt.TabIndex = 14
        '
        'idMarcaTxt
        '
        Me.idMarcaTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.idMarcaTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.idMarcaTxt.Location = New System.Drawing.Point(19, 57)
        Me.idMarcaTxt.Name = "idMarcaTxt"
        Me.idMarcaTxt.Size = New System.Drawing.Size(88, 21)
        Me.idMarcaTxt.TabIndex = 11
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(15, 11)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(155, 20)
        Me.CMBLabel1.TabIndex = 9
        Me.CMBLabel1.Text = "Buscar Marca por:"
        '
        'gridMarcas
        '
        Me.gridMarcas.AllowUserToAddRows = False
        Me.gridMarcas.AllowUserToDeleteRows = False
        Me.gridMarcas.AllowUserToOrderColumns = True
        Me.gridMarcas.AllowUserToResizeRows = False
        Me.gridMarcas.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gridMarcas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.gridMarcas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridMarcas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdMarca, Me.Marca})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gridMarcas.DefaultCellStyle = DataGridViewCellStyle2
        Me.gridMarcas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gridMarcas.Location = New System.Drawing.Point(0, 0)
        Me.gridMarcas.Name = "gridMarcas"
        Me.gridMarcas.ReadOnly = True
        Me.gridMarcas.RowHeadersVisible = False
        Me.gridMarcas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.gridMarcas.Size = New System.Drawing.Size(548, 533)
        Me.gridMarcas.TabIndex = 17
        Me.gridMarcas.TabStop = False
        '
        'IdMarca
        '
        Me.IdMarca.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.IdMarca.FillWeight = 50.76141!
        Me.IdMarca.HeaderText = "Id"
        Me.IdMarca.MinimumWidth = 20
        Me.IdMarca.Name = "IdMarca"
        Me.IdMarca.ReadOnly = True
        '
        'Marca
        '
        Me.Marca.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Marca.FillWeight = 149.2385!
        Me.Marca.HeaderText = "Marca"
        Me.Marca.MinimumWidth = 50
        Me.Marca.Name = "Marca"
        Me.Marca.ReadOnly = True
        '
        'bSalir
        '
        Me.bSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.bSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bSalir.ForeColor = System.Drawing.Color.Black
        Me.bSalir.Location = New System.Drawing.Point(850, 496)
        Me.bSalir.Name = "bSalir"
        Me.bSalir.Size = New System.Drawing.Size(136, 36)
        Me.bSalir.TabIndex = 23
        Me.bSalir.Text = "&SALIR"
        Me.bSalir.UseVisualStyleBackColor = False
        '
        'bModificar
        '
        Me.bModificar.BackColor = System.Drawing.Color.Orange
        Me.bModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bModificar.ForeColor = System.Drawing.Color.Black
        Me.bModificar.Location = New System.Drawing.Point(850, 96)
        Me.bModificar.Name = "bModificar"
        Me.bModificar.Size = New System.Drawing.Size(136, 36)
        Me.bModificar.TabIndex = 26
        Me.bModificar.Text = "&MODIFICAR"
        Me.bModificar.UseVisualStyleBackColor = False
        '
        'bConsultar
        '
        Me.bConsultar.BackColor = System.Drawing.Color.Orange
        Me.bConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bConsultar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bConsultar.ForeColor = System.Drawing.Color.Black
        Me.bConsultar.Location = New System.Drawing.Point(850, 54)
        Me.bConsultar.Name = "bConsultar"
        Me.bConsultar.Size = New System.Drawing.Size(136, 36)
        Me.bConsultar.TabIndex = 25
        Me.bConsultar.Text = "&CONSULTA"
        Me.bConsultar.UseVisualStyleBackColor = False
        '
        'bNuevo
        '
        Me.bNuevo.BackColor = System.Drawing.Color.Orange
        Me.bNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bNuevo.ForeColor = System.Drawing.Color.Black
        Me.bNuevo.Location = New System.Drawing.Point(850, 12)
        Me.bNuevo.Name = "bNuevo"
        Me.bNuevo.Size = New System.Drawing.Size(136, 36)
        Me.bNuevo.TabIndex = 24
        Me.bNuevo.Text = "&NUEVO"
        Me.bNuevo.UseVisualStyleBackColor = False
        '
        'btnBuscarPorNombre
        '
        Me.btnBuscarPorNombre.BackColor = System.Drawing.Color.DarkOrange
        Me.btnBuscarPorNombre.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscarPorNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscarPorNombre.ForeColor = System.Drawing.Color.Black
        Me.btnBuscarPorNombre.Location = New System.Drawing.Point(19, 132)
        Me.btnBuscarPorNombre.Name = "btnBuscarPorNombre"
        Me.btnBuscarPorNombre.Size = New System.Drawing.Size(88, 23)
        Me.btnBuscarPorNombre.TabIndex = 46
        Me.btnBuscarPorNombre.Text = "&Buscar"
        Me.btnBuscarPorNombre.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(113, 57)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(88, 23)
        Me.Button1.TabIndex = 45
        Me.Button1.Text = "&Buscar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(16, 86)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 15)
        Me.Label2.TabIndex = 48
        Me.Label2.Text = "Marca :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(16, 38)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 15)
        Me.Label3.TabIndex = 47
        Me.Label3.Text = "Id Marca :"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(4, 3)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.idMarcaTxt)
        Me.SplitContainer1.Panel1.Controls.Add(Me.marcaTxt)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnBuscarPorNombre)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.gridMarcas)
        Me.SplitContainer1.Size = New System.Drawing.Size(827, 533)
        Me.SplitContainer1.SplitterDistance = 275
        Me.SplitContainer1.TabIndex = 53
        Me.SplitContainer1.TabStop = False
        '
        'BrwCatalogoMarcas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(1007, 544)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.bModificar)
        Me.Controls.Add(Me.bConsultar)
        Me.Controls.Add(Me.bNuevo)
        Me.Controls.Add(Me.bSalir)
        Me.Name = "BrwCatalogoMarcas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catalogo Marcas"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.gridMarcas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents laUltimoClienteAsignado As System.Windows.Forms.Label
    Friend WithEvents laUltimaFechaLiberacion As System.Windows.Forms.Label
    Friend WithEvents laUltimaFechaAsignacion As System.Windows.Forms.Label
    Friend WithEvents laStatus As System.Windows.Forms.Label
    Friend WithEvents elIdMarcaLbl As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents laMarcaLbl As System.Windows.Forms.Label
    Friend WithEvents marcaTxt As System.Windows.Forms.TextBox
    Friend WithEvents idMarcaTxt As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents gridMarcas As System.Windows.Forms.DataGridView
    Friend WithEvents bSalir As System.Windows.Forms.Button
    Friend WithEvents bModificar As System.Windows.Forms.Button
    Friend WithEvents bConsultar As System.Windows.Forms.Button
    Friend WithEvents bNuevo As System.Windows.Forms.Button
    Friend WithEvents IdMarca As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Marca As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnBuscarPorNombre As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
End Class
