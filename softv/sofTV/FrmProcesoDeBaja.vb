Imports System.Data.SqlClient
Imports System.Text
Public Class FrmProcesoDeBaja
    Dim Contrato As Long = 0
    Dim ClvTipSer As Integer = 0
    Dim ClvMotCan As Integer = 0
    Dim AContratoNet As Long = 0
    Dim AClvUnicaNet As Long = 0
    Dim BContratoNet As Long = 0
    Dim BClvUnicaNet As Long = 0
    Dim Tipo As String = ""
    Dim Res As Integer = 0
    Dim RetiroDeAparato As Boolean = False
    Dim Pregunta As Boolean = False
    Dim Todos As Boolean = False

    Private Function ConsultaAparatosActivos(ByVal Clv_Session As Long, ByVal Contrato As Long, ByVal Clv_TipSer As Integer) As DataTable

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC ConsultaAparatosActivos " & CStr(Clv_Session) & ", " & CStr(Contrato) & ", " & CStr(Clv_TipSer))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTableActivos As New DataTable


        Try
            dataAdapter.Fill(dataTableActivos)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

        Return dataTableActivos

    End Function

    Private Function ConsultaServiciosActivos(ByVal Clv_Session As Long, ByVal Clv_TipSer As Integer, ByVal ContratoNet As Long) As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC ConsultaServiciosActivos " & CStr(Clv_Session) & ", " & CStr(Clv_TipSer) & ", " & CStr(ContratoNet))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTableActivos As New DataTable

        Try
            dataAdapter.Fill(dataTableActivos)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

        Return dataTableActivos

    End Function

    Private Function ConsultaPreBajaClientes(ByVal Clv_Session As Long, ByVal Contrato As Long, ByVal Clv_TipSer As Integer) As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC ConsultaPreBajaClientes " & CStr(Clv_Session) & ", " & CStr(Contrato) & ", " & CStr(Clv_TipSer))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTableBajas As New DataTable

        Try
            dataAdapter.Fill(dataTableBajas)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

        Return dataTableBajas
    End Function

    Private Function ConsultaPreBajaCont(ByVal Clv_Session As Long, ByVal Clv_TipSer As Integer, ByVal ContratoNet As Long) As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC ConsultaPreBajaCont " & CStr(Clv_Session) & ", " & CStr(Clv_TipSer) & ", " & CStr(ContratoNet))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTableBajas As New DataTable

        Try
            dataAdapter.Fill(dataTableBajas)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

        Return dataTableBajas

    End Function

    Private Sub InsertaPreBajaClientes(ByVal Clv_Session As Long, ByVal Contrato As Long, ByVal Clv_TipSer As Integer, ByVal ContratoNet As Long, ByVal RetiroDeAparato As Boolean)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("InsertaPreBajaClientes", conexion)
        comando.CommandType = CommandType.StoredProcedure
        Dim reader As SqlDataReader

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Contrato
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Clv_TipSer
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Contratonet", SqlDbType.BigInt)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = ContratoNet
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@RetiroDeAparato", SqlDbType.BigInt)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = RetiroDeAparato
        comando.Parameters.Add(parametro5)

        Try
            conexion.Open()
            reader = comando.ExecuteReader()
            While (reader.Read())

            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub InsertaPreBajaCont(ByVal Clv_Session As Long, ByVal Tipo As String, ByVal Contrato As Long, ByVal Clv_TipSer As Integer, ByVal ContratoNet As Long, ByVal Clv_UnicaNet As Long, ByVal Clv_MotCan As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("InsertaPreBajaCont", conexion)
        comando.CommandType = CommandType.StoredProcedure
        Dim reader As SqlDataReader

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Tipo", SqlDbType.VarChar, 1)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Tipo
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Contrato
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Clv_TipSer
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Contratonet", SqlDbType.BigInt)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = ContratoNet
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Clv_UnicaNet", SqlDbType.BigInt)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = Clv_UnicaNet
        comando.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@Clv_MotCan", SqlDbType.BigInt)
        parametro7.Direction = ParameterDirection.Input
        parametro7.Value = Clv_MotCan
        comando.Parameters.Add(parametro7)

        Try
            conexion.Open()
            reader = comando.ExecuteReader()
            While (reader.Read())

            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub EliminaPreBaja(ByVal Clv_Session As Long, ByVal Tipo As String, ByVal Contrato As Long, ByVal Clv_TipSer As Integer, ByVal ContratoNet As Long, ByVal Clv_UnicaNet As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("EliminaPreBaja", conexion)
        comando.CommandType = CommandType.StoredProcedure
        Dim reader As SqlDataReader

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Tipo", SqlDbType.VarChar, 1)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Tipo
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Contrato
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Clv_TipSer
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Contratonet", SqlDbType.BigInt)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = ContratoNet
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Clv_UnicaNet", SqlDbType.BigInt)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = Clv_UnicaNet
        comando.Parameters.Add(parametro6)

        Try
            conexion.Open()
            reader = comando.ExecuteReader()
            While (reader.Read())

            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try


    End Sub

    Private Function ChecaRetiroDeAparato(ByVal Clv_Session As Long, ByVal Contrato As Long, ByVal Clv_TipSer As Integer, ByVal ContratoNet As Long) As Boolean
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ChecaRetiroDeAparato", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Contrato
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Clv_TipSer
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Contratonet", SqlDbType.BigInt)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = ContratoNet
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Bnd", SqlDbType.Bit)
        parametro5.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Pregunta", SqlDbType.Bit)
        parametro6.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("Todos", SqlDbType.Bit)
        parametro7.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro7)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Pregunta = parametro6.Value.ToString()
            Todos = parametro7.Value.ToString()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

        Return parametro5.Value

    End Function

    Private Function ChecaServicioPrincipal(ByVal Clv_TipSer As Integer, ByVal Clv_UnicaNet As Long) As Boolean
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ChecaServicioPrincipal", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_TipSer
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_UnicaNet", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_UnicaNet
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Bnd", SqlDbType.Bit)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

        Return parametro3.Value.ToString()

    End Function

    Private Sub RealizaBaja(ByVal Clv_Session As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("RealizaBaja", conexion)
        comando.CommandType = CommandType.StoredProcedure
        Dim reader As SqlDataReader
        Dim mensaje As String = ""

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            reader = comando.ExecuteReader()
            While (reader.Read())
                mensaje = mensaje & vbCrLf & "Orden: " & reader(0) & " | " & reader(1)
                InsertaRelOrdenUsuario(CInt(reader(0).ToString()), GloClvUsuario, 0)
                bitsist(GloUsuario, Contrato, Loc1clv_sistema, "Proceso de Baja", Me.Text, "", "Gener� la orden: " + reader(0).ToString(), IdSistema)
            End While

            MsgBox(mensaje, MsgBoxStyle.Information)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub LimpiaPreBaja()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("LimpiaPreBaja", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = eClv_Session
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub MuestraTipSerCliente(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC MuestraTipSerCliente " & CStr(Contrato))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.ComboBoxTipSer.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub MuestraInfoCliente(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("MuestraInfoCliente", conexion)
        comando.CommandType = CommandType.StoredProcedure
        Dim reader As SqlDataReader

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            reader = comando.ExecuteReader
            While (reader.Read())
                Me.LabelNombre.Text = reader(0).ToString()
                Me.LabelCalle.Text = reader(1).ToString()
                Me.LabelNumero.Text = reader(2).ToString()
                Me.LabelColonia.Text = reader(3).ToString()
                Me.LabelCiudad.Text = reader(4).ToString()
                Me.LabelTelefono.Text = reader(5).ToString()
                Me.LabelCelular.Text = reader(6).ToString()
                Me.CheckBoxSoloInternet.Checked = reader(7).ToString
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub MuestraMotCan()
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC MuestraMotCan 0,0")
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.ComboBoxMotCan.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Dame_clv_session_Reportes()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Dame_clv_session_Reportes", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eClv_Session = CLng(parametro.Value.ToString)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Function DameContratoNet(ByVal Clv_UnicaNet) As Long
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameContratoNet", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_UnicaNet", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_UnicaNet
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

        Return CLng(parametro2.Value.ToString)

    End Function

    Private Function DameContratoNetDig(ByVal Clv_UnicaNet) As Long
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameContratoNetDig", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_UnicaNet", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_UnicaNet
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

        Return CLng(parametro2.Value.ToString)

    End Function

    Private Sub InsertaRelOrdenUsuario(ByVal Clv_Orden As Long, ByVal Clv_UsuarioGenero As Integer, ByVal Clv_UsuarioEjecuto As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("InsertaRelOrdenUsuario", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Orden", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Orden
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_UsuarioGenero", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_UsuarioGenero
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Clv_UsuarioEjecuto", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Clv_UsuarioEjecuto
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub LlenaTreeViewActivos()
        Dim dataTableAparatos As New DataTable
        Dim dataTableServicios As New DataTable
        Dim x As Integer = 0
        Dim y As Integer = 0

        Me.TreeViewActivos.Nodes.Clear()
        dataTableAparatos = ConsultaAparatosActivos(eClv_Session, Contrato, ClvTipSer)

        For Each RowAparato As DataRow In dataTableAparatos.Rows

            Me.TreeViewActivos.Nodes.Add(RowAparato("Aparato").ToString())
            Me.TreeViewActivos.Nodes(x).Tag = RowAparato("ContratoNet").ToString()
            dataTableServicios = ConsultaServiciosActivos(eClv_Session, Me.ComboBoxTipSer.SelectedValue, RowAparato("ContratoNet").ToString())

            For Each RowServicio As DataRow In dataTableServicios.Rows
                Me.TreeViewActivos.Nodes(x).Nodes.Add(RowServicio("Servicio").ToString()).ForeColor = Color.Blue
                Me.TreeViewActivos.Nodes(x).Nodes(y).Tag = RowServicio("Clv_UnicaNet").ToString()
                y += 1
            Next

            Me.TreeViewActivos.Nodes(x).ExpandAll()
            x += 1
            y = 0
        Next
    End Sub

    Private Sub LlenaTreeViewBajas()

        Dim dataTableAparatos As New DataTable
        Dim dataTableServicios As New DataTable
        Dim x As Integer = 0
        Dim y As Integer = 0

        TreeViewPreBajas.Nodes.Clear()
        dataTableAparatos = ConsultaPreBajaClientes(eClv_Session, Contrato, ClvTipSer)

        For Each RowAparato As DataRow In dataTableAparatos.Rows

            Me.TreeViewPreBajas.Nodes.Add(RowAparato("Aparato").ToString())
            Me.TreeViewPreBajas.Nodes(x).Tag = RowAparato("ContratoNet").ToString()
            dataTableServicios = ConsultaPreBajaCont(eClv_Session, Me.ComboBoxTipSer.SelectedValue, RowAparato("ContratoNet").ToString())

            For Each RowServicio As DataRow In dataTableServicios.Rows
                Me.TreeViewPreBajas.Nodes(x).Nodes.Add(RowServicio("Servicio").ToString()).ForeColor = Color.Red
                Me.TreeViewPreBajas.Nodes(x).Nodes(y).Tag = RowServicio("Clv_UnicaNet").ToString()
                y += 1
            Next

            TreeViewPreBajas.Nodes(x).ExpandAll()
            x += 1
            y = 0
        Next

    End Sub

    Private Sub Insertar()

        InsertaPreBajaCont(eClv_Session, Tipo, Contrato, ClvTipSer, AContratoNet, AClvUnicaNet, ClvMotCan)

        RetiroDeAparato = False

        If ChecaRetiroDeAparato(eClv_Session, Contrato, ClvTipSer, AContratoNet) = True Then
            RetiroDeAparato = True
            If Pregunta = True Then
                Res = MsgBox("�Se genera Orden de Retiro de Aparato?", MsgBoxStyle.YesNo)
                If Res = 6 Then RetiroDeAparato = True
                If Res = 7 Then RetiroDeAparato = False
            End If
            InsertaPreBajaClientes(eClv_Session, Contrato, ClvTipSer, AContratoNet, RetiroDeAparato)
        End If

        If Todos = True Then
            InsertaPreBajaClientes(eClv_Session, Contrato, ClvTipSer, AContratoNet, False)
        End If

        LlenaTreeViewActivos()
        LlenaTreeViewBajas()

    End Sub

    Private Sub Eliminar()
        EliminaPreBaja(eClv_Session, Tipo, Contrato, ClvTipSer, BContratoNet, BClvUnicaNet)
        LlenaTreeViewActivos()
        LlenaTreeViewBajas()
    End Sub

    Private Sub Guardar()
        If TreeViewPreBajas.Nodes.Count = 0 Then
            MsgBox("Sin servicios que procesar.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        RealizaBaja(eClv_Session)
        TextBoxContrato.Text = 0
    End Sub

    Private Sub Limpiar()
        Me.LabelNombre.Text = ""
        Me.LabelCalle.Text = ""
        Me.LabelNumero.Text = ""
        Me.LabelColonia.Text = ""
        Me.LabelCiudad.Text = ""
        Me.LabelTelefono.Text = ""
        Me.LabelCelular.Text = ""
        Me.CheckBoxSoloInternet.Checked = False
        Me.TreeViewActivos.Nodes.Clear()
        Me.TreeViewPreBajas.Nodes.Clear()
    End Sub

    Private Sub FrmProcesoDeBaja_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GLOCONTRATOSEL > 0 Then
            Me.TextBoxContrato.Text = GLOCONTRATOSEL
            GLOCONTRATOSEL = 0
            Contrato = Me.TextBoxContrato.Text
            Dame_clv_session_Reportes()
            MuestraTipSerCliente(Contrato)
            MuestraInfoCliente(Contrato)
            LlenaTreeViewActivos()
            LlenaTreeViewBajas()
        End If
    End Sub

    Private Sub FrmProcesoDeBaja_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        LimpiaPreBaja()
    End Sub

    Private Sub FrmProcesoDeBaja_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, "FrmProcesoDeBaja")
        GLOCONTRATOSEL = 0
        MuestraMotCan()
    End Sub

    Private Sub TextBoxContrato_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBoxContrato.KeyDown
        If (e.KeyCode <> Keys.Enter) Then
            Exit Sub
        End If
        If IsNumeric(Me.TextBoxContrato.Text) = False Then
            Exit Sub
        End If
        If CInt(Me.TextBoxContrato.Text) <= 0 Then
            Exit Sub
        End If
        Contrato = Me.TextBoxContrato.Text
        LimpiaPreBaja()
        Dame_clv_session_Reportes()
        MuestraTipSerCliente(Contrato)
        MuestraInfoCliente(Contrato)
        LlenaTreeViewActivos()
        LlenaTreeViewBajas()

    End Sub

    Private Sub TextBoxContrato_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxContrato.KeyPress
        e.KeyChar = Chr(ValidaKey(TextBoxContrato, Asc(LCase(e.KeyChar)), "N"))
    End Sub

    Private Sub TextBoxContrato_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxContrato.TextChanged
        Limpiar()
    End Sub

    Private Sub TreeViewActivos_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeViewActivos.AfterSelect
        If IsNumeric(e.Node.Tag) = False Then
            Exit Sub
        End If
        If e.Node.Level = 0 Then
            Tipo = "A"
            AContratoNet = e.Node.Tag
        Else
            Tipo = "S"
            If ClvTipSer = 2 Then AContratoNet = DameContratoNet(e.Node.Tag)
            If ClvTipSer = 3 Then AContratoNet = DameContratoNetDig(e.Node.Tag)
            AClvUnicaNet = e.Node.Tag

        End If
    End Sub

    Private Sub TreeViewPreBajas_AfterSelect(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeViewPreBajas.AfterSelect
        If IsNumeric(e.Node.Tag) = False Then
            Exit Sub
        End If
        If e.Node.Level = 0 Then
            Tipo = "A"
            BContratoNet = e.Node.Tag
        Else
            Tipo = "S"
            If ClvTipSer = 2 Then BContratoNet = DameContratoNet(e.Node.Tag)
            If ClvTipSer = 3 Then BContratoNet = DameContratoNetDig(e.Node.Tag)
            BClvUnicaNet = e.Node.Tag
        End If
    End Sub

    Private Sub ComboBoxTipSer_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxTipSer.SelectedIndexChanged
        If IsNumeric(ComboBoxTipSer.SelectedValue) = False Then
            Exit Sub
        End If
        ClvTipSer = ComboBoxTipSer.SelectedValue
        LimpiaPreBaja()
        Dame_clv_session_Reportes()
        LlenaTreeViewActivos()
        LlenaTreeViewBajas()
    End Sub

    Private Sub ComboBoxMotCan_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxMotCan.SelectedIndexChanged
        If IsNumeric(ComboBoxMotCan.SelectedValue) = False Then
            Exit Sub
        End If
        ClvMotCan = ComboBoxMotCan.SelectedValue
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If IsNumeric(Me.ComboBoxMotCan.SelectedValue) = False Then
            Exit Sub
        End If
        Insertar()
        PanelMotCan.Visible = False
    End Sub

    Private Sub ButtonInsertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonInsertar.Click

        If TreeViewActivos.Nodes.Count = 0 Then
            MsgBox("Selecciona un Aparato/Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If

        TreeViewActivos.Select()

        If Tipo = "S" Then
            If ChecaServicioPrincipal(ClvTipSer, AClvUnicaNet) = True Then
                Res = MsgBox("Al pasar a baja un Servicio Principal, pasar� el Aparato y todos sus Servicios Adicionales Activos. �Deseas continuar?", MsgBoxStyle.YesNo)
                If Res = 7 Then Exit Sub
            End If
        End If
        PanelMotCan.Visible = True
    End Sub

    Private Sub ButtonEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonEliminar.Click

        If TreeViewPreBajas.Nodes.Count = 0 Then
            MsgBox("Selecciona un Aparato/Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If

        TreeViewPreBajas.Select()

        Eliminar()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub ButtonBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBuscar.Click
        GLOCONTRATOSEL = 0
        FrmSelCliente.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Guardar()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        PanelMotCan.Visible = False
    End Sub

End Class