Imports System.Data.SqlClient
Public Class FrmCONEX
    Private opcionlocal As String = "N"

    Private Sub CONCONEXBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCONEXBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.ExtAdicTextBox.Text) = False Then
            MsgBox("Capture el Numero de Extenciones que desea Instalar ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Me.ExtAdicTextBox.Text <= 0 Then
            MsgBox("El Numero de Extenciones que desea Instalar debe ser mayor a 0 ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Contrato > 0 And gloClv_Orden > 0 And GloDetClave > 0 Then
            Me.Validate()
            Me.CONCONEXBindingSource.EndEdit()
            Me.CONCONEXTableAdapter.Connection = CON
            Me.CONCONEXTableAdapter.Update(Me.NewSofTvDataSet.CONCONEX)
            CON.Close()
            Me.Close()
        End If
    End Sub

    Private Sub bUSCA()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.CONCONEXTableAdapter.Connection = CON
            Me.CONCONEXTableAdapter.Fill(Me.NewSofTvDataSet.CONCONEX, New System.Nullable(Of Long)(CType(GloDetClave, Long)), New System.Nullable(Of Long)(CType(gloClv_Orden, Long)), New System.Nullable(Of Long)(CType(Contrato, Long)))
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub

    Private Sub FrmCONEX_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BORDetOrdSer_INTELIGENTETableAdapter.Connection = CON
        Me.BORDetOrdSer_INTELIGENTETableAdapter.Fill(Me.NewSofTvDataSet.BORDetOrdSer_INTELIGENTE, New System.Nullable(Of Long)(CType(GloDetClave, Long)))
        GloBndTrabajo = True
        GloBloqueaDetalle = True
        CON.Close()
    End Sub


    Private Sub FrmCONEX_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        bUSCA()
        If IsNumeric(Me.Clv_OrdenTextBox.Text) = False Then
            opcionlocal = "N"
            Me.CONCONEXBindingSource.AddNew()
            Me.ContratoTextBox.Text = Contrato
            Me.Clv_OrdenTextBox.Text = gloClv_Orden
            Me.ClaveTextBox.Text = GloDetClave
        Else
            opcionlocal = "M"
        End If
        If Bloquea = True Or opcion = "M" Or opcion = "C" Then
            Me.CONCONEXBindingNavigator.Enabled = False
            Me.ExtAdicTextBox.Enabled = False
        End If

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Contrato > 0 And gloClv_Orden > 0 And GloDetClave > 0 Then
            Me.CONCONEXTableAdapter.Connection = CON
            Me.CONCONEXTableAdapter.Delete(GloDetClave, gloClv_Orden, Contrato)
            Me.Close()
        Else
            MsgBox("No ahi Datos para borrar", MsgBoxStyle.Information)
        End If
        CON.Close()
    End Sub


    Private Sub CONCONEXBindingNavigator_RefreshItems(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCONEXBindingNavigator.RefreshItems

    End Sub
End Class