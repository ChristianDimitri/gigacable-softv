Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports CrystalDecisions.CrystalReports.Engine
Imports System.IO
Public Class FrmSelRepAgendaTecnico
    Public DstReporte As New DataSet
    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub FrmSelRepAgendaTecnico_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        Me.GroupBox1.BackColor = Me.BackColor
        Me.Muestra_Tecnicos_AlmacenTableAdapter.Connection = CON
        Me.Muestra_Tecnicos_AlmacenTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Tecnicos_Almacen, 0)
        Me.ComboBox1.Text = ""
        UspLlenaComboTecnicosVirtuales()
        CON.Close()
    End Sub

    Private Sub UspLlenaComboTecnicosVirtuales()
        Try
            BaseII.limpiaParametros()
            Me.ComboBox2.DataSource = BaseII.ConsultaDT("UspLlenaComboTecnicosVirtuales")
        Catch ex As Exception

        End Try
      
    End Sub
    Private Sub UspAsignacionTecnicoAlmacen()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVTECNICOVIRTUAL", SqlDbType.Int, GloTecnicoVirtual)
            BaseII.CreateMyParameter("@CLVTECNICOALMACEN", SqlDbType.Int, LocClv_tecnico)
            BaseII.CreateMyParameter("@fecha1", SqlDbType.DateTime, LocFecha1Agnd)
            BaseII.CreateMyParameter("@fecha2", SqlDbType.DateTime, LocFecha2Agnd)
            BaseII.Inserta("UspAsignacionTecnicoAlmacen")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    '] (@CLVTECNICOVIRTUAL INT,@CLVTECNICOALMACEN INT,@fecha1 datetime,@fecha2 datetime)

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If CheckBox3.Checked = False Then
            If Me.ComboBox1.Text = "" Then
                MsgBox("Debe de seleccionar un t�cnico", MsgBoxStyle.Information, "Agenda T�cnico")
                Exit Sub
            End If
            If CheckBox3.Checked = False Then
                If Me.ComboBox2.Text = "" Then
                    MsgBox("Debe de seleccionar un t�cnico", MsgBoxStyle.Information, "Agenda T�cnico")
                    Exit Sub
                End If
            End If
        End If

        LocFecha1Agnd = Me.DateTimePicker1.Text
        LocFecha2Agnd = Me.DateTimePicker2.Text
        LocClv_tecnico = Me.ComboBox1.SelectedValue
        GloTecnicoVirtual = Me.ComboBox2.SelectedValue
        If CheckBox2.Checked = False And CheckBox3.Checked = False Then
            UspAsignacionTecnicoAlmacen()
        End If

        If Me.CheckBox1.Checked = True Then
            LocopAgnd = 1
        Else
            LocopAgnd = 0
        End If
        'Locbndagnd = True
        REPORTEAGENDA()
        Me.Close()
    End Sub

    Private Sub REPORTEAGENDA()

        Dim frmic As New FrmImprimirCentralizada()
        Dim rd As New ReportDocument()
        DstReporte = New DataSet
        rd.Load(RutaReportes + "\ReportAgendaTecnicoRangoFechas" + ".rpt")
        Dim mySelectFormula As String = Nothing

        BaseII.limpiaParametros()
        If CheckBox3.Checked = True Then
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, GloTecnicoVirtual)
        Else
            BaseII.CreateMyParameter("@CLV_TECNICO", SqlDbType.Int, LocClv_tecnico)
        End If

        BaseII.CreateMyParameter("@fecha1", SqlDbType.DateTime, LocFecha1Agnd)
        BaseII.CreateMyParameter("@fecha2", SqlDbType.DateTime, LocFecha2Agnd)
        If CheckBox3.Checked = True Then
            BaseII.CreateMyParameter("@esvirtual", SqlDbType.Int, 1)
        Else
            BaseII.CreateMyParameter("@esvirtual", SqlDbType.Int, 0)
        End If

        Dim Lt As New List(Of String)

        Lt.Add("DameAgendaTecnico_ReporteNuevo")

        DstReporte = BaseII.ConsultaDS("DameAgendaTecnicoReporteNew", Lt)
        If CheckBox3.Checked = True Then
            mySelectFormula = "Agenda de Actividades Del T�cnico Virtual"
        Else
            mySelectFormula = "Agenda de Actividades Del T�cnico"
        End If

        Dim fechas As String = Nothing

        fechas = "De la Fecha: " + LocFecha1Agnd + " A la Fecha: " + LocFecha2Agnd
        rd.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
        rd.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        rd.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"
        rd.DataDefinition.FormulaFields("nomservicio").Text = "'" & fechas & "'"
        rd.SetDataSource(DstReporte)
        frmic.rd = rd
        frmic.ShowDialog()

        If LocopAgnd = 1 Then
            ReporteOrdenes()
            REPORTEQUEJAS()
        End If
    End Sub
    Private Sub ReporteOrdenes()
        Dim frmic As New FrmImprimirCentralizada()
        Dim rd As New ReportDocument()
        DstReporte = New DataSet
        Dim mySelectFormula As String = Nothing
        Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
        Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
        Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
        Dim Num1 As String = 0, Num2 As String = 0
        Dim nclv_trabajo As String = "0"
        Dim nClv_colonia As String = "0"
        Dim Status As String = Nothing

        DstReporte = uspReporteOrdenesDeServicio(0, Op1, Op2, Op3, Op4, Op5, StatusPen, StatusEje, StatusVis, CLng(Num1), CLng(Num2), Fec1Ini, Fec1Fin, Fec2Ini, Fec2Fin, nclv_trabajo, nClv_colonia, 0)
        If DstReporte.Tables(9).Rows.Count = 0 Then
            Exit Sub
        End If

        rd.Load(RutaReportes + "\rptReporteOrdenesDeServicio" + ".rpt")
        rd.SetDataSource(DstReporte)
       

        mySelectFormula = "Orden De Servicio: "
        rd.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"

        frmic.rd = rd
        frmic.ShowDialog()
    End Sub

    Private Sub REPORTEQUEJAS()
        Try
            Dim frmic As New FrmImprimirCentralizada()
            Dim rd As New ReportDocument()
            DstReporte = New DataSet
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0", Op6 As String = "0", Op7 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As Date = "01/01/1900", Fec1Fin As Date = "01/01/1900", Fec2Ini As Date = "01/01/1900", Fec2Fin As Date = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"

            Dim CON As New SqlConnection(MiConexion)
            Dim mySelectFormula As String = ""
            Dim Impresora_Ordenes As String = ""
            Dim a As Integer = 0
            Dim OpOrdenar As Integer = 0



            DstReporte = ReporteAreaTecnicaQuejas(GloClv_TipSer, Op1, Op2, Op3, Op4, Op5, Op6, StatusPen, StatusEje, StatusVis, Num1, Num2, Fec1Ini, Fec1Fin, Fec2Ini, Fec2Fin, nclv_trabajo, nClv_colonia, OpOrdenar, LocclvDepto, Op7, 0)
            If DstReporte.Tables(7).Rows.Count = 0 Then
                Exit Sub
            End If

            rd.Load(RutaReportes + "\ReporteFormatoQuejasBueno.rpt")
            rd.SetDataSource(DstReporte)

            mySelectFormula = "Quejas " + Me.ComboBox1.Text
            rd.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"

            frmic.rd = rd
            frmic.ShowDialog()

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Function uspReporteOrdenesDeServicio(ByVal clvTipSer As Integer, ByVal op1 As Integer, ByVal op2 As Integer, ByVal op3 As Integer, ByVal op4 As Integer, ByVal op5 As Integer,
                                             ByVal statusPen As Boolean, ByVal statusEje As Boolean, ByVal statusVis As Boolean, ByVal clvOrdenIni As Integer, ByVal clvOrdenFin As Integer,
                                             ByVal fecha1Ini As DateTime, ByVal fecha1Fin As DateTime, ByVal fecha2Ini As DateTime, ByVal fecha2Fin As DateTime, ByVal clvTrabajo As Integer,
                                             ByVal clvColonia As Integer, ByVal opOrden As Integer) As DataSet

        Dim tableNameList As New List(Of String)
        tableNameList.Add("CALLES")
        tableNameList.Add("CIUDADES")
        tableNameList.Add("CLIENTES")
        tableNameList.Add("COLONIAS")
        tableNameList.Add("Comentarios_DetalleOrden")
        tableNameList.Add("DetOrdSer")
        tableNameList.Add("OrdSer")
        tableNameList.Add("Rel_Contrato_Macs")
        tableNameList.Add("Rel_Contrato_NoInt")
        tableNameList.Add("ReporteAreaTecnicaOrdSer")
        tableNameList.Add("Servicio_Clientes")
        tableNameList.Add("Trabajos")
        tableNameList.Add("ServiciosActivosClientes")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clvTipSer", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@op1", SqlDbType.Int, 1)
        BaseII.CreateMyParameter("@op2", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@op3", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@op4", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@op5", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@statusPen", SqlDbType.Bit, 0)
        BaseII.CreateMyParameter("@statusEje", SqlDbType.Bit, 0)
        BaseII.CreateMyParameter("@statusVis", SqlDbType.Bit, 0)
        If Me.CheckBox3.Checked = True Then
            BaseII.CreateMyParameter("@clvOrdenIni", SqlDbType.Int, GloTecnicoVirtual)
        Else
            BaseII.CreateMyParameter("@clvOrdenIni", SqlDbType.Int, LocClv_tecnico)
        End If

        BaseII.CreateMyParameter("@clvOrdenFin", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@fecha1Ini", SqlDbType.DateTime, LocFecha1Agnd)
        BaseII.CreateMyParameter("@fecha1Fin", SqlDbType.DateTime, LocFecha2Agnd)
        BaseII.CreateMyParameter("@fecha2Ini", SqlDbType.DateTime, "1900-01-01")
        BaseII.CreateMyParameter("@fecha2Fin", SqlDbType.DateTime, "1900-01-01")
        BaseII.CreateMyParameter("@clvTrabajo", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clvColonia", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@opOrden", SqlDbType.Int, 0)
        Return BaseII.ConsultaDS("uspReporteOrdenesDeServicioAgenda", tableNameList)
    End Function

    Private Function ReporteAreaTecnicaQuejas(ByVal Clv_TipSer As Integer, ByVal op1 As Integer, ByVal op2 As Integer, ByVal op3 As Integer, ByVal op4 As Integer, ByVal op5 As Integer, ByVal op6 As Integer,
                                         ByVal StatusPen As Boolean, ByVal StatusEje As Boolean, ByVal StatusVis As Boolean, ByVal Clv_OrdenIni As Integer, ByVal Clv_OrdenFin As Integer,
                                         ByVal Fec1Ini As DateTime, ByVal Fec1Fin As DateTime, ByVal Fec2Ini As DateTime, ByVal Fec2Fin As DateTime, ByVal Clv_Trabajo As Integer, ByVal Clv_Colonia As Integer,
                                         ByVal OpOrden As Integer, ByVal clv_Depto As Integer, ByVal Op7 As Integer, ByVal Contrato As Integer) As DataSet

        Dim tableNameList As New List(Of String)
        tableNameList.Add("CALLES")
        tableNameList.Add("CIUDADES")
        tableNameList.Add("CLIENTES")
        tableNameList.Add("COLONIAS")
        tableNameList.Add("Quejas")
        tableNameList.Add("Rel_Contrato_Macs")
        tableNameList.Add("Rel_Contrato_NoInt")
        tableNameList.Add("ReporteAreaTecnicaQuejas")
        tableNameList.Add("Servicio_Clientes")
        tableNameList.Add("Trabajos")
        tableNameList.Add("XPARAMETROS")
        tableNameList.Add("XSERVICIOS")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@op1", SqlDbType.Int, 1)
        BaseII.CreateMyParameter("@op2", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@op3", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@op4", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@op5", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@op6", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@StatusPen", SqlDbType.Bit, 0)
        BaseII.CreateMyParameter("@StatusEje", SqlDbType.Bit, 0)
        BaseII.CreateMyParameter("@StatusVis", SqlDbType.Bit, 0)
        If Me.CheckBox3.Checked = True Then
            BaseII.CreateMyParameter("@Clv_OrdenIni", SqlDbType.Int, GloTecnicoVirtual)
        Else
            BaseII.CreateMyParameter("@Clv_OrdenIni", SqlDbType.Int, LocClv_tecnico)

        End If
      
        BaseII.CreateMyParameter("@Clv_OrdenFin", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Fec1Ini", SqlDbType.DateTime, LocFecha1Agnd)
        BaseII.CreateMyParameter("@Fec1Fin", SqlDbType.DateTime, LocFecha2Agnd)
        BaseII.CreateMyParameter("@Fec2Ini", SqlDbType.DateTime, "1900-01-01")
        BaseII.CreateMyParameter("@Fec2Fin", SqlDbType.DateTime, "1900-01-01")
        BaseII.CreateMyParameter("@Clv_Trabajo", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@OpOrden", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_Depto", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Op7", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, 0)
        Return BaseII.ConsultaDS("REPORTEQuejaAgenda", tableNameList)

    End Function

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If CheckBox3.Checked = True Then
            If CheckBox1.Checked = True Then
                CheckBox1.Checked = False
            End If
            If CheckBox2.Checked = True Then
                CheckBox2.Checked = False
            End If
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If CheckBox2.Checked = True Then
            If CheckBox3.Checked = True Then
                CheckBox3.Checked = False
            End If
        End If
    End Sub
End Class