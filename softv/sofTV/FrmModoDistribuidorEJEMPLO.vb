﻿Imports System.Data.SqlClient
Imports System.Text

Public Class FrmModoDistribuidorEJEMPLO

    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub


    Private Sub FrmMenu_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        llenarDistribuidorModo()

    End Sub

    Private Sub llenarDistribuidorModo()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, GloClv_Servicio)
        Dim dataTable As DataTable = BaseII.ConsultaDT("dameModoDistribuidor_PROCEJEMPLO")

        CheckedListBox1.DataSource = dataTable
        CheckedListBox1.DisplayMember = "Nombre"
        CheckedListBox1.ValueMember = "Clv_Plaza"
        ' Dim contador = 0, total = CheckedListBox1.Items.Count

        For Each row As DataRow In DataTable.Rows

            ' Dim strDetail = row.Item("Clv_Plaza")
            Dim isChecked As Boolean = row.Item("Checked")
            Dim index = DataTable.Rows.IndexOf(row)
            CheckedListBox1.SetItemChecked(index, isChecked)

            'If isChecked = True Then
            '    contador = contador + 1
            'End If
        Next row



    End Sub


    Private Sub CheckedListBox1_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles CheckedListBox1.ItemCheck

        'Dim messageBoxVB As New System.Text.StringBuilder()
        'messageBoxVB.AppendFormat("{0} = {1}", "Index", e.Index)
        'messageBoxVB.AppendLine()
        'messageBoxVB.AppendFormat("{0} = {1}", "NewValue", e.NewValue)
        'messageBoxVB.AppendLine()
        'messageBoxVB.AppendFormat("{0} = {1}", "CurrentValue", e.CurrentValue)
        'messageBoxVB.AppendLine()
        'MessageBox.Show(messageBoxVB.ToString(), "ItemCheck Event")

    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click

        For Each item As DataRowView In CheckedListBox1.Items   'CheckedListBox1.CheckedItems ' CheckedListBox1.Items    

            Dim value = item(CheckedListBox1.ValueMember)
            Dim estado = CheckedListBox1.GetItemCheckState(CheckedListBox1.Items.IndexOf(item)).ToString()
            Dim newModo = 0

            If estado = "Checked" Then
                newModo = 1
            End If

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@idmedio", SqlDbType.Int, value)
            BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, GloClv_Servicio)
            BaseII.CreateMyParameter("@checked", SqlDbType.Bit, newModo)
            BaseII.Inserta("guardaModoDistribuidor_PROCDEMUESTRA")

        Next

        MessageBox.Show("Cambios guardados")
        llenarDistribuidorModo()


    End Sub


    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged

        Dim accion = "" ' "False"

        If CheckBox1.Checked = False Then
            accion = "False"
        ElseIf CheckBox1.Checked = True Then
            accion = "True"
        End If

        For i As Integer = 0 To CheckedListBox1.Items.Count - 1
            CheckedListBox1.SetItemChecked(i, accion)
        Next i

    End Sub


    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Me.Close()
    End Sub


End Class
