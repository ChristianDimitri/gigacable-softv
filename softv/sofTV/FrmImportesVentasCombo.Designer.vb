﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmImportesVentasCombo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.cmbServicioPrincipal = New System.Windows.Forms.ComboBox()
        Me.cmbServicioSecundario = New System.Windows.Forms.ComboBox()
        Me.dgImportesCombos = New System.Windows.Forms.DataGridView()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.btnBorrar = New System.Windows.Forms.Button()
        Me.lblServicioPrincipal = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLVSERVICIO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DESCRIPCION = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLVGRUPOVENTAS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VENTAS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLVGRUPOSUCURSAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SUCURSAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLVGRUPOTELEMARKETING = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TELEMARKETING = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLVGRUPOCRM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CRM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLVGRUPOWEB = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WEB = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLVGRUPOVENTATELEFONICA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VENTATELEFONICA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLVGRUPOGestoria = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GESTORIA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgImportesCombos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancelar
        '
        Me.btnCancelar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.ForeColor = System.Drawing.Color.Black
        Me.btnCancelar.Location = New System.Drawing.Point(615, 425)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(136, 36)
        Me.btnCancelar.TabIndex = 23
        Me.btnCancelar.Text = "&Salir"
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'cmbServicioPrincipal
        '
        Me.cmbServicioPrincipal.DisplayMember = "Descripcion"
        Me.cmbServicioPrincipal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbServicioPrincipal.FormattingEnabled = True
        Me.cmbServicioPrincipal.Location = New System.Drawing.Point(347, 31)
        Me.cmbServicioPrincipal.Name = "cmbServicioPrincipal"
        Me.cmbServicioPrincipal.Size = New System.Drawing.Size(270, 23)
        Me.cmbServicioPrincipal.TabIndex = 25
        Me.cmbServicioPrincipal.ValueMember = "Clv_Servicio"
        '
        'cmbServicioSecundario
        '
        Me.cmbServicioSecundario.DisplayMember = "DESCRIPCION"
        Me.cmbServicioSecundario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbServicioSecundario.FormattingEnabled = True
        Me.cmbServicioSecundario.Location = New System.Drawing.Point(347, 79)
        Me.cmbServicioSecundario.Name = "cmbServicioSecundario"
        Me.cmbServicioSecundario.Size = New System.Drawing.Size(270, 23)
        Me.cmbServicioSecundario.TabIndex = 26
        Me.cmbServicioSecundario.ValueMember = "CLV_SERVICIO"
        '
        'dgImportesCombos
        '
        Me.dgImportesCombos.AllowUserToAddRows = False
        Me.dgImportesCombos.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgImportesCombos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgImportesCombos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgImportesCombos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.CLVSERVICIO, Me.DESCRIPCION, Me.CLVGRUPOVENTAS, Me.VENTAS, Me.CLVGRUPOSUCURSAL, Me.SUCURSAL, Me.CLVGRUPOTELEMARKETING, Me.TELEMARKETING, Me.CLVGRUPOCRM, Me.CRM, Me.CLVGRUPOWEB, Me.WEB, Me.CLVGRUPOVENTATELEFONICA, Me.VENTATELEFONICA, Me.CLVGRUPOGestoria, Me.GESTORIA})
        Me.dgImportesCombos.Location = New System.Drawing.Point(12, 163)
        Me.dgImportesCombos.Name = "dgImportesCombos"
        Me.dgImportesCombos.Size = New System.Drawing.Size(739, 256)
        Me.dgImportesCombos.TabIndex = 27
        '
        'btnAgregar
        '
        Me.btnAgregar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar.ForeColor = System.Drawing.Color.Black
        Me.btnAgregar.Location = New System.Drawing.Point(214, 121)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(136, 36)
        Me.btnAgregar.TabIndex = 28
        Me.btnAgregar.Text = "&Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = False
        '
        'btnBorrar
        '
        Me.btnBorrar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnBorrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBorrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBorrar.ForeColor = System.Drawing.Color.Black
        Me.btnBorrar.Location = New System.Drawing.Point(422, 121)
        Me.btnBorrar.Name = "btnBorrar"
        Me.btnBorrar.Size = New System.Drawing.Size(136, 36)
        Me.btnBorrar.TabIndex = 29
        Me.btnBorrar.Text = "&Borrar"
        Me.btnBorrar.UseVisualStyleBackColor = False
        '
        'lblServicioPrincipal
        '
        Me.lblServicioPrincipal.AutoSize = True
        Me.lblServicioPrincipal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblServicioPrincipal.Location = New System.Drawing.Point(126, 34)
        Me.lblServicioPrincipal.Name = "lblServicioPrincipal"
        Me.lblServicioPrincipal.Size = New System.Drawing.Size(215, 15)
        Me.lblServicioPrincipal.TabIndex = 30
        Me.lblServicioPrincipal.Text = "Seleccione el servicio principal :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(176, 82)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(165, 15)
        Me.Label1.TabIndex = 31
        Me.Label1.Text = "Seleccione el Adicional :"
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.Color.Black
        Me.btnGuardar.Location = New System.Drawing.Point(473, 425)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(136, 36)
        Me.btnGuardar.TabIndex = 32
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'ID
        '
        Me.ID.DataPropertyName = "ID"
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.Visible = False
        '
        'CLVSERVICIO
        '
        Me.CLVSERVICIO.DataPropertyName = "CLVSERVICIO"
        Me.CLVSERVICIO.HeaderText = "CLVSERVICIO"
        Me.CLVSERVICIO.Name = "CLVSERVICIO"
        Me.CLVSERVICIO.ReadOnly = True
        Me.CLVSERVICIO.Visible = False
        '
        'DESCRIPCION
        '
        Me.DESCRIPCION.DataPropertyName = "DESCRIPCION"
        Me.DESCRIPCION.HeaderText = "SERVICIO"
        Me.DESCRIPCION.Name = "DESCRIPCION"
        Me.DESCRIPCION.ReadOnly = True
        '
        'CLVGRUPOVENTAS
        '
        Me.CLVGRUPOVENTAS.DataPropertyName = "CLVGRUPOVENTAS"
        Me.CLVGRUPOVENTAS.HeaderText = "CLVGRUPOVENTAS"
        Me.CLVGRUPOVENTAS.Name = "CLVGRUPOVENTAS"
        Me.CLVGRUPOVENTAS.Visible = False
        '
        'VENTAS
        '
        Me.VENTAS.DataPropertyName = "VENTAS"
        Me.VENTAS.HeaderText = "VENTAS"
        Me.VENTAS.Name = "VENTAS"
        '
        'CLVGRUPOSUCURSAL
        '
        Me.CLVGRUPOSUCURSAL.DataPropertyName = "CLVGRUPOSUCURSAL"
        Me.CLVGRUPOSUCURSAL.HeaderText = "CLVGRUPOSUCURSAL"
        Me.CLVGRUPOSUCURSAL.Name = "CLVGRUPOSUCURSAL"
        Me.CLVGRUPOSUCURSAL.Visible = False
        '
        'SUCURSAL
        '
        Me.SUCURSAL.DataPropertyName = "SUCURSAL"
        Me.SUCURSAL.HeaderText = "SUCURSAL"
        Me.SUCURSAL.Name = "SUCURSAL"
        '
        'CLVGRUPOTELEMARKETING
        '
        Me.CLVGRUPOTELEMARKETING.DataPropertyName = "CLVGRUPOTELEMARKETING"
        Me.CLVGRUPOTELEMARKETING.HeaderText = "CLVGRUPOTELEMARKETING"
        Me.CLVGRUPOTELEMARKETING.Name = "CLVGRUPOTELEMARKETING"
        Me.CLVGRUPOTELEMARKETING.Visible = False
        '
        'TELEMARKETING
        '
        Me.TELEMARKETING.DataPropertyName = "TELEMARKETING"
        Me.TELEMARKETING.HeaderText = "TELEMARKETING"
        Me.TELEMARKETING.Name = "TELEMARKETING"
        '
        'CLVGRUPOCRM
        '
        Me.CLVGRUPOCRM.DataPropertyName = "CLVGRUPOCRM"
        Me.CLVGRUPOCRM.HeaderText = "CLVGRUPOCRM"
        Me.CLVGRUPOCRM.Name = "CLVGRUPOCRM"
        Me.CLVGRUPOCRM.Visible = False
        '
        'CRM
        '
        Me.CRM.DataPropertyName = "CRM"
        Me.CRM.HeaderText = "CRM"
        Me.CRM.Name = "CRM"
        '
        'CLVGRUPOWEB
        '
        Me.CLVGRUPOWEB.DataPropertyName = "CLVGRUPOWEB"
        Me.CLVGRUPOWEB.HeaderText = "CLVGRUPOWEB"
        Me.CLVGRUPOWEB.Name = "CLVGRUPOWEB"
        Me.CLVGRUPOWEB.Visible = False
        '
        'WEB
        '
        Me.WEB.DataPropertyName = "WEB"
        Me.WEB.HeaderText = "WEB"
        Me.WEB.Name = "WEB"
        '
        'CLVGRUPOVENTATELEFONICA
        '
        Me.CLVGRUPOVENTATELEFONICA.DataPropertyName = "CLVGRUPOVENTATELEFONICA"
        Me.CLVGRUPOVENTATELEFONICA.HeaderText = "CLVGRUPOVENTATELEFONICA"
        Me.CLVGRUPOVENTATELEFONICA.Name = "CLVGRUPOVENTATELEFONICA"
        Me.CLVGRUPOVENTATELEFONICA.Visible = False
        '
        'VENTATELEFONICA
        '
        Me.VENTATELEFONICA.DataPropertyName = "VENTATELEFONICA"
        Me.VENTATELEFONICA.HeaderText = "ATENCIÓN TELEFONICA"
        Me.VENTATELEFONICA.Name = "VENTATELEFONICA"
        '
        'CLVGRUPOGestoria
        '
        Me.CLVGRUPOGestoria.DataPropertyName = "CLVGRUPOGestoria"
        Me.CLVGRUPOGestoria.HeaderText = "CLVGRUPOGestoria"
        Me.CLVGRUPOGestoria.Name = "CLVGRUPOGestoria"
        Me.CLVGRUPOGestoria.Visible = False
        '
        'GESTORIA
        '
        Me.GESTORIA.DataPropertyName = "GESTORIA"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.GESTORIA.DefaultCellStyle = DataGridViewCellStyle2
        Me.GESTORIA.HeaderText = "GESTORIA"
        Me.GESTORIA.Name = "GESTORIA"
        '
        'FrmImportesVentasCombo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(763, 470)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblServicioPrincipal)
        Me.Controls.Add(Me.btnBorrar)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.dgImportesCombos)
        Me.Controls.Add(Me.cmbServicioSecundario)
        Me.Controls.Add(Me.cmbServicioPrincipal)
        Me.Controls.Add(Me.btnCancelar)
        Me.Name = "FrmImportesVentasCombo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Importes Adicionales por Ventas Combos"
        CType(Me.dgImportesCombos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents cmbServicioPrincipal As System.Windows.Forms.ComboBox
    Friend WithEvents cmbServicioSecundario As System.Windows.Forms.ComboBox
    Friend WithEvents dgImportesCombos As System.Windows.Forms.DataGridView
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents btnBorrar As System.Windows.Forms.Button
    Friend WithEvents lblServicioPrincipal As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLVSERVICIO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DESCRIPCION As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLVGRUPOVENTAS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VENTAS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLVGRUPOSUCURSAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SUCURSAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLVGRUPOTELEMARKETING As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TELEMARKETING As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLVGRUPOCRM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CRM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLVGRUPOWEB As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WEB As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLVGRUPOVENTATELEFONICA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VENTATELEFONICA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLVGRUPOGestoria As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GESTORIA As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
