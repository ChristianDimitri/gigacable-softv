Imports System.Data.SqlClient
Public Class FrmContratoF

    Private Sub FrmContratoF_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraTipServEricTableAdapter.Connection = CON
        Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric2.MuestraTipServEric, 0, 0)
        CON.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eTipSer = Me.ConceptoComboBox.SelectedValue
        eServicio = Me.ConceptoComboBox.Text

        If rbContrato.Checked = True Then
            eOpContratoF = 1
        ElseIf rbCalle.Checked = True Then
            eOpContratoF = 2
        ElseIf rbColonia.Checked = True Then
            eOpContratoF = 3
        End If

        FrmImprimirComision.Show()
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class