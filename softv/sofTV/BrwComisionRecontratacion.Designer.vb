﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwComisionRecontratacion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Label12 As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim Clv_calleLabel1 As System.Windows.Forms.Label
        Dim Label9 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.cmbNivelUsuario = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cmbPaquete = New System.Windows.Forms.ComboBox()
        Me.cmbGrupo = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblMesesPagadosFin = New System.Windows.Forms.TextBox()
        Me.lblMesesBajaFin = New System.Windows.Forms.TextBox()
        Me.lblMesesBajaIni = New System.Windows.Forms.TextBox()
        Me.lblMesesPagadosIni = New System.Windows.Forms.TextBox()
        Me.lblPorcentaje = New System.Windows.Forms.TextBox()
        Me.lblNivelUsuario = New System.Windows.Forms.TextBox()
        Me.lblGrupo = New System.Windows.Forms.TextBox()
        Me.lblServicioPrincipal = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblClave = New System.Windows.Forms.Label()
        Me.txtClave = New System.Windows.Forms.TextBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Grupo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nivel = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PorcentajeIni = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MesesPagadosInicial = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MesesPagadosFinal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MesesBajaInicial = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MesesBajaFinal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.BtnConsultar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Label12 = New System.Windows.Forms.Label()
        Label11 = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        Clv_calleLabel1 = New System.Windows.Forms.Label()
        Label9 = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label12.ForeColor = System.Drawing.Color.White
        Label12.Location = New System.Drawing.Point(17, 278)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(153, 15)
        Label12.TabIndex = 19
        Label12.Text = "Meses en Baja Inicial :"
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label11.ForeColor = System.Drawing.Color.White
        Label11.Location = New System.Drawing.Point(17, 208)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(160, 15)
        Label11.TabIndex = 17
        Label11.Text = "Meses Pagados Inicial :"
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label8.ForeColor = System.Drawing.Color.White
        Label8.Location = New System.Drawing.Point(17, 173)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(84, 15)
        Label8.TabIndex = 11
        Label8.Text = "Porcentaje :"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label7.ForeColor = System.Drawing.Color.White
        Label7.Location = New System.Drawing.Point(17, 138)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(101, 15)
        Label7.TabIndex = 9
        Label7.Text = "Nivel Usuario :"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label6.ForeColor = System.Drawing.Color.White
        Label6.Location = New System.Drawing.Point(17, 103)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(121, 15)
        Label6.TabIndex = 7
        Label6.Text = "Grupo de Ventas :"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.White
        NombreLabel.Location = New System.Drawing.Point(17, 68)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(127, 15)
        NombreLabel.TabIndex = 3
        NombreLabel.Text = "Servicio Principal :"
        '
        'Clv_calleLabel1
        '
        Clv_calleLabel1.AutoSize = True
        Clv_calleLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_calleLabel1.ForeColor = System.Drawing.Color.White
        Clv_calleLabel1.Location = New System.Drawing.Point(17, 50)
        Clv_calleLabel1.Name = "Clv_calleLabel1"
        Clv_calleLabel1.Size = New System.Drawing.Size(50, 15)
        Clv_calleLabel1.TabIndex = 1
        Clv_calleLabel1.Text = "Clave :"
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label9.ForeColor = System.Drawing.Color.White
        Label9.Location = New System.Drawing.Point(17, 313)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(146, 15)
        Label9.TabIndex = 21
        Label9.Text = "Meses en Baja Final :"
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label10.ForeColor = System.Drawing.Color.White
        Label10.Location = New System.Drawing.Point(17, 243)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(149, 15)
        Label10.TabIndex = 23
        Label10.Text = "Meses Pagados final :"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(9, 13)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.cmbNivelUsuario)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cmbPaquete)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cmbGrupo)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnBuscar)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtClave)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.DataGridView1)
        Me.SplitContainer1.Size = New System.Drawing.Size(836, 694)
        Me.SplitContainer1.SplitterDistance = 278
        Me.SplitContainer1.TabIndex = 38
        Me.SplitContainer1.TabStop = False
        '
        'cmbNivelUsuario
        '
        Me.cmbNivelUsuario.DisplayMember = "DESCRIPCION"
        Me.cmbNivelUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbNivelUsuario.FormattingEnabled = True
        Me.cmbNivelUsuario.Location = New System.Drawing.Point(17, 190)
        Me.cmbNivelUsuario.Name = "cmbNivelUsuario"
        Me.cmbNivelUsuario.Size = New System.Drawing.Size(243, 23)
        Me.cmbNivelUsuario.TabIndex = 37
        Me.cmbNivelUsuario.ValueMember = "CLVNIVEL"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(14, 172)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(121, 15)
        Me.Label5.TabIndex = 36
        Me.Label5.Text = "Nivel de Usuario :"
        '
        'cmbPaquete
        '
        Me.cmbPaquete.DisplayMember = "DESCRIPCION"
        Me.cmbPaquete.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbPaquete.FormattingEnabled = True
        Me.cmbPaquete.Location = New System.Drawing.Point(17, 147)
        Me.cmbPaquete.Name = "cmbPaquete"
        Me.cmbPaquete.Size = New System.Drawing.Size(243, 23)
        Me.cmbPaquete.TabIndex = 35
        Me.cmbPaquete.ValueMember = "CLVSERVICIO"
        '
        'cmbGrupo
        '
        Me.cmbGrupo.DisplayMember = "DESCRIPCION"
        Me.cmbGrupo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbGrupo.FormattingEnabled = True
        Me.cmbGrupo.Location = New System.Drawing.Point(17, 103)
        Me.cmbGrupo.Name = "cmbGrupo"
        Me.cmbGrupo.Size = New System.Drawing.Size(243, 23)
        Me.cmbGrupo.TabIndex = 34
        Me.cmbGrupo.ValueMember = "CLVGRUPO"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(14, 129)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 15)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Paquete :"
        '
        'btnBuscar
        '
        Me.btnBuscar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscar.ForeColor = System.Drawing.Color.Black
        Me.btnBuscar.Location = New System.Drawing.Point(17, 219)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(88, 23)
        Me.btnBuscar.TabIndex = 8
        Me.btnBuscar.Text = "&Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(Me.lblMesesPagadosFin)
        Me.Panel1.Controls.Add(Label10)
        Me.Panel1.Controls.Add(Me.lblMesesBajaFin)
        Me.Panel1.Controls.Add(Label9)
        Me.Panel1.Controls.Add(Me.lblMesesBajaIni)
        Me.Panel1.Controls.Add(Label12)
        Me.Panel1.Controls.Add(Me.lblMesesPagadosIni)
        Me.Panel1.Controls.Add(Label11)
        Me.Panel1.Controls.Add(Me.lblPorcentaje)
        Me.Panel1.Controls.Add(Label8)
        Me.Panel1.Controls.Add(Me.lblNivelUsuario)
        Me.Panel1.Controls.Add(Label7)
        Me.Panel1.Controls.Add(Me.lblGrupo)
        Me.Panel1.Controls.Add(Label6)
        Me.Panel1.Controls.Add(Me.lblServicioPrincipal)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(Clv_calleLabel1)
        Me.Panel1.Controls.Add(Me.lblClave)
        Me.Panel1.Location = New System.Drawing.Point(17, 296)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(243, 383)
        Me.Panel1.TabIndex = 8
        '
        'lblMesesPagadosFin
        '
        Me.lblMesesPagadosFin.BackColor = System.Drawing.Color.DarkOrange
        Me.lblMesesPagadosFin.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblMesesPagadosFin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMesesPagadosFin.ForeColor = System.Drawing.Color.Black
        Me.lblMesesPagadosFin.Location = New System.Drawing.Point(20, 261)
        Me.lblMesesPagadosFin.Name = "lblMesesPagadosFin"
        Me.lblMesesPagadosFin.ReadOnly = True
        Me.lblMesesPagadosFin.Size = New System.Drawing.Size(207, 14)
        Me.lblMesesPagadosFin.TabIndex = 24
        Me.lblMesesPagadosFin.TabStop = False
        '
        'lblMesesBajaFin
        '
        Me.lblMesesBajaFin.BackColor = System.Drawing.Color.DarkOrange
        Me.lblMesesBajaFin.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblMesesBajaFin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMesesBajaFin.ForeColor = System.Drawing.Color.Black
        Me.lblMesesBajaFin.Location = New System.Drawing.Point(20, 331)
        Me.lblMesesBajaFin.Name = "lblMesesBajaFin"
        Me.lblMesesBajaFin.ReadOnly = True
        Me.lblMesesBajaFin.Size = New System.Drawing.Size(207, 14)
        Me.lblMesesBajaFin.TabIndex = 22
        Me.lblMesesBajaFin.TabStop = False
        '
        'lblMesesBajaIni
        '
        Me.lblMesesBajaIni.BackColor = System.Drawing.Color.DarkOrange
        Me.lblMesesBajaIni.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblMesesBajaIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMesesBajaIni.ForeColor = System.Drawing.Color.Black
        Me.lblMesesBajaIni.Location = New System.Drawing.Point(20, 296)
        Me.lblMesesBajaIni.Name = "lblMesesBajaIni"
        Me.lblMesesBajaIni.ReadOnly = True
        Me.lblMesesBajaIni.Size = New System.Drawing.Size(207, 14)
        Me.lblMesesBajaIni.TabIndex = 20
        Me.lblMesesBajaIni.TabStop = False
        '
        'lblMesesPagadosIni
        '
        Me.lblMesesPagadosIni.BackColor = System.Drawing.Color.DarkOrange
        Me.lblMesesPagadosIni.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblMesesPagadosIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMesesPagadosIni.ForeColor = System.Drawing.Color.Black
        Me.lblMesesPagadosIni.Location = New System.Drawing.Point(20, 226)
        Me.lblMesesPagadosIni.Name = "lblMesesPagadosIni"
        Me.lblMesesPagadosIni.ReadOnly = True
        Me.lblMesesPagadosIni.Size = New System.Drawing.Size(207, 14)
        Me.lblMesesPagadosIni.TabIndex = 18
        Me.lblMesesPagadosIni.TabStop = False
        '
        'lblPorcentaje
        '
        Me.lblPorcentaje.BackColor = System.Drawing.Color.DarkOrange
        Me.lblPorcentaje.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblPorcentaje.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPorcentaje.ForeColor = System.Drawing.Color.Black
        Me.lblPorcentaje.Location = New System.Drawing.Point(20, 191)
        Me.lblPorcentaje.Name = "lblPorcentaje"
        Me.lblPorcentaje.ReadOnly = True
        Me.lblPorcentaje.Size = New System.Drawing.Size(207, 14)
        Me.lblPorcentaje.TabIndex = 12
        Me.lblPorcentaje.TabStop = False
        '
        'lblNivelUsuario
        '
        Me.lblNivelUsuario.BackColor = System.Drawing.Color.DarkOrange
        Me.lblNivelUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblNivelUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNivelUsuario.ForeColor = System.Drawing.Color.Black
        Me.lblNivelUsuario.Location = New System.Drawing.Point(20, 156)
        Me.lblNivelUsuario.Name = "lblNivelUsuario"
        Me.lblNivelUsuario.ReadOnly = True
        Me.lblNivelUsuario.Size = New System.Drawing.Size(207, 14)
        Me.lblNivelUsuario.TabIndex = 10
        Me.lblNivelUsuario.TabStop = False
        '
        'lblGrupo
        '
        Me.lblGrupo.BackColor = System.Drawing.Color.DarkOrange
        Me.lblGrupo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblGrupo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrupo.ForeColor = System.Drawing.Color.Black
        Me.lblGrupo.Location = New System.Drawing.Point(20, 121)
        Me.lblGrupo.Name = "lblGrupo"
        Me.lblGrupo.ReadOnly = True
        Me.lblGrupo.Size = New System.Drawing.Size(207, 14)
        Me.lblGrupo.TabIndex = 8
        Me.lblGrupo.TabStop = False
        '
        'lblServicioPrincipal
        '
        Me.lblServicioPrincipal.BackColor = System.Drawing.Color.DarkOrange
        Me.lblServicioPrincipal.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblServicioPrincipal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblServicioPrincipal.ForeColor = System.Drawing.Color.Black
        Me.lblServicioPrincipal.Location = New System.Drawing.Point(20, 86)
        Me.lblServicioPrincipal.Name = "lblServicioPrincipal"
        Me.lblServicioPrincipal.ReadOnly = True
        Me.lblServicioPrincipal.Size = New System.Drawing.Size(207, 14)
        Me.lblServicioPrincipal.TabIndex = 6
        Me.lblServicioPrincipal.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(3, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(154, 20)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Datos del Servicio"
        '
        'lblClave
        '
        Me.lblClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClave.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lblClave.Location = New System.Drawing.Point(75, 50)
        Me.lblClave.Name = "lblClave"
        Me.lblClave.Size = New System.Drawing.Size(165, 23)
        Me.lblClave.TabIndex = 2
        '
        'txtClave
        '
        Me.txtClave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClave.Location = New System.Drawing.Point(17, 61)
        Me.txtClave.Name = "txtClave"
        Me.txtClave.Size = New System.Drawing.Size(88, 21)
        Me.txtClave.TabIndex = 5
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(13, 12)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(217, 24)
        Me.CMBLabel1.TabIndex = 1
        Me.CMBLabel1.Text = "Buscar Comisión Por :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(14, 43)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(50, 15)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Clave :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(14, 85)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 15)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Grupo :"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.Descripcion, Me.Grupo, Me.Nivel, Me.PorcentajeIni, Me.MesesPagadosInicial, Me.MesesPagadosFinal, Me.MesesBajaInicial, Me.MesesBajaFinal})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(554, 694)
        Me.DataGridView1.TabIndex = 1
        Me.DataGridView1.TabStop = False
        '
        'ID
        '
        Me.ID.DataPropertyName = "ID"
        Me.ID.HeaderText = "Clave"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Paquete"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        '
        'Grupo
        '
        Me.Grupo.DataPropertyName = "Grupo"
        Me.Grupo.HeaderText = "Grupo de Ventas"
        Me.Grupo.Name = "Grupo"
        Me.Grupo.ReadOnly = True
        '
        'Nivel
        '
        Me.Nivel.DataPropertyName = "EsSupervisor"
        Me.Nivel.HeaderText = "Nivel Usuario"
        Me.Nivel.Name = "Nivel"
        Me.Nivel.ReadOnly = True
        '
        'PorcentajeIni
        '
        Me.PorcentajeIni.DataPropertyName = "Porcentaje"
        Me.PorcentajeIni.HeaderText = "Porcentaje "
        Me.PorcentajeIni.Name = "PorcentajeIni"
        Me.PorcentajeIni.ReadOnly = True
        '
        'MesesPagadosInicial
        '
        Me.MesesPagadosInicial.DataPropertyName = "MesesPagadosInicial"
        Me.MesesPagadosInicial.HeaderText = "Meses Pagados Inicial"
        Me.MesesPagadosInicial.Name = "MesesPagadosInicial"
        Me.MesesPagadosInicial.ReadOnly = True
        '
        'MesesPagadosFinal
        '
        Me.MesesPagadosFinal.DataPropertyName = "MesesPagadosFinal"
        Me.MesesPagadosFinal.HeaderText = "Meses Pagados Final"
        Me.MesesPagadosFinal.Name = "MesesPagadosFinal"
        Me.MesesPagadosFinal.ReadOnly = True
        '
        'MesesBajaInicial
        '
        Me.MesesBajaInicial.DataPropertyName = "MesesBajaInicial"
        Me.MesesBajaInicial.HeaderText = "Meses Baja Inicial"
        Me.MesesBajaInicial.Name = "MesesBajaInicial"
        Me.MesesBajaInicial.ReadOnly = True
        '
        'MesesBajaFinal
        '
        Me.MesesBajaFinal.DataPropertyName = "MesesBajaFinal"
        Me.MesesBajaFinal.HeaderText = "Meses Baja Final"
        Me.MesesBajaFinal.Name = "MesesBajaFinal"
        Me.MesesBajaFinal.ReadOnly = True
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(865, 668)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(136, 36)
        Me.btnSalir.TabIndex = 37
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'btnModificar
        '
        Me.btnModificar.BackColor = System.Drawing.Color.Orange
        Me.btnModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModificar.ForeColor = System.Drawing.Color.Black
        Me.btnModificar.Location = New System.Drawing.Point(865, 97)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(136, 36)
        Me.btnModificar.TabIndex = 36
        Me.btnModificar.Text = "&MODIFICAR"
        Me.btnModificar.UseVisualStyleBackColor = False
        '
        'BtnConsultar
        '
        Me.BtnConsultar.BackColor = System.Drawing.Color.Orange
        Me.BtnConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnConsultar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnConsultar.ForeColor = System.Drawing.Color.Black
        Me.BtnConsultar.Location = New System.Drawing.Point(865, 55)
        Me.BtnConsultar.Name = "BtnConsultar"
        Me.BtnConsultar.Size = New System.Drawing.Size(136, 36)
        Me.BtnConsultar.TabIndex = 35
        Me.BtnConsultar.Text = "&CONSULTA"
        Me.BtnConsultar.UseVisualStyleBackColor = False
        '
        'btnNuevo
        '
        Me.btnNuevo.BackColor = System.Drawing.Color.Orange
        Me.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo.ForeColor = System.Drawing.Color.Black
        Me.btnNuevo.Location = New System.Drawing.Point(865, 13)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(136, 36)
        Me.btnNuevo.TabIndex = 34
        Me.btnNuevo.Text = "&NUEVO"
        Me.btnNuevo.UseVisualStyleBackColor = False
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'BrwComisionRecontratacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1011, 721)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.BtnConsultar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Name = "BrwComisionRecontratacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Comisión de Recontratación"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents cmbNivelUsuario As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmbPaquete As System.Windows.Forms.ComboBox
    Friend WithEvents cmbGrupo As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblMesesBajaIni As System.Windows.Forms.TextBox
    Friend WithEvents lblMesesPagadosIni As System.Windows.Forms.TextBox
    Friend WithEvents lblPorcentaje As System.Windows.Forms.TextBox
    Friend WithEvents lblNivelUsuario As System.Windows.Forms.TextBox
    Friend WithEvents lblGrupo As System.Windows.Forms.TextBox
    Friend WithEvents lblServicioPrincipal As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblClave As System.Windows.Forms.Label
    Friend WithEvents txtClave As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents BtnConsultar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Grupo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nivel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PorcentajeIni As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MesesPagadosInicial As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MesesPagadosFinal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MesesBajaInicial As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MesesBajaFinal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblMesesPagadosFin As System.Windows.Forms.TextBox
    Friend WithEvents lblMesesBajaFin As System.Windows.Forms.TextBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
