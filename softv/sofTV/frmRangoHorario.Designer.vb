﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRangoHorario
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cbRangoHorario = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.gbDatosRangoHorario = New System.Windows.Forms.GroupBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtpHoraFinal = New System.Windows.Forms.DateTimePicker()
        Me.dtpHoraInicial = New System.Windows.Forms.DateTimePicker()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnBuscarMotivo = New System.Windows.Forms.Button()
        Me.dgRangoHorario = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HORAINICIAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HORAFINAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1.SuspendLayout()
        Me.gbDatosRangoHorario.SuspendLayout()
        CType(Me.dgRangoHorario, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(647, 432)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(113, 33)
        Me.btnSalir.TabIndex = 18
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModificar.Location = New System.Drawing.Point(647, 65)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(113, 33)
        Me.btnModificar.TabIndex = 17
        Me.btnModificar.Text = "Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar.Location = New System.Drawing.Point(647, 22)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(113, 33)
        Me.btnAgregar.TabIndex = 16
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cbRangoHorario)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.gbDatosRangoHorario)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.btnBuscarMotivo)
        Me.Panel1.Controls.Add(Me.dgRangoHorario)
        Me.Panel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(9, 18)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(632, 450)
        Me.Panel1.TabIndex = 15
        '
        'cbRangoHorario
        '
        Me.cbRangoHorario.DisplayMember = "HORARIO"
        Me.cbRangoHorario.FormattingEnabled = True
        Me.cbRangoHorario.Location = New System.Drawing.Point(4, 66)
        Me.cbRangoHorario.Name = "cbRangoHorario"
        Me.cbRangoHorario.Size = New System.Drawing.Size(197, 23)
        Me.cbRangoHorario.TabIndex = 19
        Me.cbRangoHorario.ValueMember = "ID"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(1, 48)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(132, 15)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Rango de Horarios:"
        '
        'gbDatosRangoHorario
        '
        Me.gbDatosRangoHorario.Controls.Add(Me.btnCancelar)
        Me.gbDatosRangoHorario.Controls.Add(Me.Label5)
        Me.gbDatosRangoHorario.Controls.Add(Me.Label4)
        Me.gbDatosRangoHorario.Controls.Add(Me.Label1)
        Me.gbDatosRangoHorario.Controls.Add(Me.dtpHoraFinal)
        Me.gbDatosRangoHorario.Controls.Add(Me.dtpHoraInicial)
        Me.gbDatosRangoHorario.Controls.Add(Me.btnGuardar)
        Me.gbDatosRangoHorario.Location = New System.Drawing.Point(240, 150)
        Me.gbDatosRangoHorario.Name = "gbDatosRangoHorario"
        Me.gbDatosRangoHorario.Size = New System.Drawing.Size(317, 129)
        Me.gbDatosRangoHorario.TabIndex = 6
        Me.gbDatosRangoHorario.TabStop = False
        Me.gbDatosRangoHorario.Text = "Rangos de Horarios"
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(236, 100)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 24
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(155, 67)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(12, 15)
        Me.Label5.TabIndex = 23
        Me.Label5.Text = "-"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(195, 44)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 15)
        Me.Label4.TabIndex = 22
        Me.Label4.Text = "Hora Final:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 44)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 15)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Hora Inicial:"
        '
        'dtpHoraFinal
        '
        Me.dtpHoraFinal.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpHoraFinal.Location = New System.Drawing.Point(198, 62)
        Me.dtpHoraFinal.Name = "dtpHoraFinal"
        Me.dtpHoraFinal.ShowUpDown = True
        Me.dtpHoraFinal.Size = New System.Drawing.Size(113, 21)
        Me.dtpHoraFinal.TabIndex = 21
        '
        'dtpHoraInicial
        '
        Me.dtpHoraInicial.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpHoraInicial.Location = New System.Drawing.Point(6, 62)
        Me.dtpHoraInicial.Name = "dtpHoraInicial"
        Me.dtpHoraInicial.ShowUpDown = True
        Me.dtpHoraInicial.Size = New System.Drawing.Size(113, 21)
        Me.dtpHoraInicial.TabIndex = 20
        '
        'btnGuardar
        '
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.Location = New System.Drawing.Point(155, 100)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 7
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(3, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(126, 15)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Buscar Rango por:"
        '
        'btnBuscarMotivo
        '
        Me.btnBuscarMotivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscarMotivo.Location = New System.Drawing.Point(126, 95)
        Me.btnBuscarMotivo.Name = "btnBuscarMotivo"
        Me.btnBuscarMotivo.Size = New System.Drawing.Size(75, 23)
        Me.btnBuscarMotivo.TabIndex = 1
        Me.btnBuscarMotivo.Text = "Buscar"
        Me.btnBuscarMotivo.UseVisualStyleBackColor = True
        '
        'dgRangoHorario
        '
        Me.dgRangoHorario.AllowUserToAddRows = False
        Me.dgRangoHorario.AllowUserToDeleteRows = False
        Me.dgRangoHorario.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgRangoHorario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgRangoHorario.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.HORAINICIAL, Me.HORAFINAL})
        Me.dgRangoHorario.Location = New System.Drawing.Point(207, 13)
        Me.dgRangoHorario.MultiSelect = False
        Me.dgRangoHorario.Name = "dgRangoHorario"
        Me.dgRangoHorario.ReadOnly = True
        Me.dgRangoHorario.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgRangoHorario.Size = New System.Drawing.Size(406, 424)
        Me.dgRangoHorario.TabIndex = 1
        '
        'ID
        '
        Me.ID.DataPropertyName = "ID"
        Me.ID.HeaderText = "Clave"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        '
        'HORAINICIAL
        '
        Me.HORAINICIAL.DataPropertyName = "HORAINICIAL"
        Me.HORAINICIAL.HeaderText = "Hora Inicial"
        Me.HORAINICIAL.Name = "HORAINICIAL"
        Me.HORAINICIAL.ReadOnly = True
        '
        'HORAFINAL
        '
        Me.HORAFINAL.DataPropertyName = "HORAFINAL"
        Me.HORAFINAL.HeaderText = "Hora Final"
        Me.HORAFINAL.Name = "HORAFINAL"
        Me.HORAFINAL.ReadOnly = True
        '
        'frmRangoHorario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(768, 487)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmRangoHorario"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Rango de Horario"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.gbDatosRangoHorario.ResumeLayout(False)
        Me.gbDatosRangoHorario.PerformLayout()
        CType(Me.dgRangoHorario, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents gbDatosRangoHorario As System.Windows.Forms.GroupBox
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnBuscarMotivo As System.Windows.Forms.Button
    Friend WithEvents dgRangoHorario As System.Windows.Forms.DataGridView
    Friend WithEvents cbRangoHorario As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpHoraFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpHoraInicial As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents HORAINICIAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents HORAFINAL As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
