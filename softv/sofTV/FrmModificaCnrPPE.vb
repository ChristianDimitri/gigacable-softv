Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class FrmModificaCnrPPE

    Private antComando As String
    Private antResultado As Integer
    Public antFechaH As String
    Public Consecutivo As Long

    Private Sub FrmModificaCnrPPE_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.dtpFechaH.MinDate = DateTime.Today
        antComando = Me.lblComando.Text
        antResultado = Me.lblResultado.Text

        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub ConCNRDigBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConCNRDigBindingNavigatorSaveItem.Click
        Me.GuardaCambios(Me.Consecutivo, Me.cbComando.SelectedItem.ToString, Convert.ToInt32(Me.cbResultado.SelectedItem.ToString), Me.dtpFechaH.Value)
        MsgBox("Se han hecho los cambios con exito", MsgBoxStyle.Information)
        Me.Close()
        Me.guardaBit()
        FrmCnrPPE.llenaGrid(1, 0, 0, "", 0, 0, DateTime.Today, DateTime.Today)
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub GuardaCambios(ByVal consec As Long, ByVal comando As String, ByVal resultado As Integer, ByVal fecha As DateTime)
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("ActualizaComandoPPE", con)
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.Add(New SqlParameter("@Consecutivo", SqlDbType.BigInt))
        com.Parameters("@Consecutivo").Value = consec
        com.Parameters.Add(New SqlParameter("@Comando", SqlDbType.VarChar, 15))
        com.Parameters("@Comando").Value = comando
        com.Parameters.Add(New SqlParameter("@Resultado", SqlDbType.Int))
        com.Parameters("@Resultado").Value = resultado
        com.Parameters.Add(New SqlParameter("@FechaHabilitar", SqlDbType.DateTime))
        com.Parameters("@FechaHabilitar").Value = fecha
        Try
            con.Open()
            com.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub guardaBit()
        bitsist(GloUsuario, CLng(Contrato), LocGloSistema, Me.Text, "Consecutivo en dbo.Cnr_PPE:" + CStr(eConsecutivo), "Comando: " + Me.antComando, "Comando: " + Me.cbComando.SelectedItem.ToString, LocClv_Ciudad)
        bitsist(GloUsuario, CLng(Contrato), LocGloSistema, Me.Text, "Consecutivo en dbo.Cnr_PPE:" + CStr(eConsecutivo), "Resultado :" + Me.antResultado.ToString, "Resultado :" + Me.cbResultado.SelectedItem.ToString, LocClv_Ciudad)
        bitsist(GloUsuario, CLng(Contrato), LocGloSistema, Me.Text, "Consecutivo en dbo.Cnr_PPE:" + CStr(eConsecutivo), "Fecha Habilitar: " + Me.antFechaH, "Fecha Habilitar: " + Me.dtpFechaH.Value.ToShortDateString, LocClv_Ciudad)
    End Sub

#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region

End Class