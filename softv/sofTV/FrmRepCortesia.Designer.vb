﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRepCortesia
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.cbActivo = New System.Windows.Forms.CheckBox()
        Me.cbInactivo = New System.Windows.Forms.CheckBox()
        Me.bnCancelar = New System.Windows.Forms.Button()
        Me.bnAceptar = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.SuspendLayout()
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(12, 18)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(140, 15)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Status de la Cortesía"
        '
        'cbActivo
        '
        Me.cbActivo.AutoSize = True
        Me.cbActivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbActivo.Location = New System.Drawing.Point(82, 71)
        Me.cbActivo.Name = "cbActivo"
        Me.cbActivo.Size = New System.Drawing.Size(63, 19)
        Me.cbActivo.TabIndex = 1
        Me.cbActivo.Text = "Activo"
        Me.cbActivo.UseVisualStyleBackColor = True
        '
        'cbInactivo
        '
        Me.cbInactivo.AutoSize = True
        Me.cbInactivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbInactivo.Location = New System.Drawing.Point(82, 96)
        Me.cbInactivo.Name = "cbInactivo"
        Me.cbInactivo.Size = New System.Drawing.Size(75, 19)
        Me.cbInactivo.TabIndex = 2
        Me.cbInactivo.Text = "Inactivo"
        Me.cbInactivo.UseVisualStyleBackColor = True
        '
        'bnCancelar
        '
        Me.bnCancelar.BackColor = System.Drawing.Color.DarkOrange
        Me.bnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnCancelar.Location = New System.Drawing.Point(224, 145)
        Me.bnCancelar.Name = "bnCancelar"
        Me.bnCancelar.Size = New System.Drawing.Size(136, 36)
        Me.bnCancelar.TabIndex = 9
        Me.bnCancelar.Text = "&Cancelar"
        Me.bnCancelar.UseVisualStyleBackColor = False
        '
        'bnAceptar
        '
        Me.bnAceptar.BackColor = System.Drawing.Color.DarkOrange
        Me.bnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnAceptar.Location = New System.Drawing.Point(82, 145)
        Me.bnAceptar.Name = "bnAceptar"
        Me.bnAceptar.Size = New System.Drawing.Size(136, 36)
        Me.bnAceptar.TabIndex = 8
        Me.bnAceptar.Text = "&Aceptar"
        Me.bnAceptar.UseVisualStyleBackColor = False
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmRepCortesia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(372, 193)
        Me.Controls.Add(Me.bnCancelar)
        Me.Controls.Add(Me.bnAceptar)
        Me.Controls.Add(Me.cbInactivo)
        Me.Controls.Add(Me.cbActivo)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Name = "FrmRepCortesia"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reporte de Cortesía"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents cbActivo As System.Windows.Forms.CheckBox
    Friend WithEvents cbInactivo As System.Windows.Forms.CheckBox
    Friend WithEvents bnCancelar As System.Windows.Forms.Button
    Friend WithEvents bnAceptar As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
