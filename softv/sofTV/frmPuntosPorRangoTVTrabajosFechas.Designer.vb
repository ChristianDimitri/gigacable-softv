﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPuntosPorRangoTVTrabajosFechas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnBorrar = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TXTPUNTOS = New System.Windows.Forms.TextBox()
        Me.TXTFINAL = New System.Windows.Forms.TextBox()
        Me.TXTTVINICIAL = New System.Windows.Forms.TextBox()
        Me.dgPuntosTVS = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TVINICIAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TVFINAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PUNTOS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgPuntosTVS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(146, 252)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(100, 23)
        Me.btnSalir.TabIndex = 21
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnBorrar
        '
        Me.btnBorrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBorrar.Location = New System.Drawing.Point(171, 67)
        Me.btnBorrar.Name = "btnBorrar"
        Me.btnBorrar.Size = New System.Drawing.Size(75, 23)
        Me.btnBorrar.TabIndex = 20
        Me.btnBorrar.Text = "&Borrar"
        Me.btnBorrar.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModificar.Location = New System.Drawing.Point(90, 67)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(75, 23)
        Me.btnModificar.TabIndex = 19
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(183, 8)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(51, 15)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "Puntos"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(98, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 15)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "TV Final"
        '
        'btnGuardar
        '
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.Location = New System.Drawing.Point(9, 66)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 16
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(15, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 15)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Tv Inicial"
        '
        'TXTPUNTOS
        '
        Me.TXTPUNTOS.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXTPUNTOS.Location = New System.Drawing.Point(172, 27)
        Me.TXTPUNTOS.Name = "TXTPUNTOS"
        Me.TXTPUNTOS.Size = New System.Drawing.Size(74, 21)
        Me.TXTPUNTOS.TabIndex = 14
        '
        'TXTFINAL
        '
        Me.TXTFINAL.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXTFINAL.Location = New System.Drawing.Point(90, 27)
        Me.TXTFINAL.Name = "TXTFINAL"
        Me.TXTFINAL.Size = New System.Drawing.Size(76, 21)
        Me.TXTFINAL.TabIndex = 13
        '
        'TXTTVINICIAL
        '
        Me.TXTTVINICIAL.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXTTVINICIAL.Location = New System.Drawing.Point(9, 27)
        Me.TXTTVINICIAL.Name = "TXTTVINICIAL"
        Me.TXTTVINICIAL.Size = New System.Drawing.Size(75, 21)
        Me.TXTTVINICIAL.TabIndex = 12
        '
        'dgPuntosTVS
        '
        Me.dgPuntosTVS.AllowUserToAddRows = False
        Me.dgPuntosTVS.AllowUserToDeleteRows = False
        Me.dgPuntosTVS.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgPuntosTVS.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgPuntosTVS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgPuntosTVS.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.TVINICIAL, Me.TVFINAL, Me.PUNTOS})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgPuntosTVS.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgPuntosTVS.Location = New System.Drawing.Point(9, 96)
        Me.dgPuntosTVS.Name = "dgPuntosTVS"
        Me.dgPuntosTVS.ReadOnly = True
        Me.dgPuntosTVS.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgPuntosTVS.Size = New System.Drawing.Size(237, 150)
        Me.dgPuntosTVS.TabIndex = 11
        '
        'ID
        '
        Me.ID.DataPropertyName = "ID"
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        Me.ID.Visible = False
        '
        'TVINICIAL
        '
        Me.TVINICIAL.DataPropertyName = "TVINICIAL"
        Me.TVINICIAL.HeaderText = "TV Inicial"
        Me.TVINICIAL.Name = "TVINICIAL"
        Me.TVINICIAL.ReadOnly = True
        '
        'TVFINAL
        '
        Me.TVFINAL.DataPropertyName = "TVFINAL"
        Me.TVFINAL.HeaderText = "TV Final"
        Me.TVFINAL.Name = "TVFINAL"
        Me.TVFINAL.ReadOnly = True
        '
        'PUNTOS
        '
        Me.PUNTOS.DataPropertyName = "PUNTOS"
        Me.PUNTOS.HeaderText = "Puntos"
        Me.PUNTOS.Name = "PUNTOS"
        Me.PUNTOS.ReadOnly = True
        '
        'frmPuntosPorRangoTVTrabajosFechas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(255, 286)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnBorrar)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TXTPUNTOS)
        Me.Controls.Add(Me.TXTFINAL)
        Me.Controls.Add(Me.TXTTVINICIAL)
        Me.Controls.Add(Me.dgPuntosTVS)
        Me.Name = "frmPuntosPorRangoTVTrabajosFechas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmPuntosPorRangoTVTrabajosFechas"
        CType(Me.dgPuntosTVS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnBorrar As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TXTPUNTOS As System.Windows.Forms.TextBox
    Friend WithEvents TXTFINAL As System.Windows.Forms.TextBox
    Friend WithEvents TXTTVINICIAL As System.Windows.Forms.TextBox
    Friend WithEvents dgPuntosTVS As System.Windows.Forms.DataGridView
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TVINICIAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TVFINAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PUNTOS As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
