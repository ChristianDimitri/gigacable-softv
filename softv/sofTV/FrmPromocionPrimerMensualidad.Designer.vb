﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPromocionPrimerMensualidad
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPromocionPrimerMensualidad))
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.dgvPromocion = New System.Windows.Forms.DataGridView()
        Me.cbTipSer = New System.Windows.Forms.ComboBox()
        Me.bnPromocion = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbGuardar = New System.Windows.Forms.ToolStripButton()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.CMBlbConDig = New System.Windows.Forms.Label()
        Me.CMBlbSinDig = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Clv_Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Importe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descuento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescuentoCombo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Importe0 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descuento0 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescuentoCombo0 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvPromocion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bnPromocion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnPromocion.SuspendLayout()
        Me.SuspendLayout()
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(40, 39)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(110, 15)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Tipo de Servicio"
        '
        'dgvPromocion
        '
        Me.dgvPromocion.AllowUserToAddRows = False
        Me.dgvPromocion.AllowUserToDeleteRows = False
        Me.dgvPromocion.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPromocion.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvPromocion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPromocion.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Servicio, Me.Servicio, Me.Importe, Me.Descuento, Me.DescuentoCombo, Me.Importe0, Me.Descuento0, Me.DescuentoCombo0})
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvPromocion.DefaultCellStyle = DataGridViewCellStyle8
        Me.dgvPromocion.Location = New System.Drawing.Point(40, 140)
        Me.dgvPromocion.Name = "dgvPromocion"
        Me.dgvPromocion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPromocion.Size = New System.Drawing.Size(945, 333)
        Me.dgvPromocion.TabIndex = 3
        '
        'cbTipSer
        '
        Me.cbTipSer.DisplayMember = "Concepto"
        Me.cbTipSer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTipSer.FormattingEnabled = True
        Me.cbTipSer.Location = New System.Drawing.Point(40, 57)
        Me.cbTipSer.Name = "cbTipSer"
        Me.cbTipSer.Size = New System.Drawing.Size(260, 23)
        Me.cbTipSer.TabIndex = 4
        Me.cbTipSer.ValueMember = "Clv_TipSer"
        '
        'bnPromocion
        '
        Me.bnPromocion.AddNewItem = Nothing
        Me.bnPromocion.CountItem = Nothing
        Me.bnPromocion.DeleteItem = Nothing
        Me.bnPromocion.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnPromocion.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbGuardar})
        Me.bnPromocion.Location = New System.Drawing.Point(0, 0)
        Me.bnPromocion.MoveFirstItem = Nothing
        Me.bnPromocion.MoveLastItem = Nothing
        Me.bnPromocion.MoveNextItem = Nothing
        Me.bnPromocion.MovePreviousItem = Nothing
        Me.bnPromocion.Name = "bnPromocion"
        Me.bnPromocion.PositionItem = Nothing
        Me.bnPromocion.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnPromocion.Size = New System.Drawing.Size(1028, 25)
        Me.bnPromocion.TabIndex = 5
        Me.bnPromocion.Text = "BindingNavigator1"
        '
        'tsbGuardar
        '
        Me.tsbGuardar.Image = CType(resources.GetObject("tsbGuardar.Image"), System.Drawing.Image)
        Me.tsbGuardar.Name = "tsbGuardar"
        Me.tsbGuardar.Size = New System.Drawing.Size(88, 22)
        Me.tsbGuardar.Text = "&GUARDAR"
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(880, 494)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 6
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'CMBlbConDig
        '
        Me.CMBlbConDig.BackColor = System.Drawing.Color.Black
        Me.CMBlbConDig.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlbConDig.ForeColor = System.Drawing.Color.White
        Me.CMBlbConDig.Location = New System.Drawing.Point(334, 101)
        Me.CMBlbConDig.Name = "CMBlbConDig"
        Me.CMBlbConDig.Size = New System.Drawing.Size(313, 23)
        Me.CMBlbConDig.TabIndex = 7
        Me.CMBlbConDig.Text = "Con Digital Contratado"
        Me.CMBlbConDig.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CMBlbSinDig
        '
        Me.CMBlbSinDig.BackColor = System.Drawing.Color.Black
        Me.CMBlbSinDig.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlbSinDig.ForeColor = System.Drawing.Color.White
        Me.CMBlbSinDig.Location = New System.Drawing.Point(653, 101)
        Me.CMBlbSinDig.Name = "CMBlbSinDig"
        Me.CMBlbSinDig.Size = New System.Drawing.Size(314, 23)
        Me.CMBlbSinDig.TabIndex = 8
        Me.CMBlbSinDig.Text = "Sin Digital Contratado"
        Me.CMBlbSinDig.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Clv_Servicio
        '
        Me.Clv_Servicio.DataPropertyName = "Clv_Servicio"
        Me.Clv_Servicio.HeaderText = "Clv_Servicio"
        Me.Clv_Servicio.Name = "Clv_Servicio"
        Me.Clv_Servicio.Visible = False
        '
        'Servicio
        '
        Me.Servicio.DataPropertyName = "Servicio"
        Me.Servicio.HeaderText = "Servicio"
        Me.Servicio.Name = "Servicio"
        Me.Servicio.Width = 250
        '
        'Importe
        '
        Me.Importe.DataPropertyName = "Importe"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.Format = "C2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.Importe.DefaultCellStyle = DataGridViewCellStyle2
        Me.Importe.HeaderText = "Importe"
        Me.Importe.Name = "Importe"
        Me.Importe.Width = 80
        '
        'Descuento
        '
        Me.Descuento.DataPropertyName = "Descuento"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.Format = "C2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.Descuento.DefaultCellStyle = DataGridViewCellStyle3
        Me.Descuento.HeaderText = "Descuento Servicio Único"
        Me.Descuento.Name = "Descuento"
        Me.Descuento.Width = 140
        '
        'DescuentoCombo
        '
        Me.DescuentoCombo.DataPropertyName = "DescuentoCombo"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.Format = "C2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.DescuentoCombo.DefaultCellStyle = DataGridViewCellStyle4
        Me.DescuentoCombo.HeaderText = "Descuento Combo"
        Me.DescuentoCombo.Name = "DescuentoCombo"
        '
        'Importe0
        '
        Me.Importe0.DataPropertyName = "Importe0"
        DataGridViewCellStyle5.Format = "c2"
        Me.Importe0.DefaultCellStyle = DataGridViewCellStyle5
        Me.Importe0.HeaderText = "Importe"
        Me.Importe0.Name = "Importe0"
        Me.Importe0.Width = 80
        '
        'Descuento0
        '
        Me.Descuento0.DataPropertyName = "Descuento0"
        DataGridViewCellStyle6.Format = "c2"
        Me.Descuento0.DefaultCellStyle = DataGridViewCellStyle6
        Me.Descuento0.HeaderText = "Descuento Servicio Único"
        Me.Descuento0.Name = "Descuento0"
        Me.Descuento0.Width = 140
        '
        'DescuentoCombo0
        '
        Me.DescuentoCombo0.DataPropertyName = "DescuentoCombo0"
        DataGridViewCellStyle7.Format = "c2"
        Me.DescuentoCombo0.DefaultCellStyle = DataGridViewCellStyle7
        Me.DescuentoCombo0.HeaderText = "Descuento Combo"
        Me.DescuentoCombo0.Name = "DescuentoCombo0"
        '
        'FrmPromocionPrimerMensualidad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1028, 542)
        Me.Controls.Add(Me.CMBlbSinDig)
        Me.Controls.Add(Me.CMBlbConDig)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnPromocion)
        Me.Controls.Add(Me.cbTipSer)
        Me.Controls.Add(Me.dgvPromocion)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Name = "FrmPromocionPrimerMensualidad"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Promoción Primer Mensualidad"
        CType(Me.dgvPromocion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bnPromocion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnPromocion.ResumeLayout(False)
        Me.bnPromocion.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents dgvPromocion As System.Windows.Forms.DataGridView
    Friend WithEvents cbTipSer As System.Windows.Forms.ComboBox
    Friend WithEvents bnPromocion As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents CMBlbConDig As System.Windows.Forms.Label
    Friend WithEvents CMBlbSinDig As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Clv_Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Importe As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descuento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescuentoCombo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Importe0 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descuento0 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescuentoCombo0 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
