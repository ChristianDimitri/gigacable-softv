﻿Imports sofTV.BAL
Imports System.Windows.Forms
Public Class FrmTrabajos
    Public clvTrabajo As Integer = 0
    Public clvTipoServicio As Integer = 0
    Dim tipoTrabajo As Char = Nothing
    Dim clvMaterial As Long = 0
    Dim SeCobraMaterial As Boolean = Nothing

    Private Sub FrmTrabajos_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Globals.GlobalSettings.Acc = Globals.Accion.Nothing
    End Sub

    Private Sub FrmTrabajos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        LlenarCmbMaterial()
        If Globals.GlobalSettings.Acc = Globals.Accion.Nuevo Then
            Me.pnlMaterial.Enabled = False
        End If

        If Globals.GlobalSettings.Acc = Globals.Accion.Consultar Or Globals.GlobalSettings.Acc = Globals.Accion.Modificar Then
            ConsultaTrabajo(Trabajo.GetOne(clvTrabajo))
            'ConRelTrabajosCuadrilla(clvTrabajo)''''ESTA PARTE SE TIENE QUE HABILITAR SÓLO PARA SALTILLO (INICIO)
            llenaGridMaterial()
        End If

        If checkCobroMaterial.Checked Then
            Me.pnlMaterial.Enabled = True
        Else
            Me.pnlMaterial.Enabled = False
        End If

        If Globals.GlobalSettings.Acc = Globals.Accion.Consultar Then
            Me.pnlMaterial.Enabled = False
            Me.pnlTrabajos.Enabled = False
        End If

        UspVerificarRangoTV()

        ''''ESTOS DOS LÍNEAS SE DEBEN COMENTAR CUANDO SE PUBLIQUE PARA SALTILLO
        Me.lblcuadrilla.visible = False
        Me.txtPuntosCuadrilla.Visible = False

        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub UspVerificarRangoTV()
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVTRABAJO", SqlDbType.Int, clvTrabajo)
            DT = BaseII.ConsultaDT("UspVerificarRangoTV")
            If DT.Rows.Count > 0 Then
                If DT.Rows(0)(0).ToString = "1" Then
                    Me.btnPuntosRangoTV.Visible = True
                    Me.PUNTOSTextBox.Enabled = False
                    Me.PUNTOSTextBox.Text = "0"
                Else
                    Me.btnPuntosRangoTV.Visible = False
                    Me.PUNTOSTextBox.Enabled = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LlenarCmbMaterial()
        cmbMaterial.DisplayMember = "concepto"
        cmbMaterial.ValueMember = "clv_tipo"
        cmbMaterial.DataSource = RelMaterialTrabajo.Muestra_Articulos_Acometida().Tables(0)
    End Sub

    Private Sub LlenarCmbArticulos(ByVal clv_tipo As Long)
        cmbArticulos.DataSource = Nothing
        cmbArticulos.Items.Clear()
        cmbArticulos.DisplayMember = "concepto"
        cmbArticulos.ValueMember = "NoArticulo"
        cmbArticulos.DataSource = RelMaterialTrabajo.Muestra_Articulos_Clasificacion(clv_tipo).Tables(0)
    End Sub

    Private Sub cmbMaterial_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbMaterial.SelectedIndexChanged
        If (Me.cmbMaterial.Items.Count > 0) Then
            If (Not cmbMaterial.SelectedItem Is Nothing) Then
                LlenarCmbArticulos(Long.Parse(cmbMaterial.SelectedValue.ToString))
            End If
        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles checkCobroMaterial.CheckedChanged
        If Me.checkCobroMaterial.Checked And Globals.GlobalSettings.Acc = Globals.Accion.Nuevo Then
            MsgBox("Proceda a Guardar el trabajo para realizar la carga de Material", MsgBoxStyle.Information)
            Me.checkCobroMaterial.CheckState = Windows.Forms.CheckState.Unchecked
            Exit Sub
        End If

        If Me.checkCobroMaterial.Checked Then
            Me.pnlMaterial.Enabled = True
        Else
            Me.pnlMaterial.Enabled = False
        End If
    End Sub

    Private Sub GuardaTrabajo(ByVal prmClvTxt As String, ByVal prmClvTipSer As Integer, ByVal prmDescripcion As String, ByVal prmPuntos As Decimal, ByVal prmCobranza As Boolean, _
                               ByVal prmTipo As String, ByVal prmProspectos As Boolean, ByVal prmSica As Boolean, ByVal prmCobroMaterial As Boolean)

        clvTrabajo = Trabajo.Add(Nothing, prmClvTxt, prmClvTipSer, prmDescripcion, prmPuntos, prmCobranza, prmTipo, prmProspectos, prmSica, prmCobroMaterial)
        UspActualizarPuntosTrabajo(prmPuntos)
    End Sub

    Private Sub ActualizaTrabajo(ByVal prmClvTxt As String, ByVal prmClvTipSer As Integer, ByVal prmDescripcion As String, ByVal prmPuntos As Decimal, ByVal prmCobranza As Boolean, _
                               ByVal prmTipo As String, ByVal prmProspectos As Boolean, ByVal prmSica As Boolean, ByVal prmCobroMaterial As Boolean)

        Trabajo.Edit(clvTrabajo, prmClvTxt, prmClvTipSer, prmDescripcion, prmPuntos, prmCobranza, prmTipo, prmProspectos, prmSica, prmCobroMaterial)
        UspActualizarPuntosTrabajo(prmPuntos)
    End Sub

    Private Sub UspActualizarPuntosTrabajo(ByVal prmPuntos As Decimal)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVTRABAJO", SqlDbType.Int, clvTrabajo)
            BaseII.CreateMyParameter("@PUNTOS", SqlDbType.Decimal, prmPuntos)
            BaseII.Inserta("UspActualizarPuntosTrabajo")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ConsultaTrabajo(ByRef trab As Trabajo)
        TRABAJOTextBox.Text = trab.TRABAJO
        DESCRIPCIONTextBox.Text = trab.DESCRIPCION
        PUNTOSTextBox.Text = IIf(trab.PUNTOS.HasValue, trab.PUNTOS, 0)
        checkCobroMaterial.Checked = IIf(trab.SeCobraMaterial.HasValue, trab.SeCobraMaterial, False)
        SICACheckBox.Checked = IIf(trab.SICA.HasValue, trab.SICA, False)
        If (trab.Tipo.Equals("O")) Then
            rbServicio.Checked = True
        Else
            rdQueja.Checked = True
        End If

        SeCobraMaterial = IIf(trab.SeCobraMaterial.HasValue, trab.SeCobraMaterial, False)
    End Sub

    Private Sub CONTRABAJOSBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONTRABAJOSBindingNavigatorSaveItem.Click
        If Len(TRABAJOTextBox.Text) = 0 Then
            MsgBox("Capture la clave del trabajo a capturar", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Len(DESCRIPCIONTextBox.Text) = 0 Then
            MsgBox("Capture la descripción del trabajo a capturar", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Len(PUNTOSTextBox.Text) = 0 Then
            MsgBox("Capture los puntos que aplica el trabajo, sino aplica ingrese 0", MsgBoxStyle.Information)
            Exit Sub
        End If
        If checkCobroMaterial.Checked And Me.dgvMaterial.RowCount = 0 And Globals.GlobalSettings.Acc = Globals.Accion.Modificar Then
            MsgBox("Capture el Material o deshabilite la opción de Cobro de Material", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.checkCobroMaterial.Checked = False Then
            If Me.checkCobroMaterial.CheckState <> SeCobraMaterial Then
                Dim res = MsgBox("Se eliminará el Material anteriormente capturado de la lista. ¿Deseas Continuar?", MsgBoxStyle.YesNo)
                If res = MsgBoxResult.Yes Then
                    RelMaterialTrabajo.Softv_DeleteRelMaterialTrabajoByclvTrabajo(clvTrabajo)
                    llenaGridMaterial()
                Else
                    Exit Sub
                End If
            End If
        End If

        If Me.checkCobroMaterial.Checked = False And Me.dgvMaterial.RowCount > 0 Then
            RelMaterialTrabajo.Softv_DeleteRelMaterialTrabajoByclvTrabajo(clvTrabajo)
            llenaGridMaterial()
        End If

        If Globals.GlobalSettings.Acc = Globals.Accion.Modificar Then
            ActualizaTrabajo(Me.TRABAJOTextBox.Text, clvTipoServicio, Me.DESCRIPCIONTextBox.Text, CDec(Me.PUNTOSTextBox.Text), False, IIf(rbServicio.Checked, "O", "Q"), 0, Me.SICACheckBox.CheckState, Me.checkCobroMaterial.CheckState)
        ElseIf Globals.GlobalSettings.Acc = Globals.Accion.Nuevo Then
            GuardaTrabajo(Me.TRABAJOTextBox.Text, clvTipoServicio, Me.DESCRIPCIONTextBox.Text, Me.PUNTOSTextBox.Text, False, IIf(rbServicio.Checked, "O", "Q"), 0, Me.SICACheckBox.CheckState, Me.checkCobroMaterial.CheckState)
        End If

        ''''ESTA PARTE SE TIENE QUE HABILITAR SÓLO PARA SALTILLO (INICIO)
        'If txtPuntosCuadrilla.Text.Length = 0 Then txtPuntosCuadrilla.Text = "0"
        'If IsNumeric(txtPuntosCuadrilla.Text) = False Then txtPuntosCuadrilla.Text = "0"
        'NueRelTrabajosCuadrilla(Me.TRABAJOTextBox.Text, Me.txtPuntosCuadrilla.Text)
        ''''ESTA PARTE SE TIENE QUE HABILITAR SÓLO PARA SALTILLO (FIN)

        Globals.GlobalSettings.Acc = Globals.Accion.Guardado
        MsgBox("Trabajo almacenado Satisfactoriamente", MsgBoxStyle.Information)
        If Globals.GlobalSettings.Acc = Globals.Accion.Modificar Then
            Me.Close()
        End If
    End Sub

    'Private Sub cambioRB(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdQueja.CheckedChanged, rbServicio.CheckedChanged
    '    Dim rbgenerico = CType(sender, RadioButton)
    '    If (rbgenerico.Checked) Then
    '        If (rbgenerico.Name = "rbServicio") Then
    '            tipoTrabajo = "O"
    '        Else
    '            tipoTrabajo = "Q"
    '        End If
    '    End If
    'End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim RES = MsgBox("¿Realmente deseas Eliminar el Trabajo?", MsgBoxStyle.YesNo)
        If RES = MsgBoxResult.Yes Then
            Trabajo.Delete(clvTrabajo)
        Else
            Exit Sub
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        If cmbMaterial.Items.Count = 0 Then
            MsgBox("No ha seleccionado un material para guardar, verifique que tenga material capturado")
            Exit Sub
        ElseIf cmbArticulos.Items.Count = 0 Then
            MsgBox("No ha seleccionado un tipo material para guardar")
            Exit Sub
        ElseIf Len(txtCantidad.Text) = 0 Then
            MsgBox("No ha seleccionado la cantidad")
            Exit Sub
        End If

        clvMaterial = RelMaterialTrabajo.Add(Nothing, CInt(Me.cmbMaterial.SelectedValue.ToString()), CInt(Me.cmbArticulos.SelectedValue.ToString()), CInt(Me.txtCantidad.Text), clvTrabajo, clvTipoServicio)
        If clvMaterial = 0 Then
            MsgBox("El artículo ya existe en la lista", MsgBoxStyle.Information)
            Me.txtCantidad.Clear()
            Exit Sub
        End If
        MsgBox("Material agregado satisfactoriamente", MsgBoxStyle.Information)
        llenaGridMaterial()
        Me.txtCantidad.Clear()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Try
            If IsNumeric(Me.dgvMaterial.SelectedCells(0).Value) = True Then
                RelMaterialTrabajo.Delete(Me.dgvMaterial.SelectedCells(0).Value)
                MsgBox("Material eliminado satisfactoriamente", MsgBoxStyle.Information)
                llenaGridMaterial()
            Else
                MsgBox("Seleccione el Material a eliminar de la Lista", MsgBoxStyle.Information)
                Exit Sub
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub llenaGridMaterial()
        If Me.checkCobroMaterial.Checked = False And Me.dgvMaterial.RowCount > 0 Then
            RelMaterialTrabajo.Softv_DeleteRelMaterialTrabajoByclvTrabajo(clvTrabajo)
        End If
        dgvMaterial.DataSource = Trabajo.ConsultaRelMaterialTrabajos(clvTrabajo)
    End Sub

    Private Sub SICACheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SICACheckBox.CheckedChanged
        If Me.SICACheckBox.Checked Then
            If Me.checkCobroMaterial.Checked Then
                MsgBox("Esta opción no puede ser seleccionada hasta quitar el Cobro de Material", MsgBoxStyle.Information)
                Me.SICACheckBox.Checked = False
                Exit Sub
            ElseIf Me.dgvMaterial.RowCount > 0 Then
                MsgBox("Primero debes eliminar el Material de la lista para poder activar esta opción", MsgBoxStyle.Information)
                Me.SICACheckBox.Checked = False
                Exit Sub
            End If
        End If
    End Sub

    Private Sub NueRelTrabajosCuadrilla(ByVal prmClvTrabajo As String, ByVal prmPuntosCuadrilla As Decimal)
        BaseII.limpiaParametros()

        BaseII.CreateMyParameter("@TRABAJO", SqlDbType.VarChar, prmClvTrabajo, 10)
        BaseII.CreateMyParameter("@PuntosCuadrilla", SqlDbType.Decimal, prmPuntosCuadrilla)

        BaseII.Inserta("NueRelTrabajosCuadrilla")
    End Sub

    Private Sub ConRelTrabajosCuadrilla(ByVal prmClvTrabajo As Integer)
        BaseII.limpiaParametros()

        BaseII.CreateMyParameter("@Clv_Trabajo", SqlDbType.Int, prmClvTrabajo)
        BaseII.CreateMyParameter("@puntosCuadrilla", ParameterDirection.Output, SqlDbType.Decimal)

        BaseII.ProcedimientoOutPut("ConRelTrabajosCuadrilla")

        Me.txtPuntosCuadrilla.Text = BaseII.dicoPar("@puntosCuadrilla").ToString()
    End Sub

#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region

    Private Sub btnPuntosRangoTV_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPuntosRangoTV.Click
        GloClvTrabajo = clvTrabajo
        FrmPuntosPorRangoTVTrabajos.Show()
    End Sub
End Class