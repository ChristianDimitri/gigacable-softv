Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Collections.Generic
Public Class FrmImprimirFac
    Private customersByCityReport As ReportDocument

    Private op As String = Nothing
    Private Titulo As String = Nothing
    'Private Const PARAMETER_FIELD_NAME As String = "Op"    

    Private Sub ConfigureCrystalReports(ByVal Clv_Factura As Long)

        Dim rDocument As New ReportDocument
        Dim dSet As New DataSet

        dSet = REPORTETicket(Clv_Factura, 0, 0, DateTime.Today, DateTime.Today, 0)
        rDocument.Load(RutaReportes + "\ReporteTicket.rpt")
        rDocument.SetDataSource(dSet)

        rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        rDocument.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        rDocument.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        rDocument.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        rDocument.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        rDocument.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        rDocument.DataDefinition.FormulaFields("Copia").Text = "'Copia'"

        CrystalReportViewer1.ReportSource = rDocument

        If GloOpFacturas = 3 Then
            CrystalReportViewer1.ShowExportButton = False
            CrystalReportViewer1.ShowPrintButton = False
            CrystalReportViewer1.ShowRefreshButton = False
        End If

        customersByCityReport = Nothing
    End Sub


    Private Sub OpenSubreport(ByVal reportObjectName As String)

        ' Preview the subreport.

    End Sub

    Private Sub ConfigureCrystalReports1(ByVal No_Contrato As Integer)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim mySelectFormula As String = Nothing
        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\Tarjetas.rpt"
        customersByCityReport.Load(reportPath)


        SetDBLogonForReport(connectionInfo, customersByCityReport)

        '@No_Contrato 
        customersByCityReport.SetParameterValue(0, No_Contrato)
        mySelectFormula = "*" & No_Contrato & "*"
        customersByCityReport.DataDefinition.FormulaFields("codigo").Text = "'" & mySelectFormula & "'"
        customersByCityReport.PrintOptions.PrinterName = "Datacard Printer"
        customersByCityReport.PrintToPrinter(1, True, 1, 1)

        'CrystalReportViewer1.ReportSource = customersByCityReport
        'CrystalReportViewer1.PrintReport()
        customersByCityReport = Nothing
    End Sub

    'Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
    '    customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
    '    'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

    '    Dim myTables As Tables = myReportDocument.Database.Tables
    '    Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
    '    For Each myTable In myTables
    '        Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
    '        myTableLogonInfo.ConnectionInfo = myConnectionInfo
    '        myTable.ApplyLogOnInfo(myTableLogonInfo)
    '        myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
    '    Next
    'End Sub

    Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub

    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ConfigureCrystalReportsPaquetes()
        Dim CON As New SqlConnection(MiConexion)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim Servicios As String = Nothing
        Dim Titulo As String = Nothing
        Dim Subtitulo As String = Nothing
        Dim Paquetes As String = Nothing
        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\ReportePaquetes.rpt"
        customersByCityReport.Load(reportPath)
        SetDBLogonForReport(connectionInfo, customersByCityReport)

        '@clv_session,, , , ,, ,, , , 
        customersByCityReport.SetParameterValue(0, LocClv_session)
        ',@contratado bit
        customersByCityReport.SetParameterValue(1, LocBndC)
        ',@Baja bit
        customersByCityReport.SetParameterValue(2, LocBndB)
        ',@Insta bit
        customersByCityReport.SetParameterValue(3, LocBndI)
        '@Desconect bit
        customersByCityReport.SetParameterValue(4, LocBndD)
        '@Suspendido bit
        customersByCityReport.SetParameterValue(5, LocBndS)
        '@Fuera bit
        customersByCityReport.SetParameterValue(6, LocBndF)
        ' @tempo
        customersByCityReport.SetParameterValue(7, LocBndDT)
        '@habilita
        customersByCityReport.SetParameterValue(8, LocValidaHab)
        '@periodo1
        customersByCityReport.SetParameterValue(9, LocPeriodo1)
        '@periodo2
        customersByCityReport.SetParameterValue(10, LocPeriodo2)
        CON.Open()
        Me.Dame_Servicios_SeleccionadosTableAdapter.Connection = CON
        Me.Dame_Servicios_SeleccionadosTableAdapter.Fill(Me.ProcedimientosArnoldo2.Dame_Servicios_Seleccionados, LocClv_session, Servicios, Paquetes)
        CON.Close()



        Titulo = "Resumen de Clientes con Servicio(s): " + Servicios
        Subtitulo = " Y con Paquetes: " + Paquetes


        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
        customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Subtitulo & "'"

        CrystalReportViewer1.ReportSource = customersByCityReport

        customersByCityReport = Nothing
    End Sub

    Private Sub FrmImprimirFac_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If LocGloOpRep = 0 Then
            If GloClv_Factura = 0 Then Me.Opacity = 0 Else Me.Opacity = 1
            ConfigureCrystalReports(GloClv_Factura)
        ElseIf LocGloOpRep = 1 Then
            'ConfigureCrystalReports1(LocTarjNo_Contrato)
        ElseIf LocGloOpRep = 5 Then
            ConfigureCrystalReportsDesPagAde()
        ElseIf LocGloOpRep = 2 Then
            Me.Text = "Reporte de Paquetes"
            ConfigureCrystalReportsPaquetes()
        ElseIf LocGloOpRep = 3 Then
            Me.Text = "Nota de Cr�dito"
            ConfigureCrystalReportsNota(gloClvNota)
        ElseIf LocGloOpRep = 6 Then
            ConfigureCrystalReports_tickets2(GloClv_Factura)
        End If
    End Sub

    Private Sub ConfigureCrystalReportsDesPagAde()
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword
        Dim reportPath As String = Nothing
        Dim Titulo As String = Nothing
        Dim Sucursal As String = Nothing
        Dim Ciudades As String = Nothing
        Ciudades = " Ciudad(es): " + LocCiudades
        reportPath = RutaReportes + "\ClientesconPagosAdelantados.rpt"
        Titulo = "Relaci�n de Clientes con Pagos Adelantados"

        Sucursal = " Sucursal: " + GloSucursal
        customersByCityReport.Load(reportPath)

        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)

        '@FECHA_INI
        customersByCityReport.SetParameterValue(0, GloFecha_Ini)
        '@FECHA_FIN
        customersByCityReport.SetParameterValue(1, GloFecha_Ini)
        '@TIPO
        customersByCityReport.SetParameterValue(2, "")
        '@SUCURSAL
        customersByCityReport.SetParameterValue(3, "0")
        '@CAJA
        customersByCityReport.SetParameterValue(4, "0")
        '@CAJERA
        customersByCityReport.SetParameterValue(5, "")
        '@OP
        customersByCityReport.SetParameterValue(6, "1")
        'Clv_Session
        customersByCityReport.SetParameterValue(7, glosessioncar)



        Dim eFechaTitulo As String = Nothing
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
        eFechaTitulo = "de la Fecha " & GloFecha_Ini
        customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFechaTitulo & "'"
        customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Sucursal & "'"
        customersByCityReport.DataDefinition.FormulaFields("Ciudades").Text = "'" & Ciudades & "'"


        CrystalReportViewer1.ReportSource = customersByCityReport
        CrystalReportViewer1.Zoom(75)

        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing

    End Sub

    Private Sub ConfigureCrystalReportsNota(ByVal nota As Long)
        Dim rDocument As New ReportDocument
        Dim dSet As New DataSet

        dSet = REPORTENotaDeCredito(nota, 0, 0, DateTime.Today, DateTime.Today, 0)
        rDocument.Load(RutaReportes + "\REPORTENotasCredito.rpt")
        rDocument.SetDataSource(dSet)

        rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
        rDocument.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        rDocument.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        rDocument.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        rDocument.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        rDocument.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        rDocument.DataDefinition.FormulaFields("Copia").Text = "'Copia'"

        CrystalReportViewer1.ReportSource = rDocument
        customersByCityReport = Nothing
    End Sub
    Private Sub SetDBLogonForReport1(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub ConfigureCrystalReports_tickets2(ByVal Clv_Factura As Long)
        Dim ba As Boolean = False
        Select Case IdSistema
            Case "LO"
                customersByCityReport = New ReporteCajasTickets_2Log
            Case "YU"
                customersByCityReport = New ReporteCajasTickets_2Log
        End Select
        Dim connectionInfo As New ConnectionInfo
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword
        Dim reportPath As String = Nothing
        SetDBLogonForReport(connectionInfo, customersByCityReport)
        customersByCityReport.SetParameterValue(0, Clv_Factura)
        'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
        CrystalReportViewer1.ReportSource = customersByCityReport
        CrystalReportViewer1.ShowExportButton = True
        CrystalReportViewer1.ShowPrintButton = True
        CrystalReportViewer1.ShowRefreshButton = True

    End Sub

    Private Function REPORTETicket(ByVal CLV_FACTURA As Integer, ByVal CLV_FACTURA_INI As Integer, ByVal CLV_FACTURA_FIN As Integer, ByVal FECHA_INI As DateTime, ByVal FECHA_FIN As DateTime, ByVal OP As Integer) As DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("CALLES")
        tableNameList.Add("CATALOGOCAJAS")
        tableNameList.Add("CIUDADES")
        tableNameList.Add("CLIENTES")
        tableNameList.Add("COLONIAS")
        tableNameList.Add("DATOSFISCALES")
        tableNameList.Add("DETFACTURAS")
        tableNameList.Add("DETFACTURASIMPUESTOS")
        tableNameList.Add("FACTURAS")
        tableNameList.Add("GENERALDESCONEXION")
        tableNameList.Add("ReportesFacturas;1")
        tableNameList.Add("SUCURSALES")
        tableNameList.Add("USUARIOS")
        tableNameList.Add("PROMO")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_FACTURA", SqlDbType.Int, CLV_FACTURA)
        BaseII.CreateMyParameter("@CLV_FACTURA_INI", SqlDbType.Int, CLV_FACTURA_INI)
        BaseII.CreateMyParameter("@CLV_FACTURA_FIN", SqlDbType.Int, CLV_FACTURA_FIN)
        BaseII.CreateMyParameter("@FECHA_INI", SqlDbType.DateTime, FECHA_INI)
        BaseII.CreateMyParameter("@FECHA_FIN", SqlDbType.DateTime, FECHA_FIN)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, OP)
        Return BaseII.ConsultaDS("REPORTETicket", tableNameList)
    End Function

    Private Function REPORTENotaDeCredito(ByVal CLAVE As Integer, ByVal CLV_NOTA_INI As Integer, ByVal CLV_NOTA_FIN As Integer, ByVal FECHA_INI As DateTime, ByVal FECHA_FIN As DateTime, ByVal OP As Integer) As DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("DETFACTURAS_NOTADECREDITO")
        tableNameList.Add("NOTAS_DE_CREDITO")
        tableNameList.Add("REL_NOTACREDITO_CONCEPTOSSERV")
        tableNameList.Add("REPORTENOTACREDITO")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLAVE", SqlDbType.Int, CLAVE)
        BaseII.CreateMyParameter("@CLV_NOTA_INI", SqlDbType.Int, CLV_NOTA_INI)
        BaseII.CreateMyParameter("@CLV_NOTA_FIN", SqlDbType.Int, CLV_NOTA_FIN)
        BaseII.CreateMyParameter("@FECHA_INI", SqlDbType.DateTime, FECHA_INI)
        BaseII.CreateMyParameter("@FECHA_FIN", SqlDbType.DateTime, FECHA_FIN)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, OP)
        Return BaseII.ConsultaDS("REPORTENotaDeCredito", tableNameList)
    End Function

End Class