﻿Public Class FrmCataMedioTelmex


    Private Sub FrmCataMedioTelmex_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ColoreaFrm(Me)
        'colorea(Me, Me.Name)
        llenarComboTipo()
        llenar_RelMedioServ()   ' Relacion medio servicio
        llenar_RelClvServicioMedioTelmexList() ' Relacion - ClvServicio - Medio Telmex


        BindingNavigatorDeleteItem.Visible = False
        Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Enabled = True




        'If bndCatalogoTelmex = "M" Or bndCatalogoTelmex = "N" Then
        '    Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Enabled = True
        '    tipoCombo.Enabled = True

        'End If


        'If bndCatalogoTelmex = "C" Then
        '    Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Enabled = False
        '    tipoCombo.Enabled = False

        'ElseIf bndCatalogoTelmex = "N" Then

        'End If


        'If bndCatalogoTelmex = "M" Or bndCatalogoTelmex = "C" Then

        '    BaseII.limpiaParametros()
        '    BaseII.CreateMyParameter("@IdModelo", SqlDbType.BigInt, IdModeloT)
        '    Dim dt As DataTable
        '    dt = BaseII.ConsultaDT("GetModelosByIdModelo")
        '    Dim dr As DataRow
        '    For Each dr In dt.Rows
        '        IdModeloTxt.Text = dr("IdModelo")
        '        modeloTxt.Text = dr("Modelo")
        '        tipoCombo.SelectedValue = dr("IdMarca")
        '        precioTxt.Text = dr("Precio")
        '    Next

        'End If

    End Sub

    Private Sub llenar_RelMedioServ()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.BigInt, GloClv_TipSer) 'Viene desde el FrmServicios
        Dim dataTable As DataTable = BaseII.ConsultaDT("RelMedioServbyClvTipserList")

    End Sub


    Private Sub llenar_RelClvServicioMedioTelmexList()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.BigInt, GloClv_Servicio)
        BaseII.CreateMyParameter("@tiposuscriptor", SqlDbType.BigInt, GloClv_TipSer)
        Dim dataTable As DataTable = BaseII.ConsultaDT("RelClvServicioMedioTelmexList")

        CheckedListBox1.DataSource = dataTable
        CheckedListBox1.DisplayMember = "IdMedio"
        CheckedListBox1.ValueMember = "Descripcion"
        ' Dim contador = 0, total = CheckedListBox1.Items.Count

        For Each row As DataRow In dataTable.Rows

            ' Dim strDetail = row.Item("Clv_Plaza")
            Dim isChecked As Boolean = row.Item("Checked")
            Dim index = dataTable.Rows.IndexOf(row)
            CheckedListBox1.SetItemChecked(index, isChecked)

            'If isChecked = True Then
            '    contador = contador + 1
            'End If
        Next row



    End Sub


    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    Private Sub llenarComboTipo()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, 0) 'Todas las marcas
        tipoCombo.DataSource = BaseII.ConsultaDT("MUESTRA_TIPOCLIENTES")

    End Sub



    Private Sub Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Click

        For Each item As DataRowView In CheckedListBox1.Items   'CheckedListBox1.CheckedItems ' CheckedListBox1.Items    

            Dim value = item(CheckedListBox1.ValueMember)
            Dim estado = CheckedListBox1.GetItemCheckState(CheckedListBox1.Items.IndexOf(item)).ToString()
            Dim newModo = 0

            If estado = "Checked" Then
                newModo = 1
            End If

            'BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, value)
            'BaseII.CreateMyParameter("@modoAudi", SqlDbType.Int, newModo)
            'BaseII.Inserta("guardaModoDistribuidor_PROCDEMUESTRA")

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, 9999)
            BaseII.CreateMyParameter("@idmedio", SqlDbType.Int, 1)
            BaseII.CreateMyParameter("@precio", SqlDbType.Int, 20)
            BaseII.CreateMyParameter("@Select", SqlDbType.Bit, 1)
            BaseII.CreateMyParameter("@TipoSucriptor", SqlDbType.Int, 1)
            BaseII.Inserta("AddRelClvServicioMedioTelmex")

        Next

        MessageBox.Show("Cambios guardados")
        'llenarDistribuidorModo()
        llenar_RelMedioServ()






        'Dim yaExiste As Integer

        'If modeloTxt.Text.Trim() = "" Then
        '    MessageBox.Show("Ingrese el Modelo")
        '    Exit Sub
        'End If
        'If precioTxt.Text.Trim() = "" Then
        '    MessageBox.Show("Ingrese el Precio")
        '    Exit Sub
        'End If

        'Dim idMarcaCombo As Long = tipoCombo.SelectedValue()

        'If bndCatalogoTelmex = "N" Then
        '    If idMarcaCombo > 0 Then
        '        BaseII.limpiaParametros()
        '        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.BigInt, 1)
        '        BaseII.CreateMyParameter("@idmedio", SqlDbType.BigInt, 2)
        '        BaseII.CreateMyParameter("@precio", SqlDbType.Money, 80)
        '        BaseII.CreateMyParameter("@Select", SqlDbType.Bit, True)
        '        BaseII.CreateMyParameter("@TipoSucriptor", SqlDbType.BigInt, 1)
        '        Dim dt As DataTable
        '        dt = BaseII.ConsultaDT("AddRelClvServicioMedioTelmex")
        '        Dim dr As DataRow
        '        For Each dr In dt.Rows
        '            yaExiste = dr("Error")
        '        Next

        '        If yaExiste = True Then
        '            MessageBox.Show("Ya existe este Modelo")
        '        Else
        '            MessageBox.Show("Modelo agregado correctamente")
        '        End If
        '    Else
        '        MessageBox.Show("Seleccione una Marca")
        '        Exit Sub
        '    End If
        'End If

        'If bndCatalogoTelmex = "M" Then

        '    BaseII.limpiaParametros()
        '    BaseII.CreateMyParameter("@IdModelo", SqlDbType.BigInt, IdModeloTxt.Text)
        '    BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, modeloTxt.Text, 500)
        '    BaseII.CreateMyParameter("@IdMarca", SqlDbType.BigInt, idMarcaCombo)
        '    BaseII.CreateMyParameter("@Precio", SqlDbType.Money, precioTxt.Text)
        '    Dim dt As DataTable
        '    dt = BaseII.ConsultaDT("UpdateModelos")
        '    Dim dr As DataRow
        '    For Each dr In dt.Rows
        '        yaExiste = dr("Error")
        '    Next


        '    If yaExiste = 1 Then
        '        MessageBox.Show("No se ha actualizado")
        '    ElseIf yaExiste = 2 Then
        '        MessageBox.Show("No se ha actualizado")
        '    Else
        '        MessageBox.Show("Modelo actualizado correctamente")
        '    End If
        'End If
        Me.Close()
    End Sub






End Class