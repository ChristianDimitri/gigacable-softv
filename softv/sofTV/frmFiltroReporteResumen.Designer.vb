﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFiltroReporteResumen
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btAceptar = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.btDeseleccionaUno = New System.Windows.Forms.Button()
        Me.btSeleccionaUno = New System.Windows.Forms.Button()
        Me.btSeleccionarTodos = New System.Windows.Forms.Button()
        Me.lbTrabajosSeleccionados = New System.Windows.Forms.ListBox()
        Me.lbTrabajosASeleccionar = New System.Windows.Forms.ListBox()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 81)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(207, 15)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Seleccione los trabajos a filtrar"
        '
        'btAceptar
        '
        Me.btAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btAceptar.Location = New System.Drawing.Point(400, 432)
        Me.btAceptar.Name = "btAceptar"
        Me.btAceptar.Size = New System.Drawing.Size(98, 31)
        Me.btAceptar.TabIndex = 14
        Me.btAceptar.Text = "Aceptar"
        Me.btAceptar.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(272, 327)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(51, 23)
        Me.Button4.TabIndex = 13
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'btDeseleccionaUno
        '
        Me.btDeseleccionaUno.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btDeseleccionaUno.Location = New System.Drawing.Point(272, 298)
        Me.btDeseleccionaUno.Name = "btDeseleccionaUno"
        Me.btDeseleccionaUno.Size = New System.Drawing.Size(51, 23)
        Me.btDeseleccionaUno.TabIndex = 12
        Me.btDeseleccionaUno.Text = "<"
        Me.btDeseleccionaUno.UseVisualStyleBackColor = True
        '
        'btSeleccionaUno
        '
        Me.btSeleccionaUno.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btSeleccionaUno.Location = New System.Drawing.Point(272, 158)
        Me.btSeleccionaUno.Name = "btSeleccionaUno"
        Me.btSeleccionaUno.Size = New System.Drawing.Size(51, 23)
        Me.btSeleccionaUno.TabIndex = 11
        Me.btSeleccionaUno.Text = ">"
        Me.btSeleccionaUno.UseVisualStyleBackColor = True
        '
        'btSeleccionarTodos
        '
        Me.btSeleccionarTodos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btSeleccionarTodos.Location = New System.Drawing.Point(272, 187)
        Me.btSeleccionarTodos.Name = "btSeleccionarTodos"
        Me.btSeleccionarTodos.Size = New System.Drawing.Size(51, 23)
        Me.btSeleccionarTodos.TabIndex = 10
        Me.btSeleccionarTodos.Text = ">>"
        Me.btSeleccionarTodos.UseVisualStyleBackColor = True
        '
        'lbTrabajosSeleccionados
        '
        Me.lbTrabajosSeleccionados.DisplayMember = "DESCRIPCION"
        Me.lbTrabajosSeleccionados.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbTrabajosSeleccionados.FormattingEnabled = True
        Me.lbTrabajosSeleccionados.ItemHeight = 15
        Me.lbTrabajosSeleccionados.Location = New System.Drawing.Point(329, 117)
        Me.lbTrabajosSeleccionados.Name = "lbTrabajosSeleccionados"
        Me.lbTrabajosSeleccionados.Size = New System.Drawing.Size(285, 304)
        Me.lbTrabajosSeleccionados.TabIndex = 9
        Me.lbTrabajosSeleccionados.ValueMember = "CLVTRABAJO"
        '
        'lbTrabajosASeleccionar
        '
        Me.lbTrabajosASeleccionar.DisplayMember = "DESCRIPCION"
        Me.lbTrabajosASeleccionar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbTrabajosASeleccionar.FormattingEnabled = True
        Me.lbTrabajosASeleccionar.ItemHeight = 15
        Me.lbTrabajosASeleccionar.Location = New System.Drawing.Point(15, 115)
        Me.lbTrabajosASeleccionar.Name = "lbTrabajosASeleccionar"
        Me.lbTrabajosASeleccionar.Size = New System.Drawing.Size(251, 304)
        Me.lbTrabajosASeleccionar.TabIndex = 8
        Me.lbTrabajosASeleccionar.ValueMember = "clv_trabajos"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(171, 46)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(94, 21)
        Me.DateTimePicker1.TabIndex = 16
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(328, 46)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(94, 21)
        Me.DateTimePicker2.TabIndex = 17
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(104, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(394, 15)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Seleccione el rango de Fechas de Solicitud de Orden/Queja "
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(293, 51)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(12, 15)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "-"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(516, 432)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(98, 31)
        Me.Button1.TabIndex = 20
        Me.Button1.Text = "Cancelar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'frmFiltroReporteResumen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(626, 475)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.DateTimePicker2)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btAceptar)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.btDeseleccionaUno)
        Me.Controls.Add(Me.btSeleccionaUno)
        Me.Controls.Add(Me.btSeleccionarTodos)
        Me.Controls.Add(Me.lbTrabajosSeleccionados)
        Me.Controls.Add(Me.lbTrabajosASeleccionar)
        Me.Name = "frmFiltroReporteResumen"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmFiltroReporteResumen"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btAceptar As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents btDeseleccionaUno As System.Windows.Forms.Button
    Friend WithEvents btSeleccionaUno As System.Windows.Forms.Button
    Friend WithEvents btSeleccionarTodos As System.Windows.Forms.Button
    Friend WithEvents lbTrabajosSeleccionados As System.Windows.Forms.ListBox
    Friend WithEvents lbTrabajosASeleccionar As System.Windows.Forms.ListBox
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
