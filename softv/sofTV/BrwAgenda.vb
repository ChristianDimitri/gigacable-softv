
Imports System.Data.SqlClient

Public Class BrwAgenda
    
    Private Sub BUSCA(ByVal OP As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            'If IsDate(Me.TextBox3.Text) = True Then
            'FECHA_TXT
            'End If
            GloClv_tecnico = Me.ComboBox4.SelectedValue
            If OP = 0 Then
                If IsNumeric(Me.TextBox1.Text) = True Then
                    Me.BUSCAAGENDATableAdapter.Connection = CON
                    Me.BUSCAAGENDATableAdapter.Fill(Me.DataSetLidia2.BUSCAAGENDA, New System.Nullable(Of Integer)(CType(GloClv_tecnico, Integer)), 0, Me.TextBox1.Text, "", "01/01/2001", "01/01/2001", 0)
                Else
                    MsgBox("La Busqueda no se puede realizar con datos invalidos", MsgBoxStyle.Information)
                End If
            ElseIf OP = 1 Then
                If Len(Trim(Me.TextBox2.Text)) > 0 Then
                    Me.BUSCAAGENDATableAdapter.Connection = CON
                    Me.BUSCAAGENDATableAdapter.Fill(Me.DataSetLidia2.BUSCAAGENDA, New System.Nullable(Of Integer)(CType(GloClv_tecnico, Integer)), 0, 0, Me.TextBox2.Text, "01/01/2001", "01/01/2001", 1)
                Else
                    MsgBox("La Busqueda no se puede realizar con datos invalidos", MsgBoxStyle.Information)
                End If
            ElseIf OP = 2 Then
                If IsDate(Me.TextBox3.Text) = True Then
                    Me.BUSCAAGENDATableAdapter.Connection = CON
                    Me.BUSCAAGENDATableAdapter.Fill(Me.DataSetLidia2.BUSCAAGENDA, New System.Nullable(Of Integer)(CType(GloClv_tecnico, Integer)), 0, 0, "", Me.TextBox3.Text, "01/01/2001", 2)
                Else
                    MsgBox("La Busqueda no se puede realizar con datos invalidos", MsgBoxStyle.Information)
                End If
            ElseIf OP = 3 Then
                If IsDate(Me.TextBox4.Text) = True Then
                    Me.BUSCAAGENDATableAdapter.Connection = CON
                    Me.BUSCAAGENDATableAdapter.Fill(Me.DataSetLidia2.BUSCAAGENDA, New System.Nullable(Of Integer)(CType(GloClv_tecnico, Integer)), 0, 0, "", "01/01/2001", "01/01/2001" & " " & Me.TextBox4.Text, 3)
                Else
                    MsgBox("La Busqueda no se puede realizar con datos invalidos", MsgBoxStyle.Information)
                End If
            ElseIf OP = 5 Then
                If IsNumeric(Me.TextBox5.Text) = True Then
                    Me.BUSCAAGENDATableAdapter.Connection = CON
                    Me.BUSCAAGENDATableAdapter.Fill(Me.DataSetLidia2.BUSCAAGENDA, New System.Nullable(Of Integer)(CType(GloClv_tecnico, Integer)), Me.TextBox5.Text, 0, "", "01/01/2001", "01/01/2001", 5)
                Else
                    MsgBox("La Busqueda no se puede realizar con datos invalidos", MsgBoxStyle.Information)
                End If
            Else
                Me.BUSCAAGENDATableAdapter.Connection = CON
                Me.BUSCAAGENDATableAdapter.Fill(Me.DataSetLidia2.BUSCAAGENDA, New System.Nullable(Of Integer)(CType(GloClv_tecnico, Integer)), 0, 0, "", "01/01/2001", "01/01/2001", 4)
            End If
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()
            Me.TextBox3.Value = Now
            Me.TextBox4.Clear()
            Me.TextBox5.Clear()
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub BrwAgenda_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            BUSCA(4)
        End If
    End Sub

    Private Sub BrwAgenda_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)

        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MUESTRATECNICOS' Puede moverla o quitarla seg�n sea necesario.
        ' Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        'Me.MUESTRATECNICOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATECNICOS)
        Me.Muestra_Tecnicos_AlmacenTableAdapter.Connection = CON
        Me.Muestra_Tecnicos_AlmacenTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Tecnicos_Almacen, 0)
        BUSCA(4)
        CON.Close()
        UspDesactivaBotones(Me, Me.Name)
    End Sub


    Private Sub FECHALabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FECHALabel1.Click

    End Sub

    Private Sub TextBox4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            BUSCA(3)
        End If
    End Sub

    Private Sub TextBox4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Label7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label7.Click

    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        BUSCA(4)
    End Sub

    Private Sub TextBox5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox5.KeyPress
        If Asc(e.KeyChar) = 13 Then
            BUSCA(5)
        End If
    End Sub

    Private Sub TextBox5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox5.TextChanged

    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            BUSCA(0)
        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            BUSCA(1)
        End If
    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged

    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            BUSCA(2)
        End If
    End Sub

    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        BUSCA(5)
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        BUSCA(0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        BUSCA(1)
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        BUSCA(2)
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        BUSCA(3)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub SplitContainer1_Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles SplitContainer1.Panel1.Paint

    End Sub

    Private Sub TextBox4_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            BUSCA(3)
        End If
    End Sub



    Private Sub MODIFICAR()
        If IsNumeric(gloClave) = True Then
            GloClv_tecnico = Me.ComboBox4.SelectedValue
            GLONOM_TECNICO = Me.ComboBox4.Text
            opcion = "M"
            FrmAgenda.Show()

        Else
            MsgBox(mensaje1)
        End If
    End Sub

    Private Sub NUEVO()
        GloClv_tecnico = Me.ComboBox4.SelectedValue
        GLONOM_TECNICO = Me.ComboBox4.Text
        opcion = "N"
        FrmAgenda.Show()

    End Sub

    Private Sub CONSULTAR()
        If IsNumeric(gloClave) = True Then
            GloClv_tecnico = Me.ComboBox4.SelectedValue
            GLONOM_TECNICO = Me.ComboBox4.Text
            opcion = "C"
            FrmAgenda.Show()

        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub Clv_calleLabel2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.Click

    End Sub

    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        If IsNumeric(Me.Clv_calleLabel2.Text) = True Then
            gloClave = Me.Clv_calleLabel2.Text
        End If
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            CONSULTAR()
        ElseIf Button4.Enabled = True Then
            MODIFICAR()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        NUEVO()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        CONSULTAR()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        MODIFICAR()
    End Sub

    Private Sub TextBox3_KeyPress2(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            BUSCA(2)
        End If
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        FrmAgendaTecnico.Show()
    End Sub


#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region

End Class