﻿Public Class FrmSelStatus

    Private Sub bnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bnAceptar.Click
        If cbContratado.Checked = False And cbInstalado.Checked = False And cbDesconectado.Checked = False And cbSuspendido.Checked = False And cbFueraDeArea.Checked = False And cbBaja.Checked = False Then
            MessageBox.Show("Selecciona al menos un Status.")
            Exit Sub
        End If

        If cbContratado.Checked = True Then NUESeleccionStatus(LocClv_session, "C")
        If cbInstalado.Checked = True Then NUESeleccionStatus(LocClv_session, "I")
        If cbDesconectado.Checked = True Then NUESeleccionStatus(LocClv_session, "D")
        If cbSuspendido.Checked = True Then NUESeleccionStatus(LocClv_session, "S")
        If cbFueraDeArea.Checked = True Then NUESeleccionStatus(LocClv_session, "F")
        If cbBaja.Checked = True Then NUESeleccionStatus(LocClv_session, "B")

        'bndReportC = True
        '
        FrmRepCortesia.Show()
        Me.Close()
    End Sub

    Private Sub NUESeleccionStatus(ByVal Clv_Session As Integer, ByVal Status As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, CObj(Clv_Session))
        BaseII.CreateMyParameter("@Status", SqlDbType.VarChar, CObj(Status), 1)
        BaseII.Inserta("NUESeleccionStatus")
    End Sub

    Private Sub bnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles bnCancelar.Click
        bndReportC = False
        Me.Close()
    End Sub

    Private Sub FrmSelStatus_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
    End Sub
End Class