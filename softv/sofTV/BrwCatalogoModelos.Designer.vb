﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwCatalogoModelos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim nombreModeloLbl As System.Windows.Forms.Label
        Dim idMarcaLbl As System.Windows.Forms.Label
        Dim nombreMarcaLbl As System.Windows.Forms.Label
        Dim elNombrePrecioLbl As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.elPrecioLbl = New System.Windows.Forms.Label()
        Me.laMarcaLbl = New System.Windows.Forms.Label()
        Me.laUltimoClienteAsignado = New System.Windows.Forms.Label()
        Me.laUltimaFechaLiberacion = New System.Windows.Forms.Label()
        Me.laStatus = New System.Windows.Forms.Label()
        Me.elIdModeloLbl = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.elModeloLbl = New System.Windows.Forms.Label()
        Me.modeloTxt = New System.Windows.Forms.TextBox()
        Me.idModeloTxt = New System.Windows.Forms.TextBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.gridModelos = New System.Windows.Forms.DataGridView()
        Me.IdModelo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Modelo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdMarca = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Marca = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Precio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bSalir = New System.Windows.Forms.Button()
        Me.bModificar = New System.Windows.Forms.Button()
        Me.bConsultar = New System.Windows.Forms.Button()
        Me.bNuevo = New System.Windows.Forms.Button()
        Me.btnBuscarPorNombre = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.marcaCombo = New System.Windows.Forms.ComboBox()
        Me.MarcaBtn = New System.Windows.Forms.Button()
        Me.MarcaLbl = New System.Windows.Forms.Label()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        nombreModeloLbl = New System.Windows.Forms.Label()
        idMarcaLbl = New System.Windows.Forms.Label()
        nombreMarcaLbl = New System.Windows.Forms.Label()
        elNombrePrecioLbl = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.gridModelos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'nombreModeloLbl
        '
        nombreModeloLbl.AutoSize = True
        nombreModeloLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        nombreModeloLbl.ForeColor = System.Drawing.Color.White
        nombreModeloLbl.Location = New System.Drawing.Point(15, 59)
        nombreModeloLbl.Name = "nombreModeloLbl"
        nombreModeloLbl.Size = New System.Drawing.Size(59, 15)
        nombreModeloLbl.TabIndex = 3
        nombreModeloLbl.Text = "Modelo:"
        '
        'idMarcaLbl
        '
        idMarcaLbl.AutoSize = True
        idMarcaLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        idMarcaLbl.ForeColor = System.Drawing.Color.White
        idMarcaLbl.Location = New System.Drawing.Point(15, 30)
        idMarcaLbl.Name = "idMarcaLbl"
        idMarcaLbl.Size = New System.Drawing.Size(27, 15)
        idMarcaLbl.TabIndex = 1
        idMarcaLbl.Text = "Id :"
        '
        'nombreMarcaLbl
        '
        nombreMarcaLbl.AutoSize = True
        nombreMarcaLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        nombreMarcaLbl.ForeColor = System.Drawing.Color.White
        nombreMarcaLbl.Location = New System.Drawing.Point(15, 106)
        nombreMarcaLbl.Name = "nombreMarcaLbl"
        nombreMarcaLbl.Size = New System.Drawing.Size(51, 15)
        nombreMarcaLbl.TabIndex = 29
        nombreMarcaLbl.Text = "Marca:"
        '
        'elNombrePrecioLbl
        '
        elNombrePrecioLbl.AutoSize = True
        elNombrePrecioLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        elNombrePrecioLbl.ForeColor = System.Drawing.Color.White
        elNombrePrecioLbl.Location = New System.Drawing.Point(15, 154)
        elNombrePrecioLbl.Name = "elNombrePrecioLbl"
        elNombrePrecioLbl.Size = New System.Drawing.Size(52, 15)
        elNombrePrecioLbl.TabIndex = 33
        elNombrePrecioLbl.Text = "Precio:"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(elNombrePrecioLbl)
        Me.Panel1.Controls.Add(Me.elPrecioLbl)
        Me.Panel1.Controls.Add(nombreMarcaLbl)
        Me.Panel1.Controls.Add(Me.laMarcaLbl)
        Me.Panel1.Controls.Add(Me.laUltimoClienteAsignado)
        Me.Panel1.Controls.Add(Me.laUltimaFechaLiberacion)
        Me.Panel1.Controls.Add(Me.laStatus)
        Me.Panel1.Controls.Add(Me.elIdModeloLbl)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(nombreModeloLbl)
        Me.Panel1.Controls.Add(Me.elModeloLbl)
        Me.Panel1.Controls.Add(idMarcaLbl)
        Me.Panel1.Location = New System.Drawing.Point(23, 244)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(239, 220)
        Me.Panel1.TabIndex = 16
        '
        'elPrecioLbl
        '
        Me.elPrecioLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elPrecioLbl.Location = New System.Drawing.Point(73, 155)
        Me.elPrecioLbl.Name = "elPrecioLbl"
        Me.elPrecioLbl.Size = New System.Drawing.Size(148, 20)
        Me.elPrecioLbl.TabIndex = 32
        '
        'laMarcaLbl
        '
        Me.laMarcaLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.laMarcaLbl.Location = New System.Drawing.Point(21, 126)
        Me.laMarcaLbl.Name = "laMarcaLbl"
        Me.laMarcaLbl.Size = New System.Drawing.Size(210, 20)
        Me.laMarcaLbl.TabIndex = 30
        '
        'laUltimoClienteAsignado
        '
        Me.laUltimoClienteAsignado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.laUltimoClienteAsignado.Location = New System.Drawing.Point(27, 265)
        Me.laUltimoClienteAsignado.Name = "laUltimoClienteAsignado"
        Me.laUltimoClienteAsignado.Size = New System.Drawing.Size(202, 23)
        Me.laUltimoClienteAsignado.TabIndex = 28
        '
        'laUltimaFechaLiberacion
        '
        Me.laUltimaFechaLiberacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.laUltimaFechaLiberacion.Location = New System.Drawing.Point(27, 217)
        Me.laUltimaFechaLiberacion.Name = "laUltimaFechaLiberacion"
        Me.laUltimaFechaLiberacion.Size = New System.Drawing.Size(202, 23)
        Me.laUltimaFechaLiberacion.TabIndex = 26
        '
        'laStatus
        '
        Me.laStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.laStatus.Location = New System.Drawing.Point(24, 121)
        Me.laStatus.Name = "laStatus"
        Me.laStatus.Size = New System.Drawing.Size(148, 23)
        Me.laStatus.TabIndex = 20
        '
        'elIdModeloLbl
        '
        Me.elIdModeloLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elIdModeloLbl.Location = New System.Drawing.Point(74, 32)
        Me.elIdModeloLbl.Name = "elIdModeloLbl"
        Me.elIdModeloLbl.Size = New System.Drawing.Size(100, 23)
        Me.elIdModeloLbl.TabIndex = 20
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(9, 7)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(141, 18)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Datos del Modelo"
        '
        'elModeloLbl
        '
        Me.elModeloLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elModeloLbl.Location = New System.Drawing.Point(21, 79)
        Me.elModeloLbl.Name = "elModeloLbl"
        Me.elModeloLbl.Size = New System.Drawing.Size(210, 20)
        Me.elModeloLbl.TabIndex = 22
        '
        'modeloTxt
        '
        Me.modeloTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.modeloTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.modeloTxt.Location = New System.Drawing.Point(23, 182)
        Me.modeloTxt.Name = "modeloTxt"
        Me.modeloTxt.Size = New System.Drawing.Size(239, 21)
        Me.modeloTxt.TabIndex = 14
        '
        'idModeloTxt
        '
        Me.idModeloTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.idModeloTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.idModeloTxt.Location = New System.Drawing.Point(23, 135)
        Me.idModeloTxt.Name = "idModeloTxt"
        Me.idModeloTxt.Size = New System.Drawing.Size(88, 21)
        Me.idModeloTxt.TabIndex = 11
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(19, 10)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(164, 20)
        Me.CMBLabel1.TabIndex = 9
        Me.CMBLabel1.Text = "Buscar Modelo por:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(20, 117)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(75, 15)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Id Modelo:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(20, 164)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 15)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "Modelo:"
        '
        'gridModelos
        '
        Me.gridModelos.AllowUserToAddRows = False
        Me.gridModelos.AllowUserToDeleteRows = False
        Me.gridModelos.AllowUserToResizeRows = False
        Me.gridModelos.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gridModelos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.gridModelos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridModelos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdModelo, Me.Modelo, Me.IdMarca, Me.Marca, Me.Precio})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gridModelos.DefaultCellStyle = DataGridViewCellStyle2
        Me.gridModelos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gridModelos.Location = New System.Drawing.Point(0, 0)
        Me.gridModelos.Name = "gridModelos"
        Me.gridModelos.ReadOnly = True
        Me.gridModelos.RowHeadersVisible = False
        Me.gridModelos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.gridModelos.Size = New System.Drawing.Size(572, 533)
        Me.gridModelos.TabIndex = 17
        Me.gridModelos.TabStop = False
        '
        'IdModelo
        '
        Me.IdModelo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.IdModelo.FillWeight = 40.0!
        Me.IdModelo.HeaderText = "Id"
        Me.IdModelo.MinimumWidth = 40
        Me.IdModelo.Name = "IdModelo"
        Me.IdModelo.ReadOnly = True
        '
        'Modelo
        '
        Me.Modelo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Modelo.HeaderText = "Modelo"
        Me.Modelo.MinimumWidth = 100
        Me.Modelo.Name = "Modelo"
        Me.Modelo.ReadOnly = True
        '
        'IdMarca
        '
        Me.IdMarca.FillWeight = 10.0!
        Me.IdMarca.HeaderText = "IdMarca"
        Me.IdMarca.MinimumWidth = 10
        Me.IdMarca.Name = "IdMarca"
        Me.IdMarca.ReadOnly = True
        Me.IdMarca.Visible = False
        Me.IdMarca.Width = 10
        '
        'Marca
        '
        Me.Marca.HeaderText = "Marca"
        Me.Marca.MinimumWidth = 100
        Me.Marca.Name = "Marca"
        Me.Marca.ReadOnly = True
        '
        'Precio
        '
        Me.Precio.HeaderText = "Precio"
        Me.Precio.MinimumWidth = 100
        Me.Precio.Name = "Precio"
        Me.Precio.ReadOnly = True
        '
        'bSalir
        '
        Me.bSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.bSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bSalir.ForeColor = System.Drawing.Color.Black
        Me.bSalir.Location = New System.Drawing.Point(877, 499)
        Me.bSalir.Name = "bSalir"
        Me.bSalir.Size = New System.Drawing.Size(136, 36)
        Me.bSalir.TabIndex = 23
        Me.bSalir.Text = "&SALIR"
        Me.bSalir.UseVisualStyleBackColor = False
        '
        'bModificar
        '
        Me.bModificar.BackColor = System.Drawing.Color.Orange
        Me.bModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bModificar.ForeColor = System.Drawing.Color.Black
        Me.bModificar.Location = New System.Drawing.Point(877, 95)
        Me.bModificar.Name = "bModificar"
        Me.bModificar.Size = New System.Drawing.Size(136, 36)
        Me.bModificar.TabIndex = 26
        Me.bModificar.Text = "&MODIFICAR"
        Me.bModificar.UseVisualStyleBackColor = False
        '
        'bConsultar
        '
        Me.bConsultar.BackColor = System.Drawing.Color.Orange
        Me.bConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bConsultar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bConsultar.ForeColor = System.Drawing.Color.Black
        Me.bConsultar.Location = New System.Drawing.Point(877, 53)
        Me.bConsultar.Name = "bConsultar"
        Me.bConsultar.Size = New System.Drawing.Size(136, 36)
        Me.bConsultar.TabIndex = 25
        Me.bConsultar.Text = "&CONSULTA"
        Me.bConsultar.UseVisualStyleBackColor = False
        '
        'bNuevo
        '
        Me.bNuevo.BackColor = System.Drawing.Color.Orange
        Me.bNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bNuevo.ForeColor = System.Drawing.Color.Black
        Me.bNuevo.Location = New System.Drawing.Point(877, 11)
        Me.bNuevo.Name = "bNuevo"
        Me.bNuevo.Size = New System.Drawing.Size(136, 36)
        Me.bNuevo.TabIndex = 24
        Me.bNuevo.Text = "&NUEVO"
        Me.bNuevo.UseVisualStyleBackColor = False
        '
        'btnBuscarPorNombre
        '
        Me.btnBuscarPorNombre.BackColor = System.Drawing.Color.DarkOrange
        Me.btnBuscarPorNombre.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscarPorNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscarPorNombre.ForeColor = System.Drawing.Color.Black
        Me.btnBuscarPorNombre.Location = New System.Drawing.Point(23, 210)
        Me.btnBuscarPorNombre.Name = "btnBuscarPorNombre"
        Me.btnBuscarPorNombre.Size = New System.Drawing.Size(88, 23)
        Me.btnBuscarPorNombre.TabIndex = 46
        Me.btnBuscarPorNombre.Text = "&Buscar"
        Me.btnBuscarPorNombre.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(117, 135)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(88, 23)
        Me.Button1.TabIndex = 45
        Me.Button1.Text = "&Buscar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'marcaCombo
        '
        Me.marcaCombo.DisplayMember = "Marca"
        Me.marcaCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.marcaCombo.FormattingEnabled = True
        Me.marcaCombo.Location = New System.Drawing.Point(23, 61)
        Me.marcaCombo.Name = "marcaCombo"
        Me.marcaCombo.Size = New System.Drawing.Size(243, 21)
        Me.marcaCombo.TabIndex = 49
        Me.marcaCombo.ValueMember = "IdMarca"
        '
        'MarcaBtn
        '
        Me.MarcaBtn.BackColor = System.Drawing.Color.DarkOrange
        Me.MarcaBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MarcaBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MarcaBtn.ForeColor = System.Drawing.Color.Black
        Me.MarcaBtn.Location = New System.Drawing.Point(23, 88)
        Me.MarcaBtn.Name = "MarcaBtn"
        Me.MarcaBtn.Size = New System.Drawing.Size(88, 23)
        Me.MarcaBtn.TabIndex = 48
        Me.MarcaBtn.Text = "&Buscar"
        Me.MarcaBtn.UseVisualStyleBackColor = False
        '
        'MarcaLbl
        '
        Me.MarcaLbl.AutoSize = True
        Me.MarcaLbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MarcaLbl.Location = New System.Drawing.Point(20, 43)
        Me.MarcaLbl.Name = "MarcaLbl"
        Me.MarcaLbl.Size = New System.Drawing.Size(51, 15)
        Me.MarcaLbl.TabIndex = 47
        Me.MarcaLbl.Text = "Marca:"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(2, 2)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.marcaCombo)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.MarcaBtn)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.MarcaLbl)
        Me.SplitContainer1.Panel1.Controls.Add(Me.idModeloTxt)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnBuscarPorNombre)
        Me.SplitContainer1.Panel1.Controls.Add(Me.modeloTxt)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.gridModelos)
        Me.SplitContainer1.Size = New System.Drawing.Size(862, 533)
        Me.SplitContainer1.SplitterDistance = 286
        Me.SplitContainer1.TabIndex = 50
        Me.SplitContainer1.TabStop = False
        '
        'BrwCatalogoModelos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(1034, 553)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.bModificar)
        Me.Controls.Add(Me.bConsultar)
        Me.Controls.Add(Me.bNuevo)
        Me.Controls.Add(Me.bSalir)
        Me.Name = "BrwCatalogoModelos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catalogo Modelos"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.gridModelos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents laUltimoClienteAsignado As System.Windows.Forms.Label
    Friend WithEvents laUltimaFechaLiberacion As System.Windows.Forms.Label
    Friend WithEvents laStatus As System.Windows.Forms.Label
    Friend WithEvents elIdModeloLbl As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents elModeloLbl As System.Windows.Forms.Label
    Friend WithEvents modeloTxt As System.Windows.Forms.TextBox
    Friend WithEvents idModeloTxt As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents gridModelos As System.Windows.Forms.DataGridView
    Friend WithEvents bSalir As System.Windows.Forms.Button
    Friend WithEvents bModificar As System.Windows.Forms.Button
    Friend WithEvents bConsultar As System.Windows.Forms.Button
    Friend WithEvents bNuevo As System.Windows.Forms.Button
    Friend WithEvents btnBuscarPorNombre As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents laMarcaLbl As System.Windows.Forms.Label
    Friend WithEvents marcaCombo As System.Windows.Forms.ComboBox
    Friend WithEvents MarcaBtn As System.Windows.Forms.Button
    Friend WithEvents MarcaLbl As System.Windows.Forms.Label
    Friend WithEvents elPrecioLbl As System.Windows.Forms.Label
    Friend WithEvents IdModelo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Modelo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdMarca As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Marca As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Precio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
End Class
