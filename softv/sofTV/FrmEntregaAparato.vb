﻿Imports System.Data.SqlClient
Public Class FrmEntregaAparato



    Private Sub ValidaCuantosAparatosEntrega(ByVal Clv_Orden As Long, ByVal NumDig As Integer, ByVal NumNet As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaCuantosAparatosEntrega", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Orden
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@NumDig", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = NumDig
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@NumNet", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = NumNet
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Res", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro5.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro5)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = 0
            eMsj = String.Empty
            eRes = parametro4.Value
            eMsj = parametro5.Value



        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub NueDetOrdSerEntrega(ByVal Clv_Orden As Long, ByVal Clv_TipSer As Integer, ByVal NumAparatos As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueDetOrdSerEntrega", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Orden
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_TipSer
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@NumAparatos", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = NumAparatos
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub



    Private Sub FrmEntregaAparato_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If eBndDig = False Then
            lblDig.Enabled = False
            txtDig.Enabled = False
        End If
        If eBndNet = False Then
            lblNet.Enabled = False
            txtNet.Enabled = False
        End If
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If IsNumeric(txtDig.Text) = False Then
            MsgBox("Ingresa un valor correcto.", MsgBoxStyle.Exclamation)
            txtDig.Text = 0
        End If
        If IsNumeric(txtNet.Text) = False Then
            MsgBox("Ingresa un valor correcto.", MsgBoxStyle.Exclamation)
            txtNet.Text = 0
        End If
        ValidaCuantosAparatosEntrega(gloClave, txtDig.Text, txtNet.Text)
        If eRes = 1 Then
            MsgBox(eMsj, MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If eBndDig = True Then NueDetOrdSerEntrega(gloClave, 3, txtDig.Text)
        If eBndNet = True Then NueDetOrdSerEntrega(gloClave, 2, txtNet.Text)

        eBndEntregaAparato = True
        Me.Close()
    End Sub

    Private Sub bntCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bntCancelar.Click
        eBndEntregaAparato = False
        Me.Close()
    End Sub
End Class