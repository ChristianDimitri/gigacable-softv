Public Class FrmTelmexDCInstalaServicios

    Dim InstalaraServicios As Boolean
    Dim contratoTelmex As Long
    Dim bnd_OrdenTelmexEjecutada As Boolean

    Private Sub FrmTelmexDCInstalaServicios_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed

    End Sub



    Private Sub FrmTelmexDCInstalaServicios_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name) 'ColoreaFrm(Me)

        inicializarCombos()
        LlenarCampos()

        Button2.Visible = True

        dimeSiOrdenTelmexEjecutada()

        If opcion = "C" Then ' If OpcionCli = "C" Then

            ObservacionesTxt.Enabled = False
            ProveedorPlataformaTxt.Enabled = False
            TipoEntregaModemCmb.Enabled = False
            MarcaModemCmb.Enabled = False
            ModeloModemCmb.Enabled = False
            SerieModemTxt.Enabled = False
            TextBox1.Enabled = False
            Button2.Visible = False

        ElseIf opcion = "M" Or opcion = "N" Then 'OpcionCli = "M" Or OpcionCli = "N" Then

            ObservacionesTxt.Enabled = True
            ProveedorPlataformaTxt.Enabled = True
            TipoEntregaModemCmb.Enabled = True
            MarcaModemCmb.Enabled = True
            ModeloModemCmb.Enabled = True
            SerieModemTxt.Enabled = True
            TextBox1.Enabled = True

        End If


        'If modificaOrden_Telmex = True Then
        '    Button2.Visible = True
        'Else
        '    Button2.Visible = False
        'End If


    End Sub

    Private Sub dimeSiOrdenTelmexEjecutada()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, gloClv_Orden)
        BaseII.CreateMyParameter("@bndSi", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("DimeSi_OrdenEstaEjecutada_Telmex")
        bnd_OrdenTelmexEjecutada = BaseII.dicoPar("@bndSi")

        If bnd_OrdenTelmexEjecutada = True Then 'No permitir que la orden se edite cuando ya fue ejecutada 
            Button2.Visible = False
        Else
            Button2.Visible = True
        End If

    End Sub

    Private Sub inicializarCombos()


        BaseII.limpiaParametros()
        TipoEntregaModemCmb.DataSource = BaseII.ConsultaDT("GetTipoEntregaModem")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdMarca", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, "")
        BaseII.CreateMyParameter("@Opcion", SqlDbType.Int, 0) 'Todas las marcas
        MarcaModemCmb.DataSource = BaseII.ConsultaDT("FiltroMarcas")

        BaseII.limpiaParametros()
        ModeloModemCmb.DataSource = BaseII.ConsultaDT("Dame_Modelos")

    End Sub

    Private Sub LlenarCampos()

        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, loccontratoordenes) 'Este contrato tiene servicio telmex
        Dim resultado As Integer
        Dim dt As DataTable
        dt = BaseII.ConsultaDT("DameDatos_ClienteTelmex_ByContrato")
        Dim dr As DataRow
        For Each dr In dt.Rows

            contratoTelmex = dr("ContratoTelmex")
            '----------------------------------
            ObservacionesTxt.Text = dr("Observaciones").ToString()
            TextBox1.Text = dr("SKU").ToString()

            ProveedorPlataformaTxt.Text = dr("Proveedor")
            TipoEntregaModemCmb.SelectedValue = dr("IdTipoEntrega")


            If CInt(dr("IdMarca")) > 0 Then ' Si existe idMarca > 0, Seleccionamos la marca por id
                MarcaModemCmb.SelectedValue = dr("IdMarca")
            End If

            If CInt(dr("IdModelo")) > 0 Then ' Si existe idModelo > 0, Seleccionamos el modelo por id

                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@IdModelo", SqlDbType.BigInt, dr("IdModelo"))
                BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, "")
                BaseII.CreateMyParameter("@IdMarca", SqlDbType.BigInt, 0)
                BaseII.CreateMyParameter("@Opcion", SqlDbType.Int, 2) 'opc 1: por idModelo

                ModeloModemCmb.DataSource = BaseII.ConsultaDT("FiltroModelos")
            End If


            ModeloModemCmb.SelectedValue = dr("IdModelo") 'Modelo
            SerieModemTxt.Text = dr("Serie").ToString()

        Next


    End Sub





    Private Sub Button5_Click(sender As Object, e As EventArgs)
        Me.Close()
    End Sub




    Private Sub HoraTxt_KeyPress(sender As Object, e As KeyPressEventArgs)

        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If

    End Sub






    Private Sub MarcaModemCmb_SelectedIndexChanged(sender As Object, e As EventArgs) Handles MarcaModemCmb.SelectedIndexChanged

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdModelo", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, "")
        BaseII.CreateMyParameter("@IdMarca", SqlDbType.BigInt, MarcaModemCmb.SelectedValue)
        BaseII.CreateMyParameter("@Opcion", SqlDbType.Int, 1) 'opc 1: por idMarca
        ModeloModemCmb.DataSource = BaseII.ConsultaDT("FiltroModelos")

    End Sub


    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        bndActualizaOrden_Telmex = False
        Me.Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try

            If TipoEntregaModemCmb.SelectedIndex = -1 Then  '   TipoEntregaModemCmb.SelectedValue = dr("IdTipoEntrega")
                MsgBox("Seleccione un tipo de entrega del m�dem")
                Exit Sub
            End If


            If ModeloModemCmb.SelectedIndex = -1 Or ModeloModemCmb.SelectedValue = 0 Then  '   ModeloModemCmb.SelectedValue = dr("Modelo")
                MsgBox("Seleccione un modelo")
                Exit Sub
            End If


            If SerieModemTxt.Text = "" Then '   SerieModemTxt.Text = dr("Serie").ToString()
                MsgBox("Escriba la serie del m�dem")
                Exit Sub
            End If


            If TextBox1.Text = "" Then '    SkuTxt.Text = dr("SKU").ToString()
                MsgBox("Escriba el SKU")
                Exit Sub
            End If


            If ProveedorPlataformaTxt.Text = "" Then '        ProveedorPlataformaTxt.Text = dr("Proveedor")
                MsgBox("Escriba la plataforma")
                Exit Sub
            End If



            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, loccontratoordenes)
            BaseII.CreateMyParameter("@ContratoTelmex", SqlDbType.Int, contratoTelmex)
            BaseII.CreateMyParameter("@Proveedor", SqlDbType.VarChar, ProveedorPlataformaTxt.Text)
            BaseII.CreateMyParameter("@IdTipoEntrega", SqlDbType.BigInt, TipoEntregaModemCmb.SelectedValue) '1

            BaseII.CreateMyParameter("@IdModelo", SqlDbType.BigInt, CInt(ModeloModemCmb.SelectedValue))
            BaseII.CreateMyParameter("@Observaciones", SqlDbType.VarChar, ObservacionesTxt.Text)
            BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, SerieModemTxt.Text)
            BaseII.CreateMyParameter("@ClvOrden", SqlDbType.BigInt, gloClv_Orden)
            BaseII.CreateMyParameter("@SKU", SqlDbType.VarChar, TextBox1.Text)

            BaseII.Inserta("UpdateClientesTelTelmexOrdSer")
            bndActualizaOrden_Telmex = True
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Me.Close()

    End Sub

    '15 06 2019

End Class