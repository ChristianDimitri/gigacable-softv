
Imports System.Data.SqlClient
Public Class FrmSelPeriodo2

    Private Correo As New FrmSelCorreo

    Private Sub FrmSelPeriodo2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta l�nea de c�digo carga datos en la tabla 'ProcedimientosArnoldo2.Muestra_Meses' Puede moverla o quitarla seg�n sea necesario.
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Muestra_MesesTableAdapter.Connection = CON
        Me.Muestra_MesesTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_Meses)
        CON.Close()
        colorea(Me, Me.Name)
        LocPeriodo1 = False
        LocPeriodo2 = False
        If LocValidaHab = 1 Then
            Me.CheckBox10.Checked = True
            Me.GroupBox1.Enabled = False
        End If
    End Sub

    Private Sub CheckBox10_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox10.CheckedChanged
        If Me.CheckBox10.CheckState = CheckState.Checked Then
            LocPeriodo1 = True
        Else
            LocPeriodo1 = False
        End If
    End Sub

    Private Sub CheckBox11_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox11.CheckedChanged
        If Me.CheckBox11.CheckState = CheckState.Checked Then
            LocPeriodo2 = True
        Else
            LocPeriodo2 = False
        End If
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Pasa()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub




    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox3, Asc(LCase(e.KeyChar)), "N")))
        If e.KeyChar = Chr(13) Then
            Pasa()
        End If
    End Sub

    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox3.TextChanged
    End Sub
    Private Sub Pasa()
        If LocValidaHab = 0 Then
            If Me.CheckBox10.CheckState = CheckState.Unchecked And Me.CheckBox11.CheckState = CheckState.Unchecked Then
                MsgBox("Seleccione al menos un Periodo", MsgBoxStyle.Information)
                Exit Sub
            End If

            If Me.TextBox3.Text = "" Then
                MsgBox("Teclee un a�o", MsgBoxStyle.Information)
                Exit Sub
            Else
                Locultimo_anio = Me.TextBox3.Text
            End If
            Locultimo_mes = Me.ComboBox1.SelectedValue
        ElseIf LocValidaHab = 1 Then

            If Me.TextBox3.Text = "" Then
                MsgBox("Teclee Un a�o", MsgBoxStyle.Information)
                Exit Sub
            Else
                Locultimo_anio = Me.TextBox3.Text
            End If
            Locultimo_mes = Me.ComboBox1.SelectedValue

        End If
        If LocOp = 4 Then
            Me.Close()
            Correo.ShowDialog()
            If Correo.DialogResult = Windows.Forms.DialogResult.OK Then
                GloCorreo = Correo.Op
            Else
                Exit Sub
            End If
        End If
        bndReport = True
        Me.Close()
    End Sub


    Private Sub CMBLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBLabel1.Click

    End Sub
End Class