﻿Imports System.Text
Imports System.Data.SqlClient

Public Class BrwSucursalesCFD

    Private Sub MuestraDatosFiscalesCFDSucursales(ByVal Op As Integer, ByVal Serie As String, ByVal Calle As String, ByVal Colonia As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC MuestraDatosFiscalesCFDSucursales " + Op.ToString() + ", '" + Serie + "', '" + Calle + "', '" + Colonia + "'")
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable
        Dim bSource As New BindingSource

        Try
            dAdapter.Fill(dTable)
            bSource.DataSource = dTable
            dgwSerie.DataSource = bSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub tbSerieBus_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tbSerieBus.KeyDown
        If e.KeyValue <> 13 Or tbSerieBus.Text.Length = 0 Then
            Exit Sub
        End If
        MuestraDatosFiscalesCFDSucursales(1, tbSerieBus.Text, "", "")
    End Sub

    Private Sub bnSerieBus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSerieBus.Click
        If tbSerieBus.Text.Length = 0 Then
            Exit Sub
        End If
        MuestraDatosFiscalesCFDSucursales(1, tbSerieBus.Text, "", "")
    End Sub

    Private Sub tbCalleBus_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tbCalleBus.KeyDown
        If e.KeyValue <> 13 Or tbCalleBus.Text.Length = 0 Then
            Exit Sub
        End If
        MuestraDatosFiscalesCFDSucursales(2, "", tbCalleBus.Text, "")
    End Sub

    Private Sub bnCalleBus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnCalleBus.Click
        If tbCalleBus.Text.Length = 0 Then
            Exit Sub
        End If
        MuestraDatosFiscalesCFDSucursales(2, "", tbCalleBus.Text, "")
    End Sub

    Private Sub tbColoniaBus_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tbColoniaBus.KeyDown
        If e.KeyValue <> 13 Or tbColoniaBus.Text.Length = 0 Then
            Exit Sub
        End If
        MuestraDatosFiscalesCFDSucursales(3, "", "", tbColoniaBus.Text)
    End Sub

    Private Sub bnColoniaBus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnColoniaBus.Click
        If tbColoniaBus.Text.Length = 0 Then
            Exit Sub
        End If
        MuestraDatosFiscalesCFDSucursales(3, "", "", tbColoniaBus.Text)
    End Sub

    Private Sub bnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnNuevo.Click
        eOpcion = "N"
        eRefrescar = False
        FrmSucursalesCFD.Show()
    End Sub

    Private Sub bnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnConsultar.Click
        If dgwSerie.Rows.Count = 0 Then
            Exit Sub
        End If
        eId = dgwSerie.SelectedCells(0).Value
        eOpcion = "C"
        eRefrescar = False
        FrmSucursalesCFD.Show()
    End Sub

    Private Sub bnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnModificar.Click
        If dgwSerie.Rows.Count = 0 Then
            Exit Sub
        End If
        eId = dgwSerie.SelectedCells(0).Value
        eOpcion = "M"
        eRefrescar = False
        FrmSucursalesCFD.Show()
    End Sub

    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub BrwSucursalesCFD_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        If eRefrescar = True Then
            eRefrescar = False
            MuestraDatosFiscalesCFDSucursales(0, "", "", "")
        End If
    End Sub

    Private Sub BrwSucursalesCFD_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        MuestraDatosFiscalesCFDSucursales(0, "", "", "")

        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub


#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region
End Class