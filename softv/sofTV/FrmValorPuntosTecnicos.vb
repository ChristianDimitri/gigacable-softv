﻿Public Class FrmValorPuntosTecnicos

    Dim LocTipo As String
    Dim LocClvPuntos As Integer
    Dim BANDERA As Integer

#Region "Eventos"
    Private Sub FrmValorPuntosTecnicos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        LocTipo = ""
        LocClvPuntos = 0
        Me.Panel1.BackColor = Me.BackColor
        Me.gbDatosRangoHorario.BackColor = Me.BackColor
        Me.Label1.BackColor = Me.BackColor
        Me.Label2.BackColor = Me.BackColor
        Me.Label3.BackColor = Me.BackColor
        Me.Label4.BackColor = Me.BackColor
        Me.Label5.BackColor = Me.BackColor
        Me.Label6.BackColor = Me.BackColor

        UspLlenaValorPuntos(0)

        If gbDatosRangoHorario.Visible = True Then
            Me.gbDatosRangoHorario.Visible = False
        End If
        UspLlenaComboRangoPuntos()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub txtPuntoInicial_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPuntoInicial.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtPuntoFinal_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPuntoFinal.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtValor_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtValor.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub
#End Region

#Region "Funciones"
    Private Sub UspLlenaValorPuntos(ByVal PRMCLAVE As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLAVE", SqlDbType.Int, PRMCLAVE)
            dgValorPuntos.DataSource = BaseII.ConsultaDT("UspLlenaValorPuntos")
        Catch ex As Exception

        End Try
    End Sub

    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890.", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function

    Private Sub UspGuardarRangoPuntos(ByVal PRMCLAVE As Integer)
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLAVE", SqlDbType.Int, PRMCLAVE)
            BaseII.CreateMyParameter("@PUNTOINICIAL", SqlDbType.Decimal, CDec(Me.txtPuntoInicial.Text))
            BaseII.CreateMyParameter("@PUNTOFINAL", SqlDbType.Decimal, CDec(Me.txtPuntoFinal.Text))
            BaseII.CreateMyParameter("@VALOR", SqlDbType.Decimal, CDec(Me.txtValor.Text))
            DT = BaseII.ConsultaDT("UspGuardarRangoPuntos")

            If DT.Rows.Count > 0 Then
                BANDERA = CInt(DT.Rows(0)(0).ToString)
                MsgBox(DT.Rows(0)(1).ToString, MsgBoxStyle.Information, "Valor de puntos de técnicos")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspLlenaComboRangoPuntos()
        Try
            BaseII.limpiaParametros()
            cbRangoPuntos.DataSource = BaseII.ConsultaDT("UspLlenaComboRangoPuntos")
        Catch ex As Exception

        End Try
    End Sub
#End Region


#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Try


    
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

            Next
        Catch ex As Exception

        End Try
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region


#Region "Salir"
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
#End Region

#Region "Buscar"
    Private Sub btnBuscarRango_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarRango.Click
        UspLlenaValorPuntos(CInt(Me.cbRangoPuntos.SelectedValue))
    End Sub
#End Region

#Region "Agregar"
    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        Me.txtPuntoInicial.Text = ""
        Me.txtPuntoFinal.Text = ""
        Me.txtValor.Text = ""
        Me.gbDatosRangoHorario.Visible = True
        LocTipo = "N"
    End Sub
#End Region

#Region "Modificar"
    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        If Me.dgValorPuntos.SelectedCells.Count > 0 Then
            LocClvPuntos = CInt(Me.dgValorPuntos.SelectedCells(0).Value)
            Me.gbDatosRangoHorario.Visible = True
            txtPuntoInicial.Text = Me.dgValorPuntos.SelectedCells(1).Value.ToString
            txtPuntoFinal.Text = Me.dgValorPuntos.SelectedCells(2).Value.ToString
            txtValor.Text = Me.dgValorPuntos.SelectedCells(3).Value.ToString
            LocTipo = "M"
        Else
            MsgBox("Seleccione un Rango", MsgBoxStyle.Information, "Rango de Horarios")
        End If
    End Sub
#End Region

#Region "Guardar"
    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        BANDERA = 0
        If CInt(txtPuntoInicial.Text) > CInt(txtPuntoFinal.Text) Then
            MsgBox("El punto inicial debe ser menor al punto final", MsgBoxStyle.Information, "Rango de Horarios")
            Exit Sub
        End If

        If LocTipo = "N" Then
            UspGuardarRangoPuntos(0)
            If BANDERA = 1 Then
                Exit Sub
            End If
        ElseIf LocTipo = "M" Then
            UspGuardarRangoPuntos(LocClvPuntos)
            If BANDERA = 1 Then
                Exit Sub
            End If
        End If
        Me.gbDatosRangoHorario.Visible = False
        UspLlenaValorPuntos(0)
        UspLlenaComboRangoPuntos()
    End Sub
#End Region

#Region "Cancelar"
    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.gbDatosRangoHorario.Visible = False
    End Sub
#End Region

End Class