﻿Public Class frmRangoHorario
    Dim LocTipo As String
    Dim LocClvHorario As Integer
    Dim BANDERA As Integer

#Region "EVENTOS"
    Private Sub frmRangoHorario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        LocTipo = ""
        LocClvHorario = 0
        Me.Panel1.BackColor = Me.BackColor
        Me.gbDatosRangoHorario.BackColor = Me.BackColor
        Me.Label1.BackColor = Me.BackColor
        Me.Label3.BackColor = Me.BackColor
        Me.Label4.BackColor = Me.BackColor
        Me.Label5.BackColor = Me.BackColor
        Me.Label6.BackColor = Me.BackColor

        If gbDatosRangoHorario.Visible = True Then
            Me.gbDatosRangoHorario.Visible = False
        End If
        UspLlenaComboRangoHorario()
        UspLlenaRangoHorario(0)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub
#End Region

#Region "CONTROLES"

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.gbDatosRangoHorario.Visible = False
    End Sub
    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        Me.gbDatosRangoHorario.Visible = True
        Me.dtpHoraInicial.Value = "1900-01-01 12:00:00"
        Me.dtpHoraFinal.Value = "1900-01-01 12:00:00"
        LocTipo = "N"
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        If Me.dgRangoHorario.SelectedCells.Count > 0 Then
            LocClvHorario = CInt(Me.dgRangoHorario.SelectedCells(0).Value)
            Me.gbDatosRangoHorario.Visible = True
            Me.dtpHoraInicial.Value = "1900-01-01 " + UspMuestraRangoHorario.Rows(0)(0).ToString
            Me.dtpHoraFinal.Value = "1900-01-01 " + UspMuestraRangoHorario.Rows(0)(1).ToString
            LocTipo = "M"
        Else
            MsgBox("Seleccione un Rango", MsgBoxStyle.Information, "Rango de Horarios")
        End If
    End Sub

    Private Sub btnBuscarMotivo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarMotivo.Click
        UspLlenaRangoHorario(Me.cbRangoHorario.SelectedValue)
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        BANDERA = 0
        If dtpHoraInicial.Value > dtpHoraFinal.Value Then
            MsgBox("La hora inicial debe ser menor a la hora final", MsgBoxStyle.Information, "Rango de Horarios")
            Exit Sub
        End If

        If LocTipo = "N" Then
            UspValidarRangoFechas(0)
            If BANDERA = 1 Then
                MsgBox("El rango de horas ya existe, favor de verificarlo", MsgBoxStyle.Information, "Rango Horario")
                Exit Sub
            End If
            UspGuardaRangoHorario(0)
        ElseIf LocTipo = "M" Then
            UspValidarRangoFechas(LocClvHorario)
            If BANDERA = 1 Then
                MsgBox("El rango de horas ya existe, favor de verificarlo", MsgBoxStyle.Information, "Rango Horario")
                Exit Sub
            End If
            UspGuardaRangoHorario(LocClvHorario)
        End If
        MsgBox("Fue Guardado Con Exito.", MsgBoxStyle.Information, "Rango de Horarios")
        Me.gbDatosRangoHorario.Visible = False
        UspLlenaRangoHorario(0)
        UspLlenaComboRangoHorario()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()

    End Sub
#End Region

#Region "METODOS"
    Private Sub UspValidarRangoFechas(ByVal PRMHORARIO As Integer)
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@HORAINICIAL", SqlDbType.DateTime, Me.dtpHoraInicial.Value)
            BaseII.CreateMyParameter("@HORAFINAL", SqlDbType.DateTime, Me.dtpHoraFinal.Value)
            BaseII.CreateMyParameter("@CLVHORARIO", SqlDbType.Int, PRMHORARIO)
            DT = BaseII.ConsultaDT("UspValidarRangoFechas")
            If DT.Rows.Count > 0 Then
                BANDERA = CInt(DT.Rows(0)(0).ToString)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspLlenaRangoHorario(ByVal PRMID As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ID", SqlDbType.Int, PRMID)
            Me.dgRangoHorario.DataSource = BaseII.ConsultaDT("UspLlenaRangoHorario")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function UspMuestraRangoHorario() As DataTable
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ID", SqlDbType.Int, LocClvHorario)
            UspMuestraRangoHorario = BaseII.ConsultaDT("UspMuestraRangoHorario")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub UspGuardaRangoHorario(ByVal prmID As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ID", SqlDbType.Int, prmID)
            BaseII.CreateMyParameter("@HORAINICIAL", SqlDbType.DateTime, CDate(Me.dtpHoraInicial.Value))
            BaseII.CreateMyParameter("@HORAFINAL", SqlDbType.DateTime, CDate(Me.dtpHoraFinal.Value))
            BaseII.Inserta("UspGuardaRangoHorario")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspLlenaComboRangoHorario()
        Try
            BaseII.limpiaParametros()
            cbRangoHorario.DataSource = BaseII.ConsultaDT("UspLlenaComboRangoHorario")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
  
    

#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region
   
    
   
    
End Class