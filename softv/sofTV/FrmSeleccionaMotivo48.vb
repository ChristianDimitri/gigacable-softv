﻿Public Class FrmSeleccionaMotivo48

    Private Sub FrmSeleccionaMotivo48_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Label1.ForeColor = Color.Black
        UspMuestraMotivoIntalacionFT()

    End Sub

    Private Sub UspMuestraMotivoIntalacionFT()
        Try
            BaseII.limpiaParametros()
            ComboBox1.DataSource = BaseII.ConsultaDT("UspMuestraMotivoIntalacionFT")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If Len(ComboBox1.Text) = 0 Then
            MsgBox("Debe de seleccionar un motivo de instalaciòn fuera de tiempo")
            Exit Sub
        End If
        If GloBNDMotivo48 = True Then
            GloDescrpcionMotivo48 = Me.ComboBox1.Text
            GloClvMotivo48 = Me.ComboBox1.SelectedValue
            GloBndTrabajo = True
        Else
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVORDEN", SqlDbType.BigInt, gloClv_Orden)
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, eGloContratoAux)
            BaseII.CreateMyParameter("@CLVMOTIVO", SqlDbType.Int, CInt(Me.ComboBox1.SelectedValue))
            BaseII.Inserta("UspGuardaMotivo48")


        End If
        Me.Close()
    End Sub
End Class