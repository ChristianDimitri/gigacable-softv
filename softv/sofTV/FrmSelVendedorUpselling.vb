﻿Public Class FrmSelVendedorUpselling

    Private Sub FrmSelVendedorUpselling_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        GroupBox1.BackColor = Me.BackColor
        UspLlenaGrupo()
    End Sub

    Private Sub UspLlenaGrupo()
        Try
            BaseII.limpiaParametros()
            cmbGrupo.DataSource = BaseII.ConsultaDT("UspLlenaGrupo")
            cmbGrupo.SelectedValue = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMuestraVendedoresUpselling()
        Try
            If Len(cmbGrupo.Text) > 0 Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, Me.cmbGrupo.SelectedValue)
                cmbVendedores.DataSource = BaseII.ConsultaDT("UspMuestraVendedoresUpselling")
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspGuardaClvVendedor()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, GloContrato)
            BaseII.CreateMyParameter("@CONTRTONET", SqlDbType.BigInt, GloContratonet)
            BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, cmbGrupo.SelectedValue)
            BaseII.CreateMyParameter("@CLVVENDEDOR", SqlDbType.Int, cmbVendedores.SelectedValue)
            BaseII.Inserta("UspGuardaClvVendedor")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub cmbGrupo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbGrupo.SelectedIndexChanged
        UspMuestraVendedoresUpselling()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If cmbGrupo.SelectedValue = 0 Then
            MsgBox("Seleccione el grupo de Ventas", MsgBoxStyle.Information, "Upselling")
            Exit Sub
        End If
        If cmbVendedores.SelectedValue = 0 Then
            MsgBox("Seleccione el Vendedor", MsgBoxStyle.Information, "Upselling")
            Exit Sub
        End If
        UspGuardaClvVendedor()
        Me.Close()
    End Sub
End Class