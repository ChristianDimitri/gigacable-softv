﻿Imports System.Collections.Generic
Imports CrystalDecisions.CrystalReports.Engine

Public Class FrmReporteFueraTiempoIntalaciones
    Dim op As String = Nothing
    Dim FUERA As Integer = 0
    Dim DENTRO As Integer = 0
    Dim FECHA As Integer = 0
    Dim FECHAR As Integer = 0
    Dim MOTIVO As Integer = 0
    Dim COLONIA As Integer = 0
    Dim ORDENAR As Integer = 0
    Dim Fec1Ini As Date = "01/01/1900"
    Dim Fec1Fin As Date = "01/01/1900"
    Dim Fec1IniR As Date = "01/01/1900"
    Dim Fec1FinR As Date = "01/01/1900"
    Dim nclv_trabajo As String = "0"
    Dim nClv_colonia As String = "0"

    Private Sub FrmReporteFueraTiempoIntalaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        PendientesCheckBox.Checked = True
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Close()
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If Me.CheckBox2.CheckState = CheckState.Checked Then
            op = "1"
            FrmSelFechas.Show()
        Else
            Me.FECSOLFIN.Text = ""
            Me.FECSOLINI.Text = ""
        End If
    End Sub

    Private Sub FrmReporteFueraTiempoIntalaciones_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        If op = "1" And GloBndSelFecha = True Then
            GloBndSelFecha = False
            Me.FECSOLINI.Text = GloFecha_Ini
            Me.FECSOLFIN.Text = GloFecha_Fin
            GloFecha_Ini = "01/01/1900"
            GloFecha_Fin = "01/01/1900"
        End If
        If op = "25" And GloBndSelFecha = True Then
            GloBndSelFecha = False
            Me.Label5.Text = GloFecha_Ini
            Me.Label4.Text = GloFecha_Fin
            GloFecha_Ini = "01/01/1900"
            GloFecha_Fin = "01/01/1900"
        End If
        If GloBndTrabajo = True Then
            GloBndTrabajo = False
            Me.CLV_TRABAJO.Text = GloClvMotivo48
            Me.NOMTRABAJO.Text = GloDescrpcionMotivo48
        End If
        If GlobndClv_Colonia = True Then
            GlobndClv_Colonia = False
            Me.CLV_COLONIA.Text = GloNumClv_Colonia
            Me.NOMCOLONIA.Text = GLONOMCOLONIA
        End If
    End Sub

    Private Sub CheckBox4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox4.CheckedChanged
        If Me.CheckBox4.CheckState = CheckState.Checked Then
            GloBNDMotivo48 = True
            op = "3"
            FrmSeleccionaMotivo48.ShowDialog()
            GloBNDMotivo48 = False
        Else
            Me.CLV_TRABAJO.Text = 0
            Me.NOMTRABAJO.Text = ""
        End If
    End Sub

    Private Sub CheckBox5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox5.CheckedChanged
        If Me.CheckBox5.CheckState = CheckState.Checked Then
            op = "4"
            FrmSelColonia.Show()
        Else
            Me.CLV_COLONIA.Text = 0
            Me.NOMCOLONIA.Text = ""
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If PendientesCheckBox.Checked = True Then
            DENTRO = 1
        Else
            DENTRO = 0
        End If

        If EjecutadasCheckBox.Checked = True Then
            FUERA = 1
        Else
            FUERA = 0
        End If

        If CheckBox2.Checked = True Then
            FECHA = 1
        Else
            FECHA = 0
        End If

        If CheckBox1.Checked = True Then
            FECHAR = 1
        Else
            FECHAR = 0
        End If

        If CheckBox4.Checked = True Then
            MOTIVO = 1
        Else
            MOTIVO = 0
        End If
        If CheckBox5.Checked = True Then
            COLONIA = 1
        Else
            COLONIA = 0
        End If
        If RadioButton1.Checked = True Then
            ORDENAR = 1
        End If
        If RadioButton2.Checked = True Then
            ORDENAR = 2
        End If
        If RadioButton3.Checked = True Then
            ORDENAR = 3
        End If

        Dim DS As New DataSet
        Dim rDocument As New ReportDocument
        DS = UsoReporteIntalacionesFueraTiempo()

        rDocument.Load(RutaReportes + "\ReporteFueraLimiteInstalacion.rpt")
        rDocument.SetDataSource(DS)

        SetDBReport(DS, rDocument)
        Dim fechas As String = Nothing
        'fechas = "De la Fecha: " + eFechaIni.Date + " A la Fecha: " + eFechaFin.Date
        If FECSOLINI.Text <> "" And FECSOLFIN.Text <> "" And Label5.Text = "" And Label4.Text = "" Then
            fechas = "De la Fecha de Solicitud: " + FECSOLINI.Text + " A la Fecha de Solicitud: " + FECSOLFIN.Text
        ElseIf FECSOLINI.Text = "" And FECSOLFIN.Text = "" And Label5.Text <> "" And Label4.Text <> "" Then
            fechas = "De la Fecha de Ejecución: " + Label5.Text + " A la Fecha de Ejecución: " + Label4.Text
        ElseIf FECSOLINI.Text <> "" And FECSOLFIN.Text <> "" And Label5.Text <> "" And Label4.Text <> "" Then
            fechas = "De la Fecha de Solicitud: " + FECSOLINI.Text + " A la Fecha de Solicitud: " + FECSOLFIN.Text + " y De la Fecha de Ejecución: " + Label5.Text + " A la Fecha de Ejecución: " + Label4.Text
        End If
        Dim mySelectFormula As String = Nothing
        mySelectFormula = "Reporte de Instalaciones Fuera de Tiempo"
        rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
        rDocument.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        rDocument.DataDefinition.FormulaFields("SubTitulo").Text = "'" & fechas & "'"
        'mySelectFormula = "Quejas " + Me.ComboBox1.Text
        'rDocument.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"

        CrystalReportViewer1.ReportSource = rDocument

        rDocument = Nothing

    End Sub

    Private Function UsoReporteIntalacionesFueraTiempo() As DataSet
        Try
            If IsDate(Me.FECSOLINI.Text) = True Then Fec1Ini = Me.FECSOLINI.Text
            If IsDate(Me.FECSOLFIN.Text) = True Then Fec1Fin = Me.FECSOLFIN.Text
            If IsDate(Me.Label5.Text) = True Then Fec1IniR = Me.Label5.Text
            If IsDate(Me.Label4.Text) = True Then Fec1FinR = Me.Label4.Text
            If IsNumeric(Me.CLV_TRABAJO.Text) = True Then nclv_trabajo = CStr(Me.CLV_TRABAJO.Text)
            If IsNumeric(Me.CLV_COLONIA.Text) = True Then nClv_colonia = CStr(Me.CLV_COLONIA.Text)

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@FUERA", SqlDbType.Int, FUERA)
            BaseII.CreateMyParameter("@DENTRO", SqlDbType.Int, DENTRO)
            BaseII.CreateMyParameter("@FECHA", SqlDbType.Int, FECHA)
            BaseII.CreateMyParameter("@FECINI", SqlDbType.DateTime, Fec1Ini)
            BaseII.CreateMyParameter("@FECFIN", SqlDbType.DateTime, Fec1Fin)
            BaseII.CreateMyParameter("@FECHAR", SqlDbType.Int, FECHAR)
            BaseII.CreateMyParameter("@FECINIIns", SqlDbType.DateTime, Fec1IniR)
            BaseII.CreateMyParameter("@FECFINIns", SqlDbType.DateTime, Fec1FinR)
            BaseII.CreateMyParameter("@MOTIVO", SqlDbType.Int, MOTIVO)
            BaseII.CreateMyParameter("@CLVMOTIVO", SqlDbType.Int, CInt(nclv_trabajo))
            BaseII.CreateMyParameter("@COLONIA", SqlDbType.Int, COLONIA)
            BaseII.CreateMyParameter("@CLVCOLONIA", SqlDbType.Int, CInt(nClv_colonia))
            BaseII.CreateMyParameter("@ORDENAR", SqlDbType.Int, ORDENAR)
            Dim tableNameList As New List(Of String)
            tableNameList.Add("UsoReporteIntalacionesFueraTiempo")
            Return BaseII.ConsultaDS("UsoReporteIntalacionesFueraTiempo", tableNameList)
        Catch ex As Exception

        End Try
    End Function



    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.CheckBox1.CheckState = CheckState.Checked Then
            op = "25"
            FrmSelFechas.Show()
        Else
            Me.Label4.Text = ""
            Me.Label5.Text = ""
        End If
    End Sub

    Private Sub PendientesCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PendientesCheckBox.CheckedChanged
        EjecutadasCheckBox.Checked = False
    End Sub

    Private Sub EjecutadasCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EjecutadasCheckBox.CheckedChanged
        PendientesCheckBox.Checked = False
    End Sub
End Class