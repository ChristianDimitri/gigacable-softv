Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text
Public Class FrmImprimirPPE
    Private customersByCityReport As ReportDocument

    Private Sub ConfigureCrystalReports()
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        Dim eFecha As String = Nothing
        eFecha = "Del " & eFechaIniPPE & " al " & eFechaFinPPE
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword
        Dim reportPath As String = Nothing


        If eOpPPE = 1 Then

            Me.Text = "Reporte de Venta de Pel�culas PPE"


            reportPath = RutaReportes + "\ReportServiciosPPE.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            '@Fecha_Ini
            customersByCityReport.SetParameterValue(0, eFechaIniPPE)
            '@Fecha_Ini
            customersByCityReport.SetParameterValue(1, eFechaFinPPE)


            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & GloSucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFecha & "'"
        End If

        If eOpPPE = 2 Then
            Me.Text = "Bit�cora de Activaci�n de Paquetes"


            reportPath = RutaReportes + "\ReportBitActPaq.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            '@Fecha_Ini
            customersByCityReport.SetParameterValue(0, eFechaIniPPE)
            '@Fecha_Ini
            customersByCityReport.SetParameterValue(1, eFechaFinPPE)


            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & GloSucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFecha & "'"

        End If


        If eOpPPE = 3 Then

            Me.Text = "Reporte Resumen de Ventas Sucursal"

            Dim conexion As New SqlConnection(MiConexion)

            Dim sBuilder As New StringBuilder("EXEC ReporteResumenVentas '" + eFechaIniPPE + "', '" + eFechaFinPPE + "'")
            'Dim sBuilder As New StringBuilder("EXEC REPORTETEST")
            Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
            Dim dTable As New DataTable

            dAdapter.Fill(dTable)

            reportPath = RutaReportes + "\ReportResumenVentas.rpt"
            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(dTable)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFecha & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


        End If

        If eOpPPE = 4 Then
            Me.Text = "Reporte Resumen de Ventas Vendedores"


            reportPath = RutaReportes + "\ReportResumenVentasVendedores.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            '@Fecha_Ini
            customersByCityReport.SetParameterValue(0, eFechaIniPPE)
            '@Fecha_Ini
            customersByCityReport.SetParameterValue(1, eFechaFinPPE)


            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFecha & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
        End If


       



        Me.CrystalReportViewer1.ReportSource = customersByCityReport
        customersByCityReport = Nothing
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub FrmImprimirPPE_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ConfigureCrystalReports()
    End Sub
End Class