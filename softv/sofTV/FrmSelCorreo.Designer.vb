﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelCorreo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.cbxSinCorreo = New System.Windows.Forms.CheckBox()
        Me.cbxConCorreo = New System.Windows.Forms.CheckBox()
        Me.CMBTitulo = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnCancelar
        '
        Me.btnCancelar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.ForeColor = System.Drawing.Color.Black
        Me.btnCancelar.Location = New System.Drawing.Point(192, 154)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(136, 36)
        Me.btnCancelar.TabIndex = 8
        Me.btnCancelar.Text = "&CANCELAR"
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'btnAceptar
        '
        Me.btnAceptar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.ForeColor = System.Drawing.Color.Black
        Me.btnAceptar.Location = New System.Drawing.Point(41, 154)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(136, 36)
        Me.btnAceptar.TabIndex = 7
        Me.btnAceptar.Text = "&ACEPTAR"
        Me.btnAceptar.UseVisualStyleBackColor = False
        '
        'cbxSinCorreo
        '
        Me.cbxSinCorreo.AutoSize = True
        Me.cbxSinCorreo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSinCorreo.ForeColor = System.Drawing.Color.Black
        Me.cbxSinCorreo.Location = New System.Drawing.Point(41, 112)
        Me.cbxSinCorreo.Name = "cbxSinCorreo"
        Me.cbxSinCorreo.Size = New System.Drawing.Size(100, 20)
        Me.cbxSinCorreo.TabIndex = 6
        Me.cbxSinCorreo.Text = "Sin Correo"
        Me.cbxSinCorreo.UseVisualStyleBackColor = True
        '
        'cbxConCorreo
        '
        Me.cbxConCorreo.AutoSize = True
        Me.cbxConCorreo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxConCorreo.ForeColor = System.Drawing.Color.Black
        Me.cbxConCorreo.Location = New System.Drawing.Point(41, 71)
        Me.cbxConCorreo.Name = "cbxConCorreo"
        Me.cbxConCorreo.Size = New System.Drawing.Size(105, 20)
        Me.cbxConCorreo.TabIndex = 4
        Me.cbxConCorreo.Text = "Con Correo"
        Me.cbxConCorreo.UseVisualStyleBackColor = True
        '
        'CMBTitulo
        '
        Me.CMBTitulo.AutoSize = True
        Me.CMBTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTitulo.ForeColor = System.Drawing.Color.Black
        Me.CMBTitulo.Location = New System.Drawing.Point(38, 30)
        Me.CMBTitulo.Name = "CMBTitulo"
        Me.CMBTitulo.Size = New System.Drawing.Size(179, 20)
        Me.CMBTitulo.TabIndex = 5
        Me.CMBTitulo.Text = "Seleccione la opción:"
        '
        'FrmSelCorreo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(367, 221)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.cbxSinCorreo)
        Me.Controls.Add(Me.cbxConCorreo)
        Me.Controls.Add(Me.CMBTitulo)
        Me.Name = "FrmSelCorreo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selecciona correo"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents cbxSinCorreo As System.Windows.Forms.CheckBox
    Friend WithEvents cbxConCorreo As System.Windows.Forms.CheckBox
    Friend WithEvents CMBTitulo As System.Windows.Forms.Label
End Class
