
Public Class FrmTelmexDatosClientes

    Dim InstalaraServicios As Boolean
    Dim contratoTelmex As Long

    Private Sub FrmTelmexDatosCliente_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed

    End Sub

    Private Sub FrmTelmexDatosClientes_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name) 'ColoreaFrm(Me)

        BindingNavigatorDeleteItem.Visible = False

        inicializarCombos()
        LlenarCampos()

        If OpcionCli = "C" Then

            FechaSolicitud_DTP.Enabled = False
            NumOrdenTxt.Enabled = False
            FolioTxt.Enabled = False
            NisTxt.Enabled = False
            TelefonoTelmex_Mtxt.Enabled = False
            ExtensionesNUD.Enabled = False
            FechaYhora_DTP.Enabled = False
            HoraTxt.Enabled = False
            TipoMovimientoTxt.Enabled = False
            TipoClienteCmb.Enabled = False
            PlanPagoTxt.Enabled = False
            ObservacionesTxt.Enabled = False
            ProveedorPlataformaTxt.Enabled = False
            TipoTecnologiaCmb.Enabled = False
            TipoEntregaModemCmb.Enabled = False
            MarcaModemCmb.Enabled = False
            ModeloModemCmb.Enabled = False
            SerieModemTxt.Enabled = False
            FechaEjecucion_DTP.Enabled = False
            TextBox1.Enabled = False

        ElseIf OpcionCli = "M" Or OpcionCli = "N" Then

            FechaSolicitud_DTP.Enabled = True
            NumOrdenTxt.Enabled = True
            FolioTxt.Enabled = True
            NisTxt.Enabled = True
            TelefonoTelmex_Mtxt.Enabled = True
            ExtensionesNUD.Enabled = True
            FechaYhora_DTP.Enabled = True
            HoraTxt.Enabled = True
            TipoMovimientoTxt.Enabled = False 'No permitir edici�n
            TipoClienteCmb.Enabled = False  'No permitir edici�n
            PlanPagoTxt.Enabled = False 'No permitir edici�n
            ObservacionesTxt.Enabled = True
            ProveedorPlataformaTxt.Enabled = True
            TipoTecnologiaCmb.Enabled = False
            TipoEntregaModemCmb.Enabled = True
            MarcaModemCmb.Enabled = True
            ModeloModemCmb.Enabled = True
            SerieModemTxt.Enabled = True
            FechaEjecucion_DTP.Enabled = False 'No permitir edici�n
            TextBox1.Enabled = True

        End If

    End Sub

    Private Sub inicializarCombos()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@OP", SqlDbType.BigInt, 0)
        TipoClienteCmb.DataSource = BaseII.ConsultaDT("MUESTRA_TIPOCLIENTES")

        BaseII.limpiaParametros()
        TipoTecnologiaCmb.DataSource = BaseII.ConsultaDT("dame_medios") '

        BaseII.limpiaParametros()
        TipoEntregaModemCmb.DataSource = BaseII.ConsultaDT("GetTipoEntregaModem")


        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdMarca", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, "")
        BaseII.CreateMyParameter("@Opcion", SqlDbType.Int, 0) 'Todas las marcas
        MarcaModemCmb.DataSource = BaseII.ConsultaDT("FiltroMarcas")

        BaseII.limpiaParametros()
        ModeloModemCmb.DataSource = BaseII.ConsultaDT("Dame_Modelos")

    End Sub

    Private Sub LlenarCampos()



        Try
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, contratoConTelmex)
            Dim resultado As Integer
            Dim dt As DataTable
            dt = BaseII.ConsultaDT("DameDatos_ClienteTelmex_ByContrato")

            Dim dr As DataRow

            For Each dr In dt.Rows

                contratoTelmex = dr("ContratoTelmex")

                FechaSolicitud_DTP.Text = dr("FechaSolicitudTelmex")  'FechaSolicitudTelmex
                NumOrdenTxt.Text = dr("ClvOrdenTelmex")


                'If dr("Folio") IsNot Nothing Then
                '    '    FolioTxt.Text = dr("Folio")
                '    'Else
                '    FolioTxt.Text = ""
                'End If

                'If dr("NIS") IsNot Nothing Then
                '    '    NisTxt.Text = dr("NIS")
                '    'Else
                '    NisTxt.Text = ""
                'End If

                FolioTxt.Text = dr("Folio")
                NisTxt.Text = dr("NIS")


                TelefonoTelmex_Mtxt.Text = dr("Telefono")
                ExtensionesNUD.Text = dr("CantExtencion")
                FechaYhora_DTP.Text = dr("FechaCita")   'FechaCita
                HoraTxt.Text = dr("Hora") 'Hora



                '----------------------------------
                TipoMovimientoTxt.Text = dr("Movimiento").ToString()
                TipoClienteCmb.SelectedValue = dr("TipoCliente")
                PlanPagoTxt.Text = dr("PlanPago").ToString() 'dr("PlanPago").ToString()
                ObservacionesTxt.Text = dr("Observaciones").ToString()
                '----------------------------------
                TextBox1.Text = dr("SKU").ToString()

                ProveedorPlataformaTxt.Text = dr("Proveedor")
                TipoTecnologiaCmb.SelectedValue = dr("TipoInstalacion") 'checar si es este campo
                TipoEntregaModemCmb.SelectedValue = dr("IdTipoEntrega")

                'MarcaModemCmb.SelectedValue = dr("IdMarca")

                If CInt(dr("IdMarca")) > 0 Then ' Si existe idMarca > 0, Seleccionamos la marca por id
                    MarcaModemCmb.SelectedValue = dr("IdMarca")
                End If

                If CInt(dr("IdModelo")) > 0 Then

                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@IdModelo", SqlDbType.BigInt, dr("IdModelo"))
                    BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@IdMarca", SqlDbType.BigInt, 0)
                    BaseII.CreateMyParameter("@Opcion", SqlDbType.Int, 2) 'opc 1: por idModelo

                    ModeloModemCmb.DataSource = BaseII.ConsultaDT("FiltroModelos")
                End If


                ModeloModemCmb.SelectedValue = dr("IdModelo") 'Modelo
                SerieModemTxt.Text = dr("Serie").ToString()
                FechaEjecucion_DTP.Text = dr("Fec_Eje") 'Fec_Eje


            Next

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub



    Private Sub CONRELCLIBANCOBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles CONRELCLIBANCOBindingNavigatorSaveItem.Click

        If FechaSolicitud_DTP.Text = "01/01/1900" Then  ' FechaSolicitud_DTP.Text = dr("FechaSolicitudTelmex")  'FechaSolicitudTelmex
            MsgBox("Falta la fecha de solicitud")
            Exit Sub
        End If

        If NumOrdenTxt.Text = "" Then '
            ' If Convert.ToInt32(NumOrdenTxt.Text) = 0 Or NumOrdenTxt.Text = "" Then '  NumOrdenTxt.Text = dr("ClvOrdenTelmex")
            MsgBox("Escriba el n�mero de la orden")
            Exit Sub
        End If

        If FolioTxt.Text = "" Then '     FolioTxt.Text = dr("Folio")
            MsgBox("Escriba el folio")
            Exit Sub
        End If

        If NisTxt.Text = "" Then '      NisTxt.Text = dr("NIS")
            MsgBox("Escriba el NIS")
            Exit Sub
        End If

        If TelefonoTelmex_Mtxt.Text = "(   )   -" Or TelefonoTelmex_Mtxt.Text = "(000)000-0000" Then '         TelefonoTelmex_Txt.Text = dr("Telefono")
            MsgBox("Escriba un n�mero de tel�fono Telmex v�lido")
            Exit Sub
        End If


        If ExtensionesNUD.Text = "" Then '      ExtensionesTxt.Text = dr("CantExtencion")
            MsgBox("Escriba el n�mero de extensiones")
            Exit Sub
        End If


        If FechaYhora_DTP.Text = "01/01/1900" Then  '"1900-01-01"  FechaYhora_DTP.Text = dr("FechaCita")
            MsgBox("Falta la fecha de la cita")
            Exit Sub
        End If


        Dim fecha As DateTime

        If Not DateTime.TryParse(HoraTxt.Text, fecha) Then
            MsgBox("La hora ingresada no es valida")
            'errorProvider1.SetError(HoraTxt, "la fecha ingresada no es valida")
        End If

        'If HoraTxt.Text = "" Then '           HoraTxt.Text = dr("Hora") 'Hora
        '    MsgBox("Escriba la hora de la cita")
        '    Exit Sub
        'End If

        'TipoMovimientoTxt.Text = dr("Movimiento").ToString()   ' NO PERMITIR LA EDICION
        'TipoClienteCmb.SelectedValue = dr("TipoCliente")         ' NO PERMITIR LA EDICION
        'PlanPagoTxt.Text = dr("PlanPago").ToString()            ' NO PERMITIR LA EDICION


        ' ObservacionesTxt.Text = dr("Observaciones").ToString() ' NO ES OBLIGATORIO
        '----------------------------------

        If ProveedorPlataformaTxt.Text = "" Then '        ProveedorPlataformaTxt.Text = dr("Proveedor")
            MsgBox("Escriba la plataforma")
            Exit Sub
        End If


        ' TipoTecnologiaCmb.SelectedValue = dr("TipoInstalacion") ' NO PERMITIR LA EDICION

        If TipoEntregaModemCmb.SelectedIndex = -1 Then  '   TipoEntregaModemCmb.SelectedValue = dr("IdTipoEntrega")
            MsgBox("Seleccione un tipo de entrega del m�dem")
            Exit Sub
        End If

        ' MarcaModemCmb.SelectedValue = dr("Marca")   ' No es obligatorio

        If ModeloModemCmb.SelectedIndex = -1 Or ModeloModemCmb.SelectedValue = 0 Then  '   ModeloModemCmb.SelectedValue = dr("Modelo")
            MsgBox("Seleccione un modelo")
            Exit Sub
        End If

        If SerieModemTxt.Text = "" Then '   SerieModemTxt.Text = dr("Serie").ToString()
            MsgBox("Escriba la serie del m�dem")
            Exit Sub
        End If

        '  FechaEjecucion_DTP = dr("Fec_Eje") ' NO PERMITIR LA EDICION

        If TextBox1.Text = "" Then '    SkuTxt.Text = dr("SKU").ToString()
            MsgBox("Escriba el SKU")
            Exit Sub
        End If

        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, Contrato)
            BaseII.CreateMyParameter("@ContratoTelmex", SqlDbType.Int, contratoTelmex)
            BaseII.CreateMyParameter("@Proveedor", SqlDbType.VarChar, ProveedorPlataformaTxt.Text) '"Telmex plataforma")
            BaseII.CreateMyParameter("@IdTipoEntrega", SqlDbType.BigInt, TipoEntregaModemCmb.SelectedValue) '1
            BaseII.CreateMyParameter("@FechaCita", SqlDbType.VarChar, FechaYhora_DTP.Text)

            BaseII.CreateMyParameter("@Hora", SqlDbType.VarChar, HoraTxt.Text) '"08:30"
            BaseII.CreateMyParameter("@Telefono", SqlDbType.VarChar, TelefonoTelmex_Mtxt.Text) '"(449)2112112"
            BaseII.CreateMyParameter("@NIS", SqlDbType.VarChar, NisTxt.Text) '"NIS01"
            BaseII.CreateMyParameter("@Folio", SqlDbType.VarChar, FolioTxt.Text) ' "TELMEXFOLIO01"
            BaseII.CreateMyParameter("@IdModelo", SqlDbType.BigInt, CInt(ModeloModemCmb.SelectedValue))

            BaseII.CreateMyParameter("@Observaciones", SqlDbType.VarChar, ObservacionesTxt.Text) '"OBSERVACIONES 01"
            BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, SerieModemTxt.Text) '"SERIETELMEX01"
            BaseII.CreateMyParameter("@ClvOrden", SqlDbType.BigInt, 0) 'para editar los datos la mandamos en cero
            BaseII.CreateMyParameter("@CantExtension", SqlDbType.BigInt, CInt(ExtensionesNUD.Text))
            BaseII.CreateMyParameter("@PlanPago", SqlDbType.BigInt, CInt(PlanPagoTxt.Text))

            BaseII.CreateMyParameter("@FechaSolicitudTelmex", SqlDbType.VarChar, FechaSolicitud_DTP.Text)
            BaseII.CreateMyParameter("@ClvOrdenTelmex", SqlDbType.VarChar, NumOrdenTxt.Text) ' "ORDENTELMEX01"
            BaseII.CreateMyParameter("@SKU", SqlDbType.VarChar, TextBox1.Text) '"SKU01"
            BaseII.Inserta("UpdateClientesTelTelmex")

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Me.Close()

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    Private Sub HoraTxt_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles HoraTxt.Validating

        Dim fecha As DateTime

        If Not DateTime.TryParse(HoraTxt.Text, fecha) Then
            MsgBox("la fecha ingresada no es valida")
            'errorProvider1.SetError(HoraTxt, "la fecha ingresada no es valida")
        End If

    End Sub

    Private Sub HoraTxt_KeyPress(sender As Object, e As KeyPressEventArgs) Handles HoraTxt.KeyPress

        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If

    End Sub



    Private Sub MarcaModemCmb_SelectedIndexChanged(sender As Object, e As EventArgs) Handles MarcaModemCmb.SelectedIndexChanged

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdModelo", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, "")
        BaseII.CreateMyParameter("@IdMarca", SqlDbType.BigInt, MarcaModemCmb.SelectedValue)
        BaseII.CreateMyParameter("@Opcion", SqlDbType.Int, 1) 'opc 1: por idMarca
        ModeloModemCmb.DataSource = BaseII.ConsultaDT("FiltroModelos")

    End Sub

    '15062019

End Class