<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPreguntas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPreguntas))
        Me.ButtonAgregar = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.TextBoxPregunta = New System.Windows.Forms.TextBox
        Me.TextBoxRespuesta = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.ComboBoxTipoRespuesta = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.ButtonEliminar = New System.Windows.Forms.Button
        Me.ButtonSalir = New System.Windows.Forms.Button
        Me.BindingNavigatorPreguntas = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.TSBEliminar = New System.Windows.Forms.ToolStripButton
        Me.TSBGuardar = New System.Windows.Forms.ToolStripButton
        Me.TSBNuevo = New System.Windows.Forms.ToolStripButton
        Me.TreeViewRespuesta = New System.Windows.Forms.TreeView
        Me.CheckBoxActiva = New System.Windows.Forms.CheckBox
        Me.ButtonExistentes = New System.Windows.Forms.Button
        CType(Me.BindingNavigatorPreguntas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigatorPreguntas.SuspendLayout()
        Me.SuspendLayout()
        '
        'ButtonAgregar
        '
        Me.ButtonAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAgregar.Location = New System.Drawing.Point(497, 181)
        Me.ButtonAgregar.Name = "ButtonAgregar"
        Me.ButtonAgregar.Size = New System.Drawing.Size(75, 23)
        Me.ButtonAgregar.TabIndex = 0
        Me.ButtonAgregar.Text = "&Agregar"
        Me.ButtonAgregar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(138, 107)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Pregunta"
        '
        'TextBoxPregunta
        '
        Me.TextBoxPregunta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxPregunta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxPregunta.Location = New System.Drawing.Point(209, 77)
        Me.TextBoxPregunta.Multiline = True
        Me.TextBoxPregunta.Name = "TextBoxPregunta"
        Me.TextBoxPregunta.Size = New System.Drawing.Size(375, 45)
        Me.TextBoxPregunta.TabIndex = 2
        '
        'TextBoxRespuesta
        '
        Me.TextBoxRespuesta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxRespuesta.Location = New System.Drawing.Point(209, 183)
        Me.TextBoxRespuesta.Name = "TextBoxRespuesta"
        Me.TextBoxRespuesta.Size = New System.Drawing.Size(282, 21)
        Me.TextBoxRespuesta.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(128, 189)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(75, 15)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Respuesta"
        '
        'ComboBoxTipoRespuesta
        '
        Me.ComboBoxTipoRespuesta.DisplayMember = "TipoRespuesta"
        Me.ComboBoxTipoRespuesta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxTipoRespuesta.FormattingEnabled = True
        Me.ComboBoxTipoRespuesta.Location = New System.Drawing.Point(209, 128)
        Me.ComboBoxTipoRespuesta.Name = "ComboBoxTipoRespuesta"
        Me.ComboBoxTipoRespuesta.Size = New System.Drawing.Size(282, 23)
        Me.ComboBoxTipoRespuesta.TabIndex = 8
        Me.ComboBoxTipoRespuesta.ValueMember = "IDTipoRespuesta"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(76, 136)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(127, 15)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Tipo de Respuesta"
        '
        'ButtonEliminar
        '
        Me.ButtonEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonEliminar.Location = New System.Drawing.Point(497, 210)
        Me.ButtonEliminar.Name = "ButtonEliminar"
        Me.ButtonEliminar.Size = New System.Drawing.Size(75, 23)
        Me.ButtonEliminar.TabIndex = 10
        Me.ButtonEliminar.Text = "Eliminar"
        Me.ButtonEliminar.UseVisualStyleBackColor = True
        '
        'ButtonSalir
        '
        Me.ButtonSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSalir.Location = New System.Drawing.Point(576, 521)
        Me.ButtonSalir.Name = "ButtonSalir"
        Me.ButtonSalir.Size = New System.Drawing.Size(136, 36)
        Me.ButtonSalir.TabIndex = 11
        Me.ButtonSalir.Text = "&SALIR"
        Me.ButtonSalir.UseVisualStyleBackColor = True
        '
        'BindingNavigatorPreguntas
        '
        Me.BindingNavigatorPreguntas.AddNewItem = Nothing
        Me.BindingNavigatorPreguntas.CountItem = Nothing
        Me.BindingNavigatorPreguntas.DeleteItem = Nothing
        Me.BindingNavigatorPreguntas.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorPreguntas.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSBEliminar, Me.TSBGuardar, Me.TSBNuevo})
        Me.BindingNavigatorPreguntas.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigatorPreguntas.MoveFirstItem = Nothing
        Me.BindingNavigatorPreguntas.MoveLastItem = Nothing
        Me.BindingNavigatorPreguntas.MoveNextItem = Nothing
        Me.BindingNavigatorPreguntas.MovePreviousItem = Nothing
        Me.BindingNavigatorPreguntas.Name = "BindingNavigatorPreguntas"
        Me.BindingNavigatorPreguntas.PositionItem = Nothing
        Me.BindingNavigatorPreguntas.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigatorPreguntas.Size = New System.Drawing.Size(724, 25)
        Me.BindingNavigatorPreguntas.TabIndex = 12
        Me.BindingNavigatorPreguntas.Text = "BindingNavigator1"
        '
        'TSBEliminar
        '
        Me.TSBEliminar.Image = CType(resources.GetObject("TSBEliminar.Image"), System.Drawing.Image)
        Me.TSBEliminar.Name = "TSBEliminar"
        Me.TSBEliminar.RightToLeftAutoMirrorImage = True
        Me.TSBEliminar.Size = New System.Drawing.Size(88, 22)
        Me.TSBEliminar.Text = "&ELIMINAR"
        '
        'TSBGuardar
        '
        Me.TSBGuardar.Image = CType(resources.GetObject("TSBGuardar.Image"), System.Drawing.Image)
        Me.TSBGuardar.Name = "TSBGuardar"
        Me.TSBGuardar.Size = New System.Drawing.Size(88, 22)
        Me.TSBGuardar.Text = "&GUARDAR"
        '
        'TSBNuevo
        '
        Me.TSBNuevo.Image = CType(resources.GetObject("TSBNuevo.Image"), System.Drawing.Image)
        Me.TSBNuevo.Name = "TSBNuevo"
        Me.TSBNuevo.RightToLeftAutoMirrorImage = True
        Me.TSBNuevo.Size = New System.Drawing.Size(67, 22)
        Me.TSBNuevo.Text = "&NUEVO"
        '
        'TreeViewRespuesta
        '
        Me.TreeViewRespuesta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeViewRespuesta.Location = New System.Drawing.Point(209, 210)
        Me.TreeViewRespuesta.Name = "TreeViewRespuesta"
        Me.TreeViewRespuesta.Size = New System.Drawing.Size(282, 259)
        Me.TreeViewRespuesta.TabIndex = 13
        '
        'CheckBoxActiva
        '
        Me.CheckBoxActiva.AutoSize = True
        Me.CheckBoxActiva.Checked = True
        Me.CheckBoxActiva.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxActiva.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxActiva.Location = New System.Drawing.Point(599, 77)
        Me.CheckBoxActiva.Name = "CheckBoxActiva"
        Me.CheckBoxActiva.Size = New System.Drawing.Size(63, 19)
        Me.CheckBoxActiva.TabIndex = 14
        Me.CheckBoxActiva.Text = "Activa"
        Me.CheckBoxActiva.UseVisualStyleBackColor = True
        '
        'ButtonExistentes
        '
        Me.ButtonExistentes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonExistentes.Location = New System.Drawing.Point(576, 181)
        Me.ButtonExistentes.Name = "ButtonExistentes"
        Me.ButtonExistentes.Size = New System.Drawing.Size(28, 23)
        Me.ButtonExistentes.TabIndex = 15
        Me.ButtonExistentes.Text = "..."
        Me.ButtonExistentes.UseVisualStyleBackColor = True
        '
        'FrmPreguntas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(724, 569)
        Me.Controls.Add(Me.ButtonExistentes)
        Me.Controls.Add(Me.CheckBoxActiva)
        Me.Controls.Add(Me.TreeViewRespuesta)
        Me.Controls.Add(Me.BindingNavigatorPreguntas)
        Me.Controls.Add(Me.ButtonSalir)
        Me.Controls.Add(Me.ButtonEliminar)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.ComboBoxTipoRespuesta)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TextBoxRespuesta)
        Me.Controls.Add(Me.TextBoxPregunta)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ButtonAgregar)
        Me.Name = "FrmPreguntas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Preguntas"
        CType(Me.BindingNavigatorPreguntas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigatorPreguntas.ResumeLayout(False)
        Me.BindingNavigatorPreguntas.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ButtonAgregar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBoxPregunta As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxRespuesta As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxTipoRespuesta As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ButtonEliminar As System.Windows.Forms.Button
    Friend WithEvents ButtonSalir As System.Windows.Forms.Button
    Friend WithEvents BindingNavigatorPreguntas As System.Windows.Forms.BindingNavigator
    Friend WithEvents TSBEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents TSBGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents TreeViewRespuesta As System.Windows.Forms.TreeView
    Friend WithEvents CheckBoxActiva As System.Windows.Forms.CheckBox
    Friend WithEvents ButtonExistentes As System.Windows.Forms.Button
    Friend WithEvents TSBNuevo As System.Windows.Forms.ToolStripButton
End Class
