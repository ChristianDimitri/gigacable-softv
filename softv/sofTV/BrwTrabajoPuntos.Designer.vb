﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwTrabajoPuntos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Label13 As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim Clv_calleLabel1 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.cmbTipServ = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.cmbTipo = New System.Windows.Forms.Label()
        Me.cmbTrabajos = New System.Windows.Forms.ComboBox()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblVigente = New System.Windows.Forms.TextBox()
        Me.lblFechaFin = New System.Windows.Forms.TextBox()
        Me.lblFechaIni = New System.Windows.Forms.TextBox()
        Me.lblPuntos = New System.Windows.Forms.TextBox()
        Me.lblTrabajo = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblClave = New System.Windows.Forms.Label()
        Me.txtClave = New System.Windows.Forms.TextBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dgTrabajosPuntos = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLVTRABAJO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DESCRIPCION = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FECHAINICIAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FECHAFINAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VIGENTE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PUNTOS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.BtnConsultar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Label13 = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        Label11 = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        Clv_calleLabel1 = New System.Windows.Forms.Label()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgTrabajosPuntos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label13
        '
        Label13.AutoSize = True
        Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label13.ForeColor = System.Drawing.Color.White
        Label13.Location = New System.Drawing.Point(17, 208)
        Label13.Name = "Label13"
        Label13.Size = New System.Drawing.Size(63, 15)
        Label13.TabIndex = 21
        Label13.Text = "Vigente :"
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label12.ForeColor = System.Drawing.Color.White
        Label12.Location = New System.Drawing.Point(17, 173)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(90, 15)
        Label12.TabIndex = 19
        Label12.Text = "Fecha Final :"
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label11.ForeColor = System.Drawing.Color.White
        Label11.Location = New System.Drawing.Point(17, 138)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(97, 15)
        Label11.TabIndex = 17
        Label11.Text = "Fecha Inicial :"
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label10.ForeColor = System.Drawing.Color.White
        Label10.Location = New System.Drawing.Point(17, 103)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(59, 15)
        Label10.TabIndex = 15
        Label10.Text = "Puntos :"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.White
        NombreLabel.Location = New System.Drawing.Point(17, 68)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(64, 15)
        NombreLabel.TabIndex = 3
        NombreLabel.Text = "Trabajo :"
        '
        'Clv_calleLabel1
        '
        Clv_calleLabel1.AutoSize = True
        Clv_calleLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_calleLabel1.ForeColor = System.Drawing.Color.White
        Clv_calleLabel1.Location = New System.Drawing.Point(17, 50)
        Clv_calleLabel1.Name = "Clv_calleLabel1"
        Clv_calleLabel1.Size = New System.Drawing.Size(50, 15)
        Clv_calleLabel1.TabIndex = 1
        Clv_calleLabel1.Text = "Clave :"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(9, 13)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.cmbTipServ)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.ComboBox1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cmbTipo)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cmbTrabajos)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnBuscar)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtClave)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.dgTrabajosPuntos)
        Me.SplitContainer1.Size = New System.Drawing.Size(836, 694)
        Me.SplitContainer1.SplitterDistance = 278
        Me.SplitContainer1.TabIndex = 38
        Me.SplitContainer1.TabStop = False
        '
        'cmbTipServ
        '
        Me.cmbTipServ.DisplayMember = "Concepto"
        Me.cmbTipServ.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTipServ.FormattingEnabled = True
        Me.cmbTipServ.Location = New System.Drawing.Point(17, 114)
        Me.cmbTipServ.Name = "cmbTipServ"
        Me.cmbTipServ.Size = New System.Drawing.Size(243, 23)
        Me.cmbTipServ.TabIndex = 38
        Me.cmbTipServ.ValueMember = "Clv_TipSer"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(14, 96)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(118, 15)
        Me.Label1.TabIndex = 37
        Me.Label1.Text = "Tipo de Servicio :"
        '
        'ComboBox1
        '
        Me.ComboBox1.DisplayMember = "DESCRIPCION"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(17, 171)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(243, 23)
        Me.ComboBox1.TabIndex = 36
        Me.ComboBox1.ValueMember = "TIPO"
        '
        'cmbTipo
        '
        Me.cmbTipo.AutoSize = True
        Me.cmbTipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTipo.Location = New System.Drawing.Point(14, 153)
        Me.cmbTipo.Name = "cmbTipo"
        Me.cmbTipo.Size = New System.Drawing.Size(35, 15)
        Me.cmbTipo.TabIndex = 35
        Me.cmbTipo.Text = "Tipo"
        '
        'cmbTrabajos
        '
        Me.cmbTrabajos.DisplayMember = "DESCRIPCION"
        Me.cmbTrabajos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTrabajos.FormattingEnabled = True
        Me.cmbTrabajos.Location = New System.Drawing.Point(17, 230)
        Me.cmbTrabajos.Name = "cmbTrabajos"
        Me.cmbTrabajos.Size = New System.Drawing.Size(243, 23)
        Me.cmbTrabajos.TabIndex = 34
        Me.cmbTrabajos.ValueMember = "Clv_Trabajo"
        '
        'btnBuscar
        '
        Me.btnBuscar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscar.ForeColor = System.Drawing.Color.Black
        Me.btnBuscar.Location = New System.Drawing.Point(17, 259)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(88, 23)
        Me.btnBuscar.TabIndex = 8
        Me.btnBuscar.Text = "&Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(Me.lblVigente)
        Me.Panel1.Controls.Add(Label13)
        Me.Panel1.Controls.Add(Me.lblFechaFin)
        Me.Panel1.Controls.Add(Label12)
        Me.Panel1.Controls.Add(Me.lblFechaIni)
        Me.Panel1.Controls.Add(Label11)
        Me.Panel1.Controls.Add(Me.lblPuntos)
        Me.Panel1.Controls.Add(Label10)
        Me.Panel1.Controls.Add(Me.lblTrabajo)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(Clv_calleLabel1)
        Me.Panel1.Controls.Add(Me.lblClave)
        Me.Panel1.Location = New System.Drawing.Point(17, 296)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(243, 383)
        Me.Panel1.TabIndex = 8
        '
        'lblVigente
        '
        Me.lblVigente.BackColor = System.Drawing.Color.DarkOrange
        Me.lblVigente.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblVigente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVigente.ForeColor = System.Drawing.Color.Black
        Me.lblVigente.Location = New System.Drawing.Point(20, 226)
        Me.lblVigente.Name = "lblVigente"
        Me.lblVigente.ReadOnly = True
        Me.lblVigente.Size = New System.Drawing.Size(207, 14)
        Me.lblVigente.TabIndex = 22
        Me.lblVigente.TabStop = False
        '
        'lblFechaFin
        '
        Me.lblFechaFin.BackColor = System.Drawing.Color.DarkOrange
        Me.lblFechaFin.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblFechaFin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaFin.ForeColor = System.Drawing.Color.Black
        Me.lblFechaFin.Location = New System.Drawing.Point(20, 191)
        Me.lblFechaFin.Name = "lblFechaFin"
        Me.lblFechaFin.ReadOnly = True
        Me.lblFechaFin.Size = New System.Drawing.Size(207, 14)
        Me.lblFechaFin.TabIndex = 20
        Me.lblFechaFin.TabStop = False
        '
        'lblFechaIni
        '
        Me.lblFechaIni.BackColor = System.Drawing.Color.DarkOrange
        Me.lblFechaIni.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblFechaIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaIni.ForeColor = System.Drawing.Color.Black
        Me.lblFechaIni.Location = New System.Drawing.Point(20, 156)
        Me.lblFechaIni.Name = "lblFechaIni"
        Me.lblFechaIni.ReadOnly = True
        Me.lblFechaIni.Size = New System.Drawing.Size(207, 14)
        Me.lblFechaIni.TabIndex = 18
        Me.lblFechaIni.TabStop = False
        '
        'lblPuntos
        '
        Me.lblPuntos.BackColor = System.Drawing.Color.DarkOrange
        Me.lblPuntos.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblPuntos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPuntos.ForeColor = System.Drawing.Color.Black
        Me.lblPuntos.Location = New System.Drawing.Point(20, 121)
        Me.lblPuntos.Name = "lblPuntos"
        Me.lblPuntos.ReadOnly = True
        Me.lblPuntos.Size = New System.Drawing.Size(207, 14)
        Me.lblPuntos.TabIndex = 16
        Me.lblPuntos.TabStop = False
        '
        'lblTrabajo
        '
        Me.lblTrabajo.BackColor = System.Drawing.Color.DarkOrange
        Me.lblTrabajo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblTrabajo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrabajo.ForeColor = System.Drawing.Color.Black
        Me.lblTrabajo.Location = New System.Drawing.Point(20, 86)
        Me.lblTrabajo.Name = "lblTrabajo"
        Me.lblTrabajo.ReadOnly = True
        Me.lblTrabajo.Size = New System.Drawing.Size(207, 14)
        Me.lblTrabajo.TabIndex = 6
        Me.lblTrabajo.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(3, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(143, 20)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Datos de Puntos"
        '
        'lblClave
        '
        Me.lblClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClave.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lblClave.Location = New System.Drawing.Point(75, 50)
        Me.lblClave.Name = "lblClave"
        Me.lblClave.Size = New System.Drawing.Size(165, 23)
        Me.lblClave.TabIndex = 2
        '
        'txtClave
        '
        Me.txtClave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClave.Location = New System.Drawing.Point(17, 61)
        Me.txtClave.Name = "txtClave"
        Me.txtClave.Size = New System.Drawing.Size(88, 21)
        Me.txtClave.TabIndex = 5
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(13, 12)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(201, 24)
        Me.CMBLabel1.TabIndex = 1
        Me.CMBLabel1.Text = "Buscar Trabajo Por :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(14, 43)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(50, 15)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Clave :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(14, 212)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 15)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Trabajo :"
        '
        'dgTrabajosPuntos
        '
        Me.dgTrabajosPuntos.AllowUserToAddRows = False
        Me.dgTrabajosPuntos.AllowUserToDeleteRows = False
        Me.dgTrabajosPuntos.AllowUserToOrderColumns = True
        Me.dgTrabajosPuntos.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgTrabajosPuntos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgTrabajosPuntos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgTrabajosPuntos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.CLVTRABAJO, Me.DESCRIPCION, Me.FECHAINICIAL, Me.FECHAFINAL, Me.VIGENTE, Me.PUNTOS})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgTrabajosPuntos.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgTrabajosPuntos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgTrabajosPuntos.Location = New System.Drawing.Point(0, 0)
        Me.dgTrabajosPuntos.Name = "dgTrabajosPuntos"
        Me.dgTrabajosPuntos.ReadOnly = True
        Me.dgTrabajosPuntos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgTrabajosPuntos.Size = New System.Drawing.Size(554, 694)
        Me.dgTrabajosPuntos.TabIndex = 1
        Me.dgTrabajosPuntos.TabStop = False
        '
        'ID
        '
        Me.ID.DataPropertyName = "ID"
        Me.ID.HeaderText = "Clave"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        '
        'CLVTRABAJO
        '
        Me.CLVTRABAJO.DataPropertyName = "CLVTRABAJO"
        Me.CLVTRABAJO.HeaderText = "CLVTRABAJO"
        Me.CLVTRABAJO.Name = "CLVTRABAJO"
        Me.CLVTRABAJO.ReadOnly = True
        Me.CLVTRABAJO.Visible = False
        '
        'DESCRIPCION
        '
        Me.DESCRIPCION.DataPropertyName = "DESCRIPCION"
        Me.DESCRIPCION.HeaderText = "Trabajo"
        Me.DESCRIPCION.Name = "DESCRIPCION"
        Me.DESCRIPCION.ReadOnly = True
        '
        'FECHAINICIAL
        '
        Me.FECHAINICIAL.DataPropertyName = "FECHAINICIAL"
        Me.FECHAINICIAL.HeaderText = "Fecha Inicial"
        Me.FECHAINICIAL.Name = "FECHAINICIAL"
        Me.FECHAINICIAL.ReadOnly = True
        '
        'FECHAFINAL
        '
        Me.FECHAFINAL.DataPropertyName = "FECHAFINAL"
        Me.FECHAFINAL.HeaderText = "Fecha Final"
        Me.FECHAFINAL.Name = "FECHAFINAL"
        Me.FECHAFINAL.ReadOnly = True
        '
        'VIGENTE
        '
        Me.VIGENTE.DataPropertyName = "VIGENTE"
        Me.VIGENTE.HeaderText = "Vigente"
        Me.VIGENTE.Name = "VIGENTE"
        Me.VIGENTE.ReadOnly = True
        '
        'PUNTOS
        '
        Me.PUNTOS.DataPropertyName = "PUNTOS"
        Me.PUNTOS.HeaderText = "Puntos"
        Me.PUNTOS.Name = "PUNTOS"
        Me.PUNTOS.ReadOnly = True
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(865, 668)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(136, 36)
        Me.btnSalir.TabIndex = 37
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'btnModificar
        '
        Me.btnModificar.BackColor = System.Drawing.Color.Orange
        Me.btnModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModificar.ForeColor = System.Drawing.Color.Black
        Me.btnModificar.Location = New System.Drawing.Point(865, 97)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(136, 36)
        Me.btnModificar.TabIndex = 36
        Me.btnModificar.Text = "&MODIFICAR"
        Me.btnModificar.UseVisualStyleBackColor = False
        '
        'BtnConsultar
        '
        Me.BtnConsultar.BackColor = System.Drawing.Color.Orange
        Me.BtnConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnConsultar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnConsultar.ForeColor = System.Drawing.Color.Black
        Me.BtnConsultar.Location = New System.Drawing.Point(865, 55)
        Me.BtnConsultar.Name = "BtnConsultar"
        Me.BtnConsultar.Size = New System.Drawing.Size(136, 36)
        Me.BtnConsultar.TabIndex = 35
        Me.BtnConsultar.Text = "&CONSULTA"
        Me.BtnConsultar.UseVisualStyleBackColor = False
        '
        'btnNuevo
        '
        Me.btnNuevo.BackColor = System.Drawing.Color.Orange
        Me.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo.ForeColor = System.Drawing.Color.Black
        Me.btnNuevo.Location = New System.Drawing.Point(865, 13)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(136, 36)
        Me.btnNuevo.TabIndex = 34
        Me.btnNuevo.Text = "&NUEVO"
        Me.btnNuevo.UseVisualStyleBackColor = False
        '
        'BrwTrabajoPuntos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1011, 721)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.BtnConsultar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Name = "BrwTrabajoPuntos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Puntos de trabajos por fechas"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgTrabajosPuntos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents cmbTrabajos As System.Windows.Forms.ComboBox
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblVigente As System.Windows.Forms.TextBox
    Friend WithEvents lblFechaFin As System.Windows.Forms.TextBox
    Friend WithEvents lblFechaIni As System.Windows.Forms.TextBox
    Friend WithEvents lblPuntos As System.Windows.Forms.TextBox
    Friend WithEvents lblTrabajo As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblClave As System.Windows.Forms.Label
    Friend WithEvents txtClave As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dgTrabajosPuntos As System.Windows.Forms.DataGridView
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents BtnConsultar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbTipo As System.Windows.Forms.Label
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLVTRABAJO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DESCRIPCION As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FECHAINICIAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FECHAFINAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VIGENTE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PUNTOS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmbTipServ As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
