<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAccesopoUsuarioProspectos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmAccesopoUsuarioProspectos))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LogoPictureBox = New System.Windows.Forms.PictureBox()
        Me.CMBPasswordLabel = New System.Windows.Forms.Label()
        Me.Cancel = New System.Windows.Forms.Button()
        Me.OK = New System.Windows.Forms.Button()
        Me.PasaporteTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_UsuarioTextBox = New System.Windows.Forms.TextBox()
        Me.DataSetEdgarRev2 = New sofTV.DataSetEdgarRev2()
        Me.VerAcceso_ChecaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VerAcceso_ChecaTableAdapter = New sofTV.DataSetEdgarRev2TableAdapters.VerAcceso_ChecaTableAdapter()
        Me.CMBClv_UsuarioLabel = New System.Windows.Forms.Label()
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.Dame_clvtipusuarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_clvtipusuarioTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Dame_clvtipusuarioTableAdapter()
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VerAcceso_ChecaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_clvtipusuarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.CausesValidation = False
        Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(13, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(220, 35)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "Acceso Restringido"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LogoPictureBox
        '
        Me.LogoPictureBox.Image = CType(resources.GetObject("LogoPictureBox.Image"), System.Drawing.Image)
        Me.LogoPictureBox.Location = New System.Drawing.Point(13, 38)
        Me.LogoPictureBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.LogoPictureBox.Name = "LogoPictureBox"
        Me.LogoPictureBox.Size = New System.Drawing.Size(220, 195)
        Me.LogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.LogoPictureBox.TabIndex = 11
        Me.LogoPictureBox.TabStop = False
        '
        'CMBPasswordLabel
        '
        Me.CMBPasswordLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBPasswordLabel.Location = New System.Drawing.Point(286, 99)
        Me.CMBPasswordLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBPasswordLabel.Name = "CMBPasswordLabel"
        Me.CMBPasswordLabel.Size = New System.Drawing.Size(149, 38)
        Me.CMBPasswordLabel.TabIndex = 18
        Me.CMBPasswordLabel.Text = "Contraseña del Administrador :"
        Me.CMBPasswordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Cancel
        '
        Me.Cancel.BackColor = System.Drawing.Color.Orange
        Me.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cancel.Location = New System.Drawing.Point(412, 198)
        Me.Cancel.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Cancel.Name = "Cancel"
        Me.Cancel.Size = New System.Drawing.Size(152, 35)
        Me.Cancel.TabIndex = 3
        Me.Cancel.Text = "&CANCELAR"
        Me.Cancel.UseVisualStyleBackColor = False
        '
        'OK
        '
        Me.OK.BackColor = System.Drawing.Color.Orange
        Me.OK.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OK.Location = New System.Drawing.Point(249, 198)
        Me.OK.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.OK.Name = "OK"
        Me.OK.Size = New System.Drawing.Size(155, 35)
        Me.OK.TabIndex = 2
        Me.OK.Text = "&ACEPTAR"
        Me.OK.UseVisualStyleBackColor = False
        '
        'PasaporteTextBox
        '
        Me.PasaporteTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PasaporteTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PasaporteTextBox.Location = New System.Drawing.Point(289, 140)
        Me.PasaporteTextBox.MaxLength = 10
        Me.PasaporteTextBox.Name = "PasaporteTextBox"
        Me.PasaporteTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.PasaporteTextBox.Size = New System.Drawing.Size(97, 24)
        Me.PasaporteTextBox.TabIndex = 1
        '
        'Clv_UsuarioTextBox
        '
        Me.Clv_UsuarioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_UsuarioTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Clv_UsuarioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_UsuarioTextBox.Location = New System.Drawing.Point(289, 72)
        Me.Clv_UsuarioTextBox.MaxLength = 15
        Me.Clv_UsuarioTextBox.Name = "Clv_UsuarioTextBox"
        Me.Clv_UsuarioTextBox.Size = New System.Drawing.Size(116, 24)
        Me.Clv_UsuarioTextBox.TabIndex = 0
        '
        'DataSetEdgarRev2
        '
        Me.DataSetEdgarRev2.DataSetName = "DataSetEdgarRev2"
        Me.DataSetEdgarRev2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'VerAcceso_ChecaBindingSource
        '
        Me.VerAcceso_ChecaBindingSource.DataMember = "VerAcceso_Checa"
        Me.VerAcceso_ChecaBindingSource.DataSource = Me.DataSetEdgarRev2
        '
        'VerAcceso_ChecaTableAdapter
        '
        Me.VerAcceso_ChecaTableAdapter.ClearBeforeFill = True
        '
        'CMBClv_UsuarioLabel
        '
        Me.CMBClv_UsuarioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBClv_UsuarioLabel.Location = New System.Drawing.Point(286, 31)
        Me.CMBClv_UsuarioLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBClv_UsuarioLabel.Name = "CMBClv_UsuarioLabel"
        Me.CMBClv_UsuarioLabel.Size = New System.Drawing.Size(149, 38)
        Me.CMBClv_UsuarioLabel.TabIndex = 19
        Me.CMBClv_UsuarioLabel.Text = "Login del Administrador :"
        Me.CMBClv_UsuarioLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Dame_clvtipusuarioBindingSource
        '
        Me.Dame_clvtipusuarioBindingSource.DataMember = "Dame_clvtipusuario"
        Me.Dame_clvtipusuarioBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Dame_clvtipusuarioTableAdapter
        '
        Me.Dame_clvtipusuarioTableAdapter.ClearBeforeFill = True
        '
        'FrmAccesopoUsuarioProspectos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(579, 247)
        Me.Controls.Add(Me.CMBClv_UsuarioLabel)
        Me.Controls.Add(Me.CMBPasswordLabel)
        Me.Controls.Add(Me.Cancel)
        Me.Controls.Add(Me.OK)
        Me.Controls.Add(Me.PasaporteTextBox)
        Me.Controls.Add(Me.Clv_UsuarioTextBox)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.LogoPictureBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmAccesopoUsuarioProspectos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seguridad"
        Me.TopMost = True
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VerAcceso_ChecaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_clvtipusuarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LogoPictureBox As System.Windows.Forms.PictureBox
    Friend WithEvents CMBPasswordLabel As System.Windows.Forms.Label
    Friend WithEvents Cancel As System.Windows.Forms.Button
    Friend WithEvents OK As System.Windows.Forms.Button
    Friend WithEvents PasaporteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_UsuarioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DataSetEdgarRev2 As sofTV.DataSetEdgarRev2
    Friend WithEvents VerAcceso_ChecaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VerAcceso_ChecaTableAdapter As sofTV.DataSetEdgarRev2TableAdapters.VerAcceso_ChecaTableAdapter
    Friend WithEvents CMBClv_UsuarioLabel As System.Windows.Forms.Label
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Dame_clvtipusuarioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_clvtipusuarioTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Dame_clvtipusuarioTableAdapter
End Class
