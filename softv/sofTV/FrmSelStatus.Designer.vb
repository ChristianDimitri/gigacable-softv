﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelStatus
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.cbContratado = New System.Windows.Forms.CheckBox()
        Me.bnCancelar = New System.Windows.Forms.Button()
        Me.bnAceptar = New System.Windows.Forms.Button()
        Me.cbInstalado = New System.Windows.Forms.CheckBox()
        Me.cbDesconectado = New System.Windows.Forms.CheckBox()
        Me.cbSuspendido = New System.Windows.Forms.CheckBox()
        Me.cbBaja = New System.Windows.Forms.CheckBox()
        Me.cbFueraDeArea = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'cbContratado
        '
        Me.cbContratado.AutoSize = True
        Me.cbContratado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbContratado.Location = New System.Drawing.Point(66, 36)
        Me.cbContratado.Name = "cbContratado"
        Me.cbContratado.Size = New System.Drawing.Size(96, 19)
        Me.cbContratado.TabIndex = 0
        Me.cbContratado.Text = "Contratado"
        Me.cbContratado.UseVisualStyleBackColor = True
        '
        'bnCancelar
        '
        Me.bnCancelar.BackColor = System.Drawing.Color.DarkOrange
        Me.bnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnCancelar.Location = New System.Drawing.Point(304, 169)
        Me.bnCancelar.Name = "bnCancelar"
        Me.bnCancelar.Size = New System.Drawing.Size(136, 36)
        Me.bnCancelar.TabIndex = 7
        Me.bnCancelar.Text = "&Cancelar"
        Me.bnCancelar.UseVisualStyleBackColor = False
        '
        'bnAceptar
        '
        Me.bnAceptar.BackColor = System.Drawing.Color.DarkOrange
        Me.bnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnAceptar.Location = New System.Drawing.Point(162, 169)
        Me.bnAceptar.Name = "bnAceptar"
        Me.bnAceptar.Size = New System.Drawing.Size(136, 36)
        Me.bnAceptar.TabIndex = 6
        Me.bnAceptar.Text = "&Aceptar"
        Me.bnAceptar.UseVisualStyleBackColor = False
        '
        'cbInstalado
        '
        Me.cbInstalado.AutoSize = True
        Me.cbInstalado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbInstalado.Location = New System.Drawing.Point(66, 61)
        Me.cbInstalado.Name = "cbInstalado"
        Me.cbInstalado.Size = New System.Drawing.Size(85, 19)
        Me.cbInstalado.TabIndex = 8
        Me.cbInstalado.Text = "Instalado"
        Me.cbInstalado.UseVisualStyleBackColor = True
        '
        'cbDesconectado
        '
        Me.cbDesconectado.AutoSize = True
        Me.cbDesconectado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbDesconectado.Location = New System.Drawing.Point(66, 86)
        Me.cbDesconectado.Name = "cbDesconectado"
        Me.cbDesconectado.Size = New System.Drawing.Size(117, 19)
        Me.cbDesconectado.TabIndex = 9
        Me.cbDesconectado.Text = "Desconectado"
        Me.cbDesconectado.UseVisualStyleBackColor = True
        '
        'cbSuspendido
        '
        Me.cbSuspendido.AutoSize = True
        Me.cbSuspendido.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSuspendido.Location = New System.Drawing.Point(252, 36)
        Me.cbSuspendido.Name = "cbSuspendido"
        Me.cbSuspendido.Size = New System.Drawing.Size(102, 19)
        Me.cbSuspendido.TabIndex = 10
        Me.cbSuspendido.Text = "Suspendido"
        Me.cbSuspendido.UseVisualStyleBackColor = True
        '
        'cbBaja
        '
        Me.cbBaja.AutoSize = True
        Me.cbBaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbBaja.Location = New System.Drawing.Point(252, 86)
        Me.cbBaja.Name = "cbBaja"
        Me.cbBaja.Size = New System.Drawing.Size(55, 19)
        Me.cbBaja.TabIndex = 11
        Me.cbBaja.Text = "Baja"
        Me.cbBaja.UseVisualStyleBackColor = True
        '
        'cbFueraDeArea
        '
        Me.cbFueraDeArea.AutoSize = True
        Me.cbFueraDeArea.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbFueraDeArea.Location = New System.Drawing.Point(252, 61)
        Me.cbFueraDeArea.Name = "cbFueraDeArea"
        Me.cbFueraDeArea.Size = New System.Drawing.Size(116, 19)
        Me.cbFueraDeArea.TabIndex = 12
        Me.cbFueraDeArea.Text = "Fuera de Área"
        Me.cbFueraDeArea.UseVisualStyleBackColor = True
        '
        'FrmSelStatus
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(452, 217)
        Me.Controls.Add(Me.cbFueraDeArea)
        Me.Controls.Add(Me.cbBaja)
        Me.Controls.Add(Me.cbSuspendido)
        Me.Controls.Add(Me.cbDesconectado)
        Me.Controls.Add(Me.cbInstalado)
        Me.Controls.Add(Me.bnCancelar)
        Me.Controls.Add(Me.bnAceptar)
        Me.Controls.Add(Me.cbContratado)
        Me.Name = "FrmSelStatus"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selecciona Status"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents cbContratado As System.Windows.Forms.CheckBox
    Friend WithEvents bnCancelar As System.Windows.Forms.Button
    Friend WithEvents bnAceptar As System.Windows.Forms.Button
    Friend WithEvents cbInstalado As System.Windows.Forms.CheckBox
    Friend WithEvents cbDesconectado As System.Windows.Forms.CheckBox
    Friend WithEvents cbSuspendido As System.Windows.Forms.CheckBox
    Friend WithEvents cbBaja As System.Windows.Forms.CheckBox
    Friend WithEvents cbFueraDeArea As System.Windows.Forms.CheckBox
End Class
