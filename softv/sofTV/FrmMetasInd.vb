Imports System.Data.SqlClient
Public Class FrmMetasInd
    Private Bnd As Boolean = False

    Private Sub FrmMetasInd_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            colorea(Me, Me.Name)
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.MuestraTipServEricTableAdapter.Connection = CON
            Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric2.MuestraTipServEric, 0, 0)
            Me.MuestraAniosTableAdapter.Connection = CON
            Me.MuestraAniosTableAdapter.Fill(Me.DataSetEric2.MuestraAnios)
            Me.ConGrupoVentasTableAdapter.Connection = CON
            Me.ConGrupoVentasTableAdapter.Fill(Me.DataSetEric2.ConGrupoVentas, 0, 3)
            CON.Close()
            Consultar()
            Bnd = True
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Consultar()
        Try

            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()
            Me.ConMetasIndTableAdapter.Connection = CON2
            Me.ConMetasIndTableAdapter.Fill(Me.DataSetEric2.ConMetasInd, CInt(Me.GrupoComboBox.SelectedValue), CInt(Me.NombreComboBox.SelectedValue), CInt(Me.ConceptoComboBox.SelectedValue), CInt(Me.AnioComboBox.SelectedValue))
            CON2.Close()


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub GrupoComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GrupoComboBox.SelectedValueChanged
        If Bnd = True Then
            Consultar()
        End If
    End Sub


    Private Sub NombreComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles NombreComboBox.SelectedValueChanged
        If Bnd = True Then
            Consultar()
        End If
    End Sub

    Private Sub ConceptoComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ConceptoComboBox.SelectedValueChanged
        If Bnd = True Then
            Consultar()
        End If
    End Sub


    Private Sub AnioComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AnioComboBox.SelectedValueChanged
        If Bnd = True Then
            Consultar()
        End If
    End Sub

    Private Sub ConMetasIndBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConMetasIndBindingNavigatorSaveItem.Click
        Try
            'Dim CON7 As New SqlConnection(MiConexion)
            'CON7.Open()
            'Me.ConMetasIndTableAdapter.Connection = CON7
            'Me.ConMetasIndTableAdapter.Update(Me.DataSetEric2.ConMetasInd)
            'CON7.Close()
            'MsgBox(mensaje5)
            Dim I As Integer
            Me.ConMetasIndDataGridView.EndEdit()
            For I = 0 To ConMetasIndDataGridView.Rows.Count - 1
                UspGuardaMetasInd(Me.NombreComboBox.SelectedValue, Me.ConMetasIndDataGridView(2, I).Value, Me.ConMetasIndDataGridView(4, I).Value, Me.ConMetasIndDataGridView(5, I).Value,
               Me.ConMetasIndDataGridView(6, I).Value, Me.ConMetasIndDataGridView(7, I).Value, Me.ConMetasIndDataGridView(8, I).Value, Me.ConMetasIndDataGridView(9, I).Value, Me.ConMetasIndDataGridView(10, I).Value, Me.ConMetasIndDataGridView(11, I).Value,
               Me.ConMetasIndDataGridView(12, I).Value, Me.ConMetasIndDataGridView(13, I).Value, Me.ConMetasIndDataGridView(14, I).Value, Me.ConMetasIndDataGridView(15, I).Value, CInt(Me.AnioComboBox.Text))
            Next

            Consultar()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

      

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim R As Integer = 0
        Try
            R = MsgBox("Se Eliminar� el Servicio. �Deseas Continuar?", MsgBoxStyle.YesNo, "Atenci�n")
            If R = 6 Then
                Dim CON8 As New SqlConnection(MiConexion)
                CON8.Open()
                Me.ConMetasIndTableAdapter.Connection = CON8
                Me.ConMetasIndTableAdapter.Delete(CInt(Me.Clv_GrupoTextBox.Text), CInt(Me.ClaveTextBox.Text), CInt(Me.Clv_ServicioTextBox.Text), CInt(Me.AnioTextBox.Text))
                CON8.Close()
                MsgBox(mensaje6)
                Consultar()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Bnd = False
        Me.Close()
    End Sub

    Private Sub ConMetasIndDataGridView_CellLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ConMetasIndDataGridView.CellLeave
        Try
            Dim CON7 As New SqlConnection(MiConexion)
            CON7.Open()
            Me.ConMetasIndTableAdapter.Connection = CON7
            Me.ConMetasIndTableAdapter.Update(Me.DataSetEric2.ConMetasInd)
            CON7.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Checa()
        Try
            If Me.NombreComboBox.Items.Count = 0 Then
                Bnd = False
                Me.Panel1.Enabled = False
            Else
                Bnd = True
                Me.Panel1.Enabled = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Clv_GrupoTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_GrupoTextBox1.TextChanged
        Try

            Dim CON5 As New SqlConnection(MiConexion)
            CON5.Open()
            Me.MuestraGrupoTableAdapter.Connection = CON5
            Me.MuestraGrupoTableAdapter.Fill(Me.DataSetEric2.MuestraGrupo, CInt(Me.Clv_GrupoTextBox1.Text))
            CON5.Close()
            Checa()
            If Bnd = True Then
                Consultar()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub UspGuardaMetasInd(ByVal prmClvSupervisor As Integer, ByVal prmClvServicio As Integer, ByVal prmEne As Integer, ByVal prmfeb As Integer, ByVal prmMar As Integer, _
                                        ByVal prmAbr As Integer, ByVal prmMay As Integer, ByVal prmJun As Integer, ByVal prmJul As Integer, ByVal prmAgo As Integer, ByVal prmSep As Integer, _
                                        ByVal prmOct As Integer, ByVal prmNov As Integer, ByVal prmDic As Integer, ByVal prmAnio As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSUPERVISOR", SqlDbType.Int, prmClvSupervisor)
            BaseII.CreateMyParameter("@CLVSERVICIO", SqlDbType.Int, prmClvServicio)
            BaseII.CreateMyParameter("@ENE", SqlDbType.Int, prmEne)
            BaseII.CreateMyParameter("@FEB", SqlDbType.Int, prmfeb)
            BaseII.CreateMyParameter("@MAR", SqlDbType.Int, prmMar)
            BaseII.CreateMyParameter("@ABR", SqlDbType.Int, prmAbr)
            BaseII.CreateMyParameter("@MAY", SqlDbType.Int, prmMay)
            BaseII.CreateMyParameter("@JUN", SqlDbType.Int, prmJun)
            BaseII.CreateMyParameter("@JUL", SqlDbType.Int, prmJul)
            BaseII.CreateMyParameter("@AGO", SqlDbType.Int, prmAgo)
            BaseII.CreateMyParameter("@SEP", SqlDbType.Int, prmSep)
            BaseII.CreateMyParameter("@OCT", SqlDbType.Int, prmOct)
            BaseII.CreateMyParameter("@NOV", SqlDbType.Int, prmNov)
            BaseII.CreateMyParameter("@DIC", SqlDbType.Int, prmDic)
            BaseII.CreateMyParameter("@ANIO", SqlDbType.Int, prmAnio)
            BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, Me.GrupoComboBox.SelectedValue)
            BaseII.Inserta("UspGuardaMetasInd")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class