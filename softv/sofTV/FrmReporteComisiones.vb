﻿Imports CrystalDecisions.CrystalReports.Engine
Imports System.Collections.Generic

Public Class FrmReporteComisiones

#Region "Eventos"
    Private Sub FrmReporteComisiones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Label1.ForeColor = Color.Black
        Label2.ForeColor = Color.Black
        Label3.ForeColor = Color.Black
        UspMuestraTipoComisiones()
        UspMesesComisiones()
        MuestraAniosComisiones()
        Me.RBRangoPrimer.Checked = True
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub CMBTipo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBTipo.SelectedIndexChanged
        UspVendedoresComisiones()
    End Sub
#End Region

#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabCtrl = New TabControl
                tabCtrl = ctl
                bwrTabControlPerfilDesactiva(tabCtrl, formulario, NomInterno)
                tabCtrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabpg = New TabPage
                tabpg = ctl
                bwrTabPagePerfilDesactiva(tabpg, formulario, NomInterno)
                tabpg = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabCtrl = New TabControl
                tabCtrl = ctl
                bwrTabControlPerfilDesactiva(tabCtrl, formulario, NomInterno)
                tabCtrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabpg = New TabPage
                tabpg = ctl
                bwrTabPagePerfilDesactiva(tabpg, formulario, NomInterno)
                tabpg = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabCtrl = New TabControl
                tabCtrl = ctl
                bwrTabControlPerfilDesactiva(tabCtrl, formulario, NomInterno)
                tabCtrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabpg = New TabPage
                tabpg = ctl
                bwrTabPagePerfilDesactiva(tabpg, formulario, NomInterno)
                tabpg = Nothing
            End If

        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabCtrl = New TabControl
                tabCtrl = ctl
                bwrTabControlPerfilDesactiva(tabCtrl, formulario, NomInterno)
                tabCtrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabpg = New TabPage
                tabpg = ctl
                bwrTabPagePerfilDesactiva(tabpg, formulario, NomInterno)
                tabpg = Nothing
            End If
        Next
    End Sub

    Public Sub bwrTabControlPerfilDesactiva(ByVal tabctrl1 As TabControl, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage

        For Each ctl As Control In tabctrl1.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabCtrl = New TabControl
                tabCtrl = ctl
                bwrTabControlPerfilDesactiva(tabCtrl, formulario, NomInterno)
                tabCtrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabpg = New TabPage
                tabpg = ctl
                bwrTabPagePerfilDesactiva(tabpg, formulario, NomInterno)
                tabpg = Nothing
            End If
        Next
    End Sub

    Public Sub bwrTabPagePerfilDesactiva(ByVal tabPG1 As TabPage, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage

        For Each ctl As Control In tabPG1.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabCtrl = New TabControl
                tabCtrl = ctl
                bwrTabControlPerfilDesactiva(tabCtrl, formulario, NomInterno)
                tabCtrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabpg = New TabPage
                tabpg = ctl
                bwrTabPagePerfilDesactiva(tabpg, formulario, NomInterno)
                tabpg = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region

#Region "Salir"
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
#End Region

#Region "FUNCIONES"
    Private Sub UspMuestraTipoComisiones()
        Try
            BaseII.limpiaParametros()
            CMBTipo.DataSource = BaseII.ConsultaDT("UspMuestraTipoComisiones")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub MuestraAniosComisiones()
        Try
            BaseII.limpiaParametros()
            CMBAnio.DataSource = BaseII.ConsultaDT("MuestraAniosComisiones")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMesesComisiones()
        Try
          
            BaseII.limpiaParametros()

            CMBMes.DataSource = BaseII.ConsultaDT("UspMesesComisiones")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspVendedoresComisiones()
        Try
            If Len(CMBTipo.Text) > 0 Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, Me.CMBTipo.SelectedValue, 1)
                CMBVendedores.DataSource = BaseII.ConsultaDT("UspVendedoresComisiones")
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function UspReporteComisones() As DataSet
        Try
            Dim Rango As Integer
            If RBRangoPrimer.Checked = True Then
                Rango = 1
            ElseIf RBRangoSegundo.Checked = True Then
                Rango = 2
            End If

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, Me.CMBTipo.SelectedValue, 1)
            BaseII.CreateMyParameter("@CLVVENDEDOR", SqlDbType.Int, Me.CMBVendedores.SelectedValue)
            BaseII.CreateMyParameter("@RANGO", SqlDbType.Int, Rango)
            BaseII.CreateMyParameter("@MES", SqlDbType.Int, Me.CMBMes.SelectedValue)
            BaseII.CreateMyParameter("@ANIO", SqlDbType.Int, CInt(Me.CMBAnio.Text))

            Dim tableNameList As New List(Of String)
            tableNameList.Add("UspReporteVentasNuevas")
            tableNameList.Add("UspReportecomisionesNuevas")
            tableNameList.Add("UspReportecomisionesRecontratacion")
            tableNameList.Add("UspReportecomisionesAdicionales")
            tableNameList.Add("UspReportecomisionesUpselling")
            tableNameList.Add("UspReportecomisionesVCaidas")
            tableNameList.Add("TotalVentas")
            tableNameList.Add("UspReporteBonosVentas")
            tableNameList.Add("UspReporteBonosSupervisores")
            tableNameList.Add("UspReporteBonosProductividad")
            tableNameList.Add("UspReporteBonosCombinacionExacta")
            tableNameList.Add("TotalAPagar")
            tableNameList.Add("Clientes")

            Return BaseII.ConsultaDS("UspReporteComisones", tableNameList)
        Catch ex As Exception

        End Try
    End Function

#End Region




    Private Sub BtnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAceptar.Click
        Dim DS As New DataSet
        Dim rDocument As New ReportDocument
        Dim Dia, Mes As Integer
        DS = UspReporteComisones()

        rDocument.Load(RutaReportes + "\ReporteComisiones.rpt")
        rDocument.SetDataSource(DS)

        SetDBReport(DS, rDocument)
        Dim fechas As String = Nothing
        Mes = CMBMes.SelectedValue
        Select Case Mes
            Case 1, 3, 5, 7, 8, 10, 12
                Dia = 31
            Case 4, 6, 9, 11
                Dia = 30
            Case 2
                Dia = 28
        End Select

        If RBRangoPrimer.Checked = True Then
            fechas = "De la Fecha: 01 de " + LCase(CMBMes.Text) + " del " + CMBAnio.Text + " A la Fecha: 15 de " + LCase(CMBMes.Text) + " del " + CMBAnio.Text
        Else
            fechas = "De la Fecha: 16 de " + LCase(CMBMes.Text) + " del " + CMBAnio.Text + " A la Fecha: " + CStr(Dia) + " de " + LCase(CMBMes.Text) + " del " + CMBAnio.Text
        End If

        Dim DT As New DataTable
        Dim grupo As String

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLVVENDEDOR", SqlDbType.Int, Me.CMBVendedores.SelectedValue)
        BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, Me.CMBTipo.SelectedValue, 1)
        DT = BaseII.ConsultaDT("UspDameGrupoVentas")
        If DT.Rows.Count > 0 Then
            grupo = DT.Rows(0)(0).ToString
        End If

        Dim mySelectFormula As String = Nothing
        mySelectFormula = "Comisiones"
        rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
        rDocument.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        rDocument.DataDefinition.FormulaFields("SubTitulo").Text = "'" & fechas & "'"
        rDocument.DataDefinition.FormulaFields("Ciudades").Text = "'" & grupo & "'"
        rDocument.DataDefinition.FormulaFields("Grupoventas").Text = "'" & CMBTipo.Text & "'"
        rDocument.DataDefinition.FormulaFields("Vendedor").Text = "'" & CMBVendedores.Text & "'"
        'mySelectFormula = "Quejas " + Me.ComboBox1.Text
        'rDocument.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
        Dim Imprimir As New FrmImprimirCentralizada
        Imprimir.rd = rDocument
        Imprimir.ShowDialog()

        rDocument = Nothing
    End Sub
End Class