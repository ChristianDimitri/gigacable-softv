﻿Public Class frminformacionAgenda
    Dim bnd As Boolean
#Region "EVENTOS"
    Private Sub frminformacionAgenda_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.Label1.ForeColor = Color.Black
        Me.Label2.ForeColor = Color.Black
        Me.Label3.ForeColor = Color.Black
        Me.Label4.ForeColor = Color.Black
        Me.Label5.ForeColor = Color.Black
        Me.Label6.ForeColor = Color.Black
        Me.Label7.ForeColor = Color.Black
        Me.Label8.ForeColor = Color.Black
        Me.Label9.ForeColor = Color.Black
        Me.Label10.ForeColor = Color.Black
        Me.Label11.ForeColor = Color.Black
        bnd = False
        If GloTipo = "M" Then
            Me.Button2.Visible = True
            Me.DateTimePicker1.Enabled = True
            Me.DateTimePicker2.Enabled = True
            Me.TextBox1.Enabled = True
            Me.ComboBox2.Enabled = True
            Me.cmbHorario.Enabled = True
        ElseIf GloTipo = "C" Then
            Me.Button2.Visible = False
            Me.DateTimePicker1.Enabled = False
            Me.DateTimePicker2.Enabled = False
            Me.DateTimePicker3.Enabled = False
            Me.TextBox1.Enabled = True
            Me.TextBox1.ReadOnly = True
            Me.ComboBox2.Enabled = False
            Me.cmbHorario.Enabled = False
        ElseIf GloTipo = "O" Then
            Me.Button2.Visible = True
            Me.DateTimePicker1.Enabled = True
            Me.DateTimePicker2.Enabled = True
            Me.DateTimePicker3.Enabled = False
            Me.TextBox1.Enabled = True
            Me.ComboBox2.Enabled = True
            Me.cmbHorario.Enabled = True
            Me.cmbHorario.Enabled = False
        End If
        If GloBNDReporteNuevo = True Then
            GloBNDReporteNuevo = False
            Me.Label6.Visible = False
            Me.ComboBox2.Visible = False
        End If
        Me.txtNombreTecnico.Text = Nombretecnico
        Me.txtTipo.Text = Tipotecnico
        Me.txtClvOrdenQueja.Text = ClvOrdenQueja1
        UspLlenaComboMotivos()
        UspLlenaComboRangoHorario()
        UspConsultaInformacion()
        bnd = True
    End Sub
#End Region
#Region "METODOS"
    Private Sub UspConsultaInformacion()
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVORDEN", SqlDbType.BigInt, ClvOrdenQueja1)
            BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, Tipotecnico, 10)
            DT = BaseII.ConsultaDT("UspConsultaInformacion")
            If DT.Rows.Count > 0 Then
                Me.DateTimePicker1.Value = Date.Now.Date + " " + DT.Rows(0)(1).ToString
                Me.DateTimePicker2.Value = Date.Now.Date + " " + DT.Rows(0)(2).ToString
                Me.TextBox1.Text = DT.Rows(0)(3).ToString
                Me.ComboBox2.SelectedValue = CInt(DT.Rows(0)(4).ToString)
                Me.txtEjecutivo.Text = DT.Rows(0)(5).ToString
                Me.txtAgendo.Text = DT.Rows(0)(6).ToString
                Me.txtNombreTecnico.Text = DT.Rows(0)(7).ToString
                Me.DateTimePicker3.Value = DT.Rows(0)(8).ToString
                Me.cmbHorario.SelectedValue = DT.Rows(0)(0).ToString
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspLlenaComboMotivos()
        Try
            BaseII.limpiaParametros()
            ComboBox2.DataSource = BaseII.ConsultaDT("UspLlenaComboMotivos")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub UspLlenaComboRangoHorario()
        Try
            BaseII.limpiaParametros()
            cmbHorario.DataSource = BaseII.ConsultaDT("UspLlenaComboRangoHorario")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub UspGuardarInformacion()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@OBSERVACIONES", SqlDbType.VarChar, Me.TextBox1.Text, 1000)
            BaseII.CreateMyParameter("@HORAINICIAL", SqlDbType.DateTime, Me.DateTimePicker1.Value)
            BaseII.CreateMyParameter("@HORAFINAL", SqlDbType.DateTime, Me.DateTimePicker2.Value)
            BaseII.CreateMyParameter("@CLVMOTIVO", SqlDbType.Int, Me.ComboBox2.SelectedValue)
            BaseII.CreateMyParameter("@CLVORDEN", SqlDbType.Int, CInt(Me.txtClvOrdenQueja.Text))
            BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, Me.txtTipo.Text, 10)
            BaseII.CreateMyParameter("@FECHAPROGRAMADA", SqlDbType.DateTime, Me.DateTimePicker3.Value)
            BaseII.CreateMyParameter("@CLVHORARIO", SqlDbType.Int, CInt(Me.cmbHorario.SelectedValue))
            BaseII.Inserta("UspGuardarInformacion")
            MsgBox("Se guardo correctamente.", MsgBoxStyle.Information, "Información de la agenda")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub UspValidaHoraInicialFinal()
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVHORARIO", SqlDbType.Int, CInt(Me.cmbHorario.SelectedValue))
            DT = BaseII.ConsultaDT("UspValidaHoraInicialFinal")
            If DT.Rows.Count > 0 Then
                Me.DateTimePicker1.Value = Date.Now.Date + " " + DT.Rows(0)(0).ToString
                Me.DateTimePicker2.Value = Date.Now.Date + " " + DT.Rows(0)(1).ToString
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub UspLlenaComboRangoHorarioAsignacion()
        Try
            'cmbHorario.DataSource = Nothing
            'cmbHorario.Items.Clear()
            'cmbHorario.ValueMember = "ID"
            'cmbHorario.DisplayMember = "HORARIO"

            If Me.DateTimePicker3.Value < Date.Now.Date Then
                Me.DateTimePicker3.Value = Date.Now
            End If

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@FECHA", SqlDbType.DateTime, Me.DateTimePicker3.Value)
            cmbHorario.DataSource = BaseII.ConsultaDT("UspLlenaComboRangoHorarioAsignacion")
            If cmbHorario.SelectedValue = Nothing Then
                cmbHorario.Text = ""
            End If
            'cmbRangoHorario.SelectedIndex = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region
#Region "CONTROLES"
    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        If GloTipo <> "O" Then
            If cmbHorario.Text = "" Then
                MsgBox("Debe se seleccionar un rango de horario", MsgBoxStyle.Information, "Asignación del Técnico")
                Exit Sub
            End If
            If Me.DateTimePicker3.Value < Date.Now.Date Then
                MsgBox("La fecha de programación no puede ser menor a la fecha actual", MsgBoxStyle.Information, "Información de la agenda")
                Exit Sub
            End If
            If Me.DateTimePicker1.Value > Me.DateTimePicker2.Value Then
                MsgBox("La Hora inicial debe se menor a la hora final", MsgBoxStyle.Information, "Información de la agenda")
                Exit Sub
            End If
        End If
        UspGuardarInformacion()
        Me.Close()
    End Sub
    Private Sub DateTimePicker3_ValueChanged(sender As System.Object, e As System.EventArgs) Handles DateTimePicker3.ValueChanged
        If bnd = True Then
            If GloTipo = "M" Then
                UspLlenaComboRangoHorarioAsignacion()
            End If
        End If
        
    End Sub
    Private Sub cmbHorario_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cmbHorario.SelectedIndexChanged
        If bnd = True Then
            UspValidaHoraInicialFinal()
        End If
    End Sub
#End Region




  
   
End Class