﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMetasCombo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMetasCombo))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbGrupo = New System.Windows.Forms.ComboBox()
        Me.cbTipSer = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbAnio = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.panelMetasCombo = New System.Windows.Forms.Panel()
        Me.dgvMetasCombo = New System.Windows.Forms.DataGridView()
        Me.bnMetasCombo = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminar = New System.Windows.Forms.ToolStripButton()
        Me.tsbGuardar = New System.Windows.Forms.ToolStripButton()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.IdConcepto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_TipSer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Grupo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Concepto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ene = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Feb = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Mar = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Abr = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.May = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Jun = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Jul = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ago = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sep = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Oct = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nov = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Dic = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Anio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.panelMetasCombo.SuspendLayout()
        CType(Me.dgvMetasCombo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bnMetasCombo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnMetasCombo.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(41, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(117, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Grupo de Ventas:"
        '
        'cbGrupo
        '
        Me.cbGrupo.DisplayMember = "Grupo"
        Me.cbGrupo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbGrupo.FormattingEnabled = True
        Me.cbGrupo.Location = New System.Drawing.Point(164, 21)
        Me.cbGrupo.Name = "cbGrupo"
        Me.cbGrupo.Size = New System.Drawing.Size(318, 23)
        Me.cbGrupo.TabIndex = 0
        Me.cbGrupo.ValueMember = "Clv_Grupo"
        '
        'cbTipSer
        '
        Me.cbTipSer.DisplayMember = "Concepto"
        Me.cbTipSer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTipSer.FormattingEnabled = True
        Me.cbTipSer.Location = New System.Drawing.Point(164, 50)
        Me.cbTipSer.Name = "cbTipSer"
        Me.cbTipSer.Size = New System.Drawing.Size(318, 23)
        Me.cbTipSer.TabIndex = 1
        Me.cbTipSer.ValueMember = "Clv_TipSer"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(44, 58)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(114, 15)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Tipo de Servicio:"
        '
        'cbAnio
        '
        Me.cbAnio.DisplayMember = "ANIO"
        Me.cbAnio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAnio.FormattingEnabled = True
        Me.cbAnio.Location = New System.Drawing.Point(164, 79)
        Me.cbAnio.Name = "cbAnio"
        Me.cbAnio.Size = New System.Drawing.Size(174, 23)
        Me.cbAnio.TabIndex = 2
        Me.cbAnio.ValueMember = "ANIO"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(123, 87)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 15)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Año:"
        '
        'panelMetasCombo
        '
        Me.panelMetasCombo.Controls.Add(Me.dgvMetasCombo)
        Me.panelMetasCombo.Controls.Add(Me.bnMetasCombo)
        Me.panelMetasCombo.Location = New System.Drawing.Point(12, 127)
        Me.panelMetasCombo.Name = "panelMetasCombo"
        Me.panelMetasCombo.Size = New System.Drawing.Size(984, 524)
        Me.panelMetasCombo.TabIndex = 7
        '
        'dgvMetasCombo
        '
        Me.dgvMetasCombo.AllowUserToAddRows = False
        Me.dgvMetasCombo.AllowUserToDeleteRows = False
        Me.dgvMetasCombo.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvMetasCombo.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvMetasCombo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMetasCombo.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdConcepto, Me.Clv_TipSer, Me.Clv_Servicio, Me.Clv_Grupo, Me.Concepto, Me.Ene, Me.Feb, Me.Mar, Me.Abr, Me.May, Me.Jun, Me.Jul, Me.Ago, Me.Sep, Me.Oct, Me.Nov, Me.Dic, Me.Anio})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvMetasCombo.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvMetasCombo.Location = New System.Drawing.Point(3, 28)
        Me.dgvMetasCombo.Name = "dgvMetasCombo"
        Me.dgvMetasCombo.Size = New System.Drawing.Size(978, 493)
        Me.dgvMetasCombo.TabIndex = 7
        Me.dgvMetasCombo.TabStop = False
        '
        'bnMetasCombo
        '
        Me.bnMetasCombo.AddNewItem = Nothing
        Me.bnMetasCombo.CountItem = Nothing
        Me.bnMetasCombo.DeleteItem = Nothing
        Me.bnMetasCombo.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnMetasCombo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminar, Me.tsbGuardar})
        Me.bnMetasCombo.Location = New System.Drawing.Point(0, 0)
        Me.bnMetasCombo.MoveFirstItem = Nothing
        Me.bnMetasCombo.MoveLastItem = Nothing
        Me.bnMetasCombo.MoveNextItem = Nothing
        Me.bnMetasCombo.MovePreviousItem = Nothing
        Me.bnMetasCombo.Name = "bnMetasCombo"
        Me.bnMetasCombo.PositionItem = Nothing
        Me.bnMetasCombo.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnMetasCombo.Size = New System.Drawing.Size(984, 25)
        Me.bnMetasCombo.TabIndex = 0
        Me.bnMetasCombo.Text = "BindingNavigator1"
        '
        'tsbEliminar
        '
        Me.tsbEliminar.Image = CType(resources.GetObject("tsbEliminar.Image"), System.Drawing.Image)
        Me.tsbEliminar.Name = "tsbEliminar"
        Me.tsbEliminar.RightToLeftAutoMirrorImage = True
        Me.tsbEliminar.Size = New System.Drawing.Size(88, 22)
        Me.tsbEliminar.Text = "&ELIMINAR"
        '
        'tsbGuardar
        '
        Me.tsbGuardar.Image = CType(resources.GetObject("tsbGuardar.Image"), System.Drawing.Image)
        Me.tsbGuardar.Name = "tsbGuardar"
        Me.tsbGuardar.Size = New System.Drawing.Size(88, 22)
        Me.tsbGuardar.Text = "&GUARDAR"
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(860, 682)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 3
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'IdConcepto
        '
        Me.IdConcepto.DataPropertyName = "IdConcepto"
        Me.IdConcepto.HeaderText = "IdConcepto"
        Me.IdConcepto.Name = "IdConcepto"
        Me.IdConcepto.Visible = False
        '
        'Clv_TipSer
        '
        Me.Clv_TipSer.DataPropertyName = "Clv_TipSer"
        Me.Clv_TipSer.HeaderText = "Clv_TipSer"
        Me.Clv_TipSer.Name = "Clv_TipSer"
        Me.Clv_TipSer.Visible = False
        '
        'Clv_Servicio
        '
        Me.Clv_Servicio.DataPropertyName = "Clv_Servicio"
        Me.Clv_Servicio.HeaderText = "Clv_Servicio"
        Me.Clv_Servicio.Name = "Clv_Servicio"
        Me.Clv_Servicio.Visible = False
        '
        'Clv_Grupo
        '
        Me.Clv_Grupo.DataPropertyName = "Clv_Grupo"
        Me.Clv_Grupo.HeaderText = "Clv_Grupo"
        Me.Clv_Grupo.Name = "Clv_Grupo"
        Me.Clv_Grupo.Visible = False
        '
        'Concepto
        '
        Me.Concepto.DataPropertyName = "Concepto"
        Me.Concepto.HeaderText = "Concepto"
        Me.Concepto.Name = "Concepto"
        Me.Concepto.Width = 270
        '
        'Ene
        '
        Me.Ene.DataPropertyName = "Ene"
        Me.Ene.HeaderText = "Ene"
        Me.Ene.Name = "Ene"
        Me.Ene.Width = 50
        '
        'Feb
        '
        Me.Feb.DataPropertyName = "Feb"
        Me.Feb.HeaderText = "Feb"
        Me.Feb.Name = "Feb"
        Me.Feb.Width = 50
        '
        'Mar
        '
        Me.Mar.DataPropertyName = "Mar"
        Me.Mar.HeaderText = "Mar"
        Me.Mar.Name = "Mar"
        Me.Mar.Width = 50
        '
        'Abr
        '
        Me.Abr.DataPropertyName = "Abr"
        Me.Abr.HeaderText = "Abr"
        Me.Abr.Name = "Abr"
        Me.Abr.Width = 50
        '
        'May
        '
        Me.May.DataPropertyName = "May"
        Me.May.HeaderText = "May"
        Me.May.Name = "May"
        Me.May.Width = 50
        '
        'Jun
        '
        Me.Jun.DataPropertyName = "Jun"
        Me.Jun.HeaderText = "Jun"
        Me.Jun.Name = "Jun"
        Me.Jun.Width = 50
        '
        'Jul
        '
        Me.Jul.DataPropertyName = "Jul"
        Me.Jul.HeaderText = "Jul"
        Me.Jul.Name = "Jul"
        Me.Jul.Width = 50
        '
        'Ago
        '
        Me.Ago.DataPropertyName = "Ago"
        Me.Ago.HeaderText = "Ago"
        Me.Ago.Name = "Ago"
        Me.Ago.Width = 50
        '
        'Sep
        '
        Me.Sep.DataPropertyName = "Sep"
        Me.Sep.HeaderText = "Sep"
        Me.Sep.Name = "Sep"
        Me.Sep.Width = 50
        '
        'Oct
        '
        Me.Oct.DataPropertyName = "Oct"
        Me.Oct.HeaderText = "Oct"
        Me.Oct.Name = "Oct"
        Me.Oct.Width = 50
        '
        'Nov
        '
        Me.Nov.DataPropertyName = "Nov"
        Me.Nov.HeaderText = "Nov"
        Me.Nov.Name = "Nov"
        Me.Nov.Width = 50
        '
        'Dic
        '
        Me.Dic.DataPropertyName = "Dic"
        Me.Dic.HeaderText = "Dic"
        Me.Dic.Name = "Dic"
        Me.Dic.Width = 50
        '
        'Anio
        '
        Me.Anio.DataPropertyName = "Anio"
        Me.Anio.HeaderText = "Anio"
        Me.Anio.Name = "Anio"
        Me.Anio.Visible = False
        '
        'FrmMetasCombo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1008, 730)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.panelMetasCombo)
        Me.Controls.Add(Me.cbAnio)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cbTipSer)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbGrupo)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FrmMetasCombo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmMetasCombo"
        Me.panelMetasCombo.ResumeLayout(False)
        Me.panelMetasCombo.PerformLayout()
        CType(Me.dgvMetasCombo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bnMetasCombo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnMetasCombo.ResumeLayout(False)
        Me.bnMetasCombo.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbGrupo As System.Windows.Forms.ComboBox
    Friend WithEvents cbTipSer As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbAnio As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents panelMetasCombo As System.Windows.Forms.Panel
    Friend WithEvents bnMetasCombo As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents dgvMetasCombo As System.Windows.Forms.DataGridView
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents IdConcepto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_TipSer As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Grupo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Concepto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Ene As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Feb As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Mar As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Abr As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents May As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Jun As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Jul As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Ago As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sep As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Oct As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nov As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Dic As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Anio As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
