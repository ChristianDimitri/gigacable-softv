﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmNap
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ClaveLabel = New System.Windows.Forms.Label()
        Me.IngenieriaLabel = New System.Windows.Forms.Label()
        Me.NoCasasLabel = New System.Windows.Forms.Label()
        Me.NoNegociosLabel = New System.Windows.Forms.Label()
        Me.NoLotesLabel = New System.Windows.Forms.Label()
        Me.NoServiciosLabel = New System.Windows.Forms.Label()
        Me.FrenteANumeroLabel = New System.Windows.Forms.Label()
        Me.Clv_TxtLabel = New System.Windows.Forms.Label()
        Me.NombreLabel1 = New System.Windows.Forms.Label()
        Me.NOMBRELabel = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.gbClave = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ComboBoxPuerto = New System.Windows.Forms.ComboBox()
        Me.ComboBoxtarjetas = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbPoste = New System.Windows.Forms.ComboBox()
        Me.cbSector = New System.Windows.Forms.ComboBox()
        Me.Clv_SectorTextBox1 = New System.Windows.Forms.TextBox()
        Me.tbClave = New System.Windows.Forms.TextBox()
        Me.tbIngenieria = New System.Windows.Forms.TextBox()
        Me.tbSalidas = New System.Windows.Forms.TextBox()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.gbDetalleClave = New System.Windows.Forms.GroupBox()
        Me.cbCalle = New System.Windows.Forms.ComboBox()
        Me.Clv_CalleTextBox1 = New System.Windows.Forms.TextBox()
        Me.cbColonia = New System.Windows.Forms.ComboBox()
        Me.Clv_ColoniaTextBox1 = New System.Windows.Forms.TextBox()
        Me.tbNoNegocios = New System.Windows.Forms.TextBox()
        Me.tbFrenteANumero = New System.Windows.Forms.TextBox()
        Me.tbNoServicios = New System.Windows.Forms.TextBox()
        Me.tbNoLotes = New System.Windows.Forms.TextBox()
        Me.tbNoCasas = New System.Windows.Forms.TextBox()
        Me.Clv_CalleTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_ColoniaTextBox = New System.Windows.Forms.TextBox()
        Me.ConsecutivoTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_SectorTextBox = New System.Windows.Forms.TextBox()
        Me.ConsecutivoAux = New System.Windows.Forms.TextBox()
        Me.tsbGuardar = New System.Windows.Forms.ToolStripButton()
        Me.bnTap = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.gbClave.SuspendLayout()
        Me.gbDetalleClave.SuspendLayout()
        CType(Me.bnTap, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnTap.SuspendLayout()
        Me.SuspendLayout()
        '
        'ClaveLabel
        '
        Me.ClaveLabel.AutoSize = True
        Me.ClaveLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClaveLabel.Location = New System.Drawing.Point(39, 33)
        Me.ClaveLabel.Name = "ClaveLabel"
        Me.ClaveLabel.Size = New System.Drawing.Size(50, 15)
        Me.ClaveLabel.TabIndex = 2
        Me.ClaveLabel.Text = "Clave :"
        '
        'IngenieriaLabel
        '
        Me.IngenieriaLabel.AutoSize = True
        Me.IngenieriaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IngenieriaLabel.Location = New System.Drawing.Point(306, 122)
        Me.IngenieriaLabel.Name = "IngenieriaLabel"
        Me.IngenieriaLabel.Size = New System.Drawing.Size(148, 15)
        Me.IngenieriaLabel.TabIndex = 8
        Me.IngenieriaLabel.Text = "NAP / Número de NAP"
        '
        'NoCasasLabel
        '
        Me.NoCasasLabel.AutoSize = True
        Me.NoCasasLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NoCasasLabel.Location = New System.Drawing.Point(141, 177)
        Me.NoCasasLabel.Name = "NoCasasLabel"
        Me.NoCasasLabel.Size = New System.Drawing.Size(86, 15)
        Me.NoCasasLabel.TabIndex = 16
        Me.NoCasasLabel.Text = "# de Casas :"
        '
        'NoNegociosLabel
        '
        Me.NoNegociosLabel.AutoSize = True
        Me.NoNegociosLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NoNegociosLabel.Location = New System.Drawing.Point(118, 203)
        Me.NoNegociosLabel.Name = "NoNegociosLabel"
        Me.NoNegociosLabel.Size = New System.Drawing.Size(107, 15)
        Me.NoNegociosLabel.TabIndex = 18
        Me.NoNegociosLabel.Text = "# de Negocios :"
        '
        'NoLotesLabel
        '
        Me.NoLotesLabel.AutoSize = True
        Me.NoLotesLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NoLotesLabel.Location = New System.Drawing.Point(147, 229)
        Me.NoLotesLabel.Name = "NoLotesLabel"
        Me.NoLotesLabel.Size = New System.Drawing.Size(82, 15)
        Me.NoLotesLabel.TabIndex = 20
        Me.NoLotesLabel.Text = "# de Lotes :"
        '
        'NoServiciosLabel
        '
        Me.NoServiciosLabel.AutoSize = True
        Me.NoServiciosLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NoServiciosLabel.Location = New System.Drawing.Point(120, 255)
        Me.NoServiciosLabel.Name = "NoServiciosLabel"
        Me.NoServiciosLabel.Size = New System.Drawing.Size(105, 15)
        Me.NoServiciosLabel.TabIndex = 22
        Me.NoServiciosLabel.Text = "# de Servicios :"
        '
        'FrenteANumeroLabel
        '
        Me.FrenteANumeroLabel.AutoSize = True
        Me.FrenteANumeroLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FrenteANumeroLabel.Location = New System.Drawing.Point(100, 281)
        Me.FrenteANumeroLabel.Name = "FrenteANumeroLabel"
        Me.FrenteANumeroLabel.Size = New System.Drawing.Size(127, 15)
        Me.FrenteANumeroLabel.TabIndex = 24
        Me.FrenteANumeroLabel.Text = "Frente al Número :"
        '
        'Clv_TxtLabel
        '
        Me.Clv_TxtLabel.AutoSize = True
        Me.Clv_TxtLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TxtLabel.Location = New System.Drawing.Point(18, 69)
        Me.Clv_TxtLabel.Name = "Clv_TxtLabel"
        Me.Clv_TxtLabel.Size = New System.Drawing.Size(40, 15)
        Me.Clv_TxtLabel.TabIndex = 11
        Me.Clv_TxtLabel.Text = "HUB:"
        '
        'NombreLabel1
        '
        Me.NombreLabel1.AutoSize = True
        Me.NombreLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreLabel1.Location = New System.Drawing.Point(95, 34)
        Me.NombreLabel1.Name = "NombreLabel1"
        Me.NombreLabel1.Size = New System.Drawing.Size(81, 15)
        Me.NombreLabel1.TabIndex = 26
        Me.NombreLabel1.Text = "Colonia(s) :"
        '
        'NOMBRELabel
        '
        Me.NOMBRELabel.AutoSize = True
        Me.NOMBRELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRELabel.Location = New System.Drawing.Point(95, 94)
        Me.NOMBRELabel.Name = "NOMBRELabel"
        Me.NOMBRELabel.Size = New System.Drawing.Size(65, 15)
        Me.NOMBRELabel.TabIndex = 28
        Me.NOMBRELabel.Text = "Calle(s) :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(232, 68)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(37, 15)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "OLT:"
        '
        'gbClave
        '
        Me.gbClave.Controls.Add(Me.Label2)
        Me.gbClave.Controls.Add(Me.ComboBoxPuerto)
        Me.gbClave.Controls.Add(Me.ComboBoxtarjetas)
        Me.gbClave.Controls.Add(Me.Label3)
        Me.gbClave.Controls.Add(Me.Label1)
        Me.gbClave.Controls.Add(Me.cbPoste)
        Me.gbClave.Controls.Add(Me.cbSector)
        Me.gbClave.Controls.Add(Me.Clv_SectorTextBox1)
        Me.gbClave.Controls.Add(Me.Clv_TxtLabel)
        Me.gbClave.Controls.Add(Me.tbClave)
        Me.gbClave.Controls.Add(Me.ClaveLabel)
        Me.gbClave.Controls.Add(Me.IngenieriaLabel)
        Me.gbClave.Controls.Add(Me.tbIngenieria)
        Me.gbClave.Controls.Add(Me.tbSalidas)
        Me.gbClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbClave.Location = New System.Drawing.Point(12, 46)
        Me.gbClave.Name = "gbClave"
        Me.gbClave.Size = New System.Drawing.Size(506, 213)
        Me.gbClave.TabIndex = 0
        Me.gbClave.TabStop = False
        Me.gbClave.Text = "Clave Técnica"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(194, 122)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 15)
        Me.Label2.TabIndex = 21
        Me.Label2.Text = "Puerto:"
        '
        'ComboBoxPuerto
        '
        Me.ComboBoxPuerto.DisplayMember = "puerto"
        Me.ComboBoxPuerto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxPuerto.FormattingEnabled = True
        Me.ComboBoxPuerto.Location = New System.Drawing.Point(197, 141)
        Me.ComboBoxPuerto.Name = "ComboBoxPuerto"
        Me.ComboBoxPuerto.Size = New System.Drawing.Size(81, 23)
        Me.ComboBoxPuerto.TabIndex = 20
        Me.ComboBoxPuerto.ValueMember = "valor"
        '
        'ComboBoxtarjetas
        '
        Me.ComboBoxtarjetas.DisplayMember = "Descripcion"
        Me.ComboBoxtarjetas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxtarjetas.FormattingEnabled = True
        Me.ComboBoxtarjetas.Location = New System.Drawing.Point(7, 141)
        Me.ComboBoxtarjetas.Name = "ComboBoxtarjetas"
        Me.ComboBoxtarjetas.Size = New System.Drawing.Size(170, 23)
        Me.ComboBoxtarjetas.TabIndex = 18
        Me.ComboBoxtarjetas.ValueMember = "CLAVE"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(4, 122)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 15)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "Tarjeta:"
        '
        'cbPoste
        '
        Me.cbPoste.DisplayMember = "Descripcion"
        Me.cbPoste.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPoste.FormattingEnabled = True
        Me.cbPoste.Location = New System.Drawing.Point(235, 86)
        Me.cbPoste.Name = "cbPoste"
        Me.cbPoste.Size = New System.Drawing.Size(208, 23)
        Me.cbPoste.TabIndex = 16
        Me.cbPoste.ValueMember = "Id"
        '
        'cbSector
        '
        Me.cbSector.DisplayMember = "Descripcion"
        Me.cbSector.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSector.FormattingEnabled = True
        Me.cbSector.Location = New System.Drawing.Point(21, 88)
        Me.cbSector.Name = "cbSector"
        Me.cbSector.Size = New System.Drawing.Size(170, 23)
        Me.cbSector.TabIndex = 0
        Me.cbSector.ValueMember = "Clv_Sector"
        '
        'Clv_SectorTextBox1
        '
        Me.Clv_SectorTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_SectorTextBox1.Location = New System.Drawing.Point(116, 88)
        Me.Clv_SectorTextBox1.Name = "Clv_SectorTextBox1"
        Me.Clv_SectorTextBox1.ReadOnly = True
        Me.Clv_SectorTextBox1.Size = New System.Drawing.Size(10, 21)
        Me.Clv_SectorTextBox1.TabIndex = 13
        Me.Clv_SectorTextBox1.TabStop = False
        '
        'tbClave
        '
        Me.tbClave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbClave.Location = New System.Drawing.Point(116, 31)
        Me.tbClave.Name = "tbClave"
        Me.tbClave.ReadOnly = True
        Me.tbClave.Size = New System.Drawing.Size(325, 21)
        Me.tbClave.TabIndex = 3
        Me.tbClave.TabStop = False
        Me.tbClave.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tbIngenieria
        '
        Me.tbIngenieria.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbIngenieria.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbIngenieria.Location = New System.Drawing.Point(309, 141)
        Me.tbIngenieria.Name = "tbIngenieria"
        Me.tbIngenieria.Size = New System.Drawing.Size(51, 21)
        Me.tbIngenieria.TabIndex = 1
        '
        'tbSalidas
        '
        Me.tbSalidas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbSalidas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSalidas.Location = New System.Drawing.Point(389, 140)
        Me.tbSalidas.Name = "tbSalidas"
        Me.tbSalidas.Size = New System.Drawing.Size(61, 21)
        Me.tbSalidas.TabIndex = 2
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(382, 612)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 10
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'gbDetalleClave
        '
        Me.gbDetalleClave.Controls.Add(Me.cbCalle)
        Me.gbDetalleClave.Controls.Add(Me.Clv_CalleTextBox1)
        Me.gbDetalleClave.Controls.Add(Me.NOMBRELabel)
        Me.gbDetalleClave.Controls.Add(Me.cbColonia)
        Me.gbDetalleClave.Controls.Add(Me.Clv_ColoniaTextBox1)
        Me.gbDetalleClave.Controls.Add(Me.NombreLabel1)
        Me.gbDetalleClave.Controls.Add(Me.tbNoNegocios)
        Me.gbDetalleClave.Controls.Add(Me.tbFrenteANumero)
        Me.gbDetalleClave.Controls.Add(Me.FrenteANumeroLabel)
        Me.gbDetalleClave.Controls.Add(Me.tbNoServicios)
        Me.gbDetalleClave.Controls.Add(Me.NoServiciosLabel)
        Me.gbDetalleClave.Controls.Add(Me.tbNoLotes)
        Me.gbDetalleClave.Controls.Add(Me.NoLotesLabel)
        Me.gbDetalleClave.Controls.Add(Me.NoNegociosLabel)
        Me.gbDetalleClave.Controls.Add(Me.tbNoCasas)
        Me.gbDetalleClave.Controls.Add(Me.NoCasasLabel)
        Me.gbDetalleClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDetalleClave.Location = New System.Drawing.Point(12, 284)
        Me.gbDetalleClave.Name = "gbDetalleClave"
        Me.gbDetalleClave.Size = New System.Drawing.Size(506, 322)
        Me.gbDetalleClave.TabIndex = 3
        Me.gbDetalleClave.TabStop = False
        Me.gbDetalleClave.Text = "Datos del NAP:"
        '
        'cbCalle
        '
        Me.cbCalle.DisplayMember = "Nombre"
        Me.cbCalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCalle.FormattingEnabled = True
        Me.cbCalle.Location = New System.Drawing.Point(98, 113)
        Me.cbCalle.Name = "cbCalle"
        Me.cbCalle.Size = New System.Drawing.Size(324, 23)
        Me.cbCalle.TabIndex = 4
        Me.cbCalle.ValueMember = "Clv_Calle"
        '
        'Clv_CalleTextBox1
        '
        Me.Clv_CalleTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_CalleTextBox1.Location = New System.Drawing.Point(389, 115)
        Me.Clv_CalleTextBox1.Name = "Clv_CalleTextBox1"
        Me.Clv_CalleTextBox1.ReadOnly = True
        Me.Clv_CalleTextBox1.Size = New System.Drawing.Size(10, 21)
        Me.Clv_CalleTextBox1.TabIndex = 30
        Me.Clv_CalleTextBox1.TabStop = False
        '
        'cbColonia
        '
        Me.cbColonia.DisplayMember = "Nombre"
        Me.cbColonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbColonia.FormattingEnabled = True
        Me.cbColonia.Location = New System.Drawing.Point(100, 53)
        Me.cbColonia.Name = "cbColonia"
        Me.cbColonia.Size = New System.Drawing.Size(322, 23)
        Me.cbColonia.TabIndex = 3
        Me.cbColonia.ValueMember = "Clv_Colonia"
        '
        'Clv_ColoniaTextBox1
        '
        Me.Clv_ColoniaTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_ColoniaTextBox1.Location = New System.Drawing.Point(330, 53)
        Me.Clv_ColoniaTextBox1.Name = "Clv_ColoniaTextBox1"
        Me.Clv_ColoniaTextBox1.ReadOnly = True
        Me.Clv_ColoniaTextBox1.Size = New System.Drawing.Size(10, 21)
        Me.Clv_ColoniaTextBox1.TabIndex = 28
        Me.Clv_ColoniaTextBox1.TabStop = False
        '
        'tbNoNegocios
        '
        Me.tbNoNegocios.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNoNegocios.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNoNegocios.Location = New System.Drawing.Point(251, 197)
        Me.tbNoNegocios.Name = "tbNoNegocios"
        Me.tbNoNegocios.Size = New System.Drawing.Size(100, 21)
        Me.tbNoNegocios.TabIndex = 6
        '
        'tbFrenteANumero
        '
        Me.tbFrenteANumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbFrenteANumero.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbFrenteANumero.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbFrenteANumero.Location = New System.Drawing.Point(251, 275)
        Me.tbFrenteANumero.Name = "tbFrenteANumero"
        Me.tbFrenteANumero.Size = New System.Drawing.Size(100, 21)
        Me.tbFrenteANumero.TabIndex = 9
        '
        'tbNoServicios
        '
        Me.tbNoServicios.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNoServicios.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNoServicios.Location = New System.Drawing.Point(251, 249)
        Me.tbNoServicios.Name = "tbNoServicios"
        Me.tbNoServicios.Size = New System.Drawing.Size(100, 21)
        Me.tbNoServicios.TabIndex = 8
        '
        'tbNoLotes
        '
        Me.tbNoLotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNoLotes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNoLotes.Location = New System.Drawing.Point(251, 223)
        Me.tbNoLotes.Name = "tbNoLotes"
        Me.tbNoLotes.Size = New System.Drawing.Size(100, 21)
        Me.tbNoLotes.TabIndex = 7
        '
        'tbNoCasas
        '
        Me.tbNoCasas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNoCasas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNoCasas.Location = New System.Drawing.Point(251, 171)
        Me.tbNoCasas.Name = "tbNoCasas"
        Me.tbNoCasas.Size = New System.Drawing.Size(100, 21)
        Me.tbNoCasas.TabIndex = 5
        '
        'Clv_CalleTextBox
        '
        Me.Clv_CalleTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_CalleTextBox.Location = New System.Drawing.Point(618, 662)
        Me.Clv_CalleTextBox.Name = "Clv_CalleTextBox"
        Me.Clv_CalleTextBox.ReadOnly = True
        Me.Clv_CalleTextBox.Size = New System.Drawing.Size(10, 22)
        Me.Clv_CalleTextBox.TabIndex = 15
        Me.Clv_CalleTextBox.TabStop = False
        '
        'Clv_ColoniaTextBox
        '
        Me.Clv_ColoniaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_ColoniaTextBox.Location = New System.Drawing.Point(602, 662)
        Me.Clv_ColoniaTextBox.Name = "Clv_ColoniaTextBox"
        Me.Clv_ColoniaTextBox.ReadOnly = True
        Me.Clv_ColoniaTextBox.Size = New System.Drawing.Size(10, 22)
        Me.Clv_ColoniaTextBox.TabIndex = 13
        Me.Clv_ColoniaTextBox.TabStop = False
        '
        'ConsecutivoTextBox
        '
        Me.ConsecutivoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConsecutivoTextBox.Location = New System.Drawing.Point(554, 662)
        Me.ConsecutivoTextBox.Name = "ConsecutivoTextBox"
        Me.ConsecutivoTextBox.ReadOnly = True
        Me.ConsecutivoTextBox.Size = New System.Drawing.Size(10, 22)
        Me.ConsecutivoTextBox.TabIndex = 7
        Me.ConsecutivoTextBox.TabStop = False
        '
        'Clv_SectorTextBox
        '
        Me.Clv_SectorTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_SectorTextBox.Location = New System.Drawing.Point(586, 662)
        Me.Clv_SectorTextBox.Name = "Clv_SectorTextBox"
        Me.Clv_SectorTextBox.ReadOnly = True
        Me.Clv_SectorTextBox.Size = New System.Drawing.Size(10, 22)
        Me.Clv_SectorTextBox.TabIndex = 5
        Me.Clv_SectorTextBox.TabStop = False
        '
        'ConsecutivoAux
        '
        Me.ConsecutivoAux.Location = New System.Drawing.Point(570, 662)
        Me.ConsecutivoAux.Name = "ConsecutivoAux"
        Me.ConsecutivoAux.ReadOnly = True
        Me.ConsecutivoAux.Size = New System.Drawing.Size(10, 20)
        Me.ConsecutivoAux.TabIndex = 35
        Me.ConsecutivoAux.TabStop = False
        '
        'tsbGuardar
        '
        Me.tsbGuardar.Name = "tsbGuardar"
        Me.tsbGuardar.Size = New System.Drawing.Size(75, 22)
        Me.tsbGuardar.Text = "&GUARDAR"
        '
        'bnTap
        '
        Me.bnTap.AddNewItem = Nothing
        Me.bnTap.CountItem = Nothing
        Me.bnTap.DeleteItem = Nothing
        Me.bnTap.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnTap.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbGuardar})
        Me.bnTap.Location = New System.Drawing.Point(0, 0)
        Me.bnTap.MoveFirstItem = Nothing
        Me.bnTap.MoveLastItem = Nothing
        Me.bnTap.MoveNextItem = Nothing
        Me.bnTap.MovePreviousItem = Nothing
        Me.bnTap.Name = "bnTap"
        Me.bnTap.PositionItem = Nothing
        Me.bnTap.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnTap.Size = New System.Drawing.Size(542, 25)
        Me.bnTap.TabIndex = 1
        Me.bnTap.TabStop = True
        Me.bnTap.Text = "BindingNavigator1"
        '
        'FrmNap
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(542, 672)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.ConsecutivoAux)
        Me.Controls.Add(Me.bnTap)
        Me.Controls.Add(Me.gbDetalleClave)
        Me.Controls.Add(Me.gbClave)
        Me.Controls.Add(Me.Clv_SectorTextBox)
        Me.Controls.Add(Me.Clv_ColoniaTextBox)
        Me.Controls.Add(Me.Clv_CalleTextBox)
        Me.Controls.Add(Me.ConsecutivoTextBox)
        Me.Name = "FrmNap"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NAP"
        Me.gbClave.ResumeLayout(False)
        Me.gbClave.PerformLayout()
        Me.gbDetalleClave.ResumeLayout(False)
        Me.gbDetalleClave.PerformLayout()
        CType(Me.bnTap, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnTap.ResumeLayout(False)
        Me.bnTap.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbClave As System.Windows.Forms.GroupBox
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents gbDetalleClave As System.Windows.Forms.GroupBox
    Friend WithEvents tbClave As System.Windows.Forms.TextBox
    Friend WithEvents tbIngenieria As System.Windows.Forms.TextBox
    Friend WithEvents tbSalidas As System.Windows.Forms.TextBox
    Friend WithEvents tbNoNegocios As System.Windows.Forms.TextBox
    Friend WithEvents tbFrenteANumero As System.Windows.Forms.TextBox
    Friend WithEvents tbNoServicios As System.Windows.Forms.TextBox
    Friend WithEvents tbNoLotes As System.Windows.Forms.TextBox
    Friend WithEvents tbNoCasas As System.Windows.Forms.TextBox
    Friend WithEvents Clv_CalleTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ColoniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ConsecutivoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_SectorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents cbSector As System.Windows.Forms.ComboBox
    Friend WithEvents cbColonia As System.Windows.Forms.ComboBox
    Friend WithEvents ConsecutivoAux As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ColoniaTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Clv_SectorTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents cbCalle As System.Windows.Forms.ComboBox
    Friend WithEvents Clv_CalleTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents cbPoste As System.Windows.Forms.ComboBox
    Friend WithEvents tsbGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnTap As System.Windows.Forms.BindingNavigator
    Friend WithEvents ClaveLabel As System.Windows.Forms.Label
    Friend WithEvents IngenieriaLabel As System.Windows.Forms.Label
    Friend WithEvents NoCasasLabel As System.Windows.Forms.Label
    Friend WithEvents NoNegociosLabel As System.Windows.Forms.Label
    Friend WithEvents NoLotesLabel As System.Windows.Forms.Label
    Friend WithEvents NoServiciosLabel As System.Windows.Forms.Label
    Friend WithEvents FrenteANumeroLabel As System.Windows.Forms.Label
    Friend WithEvents Clv_TxtLabel As System.Windows.Forms.Label
    Friend WithEvents NombreLabel1 As System.Windows.Forms.Label
    Friend WithEvents NOMBRELabel As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxPuerto As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxtarjetas As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
