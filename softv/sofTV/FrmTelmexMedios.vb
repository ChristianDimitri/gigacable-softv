﻿Public Class FrmTelmexMedios

   



    Private Sub FrmTelmexMedios_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        If opcion = "N" Or opcion = "M" Then
            GuardarBtn.Enabled = True
            DataGridView1.ReadOnly = False
            DataGridView1.Enabled = True

        ElseIf opcion = "C" Then
            GuardarBtn.Enabled = False
            DataGridView1.ReadOnly = True
            DataGridView1.Enabled = False

        End If


        'Revisamos si existe alguna relación del servicio con el medio
        Dim bnd As Boolean = False
        Dim tiposuscVAR As Integer = 0

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, GloClv_Servicio)
        BaseII.CreateMyParameter("@bndSiOnoTiene", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.CreateMyParameter("@tipoSuscriptor", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("DameTipoSuscriptorCon_Telmex")
        bnd = BaseII.dicoPar("@bndSiOnoTiene")
        tiposuscVAR = BaseII.dicoPar("@tipoSuscriptor")

        If bnd = True Then
            llenarGridNuevo(1, tiposuscVAR) ' 
        Else
            llenarGridNuevo(0, 0) 'opc, tipoSuscriptor
        End If
        llenarComboTipo(tiposuscVAR)

    End Sub

    Private Sub llenarComboTipo(ByVal tiposuscVAR As Integer)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, 0) 'Todas las marcas
        ComboBox1.DataSource = BaseII.ConsultaDT("MUESTRA_TIPOCLIENTES")
        If tiposuscVAR > 0 Then
            ComboBox1.SelectedValue = tiposuscVAR
        End If

    End Sub


    Private Sub llenarGridNuevo(ByVal opc As Integer, ByVal tipoSuscriptor As Integer)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_Servicio", SqlDbType.BigInt, GloClv_Servicio)
        BaseII.CreateMyParameter("@opc", SqlDbType.Int, opc)
        BaseII.CreateMyParameter("@tipoSuscriptor", SqlDbType.Int, tipoSuscriptor)
        Dim dt As DataTable
        dt = BaseII.ConsultaDT("Dame_RelClvServicioMedio_Telmex")
        Dim dr As DataRow
        DataGridView1.Rows.Clear()

        For Each dr In dt.Rows

            DataGridView1.Rows.Add()
            DataGridView1("colIdMedio", DataGridView1.Rows.Count - 1).Value = dr("IdMedio").ToString()
            DataGridView1("colNombre", DataGridView1.Rows.Count - 1).Value = dr("Nombre").ToString()
            ' gridMediosPrecios("colChecked", gridMediosPrecios.Rows.Count - 1).Value = True
            DataGridView1("colPrecio", DataGridView1.Rows.Count - 1).Value = dr("Precio")
            DataGridView1("colChecked", DataGridView1.Rows.Count - 1).Value = dr("Checked")
            DataGridView1("colTipoSuscriptor", DataGridView1.Rows.Count - 1).Value = dr("TipoSuscriptor")
        Next


    End Sub



    Private Sub GuardarGrid()

        For i As Integer = 0 To DataGridView1.Rows.Count() - 1
            Dim c As Boolean
            c = DataGridView1.Rows(i).Cells("colChecked").Value
            ' if the checkbox cell is checked
            'MessageBox.Show(DataGridView1.Rows(i).Cells("colPrecio").Value)
            'If c = True Then
            '    MessageBox.Show("Checked")
            '    ' if not
            'Else
            '    MessageBox.Show("Not Checked")
            'End If
        Next

        'Revisamos si es un servicio nuevo.

        If opcion = "N" Then

            BaseII.limpiaParametros()
            ' Dim clvSessionParaNuevo As Long
            BaseII.CreateMyParameter("@ClvSessionTempo", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.ProcedimientoOutPut("Add_RelServicioMedio_Telmex_CreaSession")
            ClvSessionTelmexTempo = BaseII.dicoPar("@ClvSessionTempo")


            For Each row As DataGridViewRow In DataGridView1.Rows

                Dim idMedio = row.Cells("colIdMedio").Value
                Dim descripcionMedio = row.Cells("colNombre").Value
                Dim precio = row.Cells("colPrecio").Value

                Dim checked As Boolean
                checked = row.Cells("colChecked").Value

                BaseII.limpiaParametros()
                Dim checarGloClv_Servicio = GloClv_Servicio
                'BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, GloClv_Servicio)
                BaseII.CreateMyParameter("@idmedio", SqlDbType.Int, idMedio)
                BaseII.CreateMyParameter("@precio", SqlDbType.Money, precio)
                BaseII.CreateMyParameter("@TipoSucriptor", SqlDbType.Int, ComboBox1.SelectedValue())
                BaseII.CreateMyParameter("@checked", SqlDbType.Bit, 1)
                BaseII.CreateMyParameter("@ClvSessionTempo", SqlDbType.BigInt, ClvSessionTelmexTempo)
                BaseII.Inserta("Add_RelServicioMedio_Telmex_SrvNuevo1")

                ' BaseII.ProcedimientoOutPut("Add_RelServicioMedio_Telmex_SrvNuevo1")

                'ClvSessionTelmexTempo = BaseII.dicoPar("@ClvSessionTempo").ToString
                'MessageBox.Show("Cambios guardados con ClvSessionTempo " + ClvSessionTelmexTempo.ToString())

                'BaseII.limpiaParametros()
                'BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, 9999)
                'BaseII.CreateMyParameter("@idmedio", SqlDbType.Int, 1)
                'BaseII.CreateMyParameter("@precio", SqlDbType.Int, 20)
                'BaseII.CreateMyParameter("@Select", SqlDbType.Bit, 1)
                'BaseII.CreateMyParameter("@TipoSucriptor", SqlDbType.Int, 1)
                'BaseII.Inserta("AddRelClvServicioMedioTelmex")

            Next

        ElseIf opcion = "C" Or opcion = "M" Then


            For Each row As DataGridViewRow In DataGridView1.Rows

                Dim idMedio = row.Cells("colIdMedio").Value
                Dim descripcionMedio = row.Cells("colNombre").Value
                Dim precio = row.Cells("colPrecio").Value

                Dim checked As Boolean
                checked = row.Cells("colChecked").Value

                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, GloClv_Servicio)
                BaseII.CreateMyParameter("@idmedio", SqlDbType.Int, idMedio)
                BaseII.CreateMyParameter("@precio", SqlDbType.Money, precio)
                BaseII.CreateMyParameter("@TipoSucriptor", SqlDbType.Int, ComboBox1.SelectedValue())
                BaseII.CreateMyParameter("@checked", SqlDbType.Bit, checked)
                BaseII.Inserta("UPDATE_RelClvServicioMedioTelmex")  'guardaModoDistribuidor_PROCDEMUESTRA

            Next


            MessageBox.Show("Cambios guardados")
            'llenarGridNuevo()

        End If

    End Sub


    Private Sub GuardarBtn_Click(sender As Object, e As EventArgs) Handles GuardarBtn.Click

        If ComboBox1.SelectedValue = 0 Then
            MsgBox("Seleccione un tipo de suscriptor")
        Else
            GuardarGrid()
            Me.Close()
        End If


        'For i As Integer = 0 To DataGridView1.Rows.Count() - 1
        '    Dim c As Boolean
        '    c = DataGridView1.Rows(i).Cells("colChecked").Value
        '    ' if the checkbox cell is checked

        '    MessageBox.Show(DataGridView1.Rows(i).Cells("colPrecio").Value)

        '    If c = True Then
        '        MessageBox.Show("Checked")
        '        ' if not
        '    Else
        '        MessageBox.Show("Not Checked")
        '    End If
        'Next
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged, ComboBox1.SelectedIndexChanged


        Dim elTipoCliente As Integer = ComboBox1.SelectedValue()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_Servicio", SqlDbType.BigInt, GloClv_Servicio)
        BaseII.CreateMyParameter("@opc", SqlDbType.Int, 1)
        BaseII.CreateMyParameter("@tipoSuscriptor", SqlDbType.Int, elTipoCliente)
        Dim dt As DataTable
        dt = BaseII.ConsultaDT("Dame_RelClvServicioMedio_Telmex")
        Dim dr As DataRow
        DataGridView1.Rows.Clear()

        For Each dr In dt.Rows

            DataGridView1.Rows.Add()
            DataGridView1("colIdMedio", DataGridView1.Rows.Count - 1).Value = dr("IdMedio").ToString()
            DataGridView1("colNombre", DataGridView1.Rows.Count - 1).Value = dr("Nombre").ToString()
            ' gridMediosPrecios("colChecked", gridMediosPrecios.Rows.Count - 1).Value = True
            DataGridView1("colPrecio", DataGridView1.Rows.Count - 1).Value = dr("Precio")
            DataGridView1("colChecked", DataGridView1.Rows.Count - 1).Value = dr("Checked")
            DataGridView1("colTipoSuscriptor", DataGridView1.Rows.Count - 1).Value = dr("TipoSuscriptor")
        Next

    End Sub

End Class