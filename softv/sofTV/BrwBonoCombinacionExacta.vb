﻿Public Class BrwBonoCombinacionExacta

#Region "Eventos"
    Private Sub BrwBonoCombinacionExacta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspLlenaNivelUsuario()
        UspLlenaGrupo()
        UspBuscaBonoCombinacionExacta(0, 0, 99, 0)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub txtClave_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtClave.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
        If KeyAscii = 13 Then
            Dim clave As Integer
            clave = 0
            If Len(txtClave.Text) > 0 Then
                clave = CInt(txtClave.Text)
            End If

            UspBuscaBonoCombinacionExacta(clave, Me.cmbGrupo.SelectedValue, Me.cmbNivelUsuario.SelectedValue, 0)
            Me.txtClave.Text = ""
            Me.cmbGrupo.SelectedValue = 0
            Me.cmbNivelUsuario.SelectedValue = 99
        End If
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        If DataGridView1.SelectedRows.Count > 0 Then
            Me.lblClave.Text = DataGridView1.SelectedCells(0).Value
            Me.lblGrupo.Text = DataGridView1.SelectedCells(1).Value
            Me.lblNivelUsuario.Text = DataGridView1.SelectedCells(2).Value
            Me.lblBono.Text = DataGridView1.SelectedCells(3).Value
            Me.lblFechaIni.Text = DataGridView1.SelectedCells(5).Value
            Me.lblFechaFin.Text = DataGridView1.SelectedCells(6).Value
            If DataGridView1.SelectedCells(4).Value = "1" Then
                Me.lblVigente.Text = "Vigente"
            Else
                Me.lblVigente.Text = "No Vigente"
            End If

        Else
            Me.lblClave.Text = ""

            Me.lblGrupo.Text = ""
            Me.lblNivelUsuario.Text = ""
            Me.lblBono.Text = ""
            Me.lblVigente.Text = ""
            Me.lblFechaIni.Text = ""
            Me.lblFechaFin.Text = ""
        End If
    End Sub

    Private Sub BrwCalculoBonos_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        UspBuscaBonoCombinacionExacta(0, 0, 99, 1)
    End Sub
#End Region

#Region "Funciones"

    Private Sub UspLlenaNivelUsuario()
        Try
            BaseII.limpiaParametros()
            cmbNivelUsuario.DataSource = BaseII.ConsultaDT("UspLlenaNivelUsuario")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub UspLlenaGrupo()
        Try
            BaseII.limpiaParametros()
            cmbGrupo.DataSource = BaseII.ConsultaDT("UspLlenaGrupo")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function

    Private Sub UspBuscaBonoCombinacionExacta(ByVal prmClave As Integer, ByVal prmClvGrupo As Integer, ByVal prmClvUsuario As Integer, ByVal prmvigente As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVBONO", SqlDbType.Int, prmClave)
            BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, prmClvGrupo)
            BaseII.CreateMyParameter("@ESSUPERVISOR", SqlDbType.Int, prmClvUsuario)
            BaseII.CreateMyParameter("@VIGENTE", SqlDbType.Int, prmvigente)
            DataGridView1.DataSource = BaseII.ConsultaDT("UspBuscaBonoCombinacionExacta")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Nuevo"
    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        opcion = "N"
        GloClaveCalculoBono = 0
        FrmBonoCombinacionExacta.Show()
    End Sub
#End Region

#Region "Consultar"
    Private Sub BtnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnConsultar.Click
        If DataGridView1.Rows.Count > 0 Then
            opcion = "C"
            GloClaveCalculoBono = DataGridView1.SelectedCells(0).Value
            FrmBonoCombinacionExacta.Show()
        End If

    End Sub
#End Region

#Region "Modificar"
    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        If DataGridView1.Rows.Count > 0 Then
            opcion = "M"
            GloClaveCalculoBono = DataGridView1.SelectedCells(0).Value
            FrmBonoCombinacionExacta.Show()
        End If

    End Sub
#End Region

#Region "Salir"
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
#End Region

#Region "Buscar"
    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Dim ClvBono As Integer
        If Len(Me.txtClave.Text) = 0 Then
            ClvBono = 0
        Else
            ClvBono = txtClave.Text
        End If

        UspBuscaBonoCombinacionExacta(ClvBono, cmbGrupo.SelectedValue, Me.cmbNivelUsuario.SelectedValue, 0)

        Me.txtClave.Text = ""
        Me.cmbNivelUsuario.SelectedIndex = 0
        Me.cmbGrupo.SelectedIndex = 0
    End Sub
#End Region


End Class