﻿Public Class frmMotivosInstalacionFT

    Dim LocTipo As String
    Dim LocClvMotivo As Integer
    Dim BND As Integer

#Region "METODOS"
    Private Sub UspLlenaMotivosInstalacionFT(ByVal PRMID As Integer, ByVal PRMDESCRIPCION As String)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ID", SqlDbType.Int, PRMID)
            BaseII.CreateMyParameter("@DESCRIPCION", SqlDbType.VarChar, PRMDESCRIPCION, 250)
            Me.dgMotivos.DataSource = BaseII.ConsultaDT("UspLlenaMotivosInstalacionFT")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspGuardaMotivoInstalacionFT(ByVal prmID As Integer)
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ID", SqlDbType.Int, prmID)
            BaseII.CreateMyParameter("@DESCRIPCION", SqlDbType.VarChar, Me.txtDescripcion.Text, 250)
            DT = BaseII.ConsultaDT("UspGuardaMotivoInstalacionFT")
            If DT.Rows.Count > 0 Then
                BND = CInt(DT.Rows(0)(0).ToString)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function UspMuestraDescripcionMotivoIntalacionFT() As DataTable
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ID", SqlDbType.Int, LocClvMotivo)
            UspMuestraDescripcionMotivoIntalacionFT = BaseII.ConsultaDT("UspMuestraDescripcionMotivoIntalacionFT")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function
#End Region

#Region "CONTROLES"

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        gbDatosMotivo.Visible = False
    End Sub

    Private Sub btnBuscarMotivo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarMotivo.Click
        Dim ClaveMotivo As Integer
        If Len(txtBuscarClave.Text) = 0 Then
            ClaveMotivo = 0
        Else
            ClaveMotivo = CInt(txtBuscarClave.Text)
        End If
        UspLlenaMotivosInstalacionFT(ClaveMotivo, Me.txtBuscarDescripcion.Text)
        Me.txtBuscarDescripcion.Text = ""
        Me.txtBuscarClave.Text = ""
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        Me.gbDatosMotivo.Visible = True
        Me.txtDescripcion.Text = ""
        LocTipo = "N"
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If LocTipo = "N" Then
            UspGuardaMotivoInstalacionFT(0)
            If BND = 1 Then
                MsgBox("El Motivo Ya existe, favor de cambiarlo.", MsgBoxStyle.Information, "Motivos de Pendientes")
                Exit Sub
            End If
        ElseIf LocTipo = "M" Then
            UspGuardaMotivoInstalacionFT(LocClvMotivo)
            If BND = 1 Then
                MsgBox("El Motivo Ya existe, favor de cambiarlo.", MsgBoxStyle.Information, "Motivos de Pendientes")
                Exit Sub
            End If
        End If
        MsgBox("Fue Guardado Con Exito.", MsgBoxStyle.Information, "Motivos de Pendientes")
        Me.gbDatosMotivo.Visible = False
        UspLlenaMotivosInstalacionFT(0, String.Empty)
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        If Me.dgMotivos.SelectedCells.Count > 0 Then
            LocClvMotivo = CInt(Me.dgMotivos.SelectedCells(0).Value)
            Me.gbDatosMotivo.Visible = True
            Me.txtDescripcion.Text = UspMuestraDescripcionMotivoIntalacionFT.Rows(0)(0).ToString
            LocTipo = "M"
        Else
            MsgBox("Seleccione un Motivo", MsgBoxStyle.Information, "Motivos de Pendientes")
        End If
    End Sub
#End Region

#Region "EVENTOS"

    Private Sub frmMotivosInstalacionFT_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        LocTipo = ""
        LocClvMotivo = 0
        Me.Panel1.BackColor = Me.BackColor
        Me.Label1.BackColor = Me.BackColor
        Me.Label2.BackColor = Me.BackColor
        Me.Label3.BackColor = Me.BackColor
        Me.Label4.BackColor = Me.BackColor
        Me.gbDatosMotivo.BackColor = Me.BackColor


        If gbDatosMotivo.Visible = True Then
            Me.gbDatosMotivo.Visible = False
        End If

        UspLlenaMotivosInstalacionFT(0, String.Empty)
    End Sub

    Private Sub txtBuscarClave_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBuscarClave.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub
#End Region
End Class