﻿Public Class FrmCalculoBonos
    Dim MSG As String = Nothing
#Region "Eventos"
    Private Sub FrmCalculoBonos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Label1.ForeColor = Color.Black
        Label2.ForeColor = Color.Black
        Label3.ForeColor = Color.Black
        Label4.ForeColor = Color.Black
        Label5.ForeColor = Color.Black
        Label6.ForeColor = Color.Black
        UspLlenaNivelUsuario()
        UspLlenaServicios()
        UspLlenaGrupo()
        If opcion = "N" Then
            Me.cmbGrupoVentas.SelectedValue = 0
            Me.cmbNivelUsuario.SelectedValue = 99
            Me.cmbServicio.SelectedValue = 0
            CheckBox1.Enabled = True
        ElseIf opcion = "C" Then
            Me.cmbGrupoVentas.Enabled = False
            Me.cmbNivelUsuario.Enabled = False
            Me.cmbServicio.Enabled = False
            txtBono.Enabled = False
            txtPorcentajeFin.Enabled = False
            txtPorcentajeIni.Enabled = False
            btnAgregar.Enabled = False
            btnBorrar.Enabled = False
            btnGuardar.Enabled = False
            btnModificar.Enabled = False
            dgCalculoBonos.Enabled = False
            DateTimePicker1.Enabled = False
            DateTimePicker2.Enabled = False
            CheckBox1.Enabled = False

            UspMuestraCalculoBono(GloClaveCalculoBono)
        ElseIf opcion = "M" Then
            UspMuestraCalculoBono(GloClaveCalculoBono)
            Me.cmbGrupoVentas.Enabled = False
            Me.cmbNivelUsuario.Enabled = False
            Me.cmbServicio.Enabled = False
            DateTimePicker1.Enabled = False
            DateTimePicker2.Enabled = False
            CheckBox1.Enabled = True
            If Me.dgCalculoBonos.Rows.Count > 0 Then
                Me.btnBorrar.Enabled = True
                Me.btnGuardar.Enabled = False
                Me.btnModificar.Enabled = True
                Me.btnAgregar.Enabled = True
            Else
                Me.btnBorrar.Enabled = False
                Me.btnGuardar.Enabled = False
                Me.btnModificar.Enabled = False
                Me.btnAgregar.Enabled = True
            End If
        End If
    End Sub

    Private Sub txtPorcentajeIni_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPorcentajeIni.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtPorcentajeFin_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPorcentajeFin.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtBono_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBono.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        DateTimePicker2.MinDate = DateTimePicker1.Value
    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        DateTimePicker1.MaxDate = DateTimePicker2.Value
    End Sub

#End Region

#Region "Agregar"
    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        If cmbServicio.SelectedValue = 0 Then
            MsgBox("No tiene seleccionado ningun paquete")
            Exit Sub
        End If
        If cmbNivelUsuario.SelectedValue = 99 Then
            MsgBox("No tiene seleccionado ningun Nivel de Usuario")
            Exit Sub
        End If
        If cmbGrupoVentas.SelectedValue = 0 Then
            MsgBox("No tiene seleccionado ningun Grupo de Ventas")
            Exit Sub
        End If
        If Len(txtPorcentajeFin.Text) = 0 Then
            MsgBox("El Porcentaje Final esta en blanco, Favor de verificar el dato")
            Exit Sub
        End If
        If Len(txtPorcentajeIni.Text) = 0 Then
            MsgBox("El Porcentaje Inicial esta en blanco, Favor de verificar el dato")
            Exit Sub
        End If
        If Len(txtBono.Text) = 0 Then
            MsgBox("El Bono esta en blanco, Favor de verificar el dato")
            Exit Sub
        End If
        If CInt(txtPorcentajeFin.Text) < CInt(txtPorcentajeIni.Text) Then
            MsgBox("El Porcentaje Inicial debe de ser menos al Porcentaje Final")
            Exit Sub
        End If

        Dim EstaVigente As Integer
        If Me.CheckBox1.Checked = True Then
            EstaVigente = 1
        Else
            EstaVigente = 0
        End If

        UspAgregaBono(Me.cmbServicio.SelectedValue, CInt(Me.txtPorcentajeIni.Text), CInt(Me.txtPorcentajeFin.Text), CDec(Me.txtBono.Text), Me.cmbNivelUsuario.SelectedValue, Me.cmbGrupoVentas.SelectedValue, Me.DateTimePicker1.Value, Me.DateTimePicker2.Value, EstaVigente)
        Me.btnModificar.Enabled = True
        Me.btnGuardar.Enabled = False
        UspMuestraCalculoBono(GloClaveCalculoBono)
        txtPorcentajeIni.Text = ""
        txtPorcentajeFin.Text = ""
        txtBono.Text = ""
        Me.cmbGrupoVentas.Enabled = False
        Me.cmbNivelUsuario.Enabled = False
        Me.cmbServicio.Enabled = False
        DateTimePicker1.Enabled = False
        DateTimePicker2.Enabled = False
        CheckBox1.Enabled = False
    End Sub
#End Region

#Region "Modificar"
    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        If Me.dgCalculoBonos.SelectedRows.Count > 0 Then
            GloClaveCalculoBono = dgCalculoBonos.SelectedCells(0).Value
            txtPorcentajeIni.Text = dgCalculoBonos.SelectedCells(4).Value
            txtPorcentajeFin.Text = dgCalculoBonos.SelectedCells(5).Value
            txtBono.Text = dgCalculoBonos.SelectedCells(6).Value
            txtPorcentajeIni.Text = Replace(txtPorcentajeIni.Text, "%", "")
            txtPorcentajeFin.Text = Replace(txtPorcentajeFin.Text, "%", "")
            Me.btnAgregar.Enabled = False
            Me.btnModificar.Enabled = False
            Me.btnGuardar.Enabled = True
            DateTimePicker1.Enabled = True
            DateTimePicker2.Enabled = True
            CheckBox1.Enabled = True
        End If
    End Sub
#End Region

#Region "Borrar"
    Private Sub btnBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBorrar.Click
        If Me.dgCalculoBonos.SelectedRows.Count > 0 Then
            GloClaveCalculoBono = dgCalculoBonos.SelectedCells(0).Value
            UspBorraCalculoBono(GloClaveCalculoBono)
            UspMuestraCalculoBono(GloClaveCalculoBono)
        End If
    End Sub
#End Region


#Region "Salir"
    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
#End Region

#Region "Funciones"
    Private Sub UspAgregaBono(ByVal prmClvServicio As Integer, ByVal prmPorcentajeIni As Integer, ByVal prmPorcentajeFin As Integer, ByVal prmBono As Decimal, ByVal prmEsSupervisor As Integer, ByVal prmClvGrupo As Integer, ByVal prmFechaIni As Date, ByVal prmFechaFin As Date, ByVal prmVigente As Integer)
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSERVICIO", SqlDbType.Int, prmClvServicio)
            BaseII.CreateMyParameter("@PORCENTAJEINI", SqlDbType.Int, prmPorcentajeIni)
            BaseII.CreateMyParameter("@PORCENTAJEFIN", SqlDbType.Int, prmPorcentajeFin)
            BaseII.CreateMyParameter("@BONO", SqlDbType.Decimal, prmBono)
            BaseII.CreateMyParameter("@ESSUPERVISOR", SqlDbType.Int, prmEsSupervisor)
            BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, prmClvGrupo)
            BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, prmFechaIni)
            BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, prmFechaFin)
            BaseII.CreateMyParameter("@VIGENTE", SqlDbType.Int, prmVigente)

            DT = BaseII.ConsultaDT("UspAgregaBono")
            MSG = DT.Rows(0)(0).ToString
            If DT.Rows(0)(1).ToString <> "0" Then
                GloClaveCalculoBono = DT.Rows(0)(1).ToString
            End If
            MsgBox(MSG)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspGuardarBono(ByVal prmClvBono As Integer, ByVal prmClvServicio As Integer, ByVal prmPorcentajeIni As Integer, ByVal prmPorcentajeFin As Integer, ByVal prmBono As Decimal, ByVal prmEsSupervisor As Integer, ByVal prmClvGrupo As Integer, ByVal prmFechaIni As Date, ByVal prmFechaFin As Date, ByVal prmVigente As Integer)
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVBONO", SqlDbType.Int, prmClvBono)
            BaseII.CreateMyParameter("@CLVSERVICIO", SqlDbType.Int, prmClvServicio)
            BaseII.CreateMyParameter("@PORCENTAJEINI", SqlDbType.Int, prmPorcentajeIni)
            BaseII.CreateMyParameter("@PORCENTAJEFIN", SqlDbType.Int, prmPorcentajeFin)
            BaseII.CreateMyParameter("@BONO", SqlDbType.Decimal, prmBono)
            BaseII.CreateMyParameter("@ESSUPERVISOR", SqlDbType.Int, prmEsSupervisor)
            BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, prmClvGrupo)
            BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, prmFechaIni)
            BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, prmFechaFin)
            BaseII.CreateMyParameter("@VIGENTE", SqlDbType.Int, prmVigente)
            DT = BaseII.ConsultaDT("UspGuardarBono")
            MSG = DT.Rows(0)(0).ToString
            MsgBox(MSG)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890.", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function

    Private Sub UspLlenaNivelUsuario()
        Try
            BaseII.limpiaParametros()
            cmbNivelUsuario.DataSource = BaseII.ConsultaDT("UspLlenaNivelUsuario")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspLlenaServicios()
        Try
            BaseII.limpiaParametros()
            cmbServicio.DataSource = BaseII.ConsultaDT("UspLlenaServicios")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub UspLlenaGrupo()
        Try
            BaseII.limpiaParametros()
            cmbGrupoVentas.DataSource = BaseII.ConsultaDT("UspLlenaGrupo")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMuestraCalculoBono(ByVal prmClave As Integer)
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVCALCULOBONO", SqlDbType.Int, prmClave)
            dgCalculoBonos.DataSource = BaseII.ConsultaDT("UspMuestraCalculoBono")
            If dgCalculoBonos.Rows.Count > 0 Then
                Me.cmbServicio.SelectedValue = CInt(Me.dgCalculoBonos(1, 0).Value)
                Me.cmbNivelUsuario.SelectedValue = CInt(dgCalculoBonos(3, 0).Value)
                Me.cmbGrupoVentas.SelectedValue = CInt(dgCalculoBonos(2, 0).Value)
                Me.DateTimePicker1.Value = dgCalculoBonos(7, 0).Value
                Me.DateTimePicker2.Value = dgCalculoBonos(8, 0).Value
                Me.CheckBox1.Checked = dgCalculoBonos(9, 0).Value
            End If
           
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspBorraCalculoBono(ByVal prmClave As Integer)
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVCALCULOBONO", SqlDbType.Int, prmClave)
            DT = BaseII.ConsultaDT("UspBorraCalculoBono")
            GloClaveCalculoBono = DT.Rows(0)(0).ToString()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Guardar"
    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If cmbServicio.SelectedValue = 0 Then
            MsgBox("No tiene seleccionado ningun paquete")
            Exit Sub
        End If
        If cmbNivelUsuario.SelectedValue = 99 Then
            MsgBox("No tiene seleccionado ningun Nivel de Usuario")
            Exit Sub
        End If
        If cmbGrupoVentas.SelectedValue = 0 Then
            MsgBox("No tiene seleccionado ningun Grupo de Ventas")
            Exit Sub
        End If
        If Len(txtPorcentajeFin.Text) = 0 Then
            MsgBox("El Porcentaje Final esta en blanco, Favor de verificar el dato")
            Exit Sub
        End If
        If Len(txtPorcentajeIni.Text) = 0 Then
            MsgBox("El Porcentaje Inicial esta en blanco, Favor de verificar el dato")
            Exit Sub
        End If
        If Len(txtBono.Text) = 0 Then
            MsgBox("El Bono esta en blanco, Favor de verificar el dato")
            Exit Sub
        End If
        If CInt(txtPorcentajeFin.Text) < CInt(txtPorcentajeIni.Text) Then
            MsgBox("El Porcentaje Inicial debe de ser menos al Porcentaje Final")
            Exit Sub
        End If
        Dim EstaVigente As Integer
        If Me.CheckBox1.Checked = True Then
            EstaVigente = 1
        Else
            EstaVigente = 0
        End If
        UspGuardarBono(GloClaveCalculoBono, Me.cmbServicio.SelectedValue, CInt(Me.txtPorcentajeIni.Text), CInt(Me.txtPorcentajeFin.Text), CDec(Me.txtBono.Text), Me.cmbNivelUsuario.SelectedValue, Me.cmbGrupoVentas.SelectedValue, Me.DateTimePicker1.Value, Me.DateTimePicker2.Value, EstaVigente)
        UspMuestraCalculoBono(GloClaveCalculoBono)
        btnGuardar.Enabled = False
        btnAgregar.Enabled = True
        btnModificar.Enabled = True
        txtPorcentajeFin.Text = ""
        txtPorcentajeIni.Text = ""
        txtBono.Text = ""
        DateTimePicker1.Enabled = False
        DateTimePicker2.Enabled = False
        CheckBox1.Enabled = False
    End Sub
#End Region

  
   
  
  
End Class