﻿Imports System.Data.SqlClient
Imports System.Text

Public Class FrmSelServicios

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

        Try
            If IsNumeric(ComboBox1.SelectedValue) = True Then
                GloGuardarNet = False
                If GloClv_TipSer <> 5 Then
                    GloClv_Servicio = ComboBox1.SelectedValue
                    locGloClv_servicioPromocion = GloClv_Servicio
                Else
                    GloClv_servTel = Me.ComboBox1.SelectedValue
                    locGloClv_servicioPromocion = GloClv_servTel
                End If

                'If UCase(Pantalla) = UCase("FrmClientes") Then
                '    If GloClv_Servicio > 0 Then
                '        Me.Validate()
                '        Me.ConsultaclientesnetTableAdapter1.Insert(Contrato, "C", 0, 0, False, False, 0, "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", 0, "", False, LoContratonet)
                '        Me.ConsultaclientesnetTableAdapter1.Update(Me.NewSofTvDataSet.CONSULTACLIENTESNET)
                '        Me.ConsultacontnetTableAdapter1.Insert(LoContratonet, GloClv_Servicio, "C", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", True, 0, 0, False, "C", "", True, "", 0, 0, "", "", LoClv_Unicanet)
                '        Me.ConsultacontnetTableAdapter1.Update(Me.NewSofTvDataSet.CONSULTACONTNET)
                '        FrmClientes.creaarbol()
                '        FrmClientes.Panel5.Visible = False
                '        FrmClientes.Panel6.Visible = True
                '        FrmClientes.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(LoContratonet, Long))
                '        FrmClientes.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(LoContratonet, Long))
                '        FrmClientes.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(LoClv_Unicanet, Long)))
                '        FrmClientes.CREAARBOL()

                '        GloClv_Servicio = 0
                '    End If
                If GloClv_TipSer = 2 Then
                    If Equip_tel = True Then
                        numtel = True
                    End If
                    GloBndSer = True
                ElseIf GloClv_TipSer = 5 Then
                    globndTel = True
                End If
                Me.Close()
                frmctr.Activar()
            Else
                MsgBox("No a Seleccionado un Servicio", MsgBoxStyle.Information)
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        GloClv_Servicio = 0
        GloClv_servTel = 0
        Me.Close()
    End Sub

    Private Sub FrmSelServicios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        locGloClv_servicioPromocion = 0
        frmctr.MdiParent = FrmClientes
        Try

            If bndInstalaraOnoTelmex = False Then   'Servicios que no son  de telmex

                'Dim CON As New SqlConnection(MiConexion)
                'CON.Open()
                'Me.MuestraServiciosTableAdapter.Connection = CON
                'Me.MuestraServiciosTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServicios, New System.Nullable(Of Integer)(CType(GloClv_TipSer, Long)))
                'CON.Close()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_tipser", SqlDbType.BigInt, GloClv_TipSer)
                BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.BigInt, FrmClientes.Clv_ColoniaTextBox.Text)
                BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.BigInt, FrmClientes.Clv_CiudadTextBox.Text)
                ComboBox1.DataSource = BaseII.ConsultaDT("MuestraServiciosPorColonia")
                ComboBox1.ValueMember = "Clv_Servicio"
                ComboBox1.DisplayMember = "Descripcion"
            Else



                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, Contrato)
                BaseII.CreateMyParameter("@tipoClienteCobro", ParameterDirection.Output, SqlDbType.BigInt)
                BaseII.ProcedimientoOutPut("Dime_tipoCliente_ByContratoTelmex")
                Dim tipoClienteCobro As Integer = BaseII.dicoPar("@tipoClienteCobro").ToString


                Dim conexion As New SqlConnection(MiConexion)
                Dim dataTable As New DataTable
                Dim dataAdapter As New SqlDataAdapter

                Dim comando2 As New SqlCommand("MuestraServicios_Telmex", conexion)
                comando2.CommandType = CommandType.StoredProcedure

                comando2.Parameters.AddWithValue("@Clv_TipSer", GloClv_TipSer)
                comando2.Parameters("@Clv_TipSer").Direction = ParameterDirection.Input
                comando2.Parameters.AddWithValue("@Contrato", Contrato)
                comando2.Parameters("@Contrato").Direction = ParameterDirection.Input
                comando2.Parameters.AddWithValue("@TipoSucriptor", tipoClienteCobro)
                comando2.Parameters("@TipoSucriptor").Direction = ParameterDirection.Input
                comando2.Parameters.AddWithValue("@TipoInstalacion", buscarServicio_ConidMedioTelmex)
                comando2.Parameters("@TipoInstalacion").Direction = ParameterDirection.Input

                dataAdapter = New SqlDataAdapter(comando2)

                Dim bindingSource As New BindingSource

                Try
                    conexion.Open()

                    dataAdapter.Fill(dataTable)
                    bindingSource.DataSource = dataTable
                    'Me.DataGridView1.DataSource = bindingSource
                    Me.ComboBox1.DataSource = bindingSource

                    'Me.CONTRATOLabel1.Text = CStr(Folio)
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conexion.Close()
                    conexion.Dispose()
                End Try

                bndInstalaraOnoTelmex = False

            End If
            '


        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class