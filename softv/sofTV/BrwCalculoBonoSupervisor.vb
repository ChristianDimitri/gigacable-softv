﻿Public Class BrwCalculoBonoSupervisor

#Region "Eventos"

    Private Sub BrwCalculoBonos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspLlenaServicios()
        UspLlenaGrupo()
        UspBuscarCalculo(0, 0, 0, 1)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub txtClave_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtClave.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        Dim VIGENTE As Integer
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
        If KeyAscii = 13 Then
            Dim clave As Integer
            clave = 0
            If Len(txtClave.Text) > 0 Then
                clave = CInt(txtClave.Text)
            End If
            If CheckBox1.Checked = True Then
                VIGENTE = 1
            Else
                VIGENTE = 0
            End If

            UspBuscarCalculo(clave, Me.cmbGrupo.SelectedValue, Me.cmbPaquete.SelectedValue, VIGENTE)
            Me.txtClave.Text = ""
            Me.cmbGrupo.SelectedValue = 0
            Me.cmbPaquete.SelectedValue = 0
        End If
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        If DataGridView1.SelectedRows.Count > 0 Then
            Me.lblClave.Text = DataGridView1.SelectedCells(0).Value
            Me.lblServicioPrincipal.Text = DataGridView1.SelectedCells(1).Value
            Me.lblGrupo.Text = DataGridView1.SelectedCells(2).Value
            Me.lblPorcentajeIni.Text = DataGridView1.SelectedCells(3).Value
            Me.lblPorcentajeFin.Text = DataGridView1.SelectedCells(4).Value
            Me.lblBono.Text = DataGridView1.SelectedCells(5).Value
            Me.lblFechaIni.Text = DataGridView1.SelectedCells(7).Value
            Me.lblFechaFin.Text = DataGridView1.SelectedCells(8).Value
            If DataGridView1.SelectedCells(6).Value = "1" Then
                Me.lblVigente.Text = "Vigente"
            Else
                Me.lblVigente.Text = "No Vigente"
            End If

        Else
            Me.lblClave.Text = ""
            Me.lblServicioPrincipal.Text = ""
            Me.lblGrupo.Text = ""
            Me.lblPorcentajeIni.Text = ""
            Me.lblPorcentajeFin.Text = ""
            Me.lblBono.Text = ""
            Me.lblVigente.Text = ""
            Me.lblFechaIni.Text = ""
            Me.lblFechaFin.Text = ""
        End If
    End Sub

    Private Sub BrwCalculoBonos_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        UspBuscarCalculo(0, 0, 0, 1)
    End Sub
#End Region

#Region "Funciones"


    Private Sub UspLlenaServicios()
        Try
            BaseII.limpiaParametros()
            cmbPaquete.DataSource = BaseII.ConsultaDT("UspLlenaServiciosSupervisor")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub UspLlenaGrupo()
        Try
            BaseII.limpiaParametros()
            cmbGrupo.DataSource = BaseII.ConsultaDT("UspLlenaGrupo")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function

    Private Sub UspBuscarCalculo(ByVal prmClave As Integer, ByVal prmClvGrupo As Integer, ByVal prmClvPaquete As Integer, ByVal prmvigente As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLAVE", SqlDbType.Int, prmClave)
            BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, prmClvGrupo)
            BaseII.CreateMyParameter("@CLVPAQUETE", SqlDbType.Int, prmClvPaquete)
            BaseII.CreateMyParameter("@VIGENTE", SqlDbType.Int, prmvigente)
            DataGridView1.DataSource = BaseII.ConsultaDT("UspBuscarCalculoSupervisor")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Salir"

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

#End Region

#Region "Buscar"

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Dim clave As Integer
        Dim VIGENTE As Integer
        clave = 0
        If Len(txtClave.Text) > 0 Then
            clave = CInt(txtClave.Text)
        End If
        If CheckBox1.Checked = True Then
            VIGENTE = 1
        Else
            VIGENTE = 0
        End If
        UspBuscarCalculo(clave, Me.cmbGrupo.SelectedValue, Me.cmbPaquete.SelectedValue, VIGENTE)
        Me.txtClave.Text = ""
        Me.cmbGrupo.SelectedValue = 0
        Me.cmbPaquete.SelectedValue = 0
    End Sub

#End Region

#Region "Nuevo"

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        opcion = "N"
        FrmCalculoBonoSupervisor.Show()
    End Sub

#End Region

#Region "Modificar"
    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        opcion = "M"
        GloClaveCalculoBono = DataGridView1.SelectedCells(0).Value
        FrmCalculoBonoSupervisor.Show()
    End Sub
#End Region

#Region "Consultar"
    Private Sub BtnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnConsultar.Click
        opcion = "C"
        GloClaveCalculoBono = DataGridView1.SelectedCells(0).Value
        FrmCalculoBonoSupervisor.Show()
    End Sub
#End Region



#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region

End Class