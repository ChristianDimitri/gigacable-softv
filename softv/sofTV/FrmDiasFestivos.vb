﻿Imports System.Data.SqlClient

Public Class FrmDiasFestivos
    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Dim fechaTemp As Date, descripcion As String, res As String
        fechaTemp = Me.DateTimePicker.Value
        descripcion = txtDescripcion.Text
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("uspGuardaFestivos", con)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Fecha", SqlDbType.DateTime)
        parametro.Value = fechaTemp
        cmd.Parameters.Add(parametro)
        Dim parametro1 As New SqlParameter("@Descripcion", SqlDbType.VarChar)
        parametro1.Value = descripcion
        cmd.Parameters.Add(parametro1)
        Dim parametro3 As New SqlParameter("@Res", SqlDbType.VarChar, 200)
        parametro3.Direction = ParameterDirection.Output
        ' parametro3.Value = 0
        cmd.Parameters.Add(parametro3)
        Try
            con.Open()
            cmd.ExecuteNonQuery()
            res = parametro3.Value
        Catch ex As Exception
            Throw ex
        Finally
            con.Close()
            con.Dispose()
            MsgBox(res)
        End Try

        txtDescripcion.Text = ""
        DateTimePicker.Value = Today
    End Sub

    Private Sub FrmDiasFestivos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        txtDescripcion.Text = ""
        DateTimePicker.Value = Today
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Dim fecha As Date
        fecha = Me.DateTimePicker.Value
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Fecha", SqlDbType.DateTime, fecha)
        BaseII.Inserta("uspEliminaDiasFestivos")
        MsgBox("Se elimino con éxito")
    End Sub

    Private Sub btnReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReporte.Click
        BaseII.limpiaParametros()
        eOpVentas = 500
        FrmImprimirComision.ShowDialog()
        eOpVentas = 0
    End Sub
End Class