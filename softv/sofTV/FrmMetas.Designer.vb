<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMetas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GrupoLabel As System.Windows.Forms.Label
        Dim ConceptoLabel As System.Windows.Forms.Label
        Dim AnioLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMetas))
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.ConMetasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConMetasTableAdapter = New sofTV.DataSetEric2TableAdapters.ConMetasTableAdapter()
        Me.ConMetasDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ConGrupoVentasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConGrupoVentasTableAdapter = New sofTV.DataSetEric2TableAdapters.ConGrupoVentasTableAdapter()
        Me.GrupoComboBox = New System.Windows.Forms.ComboBox()
        Me.MuestraTipServEricBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraTipServEricTableAdapter = New sofTV.DataSetEric2TableAdapters.MuestraTipServEricTableAdapter()
        Me.ConceptoComboBox = New System.Windows.Forms.ComboBox()
        Me.MuestraAniosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraAniosTableAdapter = New sofTV.DataSetEric2TableAdapters.MuestraAniosTableAdapter()
        Me.AnioComboBox = New System.Windows.Forms.ComboBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ConMetasBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ConMetasBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Clv_ServicioTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_GrupoTextBox = New System.Windows.Forms.TextBox()
        Me.AnioTextBox = New System.Windows.Forms.TextBox()
        GrupoLabel = New System.Windows.Forms.Label()
        ConceptoLabel = New System.Windows.Forms.Label()
        AnioLabel = New System.Windows.Forms.Label()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConMetasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConMetasDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraAniosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.ConMetasBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConMetasBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'GrupoLabel
        '
        GrupoLabel.AutoSize = True
        GrupoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        GrupoLabel.Location = New System.Drawing.Point(122, 30)
        GrupoLabel.Name = "GrupoLabel"
        GrupoLabel.Size = New System.Drawing.Size(117, 15)
        GrupoLabel.TabIndex = 3
        GrupoLabel.Text = "Grupo de Ventas:"
        '
        'ConceptoLabel
        '
        ConceptoLabel.AutoSize = True
        ConceptoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ConceptoLabel.Location = New System.Drawing.Point(362, 30)
        ConceptoLabel.Name = "ConceptoLabel"
        ConceptoLabel.Size = New System.Drawing.Size(114, 15)
        ConceptoLabel.TabIndex = 6
        ConceptoLabel.Text = "Tipo de Servicio:"
        '
        'AnioLabel
        '
        AnioLabel.AutoSize = True
        AnioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        AnioLabel.Location = New System.Drawing.Point(626, 30)
        AnioLabel.Name = "AnioLabel"
        AnioLabel.Size = New System.Drawing.Size(35, 15)
        AnioLabel.TabIndex = 8
        AnioLabel.Text = "Año:"
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConMetasBindingSource
        '
        Me.ConMetasBindingSource.DataMember = "ConMetas"
        Me.ConMetasBindingSource.DataSource = Me.DataSetEric2
        '
        'ConMetasTableAdapter
        '
        Me.ConMetasTableAdapter.ClearBeforeFill = True
        '
        'ConMetasDataGridView
        '
        Me.ConMetasDataGridView.AutoGenerateColumns = False
        Me.ConMetasDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ConMetasDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.ConMetasDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn14, Me.DataGridViewTextBoxColumn15, Me.DataGridViewTextBoxColumn16, Me.DataGridViewTextBoxColumn17})
        Me.ConMetasDataGridView.DataSource = Me.ConMetasBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ConMetasDataGridView.DefaultCellStyle = DataGridViewCellStyle2
        Me.ConMetasDataGridView.Location = New System.Drawing.Point(3, 28)
        Me.ConMetasDataGridView.Name = "ConMetasDataGridView"
        Me.ConMetasDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.ConMetasDataGridView.Size = New System.Drawing.Size(892, 345)
        Me.ConMetasDataGridView.TabIndex = 2
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Clv_Servicio"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Clv_Servicio"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Servicio"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Servicio"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 200
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Clv_Grupo"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Clv_Grupo"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Grupo"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Grupo"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Ene"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Ene"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Width = 50
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Feb"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Feb"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Width = 50
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "Mar"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Mar"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Width = 50
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Abr"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Abr"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Width = 50
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "May"
        Me.DataGridViewTextBoxColumn9.HeaderText = "May"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Width = 50
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "Jun"
        Me.DataGridViewTextBoxColumn10.HeaderText = "Jun"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.Width = 50
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "Jul"
        Me.DataGridViewTextBoxColumn11.HeaderText = "Jul"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.Width = 50
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "Ago"
        Me.DataGridViewTextBoxColumn12.HeaderText = "Ago"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.Width = 50
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "Sep"
        Me.DataGridViewTextBoxColumn13.HeaderText = "Sep"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.Width = 50
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.DataPropertyName = "Oct"
        Me.DataGridViewTextBoxColumn14.HeaderText = "Oct"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.Width = 50
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.DataPropertyName = "Nov"
        Me.DataGridViewTextBoxColumn15.HeaderText = "Nov"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.Width = 50
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.DataPropertyName = "Dic"
        Me.DataGridViewTextBoxColumn16.HeaderText = "Dic"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.Width = 50
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.DataPropertyName = "Anio"
        Me.DataGridViewTextBoxColumn17.HeaderText = "Anio"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.Visible = False
        '
        'ConGrupoVentasBindingSource
        '
        Me.ConGrupoVentasBindingSource.DataMember = "ConGrupoVentas"
        Me.ConGrupoVentasBindingSource.DataSource = Me.DataSetEric2
        '
        'ConGrupoVentasTableAdapter
        '
        Me.ConGrupoVentasTableAdapter.ClearBeforeFill = True
        '
        'GrupoComboBox
        '
        Me.GrupoComboBox.DataSource = Me.ConGrupoVentasBindingSource
        Me.GrupoComboBox.DisplayMember = "Grupo"
        Me.GrupoComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GrupoComboBox.FormattingEnabled = True
        Me.GrupoComboBox.Location = New System.Drawing.Point(125, 48)
        Me.GrupoComboBox.Name = "GrupoComboBox"
        Me.GrupoComboBox.Size = New System.Drawing.Size(180, 23)
        Me.GrupoComboBox.TabIndex = 4
        Me.GrupoComboBox.ValueMember = "Clv_Grupo"
        '
        'MuestraTipServEricBindingSource
        '
        Me.MuestraTipServEricBindingSource.DataMember = "MuestraTipServEric"
        Me.MuestraTipServEricBindingSource.DataSource = Me.DataSetEric2
        '
        'MuestraTipServEricTableAdapter
        '
        Me.MuestraTipServEricTableAdapter.ClearBeforeFill = True
        '
        'ConceptoComboBox
        '
        Me.ConceptoComboBox.DataSource = Me.MuestraTipServEricBindingSource
        Me.ConceptoComboBox.DisplayMember = "Concepto"
        Me.ConceptoComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoComboBox.FormattingEnabled = True
        Me.ConceptoComboBox.Location = New System.Drawing.Point(365, 48)
        Me.ConceptoComboBox.Name = "ConceptoComboBox"
        Me.ConceptoComboBox.Size = New System.Drawing.Size(209, 23)
        Me.ConceptoComboBox.TabIndex = 7
        Me.ConceptoComboBox.ValueMember = "Clv_TipSer"
        '
        'MuestraAniosBindingSource
        '
        Me.MuestraAniosBindingSource.DataMember = "MuestraAnios"
        Me.MuestraAniosBindingSource.DataSource = Me.DataSetEric2
        '
        'MuestraAniosTableAdapter
        '
        Me.MuestraAniosTableAdapter.ClearBeforeFill = True
        '
        'AnioComboBox
        '
        Me.AnioComboBox.DataSource = Me.MuestraAniosBindingSource
        Me.AnioComboBox.DisplayMember = "Anio"
        Me.AnioComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AnioComboBox.FormattingEnabled = True
        Me.AnioComboBox.Location = New System.Drawing.Point(629, 48)
        Me.AnioComboBox.Name = "AnioComboBox"
        Me.AnioComboBox.Size = New System.Drawing.Size(136, 23)
        Me.AnioComboBox.TabIndex = 9
        Me.AnioComboBox.ValueMember = "Anio"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.ConMetasBindingNavigator)
        Me.Panel1.Controls.Add(Me.ConMetasDataGridView)
        Me.Panel1.Location = New System.Drawing.Point(12, 96)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(898, 376)
        Me.Panel1.TabIndex = 10
        '
        'ConMetasBindingNavigator
        '
        Me.ConMetasBindingNavigator.AddNewItem = Nothing
        Me.ConMetasBindingNavigator.BindingSource = Me.ConMetasBindingSource
        Me.ConMetasBindingNavigator.CountItem = Nothing
        Me.ConMetasBindingNavigator.DeleteItem = Nothing
        Me.ConMetasBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConMetasBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.ConMetasBindingNavigatorSaveItem})
        Me.ConMetasBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ConMetasBindingNavigator.MoveFirstItem = Nothing
        Me.ConMetasBindingNavigator.MoveLastItem = Nothing
        Me.ConMetasBindingNavigator.MoveNextItem = Nothing
        Me.ConMetasBindingNavigator.MovePreviousItem = Nothing
        Me.ConMetasBindingNavigator.Name = "ConMetasBindingNavigator"
        Me.ConMetasBindingNavigator.PositionItem = Nothing
        Me.ConMetasBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ConMetasBindingNavigator.Size = New System.Drawing.Size(898, 25)
        Me.ConMetasBindingNavigator.TabIndex = 3
        Me.ConMetasBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(88, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ConMetasBindingNavigatorSaveItem
        '
        Me.ConMetasBindingNavigatorSaveItem.Image = CType(resources.GetObject("ConMetasBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ConMetasBindingNavigatorSaveItem.Name = "ConMetasBindingNavigatorSaveItem"
        Me.ConMetasBindingNavigatorSaveItem.Size = New System.Drawing.Size(88, 22)
        Me.ConMetasBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(805, 511)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 11
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Clv_ServicioTextBox
        '
        Me.Clv_ServicioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConMetasBindingSource, "Clv_Servicio", True))
        Me.Clv_ServicioTextBox.Location = New System.Drawing.Point(858, 511)
        Me.Clv_ServicioTextBox.Name = "Clv_ServicioTextBox"
        Me.Clv_ServicioTextBox.ReadOnly = True
        Me.Clv_ServicioTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_ServicioTextBox.TabIndex = 12
        Me.Clv_ServicioTextBox.TabStop = False
        '
        'Clv_GrupoTextBox
        '
        Me.Clv_GrupoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConMetasBindingSource, "Clv_Grupo", True))
        Me.Clv_GrupoTextBox.Location = New System.Drawing.Point(874, 511)
        Me.Clv_GrupoTextBox.Name = "Clv_GrupoTextBox"
        Me.Clv_GrupoTextBox.ReadOnly = True
        Me.Clv_GrupoTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_GrupoTextBox.TabIndex = 13
        Me.Clv_GrupoTextBox.TabStop = False
        '
        'AnioTextBox
        '
        Me.AnioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConMetasBindingSource, "Anio", True))
        Me.AnioTextBox.Location = New System.Drawing.Point(890, 511)
        Me.AnioTextBox.Name = "AnioTextBox"
        Me.AnioTextBox.ReadOnly = True
        Me.AnioTextBox.Size = New System.Drawing.Size(10, 20)
        Me.AnioTextBox.TabIndex = 14
        Me.AnioTextBox.TabStop = False
        '
        'FrmMetas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(953, 559)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.AnioTextBox)
        Me.Controls.Add(Me.Clv_GrupoTextBox)
        Me.Controls.Add(Me.Clv_ServicioTextBox)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(AnioLabel)
        Me.Controls.Add(Me.AnioComboBox)
        Me.Controls.Add(ConceptoLabel)
        Me.Controls.Add(Me.ConceptoComboBox)
        Me.Controls.Add(GrupoLabel)
        Me.Controls.Add(Me.GrupoComboBox)
        Me.Name = "FrmMetas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Medidores Globales de Servicios de Ventas"
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConMetasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConMetasDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConGrupoVentasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraAniosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.ConMetasBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConMetasBindingNavigator.ResumeLayout(False)
        Me.ConMetasBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents ConMetasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConMetasTableAdapter As sofTV.DataSetEric2TableAdapters.ConMetasTableAdapter
    Friend WithEvents ConMetasDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents ConGrupoVentasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGrupoVentasTableAdapter As sofTV.DataSetEric2TableAdapters.ConGrupoVentasTableAdapter
    Friend WithEvents GrupoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents MuestraTipServEricBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipServEricTableAdapter As sofTV.DataSetEric2TableAdapters.MuestraTipServEricTableAdapter
    Friend WithEvents ConceptoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents MuestraAniosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraAniosTableAdapter As sofTV.DataSetEric2TableAdapters.MuestraAniosTableAdapter
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AnioComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ConMetasBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConMetasBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Clv_ServicioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_GrupoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents AnioTextBox As System.Windows.Forms.TextBox
End Class
