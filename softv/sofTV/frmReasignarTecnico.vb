﻿Public Class frmReasignarTecnico

#Region "EVENTOS"
    Private Sub frmReasignarTecnico_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.Label1.ForeColor = Color.Black
        Me.Label2.ForeColor = Color.Black
        UspLlenaComboTecnicosTodos()

    End Sub
    Private Sub frmReasignarTecnico_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        UspCancelarReasignacion()

    End Sub
#End Region

#Region "METODOS"
    Private Sub UspLlenaComboTecnicosTodos()
        Try
            BaseII.limpiaParametros()
            Me.cmbTecnnico.DataSource = BaseII.ConsultaDT("UspLlenaComboTecnicosTodos")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspCancelarReasignacion()
        Try
            BaseII.limpiaParametros()
            BaseII.Inserta("UspCancelarReasignacion")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspHacerReasignacion()
        Try
            Dim tipotecnico As String
            Dim CODIGO As String
            CODIGO = InStr(1, Me.cmbTecnnico.Text, "Virtual")
            If CODIGO = 0 Then
                tipotecnico = "T"
            Else
                tipotecnico = "V"
            End If
           
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@COMENTARIO", SqlDbType.VarChar, Me.TextBox1.Text, 500)
            BaseII.CreateMyParameter("@CLVTECNICO", SqlDbType.Int, Me.cmbTecnnico.SelectedValue)
            BaseII.CreateMyParameter("@TIPOTECNICO", SqlDbType.VarChar, tipotecnico, 10)
            BaseII.CreateMyParameter("@CLVUSUARIO", SqlDbType.VarChar, GloUsuario, 10)
            BaseII.Inserta("UspHacerReasignacion")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region



#Region "CONTROLES"
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        UspCancelarReasignacion()
        Me.Close()
    End Sub

    Private Sub btnGuardarAsignacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardarAsignacion.Click
        If Me.TextBox1.Text = "" Then
            MsgBox("Debe de poner el motivo por el cual quiere hacer la reasignación", MsgBoxStyle.Information, "Reasignación del Técnico ")
            Exit Sub
        End If
        UspHacerReasignacion()
        Me.Close()

    End Sub
#End Region

    


    
End Class