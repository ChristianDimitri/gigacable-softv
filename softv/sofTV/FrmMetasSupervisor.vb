﻿

Public Class FrmMetasSupervisor


#Region "EVENTOS"

    Private Sub FrmMetasSupervisor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        MuestraAnios()
        ConGrupoVentas()
        UspLlenaSupervisores()
        MuestraTipServEric()

        UspMuestraMetasSupervisior(Me.GrupoComboBox.SelectedValue, Me.NombreComboBox.SelectedValue, Me.ConceptoComboBox.SelectedValue, Me.AnioComboBox.Text)

    End Sub

    Private Sub GrupoComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GrupoComboBox.SelectedIndexChanged
        UspLlenaSupervisores()
        UspMuestraMetasSupervisior(Me.GrupoComboBox.SelectedValue, Me.NombreComboBox.SelectedValue, Me.ConceptoComboBox.SelectedValue, Me.AnioComboBox.Text)
    End Sub


    Private Sub ConceptoComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConceptoComboBox.SelectedIndexChanged
        UspMuestraMetasSupervisior(Me.GrupoComboBox.SelectedValue, Me.NombreComboBox.SelectedValue, Me.ConceptoComboBox.SelectedValue, Me.AnioComboBox.Text)

    End Sub

    Private Sub AnioComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AnioComboBox.SelectedIndexChanged
        UspMuestraMetasSupervisior(Me.GrupoComboBox.SelectedValue, Me.NombreComboBox.SelectedValue, Me.ConceptoComboBox.SelectedValue, Me.AnioComboBox.Text)

    End Sub


    Private Sub NombreComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NombreComboBox.SelectedIndexChanged
        UspMuestraMetasSupervisior(Me.GrupoComboBox.SelectedValue, Me.NombreComboBox.SelectedValue, Me.ConceptoComboBox.SelectedValue, Me.AnioComboBox.Text)

    End Sub

#End Region

#Region "FUNCIONES"

    Private Sub UspLlenaSupervisores()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, Me.GrupoComboBox.SelectedValue)
            Me.NombreComboBox.DataSource = BaseII.ConsultaDT("UspLlenaSupervisores")

            If NombreComboBox.Items.Count = 0 Then
                NombreComboBox.Text = ""
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ConGrupoVentas()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Grupo", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            Me.GrupoComboBox.DataSource = BaseII.ConsultaDT("ConGrupoVentas")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub MuestraAnios()
        Try
            BaseII.limpiaParametros()
            Me.AnioComboBox.DataSource = BaseII.ConsultaDT("MuestraAnios")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub MuestraTipServEric()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_TipServ", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, 0)
            Me.ConceptoComboBox.DataSource = BaseII.ConsultaDT("MuestraTipServEric")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMuestraMetasSupervisior(ByVal prmclvGrupo As Integer, ByVal prmClvSupervisior As Integer, ByVal prmclvservicio As Integer, ByVal prmanio As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Grupo", SqlDbType.Int, prmclvGrupo)
            BaseII.CreateMyParameter("@Clave", SqlDbType.Int, prmClvSupervisior)
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, prmclvservicio)
            BaseII.CreateMyParameter("@Anio", SqlDbType.Int, prmanio)
            ConMetasIndDataGridView.DataSource = BaseII.ConsultaDT("UspMuestraMetasSupervisior")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspGuardaMetasSupervisor(ByVal prmClvSupervisor As Integer, ByVal prmClvServicio As Integer, ByVal prmEne As Integer, ByVal prmfeb As Integer, ByVal prmMar As Integer, _
                                         ByVal prmAbr As Integer, ByVal prmMay As Integer, ByVal prmJun As Integer, ByVal prmJul As Integer, ByVal prmAgo As Integer, ByVal prmSep As Integer, _
                                         ByVal prmOct As Integer, ByVal prmNov As Integer, ByVal prmDic As Integer, ByVal prmAnio As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSUPERVISOR", SqlDbType.Int, prmClvSupervisor)
            BaseII.CreateMyParameter("@CLVSERVICIO", SqlDbType.Int, prmClvServicio)
            BaseII.CreateMyParameter("@ENE", SqlDbType.Int, prmEne)
            BaseII.CreateMyParameter("@FEB", SqlDbType.Int, prmfeb)
            BaseII.CreateMyParameter("@MAR", SqlDbType.Int, prmMar)
            BaseII.CreateMyParameter("@ABR", SqlDbType.Int, prmAbr)
            BaseII.CreateMyParameter("@MAY", SqlDbType.Int, prmMay)
            BaseII.CreateMyParameter("@JUN", SqlDbType.Int, prmJun)
            BaseII.CreateMyParameter("@JUL", SqlDbType.Int, prmJul)
            BaseII.CreateMyParameter("@AGO", SqlDbType.Int, prmAgo)
            BaseII.CreateMyParameter("@SEP", SqlDbType.Int, prmSep)
            BaseII.CreateMyParameter("@OCT", SqlDbType.Int, prmOct)
            BaseII.CreateMyParameter("@NOV", SqlDbType.Int, prmNov)
            BaseII.CreateMyParameter("@DIC", SqlDbType.Int, prmDic)
            BaseII.CreateMyParameter("@ANIO", SqlDbType.Int, prmAnio)
            BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, Me.GrupoComboBox.SelectedValue)
            BaseII.Inserta("UspGuardaMetasSupervisor")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspBorraMetaSupervisor(ByVal prmClvSupervisor As Integer, ByVal prmClvServicio As Integer, ByVal prmAnio As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSUPERVISOR", SqlDbType.Int, prmClvSupervisor)
            BaseII.CreateMyParameter("@CLVSERVICIO", SqlDbType.Int, prmClvServicio)
            BaseII.CreateMyParameter("@ANIO", SqlDbType.Int, prmAnio)
            BaseII.Inserta("UspBorraMetaSupervisor")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Salir"

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

#End Region

#Region "Guardar"

    Private Sub ConMetasIndBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConMetasIndBindingNavigatorSaveItem.Click
        Dim I As Integer
        Me.ConMetasIndDataGridView.EndEdit()
        For I = 0 To ConMetasIndDataGridView.Rows.Count - 1
            UspGuardaMetasSupervisor(Me.NombreComboBox.SelectedValue, Me.ConMetasIndDataGridView(2, I).Value, Me.ConMetasIndDataGridView(4, I).Value, Me.ConMetasIndDataGridView(5, I).Value,
           Me.ConMetasIndDataGridView(6, I).Value, Me.ConMetasIndDataGridView(7, I).Value, Me.ConMetasIndDataGridView(8, I).Value, Me.ConMetasIndDataGridView(9, I).Value, Me.ConMetasIndDataGridView(10, I).Value, Me.ConMetasIndDataGridView(11, I).Value,
           Me.ConMetasIndDataGridView(12, I).Value, Me.ConMetasIndDataGridView(13, I).Value, Me.ConMetasIndDataGridView(14, I).Value, Me.ConMetasIndDataGridView(15, I).Value, CInt(Me.AnioComboBox.Text))
        Next

        UspMuestraMetasSupervisior(Me.GrupoComboBox.SelectedValue, Me.NombreComboBox.SelectedValue, Me.ConceptoComboBox.SelectedValue, Me.AnioComboBox.Text)

    End Sub

#End Region

#Region "Eliminar"
    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click

        Dim I As Integer

        I = Me.ConMetasIndDataGridView.CurrentRow.Index
        UspBorraMetaSupervisor(Me.NombreComboBox.SelectedValue, Me.ConMetasIndDataGridView(2, I).Value, CInt(Me.AnioComboBox.Text))
        UspMuestraMetasSupervisior(Me.GrupoComboBox.SelectedValue, Me.NombreComboBox.SelectedValue, Me.ConceptoComboBox.SelectedValue, Me.AnioComboBox.Text)
    End Sub


#End Region







   
End Class