﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTelmexEncuesta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label3 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTelmexEncuesta))
        Me.Consulta_Cat_Num_TelefonoBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.CMBPanel1 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.si1_RadioBtn = New System.Windows.Forms.RadioButton()
        Me.no1_RadioBtn = New System.Windows.Forms.RadioButton()
        Me.cantidadExtensionesNUD = New System.Windows.Forms.NumericUpDown()
        Me.tipoInstalacionCmb = New System.Windows.Forms.ComboBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.si2_RadioBtn = New System.Windows.Forms.RadioButton()
        Me.no2_RadioBtn = New System.Windows.Forms.RadioButton()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.planPagoMesesNUD = New System.Windows.Forms.NumericUpDown()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Button5 = New System.Windows.Forms.Button()
        Label3 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        CType(Me.Consulta_Cat_Num_TelefonoBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.SuspendLayout()
        Me.CMBPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.cantidadExtensionesNUD, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.planPagoMesesNUD, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.Location = New System.Drawing.Point(214, 51)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(145, 16)
        Label3.TabIndex = 5
        Label3.Text = "Tipo de instalación:"
        Label3.Visible = False
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.Location = New System.Drawing.Point(20, 81)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(342, 16)
        Label4.TabIndex = 27
        Label4.Text = "Cantidad de extensiones telefónicas requeridas:"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.Location = New System.Drawing.Point(130, 21)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(228, 16)
        Label1.TabIndex = 86
        Label1.Text = "¿Instalará servicios de Telmex?"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.Location = New System.Drawing.Point(9, 14)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(349, 16)
        Label2.TabIndex = 88
        Label2.Text = "¿Este cliente ya cuenta con servicios de Telmex?"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.Location = New System.Drawing.Point(193, 13)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(165, 16)
        Label5.TabIndex = 90
        Label5.Text = "Plan de pago (meses):"
        '
        'Consulta_Cat_Num_TelefonoBindingNavigator
        '
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.AddNewItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.CountItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem})
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.MoveFirstItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.MoveLastItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.MoveNextItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.MovePreviousItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Name = "Consulta_Cat_Num_TelefonoBindingNavigator"
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.PositionItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Size = New System.Drawing.Size(642, 25)
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.TabIndex = 25
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(77, 22)
        Me.BindingNavigatorDeleteItem.Text = "Eliminar"
        '
        'Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem
        '
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Image = CType(resources.GetObject("Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Name = "Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem"
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 22)
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'CMBPanel1
        '
        Me.CMBPanel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMBPanel1.Controls.Add(Me.Panel1)
        Me.CMBPanel1.Controls.Add(Label1)
        Me.CMBPanel1.Location = New System.Drawing.Point(15, 48)
        Me.CMBPanel1.Name = "CMBPanel1"
        Me.CMBPanel1.Size = New System.Drawing.Size(613, 52)
        Me.CMBPanel1.TabIndex = 24
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.si1_RadioBtn)
        Me.Panel1.Controls.Add(Me.no1_RadioBtn)
        Me.Panel1.Location = New System.Drawing.Point(367, 17)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(223, 26)
        Me.Panel1.TabIndex = 87
        '
        'si1_RadioBtn
        '
        Me.si1_RadioBtn.AutoSize = True
        Me.si1_RadioBtn.Checked = True
        Me.si1_RadioBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.si1_RadioBtn.ForeColor = System.Drawing.Color.LightSlateGray
        Me.si1_RadioBtn.Location = New System.Drawing.Point(45, 3)
        Me.si1_RadioBtn.Name = "si1_RadioBtn"
        Me.si1_RadioBtn.Size = New System.Drawing.Size(38, 19)
        Me.si1_RadioBtn.TabIndex = 84
        Me.si1_RadioBtn.TabStop = True
        Me.si1_RadioBtn.Text = "Sí"
        Me.si1_RadioBtn.UseVisualStyleBackColor = True
        '
        'no1_RadioBtn
        '
        Me.no1_RadioBtn.AutoSize = True
        Me.no1_RadioBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.no1_RadioBtn.ForeColor = System.Drawing.Color.LightSlateGray
        Me.no1_RadioBtn.Location = New System.Drawing.Point(135, 3)
        Me.no1_RadioBtn.Name = "no1_RadioBtn"
        Me.no1_RadioBtn.Size = New System.Drawing.Size(43, 19)
        Me.no1_RadioBtn.TabIndex = 85
        Me.no1_RadioBtn.Text = "No"
        Me.no1_RadioBtn.UseVisualStyleBackColor = True
        '
        'cantidadExtensionesNUD
        '
        Me.cantidadExtensionesNUD.Location = New System.Drawing.Point(368, 81)
        Me.cantidadExtensionesNUD.Name = "cantidadExtensionesNUD"
        Me.cantidadExtensionesNUD.Size = New System.Drawing.Size(107, 20)
        Me.cantidadExtensionesNUD.TabIndex = 89
        '
        'tipoInstalacionCmb
        '
        Me.tipoInstalacionCmb.DisplayMember = "Descripcion"
        Me.tipoInstalacionCmb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tipoInstalacionCmb.FormattingEnabled = True
        Me.tipoInstalacionCmb.Location = New System.Drawing.Point(368, 46)
        Me.tipoInstalacionCmb.Name = "tipoInstalacionCmb"
        Me.tipoInstalacionCmb.Size = New System.Drawing.Size(223, 21)
        Me.tipoInstalacionCmb.TabIndex = 52
        Me.tipoInstalacionCmb.ValueMember = "IdMedio"
        Me.tipoInstalacionCmb.Visible = False
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Panel2)
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Controls.Add(Me.cantidadExtensionesNUD)
        Me.Panel3.Controls.Add(Label2)
        Me.Panel3.Controls.Add(Label3)
        Me.Panel3.Controls.Add(Me.tipoInstalacionCmb)
        Me.Panel3.Controls.Add(Label4)
        Me.Panel3.Location = New System.Drawing.Point(15, 96)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(613, 146)
        Me.Panel3.TabIndex = 27
        Me.Panel3.Visible = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.si2_RadioBtn)
        Me.Panel2.Controls.Add(Me.no2_RadioBtn)
        Me.Panel2.Location = New System.Drawing.Point(368, 10)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(223, 26)
        Me.Panel2.TabIndex = 91
        '
        'si2_RadioBtn
        '
        Me.si2_RadioBtn.AutoSize = True
        Me.si2_RadioBtn.Checked = True
        Me.si2_RadioBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.si2_RadioBtn.ForeColor = System.Drawing.Color.LightSlateGray
        Me.si2_RadioBtn.Location = New System.Drawing.Point(45, 3)
        Me.si2_RadioBtn.Name = "si2_RadioBtn"
        Me.si2_RadioBtn.Size = New System.Drawing.Size(38, 19)
        Me.si2_RadioBtn.TabIndex = 84
        Me.si2_RadioBtn.TabStop = True
        Me.si2_RadioBtn.Text = "Sí"
        Me.si2_RadioBtn.UseVisualStyleBackColor = True
        '
        'no2_RadioBtn
        '
        Me.no2_RadioBtn.AutoSize = True
        Me.no2_RadioBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.no2_RadioBtn.ForeColor = System.Drawing.Color.LightSlateGray
        Me.no2_RadioBtn.Location = New System.Drawing.Point(135, 3)
        Me.no2_RadioBtn.Name = "no2_RadioBtn"
        Me.no2_RadioBtn.Size = New System.Drawing.Size(43, 19)
        Me.no2_RadioBtn.TabIndex = 85
        Me.no2_RadioBtn.Text = "No"
        Me.no2_RadioBtn.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.planPagoMesesNUD)
        Me.Panel4.Controls.Add(Label5)
        Me.Panel4.Location = New System.Drawing.Point(2, 110)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(608, 35)
        Me.Panel4.TabIndex = 90
        Me.Panel4.Visible = False
        '
        'planPagoMesesNUD
        '
        Me.planPagoMesesNUD.Location = New System.Drawing.Point(369, 9)
        Me.planPagoMesesNUD.Name = "planPagoMesesNUD"
        Me.planPagoMesesNUD.Size = New System.Drawing.Size(107, 20)
        Me.planPagoMesesNUD.TabIndex = 91
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Panel5.Controls.Add(Me.Panel6)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(0, 0)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(651, 306)
        Me.Panel5.TabIndex = 28
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.SystemColors.Control
        Me.Panel6.Controls.Add(Me.Consulta_Cat_Num_TelefonoBindingNavigator)
        Me.Panel6.Controls.Add(Me.Button5)
        Me.Panel6.Controls.Add(Me.Panel3)
        Me.Panel6.Controls.Add(Me.CMBPanel1)
        Me.Panel6.Location = New System.Drawing.Point(4, 5)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(642, 296)
        Me.Panel6.TabIndex = 30
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(480, 247)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(147, 36)
        Me.Button5.TabIndex = 27
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'FrmTelmexEncuesta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(651, 306)
        Me.Controls.Add(Me.Panel5)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmTelmexEncuesta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Telmex"
        CType(Me.Consulta_Cat_Num_TelefonoBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.ResumeLayout(False)
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.PerformLayout()
        Me.CMBPanel1.ResumeLayout(False)
        Me.CMBPanel1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.cantidadExtensionesNUD, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.planPagoMesesNUD, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.ResumeLayout(False)

End Sub
    Friend WithEvents Consulta_Cat_Num_TelefonoBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CMBPanel1 As System.Windows.Forms.Panel
    Friend WithEvents tipoInstalacionCmb As System.Windows.Forms.ComboBox
    Friend WithEvents no1_RadioBtn As System.Windows.Forms.RadioButton
    Friend WithEvents si1_RadioBtn As System.Windows.Forms.RadioButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cantidadExtensionesNUD As System.Windows.Forms.NumericUpDown
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents planPagoMesesNUD As System.Windows.Forms.NumericUpDown
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents si2_RadioBtn As System.Windows.Forms.RadioButton
    Friend WithEvents no2_RadioBtn As System.Windows.Forms.RadioButton
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Button5 As System.Windows.Forms.Button
End Class
