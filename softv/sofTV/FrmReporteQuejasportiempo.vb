﻿Imports System.Collections.Generic
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient

Public Class FrmReporteQuejasporTiempo
    Dim op As String = Nothing
    Dim FUERA As Integer = 0
    Dim DENTRO As Integer = 0
    Dim FECHA As Integer = 0
    Dim FECHAE As Integer = 0
    Dim MOTIVO As Integer = 0
    Dim COLONIA As Integer = 0
    Dim ORDENAR As Integer = 0
    Dim Fec1Ini As Date = "01/01/1900"
    Dim Fec1Fin As Date = "01/01/1900"
    Dim Fec2Ini As Date = "01/01/1900"
    Dim Fec2Fin As Date = "01/01/1900"
    Dim nclv_trabajo As String = "0"
    Dim nClv_colonia As String = "0"
    Dim PorServicio As Boolean = False
    Dim tipservR As Integer = 0

    Private Sub FrmReporteQuejasporTiempo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'DataSetLidia2.MuestraTipSerPrincipal2' Puede moverla o quitarla según sea necesario.

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'TODO: esta línea de código carga datos en la tabla 'DataSetEDGAR.MuestraTipSerPrincipal' Puede moverla o quitarla según sea necesario.
        Me.MuestraTipSerPrincipal2TableAdapter.Connection = CON
        Me.MuestraTipSerPrincipal2TableAdapter.Fill(Me.DataSetLidia2.MuestraTipSerPrincipal2)
        colorea(Me, Me.Name)
        PendientesCheckBox.Checked = True

        Label9.ForeColor = Color.Black

        'If CheckBox1.Checked = True Then
        '    Label4.Enabled = True
        '    ComboBox1.Enabled = True
        'ElseIf CheckBox1.Checked = False Then
        '    Label4.Enabled = False
        '    ComboBox1.Enabled = False
        'End If

        CON.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Close()
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If Me.CheckBox2.CheckState = CheckState.Checked Then
            op = "1"
            FrmSelFechas.Show()
        Else
            Me.FECSOLFIN.Text = ""
            Me.FECSOLINI.Text = ""
        End If
    End Sub

    Private Sub FrmReporteQuejasporTiempo_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        If op = "1" And GloBndSelFecha = True Then
            GloBndSelFecha = False
            Me.FECSOLINI.Text = GloFecha_Ini
            Me.FECSOLFIN.Text = GloFecha_Fin
            GloFecha_Ini = "01/01/1900"
            GloFecha_Fin = "01/01/1900"
        End If
        If op = "225" And GloBndSelFecha = True Then
            GloBndSelFecha = False
            Me.FECEJEINIR.Text = GloFecha_Ini
            Me.FECEJEFINR.Text = GloFecha_Fin
            GloFecha_Ini = "01/01/1900"
            GloFecha_Fin = "01/01/1900"
        End If
        If GloBndTrabajo = True Then
            GloBndTrabajo = False
            Me.CLV_TRABAJO.Text = GloClvMotivo48
            Me.NOMTRABAJO.Text = GloDescrpcionMotivo48
        End If
        If GlobndClv_Colonia = True Then
            GlobndClv_Colonia = False
            Me.CLV_COLONIA.Text = GloNumClv_Colonia
            Me.NOMCOLONIA.Text = GLONOMCOLONIA
        End If
    End Sub

    Private Sub CheckBox4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox4.CheckedChanged
        If Me.CheckBox4.CheckState = CheckState.Checked Then
            GloBNDMotivo48 = True
            op = "3"
            FrmSeleccionaMotivo48.ShowDialog()
            GloBNDMotivo48 = False
        Else
            Me.CLV_TRABAJO.Text = 0
            Me.NOMTRABAJO.Text = ""
        End If
    End Sub

    Private Sub CheckBox5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox5.CheckedChanged
        If Me.CheckBox5.CheckState = CheckState.Checked Then
            op = "4"
            FrmSelColonia.Show()
        Else
            Me.CLV_COLONIA.Text = 0
            Me.NOMCOLONIA.Text = ""
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        If PendientesCheckBox.Checked = False And EjecutadasCheckBox.Checked = False Then
            MsgBox("Seleccione al menos un rango de Ejecución", MsgBoxStyle.Information, "Reporte Quejas por Tiempos")
            Exit Sub
        End If

        If CheckBox2.Checked = True And (FECSOLINI.Text = "" Or FECSOLFIN.Text = "") Then
            MsgBox("Seleccione un rango de fechas valido", MsgBoxStyle.Information, "Reporte Quejas por Tiempos")
            Exit Sub
        End If

        If PendientesCheckBox.Checked = True Then
            DENTRO = 1
        Else
            DENTRO = 0
        End If

        If EjecutadasCheckBox.Checked = True Then
            FUERA = 1
        Else
            FUERA = 0
        End If

        If CheckBox2.Checked = True Then
            FECHA = 1
        Else
            FECHA = 0
        End If

        If CheckBox3.Checked = True Then
            FECHAE = 1
        Else
            FECHAE = 0
        End If

        If RadioButton1.Checked = True Then
            ORDENAR = 1
        End If
        If RadioButton2.Checked = True Then
            ORDENAR = 2
        End If
        If RadioButton3.Checked = True Then
            ORDENAR = 3
        End If
        If RadioButton4.Checked = True Then
            ORDENAR = 4
        End If

        'If CheckBox1.Checked = True Then
        PorServicio = True
        tipservR = ComboBox1.SelectedValue
        'ElseIf CheckBox1.Checked = False Then
        '    PorServicio = False
        '    tipservR = 0
        'End If


        Dim DS As New DataSet
        Dim rDocument As New ReportDocument
        DS = UspReporteQuejasporTiempo()

        rDocument.Load(RutaReportes + "\ReporteQuejasporTiempo.rpt")
        rDocument.SetDataSource(DS)

        SetDBReport(DS, rDocument)
        Dim fechas As String = Nothing
        If FECSOLINI.Text <> "" And FECSOLINI.Text <> "" And FECEJEINIR.Text = "" And FECEJEFINR.Text = "" Then
            fechas = "De la Fecha de Solicitud: " + Fec1Ini.Date + " A la Fecha de Solicitud: " + Fec1Fin.Date
        ElseIf FECSOLINI.Text = "" And FECSOLINI.Text = "" And FECEJEINIR.Text <> "" And FECEJEFINR.Text <> "" Then
            fechas = "De la Fecha de Ejecución: " + FECEJEINIR.Text + " A la Fecha de Ejecución: " + FECEJEFINR.Text
        ElseIf FECSOLINI.Text <> "" And FECSOLINI.Text <> "" And FECEJEINIR.Text <> "" And FECEJEFINR.Text <> "" Then
            fechas = "De la Fecha de Solicitud: " + Fec1Ini.Date + " A la Fecha de Solicitud: " + Fec1Fin.Date + " y De la Fecha de Ejecución: " + FECEJEINIR.Text + " A la Fecha de Ejecución: " + FECEJEFINR.Text
        End If
        Dim mySelectFormula As String = Nothing
        mySelectFormula = "Reporte de Quejas por Tiempos"
        rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
        rDocument.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        rDocument.DataDefinition.FormulaFields("SubTitulo").Text = "'" & fechas & "'"
        'mySelectFormula = "Quejas " + Me.ComboBox1.Text
        'rDocument.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"

        CrystalReportViewer1.ReportSource = rDocument

        rDocument = Nothing

    End Sub

    Private Function UspReporteQuejasporTiempo() As DataSet
        Try
            If IsDate(Me.FECSOLINI.Text) = True Then Fec1Ini = Me.FECSOLINI.Text
            If IsDate(Me.FECSOLFIN.Text) = True Then Fec1Fin = Me.FECSOLFIN.Text
            If IsDate(Me.FECEJEINIR.Text) = True Then Fec2Ini = Me.FECEJEINIR.Text
            If IsDate(Me.FECEJEFINR.Text) = True Then Fec2Fin = Me.FECEJEFINR.Text
            If IsNumeric(Me.CLV_TRABAJO.Text) = True Then nclv_trabajo = CStr(Me.CLV_TRABAJO.Text)
            If IsNumeric(Me.CLV_COLONIA.Text) = True Then nClv_colonia = CStr(Me.CLV_COLONIA.Text)

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@FUERA", SqlDbType.Int, FUERA)
            BaseII.CreateMyParameter("@DENTRO", SqlDbType.Int, DENTRO)
            BaseII.CreateMyParameter("@FECHA", SqlDbType.Int, FECHA)
            BaseII.CreateMyParameter("@FECINI", SqlDbType.DateTime, Fec1Ini)
            BaseII.CreateMyParameter("@FECFIN", SqlDbType.DateTime, Fec1Fin)
            BaseII.CreateMyParameter("@FECHAE", SqlDbType.Int, FECHAE)
            BaseII.CreateMyParameter("@FECINIE", SqlDbType.DateTime, Fec2Ini)
            BaseII.CreateMyParameter("@FECFINE", SqlDbType.DateTime, Fec2Fin)
            BaseII.CreateMyParameter("@ORDENAR", SqlDbType.Int, ORDENAR)
            BaseII.CreateMyParameter("@PorServicio", SqlDbType.Int, PorServicio)
            BaseII.CreateMyParameter("@Tipserv", SqlDbType.Int, tipservR)
            Dim tableNameList As New List(Of String)
            tableNameList.Add("ReporteQuejasporTiempo")
            Return BaseII.ConsultaDS("UspReporteQuejasporTiempo", tableNameList)
        Catch ex As Exception

        End Try
    End Function



    Private Sub PendientesCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PendientesCheckBox.CheckedChanged
        EjecutadasCheckBox.Checked = False
    End Sub

    Private Sub EjecutadasCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EjecutadasCheckBox.CheckedChanged
        PendientesCheckBox.Checked = False
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'If CheckBox1.Checked = True Then
        '    Label4.Enabled = True
        '    ComboBox1.Enabled = True
        'ElseIf CheckBox1.Checked = False Then
        '    Label4.Enabled = False
        '    ComboBox1.Enabled = False
        'End If
    End Sub

    Private Sub FECEJEFIN_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FECEJEFIN.Click

    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If Me.CheckBox3.CheckState = CheckState.Checked Then
            op = "225"
            FrmSelFechas.Show()
        Else
            Me.FECEJEFINR.Text = ""
            Me.FECEJEINIR.Text = ""
        End If
    End Sub
End Class