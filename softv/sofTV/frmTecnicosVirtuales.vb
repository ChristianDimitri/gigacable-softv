﻿Public Class frmTecnicosVirtuales
    Dim LocClvTecnico As Integer
    Dim LocTipo As String
    Dim BND As Integer
 

#Region "Eventos"
    Private Sub frmTecnicosVirtuales_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.Panel1.BackColor = Me.BackColor
        Me.gbDatosTecnico.BackColor = Me.BackColor
        Me.Label1.BackColor = Me.BackColor
        Me.Label2.BackColor = Me.BackColor
        Me.Label3.BackColor = Me.BackColor
        Me.Label4.BackColor = Me.BackColor
        LocTipo = ""
        LocClvTecnico = 0

        If Me.gbDatosTecnico.Visible = True Then
            Me.gbDatosTecnico.Visible = False
        End If

        UspLlenaDatosTecnico(0, String.Empty)
    End Sub

    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function

    Private Sub txtBuscarClave_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBuscarClave.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub
#End Region

#Region "Metodos"
    Private Sub UspEliminarDatosTecnico()
        Try
            Dim DT As DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ID", SqlDbType.Int, LocClvTecnico)
            DT = BaseII.ConsultaDT("UspEliminarDatosTecnico")
            If DT.Rows.Count > 0 Then
                BND = CInt(DT.Rows(0)(0).ToString)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function UspMuestraNombreTecnico() As DataTable
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ID", SqlDbType.Int, LocClvTecnico)
            UspMuestraNombreTecnico = BaseII.ConsultaDT("UspMuestraNombreTecnico")
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub UspLlenaDatosTecnico(ByVal PRMID As Integer, ByVal PRMNOMBRE As String)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ID", SqlDbType.Int, PRMID)
            BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, PRMNOMBRE, 250)
            Me.dgTecnicos.DataSource = BaseII.ConsultaDT("UspLlenaDatosTecnico")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspGuardaNombreTecnico(ByVal prmID As Integer)
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ID", SqlDbType.Int, prmID)
            BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, Me.txtNombreTecnico.Text, 250)
            DT = BaseII.ConsultaDT("UspGuardaNombreTecnico")
            If DT.Rows.Count > 0 Then
                BND = CInt(DT.Rows(0)(0).ToString)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Controles"
    Private Sub btEliminar_Click(sender As System.Object, e As System.EventArgs) Handles btEliminar.Click
        If Me.dgTecnicos.SelectedCells.Count > 0 Then
            LocClvTecnico = CInt(Me.dgTecnicos.SelectedCells(0).Value)
            UspEliminarDatosTecnico()
            LocClvTecnico = 0
            If BND = 1 Then
                MsgBox("El Técnico no puede ser eliminado por que tiene asignado Ordenes/Quejas pendientes", MsgBoxStyle.Information, "Técnicos Virtuales")
                Exit Sub
            Else
                MsgBox("El Técnico fue eliminado con Exito", MsgBoxStyle.Information, "Técnicos Virtuales")
            End If
        Else
            MsgBox("Seleccione un Técnico", MsgBoxStyle.Information, "Técnicos Virtuales")
        End If
        UspLlenaDatosTecnico(0, String.Empty)
    End Sub

    Private Sub btnAgregar_Click(sender As System.Object, e As System.EventArgs) Handles btnAgregar.Click
        Me.gbDatosTecnico.Visible = True
        Me.txtNombreTecnico.Text = ""
        LocTipo = "N"
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If LocTipo = "N" Then
            UspGuardaNombreTecnico(0)
            If BND = 1 Then
                MsgBox("El Nombre del técnico ya existe, favor de cambiarlo", MsgBoxStyle.Information, "Técnicos Virtuales")
                Exit Sub
            End If
        ElseIf LocTipo = "M" Then
            UspGuardaNombreTecnico(LocClvTecnico)
            If BND = 1 Then
                MsgBox("El Nombre del técnico ya existe, favor de cambiarlo", MsgBoxStyle.Information, "Técnicos Virtuales")
                Exit Sub
            End If
        End If
        MsgBox("Fue Guardado Con Exito.", MsgBoxStyle.Information, "Técnicos Virtuales")
        Me.gbDatosTecnico.Visible = False
        UspLlenaDatosTecnico(0, String.Empty)
    End Sub

    Private Sub btnModificar_Click(sender As System.Object, e As System.EventArgs) Handles btnModificar.Click
        If Me.dgTecnicos.SelectedCells.Count > 0 Then
            LocClvTecnico = CInt(Me.dgTecnicos.SelectedCells(0).Value)
            Me.gbDatosTecnico.Visible = True
            Me.txtNombreTecnico.Text = UspMuestraNombreTecnico.Rows(0)(0).ToString
            LocTipo = "M"
        Else
            MsgBox("Seleccione un Técnico", MsgBoxStyle.Information, "Técnicos Virtuales")
        End If

    End Sub

    Private Sub btnBuscarTecnico_Click(sender As System.Object, e As System.EventArgs) Handles btnBuscarTecnico.Click
        Dim ClaveTecnico As Integer
        If Len(txtBuscarClave.Text) = 0 Then
            ClaveTecnico = 0
        Else
            ClaveTecnico = txtBuscarClave.Text
        End If

        UspLlenaDatosTecnico(ClaveTecnico, Me.txtBuscarNombre.Text)
        Me.txtBuscarNombre.Text = ""
        Me.txtBuscarClave.Text = ""

    End Sub
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.gbDatosTecnico.Visible = False
    End Sub
#End Region

    
  
End Class