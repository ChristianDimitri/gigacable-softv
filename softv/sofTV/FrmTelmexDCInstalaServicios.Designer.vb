<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTelmexDCInstalaServicios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label15 As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim Label20 As System.Windows.Forms.Label
        Dim Label16 As System.Windows.Forms.Label
        Dim Label13 As System.Windows.Forms.Label
        Dim Label17 As System.Windows.Forms.Label
        Dim Label18 As System.Windows.Forms.Label
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.CONRELCLIBANCOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONRELCLIBANCOTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONRELCLIBANCOTableAdapter()
        Me.MUESTRATIPOSDECUENTABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRABANCOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRABANCOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRABANCOSTableAdapter()
        Me.MUESTRATIPOSDECUENTATableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRATIPOSDECUENTATableAdapter()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ObservacionesTxt = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.ProveedorPlataformaTxt = New System.Windows.Forms.TextBox()
        Me.ModeloModemCmb = New System.Windows.Forms.ComboBox()
        Me.MarcaModemCmb = New System.Windows.Forms.ComboBox()
        Me.TipoEntregaModemCmb = New System.Windows.Forms.ComboBox()
        Me.SerieModemTxt = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Label15 = New System.Windows.Forms.Label()
        Label11 = New System.Windows.Forms.Label()
        Label20 = New System.Windows.Forms.Label()
        Label16 = New System.Windows.Forms.Label()
        Label13 = New System.Windows.Forms.Label()
        Label17 = New System.Windows.Forms.Label()
        Label18 = New System.Windows.Forms.Label()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONRELCLIBANCOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRATIPOSDECUENTABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRABANCOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label15
        '
        Label15.AutoSize = True
        Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label15.ForeColor = System.Drawing.Color.LightSlateGray
        Label15.Location = New System.Drawing.Point(16, 21)
        Label15.Name = "Label15"
        Label15.Size = New System.Drawing.Size(184, 15)
        Label15.TabIndex = 424
        Label15.Text = "Tipo de entrega de modem:"
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label11.ForeColor = System.Drawing.Color.LightSlateGray
        Label11.Location = New System.Drawing.Point(272, 127)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(105, 15)
        Label11.TabIndex = 419
        Label11.Text = "Observaciones:"
        '
        'Label20
        '
        Label20.AutoSize = True
        Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label20.ForeColor = System.Drawing.Color.LightSlateGray
        Label20.Location = New System.Drawing.Point(277, 19)
        Label20.Name = "Label20"
        Label20.Size = New System.Drawing.Size(39, 15)
        Label20.TabIndex = 431
        Label20.Text = "SKU:"
        '
        'Label16
        '
        Label16.AutoSize = True
        Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label16.ForeColor = System.Drawing.Color.LightSlateGray
        Label16.Location = New System.Drawing.Point(16, 97)
        Label16.Name = "Label16"
        Label16.Size = New System.Drawing.Size(123, 15)
        Label16.TabIndex = 426
        Label16.Text = "Marca de modem:"
        '
        'Label13
        '
        Label13.AutoSize = True
        Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label13.ForeColor = System.Drawing.Color.LightSlateGray
        Label13.Location = New System.Drawing.Point(277, 74)
        Label13.Name = "Label13"
        Label13.Size = New System.Drawing.Size(169, 15)
        Label13.TabIndex = 421
        Label13.Text = "Proveedor de plataforma:"
        '
        'Label17
        '
        Label17.AutoSize = True
        Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label17.ForeColor = System.Drawing.Color.LightSlateGray
        Label17.Location = New System.Drawing.Point(16, 175)
        Label17.Name = "Label17"
        Label17.Size = New System.Drawing.Size(131, 15)
        Label17.TabIndex = 428
        Label17.Text = "Modelo de modem:"
        '
        'Label18
        '
        Label18.AutoSize = True
        Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label18.ForeColor = System.Drawing.Color.LightSlateGray
        Label18.Location = New System.Drawing.Point(16, 255)
        Label18.Name = "Label18"
        Label18.Size = New System.Drawing.Size(117, 15)
        Label18.TabIndex = 430
        Label18.Text = "Serie de modem:"
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CONRELCLIBANCOBindingSource
        '
        Me.CONRELCLIBANCOBindingSource.DataMember = "CONRELCLIBANCO"
        Me.CONRELCLIBANCOBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONRELCLIBANCOTableAdapter
        '
        Me.CONRELCLIBANCOTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATIPOSDECUENTABindingSource
        '
        Me.MUESTRATIPOSDECUENTABindingSource.DataMember = "MUESTRATIPOSDECUENTA"
        Me.MUESTRATIPOSDECUENTABindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRABANCOSBindingSource
        '
        Me.MUESTRABANCOSBindingSource.DataMember = "MUESTRABANCOS"
        Me.MUESTRABANCOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRABANCOSTableAdapter
        '
        Me.MUESTRABANCOSTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATIPOSDECUENTATableAdapter
        '
        Me.MUESTRATIPOSDECUENTATableAdapter.ClearBeforeFill = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Label15)
        Me.Panel1.Controls.Add(Label11)
        Me.Panel1.Controls.Add(Me.ObservacionesTxt)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(Me.ProveedorPlataformaTxt)
        Me.Panel1.Controls.Add(Me.ModeloModemCmb)
        Me.Panel1.Controls.Add(Label20)
        Me.Panel1.Controls.Add(Label16)
        Me.Panel1.Controls.Add(Label13)
        Me.Panel1.Controls.Add(Label17)
        Me.Panel1.Controls.Add(Label18)
        Me.Panel1.Controls.Add(Me.MarcaModemCmb)
        Me.Panel1.Controls.Add(Me.TipoEntregaModemCmb)
        Me.Panel1.Controls.Add(Me.SerieModemTxt)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(528, 318)
        Me.Panel1.TabIndex = 0
        Me.Panel1.TabStop = True
        '
        'ObservacionesTxt
        '
        Me.ObservacionesTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ObservacionesTxt.Location = New System.Drawing.Point(275, 145)
        Me.ObservacionesTxt.MaxLength = 250
        Me.ObservacionesTxt.Multiline = True
        Me.ObservacionesTxt.Name = "ObservacionesTxt"
        Me.ObservacionesTxt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.ObservacionesTxt.Size = New System.Drawing.Size(224, 157)
        Me.ObservacionesTxt.TabIndex = 420
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(275, 38)
        Me.TextBox1.MaxLength = 50
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TextBox1.Size = New System.Drawing.Size(224, 22)
        Me.TextBox1.TabIndex = 432
        '
        'ProveedorPlataformaTxt
        '
        Me.ProveedorPlataformaTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ProveedorPlataformaTxt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ProveedorPlataformaTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ProveedorPlataformaTxt.Location = New System.Drawing.Point(275, 92)
        Me.ProveedorPlataformaTxt.Name = "ProveedorPlataformaTxt"
        Me.ProveedorPlataformaTxt.Size = New System.Drawing.Size(224, 21)
        Me.ProveedorPlataformaTxt.TabIndex = 422
        '
        'ModeloModemCmb
        '
        Me.ModeloModemCmb.DisplayMember = "Modelo"
        Me.ModeloModemCmb.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ModeloModemCmb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ModeloModemCmb.ForeColor = System.Drawing.Color.Black
        Me.ModeloModemCmb.FormattingEnabled = True
        Me.ModeloModemCmb.Location = New System.Drawing.Point(19, 193)
        Me.ModeloModemCmb.Name = "ModeloModemCmb"
        Me.ModeloModemCmb.Size = New System.Drawing.Size(224, 21)
        Me.ModeloModemCmb.TabIndex = 427
        Me.ModeloModemCmb.ValueMember = "IdModelo"
        '
        'MarcaModemCmb
        '
        Me.MarcaModemCmb.DisplayMember = "Marca"
        Me.MarcaModemCmb.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MarcaModemCmb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MarcaModemCmb.ForeColor = System.Drawing.Color.Black
        Me.MarcaModemCmb.FormattingEnabled = True
        Me.MarcaModemCmb.Location = New System.Drawing.Point(19, 115)
        Me.MarcaModemCmb.Name = "MarcaModemCmb"
        Me.MarcaModemCmb.Size = New System.Drawing.Size(224, 21)
        Me.MarcaModemCmb.TabIndex = 425
        Me.MarcaModemCmb.ValueMember = "IdMarca"
        '
        'TipoEntregaModemCmb
        '
        Me.TipoEntregaModemCmb.DisplayMember = "TipoEntrega"
        Me.TipoEntregaModemCmb.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.TipoEntregaModemCmb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TipoEntregaModemCmb.ForeColor = System.Drawing.Color.Black
        Me.TipoEntregaModemCmb.FormattingEnabled = True
        Me.TipoEntregaModemCmb.Location = New System.Drawing.Point(19, 38)
        Me.TipoEntregaModemCmb.Name = "TipoEntregaModemCmb"
        Me.TipoEntregaModemCmb.Size = New System.Drawing.Size(224, 21)
        Me.TipoEntregaModemCmb.TabIndex = 423
        Me.TipoEntregaModemCmb.ValueMember = "IdTipoEntrega"
        '
        'SerieModemTxt
        '
        Me.SerieModemTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SerieModemTxt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.SerieModemTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SerieModemTxt.Location = New System.Drawing.Point(19, 273)
        Me.SerieModemTxt.MaxLength = 50
        Me.SerieModemTxt.Name = "SerieModemTxt"
        Me.SerieModemTxt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SerieModemTxt.Size = New System.Drawing.Size(224, 22)
        Me.SerieModemTxt.TabIndex = 429
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Orange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(404, 349)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(136, 36)
        Me.Button3.TabIndex = 434
        Me.Button3.Text = "Cancelar"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Orange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(251, 349)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 433
        Me.Button2.Text = "&Aceptar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'FrmTelmexDCInstalaServicios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(561, 406)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button3)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmTelmexDCInstalaServicios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Datos Telmex"
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONRELCLIBANCOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRATIPOSDECUENTABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRABANCOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONRELCLIBANCOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONRELCLIBANCOTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONRELCLIBANCOTableAdapter
    Friend WithEvents MUESTRABANCOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRABANCOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRABANCOSTableAdapter
    Friend WithEvents MUESTRATIPOSDECUENTABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRATIPOSDECUENTATableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRATIPOSDECUENTATableAdapter
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents ObservacionesTxt As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents ProveedorPlataformaTxt As System.Windows.Forms.TextBox
    Friend WithEvents ModeloModemCmb As System.Windows.Forms.ComboBox
    Friend WithEvents MarcaModemCmb As System.Windows.Forms.ComboBox
    Friend WithEvents TipoEntregaModemCmb As System.Windows.Forms.ComboBox
    Friend WithEvents SerieModemTxt As System.Windows.Forms.TextBox
End Class
