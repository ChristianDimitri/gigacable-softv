﻿Imports System.Text
Imports System.Data.SqlClient
Public Class FrmRecontratacion

    Dim Res As Integer = 0
    Dim Msj As String = String.Empty
    Dim eContratoNet As Long = 0
    Dim eClv_UnicaNet As Integer = 0
    Dim eId As Integer = 0
    Dim eClv_Cablemodem As Integer = 0
    Dim eClv_Servicio As Integer = 0
    Dim Op As Boolean = False
    Dim aparatoNet As Boolean = False
    Dim aparatoDig As Boolean = False
    Dim Tipo As String = ""
    Dim ContratoNetComparte As Integer = 0
    Dim Id As Integer = 0
    Dim IdDetalle As Integer = 0

    Private Sub MuestraInfoCliente(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("MuestraInfoCliente", conexion)
        comando.CommandType = CommandType.StoredProcedure
        Dim reader As SqlDataReader

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            reader = comando.ExecuteReader
            While (reader.Read())
                Me.LabelNombre.Text = reader(0).ToString()
                Me.LabelCalle.Text = reader(1).ToString()
                Me.LabelNumero.Text = reader(2).ToString()
                Me.LabelColonia.Text = reader(3).ToString()
                Me.LabelCiudad.Text = reader(4).ToString()
                Me.LabelTelefono.Text = reader(5).ToString()
                Me.LabelCelular.Text = reader(6).ToString()
                Me.CheckBoxSoloInternet.Checked = reader(7).ToString
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Limpiar()

        Me.LabelNombre.Text = ""
        Me.LabelCalle.Text = ""
        Me.LabelNumero.Text = ""
        Me.LabelColonia.Text = ""
        Me.LabelCiudad.Text = ""
        Me.LabelTelefono.Text = ""
        Me.LabelCelular.Text = ""
        Me.CheckBoxSoloInternet.Checked = False
        tvTV.Nodes.Clear()
        tvNET.Nodes.Clear()
        tvDIG.Nodes.Clear()
        tvTel.Nodes.Clear()
        lblTV.Text = ""
        lblNET.Text = ""
        lblDIG.Text = ""
        lblTel.Text = ""
        eContratoNet = 0
        eClv_Servicio = 0
        eClv_UnicaNet = 0

        BorReconSession(eClv_Session)

    End Sub

    Private Sub ButtonBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBuscar.Click
        GLOCONTRATOSEL = 0
        FrmSelCliente.Show()
    End Sub

    Private Sub TextBoxContrato_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBoxContrato.KeyDown
        If (e.KeyCode <> Keys.Enter) Then
            Exit Sub
        End If
        If IsNumeric(Me.TextBoxContrato.Text) = False Then
            Exit Sub
        End If
        If CInt(Me.TextBoxContrato.Text) <= 0 Then
            Exit Sub
        End If

        MuestraInfoCliente(TextBoxContrato.Text)
        eContrato = TextBoxContrato.Text
        tcServicios.Enabled = True


        If CheckBoxSoloInternet.Checked = True Then
            If tcServicios.TabPages.Count > 1 Then
                tcServicios.TabPages.Remove(tpDIG)
                eClv_TipSer = 2
            End If
        Else
            If tcServicios.TabPages.Count = 1 Then
                tcServicios.TabPages.Remove(tpNET)
                tcServicios.TabPages.Add(tpDIG)
                tcServicios.TabPages.Add(tpNET)
                eClv_TipSer = 3
            End If
        End If

        ValidaServ(eContrato, eClv_TipSer)

        'If Res = 1 Then
        '    Exit Sub
        'End If



        DameClv_Session()
        NueDetReconNetTmp(0, eClv_Session, eContrato, String.Empty, 0, String.Empty, String.Empty, 0, 0)
        NueDetReconDigTmp(0, eClv_Session, eContrato, String.Empty, 0, 0)
        NueDetReconTelTmp(0, eClv_Session, eContrato, 0, 0)
        LlenatvNet()
        LlenatvDig()
        LlenatvTel()

        aparatoDig = False
        If tvDIG.Nodes.Count > 0 Then aparatoDig = True

        aparatoNet = False
        If tvNET.Nodes.Count > 0 Then aparatoNet = True

    End Sub

    Private Sub TextBoxContrato_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxContrato.KeyPress
        e.KeyChar = Chr(ValidaKey(TextBoxContrato, Asc(LCase(e.KeyChar)), "N"))
    End Sub

    Private Sub TextBoxContrato_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxContrato.TextChanged
        Limpiar()
    End Sub

    Private Sub ValidaServ(ByVal Contrato As Long, ByVal Clv_TipSer As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaServReCon", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_TipSer
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Res", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Res = 0
            Msj = String.Empty
            Res = parametro3.Value
            Msj = parametro4.Value

            If Res > 0 Then
                Op = False
                If eClv_TipSer = 2 Then lblNET.Text = parametro4.Value.ToString
                If eClv_TipSer = 3 Then lblDIG.Text = parametro4.Value.ToString
                If eClv_TipSer = 5 Then lblTel.Text = parametro4.Value.ToString
            Else
                Op = True

                If eClv_TipSer = 2 Then lblNET.Text = ""
                If eClv_TipSer = 3 Then lblDIG.Text = ""
                If eClv_TipSer = 5 Then lblTel.Text = ""
            End If



            BloquearBN(Clv_TipSer, Op)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try


    End Sub

    Private Sub BloquearBN(ByVal Clv_TipSer As Integer, ByVal Op As Boolean)
        If Clv_TipSer = 2 Then
            bnNETCablemodem.Enabled = Op
            bnNETServicio.Enabled = Op
        ElseIf Clv_TipSer = 3 Then
            bnDIGTarjeta.Enabled = Op
            bnDIGServicio.Enabled = Op
        ElseIf Clv_TipSer = 5 Then
            bnPlanTel.Enabled = Op
            bnServicioTel.Enabled = Op
            bnPaqueteTel.Enabled = Op
        End If
    End Sub

    Private Function ConDetReconTVTmp(ByVal ClvSession As Long) As DataTable
        Try
            Dim conexion As New SqlConnection(MiConexion)
            Dim strSQL As New StringBuilder

            strSQL.Append("EXEC ConDetReconTVTmp " & CStr(ClvSession))

            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
            Dim dataTable As New DataTable

            dataAdapter.Fill(dataTable)

            Return dataTable

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Function

    Private Function ConDetReconNetTmp(ByVal Op As Integer, ByVal ClvSession As Long, ByVal ContratoNet As Long) As DataTable
        Try
            Dim conexion As New SqlConnection(MiConexion)
            Dim strSQL As New StringBuilder
            strSQL.Append("EXEC ConDetReconNetTmp " & CStr(Op) & ", " & CStr(ClvSession) & ", " & CStr(ContratoNet))

            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
            Dim dataTable As New DataTable

            dataAdapter.Fill(dataTable)

            Return dataTable

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Function

    Private Function ConDetReconDigTmp(ByVal Op As Integer, ByVal ClvSession As Long, ByVal ContratoNet As Long) As DataTable
        Try
            Dim conexion As New SqlConnection(MiConexion)
            Dim strSQL As New StringBuilder
            strSQL.Append("EXEC ConDetReconDigTmp " & CStr(Op) & ", " & CStr(ClvSession) & ", " & CStr(ContratoNet))

            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
            Dim dataTable As New DataTable

            dataAdapter.Fill(dataTable)

            Return dataTable

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Function

    Private Sub NueDetReconTVTmp(ByVal ClvSession As Long, ByVal Contrato As Long, ByVal TVSinPago As Integer, ByVal TVConPago As Integer, ByVal Clv_Servicio As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueDetReconTVTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Contrato
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@TVSinPago", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = TVSinPago
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@TVConPago", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = TVConPago
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = Clv_Servicio
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Res", SqlDbType.Int)
        parametro6.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro7.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro7)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Res = 0
            Msj = String.Empty
            Res = parametro6.Value
            Msj = parametro7.Value
            If Res = 1 Then
                MsgBox(Msj, MsgBoxStyle.Exclamation)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub NueDetReconNetTmp(ByVal Op As Integer, ByVal ClvSession As Long, ByVal Contrato As Long, ByVal Agregar As Char, ByVal ContratoNet As Long, ByVal TipoCablemodem As Char, ByVal TipoAdquisicion As Char, ByVal TipoServicio As Integer, ByVal Clv_Servicio As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueDetReconNetTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Op", SqlDbType.Int)
        par.Direction = ParameterDirection.Input
        par.Value = Op
        comando.Parameters.Add(par)

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Contrato
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Agregar", SqlDbType.VarChar, 1)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Agregar
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = ContratoNet
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@TipoCablemodem", SqlDbType.VarChar, 1)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = TipoCablemodem
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@TipoAdquisicion", SqlDbType.VarChar, 1)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = TipoAdquisicion
        comando.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@TipoServicio", SqlDbType.VarChar, 1)
        parametro7.Direction = ParameterDirection.Input
        parametro7.Value = TipoServicio
        comando.Parameters.Add(parametro7)

        Dim parametro8 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        parametro8.Direction = ParameterDirection.Input
        parametro8.Value = Clv_Servicio
        comando.Parameters.Add(parametro8)

        Dim parametro9 As New SqlParameter("@Res", SqlDbType.Int)
        parametro9.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro9)

        Dim parametro10 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro10.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro10)

        Dim parametro11 As New SqlParameter("@esFtth", SqlDbType.Int)
        parametro11.Direction = ParameterDirection.Input
        parametro11.Value = EsFtth
        comando.Parameters.Add(parametro11)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Res = 0
            Msj = String.Empty
            Res = parametro9.Value
            Msj = parametro10.Value
            If Res = 1 Then
                MsgBox(Msj, MsgBoxStyle.Exclamation)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub NueDetReconDigTmp(ByVal op As Integer, ByVal ClvSession As Long, ByVal Contrato As Long, ByVal Agregar As Char, ByVal ContratoNet As Long, ByVal Clv_Servicio As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueDetReconDigTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Op", SqlDbType.Int)
        par.Direction = ParameterDirection.Input
        par.Value = op
        comando.Parameters.Add(par)

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Contrato
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Agregar", SqlDbType.VarChar, 1)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Agregar
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = ContratoNet
        comando.Parameters.Add(parametro4)

        Dim parametro8 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        parametro8.Direction = ParameterDirection.Input
        parametro8.Value = Clv_Servicio
        comando.Parameters.Add(parametro8)

        Dim parametro9 As New SqlParameter("@Res", SqlDbType.Int)
        parametro9.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro9)

        Dim parametro10 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro10.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro10)

        Dim parametro11 As New SqlParameter("@esFtth", SqlDbType.Int)
        parametro11.Direction = ParameterDirection.Input
        parametro11.Value = EsFtth
        comando.Parameters.Add(parametro11)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Res = 0
            Msj = String.Empty
            Res = parametro9.Value
            Msj = parametro10.Value
            If Res = 1 Then
                MsgBox(Msj, MsgBoxStyle.Exclamation)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub BorDetReconTVTmp(ByVal ClvSession As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorDetReconTVTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub BorDetReconNetTmp(ByVal ClvSession As Long, ByVal Eliminar As Char, ByVal ContratoNet As Long, ByVal Clv_Servicio As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorDetReconNetTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Eliminar", SqlDbType.VarChar, 1)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Eliminar
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = ContratoNet
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Clv_Servicio
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Res", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro6.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro6)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Res = 0
            Msj = String.Empty
            Res = parametro5.Value
            Msj = parametro6.Value
            If Res = 1 Then
                MsgBox(Msj, MsgBoxStyle.Exclamation)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try


    End Sub

    Private Sub BorDetReconDigTmp(ByVal ClvSession As Long, ByVal Eliminar As Char, ByVal ContratoNet As Long, ByVal Clv_Servicio As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorDetReconDigTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Eliminar", SqlDbType.VarChar, 1)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Eliminar
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = ContratoNet
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Clv_Servicio
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Res", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro6.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro6)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Res = 0
            Msj = String.Empty
            Res = parametro5.Value
            Msj = parametro6.Value
            If Res = 1 Then
                MsgBox(Msj, MsgBoxStyle.Exclamation)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try


    End Sub

    Private Sub ValidaServReCon(ByVal Contrato As Long, ByVal Clv_TipSer As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaServReCon", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_TipSer
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Res", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Res = 0
            Msj = String.Empty
            Res = parametro3.Value
            Msj = parametro4.Value
            If Res = 1 Then
                MsgBox(Msj, MsgBoxStyle.Exclamation)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Function MuestraTipoCablemodem() As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraTipoCablemodem")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable

        dataAdapter.Fill(dataTable)

        Return dataTable

    End Function

    Private Function MuestraTipoAsignacion() As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraTipoAsignacion")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable

        dataAdapter.Fill(dataTable)

        Return dataTable

    End Function

    Private Function MuestraTipSerInternet() As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraTipSerInternet")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable

        dataAdapter.Fill(dataTable)

        Return dataTable
    End Function

    Private Function MuestraServicioRecon(ByVal ClvSession As Long, ByVal Clv_TipSer As Integer, ByVal ContratoNet As Long) As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraServicioRecon " & CStr(ClvSession) & ", " & CStr(Clv_TipSer) & ", " & CStr(ContratoNet) & ", " & CStr(eContrato))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable

        dataAdapter.Fill(dataTable)

        Return dataTable

    End Function

    Private Sub LlenatvTel()
        Try
            Dim dtA As New DataTable
            Dim dtS As New DataTable
            Dim a As Integer = 0
            Dim s As Integer = 0

            dtA = CONDetReconTel(eClv_Session)

            tvTel.Nodes.Clear()

            For Each aparato As DataRow In dtA.Rows

                tvTel.Nodes.Add(aparato("Descripcion").ToString())
                tvTel.Nodes(a).Tag = aparato("Id").ToString()
                dtS = CONDetReconTelAdicTmp(eClv_Session, aparato("Id").ToString())

                For Each servicio As DataRow In dtS.Rows
                    tvTel.Nodes(a).Nodes.Add(servicio("DESCRIPCION").ToString()).ForeColor = Color.Blue
                    tvTel.Nodes(a).Nodes(s).Tag = servicio("IdDetalle").ToString()
                    s += 1

                Next

                tvTel.Nodes(a).ExpandAll()
                a += 1
                s = 0
            Next



        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub LlenatvNet()
        Try
            Dim dtA As New DataTable
            Dim dtS As New DataTable
            Dim a As Integer = 0
            Dim s As Integer = 0

            dtA = ConDetReconNetTmp(0, eClv_Session, 0)

            tvNET.Nodes.Clear()

            For Each aparato As DataRow In dtA.Rows

                tvNET.Nodes.Add(aparato("Aparato").ToString())
                tvNET.Nodes(a).Tag = aparato("ContratoNet").ToString()
                dtS = ConDetReconNetTmp(1, eClv_Session, aparato("ContratoNet").ToString())

                For Each servicio As DataRow In dtS.Rows
                    tvNET.Nodes(a).Nodes.Add(servicio("Servicio").ToString()).ForeColor = Color.Blue
                    tvNET.Nodes(a).Nodes(s).Tag = servicio("Clv_Servicio").ToString()
                    s += 1

                Next

                tvNET.Nodes(a).ExpandAll()
                a += 1
                s = 0
            Next



        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub LlenatvDig()
        Try
            Dim dtA As New DataTable
            Dim dtS As New DataTable
            Dim a As Integer = 0
            Dim s As Integer = 0

            dtA = ConDetReconDigTmp(0, eClv_Session, 0)

            tvDIG.Nodes.Clear()

            For Each aparato As DataRow In dtA.Rows
                tvDIG.Nodes.Add(aparato("Aparato").ToString())
                tvDIG.Nodes(a).Tag = aparato("ContratoNet").ToString()
                dtS = ConDetReconDigTmp(1, eClv_Session, aparato("ContratoNet"))
                For Each servicio As DataRow In dtS.Rows
                    tvDIG.Nodes(a).Nodes.Add(servicio("Servicio").ToString()).ForeColor = Color.Blue
                    tvDIG.Nodes(a).Nodes(s).Tag = servicio("Clv_Servicio").ToString()
                    s += 1
                Next

                tvDIG.Nodes(a).ExpandAll()

                a += 1
                s = 0

            Next


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub GrabaReContratacion(ByVal Contrato As Long, ByVal Clv_Usuario As String, ByVal ClvSession As Long, ByVal SoloInternet As Boolean)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("GrabaReContratacion", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar, 5)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Usuario
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = ClvSession
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@SoloInternet", SqlDbType.Bit)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = SoloInternet
        comando.Parameters.Add(parametro4)


        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            MsgBox(mensaje5, MsgBoxStyle.Information)
            Limpiar()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub BorReconSession(ByVal ClvSession As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorReconSession", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub DameClv_Session()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameClv_Session", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eClv_Session = parametro.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub FrmRecontratacion_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GLOCONTRATOSEL > 0 Then
            Me.TextBoxContrato.Text = GLOCONTRATOSEL
            GLOCONTRATOSEL = 0
            eContrato = Me.TextBoxContrato.Text

            MuestraInfoCliente(TextBoxContrato.Text)
            eContrato = TextBoxContrato.Text
            DameClv_Session()
            NueDetReconNetTmp(0, eClv_Session, eContrato, String.Empty, 0, String.Empty, String.Empty, 0, 0)
            NueDetReconDigTmp(0, eClv_Session, eContrato, String.Empty, 0, 0)
            NueDetReconTelTmp(0, eClv_Session, eContrato, 0, 0)
            LlenatvNet()
            LlenatvDig()
            LlenatvTel()
            tcServicios.Enabled = True

        End If
    End Sub

    Private Sub FrmRecontratacion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        eClv_Session = 0
        eClv_TipSer = 3
        eContrato = 0
        eContratoNet = 0
        eClv_UnicaNet = 0
        eClv_Servicio = 0
        GLOCONTRATOSEL = 0
        comboTipoCa.DataSource = MuestraTipoCablemodem()
        comboTipoAs.DataSource = MuestraTipoAsignacion()
        comboTipoSer.DataSource = MuestraTipSerInternet()
        comboSerNET.DataSource = MuestraServicioRecon(eClv_Session, 2, 0)
        comboSerDIG.DataSource = MuestraServicioRecon(eClv_Session, 3, 0)
        comboSerTel.DataSource = MuestraServicioRecon(eClv_Session, 5, 0)

        tcServicios.TabPages.Remove(tpTV)

    End Sub

    Private Sub tcServicios_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tcServicios.SelectedIndexChanged
        If tcServicios.TabPages.Count = 0 Then
            Exit Sub
        End If
        If tcServicios.SelectedTab.Name = "tpTV" Then
            eClv_TipSer = 1
        ElseIf tcServicios.SelectedTab.Name = "tpNET" Then
            eClv_TipSer = 2
        ElseIf tcServicios.SelectedTab.Name = "tpDIG" Then
            eClv_TipSer = 3
        ElseIf tcServicios.SelectedTab.Name = "tpTel" Then
            eClv_TipSer = 5
        End If

        eContratoNet = 0
        eClv_Servicio = 0
        eClv_UnicaNet = 0

        If eContrato > 0 Then ValidaServ(eContrato, eClv_TipSer)
    End Sub

    Private Sub tsbAgregarNET_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAgregarNET.Click
        panelCNET.Visible = True
        tsbEliminarNET.Enabled = False
    End Sub

    Private Sub tsbEliminarNET_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbEliminarNET.Click
        If eContratoNet = 0 Then
            MsgBox("Selecciona un Cablemodem.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        BorDetReconNetTmp(eClv_Session, "A", eContratoNet, 0)
        LlenatvNet()
        eContratoNet = 0
    End Sub

    Private Sub btnCNETAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCNETAceptar.Click
        If comboTipoCa.SelectedIndex <= -1 Then
            MsgBox("Selecciona un Tipo de Cablemodem.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If comboTipoAs.SelectedIndex <= -1 Then
            MsgBox("Selecciona un Tipo de Asignación.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If comboTipoSer.SelectedValue <= -1 Then
            MsgBox("Selecciona un Tipo de Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If

        NueDetReconNetTmp(1, eClv_Session, eContrato, "A", 0, comboTipoCa.SelectedValue, comboTipoAs.SelectedValue, comboTipoSer.SelectedValue, 0)
        LlenatvNet()
        panelCNET.Visible = False
        tsbEliminarNET.Enabled = True

    End Sub

    Private Sub btnCNETCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCNETCancelar.Click
        panelCNET.Visible = False
        tsbEliminarNET.Enabled = True
    End Sub

    Private Sub tsbAgregarSNET_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAgregarSNET.Click
        If eContratoNet = 0 Then
            MsgBox("Selecciona en que Cablemodem se agregará el Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If
        panelSNET.Visible = True
        tsbEliminarSNET.Enabled = False
        comboSerNET.DataSource = MuestraServicioRecon(eClv_Session, 2, 0)
    End Sub

    Private Sub tsbEliminarSNET_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbEliminarSNET.Click
        If eClv_Servicio = 0 Then
            MsgBox("Selecciona un Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If
        BorDetReconNetTmp(eClv_Session, "S", eContratoNet, eClv_Servicio)
        eClv_Servicio = 0
        LlenatvNet()
    End Sub

    Private Sub btnSNETAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSNETAceptar.Click
        If comboSerNET.SelectedIndex <= -1 Then
            MsgBox("Selecciona un Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If
        'Pregunta FTTH
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, eContrato)
        BaseII.CreateMyParameter("@CLVSESSION", SqlDbType.BigInt, eClv_Session)
        BaseII.CreateMyParameter("@CONTRATONET", SqlDbType.BigInt, eContratoNet)
        BaseII.CreateMyParameter("@CualesMediosTiene", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("PreguntaEsFtthRecontratacion")
        Dim CualesMediosTiene As Integer = CInt(BaseII.dicoPar("@CualesMediosTiene"))
        If CualesMediosTiene = 0 Then
            EsFtth = False
        ElseIf CualesMediosTiene = 1 Then
            EsFtth = True
        Else
            If MsgBox("¿El servicio es FTTH?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                EsFtth = False
            Else
                EsFtth = True
            End If

        End If


        NueDetReconNetTmp(1, eClv_Session, 0, "S", eContratoNet, String.Empty, String.Empty, 0, comboSerNET.SelectedValue)

        LlenatvNet()

        eContratoNet = 0
        panelSNET.Visible = False
        tsbEliminarSNET.Enabled = True
    End Sub

    Private Sub btnSNETCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSNETCancelar.Click
        eContratoNet = 0
        panelSNET.Visible = False
        tsbEliminarSNET.Enabled = True
    End Sub

    Private Sub tsbAgregarTDIG_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAgregarTDIG.Click
        NueDetReconDigTmp(1, eClv_Session, eContrato, "A", 0, 0)
        LlenatvDig()
    End Sub

    Private Sub tsbEliminarTDIG_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbEliminarTDIG.Click
        If eContratoNet = 0 Then
            MsgBox("Selecciona una Tarjeta.", MsgBoxStyle.Information)
            Exit Sub
        End If
        BorDetReconDigTmp(eClv_Session, "A", eContratoNet, 0)
        LlenatvDig()

        eContratoNet = 0

    End Sub

    Private Sub tsbAgregarSDIG_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAgregarSDIG.Click
        If eContratoNet = 0 Then
            MsgBox("Selecciona una Tarjeta a la que se le agregará el Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If
        panelDIG.Visible = True
        tsbEliminarTDIG.Enabled = False
        comboSerDIG.DataSource = MuestraServicioRecon(eClv_Session, 3, eContratoNet)
    End Sub

    Private Sub tsbEliminarSDIG_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbEliminarSDIG.Click
        If eClv_Servicio = 0 Then
            MsgBox("Seleccion un Servicio.", MsgBoxStyle.Information)
        End If
        BorDetReconDigTmp(eClv_Session, "S", eContratoNet, eClv_Servicio)
        LlenatvDig()
        eClv_Servicio = 0
    End Sub

    Private Sub btnDIGAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDIGAceptar.Click
        If comboSerDIG.SelectedIndex <= -1 Then
            MsgBox("Selecciona un Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If

        'Pregunta FTTH
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, eContrato)
        BaseII.CreateMyParameter("@CualesMediosTiene", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("PreguntaEsFtthRecontratacionDig")
        Dim CualesMediosTiene As Integer = CInt(BaseII.dicoPar("@CualesMediosTiene"))
        If CualesMediosTiene = 0 Then
            EsFtth = False
        ElseIf CualesMediosTiene = 1 Then
            EsFtth = True
        Else
            If MsgBox("¿El servicio es FTTH?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                EsFtth = False
            Else
                EsFtth = True
            End If

        End If

        NueDetReconDigTmp(1, eClv_Session, eContrato, "S", eContratoNet, comboSerDIG.SelectedValue)
        LlenatvDig()
        panelDIG.Visible = False
        tsbEliminarTDIG.Enabled = True
    End Sub

    Private Sub btnDIGCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDIGCancelar.Click
        panelDIG.Visible = False
        tsbEliminarTDIG.Enabled = True
    End Sub

    Private Sub tvNET_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvNET.AfterSelect
        If IsNumeric(e.Node.Tag) = False Then
            Exit Sub
        End If
        If e.Node.Level = 0 Then
            eContratoNet = CLng(e.Node.Tag)
        Else
            eContratoNet = CLng(e.Node.Parent.Tag)
            eClv_Servicio = CInt(e.Node.Tag)
        End If
    End Sub

    Private Sub tvDIG_AfterSelect(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvDIG.AfterSelect
        If IsNumeric(e.Node.Tag) = False Then
            Exit Sub
        End If
        If e.Node.Level = 0 Then
            eContratoNet = CLng(e.Node.Tag)
        Else
            eContratoNet = CLng(e.Node.Parent.Tag)
            eClv_Servicio = CInt(e.Node.Tag)
        End If
    End Sub


    Private Sub tsbRecon_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbRecon.Click

        ChecaAntesGrabaRecontratacion(eClv_Session, TextBoxContrato.Text, CheckBoxSoloInternet.Checked, aparatoNet, aparatoDig)

        If eRes = 1 Then
            MsgBox(eMsj, MsgBoxStyle.Information)
            Exit Sub
        End If

        If eRes = 2 Then
            Dim r As String
            r = MsgBox(eMsj, MsgBoxStyle.YesNo).ToString
            If r = "Yes" Then
                GrabaReContratacion(eContrato, GloUsuario, eClv_Session, True)
                Exit Sub
            Else
                MsgBox("Agrega Servicios Digitales", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If

        GrabaReContratacion(eContrato, GloUsuario, eClv_Session, CheckBoxSoloInternet.Checked)

    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub ChecaAntesGrabaRecontratacion(ByVal ClvSession As Long, ByVal Contrato As Integer, ByVal SoloInternet As Boolean, ByVal aparatoNet As Boolean, ByVal aparatoDig As Boolean)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ChecaAntesGrabaRecontratacion", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Contrato", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = Contrato
        comando.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@SoloInternet", SqlDbType.Bit)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = SoloInternet
        comando.Parameters.Add(parametro2)

        Dim parametro5 As New SqlParameter("@aparatoNet", SqlDbType.Bit)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = aparatoNet
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@aparatoDig", SqlDbType.Bit)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = aparatoDig
        comando.Parameters.Add(parametro6)

        Dim parametro3 As New SqlParameter("@Res", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = 0
            eMsj = String.Empty
            eRes = parametro3.Value
            eMsj = parametro4.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub MUESTRAPaqueteAdicional()
        BaseII.limpiaParametros()
        comboSerTelAdic.Text = ""
        comboSerTelAdic.DataSource = BaseII.ConsultaDT("MUESTRAPaqueteAdicional")
    End Sub

    Private Sub MUESTRAServicioDigital()
        BaseII.limpiaParametros()
        comboSerTelAdic.Text = ""
        comboSerTelAdic.DataSource = BaseII.ConsultaDT("MUESTRAServicioDigital")
    End Sub

    Private Function CONDetReconTel(ByVal ClvSession As Integer) As DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ClvSession", SqlDbType.Int, ClvSession)
        CONDetReconTel = BaseII.ConsultaDT("CONDetReconTel")
    End Function

    Private Function CONDetReconTelAdicTmp(ByVal ClvSession As Integer, ByVal Id As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ClvSession", SqlDbType.Int, ClvSession)
        BaseII.CreateMyParameter("@Id ", SqlDbType.Int, Id)
        CONDetReconTelAdicTmp = BaseII.ConsultaDT("CONDetReconTelAdicTmp")
    End Function

    Private Sub BORDetReconTelTmp(ByVal ClvSession As Integer, ByVal Id As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ClvSession", SqlDbType.Int, ClvSession)
        BaseII.CreateMyParameter("@Id ", SqlDbType.Int, Id)
        BaseII.Inserta("BORDetReconTelTmp")
    End Sub

    Private Sub BORDetReconTelAdicTmp(ByVal ClvSession As Integer, ByVal IdDetalle As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ClvSession", SqlDbType.Int, ClvSession)
        BaseII.CreateMyParameter("@IdDetalle", SqlDbType.Int, IdDetalle)
        BaseII.Inserta("BORDetReconTelAdicTmp")
    End Sub

    Private Sub NueDetReconTelTmp(ByVal Op As Integer, ByVal ClvSession As Integer, ByVal Contrato As Integer, ByVal Clv_Servicio As Integer, ByVal CONTRATONETCOMPARTE As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@CLVSESSION", SqlDbType.Int, ClvSession)
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.Int, Contrato)
        BaseII.CreateMyParameter("@CLV_SERVICIO", SqlDbType.Int, Clv_Servicio)
        BaseII.CreateMyParameter("@CONTRATONETCOMPARTE", SqlDbType.Int, CONTRATONETCOMPARTE)
        BaseII.Inserta("NueDetReconTelTmp")
    End Sub

    Private Sub NUEDetReconTelAdicTmp(ByVal Id As Integer, ByVal ClvSession As Integer, ByVal Tipo As String, ByVal Clv_Servicio As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Id", SqlDbType.Int, Id)
        BaseII.CreateMyParameter("@ClvSession", SqlDbType.Int, ClvSession)
        BaseII.CreateMyParameter("@Tipo", SqlDbType.VarChar, Tipo, 1)
        BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, Clv_Servicio)
        BaseII.CreateMyParameter("@Res", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("NUEDetReconTelAdicTmp")
        eRes = 0
        eRes = CInt(BaseII.dicoPar("@Res").ToString())
    End Sub

    Private Sub VALIDADetReconTelCompartenATA(ByVal ClvSession As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLVSESSION", SqlDbType.Int, ClvSession)
        BaseII.CreateMyParameter("@CONTRATONETCOMPARTE", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("VALIDADetReconTelCompartenATA")
        ContratoNetComparte = 0
        ContratoNetComparte = CInt(BaseII.dicoPar("@CONTRATONETCOMPARTE").ToString())
    End Sub

    Private Sub tvTel_AfterSelect(sender As System.Object, e As System.Windows.Forms.TreeViewEventArgs) Handles tvTel.AfterSelect
        If IsNumeric(e.Node.Tag) = False Then
            Exit Sub
        End If
        If e.Node.Level = 0 Then
            Id = CLng(e.Node.Tag)
        Else
            Id = CLng(e.Node.Parent.Tag)
            IdDetalle = CInt(e.Node.Tag)
        End If
    End Sub

    Private Sub tsbAgregaPlan_Click(sender As System.Object, e As System.EventArgs) Handles tsbAgregaPlan.Click
        pnlTelefonia.Visible = True
        bnPlanTel.Enabled = False
        bnPaqueteTel.Enabled = False
        bnServicioTel.Enabled = False
    End Sub

    Private Sub tbsAgregarPaquete_Click(sender As System.Object, e As System.EventArgs) Handles tbsAgregarPaquete.Click
        If Id = 0 Then
            MsgBox("Selecciona el Plan Taraifario al que se agregará el Paquete Adicional.", MsgBoxStyle.Information)
            Exit Sub
        End If
        Tipo = "A"
        lbTelAdic.Text = "Paquete Adicional"
        MUESTRAPaqueteAdicional()
        pnlTelefoniaAdic.Visible = True
        bnPlanTel.Enabled = False
        bnPaqueteTel.Enabled = False
        bnServicioTel.Enabled = False
    End Sub


    Private Sub tsbAgregarServicio_Click(sender As System.Object, e As System.EventArgs) Handles tsbAgregarServicio.Click
        If Id = 0 Then
            MsgBox("Selecciona el Plan Taraifario al que se agregará el Servicio Digital.", MsgBoxStyle.Information)
            Exit Sub
        End If
        Tipo = "D"
        lbTelAdic.Text = "Servicio Digital"
        MUESTRAServicioDigital()
        pnlTelefoniaAdic.Visible = True
        bnPlanTel.Enabled = False
        bnPaqueteTel.Enabled = False
        bnServicioTel.Enabled = False
    End Sub

    Private Sub tsbEliminarPlan_Click(sender As System.Object, e As System.EventArgs) Handles tsbEliminarPlan.Click
        If eContratoNet = 0 Then
            MsgBox("Selecciona un Plan Tarifario.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        BORDetReconTelTmp(eClv_Session, eContratoNet)
        LlenatvTel()
        Id = 0
    End Sub

    Private Sub tbsEliminarPaquete_Click(sender As System.Object, e As System.EventArgs) Handles tbsEliminarPaquete.Click
        If eClv_UnicaNet = 0 Then
            MsgBox("Selecciona un Paquete Adicional.", MsgBoxStyle.Information)
            Exit Sub
        End If
        BORDetReconTelAdicTmp(eClv_Session, eClv_UnicaNet)
        LlenatvTel()
        Id = 0
    End Sub

    Private Sub tsbEliminarServicio_Click(sender As System.Object, e As System.EventArgs) Handles tsbEliminarServicio.Click
        If eClv_UnicaNet = 0 Then
            MsgBox("Selecciona un Servicio Digital.", MsgBoxStyle.Information)
            Exit Sub
        End If
        BORDetReconTelAdicTmp(eClv_Session, eClv_UnicaNet)
        LlenatvTel()
        Id = 0
    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        If comboSerTel.Text.Length = 0 Then
            MsgBox("Selecciona un Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If
        VALIDADetReconTelCompartenATA(eClv_Session)
        If ContratoNetComparte > 0 Then
            If MessageBox.Show("¿El Plan Tarifario comparte MTA?", "Atención", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.No Then ContratoNetComparte = 0
        End If
        NueDetReconTelTmp(1, eClv_Session, eContrato, comboSerTel.SelectedValue, ContratoNetComparte)
        LlenatvTel()
        pnlTelefonia.Visible = False
        bnPlanTel.Enabled = True
        bnPaqueteTel.Enabled = True
        bnServicioTel.Enabled = True
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        pnlTelefonia.Visible = False
        bnPlanTel.Enabled = True
        bnPaqueteTel.Enabled = True
        bnServicioTel.Enabled = True
    End Sub

    Private Sub btnAceptarAdic_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptarAdic.Click
        If comboSerTelAdic.Text.Length = 0 Then
            MsgBox("Selecciona un Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If

        NUEDetReconTelAdicTmp(Id, eClv_Session, Tipo, comboSerTelAdic.SelectedValue)

        If eRes = 1 Then
            MessageBox.Show("No se puede agregar el mismo Paquete Adicional/Servicio Digital.")
        End If
        LlenatvTel()
        pnlTelefoniaAdic.Visible = False
        bnPlanTel.Enabled = True
        bnPaqueteTel.Enabled = True
        bnServicioTel.Enabled = True
    End Sub

    Private Sub btnCancelarAdic_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelarAdic.Click
        pnlTelefoniaAdic.Visible = False
        bnPlanTel.Enabled = True
        bnPaqueteTel.Enabled = True
        bnServicioTel.Enabled = True
    End Sub
End Class