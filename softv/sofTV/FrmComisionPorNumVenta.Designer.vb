﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmComisionPorNumVenta
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.lblServicioPrincipal = New System.Windows.Forms.Label()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.dgImportesCombos = New System.Windows.Forms.DataGridView()
        Me.CLVNUMVENTAS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DESCRIPCION = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IMPORTE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmbServicioPrincipal = New System.Windows.Forms.ComboBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.cmbGrupoVentas = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        CType(Me.dgImportesCombos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.Color.Black
        Me.btnGuardar.Location = New System.Drawing.Point(229, 419)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(136, 36)
        Me.btnGuardar.TabIndex = 41
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'lblServicioPrincipal
        '
        Me.lblServicioPrincipal.AutoSize = True
        Me.lblServicioPrincipal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblServicioPrincipal.Location = New System.Drawing.Point(14, 58)
        Me.lblServicioPrincipal.Name = "lblServicioPrincipal"
        Me.lblServicioPrincipal.Size = New System.Drawing.Size(215, 15)
        Me.lblServicioPrincipal.TabIndex = 39
        Me.lblServicioPrincipal.Text = "Seleccione el servicio principal :"
        '
        'btnAgregar
        '
        Me.btnAgregar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar.ForeColor = System.Drawing.Color.Black
        Me.btnAgregar.Location = New System.Drawing.Point(187, 93)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(136, 36)
        Me.btnAgregar.TabIndex = 37
        Me.btnAgregar.Text = "&Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = False
        '
        'dgImportesCombos
        '
        Me.dgImportesCombos.AllowUserToAddRows = False
        Me.dgImportesCombos.AllowUserToDeleteRows = False
        Me.dgImportesCombos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgImportesCombos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgImportesCombos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgImportesCombos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CLVNUMVENTAS, Me.DESCRIPCION, Me.IMPORTE})
        Me.dgImportesCombos.Location = New System.Drawing.Point(16, 144)
        Me.dgImportesCombos.Name = "dgImportesCombos"
        Me.dgImportesCombos.Size = New System.Drawing.Size(491, 256)
        Me.dgImportesCombos.TabIndex = 36
        '
        'CLVNUMVENTAS
        '
        Me.CLVNUMVENTAS.DataPropertyName = "CLVNUMVENTAS"
        Me.CLVNUMVENTAS.HeaderText = "CLVNUMVENTAS"
        Me.CLVNUMVENTAS.Name = "CLVNUMVENTAS"
        Me.CLVNUMVENTAS.Visible = False
        '
        'DESCRIPCION
        '
        Me.DESCRIPCION.DataPropertyName = "DESCRIPCION"
        Me.DESCRIPCION.HeaderText = "Número de Ventas"
        Me.DESCRIPCION.Name = "DESCRIPCION"
        Me.DESCRIPCION.ReadOnly = True
        '
        'IMPORTE
        '
        Me.IMPORTE.DataPropertyName = "IMPORTE"
        Me.IMPORTE.HeaderText = "Comisión"
        Me.IMPORTE.Name = "IMPORTE"
        '
        'cmbServicioPrincipal
        '
        Me.cmbServicioPrincipal.DisplayMember = "Descripcion"
        Me.cmbServicioPrincipal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbServicioPrincipal.FormattingEnabled = True
        Me.cmbServicioPrincipal.Location = New System.Drawing.Point(235, 55)
        Me.cmbServicioPrincipal.Name = "cmbServicioPrincipal"
        Me.cmbServicioPrincipal.Size = New System.Drawing.Size(270, 23)
        Me.cmbServicioPrincipal.TabIndex = 34
        Me.cmbServicioPrincipal.ValueMember = "Clv_Servicio"
        '
        'btnCancelar
        '
        Me.btnCancelar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.ForeColor = System.Drawing.Color.Black
        Me.btnCancelar.Location = New System.Drawing.Point(371, 419)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(136, 36)
        Me.btnCancelar.TabIndex = 33
        Me.btnCancelar.Text = "&Salir"
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'cmbGrupoVentas
        '
        Me.cmbGrupoVentas.DisplayMember = "DESCRIPCION"
        Me.cmbGrupoVentas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbGrupoVentas.FormattingEnabled = True
        Me.cmbGrupoVentas.Location = New System.Drawing.Point(235, 26)
        Me.cmbGrupoVentas.Name = "cmbGrupoVentas"
        Me.cmbGrupoVentas.Size = New System.Drawing.Size(270, 23)
        Me.cmbGrupoVentas.TabIndex = 51
        Me.cmbGrupoVentas.ValueMember = "CLVGRUPO"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(109, 29)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(121, 15)
        Me.Label5.TabIndex = 52
        Me.Label5.Text = "Grupo de Ventas :"
        '
        'FrmComisionPorNumVenta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(517, 465)
        Me.Controls.Add(Me.cmbGrupoVentas)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.lblServicioPrincipal)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.dgImportesCombos)
        Me.Controls.Add(Me.cmbServicioPrincipal)
        Me.Controls.Add(Me.btnCancelar)
        Me.Name = "FrmComisionPorNumVenta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Comisión por número de ventas"
        CType(Me.dgImportesCombos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents lblServicioPrincipal As System.Windows.Forms.Label
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents dgImportesCombos As System.Windows.Forms.DataGridView
    Friend WithEvents cmbServicioPrincipal As System.Windows.Forms.ComboBox
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents CLVNUMVENTAS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DESCRIPCION As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IMPORTE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmbGrupoVentas As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
End Class
