﻿Public Class frmSeleccionaTrabajos
#Region "CONTROLES"
    Private Sub btSeleccionaUno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btSeleccionaUno.Click
        If lbTrabajosASeleccionar.SelectedItems.Count > 0 Then
            UspSeleccionaUnTrabajoFiltro(CInt(Me.lbTrabajosASeleccionar.SelectedValue.ToString))
            UspMuestraTblFiltrarTrabajosTMP()
            UspMuestraTblFiltrarTrabajos()
        End If
    End Sub

    Private Sub btSeleccionarTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btSeleccionarTodos.Click
        If lbTrabajosASeleccionar.Items.Count > 0 Then
            UspSeleccionaTodosTrabajosFiltro()
            UspMuestraTblFiltrarTrabajosTMP()
            UspMuestraTblFiltrarTrabajos()
        End If
    End Sub

    Private Sub btAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAceptar.Click
        Me.Close()
    End Sub

    Private Sub btDeseleccionaUno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btDeseleccionaUno.Click
        If lbTrabajosSeleccionados.SelectedItems.Count > 0 Then
            UspDeseleccionaUnTrabajoFiltro(CInt(Me.lbTrabajosSeleccionados.SelectedValue.ToString))
            UspMuestraTblFiltrarTrabajosTMP()
            UspMuestraTblFiltrarTrabajos()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If lbTrabajosSeleccionados.Items.Count > 0 Then
            UspDeseleccionaTodosTrabajosFiltro()
            UspMuestraTblFiltrarTrabajosTMP()
            UspMuestraTblFiltrarTrabajos()
        End If
    End Sub
#End Region

#Region "EVENTOS"

    Private Sub frmSeleccionaTrabajos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.Label1.ForeColor = Color.Black
        UspLlenaFiltroTrabajos()
        UspMuestraTblFiltrarTrabajosTMP()
        UspMuestraTblFiltrarTrabajos()
    End Sub
#End Region

#Region "METODOS"
    Private Sub UspLlenaFiltroTrabajos()
        Try
            BaseII.limpiaParametros()
            BaseII.Inserta("UspLlenaFiltroTrabajos")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMuestraTblFiltrarTrabajos()
        Try
            BaseII.limpiaParametros()
            Me.lbTrabajosSeleccionados.DataSource = BaseII.ConsultaDT("UspMuestraTblFiltrarTrabajos")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMuestraTblFiltrarTrabajosTMP()
        Try
            BaseII.limpiaParametros()
            Me.lbTrabajosASeleccionar.DataSource = BaseII.ConsultaDT("UspMuestraTblFiltrarTrabajosTMP")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspSeleccionaUnTrabajoFiltro(ByVal PRMCLVTRABAJO As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVTRABAJO", SqlDbType.Int, PRMCLVTRABAJO)
            BaseII.Inserta("UspSeleccionaUnTrabajoFiltro")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspSeleccionaTodosTrabajosFiltro()
        Try
            BaseII.limpiaParametros()
            BaseII.Inserta("UspSeleccionaTodosTrabajosFiltro")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspDeseleccionaUnTrabajoFiltro(ByVal PRMCLVTRABAJO As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVTRABAJO", SqlDbType.Int, PRMCLVTRABAJO)
            BaseII.Inserta("UspDeseleccionaUnTrabajoFiltro")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspDeseleccionaTodosTrabajosFiltro()
        Try
            BaseII.limpiaParametros()
            BaseII.Inserta("UspDeseleccionaTodosTrabajosFiltro")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region


    
   
   
  
   
End Class