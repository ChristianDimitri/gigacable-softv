Imports System.Data.SqlClient
Public Class FrmCANEX
    Private opcionlocal As String = "N"

    Private Sub CONCANEXBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCANEXBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.ExtAdicTextBox.Text) = False Then Me.ExtAdicTextBox.Text = 0
        If IsNumeric(Me.ExtecionesTextBox.Text) = False Then Me.ExtecionesTextBox.Text = 0
        If CLng(Me.ExtAdicTextBox.Text) > CLng(Me.ExtecionesTextBox.Text) Then
            MsgBox("El numero de Extenciones que desea cancelar es mayor al numero Extenciones Instaladas")
            Me.ExtAdicTextBox.Text = 0
            Exit Sub
        End If

        If IsNumeric(Me.ExtAdicTextBox.Text) = False Then
            MsgBox("Capture el Numero de Extenciones que desea Cancelar ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Me.ExtAdicTextBox.Text <= 0 Then
            MsgBox("El Numero de Extenciones que desea Cancelar debe ser mayor a 0 ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Contrato > 0 And gloClv_Orden > 0 And GloDetClave > 0 Then
            Me.Validate()
            Me.CONCANEXBindingSource.EndEdit()
            Me.CONCANEXTableAdapter.Connection = CON
            Me.CONCANEXTableAdapter.Update(Me.NewSofTvDataSet.CONCANEX)
            Me.Close()
        End If
        CON.Close()
    End Sub

    Private Sub busca()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.CONCANEXTableAdapter.Connection = CON
            Me.CONCANEXTableAdapter.Fill(Me.NewSofTvDataSet.CONCANEX, New System.Nullable(Of Long)(CType(GloDetClave, Long)), New System.Nullable(Of Long)(CType(gloClv_Orden, Long)), New System.Nullable(Of Long)(CType(Contrato, Long)))
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub

    Private Sub FrmCANEX_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BORDetOrdSer_INTELIGENTETableAdapter.Connection = CON
        Me.BORDetOrdSer_INTELIGENTETableAdapter.Fill(Me.NewSofTvDataSet.BORDetOrdSer_INTELIGENTE, New System.Nullable(Of Long)(CType(GloDetClave, Long)))
        GloBndTrabajo = True
        GloBloqueaDetalle = True
        CON.Close()
    End Sub

    Private Sub FrmCANEX_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        busca()
        buscaExt()
        If IsNumeric(Me.Clv_OrdenTextBox.Text) = False Then
            opcionlocal = "N"
            Me.CONCANEXBindingSource.AddNew()
            Me.ContratoTextBox.Text = Contrato
            Me.Clv_OrdenTextBox.Text = gloClv_Orden
            Me.ClaveTextBox.Text = GloDetClave
        Else
            opcionlocal = "M"
        End If
        If Bloquea = True Or opcion = "M" Or opcion = "C" Then
            Me.CONCANEXBindingNavigator.Enabled = False
            Me.ExtAdicTextBox.Enabled = False
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Contrato > 0 And gloClv_Orden > 0 And GloDetClave > 0 Then
            Me.CONCANEXTableAdapter.Connection = CON
            Me.CONCANEXTableAdapter.Delete(GloDetClave, gloClv_Orden, Contrato)
            Me.Close()
        Else
            MsgBox("No ahi Datos para borrar", MsgBoxStyle.Information)
        End If
        CON.Close()
    End Sub


    Private Sub ExtAdicTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExtAdicTextBox.TextChanged

    End Sub

    Private Sub buscaExt()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.DameExteciones_CliTableAdapter.Connection = CON
            Me.DameExteciones_CliTableAdapter.Fill(Me.NewSofTvDataSet.DameExteciones_Cli, New System.Nullable(Of Long)(CType(Contrato, Long)))
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub
End Class