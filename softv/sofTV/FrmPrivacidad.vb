﻿Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports System.Net
Imports System.Net.Sockets
Imports System.IO
Imports System
Imports System.IO.StreamReader
Imports System.IO.File
Imports System.Collections.Generic

Public Class FrmPrivacidad


    Dim ds As New DataSet()
    Private customersByCityReport As ReportDocument

    Private Sub FrmPrivacidad_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        colorea(Me, Me.Name)
    End Sub

    Private Sub cancelarB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub


    Private Sub ButtonAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAceptar.Click
       
        eOpVentas = 101
        If ActivoCheckBox.Checked = True Then
            privacidad = True
        End If
        If InactivoCheckBox.Checked = True Then
            privacidad = False
        End If


        If Me.ActivoCheckBox.Checked = False And Me.InactivoCheckBox.Checked = False Then
            MsgBox("Se Requiere que al menos seleccione una casilla ", MsgBoxStyle.Information)
            Exit Sub
        End If
        FrmImprimirComision.Show()
    End Sub
End Class