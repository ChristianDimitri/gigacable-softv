﻿Public Class frmDetalleTecnicosAgenda

    Dim locClvTecnico As Integer
    Dim LocclvOrdenQueja As Long
    Dim LocContrato As Long
    Dim BND As Boolean
#Region "CONTROLES"
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If txtBuscarClaveTecnico.Text = "" Then
            locClvTecnico = 0
        Else
            locClvTecnico = CInt(txtBuscarClaveTecnico.Text)
        End If
        If BND = True Then
            UspLlenarRelTecnico(locClvTecnico, Me.cmbTecnicos.SelectedValue.ToString, txtbuscarNombreTecnico.Text)
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        If txtBuscarClvOreden.Text = "" Then
            LocclvOrdenQueja = 0
        Else
            LocclvOrdenQueja = CLng(Me.txtBuscarClvOreden.Text)
        End If
        If txtBuscarContrato.Text = "" Then
            LocContrato = 0
        Else
            LocContrato = CLng(Me.txtBuscarContrato.Text)
        End If
        UspLlenarRelTUspLlenarRelOrdenesQuejasecnico(LocclvOrdenQueja, LocContrato, Me.txtBuscarNombre.Text, Me.txtBuscarDireccion.Text, 0, Me.cmbOrdenesQuejas.SelectedValue, String.Empty)
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Me.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim I As Integer
        Dim BANDERA As Boolean
        Dim BND2 As Boolean
        Dim BND3 As Boolean
        BND3 = False
        BND2 = False
        BANDERA = False

        For I = 0 To dgOrdenQuejas.Rows.Count - 1
            If Me.dgOrdenQuejas.Item(1, I).Value.ToString = "" Then
                BANDERA = False
            Else
                BANDERA = Me.dgOrdenQuejas.Item(1, I).Value
            End If

            If BANDERA = True And Me.dgOrdenQuejas.Item(2, I).Value = "SIN ASIGNAR" Then
                UspGuardaOrdenesTMP(Me.dgOrdenQuejas.Item(3, I).Value, Me.dgOrdenQuejas.Item(4, I).Value)
                BND2 = True
            ElseIf BANDERA = True And Me.dgOrdenQuejas.Item(2, I).Value <> "SIN ASIGNAR" Then
                UspGuardaListaReasignar(Me.dgOrdenQuejas.Item(3, I).Value, Me.dgOrdenQuejas.Item(4, I).Value)
                BND3 = True
            End If
        Next
        If BND2 = True Then
            frmAsignacionAlTecnico.ShowDialog()
            frmAsignacionAlTecnico.Dispose()

        End If
        If BND3 = True Then
            frmReasignarTecnico.ShowDialog()
            frmReasignarTecnico.Dispose()
        End If
        If BND3 = False And BND2 = False Then
            MsgBox("No hay ninguna Orden/Queja seleccionada por asignar", MsgBoxStyle.Information, "Asignación Ordenes/Quejas")
        End If
        If txtBuscarClvOreden.Text = "" Then
            LocclvOrdenQueja = 0
        Else
            LocclvOrdenQueja = CLng(Me.txtBuscarClvOreden.Text)
        End If
        If txtBuscarContrato.Text = "" Then
            LocContrato = 0
        Else
            LocContrato = CLng(Me.txtBuscarContrato.Text)
        End If
        UspLlenarRelTUspLlenarRelOrdenesQuejasecnico(LocclvOrdenQueja, LocContrato, Me.txtBuscarNombre.Text, Me.txtBuscarDireccion.Text, CInt(Me.dgTecnicos.SelectedCells(0).Value.ToString), Me.cmbOrdenesQuejas.SelectedValue, Me.cmbTecnicos.SelectedValue)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        GloTipo = "C"
        frmAsignacionOrdenQuejaTecnicos.Show()

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        If Me.dgOrdenQuejas.SelectedRows.Count > 0 Then
            If Me.dgOrdenQuejas.SelectedCells(2).Value.ToString = "SIN ASIGNAR" Then
                MsgBox("La orden/Queja no esta asignada a ningun técnico", MsgBoxStyle.Information, "Detalle Técnicos")
                Exit Sub
            End If
            Nombretecnico = Me.dgTecnicos.SelectedCells(1).Value.ToString
            Tipotecnico = Me.dgOrdenQuejas.SelectedCells(4).Value.ToString
            ClvOrdenQueja1 = CLng(Me.dgOrdenQuejas.SelectedCells(3).Value)
            If GloTipoUsuario = 40 Or GloTipoUsuario = 43 Or GloTipoUsuario = 44 Then
                GloTipo = "M"
            Else
                GloTipo = "C"
            End If

            frminformacionAgenda.ShowDialog()
            frminformacionAgenda.Dispose()
        Else
            MsgBox("No hay ninguna Orden/Queja seleccionada", MsgBoxStyle.Information, "Detalle Técnicos")
        End If
        
    End Sub
    Private Sub txtBuscarClaveTecnico_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtBuscarClaveTecnico.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
        If KeyAscii = 13 Then
            If txtBuscarClaveTecnico.Text = "" Then
                locClvTecnico = 0
            Else
                locClvTecnico = CInt(txtBuscarClaveTecnico.Text)
            End If
            If BND = True Then
                UspLlenarRelTecnico(locClvTecnico, Me.cmbTecnicos.SelectedValue.ToString, txtbuscarNombreTecnico.Text)
            End If
        End If
    End Sub
#End Region

#Region "METODOS"
    Private Sub UspGuardaOrdenesTMP(ByVal PRMCLVORDEN As Long, ByVal PRMTIPO As String)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVORDENQUEJA", SqlDbType.BigInt, PRMCLVORDEN)
            BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, PRMTIPO, 50)
            BaseII.Inserta("UspGuardaOrdenesTMP")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspGuardaListaReasignar(ByVal PRMCLVORDEN As Long, ByVal PRMTIPO As String)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVORDENQUEJA", SqlDbType.BigInt, PRMCLVORDEN)
            BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, PRMTIPO, 50)
            BaseII.Inserta("UspGuardaListaReasignar")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function
    Private Sub UspLlenaComboTecnicos()
        Try
            BaseII.limpiaParametros()
            Me.cmbTecnicos.DataSource = BaseII.ConsultaDT("UspLlenaComboTecnicos")
            Me.cmbTecnicos.SelectedIndex = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspLlenaComboOrdenesQuejas()
        Try
            BaseII.limpiaParametros()
            Me.cmbOrdenesQuejas.DataSource = BaseII.ConsultaDT("UspLlenaComboOrdenesQuejas")

            Me.cmbOrdenesQuejas.SelectedIndex = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspLlenarRelTecnico(ByVal PRMCLVTECNICO As Integer, ByVal PRMTIPO As String, ByVal PRMNOMBRE As String)
        Try
            If Len(PRMCLVTECNICO) = 0 Then
                PRMCLVTECNICO = 0
            End If
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVTECNICO", SqlDbType.Int, PRMCLVTECNICO)
            BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, PRMTIPO, 1)
            BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, PRMNOMBRE, 250)
            Me.dgTecnicos.DataSource = BaseII.ConsultaDT("UspLlenarRelTecnico")

            txtbuscarNombreTecnico.Text = ""
            txtBuscarClaveTecnico.Text = ""
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspLlenarRelTUspLlenarRelOrdenesQuejasecnico(ByVal PRMCLVORDENQUEJA As Long, ByVal PRMCONTRATO As Long, ByVal PRMNOMBRE As String, ByVal PRMDIRECCION As String, ByVal PRMCLVTECNICO As Integer, ByVal PRMTIPO As String, ByVal PRMTIPOTECNICO As String)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVORDENQUEJA", SqlDbType.BigInt, PRMCLVORDENQUEJA)
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, PRMCONTRATO)
            BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, PRMNOMBRE, 250)
            BaseII.CreateMyParameter("@DIRECCION", SqlDbType.VarChar, PRMDIRECCION, 250)
            BaseII.CreateMyParameter("@CLVTECNICO", SqlDbType.Int, PRMCLVTECNICO)
            BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, PRMTIPO, 1)
            BaseII.CreateMyParameter("@TIPOTECNICO", SqlDbType.VarChar, PRMTIPOTECNICO, 1)
            Me.dgOrdenQuejas.DataSource = BaseII.ConsultaDT("UspMostrarAsignaturaOrdenes")

            Me.txtBuscarClvOreden.Text = ""
            txtBuscarContrato.Text = ""
            txtBuscarNombre.Text = ""
            txtBuscarDireccion.Text = ""
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspDesasignarOrdenesQuejas(ByVal PRMORDENQUEJA As Long, ByVal PRMTIPO As String)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVORDENQUEJA", SqlDbType.BigInt, PRMORDENQUEJA)
            BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, PRMTIPO, 10)
            BaseII.Inserta("UspDesasignarOrdenesQuejas")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "EVENTOS"
    Private Sub frmDetalleTecnicosAgenda_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        BND = False
        Me.Panel1.BackColor = Me.BackColor
        Me.Panel2.BackColor = Me.BackColor
        Me.Panel3.BackColor = Me.BackColor
        Me.Label1.BackColor = Me.BackColor
        Me.Label2.BackColor = Me.BackColor
        Me.Label3.BackColor = Me.BackColor
        Me.Label4.BackColor = Me.BackColor
        Me.Label5.BackColor = Me.BackColor
        Me.Label6.BackColor = Me.BackColor
        Me.Label7.BackColor = Me.BackColor
        Me.Label8.BackColor = Me.BackColor
        Me.Label9.BackColor = Me.BackColor
        Me.Label10.BackColor = Me.BackColor
        UspLlenaComboTecnicos()
        BND = True
        UspLlenaComboOrdenesQuejas()
        UspLlenarRelTecnico(0, Me.cmbTecnicos.SelectedValue.ToString, txtbuscarNombreTecnico.Text)
        If GloTipoUsuario = 40 Or GloTipoUsuario = 43 Or GloTipoUsuario = 44 Or GloTipoUsuario = 46 Then
            Button3.Visible = True
        Else
            Button3.Visible = False
        End If

        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)

    End Sub

    Private Sub dgTecnicos_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgTecnicos.SelectionChanged
        If dgTecnicos.SelectedRows.Count > 0 Then
            If txtBuscarClvOreden.Text = "" Then
                LocclvOrdenQueja = 0
            Else
                LocclvOrdenQueja = CLng(Me.txtBuscarClvOreden.Text)
            End If
            If txtBuscarContrato.Text = "" Then
                LocContrato = 0
            Else
                LocContrato = CLng(Me.txtBuscarContrato.Text)
            End If
            UspLlenarRelTUspLlenarRelOrdenesQuejasecnico(LocclvOrdenQueja, LocContrato, Me.txtBuscarNombre.Text, Me.txtBuscarDireccion.Text, CInt(Me.dgTecnicos.SelectedCells(0).Value.ToString), Me.cmbOrdenesQuejas.SelectedValue, Me.dgTecnicos.SelectedCells(2).Value.ToString.Substring(0, 1))
        End If
    End Sub

    Private Sub cmbTecnicos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTecnicos.SelectedIndexChanged
        If txtBuscarClaveTecnico.Text = "" Then
            locClvTecnico = 0
        Else
            locClvTecnico = CInt(txtBuscarClaveTecnico.Text)
        End If
        If BND = True Then
            UspLlenarRelTecnico(locClvTecnico, Me.cmbTecnicos.SelectedValue.ToString, txtbuscarNombreTecnico.Text)
        End If

    End Sub


    Private Sub cmbOrdenesQuejas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbOrdenesQuejas.SelectedIndexChanged
        If txtBuscarClvOreden.Text = "" Then
            LocclvOrdenQueja = 0
        Else
            LocclvOrdenQueja = CLng(Me.txtBuscarClvOreden.Text)
        End If
        If txtBuscarContrato.Text = "" Then
            LocContrato = 0
        Else
            LocContrato = CLng(Me.txtBuscarContrato.Text)
        End If
        UspLlenarRelTUspLlenarRelOrdenesQuejasecnico(LocclvOrdenQueja, LocContrato, Me.txtBuscarNombre.Text, Me.txtBuscarDireccion.Text, 0, Me.cmbOrdenesQuejas.SelectedValue, String.Empty)

    End Sub

    Private Sub txtBuscarClvOreden_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBuscarClvOreden.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
        If KeyAscii = 13 Then
            If txtBuscarClvOreden.Text = "" Then
                LocclvOrdenQueja = 0
            Else
                LocclvOrdenQueja = CLng(Me.txtBuscarClvOreden.Text)
            End If
            If txtBuscarContrato.Text = "" Then
                LocContrato = 0
            Else
                LocContrato = CLng(Me.txtBuscarContrato.Text)
            End If
            UspLlenarRelTUspLlenarRelOrdenesQuejasecnico(LocclvOrdenQueja, LocContrato, Me.txtBuscarNombre.Text, Me.txtBuscarDireccion.Text, 0, Me.cmbOrdenesQuejas.SelectedValue, String.Empty)
        End If
    End Sub

    Private Sub txtBuscarContrato_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBuscarContrato.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
        If KeyAscii = 13 Then
            If txtBuscarClvOreden.Text = "" Then
                LocclvOrdenQueja = 0
            Else
                LocclvOrdenQueja = CLng(Me.txtBuscarClvOreden.Text)
            End If
            If txtBuscarContrato.Text = "" Then
                LocContrato = 0
            Else
                LocContrato = CLng(Me.txtBuscarContrato.Text)
            End If
            UspLlenarRelTUspLlenarRelOrdenesQuejasecnico(LocclvOrdenQueja, LocContrato, Me.txtBuscarNombre.Text, Me.txtBuscarDireccion.Text, 0, Me.cmbOrdenesQuejas.SelectedValue, String.Empty)
        End If
    End Sub

    Private Sub txtbuscarNombreTecnico_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtbuscarNombreTecnico.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        If KeyAscii = 13 Then
            If txtBuscarClaveTecnico.Text = "" Then
                locClvTecnico = 0
            Else
                locClvTecnico = CInt(txtBuscarClaveTecnico.Text)
            End If
            If BND = True Then
                UspLlenarRelTecnico(locClvTecnico, Me.cmbTecnicos.SelectedValue.ToString, txtbuscarNombreTecnico.Text)
            End If
        End If
    End Sub

    Private Sub txtBuscarNombre_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBuscarNombre.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        If KeyAscii = 13 Then
            If txtBuscarClvOreden.Text = "" Then
                LocclvOrdenQueja = 0
            Else
                LocclvOrdenQueja = CLng(Me.txtBuscarClvOreden.Text)
            End If
            If txtBuscarContrato.Text = "" Then
                LocContrato = 0
            Else
                LocContrato = CLng(Me.txtBuscarContrato.Text)
            End If
            UspLlenarRelTUspLlenarRelOrdenesQuejasecnico(LocclvOrdenQueja, LocContrato, Me.txtBuscarNombre.Text, Me.txtBuscarDireccion.Text, 0, Me.cmbOrdenesQuejas.SelectedValue, String.Empty)
        End If

    End Sub

    Private Sub txtBuscarDireccion_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBuscarDireccion.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        If KeyAscii = 13 Then
            If txtBuscarClvOreden.Text = "" Then
                LocclvOrdenQueja = 0
            Else
                LocclvOrdenQueja = CLng(Me.txtBuscarClvOreden.Text)
            End If
            If txtBuscarContrato.Text = "" Then
                LocContrato = 0
            Else
                LocContrato = CLng(Me.txtBuscarContrato.Text)
            End If
            UspLlenarRelTUspLlenarRelOrdenesQuejasecnico(LocclvOrdenQueja, LocContrato, Me.txtBuscarNombre.Text, Me.txtBuscarDireccion.Text, 0, Me.cmbOrdenesQuejas.SelectedValue, String.Empty)
        End If
    End Sub
#End Region









    Private Sub Button4_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click

        Dim I As Integer
        Dim BANDERA As Boolean
        Dim BND2 As Boolean
        Dim BND3 As Boolean
        Dim contador As Integer
        BND3 = False
        BND2 = False
        BANDERA = False
        contador = 0

        For I = 0 To dgOrdenQuejas.Rows.Count - 1
            If Me.dgOrdenQuejas.Item(1, I).Value.ToString = "" Then
                BANDERA = False
            Else
                BANDERA = Me.dgOrdenQuejas.Item(1, I).Value
            End If

            If BANDERA = True And Me.dgOrdenQuejas.Item(2, I).Value <> "SIN ASIGNAR" Then
                contador = contador + 1
            End If
        Next

        If contador > 0 Then
            If MsgBox("¿Desea quitar la asignatura a la Ordenes/Quejas seleccionadas?", MsgBoxStyle.YesNo, "Asignación Ordenes/Quejas") = MsgBoxResult.No Then
                Exit Sub
            End If
        Else
            MsgBox("No ninguna Orden/Queja seleccionada o asignada", MsgBoxStyle.Information, "Asignación Ordenes/Quejas")
            Exit Sub
        End If


        For I = 0 To dgOrdenQuejas.Rows.Count - 1
            If Me.dgOrdenQuejas.Item(1, I).Value.ToString = "" Then
                BANDERA = False
            Else
                BANDERA = Me.dgOrdenQuejas.Item(1, I).Value
            End If

            If BANDERA = True And Me.dgOrdenQuejas.Item(2, I).Value <> "SIN ASIGNAR" Then
                UspDesasignarOrdenesQuejas(Me.dgOrdenQuejas.Item(3, I).Value, Me.dgOrdenQuejas.Item(4, I).Value)
            End If
        Next

        If txtBuscarClvOreden.Text = "" Then
            LocclvOrdenQueja = 0
        Else
            LocclvOrdenQueja = CLng(Me.txtBuscarClvOreden.Text)
        End If
        If txtBuscarContrato.Text = "" Then
            LocContrato = 0
        Else
            LocContrato = CLng(Me.txtBuscarContrato.Text)
        End If
        UspLlenarRelTUspLlenarRelOrdenesQuejasecnico(LocclvOrdenQueja, LocContrato, Me.txtBuscarNombre.Text, Me.txtBuscarDireccion.Text, 0, Me.cmbOrdenesQuejas.SelectedValue, String.Empty)

    End Sub


#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region
End Class