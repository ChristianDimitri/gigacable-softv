﻿Public Class FrmMetasCombo

    Dim bandera As Boolean = False

    Private Sub ConGrupoVentas(ByVal Clv_Grupo As Integer, ByVal Op As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Grupo", SqlDbType.Int, CObj(Clv_Grupo))
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, CObj(Op))
        cbGrupo.DataSource = BaseII.ConsultaDT("ConGrupoVentas")
    End Sub

    Private Sub MuestraTipServEric(ByVal Clv_TipServ As Integer, ByVal Op As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_TipServ", SqlDbType.Int, CObj(Clv_TipServ))
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, CObj(Op))
        cbTipSer.DataSource = BaseII.ConsultaDT("MuestraTipServEric")
    End Sub

    Private Sub MuestraAnios()
        BaseII.limpiaParametros()
        cbAnio.DataSource = BaseII.ConsultaDT("MuestraAnios")
    End Sub

    Private Sub CONMetasCombo(ByVal Clv_TipSer As Integer, ByVal Clv_Grupo As Integer, ByVal Anio As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CObj(Clv_TipSer))
        BaseII.CreateMyParameter("@CLV_GRUPO", SqlDbType.Int, CObj(Clv_Grupo))
        BaseII.CreateMyParameter("@ANIO", SqlDbType.Int, CObj(Anio))
        dgvMetasCombo.DataSource = BaseII.ConsultaDT("CONMetasCombo")
    End Sub

    Private Sub MODMetasCombo(ByVal IdConcepto As Integer, ByVal Clv_Servicio As Integer, ByVal Clv_Grupo As Integer, ByVal Ene As Integer, ByVal Feb As Integer, ByVal Mar As Integer, ByVal Abr As Integer, ByVal May As Integer, ByVal Jun As Integer, ByVal Jul As Integer, ByVal Ago As Integer, ByVal Sep As Integer, ByVal Oct As Integer, ByVal Nov As Integer, ByVal Dic As Integer, ByVal Anio As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IDCONCEPTO", SqlDbType.Int, CObj(IdConcepto))
        BaseII.CreateMyParameter("@CLV_SERVICIO", SqlDbType.Int, CObj(Clv_Servicio))
        BaseII.CreateMyParameter("@CLV_GRUPO", SqlDbType.Int, CObj(Clv_Grupo))
        BaseII.CreateMyParameter("@ENE", SqlDbType.Int, CObj(Ene))
        BaseII.CreateMyParameter("@FEB", SqlDbType.Int, CObj(Feb))
        BaseII.CreateMyParameter("@MAR", SqlDbType.Int, CObj(Mar))
        BaseII.CreateMyParameter("@ABR", SqlDbType.Int, CObj(Abr))
        BaseII.CreateMyParameter("@MAY", SqlDbType.Int, CObj(May))
        BaseII.CreateMyParameter("@JUN", SqlDbType.Int, CObj(Jun))
        BaseII.CreateMyParameter("@JUL", SqlDbType.Int, CObj(Jul))
        BaseII.CreateMyParameter("@AGO", SqlDbType.Int, CObj(Ago))
        BaseII.CreateMyParameter("@SEP", SqlDbType.Int, CObj(Sep))
        BaseII.CreateMyParameter("@OCT", SqlDbType.Int, CObj(Oct))
        BaseII.CreateMyParameter("@NOV", SqlDbType.Int, CObj(Nov))
        BaseII.CreateMyParameter("@DIC", SqlDbType.Int, CObj(Dic))
        BaseII.CreateMyParameter("@ANIO", SqlDbType.Int, CObj(Anio))
        BaseII.Inserta("MODMetasCombo")
    End Sub

    Private Sub BORMetasCombo(ByVal Clv_TipSer As Integer, ByVal Clv_Grupo As Integer, ByVal Anio As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CObj(Clv_TipSer))
        BaseII.CreateMyParameter("@CLV_GRUPO", SqlDbType.Int, CObj(Clv_Grupo))
        BaseII.CreateMyParameter("@ANIO", SqlDbType.Int, CObj(Anio))
        BaseII.Inserta("BORMetasCombo")
    End Sub

    Private Sub LlenaGrid()
        If bandera = False Then
            Exit Sub
        End If
        If cbGrupo.Text.Length = 0 Then
            Exit Sub
        End If
        If cbTipSer.Text.Length = 0 Then
            Exit Sub
        End If
        If cbAnio.Text.Length = 0 Then
            Exit Sub
        End If
        CONMetasCombo(cbTipSer.SelectedValue, cbGrupo.SelectedValue, cbAnio.SelectedValue)
    End Sub

    Private Sub Guardar()
        dgvMetasCombo.EndEdit()
        Dim x As Integer = 0
        For x = 0 To dgvMetasCombo.RowCount - 1
            MODMetasCombo(dgvMetasCombo.Item("IdConcepto", x).Value,
            dgvMetasCombo.Item("Clv_Servicio", x).Value,
                dgvMetasCombo.Item("Clv_Grupo", x).Value,
                dgvMetasCombo.Item("Ene", x).Value,
                dgvMetasCombo.Item("Feb", x).Value,
                dgvMetasCombo.Item("Mar", x).Value,
                dgvMetasCombo.Item("Abr", x).Value,
                dgvMetasCombo.Item("May", x).Value,
                dgvMetasCombo.Item("Jun", x).Value,
                dgvMetasCombo.Item("Jul", x).Value,
                dgvMetasCombo.Item("Ago", x).Value,
                dgvMetasCombo.Item("Sep", x).Value,
                dgvMetasCombo.Item("Oct", x).Value,
                dgvMetasCombo.Item("Nov", x).Value,
                dgvMetasCombo.Item("Dic", x).Value,
                dgvMetasCombo.Item("Anio", x).Value)
        Next
        MessageBox.Show("Se guardó con éxito.")
    End Sub

    Private Sub Eliminar()
        If cbGrupo.Text.Length = 0 Then
            Exit Sub
        End If
        If cbTipSer.Text.Length = 0 Then
            Exit Sub
        End If
        If cbAnio.Text.Length = 0 Then
            Exit Sub
        End If
        BORMetasCombo(cbTipSer.SelectedValue, cbGrupo.SelectedValue, cbAnio.SelectedValue)
        MessageBox.Show("Se eliminó con éxito.")
        LlenaGrid()
    End Sub

    Private Sub FrmMetasCombo_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        ConGrupoVentas(0, 3)
        MuestraTipServEric(0, 0)
        MuestraAnios()
        bandera = True
        LlenaGrid()
    End Sub

    Private Sub cbGrupo_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbGrupo.SelectedIndexChanged
        LlenaGrid()
    End Sub

    Private Sub cbTipSer_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbTipSer.SelectedIndexChanged
        LlenaGrid()
    End Sub

    Private Sub cbAnio_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbAnio.SelectedIndexChanged
        LlenaGrid()
    End Sub

    Private Sub tsbGuardar_Click(sender As System.Object, e As System.EventArgs) Handles tsbGuardar.Click
        Guardar()
    End Sub

    Private Sub tsbEliminar_Click(sender As System.Object, e As System.EventArgs) Handles tsbEliminar.Click
        Eliminar()
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub
End Class