﻿Public Class FrmComisionPorNumVenta

#Region "Eventos"

    Private Sub FrmImportesVentasCombo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        lblServicioPrincipal.ForeColor = Color.Black
        Label5.ForeColor = Color.Black
        UspLlenaGrupo()
        UspLlenaServPrincipales()
        Me.cmbServicioPrincipal.Enabled = True
        Me.cmbGrupoVentas.Enabled = True
        Me.dgImportesCombos.Enabled = True
        Me.btnGuardar.Enabled = True
        Me.btnAgregar.Enabled = True

        If opcion = "M" Then
            Me.cmbGrupoVentas.SelectedValue = GloClvGrupoVentas
            Me.cmbServicioPrincipal.SelectedValue = GloClvServicioPrincipal

            UspMuestraImportesCombo()
            Me.cmbServicioPrincipal.Enabled = False
            Me.cmbGrupoVentas.Enabled = False
        ElseIf opcion = "C" Then
            Me.cmbGrupoVentas.SelectedValue = GloClvGrupoVentas
            Me.cmbServicioPrincipal.SelectedValue = GloClvServicioPrincipal

            UspMuestraImportesCombo()
            Me.cmbServicioPrincipal.Enabled = False
            Me.cmbGrupoVentas.Enabled = False
            Me.dgImportesCombos.Enabled = False
            Me.btnGuardar.Enabled = False
            Me.btnAgregar.Enabled = False

        ElseIf opcion = "N" Then

        End If

        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub cmbGrupoVentas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbGrupoVentas.SelectedIndexChanged
        UspLlenaServPrincipales()
    End Sub
#End Region

#Region "Salir"

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

#End Region

#Region "Agregar"

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        If Me.cmbGrupoVentas.SelectedValue = 0 Then
            MsgBox("Seleccione el grupo de ventas", MsgBoxStyle.Information, "Comisión por número de ventas")
            Exit Sub
        End If
        Me.cmbServicioPrincipal.Enabled = False
        Me.cmbGrupoVentas.Enabled = False
        UspAgregarServicioImporteCombo()
        UspMuestraImportesCombo()
    End Sub

#End Region

#Region "Metodos"

    Private Sub UspLlenaGrupo()
        Try
            BaseII.limpiaParametros()
            cmbGrupoVentas.DataSource = BaseII.ConsultaDT("UspLlenaGrupo")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspLlenaServPrincipales()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Var", SqlDbType.VarChar, opcion, 5)
            BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, Me.cmbGrupoVentas.SelectedValue)
            cmbServicioPrincipal.DataSource = BaseII.ConsultaDT("UspLlenaServPrincipalesNumVentas")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub UspAgregarServicioImporteCombo()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSERVICIOPRINCIPAL", SqlDbType.Int, Me.cmbServicioPrincipal.SelectedValue)
            BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, Me.cmbGrupoVentas.SelectedValue)
            BaseII.ConsultaDT("UspAgregarServicioVentas")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMuestraImportesCombo()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSERVICIOPRINCIPAL", SqlDbType.Int, Me.cmbServicioPrincipal.SelectedValue)
            BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, Me.cmbGrupoVentas.SelectedValue)
            dgImportesCombos.DataSource = BaseII.ConsultaDT("UspMuestraImportesNumVentas")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspGuardaComisionCombo()
        Try
            Dim I As Integer

            For I = 0 To dgImportesCombos.Rows.Count - 1
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLVSERVICIOPRINCIPAL", SqlDbType.Int, Me.cmbServicioPrincipal.SelectedValue)
                BaseII.CreateMyParameter("@CLVNUMVENTA", SqlDbType.Int, Me.dgImportesCombos(0, I).Value)
                BaseII.CreateMyParameter("@COMISION", SqlDbType.Money, Me.dgImportesCombos(2, I).Value)
                BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, Me.cmbGrupoVentas.SelectedValue)
                BaseII.Inserta("UspGuardaComisionNumVentas")
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Guardar"
    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        UspGuardaComisionCombo()
        UspMuestraImportesCombo()
    End Sub
#End Region

#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region

   
End Class