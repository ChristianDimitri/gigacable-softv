Imports System.Data.SqlClient
Public Class FrmDetOrSer
    Dim op_cabl As Integer = 0
    Private Sub FrmDetOrSer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        HayConex(0)
        'Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        'Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
        Me.Dime_Que_servicio_Tiene_clienteTableAdapter.Connection = CON
        Me.Dime_Que_servicio_Tiene_clienteTableAdapter.Fill(Me.Procedimientosarnoldo4.Dime_Que_servicio_Tiene_cliente, GloContratoord)

        'Me.MUESTRATRABAJOSTableAdapter.Connection = CON
        'Me.MUESTRATRABAJOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOS, CInt(Me.ComboBox4.SelectedValue))
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_TipSer", SqlDbType.Int, CInt(Me.ComboBox4.SelectedValue))
        BaseII.CreateMyParameter("@contrato", SqlDbType.Int, Contrato)
        ComboBox2.DataSource = BaseII.ConsultaDT("MUESTRATRABAJOSPorContrato")
        Me.ComboBox2.Text = ""
        CON.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim clavedetordSer As Long = 0
        Dim CON As New SqlConnection(MiConexion)
        eClv_TipSer = Me.ComboBox4.SelectedValue
        CON.Open()
        If IsNumeric(Me.ComboBox2.SelectedValue) = True And Len(Trim(Me.ComboBox2.Text)) > 0 Then
            If Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BCABM" Then
                MsgBox("La baja de Cablemodem se genera de forma automatica en el momento que todos los Servicios pase a Baja ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BAPAR" Then
                MsgBox("La baja de aparato digital se genera de forma automatica en el momento que todos los servicios se pase a baja ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Mid(Trim(Me.ComboBox2.Text), 1, 5) = "CANEX" Then
                HayConex(0)
                If Me.ContadorTextBox.Text > 0 Then
                    MsgBox("Existe una Orden de Contrataci�n de Extenci�n por lo cual se puede agregar este concepto ", MsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            If Mid(Trim(Me.ComboBox2.Text), 1, 5) = "CONEX" Then
                HayConex(1)
                If Me.ContadorTextBox.Text > 0 Then
                    MsgBox("Existe una Orden de Cancelaci�n de Extenci�n por lo cual se puede agregar este concepto ", MsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            GloClv_Trabajo_OrdSer = Me.ComboBox2.SelectedValue
            GloTrabajo_OrdSer = Me.ComboBox2.Text
            If Len(Trim(Me.ObsTextBox1.Text)) > 0 Then
                GloObs_OrdSer = Me.ObsTextBox1.Text
            Else
                GloObs_OrdSer = ""
            End If
            GloSeRealiza_OrdSer = Me.SeRealizaCheckBox.Checked
            GloBndTrabajo = True
            GLOTRABAJO = Mid(Trim(GloTrabajo_OrdSer), 1, 5)
            Me.CONDetOrdSerTableAdapter.Connection = CON
            Me.CONDetOrdSerTableAdapter.Insert(gloClv_Orden, GloClv_Trabajo_OrdSer, GloObs_OrdSer, GloSeRealiza_OrdSer, GloDetClave)
            If Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CAMDO" Or Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CANET" Or Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CADIG" Then
                FrmCAMDO.Show()
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CONEX" Then
                FrmCONEX.Show()
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CEXTE" Then
                FrmCEXTE.Show()
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CANEX" Then
                FrmCANEX.Show()
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CCABT" Then
               
                SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
                SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
                SoftvMod.VariablesGlobales.MiConexion = MiConexion
                SoftvMod.VariablesGlobales.Clave = GloDetClave
                SoftvMod.VariablesGlobales.Clv_Orden = gloClv_Orden
                SoftvMod.VariablesGlobales.Clv_Tecnico = Locclv_tec
                SoftvMod.VariablesGlobales.StatusOrden = "P"
                Dim frm As New SoftvMod.FrmCCABT
                frm.Show()

            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 6) = "ICABMT" Then
               
                SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
                SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
                SoftvMod.VariablesGlobales.MiConexion = MiConexion
                SoftvMod.VariablesGlobales.Clave = GloDetClave
                SoftvMod.VariablesGlobales.Clv_Orden = gloClv_Orden
                SoftvMod.VariablesGlobales.Clv_Tecnico = Locclv_tec
                SoftvMod.VariablesGlobales.StatusOrden = "P"
                SoftvMod.VariablesGlobales.Trabajo = "ICABMT"
                Dim frm As New SoftvMod.FrmICABMT
                frm.Show()

                'GLOTRABAJO = "ICABMT"
                'FrmRelCablemodemClientes.Show()

            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "RIMTA" Then
               
                SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
                SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
                SoftvMod.VariablesGlobales.MiConexion = MiConexion
                SoftvMod.VariablesGlobales.Clave = GloDetClave
                SoftvMod.VariablesGlobales.Clv_Orden = gloClv_Orden
                SoftvMod.VariablesGlobales.Clv_Tecnico = Locclv_tec
                SoftvMod.VariablesGlobales.StatusOrden = "P"
                SoftvMod.VariablesGlobales.Trabajo = "RIMTA"
                Dim frm As New SoftvMod.FrmICABMT
                frm.Show()

            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "REMTA" Then

                SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
                SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
                SoftvMod.VariablesGlobales.MiConexion = MiConexion
                SoftvMod.VariablesGlobales.Clave = GloDetClave
                SoftvMod.VariablesGlobales.Clv_Orden = gloClv_Orden
                Dim frm As New SoftvMod.FrmREMTA
                frm.Show()

            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "ICABM" Then
                FrmRelCablemodemClientes.Show()
            ElseIf Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BCABM" Then
                FrmRelCablemodemClientes.Show()
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CCABM" Then
                'FrmRelCablemodemClientes.Show()
                'Eric----------------
                'FrmICABMAsigna.Show()
                op_cabl = MsgBox("�El Cablemodem Que Se Va Instalar es Inal�mbrico?", MsgBoxStyle.YesNo)
                If op_cabl = 6 Then
                    LoctipoCablemdm = 2
                ElseIf op_cabl = 7 Then
                    LoctipoCablemdm = 1
                End If
                bndCCABM = True
                FrmRelCablemodemClientes.Show()

            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CAPAR" Then
                'FrmRelCablemodemClientes.Show()
                'Eric----------------
                'FrmIAPARAsigna.Show()
            ElseIf (Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "IPAQU" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BPAQU" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "DPAQU" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "RPAQU") Then
                FrmrRelPaquetesdelCliente.Show()
            ElseIf (Mid(GloTrabajo_OrdSer, 1, 6) = "IPAQUT" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BPAQT" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "DPAQT" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "RPAQT") Then
                FrmrRelPaquetesdelCliente.Show()
            ElseIf (Mid(GloTrabajo_OrdSer, 1, 5) = "BPAAD" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BSEDI") Then
                FrmrRelPaquetesdelCliente.Show()
            ElseIf (Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "IPAQD" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BPAQD" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "DPAQD" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "RPAQD") Then
                FrmrRelPaquetesdelClienteDigital.Show()
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "IAPAR" Then
                FrmRelCablemodemClientesDigital.Show()
            End If

            If GLOTRABAJO = "RETLI" Then
                Me.GuardaMotivoCanServTableAdapter.Connection = CON
                Me.GuardaMotivoCanServTableAdapter.Fill(Me.DataSetEric.GuardaMotivoCanServ, gloClv_Orden, 1, 0, 0, 0)
            End If
            Me.Close()
        Else
            MsgBox("No a Seleccionado un Servicio al Cliente")
        End If
        CON.Close()

    End Sub

    Private Sub HayConex(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.Dimesihay_ConexTableAdapter.Connection = CON
            Me.Dimesihay_ConexTableAdapter.Fill(Me.NewSofTvDataSet.Dimesihay_Conex, New System.Nullable(Of Long)(CType(gloClv_Orden, Long)), op)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub


   
    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        'Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)

        Me.ComboBox2.Text = ""
        'Me.MUESTRATRABAJOSTableAdapter.Connection = CON
        'Me.MUESTRATRABAJOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOS, CInt(Me.ComboBox4.SelectedValue))
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_TipSer", SqlDbType.Int, CInt(Me.ComboBox4.SelectedValue))
        BaseII.CreateMyParameter("@contrato", SqlDbType.Int, Contrato)
        ComboBox2.DataSource = BaseII.ConsultaDT("MUESTRATRABAJOSPorContrato")
        Me.ComboBox2.Text = ""
        CON.Close()
    End Sub
End Class