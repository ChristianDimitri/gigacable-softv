﻿Public Class frmAsignacionOrdenQuejaTecnicos
    Dim LocClvOrdenQueja As Long
    Dim LocContrato As Long

#Region "EVENTOS"
    Private Sub frmAsignacionOrdenQuejaTecnicos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.Panel1.BackColor = Me.BackColor
        Me.Panel2.BackColor = Me.BackColor
        Me.Label1.BackColor = Me.BackColor
        Me.Label2.BackColor = Me.BackColor
        Me.Label3.BackColor = Me.BackColor
        Me.Label4.BackColor = Me.BackColor
        Me.Label5.BackColor = Me.BackColor
        If GloTipo = "A" Then
            Me.dgAsignarOrdenes.Visible = True
            Me.dgConsultarOrdenes.Visible = False
        End If
        If GloTipo = "C" Then
            Me.dgAsignarOrdenes.Visible = False
            Me.dgConsultarOrdenes.Visible = True
        End If
        UspMostrarAsignaturaOrdenes()

    End Sub

#End Region

#Region "CONTROLES"
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

  

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        UspMostrarAsignaturaOrdenes()
    End Sub
    Private Sub btnGuardarAsignatura_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardarAsignatura.Click
        Dim I As Integer
        Dim BANDERA As Boolean
        Dim BND2 As Boolean
        Dim BND3 As Boolean
        BND3 = False
        BND2 = False
        BANDERA = False

        For I = 0 To dgAsignarOrdenes.Rows.Count - 1
            BANDERA = Me.dgAsignarOrdenes.Item(1, I).Value
            If BANDERA = True And Me.dgAsignarOrdenes.Item(7, I).Value = "SIN ASIGNAR" Then
                UspGuardaOrdenesTMP(Me.dgAsignarOrdenes.Item(2, I).Value, Me.dgAsignarOrdenes.Item(3, I).Value)
                BND2 = True
            ElseIf BANDERA = True And Me.dgAsignarOrdenes.Item(7, I).Value <> "SIN ASIGNAR" Then
                UspGuardaListaReasignar(Me.dgAsignarOrdenes.Item(2, I).Value, Me.dgAsignarOrdenes.Item(3, I).Value)
                BND3 = True
            End If
        Next
        If BND2 = True Then
            frmAsignacionAlTecnico.ShowDialog()
        End If
        If BND3 = True Then
            frmReasignarTecnico.ShowDialog()
        End If
        If BND3 = False And BND2 = False Then
            MsgBox("No hay ningun trabajo seleccionado por asignar", MsgBoxStyle.Information, "Asignación Ordenes/Quejas")
        End If
        Me.Close()
    End Sub
#End Region

#Region "METODOS"
    Private Sub UspMostrarAsignaturaOrdenes()
        Try
            If txtBuscarOrdenQueja.Text = "" Then
                LocClvOrdenQueja = 0
            Else
                LocClvOrdenQueja = CInt(txtBuscarOrdenQueja.Text)

            End If
            If txtbuscarContrato.Text = "" Then
                LocContrato = 0
            Else
                LocContrato = CInt(txtbuscarContrato.Text)
            End If
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVORDENQUEJA", SqlDbType.BigInt, LocClvOrdenQueja)
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, LocContrato)
            BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, Me.txtNombre.Text, 250)
            BaseII.CreateMyParameter("@DIRECCION", SqlDbType.VarChar, Me.txtdireccion.Text, 250)
            If GloTipo = "A" Then
                Me.dgAsignarOrdenes.DataSource = BaseII.ConsultaDT("UspMostrarAsignaturaOrdenes")
            ElseIf GloTipo = "C" Then
                Me.dgConsultarOrdenes.DataSource = BaseII.ConsultaDT("UspMostrarAsignaturaOrdenes")
            End If
            Me.txtdireccion.Text = ""
            Me.txtbuscarContrato.Text = ""
            Me.txtBuscarOrdenQueja.Text = ""
            Me.txtNombre.Text = ""

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspGuardaOrdenesTMP(ByVal PRMCLVORDEN As Long, ByVal PRMTIPO As String)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVORDENQUEJA", SqlDbType.BigInt, PRMCLVORDEN)
            BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, PRMTIPO, 50)
            BaseII.Inserta("UspGuardaOrdenesTMP")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspGuardaListaReasignar(ByVal PRMCLVORDEN As Long, ByVal PRMTIPO As String)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVORDENQUEJA", SqlDbType.BigInt, PRMCLVORDEN)
            BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, PRMTIPO, 50)
            BaseII.Inserta("UspGuardaListaReasignar")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region




    Private Sub dgAsignarOrdenes_CellDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgAsignarOrdenes.CellDoubleClick
        If Me.dgAsignarOrdenes.SelectedCells(7).Value.ToString = "SIN ASIGNAR" Then
            Nombretecnico = Me.dgConsultarOrdenes.SelectedCells(7).Value.ToString
            Tipotecnico = Me.dgConsultarOrdenes.SelectedCells(3).Value.ToString
            ClvOrdenQueja1 = CLng(Me.dgConsultarOrdenes.SelectedCells(2).Value)
            GloTipo = "C"
            frminformacionAgenda.Show()
        Else
            MsgBox("Esta Orden/Queja no esta asignado a ningun Técnico", MsgBoxStyle.Information, "Asignación Ordenes/Quejas")
        End If
       
    End Sub
End Class