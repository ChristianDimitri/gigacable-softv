﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTrabajos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim TipoLabel As System.Windows.Forms.Label
        Dim TRABAJOLabel As System.Windows.Forms.Label
        Dim DESCRIPCIONLabel As System.Windows.Forms.Label
        Dim CMBSICALabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTrabajos))
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.CONTRABAJOSBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.TrabajosBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.checkCobroMaterial = New System.Windows.Forms.CheckBox()
        Me.DESCRIPCIONTextBox = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.rdQueja = New System.Windows.Forms.RadioButton()
        Me.rbServicio = New System.Windows.Forms.RadioButton()
        Me.TRABAJOTextBox = New System.Windows.Forms.TextBox()
        Me.PUNTOSTextBox = New System.Windows.Forms.TextBox()
        Me.SICACheckBox = New System.Windows.Forms.CheckBox()
        Me.pnlTrabajos = New System.Windows.Forms.Panel()
        Me.btnPuntosRangoTV = New System.Windows.Forms.Button()
        Me.lblCuadrilla = New System.Windows.Forms.Label()
        Me.txtPuntosCuadrilla = New System.Windows.Forms.TextBox()
        Me.pnlMaterial = New System.Windows.Forms.Panel()
        Me.dgvMaterial = New System.Windows.Forms.DataGridView()
        Me.Clv_Material = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Concepto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cantidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.CMBLabel4 = New System.Windows.Forms.Label()
        Me.txtCantidad = New System.Windows.Forms.TextBox()
        Me.cmbArticulos = New System.Windows.Forms.ComboBox()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.cmbMaterial = New System.Windows.Forms.ComboBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        TipoLabel = New System.Windows.Forms.Label()
        TRABAJOLabel = New System.Windows.Forms.Label()
        DESCRIPCIONLabel = New System.Windows.Forms.Label()
        CMBSICALabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        CType(Me.TrabajosBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TrabajosBindingNavigator.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.pnlTrabajos.SuspendLayout()
        Me.pnlMaterial.SuspendLayout()
        CType(Me.dgvMaterial, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TipoLabel
        '
        TipoLabel.AutoSize = True
        TipoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TipoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        TipoLabel.Location = New System.Drawing.Point(3, 0)
        TipoLabel.Name = "TipoLabel"
        TipoLabel.Size = New System.Drawing.Size(51, 18)
        TipoLabel.TabIndex = 0
        TipoLabel.Text = "Tipo :"
        '
        'TRABAJOLabel
        '
        TRABAJOLabel.AutoSize = True
        TRABAJOLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TRABAJOLabel.ForeColor = System.Drawing.Color.LightSlateGray
        TRABAJOLabel.Location = New System.Drawing.Point(64, 8)
        TRABAJOLabel.Name = "TRABAJOLabel"
        TRABAJOLabel.Size = New System.Drawing.Size(50, 15)
        TRABAJOLabel.TabIndex = 6
        TRABAJOLabel.Text = "Clave :"
        '
        'DESCRIPCIONLabel
        '
        DESCRIPCIONLabel.AutoSize = True
        DESCRIPCIONLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DESCRIPCIONLabel.ForeColor = System.Drawing.Color.LightSlateGray
        DESCRIPCIONLabel.Location = New System.Drawing.Point(24, 36)
        DESCRIPCIONLabel.Name = "DESCRIPCIONLabel"
        DESCRIPCIONLabel.Size = New System.Drawing.Size(91, 15)
        DESCRIPCIONLabel.TabIndex = 8
        DESCRIPCIONLabel.Text = "Descripción :"
        '
        'CMBSICALabel
        '
        CMBSICALabel.AutoSize = True
        CMBSICALabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBSICALabel.ForeColor = System.Drawing.Color.DarkOrange
        CMBSICALabel.Location = New System.Drawing.Point(24, 180)
        CMBSICALabel.Name = "CMBSICALabel"
        CMBSICALabel.Size = New System.Drawing.Size(587, 16)
        CMBSICALabel.TabIndex = 10
        CMBSICALabel.Text = "Se Relacciona con el Proceso Automatico de Cablemodems ó Aparatos Digitales   :"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(54, 62)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(59, 15)
        Label1.TabIndex = 136
        Label1.Text = "Puntos :"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'CONTRABAJOSBindingNavigatorSaveItem
        '
        Me.CONTRABAJOSBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONTRABAJOSBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONTRABAJOSBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONTRABAJOSBindingNavigatorSaveItem.Name = "CONTRABAJOSBindingNavigatorSaveItem"
        Me.CONTRABAJOSBindingNavigatorSaveItem.Size = New System.Drawing.Size(91, 22)
        Me.CONTRABAJOSBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'TrabajosBindingNavigator
        '
        Me.TrabajosBindingNavigator.AddNewItem = Nothing
        Me.TrabajosBindingNavigator.CountItem = Nothing
        Me.TrabajosBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.TrabajosBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.CONTRABAJOSBindingNavigatorSaveItem})
        Me.TrabajosBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.TrabajosBindingNavigator.MoveFirstItem = Nothing
        Me.TrabajosBindingNavigator.MoveLastItem = Nothing
        Me.TrabajosBindingNavigator.MoveNextItem = Nothing
        Me.TrabajosBindingNavigator.MovePreviousItem = Nothing
        Me.TrabajosBindingNavigator.Name = "TrabajosBindingNavigator"
        Me.TrabajosBindingNavigator.PositionItem = Nothing
        Me.TrabajosBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TrabajosBindingNavigator.Size = New System.Drawing.Size(757, 25)
        Me.TrabajosBindingNavigator.TabIndex = 1
        Me.TrabajosBindingNavigator.TabStop = True
        Me.TrabajosBindingNavigator.Text = "BindingNavigator1"
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.CMBLabel2.Location = New System.Drawing.Point(424, 148)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(250, 16)
        Me.CMBLabel2.TabIndex = 2
        Me.CMBLabel2.Text = "Se Cobra La Descarga De Material"
        '
        'checkCobroMaterial
        '
        Me.checkCobroMaterial.AutoSize = True
        Me.checkCobroMaterial.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.checkCobroMaterial.ForeColor = System.Drawing.Color.Black
        Me.checkCobroMaterial.Location = New System.Drawing.Point(403, 148)
        Me.checkCobroMaterial.Name = "checkCobroMaterial"
        Me.checkCobroMaterial.Size = New System.Drawing.Size(15, 14)
        Me.checkCobroMaterial.TabIndex = 3
        Me.checkCobroMaterial.UseVisualStyleBackColor = True
        '
        'DESCRIPCIONTextBox
        '
        Me.DESCRIPCIONTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DESCRIPCIONTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DESCRIPCIONTextBox.Location = New System.Drawing.Point(117, 33)
        Me.DESCRIPCIONTextBox.Name = "DESCRIPCIONTextBox"
        Me.DESCRIPCIONTextBox.Size = New System.Drawing.Size(459, 21)
        Me.DESCRIPCIONTextBox.TabIndex = 1
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Wheat
        Me.Panel2.Controls.Add(Me.rdQueja)
        Me.Panel2.Controls.Add(Me.rbServicio)
        Me.Panel2.Controls.Add(TipoLabel)
        Me.Panel2.ForeColor = System.Drawing.Color.Silver
        Me.Panel2.Location = New System.Drawing.Point(117, 118)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(230, 59)
        Me.Panel2.TabIndex = 0
        '
        'rdQueja
        '
        Me.rdQueja.AutoSize = True
        Me.rdQueja.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.rdQueja.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdQueja.ForeColor = System.Drawing.Color.OrangeRed
        Me.rdQueja.Location = New System.Drawing.Point(140, 23)
        Me.rdQueja.Name = "rdQueja"
        Me.rdQueja.Size = New System.Drawing.Size(73, 24)
        Me.rdQueja.TabIndex = 1
        Me.rdQueja.TabStop = True
        Me.rdQueja.Text = "Queja"
        Me.rdQueja.UseVisualStyleBackColor = True
        '
        'rbServicio
        '
        Me.rbServicio.AutoSize = True
        Me.rbServicio.Checked = True
        Me.rbServicio.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.rbServicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbServicio.ForeColor = System.Drawing.Color.OrangeRed
        Me.rbServicio.Location = New System.Drawing.Point(14, 23)
        Me.rbServicio.Name = "rbServicio"
        Me.rbServicio.Size = New System.Drawing.Size(89, 24)
        Me.rbServicio.TabIndex = 0
        Me.rbServicio.TabStop = True
        Me.rbServicio.Text = "Servicio"
        Me.rbServicio.UseVisualStyleBackColor = True
        '
        'TRABAJOTextBox
        '
        Me.TRABAJOTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TRABAJOTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TRABAJOTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TRABAJOTextBox.Location = New System.Drawing.Point(117, 5)
        Me.TRABAJOTextBox.MaxLength = 6
        Me.TRABAJOTextBox.Name = "TRABAJOTextBox"
        Me.TRABAJOTextBox.Size = New System.Drawing.Size(104, 21)
        Me.TRABAJOTextBox.TabIndex = 0
        Me.TRABAJOTextBox.TabStop = False
        '
        'PUNTOSTextBox
        '
        Me.PUNTOSTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PUNTOSTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PUNTOSTextBox.Location = New System.Drawing.Point(117, 60)
        Me.PUNTOSTextBox.Name = "PUNTOSTextBox"
        Me.PUNTOSTextBox.Size = New System.Drawing.Size(104, 21)
        Me.PUNTOSTextBox.TabIndex = 2
        '
        'SICACheckBox
        '
        Me.SICACheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SICACheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SICACheckBox.Location = New System.Drawing.Point(617, 180)
        Me.SICACheckBox.Name = "SICACheckBox"
        Me.SICACheckBox.Size = New System.Drawing.Size(77, 24)
        Me.SICACheckBox.TabIndex = 4
        '
        'pnlTrabajos
        '
        Me.pnlTrabajos.Controls.Add(Me.btnPuntosRangoTV)
        Me.pnlTrabajos.Controls.Add(Me.lblCuadrilla)
        Me.pnlTrabajos.Controls.Add(Me.txtPuntosCuadrilla)
        Me.pnlTrabajos.Controls.Add(Label1)
        Me.pnlTrabajos.Controls.Add(Me.TRABAJOTextBox)
        Me.pnlTrabajos.Controls.Add(Me.CMBLabel2)
        Me.pnlTrabajos.Controls.Add(Me.SICACheckBox)
        Me.pnlTrabajos.Controls.Add(Me.checkCobroMaterial)
        Me.pnlTrabajos.Controls.Add(CMBSICALabel)
        Me.pnlTrabajos.Controls.Add(Me.DESCRIPCIONTextBox)
        Me.pnlTrabajos.Controls.Add(Me.PUNTOSTextBox)
        Me.pnlTrabajos.Controls.Add(Me.Panel2)
        Me.pnlTrabajos.Controls.Add(TRABAJOLabel)
        Me.pnlTrabajos.Controls.Add(DESCRIPCIONLabel)
        Me.pnlTrabajos.Location = New System.Drawing.Point(12, 28)
        Me.pnlTrabajos.Name = "pnlTrabajos"
        Me.pnlTrabajos.Size = New System.Drawing.Size(733, 206)
        Me.pnlTrabajos.TabIndex = 0
        '
        'btnPuntosRangoTV
        '
        Me.btnPuntosRangoTV.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPuntosRangoTV.Location = New System.Drawing.Point(244, 59)
        Me.btnPuntosRangoTV.Name = "btnPuntosRangoTV"
        Me.btnPuntosRangoTV.Size = New System.Drawing.Size(153, 23)
        Me.btnPuntosRangoTV.TabIndex = 139
        Me.btnPuntosRangoTV.Text = "Puntos Rango de TV's"
        Me.btnPuntosRangoTV.UseVisualStyleBackColor = True
        '
        'lblCuadrilla
        '
        Me.lblCuadrilla.AutoSize = True
        Me.lblCuadrilla.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCuadrilla.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblCuadrilla.Location = New System.Drawing.Point(49, 91)
        Me.lblCuadrilla.Name = "lblCuadrilla"
        Me.lblCuadrilla.Size = New System.Drawing.Size(64, 13)
        Me.lblCuadrilla.TabIndex = 138
        Me.lblCuadrilla.Text = "Cuadrilla :"
        '
        'txtPuntosCuadrilla
        '
        Me.txtPuntosCuadrilla.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPuntosCuadrilla.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPuntosCuadrilla.Location = New System.Drawing.Point(117, 87)
        Me.txtPuntosCuadrilla.Name = "txtPuntosCuadrilla"
        Me.txtPuntosCuadrilla.Size = New System.Drawing.Size(104, 21)
        Me.txtPuntosCuadrilla.TabIndex = 137
        '
        'pnlMaterial
        '
        Me.pnlMaterial.Controls.Add(Me.dgvMaterial)
        Me.pnlMaterial.Controls.Add(Me.btnEliminar)
        Me.pnlMaterial.Controls.Add(Me.btnAgregar)
        Me.pnlMaterial.Controls.Add(Me.CMBLabel4)
        Me.pnlMaterial.Controls.Add(Me.txtCantidad)
        Me.pnlMaterial.Controls.Add(Me.cmbArticulos)
        Me.pnlMaterial.Controls.Add(Me.CMBLabel3)
        Me.pnlMaterial.Controls.Add(Me.cmbMaterial)
        Me.pnlMaterial.Controls.Add(Me.CMBLabel1)
        Me.pnlMaterial.Location = New System.Drawing.Point(12, 240)
        Me.pnlMaterial.Name = "pnlMaterial"
        Me.pnlMaterial.Size = New System.Drawing.Size(736, 370)
        Me.pnlMaterial.TabIndex = 0
        '
        'dgvMaterial
        '
        Me.dgvMaterial.AllowUserToAddRows = False
        Me.dgvMaterial.AllowUserToDeleteRows = False
        Me.dgvMaterial.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvMaterial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMaterial.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Material, Me.Concepto, Me.Descripcion, Me.Cantidad})
        Me.dgvMaterial.Location = New System.Drawing.Point(3, 116)
        Me.dgvMaterial.Name = "dgvMaterial"
        Me.dgvMaterial.ReadOnly = True
        Me.dgvMaterial.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvMaterial.Size = New System.Drawing.Size(730, 251)
        Me.dgvMaterial.TabIndex = 0
        '
        'Clv_Material
        '
        Me.Clv_Material.DataPropertyName = "Clv_Material"
        Me.Clv_Material.HeaderText = "# Material"
        Me.Clv_Material.Name = "Clv_Material"
        Me.Clv_Material.ReadOnly = True
        '
        'Concepto
        '
        Me.Concepto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Concepto.DataPropertyName = "Concepto"
        Me.Concepto.HeaderText = "Material"
        Me.Concepto.Name = "Concepto"
        Me.Concepto.ReadOnly = True
        '
        'Descripcion
        '
        Me.Descripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Tipo Material"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        '
        'Cantidad
        '
        Me.Cantidad.DataPropertyName = "Cantidad"
        Me.Cantidad.HeaderText = "Cantidad"
        Me.Cantidad.Name = "Cantidad"
        Me.Cantidad.ReadOnly = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar.Location = New System.Drawing.Point(382, 88)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(96, 23)
        Me.btnEliminar.TabIndex = 4
        Me.btnEliminar.Text = "&Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar.Location = New System.Drawing.Point(264, 88)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(96, 23)
        Me.btnAgregar.TabIndex = 3
        Me.btnAgregar.Text = "&Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.Location = New System.Drawing.Point(180, 62)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(78, 16)
        Me.CMBLabel4.TabIndex = 8
        Me.CMBLabel4.Text = "Cantidad :"
        '
        'txtCantidad
        '
        Me.txtCantidad.Location = New System.Drawing.Point(264, 62)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(114, 20)
        Me.txtCantidad.TabIndex = 2
        '
        'cmbArticulos
        '
        Me.cmbArticulos.CausesValidation = False
        Me.cmbArticulos.FormattingEnabled = True
        Me.cmbArticulos.Location = New System.Drawing.Point(264, 35)
        Me.cmbArticulos.Name = "cmbArticulos"
        Me.cmbArticulos.Size = New System.Drawing.Size(298, 21)
        Me.cmbArticulos.TabIndex = 1
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(77, 36)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(181, 16)
        Me.CMBLabel3.TabIndex = 7
        Me.CMBLabel3.Text = "Descripción del Articulo :"
        '
        'cmbMaterial
        '
        Me.cmbMaterial.FormattingEnabled = True
        Me.cmbMaterial.Location = New System.Drawing.Point(264, 8)
        Me.cmbMaterial.Name = "cmbMaterial"
        Me.cmbMaterial.Size = New System.Drawing.Size(298, 21)
        Me.cmbMaterial.TabIndex = 0
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(71, 9)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(187, 16)
        Me.CMBLabel1.TabIndex = 6
        Me.CMBLabel1.Text = "Clasificación de Material :"
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(612, 616)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(136, 33)
        Me.btnSalir.TabIndex = 0
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'FrmTrabajos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(757, 656)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.pnlMaterial)
        Me.Controls.Add(Me.pnlTrabajos)
        Me.Controls.Add(Me.TrabajosBindingNavigator)
        Me.Name = "FrmTrabajos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Trabajos"
        CType(Me.TrabajosBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TrabajosBindingNavigator.ResumeLayout(False)
        Me.TrabajosBindingNavigator.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.pnlTrabajos.ResumeLayout(False)
        Me.pnlTrabajos.PerformLayout()
        Me.pnlMaterial.ResumeLayout(False)
        Me.pnlMaterial.PerformLayout()
        CType(Me.dgvMaterial, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONTRABAJOSBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents TrabajosBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents checkCobroMaterial As System.Windows.Forms.CheckBox
    Friend WithEvents DESCRIPCIONTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents rdQueja As System.Windows.Forms.RadioButton
    Friend WithEvents rbServicio As System.Windows.Forms.RadioButton
    Friend WithEvents TRABAJOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PUNTOSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SICACheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents pnlTrabajos As System.Windows.Forms.Panel
    Friend WithEvents pnlMaterial As System.Windows.Forms.Panel
    Friend WithEvents dgvMaterial As System.Windows.Forms.DataGridView
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    Friend WithEvents txtCantidad As System.Windows.Forms.TextBox
    Friend WithEvents cmbArticulos As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents cmbMaterial As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents Clv_Material As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Concepto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cantidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtPuntosCuadrilla As System.Windows.Forms.TextBox
    Friend WithEvents lblCuadrilla As System.Windows.Forms.Label
    Friend WithEvents btnPuntosRangoTV As System.Windows.Forms.Button
End Class
