﻿Public Class BrwTipoUsuario

    Dim bandNueoMod As Integer = 0 'bandNueoMod 1 para Agregar 2 para modificar
    Dim nopasa As Integer = 0 'Se usa para validar que no este repetida la descripción


    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label2.Click

    End Sub

    Private Sub BrwTipoUsuario_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        colorea(Me, Me.Name)
        Label1.ForeColor = Color.Black
        Label2.ForeColor = Color.Black
        Label3.ForeColor = Color.Black
        ConsultaTipoUsuario()

        DataGridView1.Columns(0).Width = 180
        DataGridView1.Columns(1).Width = 220

        DataGridView1.Columns(0).HeaderText = "Clave TipoUsuario"
        DataGridView1.Columns(1).HeaderText = "Nombre TipoUsuario"
       

    End Sub


    Private Sub ConsultaTipoUsuario()
        Try
            BaseII.limpiaParametros()
            Me.DataGridView1.DataSource = BaseII.ConsultaDT("ConsultaTipoUsuario")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub InsertModTipoUsuario()
        Try

            If TextBox1.Text = "" Then
                TextBox1.Text = 0
            End If

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, bandNueoMod)
            BaseII.CreateMyParameter("@clv_tipoUsuario", SqlDbType.Int, CInt(TextBox1.Text))
            BaseII.CreateMyParameter("@descripción", SqlDbType.VarChar, TextBox2.Text, 250)
            BaseII.CreateMyParameter("@nopasa", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("InsertModTipoUsuario")
            nopasa = BaseII.dicoPar("@nopasa").ToString
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Label3.Visible = True
        Label3.Text = "Agregar Usuario"
        bandNueoMod = 1
        TextBox1.Visible = False
        Label1.Visible = False
        Label2.Visible = True
        TextBox2.Visible = True
        Button3.Visible = True
        TextBox2.Text = ""
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Label3.Visible = True
        Label3.Text = "Modificar Usuario"
        bandNueoMod = 2
        TextBox1.Visible = True
        Label1.Visible = True
        Label2.Visible = True
        TextBox2.Visible = True
        Button3.Visible = True
        Dim row As Integer = Integer.Parse(Me.DataGridView1.CurrentRow.Index.ToString)
        TextBox1.Text = Me.DataGridView1.Rows(row).Cells("Clave_Tipo_Usuario").Value.ToString
        TextBox2.Text = Me.DataGridView1.Rows(row).Cells("Nombre_Usuario").Value.ToString
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Dim row As Integer = Integer.Parse(Me.DataGridView1.CurrentRow.Index.ToString)
        TextBox1.Text = Me.DataGridView1.Rows(row).Cells("Clave_Tipo_Usuario").Value.ToString
        TextBox2.Text = Me.DataGridView1.Rows(row).Cells("Nombre_Usuario").Value.ToString
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        InsertModTipoUsuario()
        If nopasa = 1 Then
            MsgBox("¡El Nombre de Usuario ya existe, favor de ingresar uno diferente!", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        If bandNueoMod = 1 Then
            MsgBox("¡El Usuario se ha Agregado con exito!", MsgBoxStyle.Information)
        ElseIf bandNueoMod = 2 Then
            MsgBox("¡El Usuario se ha Modificado con exito!", MsgBoxStyle.Information)
        End If
        ConsultaTipoUsuario()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub
End Class