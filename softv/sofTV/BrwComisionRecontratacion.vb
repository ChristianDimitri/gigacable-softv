﻿Public Class BrwComisionRecontratacion


#Region "Eventos"

    Private Sub BrwCalculoBonos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        UspLlenaNivelUsuario()
        UspLlenaServicios()
        UspLlenaGrupo()
        UspBuscarComisionRecontratacion(0, 0, 0, 99)

        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub txtClave_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtClave.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
        If KeyAscii = 13 Then
            Dim clave As Integer
            clave = 0
            If Len(txtClave.Text) > 0 Then
                clave = CInt(txtClave.Text)
            End If

            UspBuscarComisionRecontratacion(clave, Me.cmbGrupo.SelectedValue, Me.cmbPaquete.SelectedValue, Me.cmbNivelUsuario.SelectedValue)
            Me.txtClave.Text = ""
            Me.cmbGrupo.SelectedValue = 0
            Me.cmbPaquete.SelectedValue = 0
            Me.cmbNivelUsuario.SelectedValue = 99
        End If
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        If DataGridView1.SelectedRows.Count > 0 Then
            Me.lblClave.Text = DataGridView1.SelectedCells(0).Value
            Me.lblServicioPrincipal.Text = DataGridView1.SelectedCells(1).Value
            Me.lblGrupo.Text = DataGridView1.SelectedCells(2).Value
            Me.lblNivelUsuario.Text = DataGridView1.SelectedCells(3).Value
            Me.lblPorcentaje.Text = DataGridView1.SelectedCells(4).Value
            Me.lblMesesPagadosIni.Text = DataGridView1.SelectedCells(5).Value
            Me.lblMesesPagadosFin.Text = DataGridView1.SelectedCells(6).Value
            Me.lblMesesBajaIni.Text = DataGridView1.SelectedCells(7).Value
            Me.lblMesesBajaFin.Text = DataGridView1.SelectedCells(8).Value

        Else
            Me.lblClave.Text = ""
            Me.lblServicioPrincipal.Text = ""
            Me.lblGrupo.Text = ""
            Me.lblNivelUsuario.Text = ""
            Me.lblPorcentaje.Text = ""
            Me.lblMesesPagadosIni.Text = ""
            Me.lblMesesBajaIni.Text = ""
            Me.lblMesesPagadosFin.Text = ""
            Me.lblMesesBajaFin.Text = ""
        End If
    End Sub

    Private Sub BrwCalculoBonos_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        UspBuscarComisionRecontratacion(0, 0, 0, 99)
    End Sub
#End Region

#Region "Funciones"

    Private Sub UspLlenaNivelUsuario()
        Try
            BaseII.limpiaParametros()
            cmbNivelUsuario.DataSource = BaseII.ConsultaDT("UspLlenaNivelUsuario")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspLlenaServicios()
        Try
            BaseII.limpiaParametros()
            cmbPaquete.DataSource = BaseII.ConsultaDT("UspLlenaServicios")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub UspLlenaGrupo()
        Try
            BaseII.limpiaParametros()
            cmbGrupo.DataSource = BaseII.ConsultaDT("UspLlenaGrupo")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function

    Private Sub UspBuscarComisionRecontratacion(ByVal prmClave As Integer, ByVal prmClvGrupo As Integer, ByVal prmClvPaquete As Integer, ByVal prmClvUsuario As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLAVE", SqlDbType.Int, prmClave)
            BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, prmClvGrupo)
            BaseII.CreateMyParameter("@CLVPAQUETE", SqlDbType.Int, prmClvPaquete)
            BaseII.CreateMyParameter("@NIVELUSUARIO", SqlDbType.Int, prmClvUsuario)
            DataGridView1.DataSource = BaseII.ConsultaDT("UspBuscarComisionRecontratacion")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Salir"

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

#End Region

#Region "Buscar"

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Dim clave As Integer
        clave = 0
        If Len(txtClave.Text) > 0 Then
            clave = CInt(txtClave.Text)
        End If
        UspBuscarComisionRecontratacion(clave, Me.cmbGrupo.SelectedValue, Me.cmbPaquete.SelectedValue, Me.cmbNivelUsuario.SelectedValue)
        Me.txtClave.Text = ""
        Me.cmbGrupo.SelectedValue = 0
        Me.cmbPaquete.SelectedValue = 0
        Me.cmbNivelUsuario.SelectedValue = 99
    End Sub

#End Region

#Region "Nuevo"

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        opcion = "N"
        FrmComisionRecontratacion.Show()
    End Sub

#End Region

#Region "Modificar"
    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        opcion = "M"
        GloClaveCalculoBono = DataGridView1.SelectedCells(0).Value
        FrmComisionRecontratacion.Show()
    End Sub
#End Region

#Region "Consultar"
    Private Sub BtnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnConsultar.Click
        opcion = "C"
        GloClaveCalculoBono = DataGridView1.SelectedCells(0).Value
        FrmComisionRecontratacion.Show()
    End Sub
#End Region



#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region
End Class