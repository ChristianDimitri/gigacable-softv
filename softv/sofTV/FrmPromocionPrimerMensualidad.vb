﻿Imports System.Text
Imports System.Data.SqlClient

Public Class FrmPromocionPrimerMensualidad

    Private Sub ConPromocionPrimerMensualidad(ByVal Clv_TipSer As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC ConPromocionPrimerMensualidad " + Clv_TipSer.ToString())
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable
        Dim bSource As New BindingSource

        Try
            dAdapter.Fill(dTable)
            bSource.DataSource = dTable
            dgvPromocion.DataSource = bSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try

    End Sub

    Private Sub ModPromocionPrimerMensualidad(ByVal Clv_Servicio As Integer, ByVal Importe As Decimal, ByVal Descuento As Decimal, ByVal DescuentoCombo As Decimal, ByVal Importe0 As Decimal, ByVal Descuento0 As Decimal, ByVal DescuentoCombo0 As Decimal)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ModPromocionPrimerMensualidad", conexion)
        comando.CommandTimeout = 0
        comando.CommandType = CommandType.StoredProcedure

        Dim par0 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        par0.Direction = ParameterDirection.Input
        par0.Value = Clv_Servicio
        comando.Parameters.Add(par0)

        Dim par1 As New SqlParameter("@Importe", SqlDbType.Decimal)
        par1.Direction = ParameterDirection.Input
        par1.Value = Importe
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Descuento", SqlDbType.Decimal)
        par2.Direction = ParameterDirection.Input
        par2.Value = Descuento
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@DescuentoCombo", SqlDbType.Decimal)
        par3.Direction = ParameterDirection.Input
        par3.Value = DescuentoCombo
        comando.Parameters.Add(par3)

        Dim par10 As New SqlParameter("@Importe0", SqlDbType.Decimal)
        par10.Direction = ParameterDirection.Input
        par10.Value = Importe0
        comando.Parameters.Add(par10)

        Dim par20 As New SqlParameter("@Descuento0", SqlDbType.Decimal)
        par20.Direction = ParameterDirection.Input
        par20.Value = Descuento0
        comando.Parameters.Add(par20)

        Dim par30 As New SqlParameter("@DescuentoCombo0", SqlDbType.Decimal)
        par30.Direction = ParameterDirection.Input
        par30.Value = DescuentoCombo0
        comando.Parameters.Add(par30)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub MuestraTipServEric()
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC MuestraTipServEric 0,0")
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable
        Dim bSource As New BindingSource

        Try
            dAdapter.Fill(dTable)
            bSource.DataSource = dTable
            cbTipSer.DataSource = bSource

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub FrmPromocionPrimerMensualidad_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        MuestraTipServEric()
    End Sub

    Private Sub cbTipSer_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbTipSer.SelectedIndexChanged
        If cbTipSer.Text.Length = 0 Then
            Exit Sub
        End If
        ConPromocionPrimerMensualidad(cbTipSer.SelectedValue)
    End Sub

    Private Sub tsbGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbGuardar.Click
        Try
            Dim i As Integer
            dgvPromocion.EndEdit()
            For i = 0 To dgvPromocion.Rows.Count - 1
                ModPromocionPrimerMensualidad(dgvPromocion.Item(0, i).Value, dgvPromocion.Item(2, i).Value, dgvPromocion.Item(3, i).Value, dgvPromocion.Item(4, i).Value, dgvPromocion.Item(5, i).Value, dgvPromocion.Item(6, i).Value, dgvPromocion.Item(7, i).Value)
            Next
            MsgBox("Se guardó con éxito", MsgBoxStyle.Information)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub
End Class