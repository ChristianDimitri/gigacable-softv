﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMetasSupervisor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim AnioLabel As System.Windows.Forms.Label
        Dim ConceptoLabel As System.Windows.Forms.Label
        Dim GrupoLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMetasSupervisor))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Clv_GrupoTextBox1 = New System.Windows.Forms.TextBox()
        Me.AnioTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_ServicioTextBox = New System.Windows.Forms.TextBox()
        Me.ClaveTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_GrupoTextBox = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ConMetasIndBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ConMetasIndBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ConMetasIndDataGridView = New System.Windows.Forms.DataGridView()
        Me.Clv_Grupo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clave = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ene = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Feb = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Mar = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Abr = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.May = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Jun = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Jul = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ago = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sep = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Oct = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nov = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Dic = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Anio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AnioComboBox = New System.Windows.Forms.ComboBox()
        Me.ConceptoComboBox = New System.Windows.Forms.ComboBox()
        Me.GrupoComboBox = New System.Windows.Forms.ComboBox()
        Me.NombreComboBox = New System.Windows.Forms.ComboBox()
        AnioLabel = New System.Windows.Forms.Label()
        ConceptoLabel = New System.Windows.Forms.Label()
        GrupoLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.ConMetasIndBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConMetasIndBindingNavigator.SuspendLayout()
        CType(Me.ConMetasIndDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'AnioLabel
        '
        AnioLabel.AutoSize = True
        AnioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        AnioLabel.Location = New System.Drawing.Point(741, 8)
        AnioLabel.Name = "AnioLabel"
        AnioLabel.Size = New System.Drawing.Size(35, 15)
        AnioLabel.TabIndex = 24
        AnioLabel.Text = "Año:"
        '
        'ConceptoLabel
        '
        ConceptoLabel.AutoSize = True
        ConceptoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ConceptoLabel.Location = New System.Drawing.Point(527, 8)
        ConceptoLabel.Name = "ConceptoLabel"
        ConceptoLabel.Size = New System.Drawing.Size(114, 15)
        ConceptoLabel.TabIndex = 23
        ConceptoLabel.Text = "Tipo de Servicio:"
        '
        'GrupoLabel
        '
        GrupoLabel.AutoSize = True
        GrupoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        GrupoLabel.Location = New System.Drawing.Point(23, 8)
        GrupoLabel.Name = "GrupoLabel"
        GrupoLabel.Size = New System.Drawing.Size(117, 15)
        GrupoLabel.TabIndex = 21
        GrupoLabel.Text = "Grupo de Ventas:"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.Location = New System.Drawing.Point(245, 8)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(79, 15)
        NombreLabel.TabIndex = 19
        NombreLabel.Text = "Supervisor:"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(771, 484)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 28
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Clv_GrupoTextBox1
        '
        Me.Clv_GrupoTextBox1.Location = New System.Drawing.Point(834, 500)
        Me.Clv_GrupoTextBox1.Name = "Clv_GrupoTextBox1"
        Me.Clv_GrupoTextBox1.ReadOnly = True
        Me.Clv_GrupoTextBox1.Size = New System.Drawing.Size(10, 20)
        Me.Clv_GrupoTextBox1.TabIndex = 33
        Me.Clv_GrupoTextBox1.TabStop = False
        '
        'AnioTextBox
        '
        Me.AnioTextBox.Location = New System.Drawing.Point(882, 500)
        Me.AnioTextBox.Name = "AnioTextBox"
        Me.AnioTextBox.ReadOnly = True
        Me.AnioTextBox.Size = New System.Drawing.Size(10, 20)
        Me.AnioTextBox.TabIndex = 32
        Me.AnioTextBox.TabStop = False
        '
        'Clv_ServicioTextBox
        '
        Me.Clv_ServicioTextBox.Location = New System.Drawing.Point(898, 500)
        Me.Clv_ServicioTextBox.Name = "Clv_ServicioTextBox"
        Me.Clv_ServicioTextBox.ReadOnly = True
        Me.Clv_ServicioTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_ServicioTextBox.TabIndex = 31
        Me.Clv_ServicioTextBox.TabStop = False
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.Location = New System.Drawing.Point(850, 500)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.ReadOnly = True
        Me.ClaveTextBox.Size = New System.Drawing.Size(10, 20)
        Me.ClaveTextBox.TabIndex = 30
        Me.ClaveTextBox.TabStop = False
        '
        'Clv_GrupoTextBox
        '
        Me.Clv_GrupoTextBox.Location = New System.Drawing.Point(866, 500)
        Me.Clv_GrupoTextBox.Name = "Clv_GrupoTextBox"
        Me.Clv_GrupoTextBox.ReadOnly = True
        Me.Clv_GrupoTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_GrupoTextBox.TabIndex = 29
        Me.Clv_GrupoTextBox.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.ConMetasIndBindingNavigator)
        Me.Panel1.Controls.Add(Me.ConMetasIndDataGridView)
        Me.Panel1.Location = New System.Drawing.Point(12, 74)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(898, 376)
        Me.Panel1.TabIndex = 27
        '
        'ConMetasIndBindingNavigator
        '
        Me.ConMetasIndBindingNavigator.AddNewItem = Nothing
        Me.ConMetasIndBindingNavigator.CountItem = Nothing
        Me.ConMetasIndBindingNavigator.DeleteItem = Nothing
        Me.ConMetasIndBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConMetasIndBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.ConMetasIndBindingNavigatorSaveItem})
        Me.ConMetasIndBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ConMetasIndBindingNavigator.MoveFirstItem = Nothing
        Me.ConMetasIndBindingNavigator.MoveLastItem = Nothing
        Me.ConMetasIndBindingNavigator.MoveNextItem = Nothing
        Me.ConMetasIndBindingNavigator.MovePreviousItem = Nothing
        Me.ConMetasIndBindingNavigator.Name = "ConMetasIndBindingNavigator"
        Me.ConMetasIndBindingNavigator.PositionItem = Nothing
        Me.ConMetasIndBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ConMetasIndBindingNavigator.Size = New System.Drawing.Size(898, 25)
        Me.ConMetasIndBindingNavigator.TabIndex = 4
        Me.ConMetasIndBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(88, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ConMetasIndBindingNavigatorSaveItem
        '
        Me.ConMetasIndBindingNavigatorSaveItem.Image = CType(resources.GetObject("ConMetasIndBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ConMetasIndBindingNavigatorSaveItem.Name = "ConMetasIndBindingNavigatorSaveItem"
        Me.ConMetasIndBindingNavigatorSaveItem.Size = New System.Drawing.Size(88, 22)
        Me.ConMetasIndBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'ConMetasIndDataGridView
        '
        Me.ConMetasIndDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ConMetasIndDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.ConMetasIndDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Grupo, Me.Clave, Me.Clv_Servicio, Me.Descripcion, Me.Ene, Me.Feb, Me.Mar, Me.Abr, Me.May, Me.Jun, Me.Jul, Me.Ago, Me.Sep, Me.Oct, Me.Nov, Me.Dic, Me.Anio})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ConMetasIndDataGridView.DefaultCellStyle = DataGridViewCellStyle2
        Me.ConMetasIndDataGridView.Location = New System.Drawing.Point(3, 28)
        Me.ConMetasIndDataGridView.Name = "ConMetasIndDataGridView"
        Me.ConMetasIndDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.ConMetasIndDataGridView.Size = New System.Drawing.Size(892, 345)
        Me.ConMetasIndDataGridView.TabIndex = 2
        '
        'Clv_Grupo
        '
        Me.Clv_Grupo.DataPropertyName = "Clv_Grupo"
        Me.Clv_Grupo.HeaderText = "Clv_Grupo"
        Me.Clv_Grupo.Name = "Clv_Grupo"
        Me.Clv_Grupo.Visible = False
        '
        'Clave
        '
        Me.Clave.DataPropertyName = "Clave"
        Me.Clave.HeaderText = "Clave"
        Me.Clave.Name = "Clave"
        Me.Clave.Visible = False
        '
        'Clv_Servicio
        '
        Me.Clv_Servicio.DataPropertyName = "Clv_Servicio"
        Me.Clv_Servicio.HeaderText = "Clv_Servicio"
        Me.Clv_Servicio.Name = "Clv_Servicio"
        Me.Clv_Servicio.Visible = False
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Servicio"
        Me.Descripcion.HeaderText = "Descripcion"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        Me.Descripcion.Width = 200
        '
        'Ene
        '
        Me.Ene.DataPropertyName = "Ene"
        Me.Ene.HeaderText = "Ene"
        Me.Ene.Name = "Ene"
        Me.Ene.Width = 50
        '
        'Feb
        '
        Me.Feb.DataPropertyName = "Feb"
        Me.Feb.HeaderText = "Feb"
        Me.Feb.Name = "Feb"
        Me.Feb.Width = 50
        '
        'Mar
        '
        Me.Mar.DataPropertyName = "Mar"
        Me.Mar.HeaderText = "Mar"
        Me.Mar.Name = "Mar"
        Me.Mar.Width = 50
        '
        'Abr
        '
        Me.Abr.DataPropertyName = "Abr"
        Me.Abr.HeaderText = "Abr"
        Me.Abr.Name = "Abr"
        Me.Abr.Width = 50
        '
        'May
        '
        Me.May.DataPropertyName = "May"
        Me.May.HeaderText = "May"
        Me.May.Name = "May"
        Me.May.Width = 50
        '
        'Jun
        '
        Me.Jun.DataPropertyName = "Jun"
        Me.Jun.HeaderText = "Jun"
        Me.Jun.Name = "Jun"
        Me.Jun.Width = 50
        '
        'Jul
        '
        Me.Jul.DataPropertyName = "Jul"
        Me.Jul.HeaderText = "Jul"
        Me.Jul.Name = "Jul"
        Me.Jul.Width = 50
        '
        'Ago
        '
        Me.Ago.DataPropertyName = "Ago"
        Me.Ago.HeaderText = "Ago"
        Me.Ago.Name = "Ago"
        Me.Ago.Width = 50
        '
        'Sep
        '
        Me.Sep.DataPropertyName = "Sep"
        Me.Sep.HeaderText = "Sep"
        Me.Sep.Name = "Sep"
        Me.Sep.Width = 50
        '
        'Oct
        '
        Me.Oct.DataPropertyName = "Oct"
        Me.Oct.HeaderText = "Oct"
        Me.Oct.Name = "Oct"
        Me.Oct.Width = 50
        '
        'Nov
        '
        Me.Nov.DataPropertyName = "Nov"
        Me.Nov.HeaderText = "Nov"
        Me.Nov.Name = "Nov"
        Me.Nov.Width = 50
        '
        'Dic
        '
        Me.Dic.DataPropertyName = "Dic"
        Me.Dic.HeaderText = "Dic"
        Me.Dic.Name = "Dic"
        Me.Dic.Width = 50
        '
        'Anio
        '
        Me.Anio.DataPropertyName = "Anio"
        Me.Anio.HeaderText = "Anio"
        Me.Anio.Name = "Anio"
        Me.Anio.Visible = False
        '
        'AnioComboBox
        '
        Me.AnioComboBox.DisplayMember = "Anio"
        Me.AnioComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AnioComboBox.FormattingEnabled = True
        Me.AnioComboBox.Location = New System.Drawing.Point(744, 26)
        Me.AnioComboBox.Name = "AnioComboBox"
        Me.AnioComboBox.Size = New System.Drawing.Size(121, 23)
        Me.AnioComboBox.TabIndex = 26
        Me.AnioComboBox.ValueMember = "Anio"
        '
        'ConceptoComboBox
        '
        Me.ConceptoComboBox.DisplayMember = "Concepto"
        Me.ConceptoComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoComboBox.FormattingEnabled = True
        Me.ConceptoComboBox.Location = New System.Drawing.Point(530, 26)
        Me.ConceptoComboBox.Name = "ConceptoComboBox"
        Me.ConceptoComboBox.Size = New System.Drawing.Size(191, 23)
        Me.ConceptoComboBox.TabIndex = 25
        Me.ConceptoComboBox.ValueMember = "Clv_TipSer"
        '
        'GrupoComboBox
        '
        Me.GrupoComboBox.DisplayMember = "Grupo"
        Me.GrupoComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GrupoComboBox.FormattingEnabled = True
        Me.GrupoComboBox.Location = New System.Drawing.Point(26, 26)
        Me.GrupoComboBox.Name = "GrupoComboBox"
        Me.GrupoComboBox.Size = New System.Drawing.Size(186, 23)
        Me.GrupoComboBox.TabIndex = 22
        Me.GrupoComboBox.ValueMember = "Clv_Grupo"
        '
        'NombreComboBox
        '
        Me.NombreComboBox.DisplayMember = "Nombre"
        Me.NombreComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreComboBox.FormattingEnabled = True
        Me.NombreComboBox.Location = New System.Drawing.Point(248, 26)
        Me.NombreComboBox.Name = "NombreComboBox"
        Me.NombreComboBox.Size = New System.Drawing.Size(247, 23)
        Me.NombreComboBox.TabIndex = 20
        Me.NombreComboBox.ValueMember = "Clave"
        '
        'FrmMetasSupervisor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(923, 541)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Clv_GrupoTextBox1)
        Me.Controls.Add(Me.AnioTextBox)
        Me.Controls.Add(Me.Clv_ServicioTextBox)
        Me.Controls.Add(Me.ClaveTextBox)
        Me.Controls.Add(Me.Clv_GrupoTextBox)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(AnioLabel)
        Me.Controls.Add(Me.AnioComboBox)
        Me.Controls.Add(ConceptoLabel)
        Me.Controls.Add(Me.ConceptoComboBox)
        Me.Controls.Add(GrupoLabel)
        Me.Controls.Add(Me.GrupoComboBox)
        Me.Controls.Add(NombreLabel)
        Me.Controls.Add(Me.NombreComboBox)
        Me.Name = "FrmMetasSupervisor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmMetasSupervisor"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.ConMetasIndBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConMetasIndBindingNavigator.ResumeLayout(False)
        Me.ConMetasIndBindingNavigator.PerformLayout()
        CType(Me.ConMetasIndDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Clv_GrupoTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents AnioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ServicioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_GrupoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents AnioComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ConceptoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents GrupoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents NombreComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ConMetasIndBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConMetasIndBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConMetasIndDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Clv_Grupo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clave As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Ene As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Feb As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Mar As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Abr As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents May As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Jun As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Jul As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Ago As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sep As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Oct As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nov As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Dic As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Anio As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
