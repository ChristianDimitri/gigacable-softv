﻿Imports CrystalDecisions.CrystalReports.Engine
Imports System.Collections.Generic

Public Class FrmComisionesTecnicos




#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabCtrl = New TabControl
                tabCtrl = ctl
                bwrTabControlPerfilDesactiva(tabCtrl, formulario, NomInterno)
                tabCtrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabpg = New TabPage
                tabpg = ctl
                bwrTabPagePerfilDesactiva(tabpg, formulario, NomInterno)
                tabpg = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabCtrl = New TabControl
                tabCtrl = ctl
                bwrTabControlPerfilDesactiva(tabCtrl, formulario, NomInterno)
                tabCtrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabpg = New TabPage
                tabpg = ctl
                bwrTabPagePerfilDesactiva(tabpg, formulario, NomInterno)
                tabpg = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabCtrl = New TabControl
                tabCtrl = ctl
                bwrTabControlPerfilDesactiva(tabCtrl, formulario, NomInterno)
                tabCtrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabpg = New TabPage
                tabpg = ctl
                bwrTabPagePerfilDesactiva(tabpg, formulario, NomInterno)
                tabpg = Nothing
            End If

        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabCtrl = New TabControl
                tabCtrl = ctl
                bwrTabControlPerfilDesactiva(tabCtrl, formulario, NomInterno)
                tabCtrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabpg = New TabPage
                tabpg = ctl
                bwrTabPagePerfilDesactiva(tabpg, formulario, NomInterno)
                tabpg = Nothing
            End If
        Next
    End Sub

    Public Sub bwrTabControlPerfilDesactiva(ByVal tabctrl1 As TabControl, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage

        For Each ctl As Control In tabctrl1.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabCtrl = New TabControl
                tabCtrl = ctl
                bwrTabControlPerfilDesactiva(tabCtrl, formulario, NomInterno)
                tabCtrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabpg = New TabPage
                tabpg = ctl
                bwrTabPagePerfilDesactiva(tabpg, formulario, NomInterno)
                tabpg = Nothing
            End If
        Next
    End Sub

    Public Sub bwrTabPagePerfilDesactiva(ByVal tabPG1 As TabPage, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage

        For Each ctl As Control In tabPG1.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabCtrl = New TabControl
                tabCtrl = ctl
                bwrTabControlPerfilDesactiva(tabCtrl, formulario, NomInterno)
                tabCtrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabpg = New TabPage
                tabpg = ctl
                bwrTabPagePerfilDesactiva(tabpg, formulario, NomInterno)
                tabpg = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region


#Region "Eventos"
    Private Sub FrmComisionesTecnicos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Label1.ForeColor = Color.Black
        Label2.ForeColor = Color.Black
        Label3.ForeColor = Color.Black
        UspMostrarTecnicosReporteComision()
        MuestraAniosComisiones()
        UspMesesComisiones()
        Me.cboxMesCompleto.Checked = True

        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub cboxMesCompleto_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboxMesCompleto.CheckedChanged
        If cboxMesCompleto.Checked = True Then
            GroupBox2.Enabled = True
            dtpFechaInicial.Enabled = False
            dtpFechaFinal.Enabled = False
        Else
            GroupBox2.Enabled = False
            dtpFechaInicial.Enabled = True
            dtpFechaFinal.Enabled = True
        End If
    End Sub

    Private Sub dtpFechaInicial_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaInicial.ValueChanged
        dtpFechaFinal.MinDate = dtpFechaInicial.Value
    End Sub

    Private Sub dtpFechaFinal_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaFinal.ValueChanged
        dtpFechaInicial.MaxDate = dtpFechaFinal.Value
    End Sub

    Private Sub CMBAnio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBAnio.SelectedIndexChanged
        If Len(CMBAnio.Text) > 0 Then
            UspMesesComisiones()
        End If
    End Sub
#End Region

#Region "Aceptar"
    Private Sub BtnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAceptar.Click
        Dim DS As New DataSet
        Dim rDocument As New ReportDocument
        If cboxMesCompleto.Checked = False Then
            DS = UspReporteComisionTecnico()

            rDocument.Load(RutaReportes + "\ReporteComisionesFechas.rpt")
            rDocument.SetDataSource(DS)

            SetDBReport(DS, rDocument)
            Dim fechas As String = Nothing
            fechas = "De la Fecha: " + dtpFechaInicial.Value + " A la Fecha: " + dtpFechaFinal.Value
            Dim mySelectFormula As String = Nothing
            mySelectFormula = "Comisiones Técnicos"
            rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            rDocument.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            rDocument.DataDefinition.FormulaFields("SubTitulo").Text = "'" & fechas & "'"
        Else
            DS = UspReporteComisionesMes()

            rDocument.Load(RutaReportes + "\ReporteComisionesMes.rpt")
            rDocument.SetDataSource(DS)

            SetDBReport(DS, rDocument)
            Dim fechas As String = Nothing
            fechas = "Del mes de " + Me.CMBMes.Text + " " + Me.CMBAnio.Text
            Dim mySelectFormula As String = Nothing
            mySelectFormula = "Comisiones Técnicos"
            rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            rDocument.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            rDocument.DataDefinition.FormulaFields("SubTitulo").Text = "'" & fechas & "'"
        End If

        'mySelectFormula = "Quejas " + Me.ComboBox1.Text
        'rDocument.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
        Dim Imprimir As New FrmImprimirCentralizada
        Imprimir.rd = rDocument
        Imprimir.ShowDialog()

        rDocument = Nothing
    End Sub
#End Region

#Region "FUNCIONES"
    Private Sub UspMostrarTecnicosReporteComision()
        Try
            BaseII.limpiaParametros()
            cbTecnicos.DataSource = BaseII.ConsultaDT("UspMostrarTecnicosReporteComision")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub MuestraAniosComisiones()
        Try
            BaseII.limpiaParametros()
            CMBAnio.DataSource = BaseII.ConsultaDT("MuestraAniosComisiones")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMesesComisiones()
        Try
            BaseII.limpiaParametros()
            CMBMes.DataSource = BaseII.ConsultaDT("UspMesesComisiones")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function UspReporteComisionTecnico() As DataSet
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVTECNICO", SqlDbType.Int, Me.cbTecnicos.SelectedValue)
            BaseII.CreateMyParameter("@FECHAINICIAL", SqlDbType.DateTime, Me.dtpFechaInicial.Value)
            BaseII.CreateMyParameter("@FECHAFINAL", SqlDbType.DateTime, Me.dtpFechaFinal.Value)

            Dim tableNameList As New List(Of String)
            tableNameList.Add("UspReporteComisionTecnico")
            Return BaseII.ConsultaDS("UspReporteComisionTecnico", tableNameList)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function UspReporteComisionesMes() As DataSet
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVTECNICO", SqlDbType.Int, Me.cbTecnicos.SelectedValue)
            BaseII.CreateMyParameter("@MES", SqlDbType.Int, CInt(Me.CMBMes.SelectedValue))
            BaseII.CreateMyParameter("@ANIO", SqlDbType.Int, CInt(CMBAnio.Text))

            Dim tableNameList As New List(Of String)
            tableNameList.Add("TblComisionesTecnicos")
            tableNameList.Add("TblDetComisionesTecnicos")
            Return BaseII.ConsultaDS("UspReporteComisionesMes", tableNameList)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region

#Region "Salir"
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
#End Region

    
End Class