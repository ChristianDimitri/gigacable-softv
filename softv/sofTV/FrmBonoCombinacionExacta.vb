﻿Public Class FrmBonoCombinacionExacta

#Region "Eventos"
    Private Sub FrmBonoCombinacionExacta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspLlenaNivelUsuario()
        UspLlenaGrupo()
        If opcion = "N" Then
            GloClaveCalculoBono = 0
            UspDesactivaBotones(Me, Me.Name)
        ElseIf opcion = "C" Then
            Me.GroupBox1.Enabled = False
            Me.GroupBox2.Enabled = False
            Me.GroupBox3.Enabled = False
            UspMuestraBonoCombinacionExacta()
            btnGuardar.Enabled = False
        ElseIf opcion = "M" Then
            GroupBox2.Enabled = False
            UspMuestraBonoCombinacionExacta()
            UspDesactivaBotones(Me, Me.Name)
        End If

    End Sub

    Private Sub txtBono_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBono.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        Me.DateTimePicker1.MaxDate = Me.DateTimePicker1.Value
    End Sub
#End Region

#Region "funciones"
    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890.", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function

    Private Sub UspGuardarBonoCombinacionExacta()
        Try
            Dim Vigente As Integer
            Dim DT As New DataTable

            If CheckBox1.Checked = False Then
                Vigente = 0
            Else
                Vigente = 1
            End If

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@OPCION", SqlDbType.VarChar, opcion, 1)
            BaseII.CreateMyParameter("@CLVBONO", SqlDbType.Int, GloClaveCalculoBono)
            BaseII.CreateMyParameter("@BONO", SqlDbType.Int, CInt(Me.txtBono.Text))
            BaseII.CreateMyParameter("@FECHAINICIAL", SqlDbType.DateTime, Me.DateTimePicker1.Value)
            BaseII.CreateMyParameter("@FECHAFINAL", SqlDbType.DateTime, Me.DateTimePicker2.Value)
            BaseII.CreateMyParameter("@VIGENTE", SqlDbType.Int, Vigente)
            BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, Me.cmbGrupoVentas.SelectedValue)
            BaseII.CreateMyParameter("@ESSUPERVISOR", SqlDbType.Int, Me.cmbNivelUsuario.SelectedValue)
            DT = BaseII.ConsultaDT("UspGuardarBonoCombinacionExacta")
            If DT.Rows.Count > 0 Then
                If DT.Rows(0)(0).ToString = "1" Then
                    MsgBox(DT.Rows(0)(1).ToString, MsgBoxStyle.Information, "Bono de combinación Exacta")
                    Exit Sub
                Else
                    MsgBox(DT.Rows(0)(1).ToString, MsgBoxStyle.Information, "Bono de combinación Exacta")
                    If opcion = "N" Then
                        Me.Close()
                    End If
                End If

            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspLlenaNivelUsuario()
        Try
            BaseII.limpiaParametros()
            cmbNivelUsuario.DataSource = BaseII.ConsultaDT("UspLlenaNivelUsuario")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub UspLlenaGrupo()
        Try
            BaseII.limpiaParametros()
            cmbGrupoVentas.DataSource = BaseII.ConsultaDT("UspLlenaGrupo")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMuestraBonoCombinacionExacta()
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVBONO", SqlDbType.Int, GloClaveCalculoBono)
            DT = BaseII.ConsultaDT("UspMuestraBonoCombinacionExacta")
            If DT.Rows.Count > 0 Then
                Me.cmbGrupoVentas.SelectedValue = CInt(DT.Rows(0)(0).ToString)
                Me.cmbNivelUsuario.SelectedValue = CInt(DT.Rows(0)(1).ToString)
                Me.txtBono.Text = DT.Rows(0)(2).ToString
                Me.CheckBox1.Checked = DT.Rows(0)(3).ToString
                Me.DateTimePicker1.Value = CDate(DT.Rows(0)(4).ToString)
                Me.DateTimePicker2.Value = CDate(DT.Rows(0)(5).ToString)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Salir"
    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
#End Region

#Region "Guardar"
    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If cmbNivelUsuario.SelectedValue = 99 Then
            MsgBox("Seleccione el Nivel", MsgBoxStyle.Information, "Upselling")
            Exit Sub
        End If
        If cmbGrupoVentas.SelectedValue = 0 Then
            MsgBox("Seleccione el grupo de ventas", MsgBoxStyle.Information, "Upselling")
            Exit Sub
        End If
        UspGuardarBonoCombinacionExacta()
        Me.cmbGrupoVentas.Enabled = False
        Me.cmbNivelUsuario.Enabled = False
    End Sub
#End Region

   
End Class