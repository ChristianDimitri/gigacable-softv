﻿Public Class NumeroVentas
    Dim Mensaje As String
    Dim OpGuardar As Integer
    Dim ClaveNumVenta As Integer

#Region "Nuevo"
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        TxtClave.Clear()
        TxtDescripcion.Clear()
        OpGuardar = 1
    End Sub
#End Region

#Region "Cancelar"
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        TxtClave.Clear()
        TxtDescripcion.Clear()
    End Sub
#End Region

#Region "Guardar"
    Private Sub TxtDescripcion_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtDescripcion.TextChanged
        If Len(Me.TxtDescripcion.Text) > 0 Then
            btnGuardar.Enabled = True
            btnCancelar.Enabled = True
        Else
            btnGuardar.Enabled = False
            btnCancelar.Enabled = False
        End If
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If Len(TxtClave.Text) > 0 Then
            ClaveNumVenta = CInt(TxtClave.Text)
        Else
            ClaveNumVenta = 0
            OpGuardar = 1
        End If
        UspGuardarNumVentas(Me.TxtDescripcion.Text)
        UspMostrarGridNumVentas()
        TxtDescripcion.Clear()
        TxtClave.Clear()
    End Sub

    Private Sub UspGuardarNumVentas(ByVal prmDescripcion As String)
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@DESCRIPCION", SqlDbType.VarChar, prmDescripcion, 100)
            BaseII.CreateMyParameter("@CLAVE", SqlDbType.Int, ClaveNumVenta)
            BaseII.CreateMyParameter("@OPC", SqlDbType.Int, OpGuardar)
            BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, Me.cmbGrupoVentas.SelectedValue)
            DT = BaseII.ConsultaDT("UspGuardarNumVentas")
            Mensaje = DT.Rows(0)(0).ToString
            MsgBox(Mensaje, MsgBoxStyle.Information, Me.Text)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Modificar"
    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        If Me.dgNumVentas.SelectedRows.Count > 0 Then
            Me.TxtClave.Text = Me.dgNumVentas.SelectedCells(1).Value.ToString
            Me.TxtDescripcion.Text = Me.dgNumVentas.SelectedCells(2).Value.ToString
            OpGuardar = 2
        End If
    End Sub
#End Region

#Region "Mostrar"
    Private Sub UspMostrarGridNumVentas()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, Me.cmbGrupoVentas.SelectedValue)
            dgNumVentas.DataSource = BaseII.ConsultaDT("UspMostrarGridNumVentas")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Eventos"
    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Me.Close()
    End Sub

    Private Sub NumeroVentas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Panel1.BackColor = Me.BackColor
        lblDescripcion.BackColor = Me.BackColor
        lblClave.BackColor = Me.BackColor
        Label5.BackColor = Me.BackColor
        Me.btnCancelar.Enabled = False
        Me.btnGuardar.Enabled = False
        UspLlenaGrupo()

        UspMostrarGridNumVentas()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub cmbGrupoVentas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbGrupoVentas.SelectedIndexChanged
        UspMostrarGridNumVentas()
    End Sub
#End Region

#Region "Metodos"
    Private Sub UspLlenaGrupo()
        Try
            BaseII.limpiaParametros()
            cmbGrupoVentas.DataSource = BaseII.ConsultaDT("UspLlenaGrupo")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabCtrl = New TabControl
                tabCtrl = ctl
                bwrTabControlPerfilDesactiva(tabCtrl, formulario, NomInterno)
                tabCtrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabpg = New TabPage
                tabpg = ctl
                bwrTabPagePerfilDesactiva(tabpg, formulario, NomInterno)
                tabpg = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabCtrl = New TabControl
                tabCtrl = ctl
                bwrTabControlPerfilDesactiva(tabCtrl, formulario, NomInterno)
                tabCtrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabpg = New TabPage
                tabpg = ctl
                bwrTabPagePerfilDesactiva(tabpg, formulario, NomInterno)
                tabpg = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabCtrl = New TabControl
                tabCtrl = ctl
                bwrTabControlPerfilDesactiva(tabCtrl, formulario, NomInterno)
                tabCtrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabpg = New TabPage
                tabpg = ctl
                bwrTabPagePerfilDesactiva(tabpg, formulario, NomInterno)
                tabpg = Nothing
            End If

        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabCtrl = New TabControl
                tabCtrl = ctl
                bwrTabControlPerfilDesactiva(tabCtrl, formulario, NomInterno)
                tabCtrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabpg = New TabPage
                tabpg = ctl
                bwrTabPagePerfilDesactiva(tabpg, formulario, NomInterno)
                tabpg = Nothing
            End If
        Next
    End Sub

    Public Sub bwrTabControlPerfilDesactiva(ByVal tabctrl1 As TabControl, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage

        For Each ctl As Control In tabctrl1.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabctrl = New TabControl
                tabctrl = ctl
                bwrTabControlPerfilDesactiva(tabctrl, formulario, NomInterno)
                tabctrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabpg = New TabPage
                tabpg = ctl
                bwrTabPagePerfilDesactiva(tabpg, formulario, NomInterno)
                tabpg = Nothing
            End If
        Next
    End Sub

    Public Sub bwrTabPagePerfilDesactiva(ByVal tabPG1 As TabPage, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage

        For Each ctl As Control In tabPG1.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabCtrl = New TabControl
                tabCtrl = ctl
                bwrTabControlPerfilDesactiva(tabCtrl, formulario, NomInterno)
                tabCtrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabPG = New TabPage
                tabPG = ctl
                bwrTabPagePerfilDesactiva(tabPG, formulario, NomInterno)
                tabPG = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region

  
End Class