﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmComisionPorNumVentaFechas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.cmbGrupoVentas = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.lblServicioPrincipal = New System.Windows.Forms.Label()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.dgImportesCombos = New System.Windows.Forms.DataGridView()
        Me.CLAVE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLVNUMVENTAS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DESCRIPCION = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IMPORTE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FECHAINICIAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FECHAFINAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VIGENTE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmbServicioPrincipal = New System.Windows.Forms.ComboBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.DTPFechaFinal = New System.Windows.Forms.DateTimePicker()
        Me.DTPFechaInicial = New System.Windows.Forms.DateTimePicker()
        Me.CBVigente = New System.Windows.Forms.CheckBox()
        CType(Me.dgImportesCombos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmbGrupoVentas
        '
        Me.cmbGrupoVentas.DisplayMember = "DESCRIPCION"
        Me.cmbGrupoVentas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbGrupoVentas.FormattingEnabled = True
        Me.cmbGrupoVentas.Location = New System.Drawing.Point(233, 18)
        Me.cmbGrupoVentas.Name = "cmbGrupoVentas"
        Me.cmbGrupoVentas.Size = New System.Drawing.Size(270, 23)
        Me.cmbGrupoVentas.TabIndex = 59
        Me.cmbGrupoVentas.ValueMember = "CLVGRUPO"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(107, 21)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(121, 15)
        Me.Label5.TabIndex = 60
        Me.Label5.Text = "Grupo de Ventas :"
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.Color.Black
        Me.btnGuardar.Location = New System.Drawing.Point(227, 490)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(136, 36)
        Me.btnGuardar.TabIndex = 58
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'lblServicioPrincipal
        '
        Me.lblServicioPrincipal.AutoSize = True
        Me.lblServicioPrincipal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblServicioPrincipal.Location = New System.Drawing.Point(12, 50)
        Me.lblServicioPrincipal.Name = "lblServicioPrincipal"
        Me.lblServicioPrincipal.Size = New System.Drawing.Size(215, 15)
        Me.lblServicioPrincipal.TabIndex = 57
        Me.lblServicioPrincipal.Text = "Seleccione el servicio principal :"
        '
        'btnAgregar
        '
        Me.btnAgregar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar.ForeColor = System.Drawing.Color.Black
        Me.btnAgregar.Location = New System.Drawing.Point(185, 164)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(136, 36)
        Me.btnAgregar.TabIndex = 56
        Me.btnAgregar.Text = "&Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = False
        '
        'dgImportesCombos
        '
        Me.dgImportesCombos.AllowUserToAddRows = False
        Me.dgImportesCombos.AllowUserToDeleteRows = False
        Me.dgImportesCombos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgImportesCombos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgImportesCombos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgImportesCombos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CLAVE, Me.CLVNUMVENTAS, Me.DESCRIPCION, Me.IMPORTE, Me.FECHAINICIAL, Me.FECHAFINAL, Me.VIGENTE})
        Me.dgImportesCombos.Location = New System.Drawing.Point(14, 215)
        Me.dgImportesCombos.Name = "dgImportesCombos"
        Me.dgImportesCombos.Size = New System.Drawing.Size(491, 256)
        Me.dgImportesCombos.TabIndex = 55
        '
        'CLAVE
        '
        Me.CLAVE.DataPropertyName = "CLAVE"
        Me.CLAVE.HeaderText = "CLAVE"
        Me.CLAVE.Name = "CLAVE"
        Me.CLAVE.Visible = False
        '
        'CLVNUMVENTAS
        '
        Me.CLVNUMVENTAS.DataPropertyName = "CLVNUMVENTAS"
        Me.CLVNUMVENTAS.HeaderText = "CLVNUMVENTAS"
        Me.CLVNUMVENTAS.Name = "CLVNUMVENTAS"
        Me.CLVNUMVENTAS.Visible = False
        '
        'DESCRIPCION
        '
        Me.DESCRIPCION.DataPropertyName = "DESCRIPCION"
        Me.DESCRIPCION.HeaderText = "Número de Ventas"
        Me.DESCRIPCION.Name = "DESCRIPCION"
        Me.DESCRIPCION.ReadOnly = True
        '
        'IMPORTE
        '
        Me.IMPORTE.DataPropertyName = "IMPORTE"
        Me.IMPORTE.HeaderText = "Comisión"
        Me.IMPORTE.Name = "IMPORTE"
        '
        'FECHAINICIAL
        '
        Me.FECHAINICIAL.DataPropertyName = "FECHAINICIAL"
        Me.FECHAINICIAL.HeaderText = "FECHAINICIAL"
        Me.FECHAINICIAL.Name = "FECHAINICIAL"
        Me.FECHAINICIAL.Visible = False
        '
        'FECHAFINAL
        '
        Me.FECHAFINAL.DataPropertyName = "FECHAFINAL"
        Me.FECHAFINAL.HeaderText = "FECHAFINAL"
        Me.FECHAFINAL.Name = "FECHAFINAL"
        Me.FECHAFINAL.Visible = False
        '
        'VIGENTE
        '
        Me.VIGENTE.DataPropertyName = "VIGENTE"
        Me.VIGENTE.HeaderText = "VIGENTE"
        Me.VIGENTE.Name = "VIGENTE"
        Me.VIGENTE.Visible = False
        '
        'cmbServicioPrincipal
        '
        Me.cmbServicioPrincipal.DisplayMember = "Descripcion"
        Me.cmbServicioPrincipal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbServicioPrincipal.FormattingEnabled = True
        Me.cmbServicioPrincipal.Location = New System.Drawing.Point(233, 47)
        Me.cmbServicioPrincipal.Name = "cmbServicioPrincipal"
        Me.cmbServicioPrincipal.Size = New System.Drawing.Size(270, 23)
        Me.cmbServicioPrincipal.TabIndex = 54
        Me.cmbServicioPrincipal.ValueMember = "Clv_Servicio"
        '
        'btnCancelar
        '
        Me.btnCancelar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.ForeColor = System.Drawing.Color.Black
        Me.btnCancelar.Location = New System.Drawing.Point(369, 490)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(136, 36)
        Me.btnCancelar.TabIndex = 53
        Me.btnCancelar.Text = "&Salir"
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(106, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(97, 15)
        Me.Label1.TabIndex = 61
        Me.Label1.Text = "Fecha Inicial :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(286, 11)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 15)
        Me.Label2.TabIndex = 62
        Me.Label2.Text = "Fecha Final :"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.DTPFechaFinal)
        Me.Panel1.Controls.Add(Me.DTPFechaInicial)
        Me.Panel1.Controls.Add(Me.CBVigente)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(12, 78)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(493, 80)
        Me.Panel1.TabIndex = 64
        '
        'DTPFechaFinal
        '
        Me.DTPFechaFinal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DTPFechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DTPFechaFinal.Location = New System.Drawing.Point(275, 29)
        Me.DTPFechaFinal.Name = "DTPFechaFinal"
        Me.DTPFechaFinal.Size = New System.Drawing.Size(116, 21)
        Me.DTPFechaFinal.TabIndex = 66
        '
        'DTPFechaInicial
        '
        Me.DTPFechaInicial.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DTPFechaInicial.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DTPFechaInicial.Location = New System.Drawing.Point(97, 28)
        Me.DTPFechaInicial.Name = "DTPFechaInicial"
        Me.DTPFechaInicial.Size = New System.Drawing.Size(111, 21)
        Me.DTPFechaInicial.TabIndex = 65
        '
        'CBVigente
        '
        Me.CBVigente.AutoSize = True
        Me.CBVigente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBVigente.Location = New System.Drawing.Point(203, 56)
        Me.CBVigente.Name = "CBVigente"
        Me.CBVigente.Size = New System.Drawing.Size(74, 19)
        Me.CBVigente.TabIndex = 64
        Me.CBVigente.Text = "Vigente"
        Me.CBVigente.UseVisualStyleBackColor = True
        '
        'FrmComisionPorNumVentaFechas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(517, 534)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.cmbGrupoVentas)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.lblServicioPrincipal)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.dgImportesCombos)
        Me.Controls.Add(Me.cmbServicioPrincipal)
        Me.Controls.Add(Me.btnCancelar)
        Me.Name = "FrmComisionPorNumVentaFechas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Comisión por número de ventas fechas"
        CType(Me.dgImportesCombos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmbGrupoVentas As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents lblServicioPrincipal As System.Windows.Forms.Label
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents dgImportesCombos As System.Windows.Forms.DataGridView
    Friend WithEvents cmbServicioPrincipal As System.Windows.Forms.ComboBox
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents DTPFechaFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents DTPFechaInicial As System.Windows.Forms.DateTimePicker
    Friend WithEvents CBVigente As System.Windows.Forms.CheckBox
    Friend WithEvents CLAVE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLVNUMVENTAS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DESCRIPCION As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IMPORTE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FECHAINICIAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FECHAFINAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VIGENTE As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
