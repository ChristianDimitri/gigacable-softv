<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTelmexDatosClientes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ContratoLabel As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label20 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label18 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim Label19 As System.Windows.Forms.Label
        Dim Label17 As System.Windows.Forms.Label
        Dim Label16 As System.Windows.Forms.Label
        Dim Label15 As System.Windows.Forms.Label
        Dim Label14 As System.Windows.Forms.Label
        Dim Label13 As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label9 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTelmexDatosClientes))
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.CONRELCLIBANCOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONRELCLIBANCOTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONRELCLIBANCOTableAdapter()
        Me.CONRELCLIBANCOBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.CONRELCLIBANCOBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ContratoTextBox = New System.Windows.Forms.TextBox()
        Me.MUESTRATIPOSDECUENTABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRABANCOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRABANCOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRABANCOSTableAdapter()
        Me.MUESTRATIPOSDECUENTATableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRATIPOSDECUENTATableAdapter()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.FechaEjecucion_DTP = New System.Windows.Forms.TextBox()
        Me.PlanPagoTxt = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.ExtensionesNUD = New System.Windows.Forms.NumericUpDown()
        Me.TelefonoTelmex_Mtxt = New System.Windows.Forms.MaskedTextBox()
        Me.HoraTxt = New System.Windows.Forms.MaskedTextBox()
        Me.FechaYhora_DTP = New System.Windows.Forms.DateTimePicker()
        Me.SerieModemTxt = New System.Windows.Forms.TextBox()
        Me.ModeloModemCmb = New System.Windows.Forms.ComboBox()
        Me.MarcaModemCmb = New System.Windows.Forms.ComboBox()
        Me.TipoEntregaModemCmb = New System.Windows.Forms.ComboBox()
        Me.ProveedorPlataformaTxt = New System.Windows.Forms.TextBox()
        Me.ObservacionesTxt = New System.Windows.Forms.TextBox()
        Me.TipoClienteCmb = New System.Windows.Forms.ComboBox()
        Me.TipoMovimientoTxt = New System.Windows.Forms.TextBox()
        Me.NisTxt = New System.Windows.Forms.TextBox()
        Me.FolioTxt = New System.Windows.Forms.TextBox()
        Me.NumOrdenTxt = New System.Windows.Forms.TextBox()
        Me.TipoTecnologiaCmb = New System.Windows.Forms.ComboBox()
        Me.FechaSolicitud_DTP = New System.Windows.Forms.DateTimePicker()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Button5 = New System.Windows.Forms.Button()
        ContratoLabel = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Label20 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label18 = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        Label19 = New System.Windows.Forms.Label()
        Label17 = New System.Windows.Forms.Label()
        Label16 = New System.Windows.Forms.Label()
        Label15 = New System.Windows.Forms.Label()
        Label14 = New System.Windows.Forms.Label()
        Label13 = New System.Windows.Forms.Label()
        Label11 = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Label9 = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONRELCLIBANCOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONRELCLIBANCOBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONRELCLIBANCOBindingNavigator.SuspendLayout()
        CType(Me.MUESTRATIPOSDECUENTABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRABANCOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.ExtensionesNUD, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ContratoLabel
        '
        ContratoLabel.AutoSize = True
        ContratoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContratoLabel.Location = New System.Drawing.Point(132, 3)
        ContratoLabel.Name = "ContratoLabel"
        ContratoLabel.Size = New System.Drawing.Size(65, 15)
        ContratoLabel.TabIndex = 2
        ContratoLabel.Text = "Contrato:"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label7.ForeColor = System.Drawing.Color.LightSlateGray
        Label7.Location = New System.Drawing.Point(18, 329)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(140, 15)
        Label7.TabIndex = 434
        Label7.Text = "Fecha y hora de cita:"
        '
        'Label20
        '
        Label20.AutoSize = True
        Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label20.ForeColor = System.Drawing.Color.LightSlateGray
        Label20.Location = New System.Drawing.Point(532, 331)
        Label20.Name = "Label20"
        Label20.Size = New System.Drawing.Size(39, 15)
        Label20.TabIndex = 453
        Label20.Text = "SKU:"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Label5.Location = New System.Drawing.Point(18, 237)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(181, 15)
        Label5.TabIndex = 427
        Label5.Text = "Teléfono asignado Telmex:"
        '
        'Label18
        '
        Label18.AutoSize = True
        Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label18.ForeColor = System.Drawing.Color.LightSlateGray
        Label18.Location = New System.Drawing.Point(527, 241)
        Label18.Name = "Label18"
        Label18.Size = New System.Drawing.Size(117, 15)
        Label18.TabIndex = 452
        Label18.Text = "Serie de modem:"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Label6.Location = New System.Drawing.Point(18, 283)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(167, 15)
        Label6.TabIndex = 429
        Label6.Text = "Extensiones Telefónicas:"
        '
        'Label19
        '
        Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label19.ForeColor = System.Drawing.Color.LightSlateGray
        Label19.Location = New System.Drawing.Point(527, 288)
        Label19.Name = "Label19"
        Label19.Size = New System.Drawing.Size(163, 16)
        Label19.TabIndex = 450
        Label19.Text = "Fecha de ejecución:"
        '
        'Label17
        '
        Label17.AutoSize = True
        Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label17.ForeColor = System.Drawing.Color.LightSlateGray
        Label17.Location = New System.Drawing.Point(527, 196)
        Label17.Name = "Label17"
        Label17.Size = New System.Drawing.Size(131, 15)
        Label17.TabIndex = 449
        Label17.Text = "Modelo de modem:"
        '
        'Label16
        '
        Label16.AutoSize = True
        Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label16.ForeColor = System.Drawing.Color.LightSlateGray
        Label16.Location = New System.Drawing.Point(527, 146)
        Label16.Name = "Label16"
        Label16.Size = New System.Drawing.Size(123, 15)
        Label16.TabIndex = 447
        Label16.Text = "Marca de modem:"
        '
        'Label15
        '
        Label15.AutoSize = True
        Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label15.ForeColor = System.Drawing.Color.LightSlateGray
        Label15.Location = New System.Drawing.Point(527, 104)
        Label15.Name = "Label15"
        Label15.Size = New System.Drawing.Size(184, 15)
        Label15.TabIndex = 445
        Label15.Text = "Tipo de entrega de modem:"
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Label14.Location = New System.Drawing.Point(528, 9)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(130, 15)
        Label14.TabIndex = 443
        Label14.Text = "Tipo de tecnología:"
        Label14.Visible = False
        '
        'Label13
        '
        Label13.AutoSize = True
        Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label13.ForeColor = System.Drawing.Color.LightSlateGray
        Label13.Location = New System.Drawing.Point(532, 58)
        Label13.Name = "Label13"
        Label13.Size = New System.Drawing.Size(169, 15)
        Label13.TabIndex = 441
        Label13.Text = "Proveedor de plataforma:"
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label11.ForeColor = System.Drawing.Color.LightSlateGray
        Label11.Location = New System.Drawing.Point(277, 193)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(105, 15)
        Label11.TabIndex = 438
        Label11.Text = "Observaciones:"
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Label10.Location = New System.Drawing.Point(277, 143)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(96, 15)
        Label10.TabIndex = 437
        Label10.Text = "Plan de pago:"
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Label9.Location = New System.Drawing.Point(277, 99)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(86, 15)
        Label9.TabIndex = 436
        Label9.Text = "Tipo cliente:"
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Label8.Location = New System.Drawing.Point(277, 49)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(137, 15)
        Label8.TabIndex = 435
        Label8.Text = "Tipo de movimiento:"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Label4.Location = New System.Drawing.Point(18, 186)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(34, 15)
        Label4.TabIndex = 431
        Label4.Text = "NIS:"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Label3.Location = New System.Drawing.Point(18, 143)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(43, 15)
        Label3.TabIndex = 428
        Label3.Text = "Folio:"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(23, 100)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(196, 15)
        Label2.TabIndex = 421
        Label2.Text = "Número de Orden de Telmex:"
        '
        'Label1
        '
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(3, 46)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(216, 16)
        Label1.TabIndex = 423
        Label1.Text = "Fecha de solicitud Telmex: "
        Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CONRELCLIBANCOBindingSource
        '
        Me.CONRELCLIBANCOBindingSource.DataMember = "CONRELCLIBANCO"
        Me.CONRELCLIBANCOBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONRELCLIBANCOTableAdapter
        '
        Me.CONRELCLIBANCOTableAdapter.ClearBeforeFill = True
        '
        'CONRELCLIBANCOBindingNavigator
        '
        Me.CONRELCLIBANCOBindingNavigator.AddNewItem = Nothing
        Me.CONRELCLIBANCOBindingNavigator.BindingSource = Me.CONRELCLIBANCOBindingSource
        Me.CONRELCLIBANCOBindingNavigator.CountItem = Nothing
        Me.CONRELCLIBANCOBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONRELCLIBANCOBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.CONRELCLIBANCOBindingNavigatorSaveItem})
        Me.CONRELCLIBANCOBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONRELCLIBANCOBindingNavigator.MoveFirstItem = Nothing
        Me.CONRELCLIBANCOBindingNavigator.MoveLastItem = Nothing
        Me.CONRELCLIBANCOBindingNavigator.MoveNextItem = Nothing
        Me.CONRELCLIBANCOBindingNavigator.MovePreviousItem = Nothing
        Me.CONRELCLIBANCOBindingNavigator.Name = "CONRELCLIBANCOBindingNavigator"
        Me.CONRELCLIBANCOBindingNavigator.PositionItem = Nothing
        Me.CONRELCLIBANCOBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONRELCLIBANCOBindingNavigator.Size = New System.Drawing.Size(811, 25)
        Me.CONRELCLIBANCOBindingNavigator.TabIndex = 6
        Me.CONRELCLIBANCOBindingNavigator.TabStop = True
        Me.CONRELCLIBANCOBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(77, 22)
        Me.BindingNavigatorDeleteItem.Text = "&Eliminar"
        '
        'CONRELCLIBANCOBindingNavigatorSaveItem
        '
        Me.CONRELCLIBANCOBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONRELCLIBANCOBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONRELCLIBANCOBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONRELCLIBANCOBindingNavigatorSaveItem.Name = "CONRELCLIBANCOBindingNavigatorSaveItem"
        Me.CONRELCLIBANCOBindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 22)
        Me.CONRELCLIBANCOBindingNavigatorSaveItem.Text = "&Guardar datos"
        '
        'ContratoTextBox
        '
        Me.ContratoTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ContratoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONRELCLIBANCOBindingSource, "Contrato", True))
        Me.ContratoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratoTextBox.Location = New System.Drawing.Point(203, 0)
        Me.ContratoTextBox.Name = "ContratoTextBox"
        Me.ContratoTextBox.Size = New System.Drawing.Size(100, 21)
        Me.ContratoTextBox.TabIndex = 3
        Me.ContratoTextBox.TabStop = False
        '
        'MUESTRATIPOSDECUENTABindingSource
        '
        Me.MUESTRATIPOSDECUENTABindingSource.DataMember = "MUESTRATIPOSDECUENTA"
        Me.MUESTRATIPOSDECUENTABindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRABANCOSBindingSource
        '
        Me.MUESTRABANCOSBindingSource.DataMember = "MUESTRABANCOS"
        Me.MUESTRABANCOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRABANCOSTableAdapter
        '
        Me.MUESTRABANCOSTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATIPOSDECUENTATableAdapter
        '
        Me.MUESTRATIPOSDECUENTATableAdapter.ClearBeforeFill = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.DateTimePicker1)
        Me.Panel1.Controls.Add(Me.FechaEjecucion_DTP)
        Me.Panel1.Controls.Add(Me.PlanPagoTxt)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(Me.ExtensionesNUD)
        Me.Panel1.Controls.Add(Me.TelefonoTelmex_Mtxt)
        Me.Panel1.Controls.Add(Me.HoraTxt)
        Me.Panel1.Controls.Add(Label7)
        Me.Panel1.Controls.Add(Label20)
        Me.Panel1.Controls.Add(Me.FechaYhora_DTP)
        Me.Panel1.Controls.Add(Label5)
        Me.Panel1.Controls.Add(Label18)
        Me.Panel1.Controls.Add(Label6)
        Me.Panel1.Controls.Add(Me.SerieModemTxt)
        Me.Panel1.Controls.Add(Label19)
        Me.Panel1.Controls.Add(Label17)
        Me.Panel1.Controls.Add(Me.ModeloModemCmb)
        Me.Panel1.Controls.Add(Label16)
        Me.Panel1.Controls.Add(Me.MarcaModemCmb)
        Me.Panel1.Controls.Add(Label15)
        Me.Panel1.Controls.Add(Me.TipoEntregaModemCmb)
        Me.Panel1.Controls.Add(Label14)
        Me.Panel1.Controls.Add(Label13)
        Me.Panel1.Controls.Add(Me.ProveedorPlataformaTxt)
        Me.Panel1.Controls.Add(Me.ObservacionesTxt)
        Me.Panel1.Controls.Add(Me.TipoClienteCmb)
        Me.Panel1.Controls.Add(Label11)
        Me.Panel1.Controls.Add(Label10)
        Me.Panel1.Controls.Add(Label9)
        Me.Panel1.Controls.Add(Label8)
        Me.Panel1.Controls.Add(Me.TipoMovimientoTxt)
        Me.Panel1.Controls.Add(Label4)
        Me.Panel1.Controls.Add(Me.NisTxt)
        Me.Panel1.Controls.Add(Label3)
        Me.Panel1.Controls.Add(Me.FolioTxt)
        Me.Panel1.Controls.Add(Label2)
        Me.Panel1.Controls.Add(Me.NumOrdenTxt)
        Me.Panel1.Controls.Add(Me.TipoTecnologiaCmb)
        Me.Panel1.Controls.Add(Me.FechaSolicitud_DTP)
        Me.Panel1.Controls.Add(Label1)
        Me.Panel1.Location = New System.Drawing.Point(12, 37)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(782, 386)
        Me.Panel1.TabIndex = 0
        Me.Panel1.TabStop = True
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(607, 323)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(224, 21)
        Me.DateTimePicker1.TabIndex = 460
        Me.DateTimePicker1.Value = New Date(1900, 1, 1, 12, 52, 0, 0)
        Me.DateTimePicker1.Visible = False
        '
        'FechaEjecucion_DTP
        '
        Me.FechaEjecucion_DTP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FechaEjecucion_DTP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.FechaEjecucion_DTP.Enabled = False
        Me.FechaEjecucion_DTP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaEjecucion_DTP.Location = New System.Drawing.Point(530, 307)
        Me.FechaEjecucion_DTP.MaxLength = 50
        Me.FechaEjecucion_DTP.Name = "FechaEjecucion_DTP"
        Me.FechaEjecucion_DTP.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FechaEjecucion_DTP.Size = New System.Drawing.Size(224, 22)
        Me.FechaEjecucion_DTP.TabIndex = 459
        '
        'PlanPagoTxt
        '
        Me.PlanPagoTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PlanPagoTxt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.PlanPagoTxt.Enabled = False
        Me.PlanPagoTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PlanPagoTxt.Location = New System.Drawing.Point(280, 163)
        Me.PlanPagoTxt.MaxLength = 50
        Me.PlanPagoTxt.Name = "PlanPagoTxt"
        Me.PlanPagoTxt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PlanPagoTxt.Size = New System.Drawing.Size(224, 22)
        Me.PlanPagoTxt.TabIndex = 458
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(530, 350)
        Me.TextBox1.MaxLength = 50
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TextBox1.Size = New System.Drawing.Size(224, 22)
        Me.TextBox1.TabIndex = 457
        '
        'ExtensionesNUD
        '
        Me.ExtensionesNUD.Location = New System.Drawing.Point(21, 306)
        Me.ExtensionesNUD.Name = "ExtensionesNUD"
        Me.ExtensionesNUD.Size = New System.Drawing.Size(224, 20)
        Me.ExtensionesNUD.TabIndex = 456
        '
        'TelefonoTelmex_Mtxt
        '
        Me.TelefonoTelmex_Mtxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TelefonoTelmex_Mtxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TelefonoTelmex_Mtxt.Location = New System.Drawing.Point(21, 255)
        Me.TelefonoTelmex_Mtxt.Mask = "(999)000-0000"
        Me.TelefonoTelmex_Mtxt.Name = "TelefonoTelmex_Mtxt"
        Me.TelefonoTelmex_Mtxt.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.TelefonoTelmex_Mtxt.Size = New System.Drawing.Size(224, 21)
        Me.TelefonoTelmex_Mtxt.TabIndex = 455
        '
        'HoraTxt
        '
        Me.HoraTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.HoraTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HoraTxt.Location = New System.Drawing.Point(164, 347)
        Me.HoraTxt.Mask = "00:00"
        Me.HoraTxt.Name = "HoraTxt"
        Me.HoraTxt.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.HoraTxt.Size = New System.Drawing.Size(81, 21)
        Me.HoraTxt.TabIndex = 454
        Me.HoraTxt.ValidatingType = GetType(Date)
        '
        'FechaYhora_DTP
        '
        Me.FechaYhora_DTP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaYhora_DTP.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FechaYhora_DTP.Location = New System.Drawing.Point(21, 347)
        Me.FechaYhora_DTP.Name = "FechaYhora_DTP"
        Me.FechaYhora_DTP.Size = New System.Drawing.Size(137, 21)
        Me.FechaYhora_DTP.TabIndex = 432
        Me.FechaYhora_DTP.Value = New Date(1900, 1, 1, 12, 52, 0, 0)
        '
        'SerieModemTxt
        '
        Me.SerieModemTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SerieModemTxt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.SerieModemTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SerieModemTxt.Location = New System.Drawing.Point(530, 259)
        Me.SerieModemTxt.MaxLength = 50
        Me.SerieModemTxt.Name = "SerieModemTxt"
        Me.SerieModemTxt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SerieModemTxt.Size = New System.Drawing.Size(224, 22)
        Me.SerieModemTxt.TabIndex = 451
        '
        'ModeloModemCmb
        '
        Me.ModeloModemCmb.DisplayMember = "Modelo"
        Me.ModeloModemCmb.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ModeloModemCmb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ModeloModemCmb.ForeColor = System.Drawing.Color.Black
        Me.ModeloModemCmb.FormattingEnabled = True
        Me.ModeloModemCmb.Location = New System.Drawing.Point(530, 214)
        Me.ModeloModemCmb.Name = "ModeloModemCmb"
        Me.ModeloModemCmb.Size = New System.Drawing.Size(224, 21)
        Me.ModeloModemCmb.TabIndex = 448
        Me.ModeloModemCmb.ValueMember = "IdModelo"
        '
        'MarcaModemCmb
        '
        Me.MarcaModemCmb.DisplayMember = "Marca"
        Me.MarcaModemCmb.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MarcaModemCmb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MarcaModemCmb.ForeColor = System.Drawing.Color.Black
        Me.MarcaModemCmb.FormattingEnabled = True
        Me.MarcaModemCmb.Location = New System.Drawing.Point(530, 164)
        Me.MarcaModemCmb.Name = "MarcaModemCmb"
        Me.MarcaModemCmb.Size = New System.Drawing.Size(224, 21)
        Me.MarcaModemCmb.TabIndex = 446
        Me.MarcaModemCmb.ValueMember = "IdMarca"
        '
        'TipoEntregaModemCmb
        '
        Me.TipoEntregaModemCmb.DisplayMember = "TipoEntrega"
        Me.TipoEntregaModemCmb.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.TipoEntregaModemCmb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TipoEntregaModemCmb.ForeColor = System.Drawing.Color.Black
        Me.TipoEntregaModemCmb.FormattingEnabled = True
        Me.TipoEntregaModemCmb.Location = New System.Drawing.Point(530, 121)
        Me.TipoEntregaModemCmb.Name = "TipoEntregaModemCmb"
        Me.TipoEntregaModemCmb.Size = New System.Drawing.Size(224, 21)
        Me.TipoEntregaModemCmb.TabIndex = 444
        Me.TipoEntregaModemCmb.ValueMember = "IdTipoEntrega"
        '
        'ProveedorPlataformaTxt
        '
        Me.ProveedorPlataformaTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ProveedorPlataformaTxt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ProveedorPlataformaTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ProveedorPlataformaTxt.Location = New System.Drawing.Point(530, 76)
        Me.ProveedorPlataformaTxt.Name = "ProveedorPlataformaTxt"
        Me.ProveedorPlataformaTxt.Size = New System.Drawing.Size(224, 21)
        Me.ProveedorPlataformaTxt.TabIndex = 442
        '
        'ObservacionesTxt
        '
        Me.ObservacionesTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ObservacionesTxt.Location = New System.Drawing.Point(280, 211)
        Me.ObservacionesTxt.MaxLength = 250
        Me.ObservacionesTxt.Multiline = True
        Me.ObservacionesTxt.Name = "ObservacionesTxt"
        Me.ObservacionesTxt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.ObservacionesTxt.Size = New System.Drawing.Size(224, 157)
        Me.ObservacionesTxt.TabIndex = 440
        '
        'TipoClienteCmb
        '
        Me.TipoClienteCmb.DisplayMember = "DESCRIPCION"
        Me.TipoClienteCmb.Enabled = False
        Me.TipoClienteCmb.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.TipoClienteCmb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TipoClienteCmb.ForeColor = System.Drawing.Color.Red
        Me.TipoClienteCmb.FormattingEnabled = True
        Me.TipoClienteCmb.Location = New System.Drawing.Point(280, 118)
        Me.TipoClienteCmb.Name = "TipoClienteCmb"
        Me.TipoClienteCmb.Size = New System.Drawing.Size(224, 21)
        Me.TipoClienteCmb.TabIndex = 439
        Me.TipoClienteCmb.ValueMember = "CLV_TIPOCLIENTE"
        '
        'TipoMovimientoTxt
        '
        Me.TipoMovimientoTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TipoMovimientoTxt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TipoMovimientoTxt.Enabled = False
        Me.TipoMovimientoTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TipoMovimientoTxt.Location = New System.Drawing.Point(280, 67)
        Me.TipoMovimientoTxt.MaxLength = 50
        Me.TipoMovimientoTxt.Name = "TipoMovimientoTxt"
        Me.TipoMovimientoTxt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TipoMovimientoTxt.Size = New System.Drawing.Size(224, 22)
        Me.TipoMovimientoTxt.TabIndex = 433
        '
        'NisTxt
        '
        Me.NisTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NisTxt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.NisTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NisTxt.Location = New System.Drawing.Point(21, 204)
        Me.NisTxt.MaxLength = 50
        Me.NisTxt.Name = "NisTxt"
        Me.NisTxt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.NisTxt.Size = New System.Drawing.Size(224, 22)
        Me.NisTxt.TabIndex = 430
        '
        'FolioTxt
        '
        Me.FolioTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FolioTxt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.FolioTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FolioTxt.Location = New System.Drawing.Point(21, 161)
        Me.FolioTxt.MaxLength = 50
        Me.FolioTxt.Name = "FolioTxt"
        Me.FolioTxt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FolioTxt.Size = New System.Drawing.Size(224, 22)
        Me.FolioTxt.TabIndex = 426
        '
        'NumOrdenTxt
        '
        Me.NumOrdenTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NumOrdenTxt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.NumOrdenTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumOrdenTxt.Location = New System.Drawing.Point(21, 118)
        Me.NumOrdenTxt.Name = "NumOrdenTxt"
        Me.NumOrdenTxt.Size = New System.Drawing.Size(224, 21)
        Me.NumOrdenTxt.TabIndex = 422
        '
        'TipoTecnologiaCmb
        '
        Me.TipoTecnologiaCmb.DisplayMember = "DESCRIPCION"
        Me.TipoTecnologiaCmb.Enabled = False
        Me.TipoTecnologiaCmb.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.TipoTecnologiaCmb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TipoTecnologiaCmb.ForeColor = System.Drawing.Color.Black
        Me.TipoTecnologiaCmb.FormattingEnabled = True
        Me.TipoTecnologiaCmb.Location = New System.Drawing.Point(531, 31)
        Me.TipoTecnologiaCmb.Name = "TipoTecnologiaCmb"
        Me.TipoTecnologiaCmb.Size = New System.Drawing.Size(224, 21)
        Me.TipoTecnologiaCmb.TabIndex = 425
        Me.TipoTecnologiaCmb.ValueMember = "IdMedio"
        Me.TipoTecnologiaCmb.Visible = False
        '
        'FechaSolicitud_DTP
        '
        Me.FechaSolicitud_DTP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaSolicitud_DTP.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FechaSolicitud_DTP.Location = New System.Drawing.Point(21, 67)
        Me.FechaSolicitud_DTP.Name = "FechaSolicitud_DTP"
        Me.FechaSolicitud_DTP.Size = New System.Drawing.Size(224, 21)
        Me.FechaSolicitud_DTP.TabIndex = 424
        Me.FechaSolicitud_DTP.Value = New Date(1900, 1, 1, 12, 52, 0, 0)
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(647, 444)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(147, 36)
        Me.Button5.TabIndex = 27
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'FrmTelmexDatosClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(811, 501)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.CONRELCLIBANCOBindingNavigator)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(ContratoLabel)
        Me.Controls.Add(Me.ContratoTextBox)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmTelmexDatosClientes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Datos Telmex del Cliente"
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONRELCLIBANCOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONRELCLIBANCOBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONRELCLIBANCOBindingNavigator.ResumeLayout(False)
        Me.CONRELCLIBANCOBindingNavigator.PerformLayout()
        CType(Me.MUESTRATIPOSDECUENTABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRABANCOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.ExtensionesNUD, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONRELCLIBANCOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONRELCLIBANCOTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONRELCLIBANCOTableAdapter
    Friend WithEvents CONRELCLIBANCOBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONRELCLIBANCOBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ContratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MUESTRABANCOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRABANCOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRABANCOSTableAdapter
    Friend WithEvents MUESTRATIPOSDECUENTABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRATIPOSDECUENTATableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRATIPOSDECUENTATableAdapter
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents FechaEjecucion_DTP As System.Windows.Forms.TextBox
    Friend WithEvents PlanPagoTxt As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ExtensionesNUD As System.Windows.Forms.NumericUpDown
    Friend WithEvents TelefonoTelmex_Mtxt As System.Windows.Forms.MaskedTextBox
    Friend WithEvents HoraTxt As System.Windows.Forms.MaskedTextBox
    Friend WithEvents FechaYhora_DTP As System.Windows.Forms.DateTimePicker
    Friend WithEvents SerieModemTxt As System.Windows.Forms.TextBox
    Friend WithEvents ModeloModemCmb As System.Windows.Forms.ComboBox
    Friend WithEvents MarcaModemCmb As System.Windows.Forms.ComboBox
    Friend WithEvents TipoEntregaModemCmb As System.Windows.Forms.ComboBox
    Friend WithEvents ProveedorPlataformaTxt As System.Windows.Forms.TextBox
    Friend WithEvents ObservacionesTxt As System.Windows.Forms.TextBox
    Friend WithEvents TipoClienteCmb As System.Windows.Forms.ComboBox
    Friend WithEvents TipoMovimientoTxt As System.Windows.Forms.TextBox
    Friend WithEvents NisTxt As System.Windows.Forms.TextBox
    Friend WithEvents FolioTxt As System.Windows.Forms.TextBox
    Friend WithEvents NumOrdenTxt As System.Windows.Forms.TextBox
    Friend WithEvents TipoTecnologiaCmb As System.Windows.Forms.ComboBox
    Friend WithEvents FechaSolicitud_DTP As System.Windows.Forms.DateTimePicker
    Friend WithEvents Button5 As System.Windows.Forms.Button
End Class
