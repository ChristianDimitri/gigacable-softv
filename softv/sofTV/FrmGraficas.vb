
Imports System.Data.SqlClient
Public Class FrmGraficas

    Private Sub FrmGraficas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If eBndVen = True Then
            eBndVen = False
            LlenaVendedor()
            LlenaSucursal()
            LLenaServicios()
        End If
    End Sub

    Private Sub FrmGraficas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Dame_clv_session_ReportesTableAdapter.Connection = CON
        Me.Dame_clv_session_ReportesTableAdapter.Fill(Me.DataSetEric2.Dame_clv_session_Reportes, eClv_Session)
        Me.MuestraTipServEricTableAdapter.Connection = CON
        Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric2.MuestraTipServEric, 0, 2)
        Me.DateTimePicker1.Value = Today
        Me.DateTimePicker2.Value = Today
        eBndVen = False
        LlenaVendedor()
        LlenaSucursal()
        LLenaServicios()
        Me.CheckBox1.Checked = True
        CON.Close()
    End Sub

    Private Sub LlenaVendedor()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BorrarVendedorTmpTableAdapter.Connection = CON
        Me.BorrarVendedorTmpTableAdapter.Fill(Me.DataSetEric2.BorrarVendedorTmp, 0, eClv_Session, 2)
        Me.ConVentasVendedoresProTableAdapter.Connection = CON
        Me.ConVentasVendedoresProTableAdapter.Fill(Me.DataSetEric2.ConVentasVendedoresPro, eClv_Session, 0)
        Me.ConVentasVendedoresTmpTableAdapter.Connection = CON
        Me.ConVentasVendedoresTmpTableAdapter.Fill(Me.DataSetEric2.ConVentasVendedoresTmp, eClv_Session)
        CON.Close()
    End Sub

    Private Sub LLenaServicios()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BorrarServiciosTmpTableAdapter.Connection = CON
        Me.BorrarServiciosTmpTableAdapter.Fill(Me.DataSetEric2.BorrarServiciosTmp, 0, eClv_Session, 2)
        Me.ConServiciosProTableAdapter.Connection = CON
        Me.ConServiciosProTableAdapter.Fill(Me.DataSetEric2.ConServiciosPro, CInt(Me.ConceptoComboBox.SelectedValue), eClv_Session, 0)
        Me.ConServiciosTmpTableAdapter.Connection = CON
        Me.ConServiciosTmpTableAdapter.Fill(Me.DataSetEric2.ConServiciosTmp, eClv_Session)
        CON.Close()
    End Sub

    Private Sub LlenaSucursal()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BorrarSucursalesTmpTableAdapter.Connection = CON
        Me.BorrarSucursalesTmpTableAdapter.Fill(Me.DataSetEric2.BorrarSucursalesTmp, 0, eClv_Session, 2)
        Me.ConSucursalesProTableAdapter.Connection = CON
        Me.ConSucursalesProTableAdapter.Fill(Me.DataSetEric2.ConSucursalesPro, eClv_Session, 0)
        Me.ConSucursalesTmpTableAdapter.Connection = CON
        Me.ConSucursalesTmpTableAdapter.Fill(Me.DataSetEric2.ConSucursalesTmp, eClv_Session)
        CON.Close()
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        Check()
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        Check()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.NombreListBox.Items.Count > 0 Then
            CON.Open()
            Me.InsertarVendedorTmpTableAdapter.Connection = CON
            Me.InsertarVendedorTmpTableAdapter.Fill(Me.DataSetEric2.InsertarVendedorTmp, CLng(Me.NombreListBox.SelectedValue), eClv_Session, 0)
            Me.ConVentasVendedoresTmpTableAdapter.Connection = CON
            Me.ConVentasVendedoresTmpTableAdapter.Fill(Me.DataSetEric2.ConVentasVendedoresTmp, eClv_Session)
            Me.ConVentasVendedoresProTableAdapter.Connection = CON
            Me.ConVentasVendedoresProTableAdapter.Fill(Me.DataSetEric2.ConVentasVendedoresPro, eClv_Session, 1)
            CON.Close()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.NombreListBox.Items.Count > 0 Then
            CON.Open()
            Me.InsertarVendedorTmpTableAdapter.Connection = CON
            Me.InsertarVendedorTmpTableAdapter.Fill(Me.DataSetEric2.InsertarVendedorTmp, 0, eClv_Session, 1)
            Me.ConVentasVendedoresTmpTableAdapter.Connection = CON
            Me.ConVentasVendedoresTmpTableAdapter.Fill(Me.DataSetEric2.ConVentasVendedoresTmp, eClv_Session)
            Me.ConVentasVendedoresProTableAdapter.Connection = CON
            Me.ConVentasVendedoresProTableAdapter.Fill(Me.DataSetEric2.ConVentasVendedoresPro, eClv_Session, 1)
            CON.Close()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.NombreListBox1.Items.Count > 0 Then
            CON.Open()
            Me.BorrarVendedorTmpTableAdapter.Connection = CON
            Me.BorrarVendedorTmpTableAdapter.Fill(Me.DataSetEric2.BorrarVendedorTmp, CLng(Me.NombreListBox1.SelectedValue), eClv_Session, 0)
            Me.ConVentasVendedoresTmpTableAdapter.Connection = CON
            Me.ConVentasVendedoresTmpTableAdapter.Fill(Me.DataSetEric2.ConVentasVendedoresTmp, eClv_Session)
            Me.ConVentasVendedoresProTableAdapter.Connection = CON
            Me.ConVentasVendedoresProTableAdapter.Fill(Me.DataSetEric2.ConVentasVendedoresPro, eClv_Session, 1)
            CON.Close()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.NombreListBox1.Items.Count > 0 Then
            CON.Open()
            Me.BorrarVendedorTmpTableAdapter.Connection = CON
            Me.BorrarVendedorTmpTableAdapter.Fill(Me.DataSetEric2.BorrarVendedorTmp, 0, eClv_Session, 1)
            Me.ConVentasVendedoresTmpTableAdapter.Connection = CON
            Me.ConVentasVendedoresTmpTableAdapter.Fill(Me.DataSetEric2.ConVentasVendedoresTmp, eClv_Session)
            Me.ConVentasVendedoresProTableAdapter.Connection = CON
            Me.ConVentasVendedoresProTableAdapter.Fill(Me.DataSetEric2.ConVentasVendedoresPro, eClv_Session, 1)
            CON.Close()
        End If
    End Sub
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.NombreListBox2.Items.Count > 0 Then
            CON.Open()
            Me.InsertarSucursalesTmpTableAdapter.Connection = CON
            Me.InsertarSucursalesTmpTableAdapter.Fill(Me.DataSetEric2.InsertarSucursalesTmp, CInt(Me.NombreListBox2.SelectedValue), eClv_Session, 0)
            Me.ConSucursalesTmpTableAdapter.Connection = CON
            Me.ConSucursalesTmpTableAdapter.Fill(Me.DataSetEric2.ConSucursalesTmp, eClv_Session)
            Me.ConSucursalesProTableAdapter.Connection = CON
            Me.ConSucursalesProTableAdapter.Fill(Me.DataSetEric2.ConSucursalesPro, eClv_Session, 1)
            CON.Close()
        End If

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.NombreListBox2.Items.Count > 0 Then
            CON.Open()
            Me.InsertarSucursalesTmpTableAdapter.Connection = CON
            Me.InsertarSucursalesTmpTableAdapter.Fill(Me.DataSetEric2.InsertarSucursalesTmp, 0, eClv_Session, 1)
            Me.ConSucursalesTmpTableAdapter.Connection = CON
            Me.ConSucursalesTmpTableAdapter.Fill(Me.DataSetEric2.ConSucursalesTmp, eClv_Session)
            Me.ConSucursalesProTableAdapter.Connection = CON
            Me.ConSucursalesProTableAdapter.Fill(Me.DataSetEric2.ConSucursalesPro, eClv_Session, 1)
            CON.Close()
        End If
    End Sub


    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.NombreListBox3.Items.Count > 0 Then
            CON.Open()
            Me.BorrarSucursalesTmpTableAdapter.Connection = CON
            Me.BorrarSucursalesTmpTableAdapter.Fill(Me.DataSetEric2.BorrarSucursalesTmp, CInt(Me.NombreListBox3.SelectedValue), eClv_Session, 0)
            Me.ConSucursalesTmpTableAdapter.Connection = CON
            Me.ConSucursalesTmpTableAdapter.Fill(Me.DataSetEric2.ConSucursalesTmp, eClv_Session)
            Me.ConSucursalesProTableAdapter.Connection = CON
            Me.ConSucursalesProTableAdapter.Fill(Me.DataSetEric2.ConSucursalesPro, eClv_Session, 1)
            CON.Close()
        End If
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.NombreListBox3.Items.Count > 0 Then
            CON.Open()
            Me.BorrarSucursalesTmpTableAdapter.Connection = CON
            Me.BorrarSucursalesTmpTableAdapter.Fill(Me.DataSetEric2.BorrarSucursalesTmp, 0, eClv_Session, 1)
            Me.ConSucursalesTmpTableAdapter.Connection = CON
            Me.ConSucursalesTmpTableAdapter.Fill(Me.DataSetEric2.ConSucursalesTmp, eClv_Session)
            Me.ConSucursalesProTableAdapter.Connection = CON
            Me.ConSucursalesProTableAdapter.Fill(Me.DataSetEric2.ConSucursalesPro, eClv_Session, 1)
            CON.Close()
        End If
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.DescripcionListBox.Items.Count > 0 Then
            CON.Open()
            Me.InsertarServiciosTmpTableAdapter.Connection = CON
            Me.InsertarServiciosTmpTableAdapter.Fill(Me.DataSetEric2.InsertarServiciosTmp, CInt(Me.DescripcionListBox.SelectedValue), eClv_Session, 0)
            Me.ConServiciosTmpTableAdapter.Connection = CON
            Me.ConServiciosTmpTableAdapter.Fill(Me.DataSetEric2.ConServiciosTmp, eClv_Session)
            Me.ConServiciosProTableAdapter.Connection = CON
            Me.ConServiciosProTableAdapter.Fill(Me.DataSetEric2.ConServiciosPro, Me.ConceptoComboBox.SelectedValue, eClv_Session, 1)
            CON.Close()
        End If
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.DescripcionListBox.Items.Count > 0 Then
            CON.Open()
            Me.InsertarServiciosTmpTableAdapter.Connection = CON
            Me.InsertarServiciosTmpTableAdapter.Fill(Me.DataSetEric2.InsertarServiciosTmp, 0, eClv_Session, 1)
            Me.ConServiciosTmpTableAdapter.Connection = CON
            Me.ConServiciosTmpTableAdapter.Fill(Me.DataSetEric2.ConServiciosTmp, eClv_Session)
            Me.ConServiciosProTableAdapter.Connection = CON
            Me.ConServiciosProTableAdapter.Fill(Me.DataSetEric2.ConServiciosPro, Me.ConceptoComboBox.SelectedValue, eClv_Session, 1)
            CON.Close()
        End If
    End Sub

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.DescripcionListBox1.Items.Count > 0 Then
            CON.Open()
            Me.BorrarServiciosTmpTableAdapter.Connection = CON
            Me.BorrarServiciosTmpTableAdapter.Fill(Me.DataSetEric2.BorrarServiciosTmp, CInt(Me.DescripcionListBox1.SelectedValue), eClv_Session, 0)
            Me.ConServiciosTmpTableAdapter.Connection = CON
            Me.ConServiciosTmpTableAdapter.Fill(Me.DataSetEric2.ConServiciosTmp, eClv_Session)
            Me.ConServiciosProTableAdapter.Connection = CON
            Me.ConServiciosProTableAdapter.Fill(Me.DataSetEric2.ConServiciosPro, Me.ConceptoComboBox.SelectedValue, eClv_Session, 1)
            CON.Close()
        End If
    End Sub

    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.DescripcionListBox1.Items.Count > 0 Then
            CON.Open()
            Me.BorrarServiciosTmpTableAdapter.Connection = CON
            Me.BorrarServiciosTmpTableAdapter.Fill(Me.DataSetEric2.BorrarServiciosTmp, 0, eClv_Session, 1)
            Me.ConServiciosTmpTableAdapter.Connection = CON
            Me.ConServiciosTmpTableAdapter.Fill(Me.DataSetEric2.ConServiciosTmp, eClv_Session)
            Me.ConServiciosProTableAdapter.Connection = CON
            Me.ConServiciosProTableAdapter.Fill(Me.DataSetEric2.ConServiciosPro, Me.ConceptoComboBox.SelectedValue, eClv_Session, 1)
            CON.Close()
        End If
    End Sub


    Private Sub ConceptoComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConceptoComboBox.SelectedIndexChanged
        LLenaServicios()
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        Me.DateTimePicker1.MaxDate = Me.DateTimePicker2.Value
    End Sub

    Private Sub Check()

        Dim CON As New SqlConnection(MiConexion)

        If Me.CheckBox1.Checked = True And Me.CheckBox2.Checked = False Then
            Me.TabControl1.TabPages(0).Text = "Sucursales"
            Me.Panel1.Visible = False
            Me.Panel2.Visible = True
        End If
        If Me.CheckBox1.Checked = False And Me.CheckBox2.Checked = True Then
            Me.TabControl1.TabPages(0).Text = "Vendedores"
            Me.Panel1.Visible = True
            Me.Panel2.Visible = False
        End If

        If (Me.CheckBox1.Checked = True And Me.CheckBox2.Checked = True) Or (Me.CheckBox1.Checked = False And Me.CheckBox2.Checked = False) Then
            Me.TabControl1.Enabled = False
        End If

        If (Me.CheckBox1.Checked = True And Me.CheckBox2.Checked = False) Or (Me.CheckBox1.Checked = False And Me.CheckBox2.Checked = True) Then
            Me.TabControl1.Enabled = True
        End If


        LlenaVendedor()
        LlenaSucursal()
        LLenaServicios()





    End Sub

    

    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        eFechaIni = Me.DateTimePicker1.Value
        eFechaFin = Me.DateTimePicker2.Value
        eTipSer = Me.ConceptoComboBox.SelectedValue
        'If eTipSer = 0 Then
        ' eServicio = "Todos los Tipos"
        'Else
        eServicio = Me.ConceptoComboBox.Text
        '




        'Grafica1 - CONSOLIDADO
        If Me.CheckBox1.Checked = True And Me.CheckBox2.Checked = True Then
            eOpVentas = 11
            eBndGraf = True
            FrmImprimirComision.Show()
        End If

        'Grafica2 - CAJAA CON SUCURSALES POR UN TIPO DE SERVICIO
        If Me.CheckBox1.Checked = True And Me.NombreListBox3.Items.Count > 0 And Me.DescripcionListBox1.Items.Count = 0 Then
            eOpVentas = 12
            eBndGraf = True
            FrmImprimirComision.Show()
        End If

        'Grafica3 - CAJAS CON SERVICIOS DE UN TIPO DE SERVICIO
        If Me.CheckBox1.Checked = True And Me.DescripcionListBox1.Items.Count > 0 And Me.NombreListBox3.Items.Count = 0 Then
            eOpVentas = 13
            eBndGraf = True
            FrmImprimirComision.Show()
        End If

        'Grafica 6 - CAJAS SON SUCURSALES Y SERVICIOS
        If Me.CheckBox1.Checked = True And Me.NombreListBox3.Items.Count > 0 And Me.DescripcionListBox1.Items.Count > 0 Then
            eOpVentas = 16
            eBndGraf = True
            FrmImprimirComision.Show()
        End If

        'Grafica4 - VENTAS DE VENDEDORES CON UN TIPO DE SERVICIO Y VENTAS DE VENDEDORES CON SERVICIOS
        If Me.CheckBox2.Checked = True And Me.NombreListBox1.Items.Count > 0 Then
            If Me.DescripcionListBox1.Items.Count = 0 Then
                eBndOpVentas = True
            End If
            eOpVentas = 14
            eBndGraf = True
            FrmImprimirComision.Show()
        End If

        'Grafica5 - VENTAS DE SERVICIOS
        If Me.CheckBox2.Checked = True And Me.DescripcionListBox1.Items.Count > 0 And Me.NombreListBox1.Items.Count = 0 Then
            eOpVentas = 15
            eBndGraf = True
            FrmImprimirComision.Show()
        End If

    End Sub
End Class