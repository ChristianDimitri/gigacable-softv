﻿Public Class BrwUpselling

#Region "Eventos"
    Private Sub BrwUpselling_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        UspLlenaNivelUsuario()
        UspLlenaGrupo()
        UspBuscarUpselling(0, 0, 99)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub txtClave_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtClave.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
        If KeyAscii = 13 Then
            Dim clave As Integer
            clave = 0
            If Len(txtClave.Text) > 0 Then
                clave = CInt(txtClave.Text)
            End If
           

            UspBuscarUpselling(clave, cmbGrupo.SelectedValue, Me.cmbNivelUsuario.SelectedValue)
            Me.txtClave.Text = ""
            Me.cmbGrupo.SelectedValue = 0
            Me.cmbNivelUsuario.SelectedValue = 99
        End If
    End Sub

    Private Sub BrwUpselling_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        UspBuscarUpselling(0, 0, 99)
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        If DataGridView1.SelectedRows.Count > 0 Then
            Me.lblClave.Text = DataGridView1.SelectedCells(0).Value
            Me.lblGrupo.Text = DataGridView1.SelectedCells(1).Value
            Me.lblNivelUsuario.Text = DataGridView1.SelectedCells(2).Value
            UspMuestraRelCatUpselling(DataGridView1.SelectedCells(0).Value)
        Else
            Me.lblClave.Text = ""
            Me.lblGrupo.Text = ""
            Me.lblNivelUsuario.Text = ""
            UspMuestraRelCatUpselling(0)
        End If
    End Sub
#End Region

#Region "Funciones"
    Private Sub UspLlenaNivelUsuario()
        Try
            BaseII.limpiaParametros()
            cmbNivelUsuario.DataSource = BaseII.ConsultaDT("UspLlenaNivelUsuario")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub UspLlenaGrupo()
        Try
            BaseII.limpiaParametros()
            cmbGrupo.DataSource = BaseII.ConsultaDT("UspLlenaGrupo")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function

    Private Sub UspBuscarUpselling(ByVal prmID As Integer, ByVal prmClvGrupo As Integer, ByVal prmEsSupervisor As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ID", SqlDbType.Int, prmID)
            BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, prmClvGrupo)
            BaseII.CreateMyParameter("@ESSUPERVISOR", SqlDbType.Int, prmEsSupervisor)
            DataGridView1.DataSource = BaseII.ConsultaDT("UspBuscarUpselling")
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub UspMuestraRelCatUpselling(ByVal prmID As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ID", SqlDbType.Int, prmID)
            DataGridView2.DataSource = BaseII.ConsultaDT("UspMuestraRelCatUpselling")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Nuevo"
    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        opcion = "N"
        GloClaveCalculoBono = 0
        FrmUpselling.Show()
    End Sub
#End Region

#Region "Consultar"
    Private Sub BtnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnConsultar.Click
        If DataGridView1.Rows.Count > 0 Then
            opcion = "C"
            GloClaveCalculoBono = DataGridView1.SelectedCells(0).Value
            FrmUpselling.Show()
        End If
       
    End Sub
#End Region

#Region "Modificar"
    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        If DataGridView1.Rows.Count > 0 Then
            opcion = "M"
            GloClaveCalculoBono = DataGridView1.SelectedCells(0).Value
            FrmUpselling.Show()
        End If

    End Sub
#End Region

#Region "Salir"
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
#End Region

#Region "buscar"
    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Dim ClvBono As Integer

        If Len(Me.txtClave.Text) = 0 Then
            ClvBono = 0
        Else
            ClvBono = txtClave.Text
        End If

        UspBuscarUpselling(ClvBono, cmbGrupo.SelectedValue, Me.cmbNivelUsuario.SelectedValue)
        Me.txtClave.Text = ""
        Me.cmbGrupo.SelectedValue = 0
        Me.cmbNivelUsuario.SelectedValue = 99
    End Sub
#End Region

   
End Class