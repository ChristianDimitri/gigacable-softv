Imports System.Data.SqlClient
Public Class BrwMotAtenTel

    Private Sub BrwMotAtenTel_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConMotAtenTelTableAdapter.Connection = CON
        Me.ConMotAtenTelTableAdapter.Fill(Me.DataSetEric.ConMotAtenTel, 0, "", 0)
        CON.Close()
    End Sub


    Private Sub BrwMotAtenTel_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConMotAtenTelTableAdapter.Connection = CON
        Me.ConMotAtenTelTableAdapter.Fill(Me.DataSetEric.ConMotAtenTel, 0, "", 0)
        CON.Close()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim CON As New SqlConnection(MiConexion)
        If Me.TextBox1.Text.Length > 0 Then
            CON.Open()
            Me.ConMotAtenTelTableAdapter.Connection = CON
            Me.ConMotAtenTelTableAdapter.Fill(Me.DataSetEric.ConMotAtenTel, 0, Me.TextBox1.Text, 1)
            CON.Close()
            Me.TextBox1.Clear()
        End If
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        If Asc(e.KeyChar) = 13 And Me.TextBox1.Text.Length > 0 Then
            CON.Open()
            Me.ConMotAtenTelTableAdapter.Connection = CON
            Me.ConMotAtenTelTableAdapter.Fill(Me.DataSetEric.ConMotAtenTel, 0, Me.TextBox1.Text, 1)
            CON.Close()
            Me.TextBox1.Clear()
        End If
    End Sub

    
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eOpcion = "N"
        FrmMotAtenTel.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.ConMotAtenTelDataGridView.RowCount > 0 Then
            eOpcion = "M"
            eClv_MotAtenTel = Me.ConMotAtenTelDataGridView.SelectedCells.Item(0).Value
            FrmMotAtenTel.Show()
        Else
            MsgBox("Selecciona un Motivo para Modificar", , "Atenci�n")
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub



#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region
End Class