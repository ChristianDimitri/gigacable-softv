Imports System.Data.SqlClient
Public Class FrmMetasRep

    Private Sub FrmMetasRep_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            eOpPPE = 0
            eOpVentas = 0
            colorea(Me, Me.Name)

            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.MuestraTipServEricTableAdapter.Connection = CON
            Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric2.MuestraTipServEric, 0, 0)

            CON.Close()
            Consultar()


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    
    Private Sub Consultar()
        Try
            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()
            Me.MuestraMesesTableAdapter.Connection = CON2
            Me.MuestraMesesTableAdapter.Fill(Me.DataSetEric2.MuestraMeses)
            Me.MuestraMeses1TableAdapter.Connection = CON2
            Me.MuestraMeses1TableAdapter.Fill(Me.DataSetEric2.MuestraMeses1)
            Me.MuestraAniosTableAdapter.Connection = CON2
            Me.MuestraAniosTableAdapter.Fill(Me.DataSetEric2.MuestraAnios)
            Me.MuestraAnios1TableAdapter.Connection = CON2
            Me.MuestraAnios1TableAdapter.Fill(Me.DataSetEric2.MuestraAnios1)
            CON2.Close()


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub Checa()
        Try
            eMesIni = Me.MesComboBox.SelectedValue
            eAnioIni = Me.AnioComboBox.SelectedValue
            eMesFin = Me.MesComboBox1.SelectedValue
            eAnioFin = Me.AnioComboBox1.SelectedValue
            eTipSer = Me.ConceptoComboBox.SelectedValue
            eServicio = Me.ConceptoComboBox.Text
            eStrMesIni = Me.MesComboBox.Text
            eStrMesFin = Me.MesComboBox1.Text

            If (eMesIni > eMesFin And eAnioIni = eAnioFin) Or (eAnioIni > eAnioFin) Then
                MsgBox("La Fecha Inicial ( " & eStrMesIni & "/" & CStr(eAnioIni) & " ) no puede ser Mayor a la Fecha Final (" & eStrMesFin & "/" & CStr(eAnioFin) & ").", , "Error")

                Consultar()
            Else

                'Reporte
                If Me.RadioButton3.Checked = True Then

                    If Me.RadioButton1.Checked = True Then
                        'Reporte Vendedores
                        eOpVentas = 20
                        FrmSelVendedor.Show()
                    ElseIf Me.RadioButton2.Checked = True Then
                        'Reporte Sucursales
                        eOpVentas = 21
                        FrmSelSucursal.Show()
                    ElseIf Me.RadioButton5.Checked = True Then
                        'Reporte Ingresos
                        eOpVentas = 30
                        FrmImprimirComision.Show()
                    ElseIf Me.RadioButton6.Checked = True Then
                        'Reporte Ingresos Por Punto de Cobro
                        eOpVentas = 32
                        FrmImprimirComision.Show()
                    ElseIf Me.RadioButton7.Checked = True Then
                        'Reporte Carteras
                        eOpVentas = 34
                        FrmImprimirComision.Show()
                    End If

                    'Gr�fica
                ElseIf Me.RadioButton4.Checked = True Then

                    If Me.RadioButton1.Checked = True Then
                        'Gr�fica Vendedores
                        eOpVentas = 22
                        FrmSelVendedor.Show()
                    ElseIf Me.RadioButton2.Checked = True Then
                        'Gr�fica Sucursales
                        eOpVentas = 23
                        FrmSelSucursal.Show()
                    ElseIf Me.RadioButton5.Checked = True Then
                        'Gr�fica Ingresos
                        eOpVentas = 31
                        FrmImprimirComision.Show()
                    ElseIf Me.RadioButton6.Checked = True Then
                        'Gr�fica Ingresos Por Punto de Cobro
                        eOpVentas = 33
                        FrmImprimirComision.Show()
                    End If
                End If


            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Checa()
    End Sub

    

    Private Sub RadioButton5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton5.CheckedChanged
        If Me.RadioButton5.Checked = True And Me.RadioButton3.Checked = True Then
            Me.GroupBox2.Enabled = False
        ElseIf Me.RadioButton5.Checked = True And Me.RadioButton4.Checked = True Then
            Me.GroupBox2.Enabled = True
        ElseIf Me.RadioButton5.Checked = False Then
            Me.GroupBox2.Enabled = True
        End If
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        If Me.RadioButton3.Checked = True And (Me.RadioButton5.Checked = True Or Me.RadioButton6.Checked = True) Then
            Me.GroupBox2.Enabled = False
        ElseIf Me.RadioButton3.Checked = False And (Me.RadioButton5.Checked = True Or Me.RadioButton6.Checked = True) Then
            Me.GroupBox2.Enabled = True
        End If
    End Sub

    
    Private Sub RadioButton6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton6.CheckedChanged
        If Me.RadioButton6.Checked = True Then

            Me.GroupBox2.Enabled = False
        Else

            Me.GroupBox2.Enabled = True
        End If
    End Sub

    
    
    Private Sub RadioButton7_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton7.CheckedChanged
        If Me.RadioButton7.Checked = True Then
            Me.RadioButton4.Enabled = False
            Me.RadioButton3.Checked = True
            Me.GroupBox2.Enabled = False
        Else
            Me.RadioButton4.Enabled = True
            Me.GroupBox2.Enabled = True
        End If
    End Sub
End Class