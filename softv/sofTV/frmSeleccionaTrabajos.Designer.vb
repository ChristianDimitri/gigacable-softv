﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSeleccionaTrabajos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbTrabajosASeleccionar = New System.Windows.Forms.ListBox()
        Me.lbTrabajosSeleccionados = New System.Windows.Forms.ListBox()
        Me.btSeleccionarTodos = New System.Windows.Forms.Button()
        Me.btSeleccionaUno = New System.Windows.Forms.Button()
        Me.btDeseleccionaUno = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.btAceptar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lbTrabajosASeleccionar
        '
        Me.lbTrabajosASeleccionar.DisplayMember = "DESCRIPCION"
        Me.lbTrabajosASeleccionar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbTrabajosASeleccionar.FormattingEnabled = True
        Me.lbTrabajosASeleccionar.ItemHeight = 15
        Me.lbTrabajosASeleccionar.Location = New System.Drawing.Point(12, 43)
        Me.lbTrabajosASeleccionar.Name = "lbTrabajosASeleccionar"
        Me.lbTrabajosASeleccionar.Size = New System.Drawing.Size(310, 349)
        Me.lbTrabajosASeleccionar.TabIndex = 0
        Me.lbTrabajosASeleccionar.ValueMember = "CLVTRABAJO"
        '
        'lbTrabajosSeleccionados
        '
        Me.lbTrabajosSeleccionados.DisplayMember = "DESCRIPCION"
        Me.lbTrabajosSeleccionados.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbTrabajosSeleccionados.FormattingEnabled = True
        Me.lbTrabajosSeleccionados.ItemHeight = 15
        Me.lbTrabajosSeleccionados.Location = New System.Drawing.Point(394, 43)
        Me.lbTrabajosSeleccionados.Name = "lbTrabajosSeleccionados"
        Me.lbTrabajosSeleccionados.Size = New System.Drawing.Size(310, 349)
        Me.lbTrabajosSeleccionados.TabIndex = 1
        Me.lbTrabajosSeleccionados.ValueMember = "CLVTRABAJO"
        '
        'btSeleccionarTodos
        '
        Me.btSeleccionarTodos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btSeleccionarTodos.Location = New System.Drawing.Point(332, 143)
        Me.btSeleccionarTodos.Name = "btSeleccionarTodos"
        Me.btSeleccionarTodos.Size = New System.Drawing.Size(51, 23)
        Me.btSeleccionarTodos.TabIndex = 2
        Me.btSeleccionarTodos.Text = ">>"
        Me.btSeleccionarTodos.UseVisualStyleBackColor = True
        '
        'btSeleccionaUno
        '
        Me.btSeleccionaUno.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btSeleccionaUno.Location = New System.Drawing.Point(332, 114)
        Me.btSeleccionaUno.Name = "btSeleccionaUno"
        Me.btSeleccionaUno.Size = New System.Drawing.Size(51, 23)
        Me.btSeleccionaUno.TabIndex = 3
        Me.btSeleccionaUno.Text = ">"
        Me.btSeleccionaUno.UseVisualStyleBackColor = True
        '
        'btDeseleccionaUno
        '
        Me.btDeseleccionaUno.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btDeseleccionaUno.Location = New System.Drawing.Point(332, 254)
        Me.btDeseleccionaUno.Name = "btDeseleccionaUno"
        Me.btDeseleccionaUno.Size = New System.Drawing.Size(51, 23)
        Me.btDeseleccionaUno.TabIndex = 4
        Me.btDeseleccionaUno.Text = "<"
        Me.btDeseleccionaUno.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(332, 283)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(51, 23)
        Me.Button4.TabIndex = 5
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'btAceptar
        '
        Me.btAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btAceptar.Location = New System.Drawing.Point(606, 408)
        Me.btAceptar.Name = "btAceptar"
        Me.btAceptar.Size = New System.Drawing.Size(98, 31)
        Me.btAceptar.TabIndex = 6
        Me.btAceptar.Text = "Aceptar"
        Me.btAceptar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(9, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(207, 15)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Seleccione los trabajos a filtrar"
        '
        'frmSeleccionaTrabajos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(716, 458)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btAceptar)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.btDeseleccionaUno)
        Me.Controls.Add(Me.btSeleccionaUno)
        Me.Controls.Add(Me.btSeleccionarTodos)
        Me.Controls.Add(Me.lbTrabajosSeleccionados)
        Me.Controls.Add(Me.lbTrabajosASeleccionar)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSeleccionaTrabajos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selección de Trabajos"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lbTrabajosASeleccionar As System.Windows.Forms.ListBox
    Friend WithEvents lbTrabajosSeleccionados As System.Windows.Forms.ListBox
    Friend WithEvents btSeleccionarTodos As System.Windows.Forms.Button
    Friend WithEvents btSeleccionaUno As System.Windows.Forms.Button
    Friend WithEvents btDeseleccionaUno As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents btAceptar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
