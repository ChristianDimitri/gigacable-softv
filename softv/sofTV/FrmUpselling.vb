﻿Public Class FrmUpselling

#Region "Eventos"
    Private Sub FrmUpselling_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        UspLlenaNivelUsuario()
        UspLlenaGrupo()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        If opcion = "N" Then
            btnGuardar.Enabled = False
            UspDesactivaBotones(Me, Me.Name)
        ElseIf opcion = "C" Then
            GroupBox2.Enabled = False
            btnAgregar.Enabled = False
            btnGuardar.Enabled = False
            DataGridView2.Enabled = False
            UspMuestraCatUpselling(GloClaveCalculoBono)
            UspMuestraRelCatUpselling(GloClaveCalculoBono)
        ElseIf opcion = "M" Then
            GroupBox2.Enabled = False
            btnAgregar.Enabled = False
            UspMuestraCatUpselling(GloClaveCalculoBono)
            UspMuestraRelCatUpselling(GloClaveCalculoBono)
        End If


    End Sub
#End Region

#Region "Funciones"
    Private Sub UspLlenaNivelUsuario()
        Try
            BaseII.limpiaParametros()
            cmbNivelUsuario.DataSource = BaseII.ConsultaDT("UspLlenaNivelUsuario")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub UspLlenaGrupo()
        Try
            BaseII.limpiaParametros()
            cmbGrupoVentas.DataSource = BaseII.ConsultaDT("UspLlenaGrupo")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMuestraRelCatUpselling(ByVal prmID As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ID", SqlDbType.Int, prmID)
            DataGridView2.DataSource = BaseII.ConsultaDT("UspMuestraRelCatUpselling")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMuestraCatUpselling(ByVal prmID As Integer)
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ID", SqlDbType.Int, prmID)
            DT = BaseII.ConsultaDT("UspMuestraCatUpselling")
            If DT.Rows.Count > 0 Then
                cmbGrupoVentas.SelectedValue = CInt(DT.Rows(0)(0).ToString)
                cmbNivelUsuario.SelectedValue = CInt(DT.Rows(0)(1).ToString)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspAgregarUpselling()
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, Me.cmbGrupoVentas.SelectedValue)
            BaseII.CreateMyParameter("@ESSUPERVISOR", SqlDbType.Int, Me.cmbNivelUsuario.SelectedValue)
            DT = BaseII.ConsultaDT("UspAgregarUpselling")
            If DT.Rows.Count > 0 Then
                If DT.Rows(0)(0).ToString = "1" Then
                    MsgBox(DT.Rows(0)(1).ToString, MsgBoxStyle.Information, "Upselling")
                    Exit Sub
                Else
                    GloClaveCalculoBono = CInt(DT.Rows(0)(2).ToString)
                    btnAgregar.Enabled = False
                    btnGuardar.Enabled = True
                    GroupBox2.Enabled = False
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspGuardaUpsellingComision(ByVal prmIdUpselling As Integer, ByVal prmClvServicio As Integer, ByVal prmComision As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IDUPSELLING", SqlDbType.Int, prmIdUpselling)
            BaseII.CreateMyParameter("@CLVSERVICIO", SqlDbType.Int, prmClvServicio)
            BaseII.CreateMyParameter("@COMISION", SqlDbType.Int, prmComision)
            BaseII.Inserta("UspGuardaUpsellingComision")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Agregar"
    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        If cmbNivelUsuario.SelectedValue = 99 Then
            MsgBox("Seleccione el Nivel", MsgBoxStyle.Information, "Upselling")
            Exit Sub
        End If
        If cmbGrupoVentas.SelectedValue = 0 Then
            MsgBox("Seleccione el grupo de ventas", MsgBoxStyle.Information, "Upselling")
            Exit Sub
        End If
        UspAgregarUpselling()
        UspMuestraRelCatUpselling(GloClaveCalculoBono)
    End Sub
#End Region

#Region "Guardar"
    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Try
            If DataGridView2.Rows.Count > 0 Then
                Dim I As Integer
                For I = 0 To DataGridView2.Rows.Count - 1
                    UspGuardaUpsellingComision(GloClaveCalculoBono, DataGridView2(0, I).Value, DataGridView2(2, I).Value)
                Next

                MsgBox("Fue Guardado con Exito", MsgBoxStyle.Information, "Upselling")
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
#End Region

#Region "Salir"
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
#End Region
End Class