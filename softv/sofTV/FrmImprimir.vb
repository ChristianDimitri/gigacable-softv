'Imports System.Collections
'Imports System.Web.UI.WebControls
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Net
Imports System.Net.Sockets
Imports System.IO
Imports System
Imports System.IO.StreamReader
Imports System.IO.File
Imports System.Collections.Generic


Public Class FrmImprimir
    Private customersByCityReport As ReportDocument
    Private op As String = Nothing
    Private Titulo As String = Nothing
    'Private Const PARAMETER_FIELD_NAME As String = "Op"
    Private locbandrep As Boolean = False
    Private locbandrep2 As Boolean = False
    Private bndReportIrdeto As Boolean = False
    Dim Archivo As String
    Dim space As String
    Dim intro As String
    Dim indice As Integer
    Dim indice2 As Integer
    Dim indice3 As Integer
    Dim indice4 As Integer
    Dim Mac As String
    Dim solointernet As Integer = 0

    Dim ConLidia As New SqlClient.SqlConnection(MiConexion)

    Private Sub MandaReportes()
        RutaReportes = DameRutaReportes(1)
        Dim CON100 As New SqlConnection(MiConexion)
        CON100.Open()
        If Me.ComboBox4.SelectedValue = 1 Then
            If IsNumeric(DataGridView1.SelectedCells(0).Value) = True Then
                op = CStr(DataGridView1.SelectedCells(0).Value)
                Titulo = CStr(DataGridView1.SelectedCells(1).Value)
                GloOpRep = op
                If op = "14" Then
                    '(op = "14" And GloTipoUsuario = 40 And IdSistema = "AG") Or (op = "14" And IdSistema <> "AG") Then
                    GloBndEtiqueta = False
                    GloOpEtiqueta = "0"
                    GloSelBanco = 0
                    LocOp = 6
                    'FrmEtiquetas.Show()
                    FrmTipoClientes.Show()
                    'ElseIf op = "14" And GloTipoUsuario <> 40 And IdSistema = "AG" Then
                    '    FrmAccesoReportes.Show()
                ElseIf op = "15" Then
                    '(op = "15" And GloTipoUsuario = 40 And IdSistema = "AG") Or (op = "15" And IdSistema <> "AG") Then
                    LocOp = 8
                    FrmSelCiudad.Show()
                    'ElseIf op = "15" And GloTipoUsuario <> 40 And IdSistema = "AG" Then
                    '    FrmAccesoReportes.Show()
                ElseIf op = "8" Then
                    '(op = "8" And GloTipoUsuario = 40 And IdSistema = "AG") Or (op = "8" And IdSistema <> "AG") Then
                    'GloBndSelBanco = True
                    LocOp = 22
                    GloSelBanco = 0
                    'FrmSelBanco.Show()
                    FrmSelCiudad.Show()
                    'ElseIf op = "8" And GloTipoUsuario <> 40 And IdSistema = "AG" Then
                    '    FrmAccesoReportes.Show()
                ElseIf op = "7" Then
                    LocOp = 9
                    FrmTipoClientes.Show()
                ElseIf op = "4" Or op = "9" Or op = "10" Or op = "13" Or op = "26" Then
                    LocOp = 3
                    If op = "10" Then
                        locbndrepcancelaciones = True
                        ReportOp = 10
                    End If
                    If op = "26" Then
                        bnd_Canc_Sin_Mens = True
                    End If
                    FrmTipoClientes.Show()
                ElseIf op = "16" Then
                    '(op = "16" And GloTipoUsuario = 40 And IdSistema = "AG") Or (op = "16" And IdSistema <> "AG") Then
                    LocOp = 1
                    FrmSelServRep.Show()
                    'ElseIf op = "16" And GloTipoUsuario <> 40 And IdSistema = "AG" Then
                    '    FrmAccesoReportes.Show()
                ElseIf op = "17" Then
                    '(op = "17" And GloTipoUsuario = 40 And IdSistema = "AG") Or (op = "17" And IdSistema <> "AG") Then
                    LocOp = 7
                    FrmTipoClientes.Show()
                    'ElseIf op = "17" And GloTipoUsuario <> 40 And IdSistema = "AG" Then
                    '    FrmAccesoReportes.Show()
                ElseIf op = "1" Or op = "6" Or op = "0" Then
                    '((op = "1" Or op = "6" Or op = "0") And GloTipoUsuario = 40 And IdSistema = "AG") Or ((op = "1" Or op = "6" Or op = "0") And IdSistema <> "AG") Then
                    LocOp = 4
                    If op = "6" Then
                        bnd1 = True
                    Else
                        bnd1 = False
                    End If
                    FrmTipoClientes.Show()
                    'ElseIf (op = "1" Or op = "6" Or op = "0") And GloTipoUsuario <> 40 And IdSistema = "AG" Then
                    '    FrmAccesoReportes.Show()
                ElseIf op = "21" Then
                    LocOp = 4
                    If op = "6" Then
                        bnd1 = True
                    Else
                        bnd1 = False
                    End If
                    FrmTipoClientes.Show()
                ElseIf op = "12" Or op = "2" Or op = "3" Then
                    LocOp = 5
                    ReportOp = 12
                    FrmTipoClientes.Show()

                ElseIf op = "18" Then
                    LocOp = 10
                    FrmTipoClientes.Show()
                ElseIf op = "19" Then
                    '(op = "19" And GloTipoUsuario = 40 And IdSistema = "AG") Or (op = "19" And IdSistema <> "AG") Then
                    LocOp = 20
                    FrmTipoClientes.Show()
                    'ElseIf op = "19" And GloTipoUsuario <> 40 And IdSistema = "AG" Then
                    '    FrmAccesoReportes.Show()
                ElseIf op = "20" Then
                    LocOp = 21
                    FrmSelFechas.Show()
                ElseIf op = "22" Then
                    LocOp = 25
                    FrmTipoClientes.Show()
                ElseIf op = "25" Then
                    LocOp = 35
                    FrmTipoClientes.Show()
                End If
            End If
        ElseIf Me.ComboBox4.SelectedValue = 2 Or Me.ComboBox4.SelectedValue = 5 Then
            If IsNumeric(DataGridView1.SelectedCells(0).Value) = True Then
                op = CStr(DataGridView1.SelectedCells(0).Value)
                Titulo = CStr(DataGridView1.SelectedCells(1).Value)
                GloOpRep = op
                Checa_SoloInternet()
                If op = "14" Then
                    '(op = "14" And GloTipoUsuario = 40 And IdSistema = "AG") Or (op = "14" And IdSistema <> "AG") Then
                    GloBndEtiqueta = False
                    GloOpEtiqueta = "0"
                    GloSelBanco = 0
                    LocOp = 6
                    'FrmEtiquetas.Show()
                    FrmTipoClientes.Show()
                    'ElseIf op = "14" And GloTipoUsuario <> 40 And IdSistema = "AG" Then
                    '    FrmAccesoReportes.Show()
                ElseIf op = "15" Then
                    '(op = "15" And GloTipoUsuario = 40 And IdSistema = "AG") Or (op = "15" And IdSistema <> "AG") Then
                    LocOp = 8
                    FrmSelCiudad.Show()
                    'ElseIf op = "15" And GloTipoUsuario <> 40 And IdSistema = "AG" Then
                    '    FrmAccesoReportes.Show()
                ElseIf op = "8" Then
                    '(op = "8" And GloTipoUsuario = 40 And IdSistema = "AG") Or (op = "8" And IdSistema <> "AG") Then
                    'GloBndSelBanco = True
                    LocOp = 22
                    GloSelBanco = 0
                    'FrmSelBanco.Show()
                    FrmSelCiudad.Show()
                    'ElseIf op = "8" And GloTipoUsuario <> 40 And IdSistema = "AG" Then
                    '    FrmAccesoReportes.Show()
                ElseIf op = "7" Then
                    LocOp = 9
                    FrmTipoClientes.Show()
                ElseIf op = "4" Or op = "9" Or op = "10" Or op = "13" Or op = "26" Then
                    LocOp = 3
                    If op = "10" Then
                        locbndrepcancelaciones = True
                        ReportOp = 10
                    End If
                    If op = "26" Then
                        bnd_Canc_Sin_Mens = True
                    End If
                    FrmTipoClientes.Show()
                ElseIf op = "16" Then
                    '(op = "16" And GloTipoUsuario = 40 And IdSistema = "AG") Or (op = "16" And IdSistema <> "AG") Then
                    LocOp = 1
                    FrmSelServRep.Show()
                    'ElseIf op = "16" And GloTipoUsuario <> 40 And IdSistema = "AG" Then
                    '    FrmAccesoReportes.Show()
                ElseIf op = "17" Then
                    '(op = "17" And GloTipoUsuario = 40 And IdSistema = "AG") Or (op = "17" And IdSistema <> "AG") Then
                    LocOp = 7
                    FrmTipoClientes.Show()
                    'ElseIf op = "17" And GloTipoUsuario <> 40 And IdSistema = "AG" Then
                    '    FrmAccesoReportes.Show()
                ElseIf op = "1" Or op = "6" Or op = "0" Then
                    '((op = "1" Or op = "6" Or op = "0") And GloTipoUsuario = 40 And IdSistema = "AG") Or ((op = "1" Or op = "6" Or op = "0") And IdSistema <> "AG") Then
                    LocOp = 4
                    If op = "6" Then
                        bnd1 = True
                    Else
                        bnd1 = False
                    End If
                    FrmTipoClientes.Show()
                    'ElseIf (op = "1" Or op = "6" Or op = "0") And GloTipoUsuario <> 40 And IdSistema = "AG" Then
                    '    FrmAccesoReportes.Show()
                ElseIf op = "12" Or op = "2" Or op = "3" Then
                    LocOp = 5
                    ReportOp = 12
                    FrmTipoClientes.Show()
                ElseIf op = "18" Then
                    LocOp = 10
                    FrmTipoClientes.Show()
                ElseIf op = "19" Then
                    '(op = "19" And GloTipoUsuario = 40 And IdSistema = "AG") Or (op = "19" And IdSistema <> "AG") Then
                    LocOp = 20
                    FrmTipoClientes.Show()
                    'ElseIf op = "19" And GloTipoUsuario <> 40 And IdSistema = "AG" Then
                    '    FrmAccesoReportes.Show()
                ElseIf op = "20" Then
                    LocOp = 21
                    FrmSelFechas.Show()
                ElseIf op = "22" Then
                    LocOp = 25
                    FrmTipoClientes.Show()
                ElseIf op = "25" Then
                    LocOp = 35
                    FrmTipoClientes.Show()
                End If
            End If
        Else
            If IsNumeric(DataGridView1.SelectedCells(0).Value) = True Then
                op = CStr(DataGridView1.SelectedCells(0).Value)
                Titulo = CStr(DataGridView1.SelectedCells(1).Value)
                GloOpRep = op
                If op = "14" Then
                    '(op = "14" And GloTipoUsuario = 40 And IdSistema = "AG") Or (op = "14" And IdSistema <> "AG") Then
                    GloBndEtiqueta = False
                    GloOpEtiqueta = "0"
                    GloSelBanco = 0
                    LocOp = 6
                    'FrmEtiquetas.Show()
                    FrmTipoClientes.Show()
                    'ElseIf op = "14" And GloTipoUsuario <> 40 And IdSistema = "AG" Then
                    '    FrmAccesoReportes.Show()
                ElseIf op = "15" Then
                    '(op = "15" And GloTipoUsuario = 40 And IdSistema = "AG") Or (op = "15" And IdSistema <> "AG") Then
                    LocOp = 8
                    FrmSelCiudad.Show()
                    'ElseIf op = "15" And GloTipoUsuario <> 40 And IdSistema = "AG" Then
                    '    FrmAccesoReportes.Show()
                ElseIf op = "8" Then
                    '(op = "8" And GloTipoUsuario = 40 And IdSistema = "AG") Or (op = "8" And IdSistema <> "AG") Then
                    'GloBndSelBanco = True
                    GloSelBanco = 0
                    LocOp = 22
                    'FrmSelBanco.Show()
                    FrmSelCiudad.Show()
                    'ElseIf op = "8" And GloTipoUsuario <> 40 And IdSistema = "AG" Then
                    '    FrmAccesoReportes.Show()
                ElseIf op = "7" Then
                    LocOp = 9
                    FrmTipoClientes.Show()
                ElseIf op = "4" Or op = "9" Or op = "10" Or op = "13" Or op = "26" Then
                    LocOp = 3
                    If op = "10" Then
                        ReportOp = 10
                        locbndrepcancelaciones = True
                    End If
                    If op = "26" Then
                        bnd_Canc_Sin_Mens = True
                    End If
                    FrmTipoClientes.Show()
                ElseIf op = "16" Then
                    '(op = "16" And GloTipoUsuario = 40 And IdSistema = "AG") Or (op = "16" And IdSistema <> "AG") Then
                    LocOp = 1
                    FrmSelServRep.Show()
                    'ElseIf op = "16" And GloTipoUsuario <> 40 And IdSistema = "AG" Then
                    '    FrmAccesoReportes.Show()
                ElseIf op = "17" Then
                    '(op = "17" And GloTipoUsuario = 40 And IdSistema = "AG") Or (op = "17" And IdSistema <> "AG") Then
                    LocOp = 7
                    FrmTipoClientes.Show()
                    'ElseIf op = "17" And GloTipoUsuario <> 40 And IdSistema = "AG" Then
                    '    FrmAccesoReportes.Show()
                ElseIf op = "1" Or op = "6" Or op = "0" Then
                    '((op = "1" Or op = "6" Or op = "0") And (GloTipoUsuario = 40 Or GloTipoUsuario = 1) And IdSistema = "AG") Or ((op = "1" Or op = "6" Or op = "0") And IdSistema <> "AG") Then
                    LocOp = 4
                    If op = "6" Then
                        bnd1 = True
                    Else
                        bnd1 = False
                    End If
                    FrmTipoClientes.Show()
                    'ElseIf (op = "1" Or op = "6" Or op = "0") And GloTipoUsuario <> 40 And IdSistema = "AG" Then
                    '    FrmAccesoReportes.Show()
                ElseIf op = "12" Or op = "2" Or op = "3" Then
                    LocOp = 5
                    FrmTipoClientes.Show()
                ElseIf op = "18" Then
                    LocOp = 10
                    FrmTipoClientes.Show()
                ElseIf op = "19" Then
                    Dim i As Integer
                    Dim Paquetes As String
                    Dim x As Integer
                    Me.Borra_Archivo_IrdetoTableAdapter.Connection = CON100
                    Me.Borra_Archivo_IrdetoTableAdapter.Fill(Me.DataSetarnoldo.Borra_Archivo_Irdeto)
                    Me.OpenFileDialog1.FileName = ""
                    Me.OpenFileDialog1.Filter = "Archivo Resultados *.txt|*.txt"
                    Me.OpenFileDialog1.ShowDialog()
                    If Me.OpenFileDialog1.FileName = "" Then
                        MsgBox("No Se Ha Seleccionado el Archivo", MsgBoxStyle.Information)
                    Else

                        Archivo = My.Computer.FileSystem.ReadAllText(Me.OpenFileDialog1.FileName)
                        ' Me.TextBox1.Text = Archivo.ToString

                        ''=====================Se obtiene la primera Mac ============================
                        indice = Archivo.IndexOfAny(",", 0)
                        'Primera Vuelta
                        For i = 0 To indice - 1
                            If IsNumeric(String.Concat(Mac, Archivo(i))) = True Then
                                Mac = String.Concat(Mac, Archivo(i))
                            End If
                            indice += 1
                        Next
                        'Me.ListBox1.Items.Add(Mac)

                        intro = ChrW((Keys.Enter))
                        indice3 = Archivo.IndexOfAny(intro, indice)
                        For x = 13 To indice3 - 16
                            Paquetes = String.Concat(Paquetes, Archivo(x))
                            indice4 += 1
                        Next

                        Me.Inserta_Archivo_IrdetoTableAdapter.Connection = CON100
                        Me.Inserta_Archivo_IrdetoTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Archivo_Irdeto, Trim(Mac), Trim(Paquetes))


                        '===Ya despues del primer renglon  Se obtienen los dem�s datos de la MAC ===========
                        While (indice <> -1)
                            Mac = ""
                            Paquetes = ""
                            intro = ChrW((Keys.Enter))
                            indice2 = Archivo.IndexOfAny(intro, indice)
                            indice = Archivo.IndexOfAny(",", indice2 + 1)
                            If indice = -1 Then
                                Exit While
                            End If
                            indice3 = indice2
                            For i = indice2 To indice - 1
                                If IsNumeric(String.Concat(Mac, Archivo(i))) = True Then
                                    Mac = String.Concat(Mac, Archivo(i))
                                End If
                                indice3 += 1
                            Next
                            'MsgBox(Archivo(indice3), MsgBoxStyle.Information)
                            indice4 = Archivo.IndexOfAny(intro, indice)
                            For i = indice3 + 3 To indice4 - 16
                                Paquetes = Trim(String.Concat(Paquetes, Archivo(i)))

                            Next
                            Me.Inserta_Archivo_IrdetoTableAdapter.Connection = CON100
                            Me.Inserta_Archivo_IrdetoTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Archivo_Irdeto, Trim(Mac), Trim(Paquetes))


                        End While
                        FrmOpIrdeto.Show()
                        'ConfigureCrystalReportsNew6(op)
                    End If
                ElseIf op = "20" Then
                    '(op = "20" And GloTipoUsuario = 40 And IdSistema = "AG") Or (op = "20" And IdSistema <> "AG") Then
                    LocOp = 20
                    FrmTipoClientes.Show()
                    'ElseIf op = "20" And GloTipoUsuario <> 40 And IdSistema = "AG" Then
                    '    FrmAccesoReportes.Show()
                ElseIf op = "21" Then
                    LocOp = 21
                    FrmSelFechas.Show()
                ElseIf op = "22" Then
                    LocOp = 22
                    FrmTipoClientes.Show()
                ElseIf op = "23" Then
                    LocOp = 25
                    FrmTipoClientes.Show()
                ElseIf op = "25" Then
                    LocOp = 35
                    FrmTipoClientes.Show()
                End If
            End If
        End If
        CON100.Close()
    End Sub
    Private Sub Borra_Rel_Telefono_Report()
        Dim con1 As New SqlConnection(MiConexion)
        Dim Cmd As New SqlClient.SqlCommand()
        Try
            con1.Open()
            Cmd = New SqlClient.SqlCommand()
            With Cmd
                .CommandText = "Borra_Rel_Telefono_Report"
                .Connection = con1
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = LocClv_session
                .Parameters.Add(prm)

                Dim i As Integer = Cmd.ExecuteNonQuery()
            End With
            con1.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalReports(ByVal op As String, ByVal Titulo As String)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"
            Dim nuevo As String = Nothing
            If Me.RadioButton1.Checked = True Then
                OpOrdenar = "1"
                nuevo = "_2.rpt"
            ElseIf Me.RadioButton2.Checked = True Then
                OpOrdenar = "2"
                nuevo = ".rpt"
            End If

            Dim reportPath As String = Nothing
            Select Case op
                Case 7
                    'reportPath = Application.StartupPath + "\Reportes\" + "RepAntiguedad.rpt"
                    reportPath = RutaReportes + "\RepAntiguedad.rpt"
                Case 8
                    'reportPath = Application.StartupPath + "\Reportes\" + "RepBancos.rpt"
                    reportPath = RutaReportes + "\RepBancos.rpt"
                Case 15
                    'reportPath = Application.StartupPath + "\Reportes\" + "RepReconexionesPendientes.rpt"
                    reportPath = RutaReportes + "\RepReconexionesPendientes" + nuevo
                    'Case 4, 6
                    '    'reportPath = Application.StartupPath + "\Reportes\" + "ReporteBasicoTvLARGO.rpt"
                    '    reportPath = RutaReportes + "\ReporteBasicoTvLARGO.rpt"
                Case Else
                    'reportPath = Application.StartupPath + "\Reportes\" + "ReporteBasicoTv.rpt"
                    reportPath = RutaReportes + "\ReporteBasicoTv.rpt"
            End Select

            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            If op = "8" Then
                'op
                customersByCityReport.SetParameterValue(0, GloSelBanco)
                '@clv_session int,
                customersByCityReport.SetParameterValue(1, LocClv_session)
                '@habilita int,
                customersByCityReport.SetParameterValue(2, LocValidaHab)
                '@periodo1 bit,
                customersByCityReport.SetParameterValue(3, LocPeriodo1)
                '@periodo2 bit
                customersByCityReport.SetParameterValue(4, LocPeriodo2)

            ElseIf op = "15" Then
                '@OP
                customersByCityReport.SetParameterValue(0, GloClv_tipser2)
                '@Habilita int
                customersByCityReport.SetParameterValue(1, LocValidaHab)
                '@Periodo1 bit
                customersByCityReport.SetParameterValue(2, LocPeriodo1)
                '@periodo2 bit
                customersByCityReport.SetParameterValue(3, LocPeriodo2)
                '@orden int
                customersByCityReport.SetParameterValue(4, CInt(OpOrdenar))
                '@clv_session
                customersByCityReport.SetParameterValue(5, LocClv_session)
            ElseIf op = "7" Then
                '@OP
                customersByCityReport.SetParameterValue(0, GloClv_tipser2)
                '@clv_session
                customersByCityReport.SetParameterValue(1, LocClv_session)
                '@Habilita int
                customersByCityReport.SetParameterValue(2, LocValidaHab)
                '@Periodo1 bit
                customersByCityReport.SetParameterValue(3, LocPeriodo1)
                '@periodo2 bit
                customersByCityReport.SetParameterValue(4, LocPeriodo2)


            End If


            Select Case op
                Case 0, 2, 3, 6, 12
                    mySelectFormula = "Reporte de Clientes " & mySelectFormula
                Case 1, 5, 11, 15, 14, 8
                    mySelectFormula = "Reporte de " & mySelectFormula
                Case 4, 7, 9, 10, 13, 16
                    mySelectFormula = "Resumen de " & mySelectFormula
            End Select



            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"



            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)
            SetDBLogonForReport2(connectionInfo)



            customersByCityReport = Nothing

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ConfigureCrystalReportsNet(ByVal op As String, ByVal Titulo As String)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"
            If Me.RadioButton1.Checked = True Then
                OpOrdenar = "0"
            ElseIf Me.RadioButton2.Checked = True Then
                OpOrdenar = "1"
            ElseIf Me.RadioButton3.Checked = True Then
                OpOrdenar = "2"
            End If

            Dim reportPath As String = Nothing
            'Select Case op
            '    Case 7
            'reportPath = Application.StartupPath + "\Reportes\" + "RepAntiguedad.rpt"
            '    Case 8
            'reportPath = Application.StartupPath + "\Reportes\" + "RepBancos.rpt"
            '    Case 15
            'reportPath = Application.StartupPath + "\Reportes\" + "RepReconexionesPendientes.rpt"
            '    Case Else
            reportPath = RutaReportes + "\ReporteInternet.rpt"

            'End Select
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            'If op = "8" Then
            '    'op
            '    customersByCityReport.SetParameterValue(0, GloSelBanco)

            'ElseIf op = "15" Then
            '    customersByCityReport.SetParameterValue(0, "0")
            '    'Fec_Ini
            '    customersByCityReport.SetParameterValue(1, "01/01/1900")
            '    '@Fec_Fin 
            '    customersByCityReport.SetParameterValue(2, "01/01/1900")
            '    'GloClave
            '    customersByCityReport.SetParameterValue(3, "0")
            'Else
            If ((op >= 1 And op < 7) Or (op >= 9 And op < 14)) Then
                '@Op int
                customersByCityReport.SetParameterValue(0, op)
                ',@OpOrden int
                customersByCityReport.SetParameterValue(1, OpOrdenar)
                ',@Status varchar(max)
                customersByCityReport.SetParameterValue(2, "")
                ',@Fecha_Ini Datetime
                customersByCityReport.SetParameterValue(3, "01/01/1900")
                ',@Fecha_Fin Datetime
                customersByCityReport.SetParameterValue(4, "01/01/1900")
                '@Calle varchar(250)
                customersByCityReport.SetParameterValue(5, "")
                '@Colonia varchar(250),
                customersByCityReport.SetParameterValue(6, "")
                '@Clv_Tecnica int
                customersByCityReport.SetParameterValue(7, "0")
                '@ClV_Tiposervicio
                customersByCityReport.SetParameterValue(8, "2")

            End If



            'MsgBox(customersByCityReport.DataDefinition.FormulaFields(0).Name)
            'defaultParameterValuesList.DataSource = GetDefaultValuesFromParameterField(customersByCityReport)
            '--SetCurrentValuesForParameterField(customersByCityReport)


            'CRXReport.FormulaFields.GetItemByName("Sucursal").Text = "'" & GloSucursal & "'"
            Select Case op
                Case 0, 2, 3, 6, 12
                    mySelectFormula = "Reporte de Clientes " & mySelectFormula
                Case 1, 5, 11, 15, 14, 8
                    mySelectFormula = "Reporte de " & mySelectFormula
                Case 4, 7, 9, 10, 13, 16
                    mySelectFormula = "Resumen de " & mySelectFormula
            End Select

            'Select Case op
            '    Case 4, 6
            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            '    Case Else
            'customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            'End Select

            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"
            'customersByCityReport.DataDefinition.FormulaFields(3).Text = "'" & op & "'"


            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)
            SetDBLogonForReport2(connectionInfo)
            'SetDBLogonForReport(connectionInfo)

            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ConfigureCrystalReportsDig(ByVal op As String, ByVal Titulo As String)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            GloClv_tipser2 = ComboBox4.SelectedValue
            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"
            If Me.RadioButton1.Checked = True Then
                OpOrdenar = "0"
            ElseIf Me.RadioButton2.Checked = True Then
                OpOrdenar = "1"
            ElseIf Me.RadioButton3.Checked = True Then
                OpOrdenar = "2"
            End If

            Dim reportPath As String = Nothing
            'Select Case op
            '    Case 7
            'reportPath = Application.StartupPath + "\Reportes\" + "RepAntiguedad.rpt"
            '    Case 8
            'reportPath = Application.StartupPath + "\Reportes\" + "RepBancos.rpt"
            '    Case 15
            'reportPath = Application.StartupPath + "\Reportes\" + "RepReconexionesPendientes.rpt"
            '    Case Else
            'reportPath = Application.StartupPath + "\Reportes\" + "ReporteDigitalTv.rpt"
            reportPath = RutaReportes + "\ReporteDigitalTv.rpt"
            'End Select
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            'If op = "8" Then
            '    'op
            '    customersByCityReport.SetParameterValue(0, GloSelBanco)

            'ElseIf op = "15" Then
            '    customersByCityReport.SetParameterValue(0, "0")
            '    'Fec_Ini
            '    customersByCityReport.SetParameterValue(1, "01/01/1900")
            '    '@Fec_Fin 
            '    customersByCityReport.SetParameterValue(2, "01/01/1900")
            '    'GloClave
            '    customersByCityReport.SetParameterValue(3, "0")
            'Else
            If ((op >= 1 And op < 7) Or (op >= 9 And op < 14)) Then
                '@Op int
                customersByCityReport.SetParameterValue(0, op)
                ',@OpOrden int
                customersByCityReport.SetParameterValue(1, OpOrdenar)
                ',@Status varchar(max)
                customersByCityReport.SetParameterValue(2, "")
                ',@Fecha_Ini Datetime
                customersByCityReport.SetParameterValue(3, "01/01/1900")
                ',@Fecha_Fin Datetime
                customersByCityReport.SetParameterValue(4, "01/01/1900")
                '@Calle varchar(250)
                customersByCityReport.SetParameterValue(5, "")
                '@Colonia varchar(250),
                customersByCityReport.SetParameterValue(6, "")
                '@Clv_Tecnica int
                customersByCityReport.SetParameterValue(7, "0")
                '@ClV_Tiposervicio
                customersByCityReport.SetParameterValue(8, "3")

            End If



            'MsgBox(customersByCityReport.DataDefinition.FormulaFields(0).Name)
            'defaultParameterValuesList.DataSource = GetDefaultValuesFromParameterField(customersByCityReport)
            '--SetCurrentValuesForParameterField(customersByCityReport)


            'CRXReport.FormulaFields.GetItemByName("Sucursal").Text = "'" & GloSucursal & "'"
            Select Case op
                Case 0, 2, 3, 6, 12
                    mySelectFormula = "Reporte de Clientes " & mySelectFormula
                Case 1, 5, 11, 15, 14, 8
                    mySelectFormula = "Reporte de " & mySelectFormula
                Case 4, 7, 9, 10, 13, 16
                    mySelectFormula = "Resumen de " & mySelectFormula
            End Select

            'Select Case op
            '    Case 4, 6
            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            '    Case Else
            'customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            'End Select

            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"
            'customersByCityReport.DataDefinition.FormulaFields(3).Text = "'" & op & "'"


            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)
            SetDBLogonForReport2(connectionInfo)

            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub ConfigureCrystalReportscontratacionessinpago()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim Contrataciones As Boolean = False
            Dim Instalaciones As Boolean = False
            Dim Fuera_Area As Boolean = False
            Dim Cancelaciones As Boolean = False
            Dim reportPath As String = Nothing
            Dim nuevo As String = Nothing

            Dim mySelectFormula As String = Titulo
            Dim Subformula As String = Nothing
            Dim OpOrdenar As String = "0"
            If Me.RadioButton1.Checked = True Then
                OpOrdenar = "1"
                nuevo = "_2.rpt"
            ElseIf Me.RadioButton2.Checked = True Then
                OpOrdenar = "2"
                nuevo = ".rpt"
            End If


            mySelectFormula = "Reporte de Contrataciones Sin Pago De La Fecha: " + GloFecha_Ini + " A La Fecha : " + GloFecha_Fin


            Select Case GloClv_tipser2
                Case 1
                    reportPath = RutaReportes + "\ReporteContratadosPrimMensTv" + nuevo
                Case 2
                    reportPath = RutaReportes + "\ReporteContratadosPrimMensInt" + nuevo
                Case 3
                    reportPath = RutaReportes + "\ReporteContratadosPrimMensDig" + nuevo
            End Select
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)


            '@clv_Session bigint,@op int,@orden int,@fecha_ini datetime,@fecha_fin datetime,@Habilita int,@periodo1 bit,@periodo2 bit

            '@clv_session
            customersByCityReport.SetParameterValue(0, LocClv_session)
            '@Op int
            customersByCityReport.SetParameterValue(1, GloClv_tipser2)
            '@Orden
            customersByCityReport.SetParameterValue(2, CInt(OpOrdenar))
            '= Me.DateTimePicker1.Text
            '@fecha_ini
            customersByCityReport.SetParameterValue(3, eFechaIni)
            'fecha_fin
            customersByCityReport.SetParameterValue(4, eFechaFin)
            '@Habilita int
            customersByCityReport.SetParameterValue(5, LocValidaHab)
            '@Periodo1 bit
            customersByCityReport.SetParameterValue(6, LocPeriodo1)
            '@periodo2 bit
            customersByCityReport.SetParameterValue(7, LocPeriodo2)






            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)

            Subformula = " Y  Con servicio: " + LocDescr2

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & Subformula & "'"


            'SetDBLogonForReport2(connectionInfo)
            'SetDBLogonForReport(connectionInfo)
            locbandrep = True
            'SetDBLogonForReport(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub ConfigureCrystalReportsNew(ByVal op As String)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim Contrataciones As Boolean = False
            Dim Instalaciones As Boolean = False
            Dim Fuera_Area As Boolean = False
            Dim Cancelaciones As Boolean = False
            Dim reportPath As String = Nothing
            Dim nuevo As String = Nothing

            Dim mySelectFormula As String = Titulo
            Dim Subformula As String = Nothing
            Dim OpOrdenar As String = "0"
            If Me.RadioButton1.Checked = True Then
                'Contrato
                OpOrdenar = "1"
                nuevo = "_2.rpt"
            ElseIf Me.RadioButton2.Checked = True Then
                'Colonia y Calle
                OpOrdenar = "2"
                nuevo = ".rpt"
            End If

            Select Case op
                Case "4"
                    Contrataciones = True
                    mySelectFormula = "Resumen de Clientes con  Status Contratado con Fecha: " + GloFecha_Ini + " A : " + GloFecha_Fin
                Case "9"
                    Instalaciones = True
                    mySelectFormula = "Resumen de Clientes con Status Instalado con Fecha: " + GloFecha_Ini + " A : " + GloFecha_Fin
                Case "10"
                    Cancelaciones = True
                    mySelectFormula = "Resumen de Clientes con Status Cancelado con Fecha: " + GloFecha_Ini + " A : " + GloFecha_Fin
                Case "13"
                    Fuera_Area = True
                    mySelectFormula = "Resumen de Clientes con Status Fuera de Area con Fecha: " + GloFecha_Ini + " A : " + GloFecha_Fin
            End Select

            Select Case GloClv_tipser2
                Case 1
                    reportPath = RutaReportes + "\Reporte_Rango_Fechas_Tv" + nuevo
                Case 2
                    If Cancelaciones = True Then
                        reportPath = RutaReportes + "\Reportes_varios_Fechas5" + nuevo
                    Else
                        reportPath = RutaReportes + "\Reportes_varios_Fechas" + nuevo
                    End If
                Case 3
                    If Cancelaciones = True Then
                        reportPath = RutaReportes + "\REPORTEClientesVariosCancelaciones" + nuevo
                    Else
                        reportPath = RutaReportes + "\Reporte_Rango_Fechas_Digital" + nuevo
                    End If
                Case 5
                    If Cancelaciones = True Then
                        reportPath = RutaReportes + "\REPORTEClientesVariosCancelacionesTel" + nuevo
                    Else
                        reportPath = RutaReportes + "\Reportes_Telvarios_Fechas" + nuevo
                    End If

                    Dim oprocedure As String = ""

                    If Cancelaciones = True Then
                        oprocedure = "REPORTEClientesVariosCancelacionesTel"
                    Else
                        oprocedure = "Reporte_TelTiposCliente_nuevo1"
                    End If



                    'customersByCityReport.Load(reportPath)

                    customersByCityReport = New ReportDocument


                    Dim cnn As New SqlConnection(MiConexion)

                    Dim cmd As New SqlCommand(oprocedure, cnn)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.CommandTimeout = 0
                    'Dim cmd1 As New SqlCommand("select * from CIUDADES", cnn)
                    'cmd1.CommandType = CommandType.Text
                    'Dim cmd2 As New SqlCommand("select * from CLIENTES", cnn)
                    'cmd2.CommandType = CommandType.Text
                    'Dim cmd3 As New SqlCommand("select * from COLONIAS", cnn)
                    'cmd3.CommandType = CommandType.Text


                    If locbndrepcancelaciones = True Then
                        locbndrepcancelaciones = False
                    End If


                    Dim parametro1 As New SqlParameter("@clv_Session", SqlDbType.BigInt)
                    parametro1.Direction = ParameterDirection.Input
                    parametro1.Value = LocClv_session
                    cmd.Parameters.Add(parametro1)


                    Dim parametro2 As New SqlParameter("@op", SqlDbType.Int)
                    parametro2.Direction = ParameterDirection.Input
                    parametro2.Value = GloClv_tipser2
                    cmd.Parameters.Add(parametro2)



                    Dim parametro3 As New SqlParameter("@conectado", SqlDbType.Bit)
                    parametro3.Direction = ParameterDirection.Input
                    parametro3.Value = Contrataciones
                    cmd.Parameters.Add(parametro3)


                    Dim parametro4 As New SqlParameter("@baja", SqlDbType.Bit)
                    parametro4.Direction = ParameterDirection.Input
                    parametro4.Value = Cancelaciones
                    cmd.Parameters.Add(parametro4)

                    Dim parametro5 As New SqlParameter("@Insta", SqlDbType.Bit)
                    parametro5.Direction = ParameterDirection.Input
                    parametro5.Value = Instalaciones
                    cmd.Parameters.Add(parametro5)


                    Dim parametro6 As New SqlParameter("@Desconect", SqlDbType.Bit)
                    parametro6.Direction = ParameterDirection.Input
                    parametro6.Value = 0
                    cmd.Parameters.Add(parametro6)

                    Dim parametro7 As New SqlParameter("@Susp", SqlDbType.Bit)
                    parametro7.Direction = ParameterDirection.Input
                    parametro7.Value = 0
                    cmd.Parameters.Add(parametro7)

                    Dim parametro8 As New SqlParameter("@Fuera ", SqlDbType.Bit)
                    parametro8.Direction = ParameterDirection.Input
                    parametro8.Value = Fuera_Area
                    cmd.Parameters.Add(parametro8)

                    Dim parametro9 As New SqlParameter("@orden", SqlDbType.Int)
                    parametro9.Direction = ParameterDirection.Input
                    parametro9.Value = CInt(OpOrdenar)
                    cmd.Parameters.Add(parametro9)

                    Dim parametro10 As New SqlParameter("@fecha_ini", SqlDbType.DateTime)
                    parametro10.Direction = ParameterDirection.Input
                    parametro10.Value = GloFecha_Ini
                    cmd.Parameters.Add(parametro10)

                    Dim parametro11 As New SqlParameter("@fecha_fin", SqlDbType.DateTime)
                    parametro11.Direction = ParameterDirection.Input
                    parametro11.Value = GloFecha_Fin
                    cmd.Parameters.Add(parametro11)

                    Dim parametro12 As New SqlParameter("@Habilita", SqlDbType.Int)
                    parametro12.Direction = ParameterDirection.Input
                    parametro12.Value = LocValidaHab
                    cmd.Parameters.Add(parametro12)

                    Dim parametro13 As New SqlParameter("@periodo1", SqlDbType.Bit)
                    parametro13.Direction = ParameterDirection.Input
                    parametro13.Value = LocPeriodo1
                    cmd.Parameters.Add(parametro13)


                    Dim parametro14 As New SqlParameter("@periodo2", SqlDbType.Bit)
                    parametro14.Direction = ParameterDirection.Input
                    parametro14.Value = LocPeriodo2
                    cmd.Parameters.Add(parametro14)

                    Dim parametro15 As New SqlParameter("@nrep", SqlDbType.Int)
                    parametro15.Direction = ParameterDirection.Input
                    parametro15.Value = 0
                    cmd.Parameters.Add(parametro15)

                    Dim parametro16 As New SqlParameter("@Correo", SqlDbType.Int)
                    parametro16.Direction = ParameterDirection.Input
                    parametro16.Value = GloContrato
                    cmd.Parameters.Add(parametro16)


                    'customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

                    'CrystalReportViewer1.ReportSource = customersByCityReport
                    Dim ds As New DataSet()
                    Dim da As New SqlDataAdapter(cmd)

                    'Dim data1 As New DataTable()
                    'Dim data2 As New DataTable()
                    'Dim data3 As New DataTable()
                    'Dim data4 As New DataTable()


                    'Dim da1 As New SqlDataAdapter(cmd)
                    'Dim da2 As New SqlDataAdapter(cmd1)
                    'Dim da3 As New SqlDataAdapter(cmd2)
                    'Dim da4 As New SqlDataAdapter(cmd3)



                    da.Fill(ds)
                    ds.Tables(0).TableName = "Reporte_TiposCliente_nuevo1"
                    ds.Tables(1).TableName = "CIUDADES"
                    ds.Tables(2).TableName = "CLIENTES"
                    ds.Tables(3).TableName = "COLONIAS"
                    'da1.Fill(data1)
                    'da2.Fill(data2)
                    'da3.Fill(data3)
                    'da4.Fill(data4)


                    'ds.Tables.Add(data1)
                    'ds.Tables.Add(data2)
                    'ds.Tables.Add(data3)
                    'ds.Tables.Add(data4)

                    customersByCityReport.Load(reportPath)

                    SetDBReport(ds, customersByCityReport)

                    CrystalReportViewer1.ReportSource = customersByCityReport

                    CrystalReportViewer1.Zoom(75)

                    Subformula = " Y  Con servicio: " + LocDescr2

                    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                    customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
                    customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & Subformula & "'"

            End Select
            If GloClv_tipser2 <> 5 Then
                If locbndrepcancelaciones = True Then
                    locbndrepcancelaciones = False
                End If
                'MsgBox(reportPath)
                customersByCityReport.Load(reportPath)

                'SAUL
                If Cancelaciones = True Then
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@clv_Session", SqlDbType.BigInt, LocClv_session)
                    BaseII.CreateMyParameter("@op", SqlDbType.Int, GloClv_tipser2)
                    BaseII.CreateMyParameter("@conectado", SqlDbType.Bit, Contrataciones)
                    BaseII.CreateMyParameter("@baja", SqlDbType.Bit, Cancelaciones)
                    BaseII.CreateMyParameter("@Insta", SqlDbType.Bit, Instalaciones)
                    BaseII.CreateMyParameter("@Desconect", SqlDbType.Bit, 0)
                    BaseII.CreateMyParameter("@Susp", SqlDbType.Bit, 0)
                    BaseII.CreateMyParameter("@Fuera", SqlDbType.Bit, Fuera_Area)
                    BaseII.CreateMyParameter("@orden", SqlDbType.Int, CInt(OpOrdenar))
                    BaseII.CreateMyParameter("@fecha_ini", SqlDbType.DateTime, GloFecha_Ini)
                    BaseII.CreateMyParameter("@fecha_fin", SqlDbType.DateTime, GloFecha_Fin)
                    BaseII.CreateMyParameter("@Habilita", SqlDbType.Int, LocValidaHab)
                    BaseII.CreateMyParameter("@periodo1", SqlDbType.Bit, LocPeriodo1)
                    BaseII.CreateMyParameter("@periodo2", SqlDbType.Bit, LocPeriodo2)
                    BaseII.CreateMyParameter("@Correo", SqlDbType.Int, GloCorreo)

                    Dim Lt As New List(Of String)
                    Lt.Add("Reporte_TiposCliente_nuevo1")
                    Lt.Add("CIUDADES")
                    Lt.Add("CLIENTES")
                    Lt.Add("COLONIAS")
                    Dim Ds As New DataSet

                    Ds = BaseII.ConsultaDS("REPORTEClientesVariosCancelaciones", Lt)

                    SetDBReport(Ds, customersByCityReport)
                Else
                    SetDBLogonForReport(connectionInfo, customersByCityReport)
                    '@clv_Session bigint,@op int,@conectado bit,@baja bit,@Insta bit,@Desconect bit,@Susp bit,@Fuera bit,@orden int,@fecha_ini datetime,@fecha_fin datetime)  

                    '@clv_session
                    customersByCityReport.SetParameterValue(0, LocClv_session)
                    '@Op int
                    customersByCityReport.SetParameterValue(1, GloClv_tipser2)
                    ',@contratado bit
                    customersByCityReport.SetParameterValue(2, Contrataciones)
                    ',@Baja bit
                    customersByCityReport.SetParameterValue(3, Cancelaciones)
                    ',@Insta bit
                    customersByCityReport.SetParameterValue(4, Instalaciones)
                    '@Desconect bit
                    customersByCityReport.SetParameterValue(5, 0)
                    '@Suspendido bit
                    customersByCityReport.SetParameterValue(6, 0)
                    '@Fuera bit
                    customersByCityReport.SetParameterValue(7, Fuera_Area)
                    '@Orden
                    customersByCityReport.SetParameterValue(8, CInt(OpOrdenar))
                    '@fecha_ini
                    customersByCityReport.SetParameterValue(9, GloFecha_Ini)
                    'fecha_fin
                    customersByCityReport.SetParameterValue(10, GloFecha_Fin)
                    '@Habilita int
                    customersByCityReport.SetParameterValue(11, LocValidaHab)
                    '@Periodo1 bit
                    customersByCityReport.SetParameterValue(12, LocPeriodo1)
                    '@periodo2 bit
                    customersByCityReport.SetParameterValue(13, LocPeriodo2)
                End If
                'SAUL FIN

                customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

                CrystalReportViewer1.ReportSource = customersByCityReport
                CrystalReportViewer1.Zoom(75)

                Subformula = " Y  Con servicio: " + LocDescr2

                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
                customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & Subformula & "'"
            End If


            'SetDBLogonForReport2(connectionInfo)
            'SetDBLogonForReport(connectionInfo)
            locbandrep = True
            'SetDBLogonForReport(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ConfigureCrystalReportNew1(ByVal op As String)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            Dim reporte As Integer = 0
            Dim mySelectFormula As String = Titulo
            Dim Subformula As String = Nothing
            Dim OpOrdenar As String = "0"
            Dim cortesia As Boolean = False
            Dim nuevo As String = Nothing
            If Me.RadioButton1.Checked = True Then
                OpOrdenar = "1"
                nuevo = "_2.rpt"
            ElseIf Me.RadioButton2.Checked = True Then
                OpOrdenar = "2"
                nuevo = ".rpt"
                'nuevo = "_2.rpt"
            End If
            If op = "16" Then
                cortesia = True
            End If

            Select Case op
                Case "1"
                    If LocTodos = 0 Then
                        If GloLocTel = True Then
                            mySelectFormula = "Resumen de Clientes Supendidos con telefono"
                        Else
                            mySelectFormula = "Resumen de Clientes Suspendidos sin telefono"
                        End If
                    Else
                        mySelectFormula = "Resumen de Clientes Suspendidos "
                    End If
                    reporte = 2
                Case "6"
                    If LocTodos = 0 Then
                        If GloLocTel = True Then
                            mySelectFormula = "Resumen de Clientes por Pagar con telefono"
                        Else
                            mySelectFormula = "Resumen de Clientes por Pagar sin telefono"
                        End If
                    Else
                        mySelectFormula = "Resumen de Clientes por Pagar "
                    End If

                    reporte = 1
                Case "0"
                    If LocTodos = 0 Then
                        If GloLocTel = True Then
                            mySelectFormula = "Resumen de Clientes Desconectados con telefono"
                        Else
                            mySelectFormula = "Resumen de Clientes Desconectados sin telefono"
                        End If
                    Else
                        mySelectFormula = "Resumen de Clientes Desconectados"
                    End If
                    reporte = 3
                Case "21"
                    If LocTodos = 0 Then
                        If GloLocTel = True Then
                            mySelectFormula = "Resumen de Clientes Con Suspension Temporal con telefono"
                        Else
                            mySelectFormula = "Resumen de Clientes Con Suspension Temporal sin telefono"
                        End If
                    Else
                        mySelectFormula = "Resumen de Clientes Con Suspension Temporal"
                    End If
                    reporte = 4
            End Select


            Select Case GloClv_tipser2
                Case 1
                    If reporte = 2 Then
                        reportPath = RutaReportes + "\REportePorPagarTv" + nuevo
                    ElseIf reporte <> 2 Then
                        reportPath = RutaReportes + "\ReportePorPagarInternet" + nuevo
                    End If
                Case 2
                    reportPath = RutaReportes + "\ReportePorPagarInternet" + nuevo
                Case 3
                    reportPath = RutaReportes + "\ReportePorPagarDigital" + nuevo
                Case 5  'Agregando a Telefonia
                    reportPath = RutaReportes + "\ReporteTelPorPagarInternet" + nuevo
                    Dim cnn As New SqlConnection(MiConexion)

                    Dim cmd As New SqlCommand("Reporte_TelTiposCliente_telefono1", cnn)
                    cmd.CommandType = CommandType.StoredProcedure


                    Dim parametro As New SqlParameter("@clv_Session", SqlDbType.BigInt)
                    parametro.Direction = ParameterDirection.Input
                    parametro.Value = LocClv_session
                    cmd.Parameters.Add(parametro)

                    Dim parametro1 As New SqlParameter("@op", SqlDbType.Int)
                    parametro1.Direction = ParameterDirection.Input
                    parametro1.Value = GloClv_tipser2
                    cmd.Parameters.Add(parametro1)

                    Dim parametro2 As New SqlParameter("@clv_reporte", SqlDbType.Int)
                    parametro2.Direction = ParameterDirection.Input
                    parametro2.Value = reporte
                    cmd.Parameters.Add(parametro2)

                    Dim parametro3 As New SqlParameter("@telefono", SqlDbType.Bit)
                    parametro3.Direction = ParameterDirection.Input
                    parametro3.Value = GloLocTel
                    cmd.Parameters.Add(parametro3)

                    Dim parametro4 As New SqlParameter("@Orden", SqlDbType.Int)
                    parametro4.Direction = ParameterDirection.Input
                    parametro4.Value = OpOrdenar
                    cmd.Parameters.Add(parametro4)

                    Dim parametro5 As New SqlParameter("@Habilita", SqlDbType.Int)
                    parametro5.Direction = ParameterDirection.Input
                    parametro5.Value = LocValidaHab
                    cmd.Parameters.Add(parametro5)

                    Dim parametro6 As New SqlParameter("@Periodo1", SqlDbType.Bit)
                    parametro6.Direction = ParameterDirection.Input
                    parametro6.Value = LocPeriodo1
                    cmd.Parameters.Add(parametro6)

                    Dim parametro7 As New SqlParameter("@periodo2", SqlDbType.Bit)
                    parametro7.Direction = ParameterDirection.Input
                    parametro7.Value = LocPeriodo2
                    cmd.Parameters.Add(parametro7)

                    Dim parametro8 As New SqlParameter("@Todos", SqlDbType.Int)
                    parametro8.Direction = ParameterDirection.Input
                    parametro8.Value = LocTodos
                    cmd.Parameters.Add(parametro8)

                    Dim parametro9 As New SqlParameter("@Ultimo_mes", SqlDbType.Int)
                    parametro9.Direction = ParameterDirection.Input
                    parametro9.Value = Locultimo_mes
                    cmd.Parameters.Add(parametro9)


                    Dim parametro10 As New SqlParameter("@ultimo_anio", SqlDbType.BigInt)
                    parametro10.Direction = ParameterDirection.Input
                    parametro10.Value = Locultimo_anio
                    cmd.Parameters.Add(parametro10)

                    Dim parametro11 As New SqlParameter("@nrep", SqlDbType.Int)
                    parametro11.Direction = ParameterDirection.Input
                    parametro11.Value = 4
                    cmd.Parameters.Add(parametro11)

                    Dim parametro12 As New SqlParameter("@Correo", SqlDbType.Int)
                    parametro12.Direction = ParameterDirection.Input
                    parametro12.Value = GloCorreo
                    cmd.Parameters.Add(parametro12)

                    Dim da As New SqlDataAdapter(cmd)

                    Dim ds As New DataSet()



                    da.Fill(ds)


                    ds.Tables(0).TableName = "Reporte_TiposCliente_telefono"
                    ds.Tables(1).TableName = "CALLES"
                    ds.Tables(2).TableName = "CIUDADES"
                    ds.Tables(3).TableName = "CLIENTES"
                    ds.Tables(4).TableName = "COLONIAS"

                    If ds.Tables(0).Rows.Count > 0 Then
                        Dim colstring As DataColumn = New DataColumn("clv_reporte")
                        colstring.DataType = System.Type.GetType("System.Int32")
                        ds.Tables(0).Columns.Add(colstring)

                        For Each dr As DataRow In ds.Tables(0).Rows
                            dr("clv_reporte") = reporte
                        Next
                    Else
                        Dim colstring As DataColumn = New DataColumn("clv_reporte")
                        colstring.DataType = System.Type.GetType("System.Int32")
                        ds.Tables(0).Columns.Add(colstring)

                        For Each dr As DataRow In ds.Tables(0).Rows
                            dr("clv_reporte") = reporte
                        Next
                    End If


                    Subformula = GloSucursal


                    customersByCityReport.Load(reportPath)
                    SetDBReport(ds, customersByCityReport)
                    CrystalReportViewer1.ReportSource = customersByCityReport
                    CrystalReportViewer1.Zoom(75)

                    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                    customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
                    customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & Subformula & "'"

            End Select
            If GloClv_tipser2 <> 5 Then
                customersByCityReport.Load(reportPath)

                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clv_Session", SqlDbType.BigInt, LocClv_session)
                BaseII.CreateMyParameter("@op", SqlDbType.Int, GloClv_tipser2)
                BaseII.CreateMyParameter("@clv_reporte", SqlDbType.Int, reporte)
                BaseII.CreateMyParameter("@telefono", SqlDbType.Bit, GloLocTel)
                BaseII.CreateMyParameter("@Orden", SqlDbType.Int, OpOrdenar)
                BaseII.CreateMyParameter("@habilita", SqlDbType.Int, LocValidaHab)
                BaseII.CreateMyParameter("@periodo1", SqlDbType.Bit, LocPeriodo1)
                BaseII.CreateMyParameter("@periodo2", SqlDbType.Bit, LocPeriodo2)
                BaseII.CreateMyParameter("@todos", SqlDbType.Int, LocTodos)
                BaseII.CreateMyParameter("@ultimo_mes", SqlDbType.Int, Locultimo_mes)
                BaseII.CreateMyParameter("@ultimo_anio", SqlDbType.BigInt, Locultimo_anio)
                BaseII.CreateMyParameter("@Correo", SqlDbType.Int, GloCorreo)

                Dim Lt As New List(Of String)
                Lt.Add("Reporte_TiposCliente_telefono")
                Lt.Add("CALLES")
                Lt.Add("CIUDADES")
                Lt.Add("CLIENTES")
                Lt.Add("COLONIAS")

                Dim DS As New DataSet
                DS = BaseII.ConsultaDS("Reporte_TiposCliente_telefono", Lt)

                SetDBReport(DS, customersByCityReport)

                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                If GloClv_tipser2 = 2 Then
                    ''@clv_Session bigint)
                    ''@clv_session
                    'customersByCityReport.SetParameterValue(0, LocClv_session)
                    '',@op int,
                    'customersByCityReport.SetParameterValue(1, GloClv_tipser2)
                    '@clv_reporte int,
                    customersByCityReport.SetParameterValue(0, reporte)
                    ''@telefono bit,
                    'customersByCityReport.SetParameterValue(3, GloLocTel)
                    ''@Orden int
                    'customersByCityReport.SetParameterValue(4, OpOrdenar)
                    ''@Habilita int
                    'customersByCityReport.SetParameterValue(5, LocValidaHab)
                    ''@Periodo1 bit
                    'customersByCityReport.SetParameterValue(6, LocPeriodo1)
                    ''@periodo2 bit
                    'customersByCityReport.SetParameterValue(7, LocPeriodo2)
                    ''@Todos
                    'customersByCityReport.SetParameterValue(8, LocTodos)
                    ''Ultimo_mes
                    'customersByCityReport.SetParameterValue(9, Locultimo_mes)
                    ''Ultimo_a�o
                    'customersByCityReport.SetParameterValue(10, Locultimo_anio)
                End If

                If reporte = 1 Or (GloClv_tipser2 = 3) Then
                    customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
                Else
                    customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                End If


                CrystalReportViewer1.ReportSource = customersByCityReport
                CrystalReportViewer1.Zoom(75)

                Subformula = GloSucursal

                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
                customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & Subformula & "'"
            End If

            'SetDBLogonForReport2(connectionInfo)
            'SetDBLogonForReport(connectionInfo)
            locbandrep = True
            'SetDBLogonForReport(connectionInfo)
            customersByCityReport = Nothing
            'Borra_Rel_Telefono_Report()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ConfigureCrystalReportNew2(ByVal op As String)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            Dim reporte As Integer = 0
            Dim mySelectFormula As String = Titulo
            Dim Subformula As String = Nothing
            Dim OpOrdenar As String = "0"
            Dim nuevo As String = Nothing

            If Me.RadioButton1.Checked = True Then
                OpOrdenar = "1"
                nuevo = "_2.rpt"
            ElseIf Me.RadioButton2.Checked = True Then
                OpOrdenar = "2"
                nuevo = ".rpt"
                'nuevo = "_2.rpt"
            End If

            Select Case op
                Case "12"
                    mySelectFormula = "Resumen de Clientes Por Instalar"
                    reporte = 2
                Case "2"
                    mySelectFormula = "Resumen de Clientes Al corriente"
                    reporte = 1
                Case "3"
                    mySelectFormula = "Resumen de Clientes Adelantados"
                    reporte = 3
            End Select

            Select Case GloClv_tipser2
                Case 1
                    reportPath = RutaReportes + "\ReportePIInt" + nuevo
                Case 2
                    reportPath = RutaReportes + "\ReportePIInt" + nuevo
                Case 3
                    reportPath = RutaReportes + "\ReportPIdig" + nuevo
                Case 5 ''Agregando los detalles de TELEFON�A


                    reportPath = RutaReportes + "\ReporteTelPIInt" + nuevo
                    Dim oprocedure As String = "Reporte_TelTiposCliente_nuevo2"

                    'customersByCityReport.Load(reportPath)

                    customersByCityReport = New ReportDocument


                    Dim cnn As New SqlConnection(MiConexion)

                    Dim cmd As New SqlCommand(oprocedure, cnn)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.CommandTimeout = 0
                    'Dim cmd1 As New SqlCommand("select * from CIUDADES", cnn)
                    'cmd1.CommandType = CommandType.Text
                    'Dim cmd2 As New SqlCommand("select * from CLIENTES", cnn)
                    'cmd2.CommandType = CommandType.Text
                    'Dim cmd3 As New SqlCommand("select * from COLONIAS", cnn)
                    'cmd3.CommandType = CommandType.Text


                    If locbndrepcancelaciones = True Then
                        locbndrepcancelaciones = False
                    End If


                    'reportPath = RutaReportes + "\ReporteTelPIInt" + nuevo
                    'Dim cnn As New SqlConnection(MiConexion)

                    'Dim cmd As New SqlCommand("Reporte_TelTiposCliente_nuevo2;1", cnn)
                    'cmd.CommandType = CommandType.StoredProcedure
                    'Dim da As New SqlDataAdapter(cmd)


                    Dim parametro As New SqlParameter("@clv_session", SqlDbType.BigInt)
                    parametro.Direction = ParameterDirection.Input
                    parametro.Value = LocClv_session
                    cmd.Parameters.Add(parametro)

                    Dim parametro1 As New SqlParameter("@op ", SqlDbType.Int)
                    parametro1.Direction = ParameterDirection.Input
                    parametro1.Value = GloClv_tipser2
                    cmd.Parameters.Add(parametro1)

                    Dim parametro2 As New SqlParameter("@op_rep", SqlDbType.Int)
                    parametro2.Direction = ParameterDirection.Input
                    parametro2.Value = reporte
                    cmd.Parameters.Add(parametro2)

                    Dim parametro3 As New SqlParameter("@Orden", SqlDbType.Int)
                    parametro3.Direction = ParameterDirection.Input
                    parametro3.Value = OpOrdenar
                    cmd.Parameters.Add(parametro3)

                    Dim parametro4 As New SqlParameter("@Habilita", SqlDbType.Int)
                    parametro4.Direction = ParameterDirection.Input
                    parametro4.Value = LocValidaHab
                    cmd.Parameters.Add(parametro4)

                    Dim parametro5 As New SqlParameter("@Periodo1", SqlDbType.Bit)
                    parametro5.Direction = ParameterDirection.Input
                    parametro5.Value = LocPeriodo1
                    cmd.Parameters.Add(parametro5)

                    Dim parametro6 As New SqlParameter("@periodo2", SqlDbType.Bit)
                    parametro6.Direction = ParameterDirection.Input
                    parametro6.Value = LocPeriodo2
                    cmd.Parameters.Add(parametro6)

                    Dim parametro7 As New SqlParameter("@nrep", SqlDbType.Int)
                    parametro7.Direction = ParameterDirection.Input
                    parametro7.Value = 0
                    cmd.Parameters.Add(parametro7)

                    Dim parametro8 As New SqlParameter("@Correo", SqlDbType.Int)
                    parametro8.Direction = ParameterDirection.Input
                    parametro8.Value = GloCorreo
                    cmd.Parameters.Add(parametro8)

                    Dim ds As New DataSet()

                    'Dim data1 As New DataTable()
                    'Dim data2 As New DataTable()
                    'Dim data3 As New DataTable()
                    'Dim data4 As New DataTable()


                    'Dim da1 As New SqlDataAdapter(cmd)
                    'Dim da2 As New SqlDataAdapter(cmd1)
                    'Dim da3 As New SqlDataAdapter(cmd2)
                    'Dim da4 As New SqlDataAdapter(cmd3)


                    Dim da As New SqlDataAdapter(cmd)
                    da.Fill(ds)
                    ds.Tables(0).TableName = "Reporte_TiposCliente_nuevo2"
                    ds.Tables(1).TableName = "CIUDADES"
                    ds.Tables(2).TableName = "CLIENTES"
                    ds.Tables(3).TableName = "COLONIAS"
                    'da1.Fill(data1)
                    'da2.Fill(data2)
                    'da3.Fill(data3)
                    'da4.Fill(data4)


                    'ds.Tables.Add(data1)
                    'ds.Tables.Add(data2)
                    'ds.Tables.Add(data3)
                    'ds.Tables.Add(data4)

                    customersByCityReport.Load(reportPath)

                    SetDBReport(ds, customersByCityReport)

                    CrystalReportViewer1.ReportSource = customersByCityReport

                    CrystalReportViewer1.Zoom(75)

                    Subformula = " Y  Con servicio: " + LocDescr2

                    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                    customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
                    customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & Subformula & "'"

            End Select
            If GloClv_tipser2 <> 5 Then
                customersByCityReport.Load(reportPath)
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clv_Session", SqlDbType.BigInt, LocClv_session)
                BaseII.CreateMyParameter("@op", SqlDbType.Int, GloClv_tipser2)
                BaseII.CreateMyParameter("@op_rep", SqlDbType.Int, reporte)
                BaseII.CreateMyParameter("@orden", SqlDbType.Int, OpOrdenar)
                BaseII.CreateMyParameter("@habilita", SqlDbType.Int, LocValidaHab)
                BaseII.CreateMyParameter("@periodo1", SqlDbType.Bit, LocPeriodo1)
                BaseII.CreateMyParameter("@periodo2", SqlDbType.Bit, LocPeriodo2)
                BaseII.CreateMyParameter("@Correo", SqlDbType.Int, GloCorreo)

                Dim Lt As New List(Of String)
                Lt.Add("Reporte_TiposCliente_nuevo2")
                Lt.Add("CIUDADES")
                Lt.Add("CLIENTES")
                Lt.Add("COLONIAS")
                Dim Ds As New DataSet
                Ds = BaseII.ConsultaDS("Reporte_TiposCliente_nuevo2", Lt)
                SetDBReport(Ds, customersByCityReport)

                'SetDBLogonForReport(connectionInfo, customersByCityReport)


                ''@clv_Session bigint)
                ''@clv_session
                'customersByCityReport.SetParameterValue(0, LocClv_session)
                '',@op int,
                'customersByCityReport.SetParameterValue(1, GloClv_tipser2)
                ''@clv_reporte int,
                'customersByCityReport.SetParameterValue(2, reporte)
                ''@Orden int
                'customersByCityReport.SetParameterValue(3, OpOrdenar)
                ''@Habilita int
                'customersByCityReport.SetParameterValue(4, LocValidaHab)
                ''@Periodo1 bit
                'customersByCityReport.SetParameterValue(5, LocPeriodo1)
                ''@periodo2 bit
                'customersByCityReport.SetParameterValue(6, LocPeriodo2)







                customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

                CrystalReportViewer1.ReportSource = customersByCityReport
                CrystalReportViewer1.Zoom(75)

                Subformula = GloSucursal

                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
                customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & Subformula & "'"

            End If

            'SetDBLogonForReport2(connectionInfo)
            'SetDBLogonForReport(connectionInfo)
            locbandrep = True
            'SetDBLogonForReport(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ConfigureCrystalReportNew3(ByVal op As String)

        Try

            Dim rDocument As New CrystalDecisions.CrystalReports.Engine.ReportDocument
            Dim dSet As New DataSet

            Dim reportPath As String = Nothing
            Dim mySelectFormula As String = Titulo
            Dim Subformula As String = Nothing
            Dim nuevo As String = Nothing
            Dim OpOrdenar As String = "0"
            Dim NCUAL As Integer = 0

            If Me.RadioButton1.Checked = True Then
                OpOrdenar = "1"
                nuevo = "_2.rpt"
                NCUAL = 0
            ElseIf Me.RadioButton2.Checked = True Then
                OpOrdenar = "2"
                nuevo = ".rpt"
                NCUAL = 1
            End If

            mySelectFormula = "Resumen de Con Paquetes de Cortes�a."

            Select Case GloClv_tipser2
                Case 1
                    reportPath = RutaReportes + "\ReporteNuevoCortesiaTv" + nuevo
                Case 2
                    reportPath = RutaReportes + "\ReporteNuevoCortesiaIntDig" + nuevo
                Case 3
                    reportPath = RutaReportes + "\ReporteNuevoCortesiaIntDig" + nuevo
                Case 5
                    reportPath = RutaReportes + "\ReporteTelNuevoCortesiaIntDig" + nuevo
            End Select

            If GloClv_tipser2 <> 5 Then
                dSet = REPORTECortesias(LocClv_session, GloFecha_Ini, GloFecha_Fin, GloClv_tipser2, OpOrdenar, LocValidaHab, LocPeriodo1, LocPeriodo2, GloRepCortesiaActivo, GloRepCortesiaInactivo)
            ElseIf GloClv_tipser2 = 5 Then
                dSet = REPORTECortesiasTEL(LocClv_session, GloFecha_Ini, GloFecha_Fin, GloClv_tipser2, OpOrdenar, LocValidaHab, LocPeriodo1, LocPeriodo2, NCUAL, GloRepCortesiaActivo, GloRepCortesiaInactivo)
            End If

            If GloClv_tipser2 = 1 Then
                rDocument.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            ElseIf GloClv_tipser2 = 2 Or GloClv_tipser2 = 3 Then
                rDocument.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            End If

            Subformula = GloSucursal
            rDocument.Load(reportPath)
            rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            rDocument.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            rDocument.DataDefinition.FormulaFields("SubTitulo").Text = "'" & Subformula & "'"
            rDocument.SetDataSource(dSet)
            CrystalReportViewer1.ReportSource = rDocument
            CrystalReportViewer1.Zoom(75)

            locbandrep = True

            rDocument = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


        'Try
        '    customersByCityReport = New ReportDocument
        '    Dim connectionInfo As New ConnectionInfo
        '    '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    '    "=True;User ID=DeSistema;Password=1975huli")
        '    connectionInfo.ServerName = GloServerName
        '    connectionInfo.DatabaseName = GloDatabaseName
        '    connectionInfo.UserID = GloUserID
        '    connectionInfo.Password = GloPassword

        '    Dim reportPath As String = Nothing
        '    Dim mySelectFormula As String = Titulo
        '    Dim Subformula As String = Nothing
        '    Dim nuevo As String = Nothing
        '    Dim OpOrdenar As String = "0"
        '    Dim NCUAL As Integer = 0

        '    If Me.RadioButton1.Checked = True Then
        '        OpOrdenar = "1"
        '        nuevo = "_2.rpt"
        '        NCUAL = 0
        '    ElseIf Me.RadioButton2.Checked = True Then
        '        OpOrdenar = "2"
        '        nuevo = ".rpt"
        '        NCUAL = 1
        '    End If

        '    mySelectFormula = "Resumen de Con Paquetes de Cortes�a."


        '    Select Case GloClv_tipser2
        '        Case 1
        '            'reportPath = RutaReportes + "\ReporteCortesiaInternet.rpt"
        '            reportPath = RutaReportes + "\ReporteNuevoCortesiaTv" + nuevo
        '        Case 2
        '            'reportPath = RutaReportes + "\ReporteCortesiaInternet.rpt"
        '            reportPath = RutaReportes + "\ReporteNuevoCortesiaIntDig" + nuevo
        '        Case 3
        '            ' reportPath = RutaReportes + "\ReporteCortesiaDigital.rpt"
        '            reportPath = RutaReportes + "\ReporteNuevoCortesiaIntDig" + nuevo
        '        Case 5
        '            reportPath = RutaReportes + "\ReporteTelNuevoCortesiaIntDig" + nuevo
        '            NCUAL = 1
        '            'Select GloClv_tipser2
        '            '   Case 1
        '            'reportPath = RutaReportes + "\ReporteCortesiaInternet.rpt"
        '            '  reportPath = RutaReportes + "\ReporteNuevoCortesiaTv" + nuevo
        '            Dim cnn As New SqlConnection(MiConexion)

        '            Dim cmd As New SqlCommand("Reporte_Telcortesia_nuevo", cnn)
        '            cmd.CommandType = CommandType.StoredProcedure
        '            Dim da As New SqlDataAdapter(cmd)


        '            Dim parametro As New SqlParameter("@clv_Session", SqlDbType.BigInt)
        '            parametro.Direction = ParameterDirection.Input
        '            parametro.Value = LocClv_session
        '            cmd.Parameters.Add(parametro)

        '            Dim parametro1 As New SqlParameter("fecha1", SqlDbType.DateTime)
        '            parametro1.Direction = ParameterDirection.Input
        '            parametro1.Value = GloFecha_Ini
        '            cmd.Parameters.Add(parametro1)

        '            Dim parametro2 As New SqlParameter("@fecha2", SqlDbType.DateTime)
        '            parametro2.Direction = ParameterDirection.Input
        '            parametro2.Value = GloFecha_Fin
        '            cmd.Parameters.Add(parametro2)

        '            Dim parametro3 As New SqlParameter("@op", SqlDbType.Int)
        '            parametro3.Direction = ParameterDirection.Input
        '            parametro3.Value = GloClv_tipser2
        '            cmd.Parameters.Add(parametro3)

        '            Dim parametro4 As New SqlParameter("@Orden", SqlDbType.Int)
        '            parametro4.Direction = ParameterDirection.Input
        '            parametro4.Value = OpOrdenar
        '            cmd.Parameters.Add(parametro4)

        '            Dim parametro5 As New SqlParameter("@Habilita", SqlDbType.Int)
        '            parametro5.Direction = ParameterDirection.Input
        '            parametro5.Value = LocValidaHab
        '            cmd.Parameters.Add(parametro5)

        '            Dim parametro6 As New SqlParameter("@Periodo1", SqlDbType.Bit)
        '            parametro6.Direction = ParameterDirection.Input
        '            parametro6.Value = LocPeriodo1
        '            cmd.Parameters.Add(parametro6)

        '            Dim parametro7 As New SqlParameter("@periodo2", SqlDbType.Bit)
        '            parametro7.Direction = ParameterDirection.Input
        '            parametro7.Value = LocPeriodo2
        '            cmd.Parameters.Add(parametro7)

        '            Dim parametro8 As New SqlParameter("@nrep", SqlDbType.Int)
        '            parametro8.Direction = ParameterDirection.Input
        '            parametro8.Value = NCUAL
        '            cmd.Parameters.Add(parametro8)


        '            Dim ds As New DataSet()

        '            'Dim data1 As New DataTable()
        '            'Dim data2 As New DataTable()
        '            'Dim data3 As New DataTable()
        '            'Dim data4 As New DataTable()
        '            'Dim data5 As New DataTable()


        '            'Dim cmd2 As New SqlCommand("Select * from CALLES", cnn)
        '            'cmd2.CommandType = CommandType.Text
        '            'Dim da2 As New SqlDataAdapter(cmd2)

        '            'Dim cmd3 As New SqlCommand("Select * from CIUDADES", cnn)
        '            'cmd3.CommandType = CommandType.Text
        '            'Dim da3 As New SqlDataAdapter(cmd3)

        '            'Dim cmd4 As New SqlCommand("select * from CLIENTES", cnn)
        '            'cmd4.CommandType = CommandType.Text
        '            'Dim da4 As New SqlDataAdapter(cmd4)

        '            'Dim cmd5 As New SqlCommand("select * from COLONIAS", cnn)
        '            'cmd5.CommandType = CommandType.Text
        '            'Dim da5 As New SqlDataAdapter(cmd5)


        '            da.Fill(ds)
        '            'da2.Fill(data2)
        '            'da3.Fill(data3)
        '            'da4.Fill(data4)
        '            'da5.Fill(data5)
        '            If NCUAL = 1 Then
        '                ds.Tables(0).TableName = "Reporte_cortesia_nuevo"
        '                ds.Tables(1).TableName = "CALLES"
        '                ds.Tables(2).TableName = "CIUDADES"
        '                ds.Tables(3).TableName = "CLIENTES"
        '                ds.Tables(4).TableName = "COLONIAS"
        '            Else
        '                ds.Tables(0).TableName = "Reporte_cortesia_nuevo"
        '                ds.Tables(1).TableName = "CIUDADES"
        '                ds.Tables(2).TableName = "CLIENTES"
        '                ds.Tables(3).TableName = "COLONIAS"
        '            End If

        '            'ds.Tables.Add(data1)
        '            'ds.Tables.Add(data2)
        '            'ds.Tables.Add(data3)
        '            'ds.Tables.Add(data4)
        '            'ds.Tables.Add(data5)
        '            customersByCityReport.Load(reportPath)
        '            SetDBReport(ds, customersByCityReport)
        '            CrystalReportViewer1.ReportSource = customersByCityReport
        '    End Select
        '    If GloClv_tipser2 <> 5 Then

        '        customersByCityReport.Load(reportPath)
        '        SetDBLogonForReport(connectionInfo, customersByCityReport)




        '        '(@clv_Session bigint,@op int,@Orden int,@Habilita int,@periodo1 bit,@periodo2 bit)
        '        '@clv_Session bigint
        '        '@clv_session
        '        customersByCityReport.SetParameterValue(0, LocClv_session)
        '        '@fecha1 datetime,
        '        customersByCityReport.SetParameterValue(1, GloFecha_Ini)
        '        '@fecha2 datetime,
        '        customersByCityReport.SetParameterValue(2, GloFecha_Fin)
        '        ',@op int,
        '        customersByCityReport.SetParameterValue(3, GloClv_tipser2)
        '        '@Orden int
        '        customersByCityReport.SetParameterValue(4, OpOrdenar)
        '        '@Habilita int
        '        customersByCityReport.SetParameterValue(5, LocValidaHab)
        '        '@Periodo1 bit
        '        customersByCityReport.SetParameterValue(6, LocPeriodo1)
        '        '@periodo2 bit
        '        customersByCityReport.SetParameterValue(7, LocPeriodo2)



        '        If GloClv_tipser2 = 1 Then
        '            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
        '        ElseIf GloClv_tipser2 = 2 Or GloClv_tipser2 = 3 Then
        '            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
        '        End If



        '        CrystalReportViewer1.ReportSource = customersByCityReport
        '    End If

        '    CrystalReportViewer1.Zoom(75)

        '    Subformula = GloSucursal

        '    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & Subformula & "'"


        '    'SetDBLogonForReport2(connectionInfo)
        '    'SetDBLogonForReport(connectionInfo)
        '    locbandrep = True
        '    'SetDBLogonForReport(connectionInfo)
        '    customersByCityReport = Nothing
        'Catch ex As System.Exception
        '    System.Windows.Forms.MessageBox.Show(ex.Message)
        'End Try
    End Sub

    Private Sub ConfigureCrystalReportsEtiquetas(ByVal op As String)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"
            If Me.RadioButton1.Checked = True Then
                OpOrdenar = "1"
            ElseIf Me.RadioButton2.Checked = True Then
                OpOrdenar = "2"
            End If

            Dim reportPath As String = Nothing
            'reportPath = Application.StartupPath + "\Reportes\" + "RepEtiquetas.rpt"
            reportPath = RutaReportes + "\RepEtiquetas.rpt"

            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@clv_Session bigint,@op int,,,,@habilita int,,)  

            '@clv_Session bigint
            customersByCityReport.SetParameterValue(0, LocClv_session)
            '@op int
            customersByCityReport.SetParameterValue(1, GloClv_tipser2)
            '@op_rep  int
            customersByCityReport.SetParameterValue(2, (CInt(op) + 1))
            '@orden int
            customersByCityReport.SetParameterValue(3, OpOrdenar)
            '@clv_banco int
            customersByCityReport.SetParameterValue(4, GloSelBanco)
            '@habilita int
            customersByCityReport.SetParameterValue(5, LocValidaHab)
            '@periodo1 bit
            customersByCityReport.SetParameterValue(6, LocPeriodo1)
            '@periodo2 bit
            customersByCityReport.SetParameterValue(7, LocPeriodo2)




            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)
            SetDBLogonForReport2(connectionInfo)
            'SetDBLogonForReport(connectionInfo)

            'SetDBLogonForReport(connectionInfo)
            locbandrep = True
            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ConfigureCrystalReportNew4(ByVal op As String)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"
            If Me.RadioButton1.Checked = True Then
                OpOrdenar = "1"
            ElseIf Me.RadioButton2.Checked = True Then
                OpOrdenar = "2"
            End If

            Dim reportPath As String = Nothing
            'reportPath = Application.StartupPath + "\Reportes\" + "RepEtiquetas.rpt"
            If IdSistema = "SA" And (op = "3" Or op = "4" Or op = "1") Then
                reportPath = RutaReportes + "\ReporteRecordatoriosSA.rpt"
            Else
                reportPath = RutaReportes + "\ReporteRecordatorios.rpt"
            End If




            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            If IdSistema = "SA" And (op = "3" Or op = "4" Or op = "1") Then
                '@clv_Session bigint
                customersByCityReport.SetParameterValue(0, LocClv_session)
                '@op int
                customersByCityReport.SetParameterValue(1, GloClv_tipser2)
                '@op_rep  int
                customersByCityReport.SetParameterValue(2, (CInt(op) + 1))
                '@orden int
                customersByCityReport.SetParameterValue(3, OpOrdenar)
                '@clv_banco int
                customersByCityReport.SetParameterValue(4, GloSelBanco)
                '@habilita int
                customersByCityReport.SetParameterValue(5, LocValidaHab)
                '@periodo1 bit
                customersByCityReport.SetParameterValue(6, LocPeriodo1)
                '@periodo2 bit
                customersByCityReport.SetParameterValue(7, LocPeriodo2)
            Else
                '@clv_Session bigint
                customersByCityReport.SetParameterValue(0, LocClv_session)
                '@op int
                customersByCityReport.SetParameterValue(1, GloClv_tipser2)
                '@op_rep  int
                customersByCityReport.SetParameterValue(2, (CInt(op) + 1))
                '@orden int
                customersByCityReport.SetParameterValue(3, OpOrdenar)
                '@clv_banco int
                customersByCityReport.SetParameterValue(4, GloSelBanco)
                '@habilita int
                customersByCityReport.SetParameterValue(5, LocValidaHab)
                '@periodo1 bit
                customersByCityReport.SetParameterValue(6, LocPeriodo1)
                '@periodo2 bit
                customersByCityReport.SetParameterValue(7, LocPeriodo2)
                'clv_aviso
                customersByCityReport.SetParameterValue(8, LocClave_txt_aviso)


            End If


            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)
            SetDBLogonForReport2(connectionInfo)

            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & LocGloNomEmpresa & "'"

            customersByCityReport = Nothing
            locbandrep = True
            'Borra_Rel_Telefono_Report()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub ConfigureCrystalReportsServicios()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            GloClv_tipser2 = ComboBox4.SelectedValue
            Dim mySelectFormula As String = Nothing
            Dim Subformula As String = Nothing
            Dim OpOrdenar As String = "0"
            If Me.RadioButton1.Checked = True Then
                OpOrdenar = "1"
            ElseIf Me.RadioButton2.Checked = True Then
                OpOrdenar = "2"
            End If

            Dim reportPath As String = Nothing
            'If LocOp = 1 Then
            'reportPath = Application.StartupPath + "\Reportes\" + "ReporteServiciosTv.rpt"
            'mySelectFormula = "Reporte de Clientes por Servicio"
            'Subformula = "Servicio:" & LocDescr
            'ElseIf LocOp = 1 Then
            'reportPath = Application.StartupPath + "\Reportes\" + "ReporteServicios.rpt"
            'mySelectFormula = "Reporte de Clientes por Servicio"
            'Subformula = "Servicio: " & LocDescr
            'ElseIf LocOp = 2 Then
            'reportPath = Application.StartupPath + "\Reportes\" + "Reporte_TipoClientesTv.rpt"
            'mySelectFormula = "Resumen de Tipos de Clientes"
            'Subformula = "Tipo Cliente: " & LocDescr
            'ElseIf LocOp = 2 Then
            'reportPath = Application.StartupPath + "\Reportes\" + "Reporte_TipoClientes.rpt"
            'mySelectFormula = "Resumen de Tipos de Clientes"
            'Subformula = "Tipo Cliente: " & LocDescr
            'End If
            If LocOp = 1 And GloClv_tipser2 = 1 Then
                'reportPath = Application.StartupPath + "\Reportes\" + "ReporteServiciosTv.rpt"
                reportPath = RutaReportes.ToString + "\ReporteServiciosTv.rpt"
                mySelectFormula = "Reporte de Clientes por Servicio"
                Subformula = "Servicio:" & LocDescr
            ElseIf LocOp = 1 And GloClv_tipser2 <> 1 Then
                'reportPath = Application.StartupPath + "\Reportes\" + "ReporteServicios.rpt"
                reportPath = RutaReportes + "\ReporteServicios.rpt"
                mySelectFormula = "Reporte de Clientes por Servicio"
                Subformula = "Servicio: " & LocDescr
            ElseIf LocOp = 2 And GloClv_tipser2 = 1 Then
                'reportPath = Application.StartupPath + "\Reportes\" + "Reporte_TipoClientesTv.rpt"
                reportPath = RutaReportes + "\Reporte_TipoClientesTv.rpt"
                mySelectFormula = "Resumen de Tipos de Clientes"
                Subformula = "Tipo Cliente: " & LocDescr
            ElseIf LocOp = 2 And GloClv_tipser2 <> 1 Then
                'reportPath = Application.StartupPath + "\Reportes\" + "Reporte_TipoClientes.rpt"
                reportPath = RutaReportes + "\Reporte_TipoClientes.rpt"
                mySelectFormula = "Resumen de Tipos de Clientes"
                Subformula = "Tipo Cliente: " & LocDescr
            End If
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@clv_session
            customersByCityReport.SetParameterValue(0, LocClv_session)
            '@Op int
            customersByCityReport.SetParameterValue(1, GloClv_tipser2)
            ',@contratado bit
            customersByCityReport.SetParameterValue(2, LocBndC)
            ',@Baja bit
            customersByCityReport.SetParameterValue(3, LocBndB)
            ',@Insta bit
            customersByCityReport.SetParameterValue(4, LocBndI)
            '@Desconect bit
            customersByCityReport.SetParameterValue(5, LocBndD)
            '@Suspendido bit
            customersByCityReport.SetParameterValue(6, LocBndS)
            '@Fuera bit
            customersByCityReport.SetParameterValue(7, LocBndF)
            '@Orden
            customersByCityReport.SetParameterValue(8, CInt(OpOrdenar))



            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape



            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & Subformula & "'"

            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)

            customersByCityReport = Nothing
            locbandrep = True
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ConfigureCrystalReportsNew5(ByVal op As String)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim Contrataciones As Boolean = False
            Dim Instalaciones As Boolean = False
            Dim Fuera_Area As Boolean = False
            Dim Cancelaciones As Boolean = False
            Dim reportPath As String = Nothing

            Dim mySelectFormula As String = Titulo
            Dim Subformula As String = Nothing
            Dim OpOrdenar As String = "0"
            If Me.RadioButton1.Checked = True Then
                OpOrdenar = "1"
            ElseIf Me.RadioButton2.Checked = True Then
                OpOrdenar = "2"
            End If


            mySelectFormula = "Resumen de Clientes con Promocion:" + LocDescr4


            Select Case GloClv_tipser2
                Case 1
                    reportPath = RutaReportes + "\Reporte_Promocion.rpt"
                Case 2
                    reportPath = RutaReportes + "\Reporte_Promocion.rpt"
                Case 3
                    reportPath = RutaReportes + "\Reporte_PromocionDig.rpt"
            End Select
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@clv_Session bigint,@op int,@conectado bit,@baja bit,@Insta bit,@Desconect bit,@Susp bit,@Fuera bit,@orden int,@fecha_ini datetime,@fecha_fin datetime)  

            '@clv_session
            customersByCityReport.SetParameterValue(0, LocClv_session)
            '@Op int
            customersByCityReport.SetParameterValue(1, GloClv_tipser2)
            ',@contratado bit
            customersByCityReport.SetParameterValue(2, LocBndC)
            ',@Baja bit
            customersByCityReport.SetParameterValue(3, LocBndB)
            ',@Insta bit
            customersByCityReport.SetParameterValue(4, LocBndI)
            '@Desconect bit
            customersByCityReport.SetParameterValue(5, LocBndD)
            '@Suspendido bit
            customersByCityReport.SetParameterValue(6, LocBndS)
            '@Fuera bit
            customersByCityReport.SetParameterValue(7, LocBndF)
            '@Orden
            customersByCityReport.SetParameterValue(8, CInt(OpOrdenar))
            '@fecha_ini
            customersByCityReport.SetParameterValue(9, GloFecha_Ini)
            'fecha_fin
            customersByCityReport.SetParameterValue(10, GloFecha_Fin)
            '@Habilita int
            customersByCityReport.SetParameterValue(11, LocValidaHab)
            '@Periodo1 bit
            customersByCityReport.SetParameterValue(12, LocPeriodo1)
            '@periodo2 bit
            customersByCityReport.SetParameterValue(13, LocPeriodo2)






            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)

            Subformula = " Y  Con servicio: " + LocDescr2

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & Subformula & "'"

            locbandrep = True

            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ConfigureCrystalReportsNew6(ByVal op As String)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim Contrataciones As Boolean = False
            Dim Instalaciones As Boolean = False
            Dim Fuera_Area As Boolean = False
            Dim Cancelaciones As Boolean = False
            Dim reportPath As String = Nothing

            Dim mySelectFormula As String = Titulo
            Dim Subformula As String = Nothing


            If op = "1" Then
                mySelectFormula = "Tarjetas en Irdeto Asiganadas a un Cliente"
                reportPath = RutaReportes + "\ReporteArchivoIrdeto.rpt"
            ElseIf op = "2" Then
                mySelectFormula = "Tarjetas en Irdeto Sin asigar a un Cliente"
                reportPath = RutaReportes + "\ReporteArchivoIrdetoSin.rpt"
            End If
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)





            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)



            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"

            locbandrep2 = True

            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ConfigureCrystalReportsNew7(ByVal op As String)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If GloClv_tipser2 = 5 Then
                If oprepetiq = 0 Then
                    customersByCityReport = New ReportDocument

                    Dim Contrataciones As Boolean = False
                    Dim Instalaciones As Boolean = False
                    Dim Fuera_Area As Boolean = False
                    Dim Cancelaciones As Boolean = False
                    Dim reportPath As String = Nothing
                    Dim nuevo As String = Nothing

                    Dim mySelectFormula As String = Titulo
                    Dim Subformula As String = Nothing
                    Dim OpOrdenar As String = "0"
                    If Me.RadioButton1.Checked = True Then
                        'contrato
                        OpOrdenar = "1"
                        nuevo = "_2.rpt"
                    ElseIf Me.RadioButton2.Checked = True Then
                        'colonia y calle
                        OpOrdenar = "2"
                        nuevo = ".rpt"
                        'nuevo = "_2.rpt"
                    End If
                    If oprepetiq = 0 Then
                        If LocOp = 20 Then
                            If LocBndB = False Then
                                mySelectFormula = "Resumen de Clientes por Ciudad."
                                reportPath = RutaReportes + "\ReporteTelCiudad" + nuevo
                                Dim cnn As New SqlConnection(MiConexion)

                                Dim cmd As New SqlCommand("Reporte_TelTiposCliente_ciudad", cnn)
                                cmd.CommandType = CommandType.StoredProcedure
                                cmd.CommandTimeout = 0
                                Dim parametro1 As New SqlParameter("@clv_Session", SqlDbType.BigInt)
                                parametro1.Direction = ParameterDirection.Input
                                parametro1.Value = LocClv_session
                                cmd.Parameters.Add(parametro1)

                                Dim parametro2 As New SqlParameter("@op", SqlDbType.Int)
                                parametro2.Direction = ParameterDirection.Input
                                parametro2.Value = GloClv_tipser2
                                cmd.Parameters.Add(parametro2)

                                Dim parametro3 As New SqlParameter("@conectado", SqlDbType.Bit)
                                parametro3.Direction = ParameterDirection.Input
                                parametro3.Value = LocBndC
                                cmd.Parameters.Add(parametro3)

                                Dim parametro4 As New SqlParameter("@baja", SqlDbType.Bit)
                                parametro4.Direction = ParameterDirection.Input
                                parametro4.Value = LocBndB
                                cmd.Parameters.Add(parametro4)

                                Dim parametro5 As New SqlParameter("@Insta", SqlDbType.Bit)
                                parametro5.Direction = ParameterDirection.Input
                                parametro5.Value = LocBndI
                                cmd.Parameters.Add(parametro5)

                                Dim parametro6 As New SqlParameter("@Desconect", SqlDbType.Bit)
                                parametro6.Direction = ParameterDirection.Input
                                parametro6.Value = LocBndD
                                cmd.Parameters.Add(parametro6)

                                Dim parametro7 As New SqlParameter("@Susp", SqlDbType.Bit)
                                parametro7.Direction = ParameterDirection.Input
                                parametro7.Value = LocBndS
                                cmd.Parameters.Add(parametro7)

                                Dim parametro8 As New SqlParameter("@Fuera ", SqlDbType.Bit)
                                parametro8.Direction = ParameterDirection.Input
                                parametro8.Value = LocBndF
                                cmd.Parameters.Add(parametro8)
                                Dim parametro10 As New SqlParameter("@DescTmp", SqlDbType.Bit)
                                parametro10.Direction = ParameterDirection.Input
                                parametro10.Value = LocBndDT
                                cmd.Parameters.Add(parametro10)

                                Dim parametro9 As New SqlParameter("@orden", SqlDbType.Int)
                                parametro9.Direction = ParameterDirection.Input
                                parametro9.Value = CInt(OpOrdenar)
                                cmd.Parameters.Add(parametro9)

                                Dim parametro12 As New SqlParameter("@Habilita", SqlDbType.Int)
                                parametro12.Direction = ParameterDirection.Input
                                parametro12.Value = LocValidaHab
                                cmd.Parameters.Add(parametro12)

                                Dim parametro13 As New SqlParameter("@periodo1", SqlDbType.Bit)
                                parametro13.Direction = ParameterDirection.Input
                                parametro13.Value = LocPeriodo1
                                cmd.Parameters.Add(parametro13)

                                Dim parametro14 As New SqlParameter("@periodo2", SqlDbType.Bit)
                                parametro14.Direction = ParameterDirection.Input
                                parametro14.Value = LocPeriodo2
                                cmd.Parameters.Add(parametro14)

                                Dim parametro15 As New SqlParameter("@nrep", SqlDbType.Int)
                                parametro15.Direction = ParameterDirection.Input
                                parametro15.Value = 0
                                cmd.Parameters.Add(parametro15)

                                Dim parametro16 As New SqlParameter("@Correo", SqlDbType.Int)
                                parametro16.Direction = ParameterDirection.Input
                                parametro16.Value = GloCorreo
                                cmd.Parameters.Add(parametro16)

                                Dim da As New SqlDataAdapter(cmd)

                                'Dim data1 As New DataTable()
                                'Dim data2 As New DataTable()
                                'Dim data3 As New DataTable()
                                'Dim data4 As New DataTable()
                                'Dim data5 As New DataTable()


                                'Dim cmd1 As New SqlCommand("select * from CALLES", cnn)
                                'cmd1.CommandType = CommandType.Text
                                'Dim da2 As New SqlDataAdapter(cmd1)

                                'Dim cmd2 As New SqlCommand("select * from CIUDADES", cnn)
                                'cmd2.CommandType = CommandType.Text
                                'Dim da3 As New SqlDataAdapter(cmd2)

                                'Dim cmd3 As New SqlCommand("select * from CLIENTES", cnn)
                                'cmd3.CommandType = CommandType.Text
                                'Dim da4 As New SqlDataAdapter(cmd3)

                                'Dim cmd4 As New SqlCommand("select * from COLONIAS", cnn)
                                'cmd3.CommandType = CommandType.Text
                                'Dim da5 As New SqlDataAdapter(cmd4)


                                Dim ds As New DataSet()
                                da.Fill(ds)
                                ds.Tables(0).TableName = "Reporte_TiposCliente_ciudad"
                                ds.Tables(1).TableName = "CALLES"
                                ds.Tables(2).TableName = "CIUDADES"
                                ds.Tables(3).TableName = "CLIENTES"
                                ds.Tables(4).TableName = "COLONIAS"

                                If ds.Tables(0).Rows.Count > 0 Or ds.Tables(0).Rows.Count = 0 Then
                                    Dim colstring As DataColumn = New DataColumn("op")
                                    colstring.DataType = System.Type.GetType("System.Int32")
                                    ds.Tables(0).Columns.Add(colstring)
                                    Dim colstring4 As DataColumn = New DataColumn("conectado")
                                    colstring4.DataType = System.Type.GetType("System.Byte")
                                    ds.Tables(0).Columns.Add(colstring4)

                                    Dim colstring1 As DataColumn = New DataColumn("baja")
                                    colstring1.DataType = System.Type.GetType("System.Byte")
                                    ds.Tables(0).Columns.Add(colstring1)
                                    Dim colstring2 As DataColumn = New DataColumn("Insta")
                                    colstring2.DataType = System.Type.GetType("System.Byte")
                                    ds.Tables(0).Columns.Add(colstring2)
                                    Dim colstring5 As DataColumn = New DataColumn("Desconect")
                                    colstring5.DataType = System.Type.GetType("System.Byte")
                                    ds.Tables(0).Columns.Add(colstring5)
                                    Dim colstring8 As DataColumn = New DataColumn("Susp")
                                    colstring8.DataType = System.Type.GetType("System.Byte")
                                    ds.Tables(0).Columns.Add(colstring8)
                                    Dim colstring7 As DataColumn = New DataColumn("Fuera")
                                    colstring7.DataType = System.Type.GetType("System.Byte")
                                    ds.Tables(0).Columns.Add(colstring7)
                                    Dim colstring9 As DataColumn = New DataColumn("DescTmp")
                                    colstring9.DataType = System.Type.GetType("System.Byte")
                                    ds.Tables(0).Columns.Add(colstring9)

                                    For Each dr As DataRow In ds.Tables(0).Rows
                                        dr("op") = GloClv_tipser2
                                        dr("conectado") = 0
                                        dr("baja") = LocBndB
                                        dr("insta") = LocBndI
                                        dr("Desconect") = LocBndD
                                        dr("Susp") = LocBndS
                                        dr("Fuera") = LocBndF
                                        dr("Desctmp") = LocBndDT

                                    Next


                                End If


                                'da2.Fill(data2)
                                'da3.Fill(data3)
                                'da4.Fill(data4)
                                'da5.Fill(data5)



                                'ds.Tables.Add(data1)
                                'ds.Tables.Add(data2)
                                'ds.Tables.Add(data3)
                                'ds.Tables.Add(data4)
                                'ds.Tables.Add(data5)

                                customersByCityReport.Load(reportPath)
                                ' SetDBReport(ds, customersByCityReport)
                                SetDBReport(ds, customersByCityReport)
                                CrystalReportViewer1.ReportSource = customersByCityReport


                            ElseIf LocBndB = True Then
                                mySelectFormula = "Resumen de Clientes por Ciudad."
                                reportPath = RutaReportes + "\ReporteTelCiudad2" + nuevo
                                Dim cnn As New SqlConnection(MiConexion)

                                Dim cmd As New SqlCommand("Reporte_TelTiposCliente_Ciudad", cnn)
                                cmd.CommandType = CommandType.StoredProcedure
                                cmd.CommandTimeout = 0

                                Dim parametro1 As New SqlParameter("@clv_Session", SqlDbType.BigInt)
                                parametro1.Direction = ParameterDirection.Input
                                parametro1.Value = LocClv_session
                                cmd.Parameters.Add(parametro1)

                                Dim parametro2 As New SqlParameter("@op", SqlDbType.Int)
                                parametro2.Direction = ParameterDirection.Input
                                parametro2.Value = GloClv_tipser2
                                cmd.Parameters.Add(parametro2)

                                Dim parametro3 As New SqlParameter("@conectado", SqlDbType.Bit)
                                parametro3.Direction = ParameterDirection.Input
                                parametro3.Value = LocBndC
                                cmd.Parameters.Add(parametro3)

                                Dim parametro4 As New SqlParameter("@baja", SqlDbType.Bit)
                                parametro4.Direction = ParameterDirection.Input
                                parametro4.Value = LocBndB
                                cmd.Parameters.Add(parametro4)

                                Dim parametro5 As New SqlParameter("@Insta", SqlDbType.Bit)
                                parametro5.Direction = ParameterDirection.Input
                                parametro5.Value = LocBndI
                                cmd.Parameters.Add(parametro5)

                                Dim parametro6 As New SqlParameter("@Desconect", SqlDbType.Bit)
                                parametro6.Direction = ParameterDirection.Input
                                parametro6.Value = LocBndD
                                cmd.Parameters.Add(parametro6)

                                Dim parametro7 As New SqlParameter("@Susp", SqlDbType.Bit)
                                parametro7.Direction = ParameterDirection.Input
                                parametro7.Value = LocBndS
                                cmd.Parameters.Add(parametro7)

                                Dim parametro8 As New SqlParameter("@Fuera ", SqlDbType.Bit)
                                parametro8.Direction = ParameterDirection.Input
                                parametro8.Value = LocBndF
                                cmd.Parameters.Add(parametro8)
                                Dim parametro10 As New SqlParameter("@DescTmp", SqlDbType.Bit)
                                parametro10.Direction = ParameterDirection.Input
                                parametro10.Value = LocBndDT
                                cmd.Parameters.Add(parametro10)

                                Dim parametro9 As New SqlParameter("@orden", SqlDbType.Int)
                                parametro9.Direction = ParameterDirection.Input
                                parametro9.Value = CInt(OpOrdenar)
                                cmd.Parameters.Add(parametro9)

                                Dim parametro12 As New SqlParameter("@Habilita", SqlDbType.Int)
                                parametro12.Direction = ParameterDirection.Input
                                parametro12.Value = LocValidaHab
                                cmd.Parameters.Add(parametro12)

                                Dim parametro13 As New SqlParameter("@periodo1", SqlDbType.Bit)
                                parametro13.Direction = ParameterDirection.Input
                                parametro13.Value = LocPeriodo1
                                cmd.Parameters.Add(parametro13)

                                Dim parametro14 As New SqlParameter("@periodo2", SqlDbType.Bit)
                                parametro14.Direction = ParameterDirection.Input
                                parametro14.Value = LocPeriodo2
                                cmd.Parameters.Add(parametro14)

                                Dim parametro15 As New SqlParameter("@nrep", SqlDbType.Int)
                                parametro15.Direction = ParameterDirection.Input
                                parametro15.Value = 0
                                cmd.Parameters.Add(parametro15)

                                Dim parametro16 As New SqlParameter("@Correo", SqlDbType.Int)
                                parametro16.Direction = ParameterDirection.Input
                                parametro16.Value = GloCorreo
                                cmd.Parameters.Add(parametro16)

                                Dim da As New SqlDataAdapter(cmd)

                                'Dim data1 As New DataTable()
                                'Dim data2 As New DataTable()
                                'Dim data3 As New DataTable()
                                'Dim data4 As New DataTable()
                                'Dim data5 As New DataTable()


                                'Dim cmd1 As New SqlCommand("select * from CALLES", cnn)
                                'cmd1.CommandType = CommandType.Text
                                'Dim da2 As New SqlDataAdapter(cmd1)

                                'Dim cmd2 As New SqlCommand("select * from CIUDADES", cnn)
                                'cmd2.CommandType = CommandType.Text
                                'Dim da3 As New SqlDataAdapter(cmd2)

                                'Dim cmd3 As New SqlCommand("select * from CLIENTES", cnn)
                                'cmd3.CommandType = CommandType.Text
                                'Dim da4 As New SqlDataAdapter(cmd3)

                                'Dim cmd4 As New SqlCommand("select * from COLONIAS", cnn)
                                'cmd3.CommandType = CommandType.Text
                                'Dim da5 As New SqlDataAdapter(cmd4)


                                Dim ds As New DataSet()
                                da.Fill(ds)
                                ds.Tables(0).TableName = "Reporte_TiposCliente_ciudad"
                                ds.Tables(1).TableName = "CALLES"
                                ds.Tables(2).TableName = "CIUDADES"
                                ds.Tables(3).TableName = "CLIENTES"
                                ds.Tables(4).TableName = "COLONIAS"

                                If ds.Tables(0).Rows.Count > 0 Or ds.Tables(0).Rows.Count = 0 Then
                                    Dim colstring As DataColumn = New DataColumn("op")
                                    colstring.DataType = System.Type.GetType("System.Int32")
                                    ds.Tables(0).Columns.Add(colstring)
                                    Dim colstring4 As DataColumn = New DataColumn("conectado")
                                    colstring4.DataType = System.Type.GetType("System.Byte")
                                    ds.Tables(0).Columns.Add(colstring4)

                                    Dim colstring1 As DataColumn = New DataColumn("baja")
                                    colstring1.DataType = System.Type.GetType("System.Byte")
                                    ds.Tables(0).Columns.Add(colstring1)
                                    Dim colstring2 As DataColumn = New DataColumn("Insta")
                                    colstring2.DataType = System.Type.GetType("System.Byte")
                                    ds.Tables(0).Columns.Add(colstring2)
                                    Dim colstring5 As DataColumn = New DataColumn("Desconect")
                                    colstring5.DataType = System.Type.GetType("System.Byte")
                                    ds.Tables(0).Columns.Add(colstring5)
                                    Dim colstring8 As DataColumn = New DataColumn("Susp")
                                    colstring8.DataType = System.Type.GetType("System.Byte")
                                    ds.Tables(0).Columns.Add(colstring8)
                                    Dim colstring7 As DataColumn = New DataColumn("Fuera")
                                    colstring7.DataType = System.Type.GetType("System.Byte")
                                    ds.Tables(0).Columns.Add(colstring7)
                                    Dim colstring9 As DataColumn = New DataColumn("DescTmp")
                                    colstring9.DataType = System.Type.GetType("System.Byte")
                                    ds.Tables(0).Columns.Add(colstring9)

                                    For Each dr As DataRow In ds.Tables(0).Rows
                                        dr("op") = GloClv_tipser2
                                        dr("conectado") = 0
                                        dr("baja") = LocBndB
                                        dr("insta") = LocBndI
                                        dr("Desconect") = LocBndD
                                        dr("Susp") = LocBndS
                                        dr("Fuera") = LocBndF
                                        dr("Desctmp") = LocBndDT

                                    Next
                                    'Dim colstring As DataColumn = New DataColumn("op")
                                    'colstring.DataType = System.Type.GetType("System.Int32")
                                    'data1.Columns.Add(colstring)

                                    'For Each dr As DataRow In data1.Rows
                                    '    dr("op") = GloClv_tipser2
                                    'Next
                                    ''data1.rows(0)("op")=GloClv_tipser2

                                    'Dim colstring1 As DataColumn = New DataColumn("baja")
                                    'colstring1.DataType = System.Type.GetType("System.Byte")
                                    'data1.Columns.Add(colstring1)

                                    'For Each dr As DataRow In data1.Rows
                                    '    dr("baja") = LocBndB
                                    'Next

                                    'Dim colstring2 As DataColumn = New DataColumn("insta")
                                    'colstring2.DataType = System.Type.GetType("System.Byte")
                                    'data1.Columns.Add(colstring2)

                                    'For Each dr As DataRow In data1.Rows
                                    '    dr("insta") = LocBndI
                                    'Next

                                    'Dim colstring3 As DataColumn = New DataColumn("validacion")
                                    'colstring3.DataType = System.Type.GetType("System.Byte")
                                    'data1.Columns.Add(colstring3)

                                    'For Each dr As DataRow In data1.Rows
                                    '    dr("validacion") = 0
                                    'Next


                                    'Dim colstring4 As DataColumn = New DataColumn("conectado")
                                    'colstring4.DataType = System.Type.GetType("System.Byte")
                                    'data1.Columns.Add(colstring4)
                                    'For Each dr As DataRow In data1.Rows
                                    '    dr("conectado") = 0
                                    'Next


                                    'Dim colstring5 As DataColumn = New DataColumn("desconect")
                                    'colstring5.DataType = System.Type.GetType("System.Byte")
                                    'data1.Columns.Add(colstring5)
                                    'For Each dr As DataRow In data1.Rows
                                    '    dr("desconect") = LocBndS
                                    'Next

                                    'Dim colstring6 As DataColumn = New DataColumn("desctmp")
                                    'colstring6.DataType = System.Type.GetType("System.byte")
                                    'data1.Columns.Add(colstring6)
                                    'For Each dr As DataRow In data1.Rows
                                    '    dr("desctmp") = 0
                                    'Next


                                    'Dim colstring7 As DataColumn = New DataColumn("fuera")
                                    'colstring7.DataType = System.Type.GetType("System.Byte")
                                    'data1.Columns.Add(colstring7)
                                    'For Each dr As DataRow In data1.Rows
                                    '    dr("fuera") = LocBndDT
                                    'Next

                                End If


                                'da2.Fill(data2)
                                'da3.Fill(data3)
                                'da4.Fill(data4)
                                'da5.Fill(data5)



                                'ds.Tables.Add(data1)
                                'ds.Tables.Add(data2)
                                'ds.Tables.Add(data3)
                                'ds.Tables.Add(data4)
                                'ds.Tables.Add(data5)

                                customersByCityReport.Load(reportPath)
                                SetDBReport(ds, customersByCityReport)
                                'ds.WriteXml("mixml.xml")
                                CrystalReportViewer1.ReportSource = customersByCityReport

                            End If
                            'mySelectFormula = "Resumen de Clientes por Ciudad."
                            'reportPath = RutaReportes + "\ReporteCiudad.rpt"
                        ElseIf LocOp = 25 Then
                            mySelectFormula = "Listado de Clientes Por Colonia y Status."
                            reportPath = RutaReportes + "\Reporte_TelResumen_Por_Colonia.rpt"
                            Dim cnn As New SqlConnection(MiConexion)
                            Dim cmd As New SqlCommand("Reporte_TelTiposCliente_Ciudad_Resumen", cnn)
                            cmd.CommandType = CommandType.StoredProcedure
                            cmd.CommandTimeout = 0
                            Dim parametro1 As New SqlParameter("@clv_Session", SqlDbType.BigInt)
                            parametro1.Direction = ParameterDirection.Input
                            parametro1.Value = LocClv_session
                            cmd.Parameters.Add(parametro1)

                            Dim parametro2 As New SqlParameter("@op", SqlDbType.Int)
                            parametro2.Direction = ParameterDirection.Input
                            parametro2.Value = GloClv_tipser2
                            cmd.Parameters.Add(parametro2)

                            Dim parametro3 As New SqlParameter("@conectado", SqlDbType.Bit)
                            parametro3.Direction = ParameterDirection.Input
                            parametro3.Value = LocBndC
                            cmd.Parameters.Add(parametro3)

                            Dim parametro4 As New SqlParameter("@baja", SqlDbType.Bit)
                            parametro4.Direction = ParameterDirection.Input
                            parametro4.Value = LocBndB
                            cmd.Parameters.Add(parametro4)

                            Dim parametro5 As New SqlParameter("@Insta", SqlDbType.Bit)
                            parametro5.Direction = ParameterDirection.Input
                            parametro5.Value = LocBndI
                            cmd.Parameters.Add(parametro5)

                            Dim parametro6 As New SqlParameter("@Desconect", SqlDbType.Bit)
                            parametro6.Direction = ParameterDirection.Input
                            parametro6.Value = LocBndD
                            cmd.Parameters.Add(parametro6)

                            Dim parametro7 As New SqlParameter("@Susp", SqlDbType.Bit)
                            parametro7.Direction = ParameterDirection.Input
                            parametro7.Value = LocBndS
                            cmd.Parameters.Add(parametro7)

                            Dim parametro8 As New SqlParameter("@Fuera ", SqlDbType.Bit)
                            parametro8.Direction = ParameterDirection.Input
                            parametro8.Value = LocBndF
                            cmd.Parameters.Add(parametro8)

                            Dim parametro15 As New SqlParameter("@DescTmp", SqlDbType.Bit)
                            parametro15.Direction = ParameterDirection.Input
                            parametro15.Value = LocBndDT

                            cmd.Parameters.Add(parametro15)

                            Dim parametro9 As New SqlParameter("@orden", SqlDbType.Int)
                            parametro9.Direction = ParameterDirection.Input
                            parametro9.Value = CInt(OpOrdenar)
                            cmd.Parameters.Add(parametro9)


                            'Dim parametro10 As New SqlParameter("@fecha_ini", SqlDbType.DateTime)
                            'parametro10.Direction = ParameterDirection.Input
                            'parametro10.Value = LocPeriodo1
                            'cmd.Parameters.Add(parametro10)

                            'Dim parametro11 As New SqlParameter("@fecha_fin", SqlDbType.DateTime)
                            'parametro11.Direction = ParameterDirection.Input
                            'parametro11.Value = LocPeriodo2
                            'cmd.Parameters.Add(parametro11)

                            Dim parametro12 As New SqlParameter("@Habilita", SqlDbType.Int)
                            parametro12.Direction = ParameterDirection.Input
                            parametro12.Value = LocValidaHab
                            cmd.Parameters.Add(parametro12)

                            Dim parametro13 As New SqlParameter("@periodo1", SqlDbType.Bit)
                            parametro13.Direction = ParameterDirection.Input
                            parametro13.Value = LocPeriodo1
                            cmd.Parameters.Add(parametro13)

                            Dim parametro14 As New SqlParameter("@periodo2", SqlDbType.Bit)
                            parametro14.Direction = ParameterDirection.Input
                            parametro14.Value = LocPeriodo2
                            cmd.Parameters.Add(parametro14)

                            Dim da As New SqlDataAdapter(cmd)
                            Dim data1 As New DataTable()
                            Dim ds As New DataSet()

                            da.Fill(ds)
                            ds.Tables(0).TableName = "Reporte_TiposCliente_Ciudad_Resumen"
                            If data1.Rows.Count > 0 Or data1.Rows.Count = 0 Then
                                Dim colstring As DataColumn = New DataColumn("op")
                                colstring.DataType = System.Type.GetType("System.Int32")
                                ds.Tables(0).Columns.Add(colstring)
                                Dim colstring4 As DataColumn = New DataColumn("conectado")
                                colstring4.DataType = System.Type.GetType("System.Byte")
                                ds.Tables(0).Columns.Add(colstring4)

                                Dim colstring1 As DataColumn = New DataColumn("baja")
                                colstring1.DataType = System.Type.GetType("System.Byte")
                                ds.Tables(0).Columns.Add(colstring1)
                                Dim colstring2 As DataColumn = New DataColumn("Insta")
                                colstring2.DataType = System.Type.GetType("System.Byte")
                                ds.Tables(0).Columns.Add(colstring2)
                                Dim colstring5 As DataColumn = New DataColumn("Desconect")
                                colstring5.DataType = System.Type.GetType("System.Byte")
                                ds.Tables(0).Columns.Add(colstring5)
                                Dim colstring8 As DataColumn = New DataColumn("Susp")
                                colstring8.DataType = System.Type.GetType("System.Byte")
                                ds.Tables(0).Columns.Add(colstring8)
                                Dim colstring7 As DataColumn = New DataColumn("Fuera")
                                colstring7.DataType = System.Type.GetType("System.Byte")
                                ds.Tables(0).Columns.Add(colstring7)
                                Dim colstring9 As DataColumn = New DataColumn("DescTmp")
                                colstring9.DataType = System.Type.GetType("System.Byte")
                                ds.Tables(0).Columns.Add(colstring9)

                                For Each dr As DataRow In ds.Tables(0).Rows
                                    dr("op") = GloClv_tipser2
                                    dr("conectado") = 0
                                    dr("baja") = LocBndB
                                    dr("insta") = LocBndI
                                    dr("Desconect") = LocBndD
                                    dr("Susp") = LocBndS
                                    dr("Fuera") = LocBndF
                                    dr("Desctmp") = LocBndDT

                                Next

                            End If

                            customersByCityReport.Load(reportPath)
                            'SetDBReport(ds, customersByCityReport)
                            SetDBReport(ds, customersByCityReport)


                            CrystalReportViewer1.ReportSource = customersByCityReport
                        Else
                            mySelectFormula = "Resumen de Clientes por Ciudad."
                            reportPath = RutaReportes + "\ReporteTelCiudad" + nuevo
                            Dim cnn As New SqlConnection(MiConexion)

                            Dim cmd As New SqlCommand("Reporte_TelTiposCliente_ciudad", cnn)
                            cmd.CommandType = CommandType.StoredProcedure
                            cmd.CommandTimeout = 0

                            Dim parametro1 As New SqlParameter("@clv_Session", SqlDbType.BigInt)
                            parametro1.Direction = ParameterDirection.Input
                            parametro1.Value = LocClv_session
                            cmd.Parameters.Add(parametro1)

                            Dim parametro2 As New SqlParameter("@op", SqlDbType.Int)
                            parametro2.Direction = ParameterDirection.Input
                            parametro2.Value = GloClv_tipser2
                            cmd.Parameters.Add(parametro2)

                            Dim parametro3 As New SqlParameter("@conectado", SqlDbType.Bit)
                            parametro3.Direction = ParameterDirection.Input
                            parametro3.Value = LocBndC
                            cmd.Parameters.Add(parametro3)

                            Dim parametro4 As New SqlParameter("@baja", SqlDbType.Bit)
                            parametro4.Direction = ParameterDirection.Input
                            parametro4.Value = LocBndB
                            cmd.Parameters.Add(parametro4)

                            Dim parametro5 As New SqlParameter("@Insta", SqlDbType.Bit)
                            parametro5.Direction = ParameterDirection.Input
                            parametro5.Value = LocBndI
                            cmd.Parameters.Add(parametro5)

                            Dim parametro6 As New SqlParameter("@Desconect", SqlDbType.Bit)
                            parametro6.Direction = ParameterDirection.Input
                            parametro6.Value = LocBndD
                            cmd.Parameters.Add(parametro6)

                            Dim parametro7 As New SqlParameter("@Susp", SqlDbType.Bit)
                            parametro7.Direction = ParameterDirection.Input
                            parametro7.Value = LocBndS
                            cmd.Parameters.Add(parametro7)

                            Dim parametro8 As New SqlParameter("@Fuera ", SqlDbType.Bit)
                            parametro8.Direction = ParameterDirection.Input
                            parametro8.Value = LocBndF
                            cmd.Parameters.Add(parametro8)
                            Dim parametro10 As New SqlParameter("@DescTmp", SqlDbType.Bit)
                            parametro10.Direction = ParameterDirection.Input
                            parametro10.Value = LocBndDT

                            cmd.Parameters.Add(parametro10)

                            Dim parametro9 As New SqlParameter("@orden", SqlDbType.Int)
                            parametro9.Direction = ParameterDirection.Input
                            parametro9.Value = CInt(OpOrdenar)
                            cmd.Parameters.Add(parametro9)

                            Dim parametro12 As New SqlParameter("@Habilita", SqlDbType.Int)
                            parametro12.Direction = ParameterDirection.Input
                            parametro12.Value = LocValidaHab
                            cmd.Parameters.Add(parametro12)

                            Dim parametro13 As New SqlParameter("@periodo1", SqlDbType.Bit)
                            parametro13.Direction = ParameterDirection.Input
                            parametro13.Value = 0
                            cmd.Parameters.Add(parametro13)

                            Dim parametro14 As New SqlParameter("@periodo2", SqlDbType.Bit)
                            parametro14.Direction = ParameterDirection.Input
                            parametro14.Value = 0
                            cmd.Parameters.Add(parametro14)

                            Dim parametro15 As New SqlParameter("@nrep", SqlDbType.Int)
                            parametro15.Direction = ParameterDirection.Input
                            parametro15.Value = 0
                            cmd.Parameters.Add(parametro15)

                            Dim parametro16 As New SqlParameter("@Correo", SqlDbType.Int)
                            parametro16.Direction = ParameterDirection.Input
                            parametro16.Value = GloCorreo
                            cmd.Parameters.Add(parametro16)

                            Dim da As New SqlDataAdapter(cmd)

                            'Dim data1 As New DataTable()
                            'Dim data2 As New DataTable()
                            'Dim data3 As New DataTable()
                            'Dim data4 As New DataTable()
                            'Dim data5 As New DataTable()


                            'Dim cmd1 As New SqlCommand("select * from CALLES", cnn)
                            'cmd1.CommandType = CommandType.Text
                            'Dim da2 As New SqlDataAdapter(cmd1)

                            'Dim cmd2 As New SqlCommand("select * from CIUDADES", cnn)
                            'cmd2.CommandType = CommandType.Text
                            'Dim da3 As New SqlDataAdapter(cmd2)

                            'Dim cmd3 As New SqlCommand("select * from CLIENTES", cnn)
                            'cmd3.CommandType = CommandType.Text
                            'Dim da4 As New SqlDataAdapter(cmd3)

                            'Dim cmd4 As New SqlCommand("select * from COLONIAS", cnn)
                            'cmd3.CommandType = CommandType.Text
                            'Dim da5 As New SqlDataAdapter(cmd4)


                            Dim ds As New DataSet()
                            da.Fill(ds)
                            ds.Tables(0).TableName = "Reporte_TiposCliente_ciudad"
                            ds.Tables(1).TableName = "CALLES"
                            ds.Tables(2).TableName = "CIUDADES"
                            ds.Tables(3).TableName = "CLIENTES"
                            ds.Tables(4).TableName = "COLONIAS"

                            If ds.Tables(0).Rows.Count > 0 Or ds.Tables(0).Rows.Count = 0 Then
                                Dim colstring As DataColumn = New DataColumn("op")
                                colstring.DataType = System.Type.GetType("System.Int32")
                                ds.Tables(0).Columns.Add(colstring)
                                Dim colstring4 As DataColumn = New DataColumn("conectado")
                                colstring4.DataType = System.Type.GetType("System.Byte")
                                ds.Tables(0).Columns.Add(colstring4)

                                Dim colstring1 As DataColumn = New DataColumn("baja")
                                colstring1.DataType = System.Type.GetType("System.Byte")
                                ds.Tables(0).Columns.Add(colstring1)
                                Dim colstring2 As DataColumn = New DataColumn("Insta")
                                colstring2.DataType = System.Type.GetType("System.Byte")
                                ds.Tables(0).Columns.Add(colstring2)
                                Dim colstring5 As DataColumn = New DataColumn("Desconect")
                                colstring5.DataType = System.Type.GetType("System.Byte")
                                ds.Tables(0).Columns.Add(colstring5)
                                Dim colstring8 As DataColumn = New DataColumn("Susp")
                                colstring8.DataType = System.Type.GetType("System.Byte")
                                ds.Tables(0).Columns.Add(colstring8)
                                Dim colstring7 As DataColumn = New DataColumn("Fuera")
                                colstring7.DataType = System.Type.GetType("System.Byte")
                                ds.Tables(0).Columns.Add(colstring7)
                                Dim colstring9 As DataColumn = New DataColumn("DescTmp")
                                colstring9.DataType = System.Type.GetType("System.Byte")
                                ds.Tables(0).Columns.Add(colstring9)

                                For Each dr As DataRow In ds.Tables(0).Rows
                                    dr("op") = GloClv_tipser2
                                    dr("conectado") = 0
                                    dr("baja") = LocBndB
                                    dr("insta") = LocBndI
                                    dr("Desconect") = LocBndD
                                    dr("Susp") = LocBndS
                                    dr("Fuera") = LocBndF
                                    dr("Desctmp") = LocBndDT

                                Next
                                'Dim colstring As DataColumn = New DataColumn("op")
                                'colstring.DataType = System.Type.GetType("System.Int32")
                                'data1.Columns.Add(colstring)

                                'For Each dr As DataRow In data1.Rows
                                '    dr("op") = GloClv_tipser2
                                'Next
                                ''data1.rows(0)("op")=GloClv_tipser2

                                'Dim colstring1 As DataColumn = New DataColumn("baja")
                                'colstring1.DataType = System.Type.GetType("System.Byte")
                                'data1.Columns.Add(colstring1)

                                'For Each dr As DataRow In data1.Rows
                                '    dr("baja") = LocBndB
                                'Next

                                'Dim colstring2 As DataColumn = New DataColumn("insta")
                                'colstring2.DataType = System.Type.GetType("System.Byte")
                                'data1.Columns.Add(colstring2)

                                'For Each dr As DataRow In data1.Rows
                                '    dr("insta") = LocBndI
                                'Next

                                'Dim colstring3 As DataColumn = New DataColumn("validacion")
                                'colstring3.DataType = System.Type.GetType("System.Byte")
                                'data1.Columns.Add(colstring3)

                                'For Each dr As DataRow In data1.Rows
                                '    dr("validacion") = 0
                                'Next


                                'Dim colstring4 As DataColumn = New DataColumn("conectado")
                                'colstring4.DataType = System.Type.GetType("System.Byte")
                                'data1.Columns.Add(colstring4)
                                'For Each dr As DataRow In data1.Rows
                                '    dr("conectado") = 0
                                'Next


                                'Dim colstring5 As DataColumn = New DataColumn("desconect")
                                'colstring5.DataType = System.Type.GetType("System.Byte")
                                'data1.Columns.Add(colstring5)
                                'For Each dr As DataRow In data1.Rows
                                '    dr("desconect") = LocBndS
                                'Next

                                'Dim colstring6 As DataColumn = New DataColumn("desctmp")
                                'colstring6.DataType = System.Type.GetType("System.byte")
                                'data1.Columns.Add(colstring6)
                                'For Each dr As DataRow In data1.Rows
                                '    dr("desctmp") = 0
                                'Next


                                'Dim colstring7 As DataColumn = New DataColumn("fuera")
                                'colstring7.DataType = System.Type.GetType("System.Byte")
                                'data1.Columns.Add(colstring7)
                                'For Each dr As DataRow In data1.Rows
                                '    dr("fuera") = LocBndDT
                                'Next

                            End If


                            'da2.Fill(data2)
                            'da3.Fill(data3)
                            'da4.Fill(data4)
                            'da5.Fill(data5)



                            'ds.Tables.Add(data1)
                            'ds.Tables.Add(data2)
                            'ds.Tables.Add(data3)
                            'ds.Tables.Add(data4)
                            'ds.Tables.Add(data5)

                            customersByCityReport.Load(reportPath)
                            SetDBReport(ds, customersByCityReport)
                            CrystalReportViewer1.ReportSource = customersByCityReport

                        End If

                        'ElseIf oprepetiq = 1 Then
                        '    reportPath = RutaReportes + "\ReporteEtiquetasTap.rpt"
                    End If



                    If LocOp = 25 Then
                        customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
                    Else
                        customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                    End If



                    CrystalReportViewer1.ReportSource = customersByCityReport
                    CrystalReportViewer1.Zoom(75)


                    Subformula = GloSucursal
                    If oprepetiq = 0 Then
                        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
                        customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & Subformula & "'"
                    End If

                    locbandrep = True
                    LocBndB = False

                    'customersByCityReport = Nothing
                Else
                    'GloProcesa = 3

                    Dim I As Integer = 0
                    Dim X As Integer = 0
                    Dim Txt As String = Nothing
                    Dim GLOBND As Boolean = True
                    'Me.DameGeneralesBancosTableAdapter.Fill(Me.NewsoftvDataSet2.DameGeneralesBancos, "PR")

                    Dim Nom_Archivo As String = Nothing
                    Dim Encabezado As String = Nothing
                    Dim imp1 As String = Nothing
                    Dim Rutatxt As String = Nothing

                    Dim Nom_ArchivoBat As String = Nothing
                    Nom_ArchivoBat = "C:" + "\" + "ImprimeEtiqueta.bat"
                    Dim fileExists2 As Boolean
                    fileExists2 = My.Computer.FileSystem.FileExists(Nom_ArchivoBat)
                    If fileExists2 = True Then
                        File.Delete(Nom_ArchivoBat)
                    End If
                    Using sw2 As StreamWriter = File.CreateText(Nom_ArchivoBat)
                        sw2.WriteLine("cd c:\")
                        sw2.WriteLine("Print Etiqueta2.txt > lpt1")
                        sw2.Close()
                    End Using
                    'If (result = DialogResult.OK) Then
                    Nom_Archivo = "C:" + "\" + "Etiqueta2.txt"

                    Dim fileExists As Boolean
                    fileExists = My.Computer.FileSystem.FileExists(Nom_Archivo)
                    If fileExists = True Then
                        File.Delete(Nom_Archivo)
                    End If
                    Using sw As StreamWriter = File.CreateText(Nom_Archivo)
                        Dim FilaRow As DataRow
                        'Me.CONSULTACNRTableAdapter.Fill(Me.DataSetLidia.CONSULTACNR)
                        Dim NumeroAfiliacion As String = Nothing
                        Dim ClaveBanco As String = Nothing
                        Dim ReferenciaCliente As String = Nothing
                        Dim NumeroTarjeta As String = Nothing
                        Dim StDetalle As String = Nothing
                        Dim StMonto As String = Nothing

                        ' Me.Reporte_TiposCliente_CiudadTableAdapter.Connection = CON
                        Me.Reporte_TiposCliente_CiudadTableAdapter.Fill(Me.DataSetEdgarRev2.Reporte_TiposCliente_Ciudad, LocClv_session, GloClv_tipser2, LocBndC, LocBndB, LocBndI, LocBndD, LocBndS, LocBndF, LocBndDT, 1, LocValidaHab, LocPeriodo1, LocPeriodo2)
                        For Each FilaRow In Me.DataSetEdgarRev2.Reporte_TiposCliente_Ciudad.Rows
                            If FilaRow("Contrato".ToString()) Is Nothing Then
                                Exit For
                            End If
                            sw.WriteLine("Q400,025")
                            sw.WriteLine("q1200")
                            sw.WriteLine("rN")
                            sw.WriteLine("S4")
                            sw.WriteLine("D7")
                            sw.WriteLine("ZT")
                            sw.WriteLine("JB")
                            sw.WriteLine("OD")
                            sw.WriteLine("R50,50")
                            sw.WriteLine("N")
                            sw.WriteLine("A0,0,0,5,1,1,N," & Chr(34) & Trim(FilaRow("Contrato".ToString())) & Chr(34))
                            sw.WriteLine("A500,0,0,5,1,1,N," & Chr(34) & Trim(FilaRow("Contrato".ToString())) & Chr(34))
                            sw.WriteLine("P1")
                        Next
                        'sw.WriteLine("^@")
                        '    Txt = "save"
                        '    sw.Write(Txt)
                        sw.Close()
                    End Using
                    Dim myProcess As New Process()
                    Dim myProcessStartInfo As New ProcessStartInfo("C:\\ImprimeEtiqueta.bat")
                    'Dim myProcessStartInfo As New ProcessStartInfo("C:\\Print Etiqueta2.txt ", "> lpt1")
                    myProcessStartInfo.WorkingDirectory = "C:\\"
                    'C:\Program Files\Network Registrar\Local\bin
                    myProcessStartInfo.UseShellExecute = False
                    myProcessStartInfo.RedirectStandardOutput = True
                    myProcess.StartInfo = myProcessStartInfo
                    myProcess.Start()

                    'End If

                End If
            Else

                If oprepetiq = 0 Then
                    customersByCityReport = New ReportDocument
                    Dim connectionInfo As New ConnectionInfo
                    '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
                    '    "=True;User ID=DeSistema;Password=1975huli")
                    connectionInfo.ServerName = GloServerName
                    connectionInfo.DatabaseName = GloDatabaseName
                    connectionInfo.UserID = GloUserID
                    connectionInfo.Password = GloPassword
                    Dim Contrataciones As Boolean = False
                    Dim Instalaciones As Boolean = False
                    Dim Fuera_Area As Boolean = False
                    Dim Cancelaciones As Boolean = False
                    Dim reportPath As String = Nothing
                    Dim nuevo As String = Nothing

                    Dim mySelectFormula As String = Titulo
                    Dim Subformula As String = Nothing
                    Dim OpOrdenar As String = "0"
                    If Me.RadioButton1.Checked = True Then
                        'contrato
                        OpOrdenar = "1"
                        nuevo = "_2.rpt"
                    ElseIf Me.RadioButton2.Checked = True Then
                        'colonia y calle
                        OpOrdenar = "2"
                        nuevo = ".rpt"
                    End If
                    If oprepetiq = 0 Then
                        If LocOp = 20 Then
                            If LocBndB = False Then
                                mySelectFormula = "Resumen de Clientes por Ciudad."
                                reportPath = RutaReportes + "\ReporteCiudad" + nuevo
                            ElseIf LocBndB = True Then
                                mySelectFormula = "Resumen de Clientes por Ciudad."
                                reportPath = RutaReportes + "\ReporteCiudad2" + nuevo
                            End If
                            'mySelectFormula = "Resumen de Clientes por Ciudad."
                            'reportPath = RutaReportes + "\ReporteCiudad.rpt"
                        ElseIf LocOp = 25 Then
                            mySelectFormula = "Listado de Clientes Por Colonia y Status."
                            reportPath = RutaReportes + "\Reporte_Resumen_Por_Colonia.rpt"
                        Else
                            mySelectFormula = "Resumen de Clientes por Ciudad."
                            reportPath = RutaReportes + "\ReporteCiudad" + nuevo
                        End If

                        'ElseIf oprepetiq = 1 Then
                        '    reportPath = RutaReportes + "\ReporteEtiquetasTap.rpt"
                    End If

                    customersByCityReport.Load(reportPath)
                    If LocOp = 25 Then
                        SetDBLogonForReport(connectionInfo, customersByCityReport)

                        '@clv_Session bigint,@op int,@conectado bit,@baja bit,@Insta bit,@Desconect bit,@Susp bit,@Fuera bit,@orden int,@fecha_ini datetime,@fecha_fin datetime)  

                        '@clv_session
                        customersByCityReport.SetParameterValue(0, LocClv_session)
                        '@Op int
                        customersByCityReport.SetParameterValue(1, GloClv_tipser2)
                        ',@contratado bit
                        customersByCityReport.SetParameterValue(2, LocBndC)
                        ',@Baja bit
                        customersByCityReport.SetParameterValue(3, LocBndB)
                        ',@Insta bit
                        customersByCityReport.SetParameterValue(4, LocBndI)
                        '@Desconect bit
                        customersByCityReport.SetParameterValue(5, LocBndD)
                        '@Suspendido bit
                        customersByCityReport.SetParameterValue(6, LocBndS)
                        '@Fuera bit
                        customersByCityReport.SetParameterValue(7, LocBndF)
                        '@DescTmp bit
                        customersByCityReport.SetParameterValue(8, LocBndDT)
                        '@Orden
                        customersByCityReport.SetParameterValue(9, CInt(OpOrdenar))
                        '@Habilita int
                        customersByCityReport.SetParameterValue(10, LocValidaHab)
                        '@Periodo1 bit
                        customersByCityReport.SetParameterValue(11, LocPeriodo1)
                        '@periodo2 bit
                        customersByCityReport.SetParameterValue(12, LocPeriodo2)
                        ''@Correo Int
                        'customersByCityReport.SetParameterValue(13, GloCorreo)
                    Else
                        BaseII.limpiaParametros()
                        BaseII.CreateMyParameter("@clv_Session", SqlDbType.BigInt, LocClv_session)
                        BaseII.CreateMyParameter("@op", SqlDbType.Int, GloClv_tipser2)
                        BaseII.CreateMyParameter("@conectado", SqlDbType.Bit, LocBndC)
                        BaseII.CreateMyParameter("@baja", SqlDbType.Bit, LocBndB)
                        BaseII.CreateMyParameter("@Insta", SqlDbType.Bit, LocBndI)
                        BaseII.CreateMyParameter("@Desconect", SqlDbType.Bit, LocBndD)
                        BaseII.CreateMyParameter("@Susp", SqlDbType.Bit, LocBndS)
                        BaseII.CreateMyParameter("@Fuera", SqlDbType.Bit, LocBndF)
                        BaseII.CreateMyParameter("@DescTmp", SqlDbType.Bit, LocBndDT)
                        BaseII.CreateMyParameter("@orden", SqlDbType.Int, CInt(OpOrdenar))
                        BaseII.CreateMyParameter("@Habilita", SqlDbType.Int, LocValidaHab)
                        BaseII.CreateMyParameter("@periodo1", SqlDbType.Bit, LocPeriodo1)
                        BaseII.CreateMyParameter("@periodo2", SqlDbType.Bit, LocPeriodo2)
                        BaseII.CreateMyParameter("@Correo", SqlDbType.Int, GloCorreo)
                        Dim DS As New DataSet()
                        Dim LT As New List(Of String)
                        LT.Add("Reporte_TiposCliente_Ciudad")
                        LT.Add("CALLES")
                        LT.Add("CIUDADES")
                        LT.Add("CLIENTES")
                        LT.Add("COLONIAS")
                        LT.Add("MotivoCancelacion")
                        DS = BaseII.ConsultaDS("Reporte_TiposCliente_Ciudad", LT)
                        SetDBReport(DS, customersByCityReport)

                        '@Op int
                        customersByCityReport.SetParameterValue(0, GloClv_tipser2)
                        ',@conECTADO bit
                        customersByCityReport.SetParameterValue(1, LocBndC)
                        ',@Baja bit
                        customersByCityReport.SetParameterValue(2, LocBndB)
                        ',@Insta bit
                        customersByCityReport.SetParameterValue(3, LocBndI)
                        '@Desconect bit
                        customersByCityReport.SetParameterValue(4, LocBndD)
                        '@Fuera bit
                        customersByCityReport.SetParameterValue(5, LocBndF)
                        '@DescTmp bit
                        customersByCityReport.SetParameterValue(6, LocBndDT)
                    End If


                    If LocOp = 25 Then
                        customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
                    Else
                        customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                    End If



                    CrystalReportViewer1.ReportSource = customersByCityReport
                    CrystalReportViewer1.Zoom(75)


                    Subformula = GloSucursal
                    If oprepetiq = 0 Then
                        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
                        customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & Subformula & "'"
                    End If

                    locbandrep = True
                    LocBndB = False

                    customersByCityReport = Nothing
                Else
                    'GloProcesa = 3

                    Dim I As Integer = 0
                    Dim X As Integer = 0
                    Dim Txt As String = Nothing
                    Dim GLOBND As Boolean = True
                    'Me.DameGeneralesBancosTableAdapter.Fill(Me.NewsoftvDataSet2.DameGeneralesBancos, "PR")

                    Dim Nom_Archivo As String = Nothing
                    Dim Encabezado As String = Nothing
                    Dim imp1 As String = Nothing
                    Dim Rutatxt As String = Nothing

                    Dim Nom_ArchivoBat As String = Nothing
                    Nom_ArchivoBat = "C:" + "\" + "ImprimeEtiqueta.bat"
                    Dim fileExists2 As Boolean
                    fileExists2 = My.Computer.FileSystem.FileExists(Nom_ArchivoBat)
                    If fileExists2 = True Then
                        File.Delete(Nom_ArchivoBat)
                    End If
                    Using sw2 As StreamWriter = File.CreateText(Nom_ArchivoBat)
                        sw2.WriteLine("cd c:\")
                        sw2.WriteLine("Print Etiqueta2.txt > lpt1")
                        sw2.Close()
                    End Using
                    'If (result = DialogResult.OK) Then
                    Nom_Archivo = "C:" + "\" + "Etiqueta2.txt"

                    Dim fileExists As Boolean
                    fileExists = My.Computer.FileSystem.FileExists(Nom_Archivo)
                    If fileExists = True Then
                        File.Delete(Nom_Archivo)
                    End If
                    Using sw As StreamWriter = File.CreateText(Nom_Archivo)
                        Dim FilaRow As DataRow
                        'Me.CONSULTACNRTableAdapter.Fill(Me.DataSetLidia.CONSULTACNR)
                        Dim NumeroAfiliacion As String = Nothing
                        Dim ClaveBanco As String = Nothing
                        Dim ReferenciaCliente As String = Nothing
                        Dim NumeroTarjeta As String = Nothing
                        Dim StDetalle As String = Nothing
                        Dim StMonto As String = Nothing

                        Me.Reporte_TiposCliente_CiudadTableAdapter.Connection = CON
                        Me.Reporte_TiposCliente_CiudadTableAdapter.Fill(Me.DataSetEdgarRev2.Reporte_TiposCliente_Ciudad, LocClv_session, GloClv_tipser2, LocBndC, LocBndB, LocBndI, LocBndD, LocBndS, LocBndF, LocBndDT, 1, LocValidaHab, LocPeriodo1, LocPeriodo2)
                        For Each FilaRow In Me.DataSetEdgarRev2.Reporte_TiposCliente_Ciudad.Rows
                            If FilaRow("Contrato".ToString()) Is Nothing Then
                                Exit For
                            End If
                            sw.WriteLine("Q400,025")
                            sw.WriteLine("q1200")
                            sw.WriteLine("rN")
                            sw.WriteLine("S4")
                            sw.WriteLine("D7")
                            sw.WriteLine("ZT")
                            sw.WriteLine("JB")
                            sw.WriteLine("OD")
                            sw.WriteLine("R50,50")
                            sw.WriteLine("N")
                            sw.WriteLine("A0,0,0,5,1,1,N," & Chr(34) & Trim(FilaRow("Contrato".ToString())) & Chr(34))
                            sw.WriteLine("A500,0,0,5,1,1,N," & Chr(34) & Trim(FilaRow("Contrato".ToString())) & Chr(34))
                            sw.WriteLine("P1")
                        Next
                        'sw.WriteLine("^@")
                        '    Txt = "save"
                        '    sw.Write(Txt)
                        sw.Close()
                    End Using
                    Dim myProcess As New Process()
                    Dim myProcessStartInfo As New ProcessStartInfo("C:\\ImprimeEtiqueta.bat")
                    'Dim myProcessStartInfo As New ProcessStartInfo("C:\\Print Etiqueta2.txt ", "> lpt1")
                    myProcessStartInfo.WorkingDirectory = "C:\\"
                    'C:\Program Files\Network Registrar\Local\bin
                    myProcessStartInfo.UseShellExecute = False
                    myProcessStartInfo.RedirectStandardOutput = True
                    myProcess.StartInfo = myProcessStartInfo
                    myProcess.Start()

                    'End If

                End If
            End If

            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ConfigureCrystalReportsNew8(ByVal op As String)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim Contrataciones As Boolean = False
            Dim Instalaciones As Boolean = False
            Dim Fuera_Area As Boolean = False
            Dim Cancelaciones As Boolean = False
            Dim reportPath As String = Nothing
            Dim Detalle As String = Nothing

            Dim mySelectFormula As String = Titulo
            Dim Subformula As String = Nothing
            Dim nuevo As String = Nothing
            Dim OpOrdenar As String = "0"
            If Me.RadioButton1.Checked = True Then
                OpOrdenar = "1"
                nuevo = "_2.rpt"
            ElseIf Me.RadioButton2.Checked = True Then
                OpOrdenar = "2"
                nuevo = ".rpt"
            End If


            mySelectFormula = "Resumen de Clientes con Paquetes de Prueba."
            Detalle = "Con Fecha de Vencimiento de la Fecha: " & GloFecha_Ini & " A la Fecha: " & GloFecha_Fin


            reportPath = RutaReportes + "\ListadoVencerPruebas" + nuevo


            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@clv_Session bigint,@op int,@conectado bit,@baja bit,@Insta bit,@Desconect bit,@Susp bit,@Fuera bit,@orden int,@fecha_ini datetime,@fecha_fin datetime)  



            '@Op int
            customersByCityReport.SetParameterValue(0, GloClv_tipser2)
            'Fecha_ini datetime
            customersByCityReport.SetParameterValue(1, GloFecha_Ini)
            'Fecha_fin datetime
            customersByCityReport.SetParameterValue(2, GloFecha_Fin)
            '@Orden
            customersByCityReport.SetParameterValue(3, CInt(OpOrdenar))
            '@Habilita int
            customersByCityReport.SetParameterValue(4, LocValidaHab)
            '@Periodo1 bit
            customersByCityReport.SetParameterValue(5, LocPeriodo1)
            '@periodo2 bit
            customersByCityReport.SetParameterValue(6, LocPeriodo2)






            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)


            Subformula = GloSucursal

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("Detalle").Text = "'" & Detalle & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & Subformula & "'"

            locbandrep = True

            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ConfigureCrystalReportsNewPPE()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing


            Dim eFecha As String = Nothing
            eFecha = "Del " & eFechaIni & " al " & eFechaFin
            reportPath = RutaReportes + "\ReportServiciosPPE.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            'FechaIni
            customersByCityReport.SetParameterValue(0, eFechaIni)
            'FechaFin
            customersByCityReport.SetParameterValue(1, eFechaFin)


            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)




            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & GloSucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFecha & "'"
            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ConfigureCrystalReports_Cancelaciones_SinMens(ByVal op As String)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim Contrataciones As Boolean = False
            Dim Instalaciones As Boolean = False
            Dim Fuera_Area As Boolean = False
            Dim Cancelaciones As Boolean = False
            Dim reportPath As String = Nothing
            Dim Detalle As String = Nothing

            Dim mySelectFormula As String = Titulo
            Dim Subformula As String = Nothing
            Dim nuevo As String = Nothing
            Dim OpOrdenar As String = "0"

            If Me.RadioButton1.Checked = True Then
                OpOrdenar = "1"
                nuevo = "_2.rpt"
            ElseIf Me.RadioButton2.Checked = True Then
                OpOrdenar = "2"
                nuevo = ".rpt"
            End If


            mySelectFormula = "Listado De Clientes Cancelados Sin Mesualidades Pagadas."
            Detalle = "Con Fecha de Cancelaci�n de la Fecha: " & GloFecha_Ini & " A la Fecha: " & GloFecha_Fin


            reportPath = RutaReportes + "\Reporte_Cancelador_SinMens" + nuevo


            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@clv_Session bigint
            customersByCityReport.SetParameterValue(0, LocClv_session)
            '@op int 
            customersByCityReport.SetParameterValue(1, GloClv_tipser2)
            '@Orden
            customersByCityReport.SetParameterValue(2, CInt(OpOrdenar))
            'Fecha_ini datetime
            customersByCityReport.SetParameterValue(3, GloFecha_Ini)
            'Fecha_fin datetime
            customersByCityReport.SetParameterValue(4, GloFecha_Fin)
            '@Habilita int
            customersByCityReport.SetParameterValue(5, LocValidaHab)
            '@Periodo1 bit
            customersByCityReport.SetParameterValue(6, LocPeriodo1)
            '@periodo2 bit
            customersByCityReport.SetParameterValue(7, LocPeriodo2)







            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)


            Subformula = GloSucursal

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & Detalle & "'"

            locbandrep = True

            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub FrmImprimir_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'AQUI VALIDACIONES PARA LA SEGURIDAD
        If eBndIrdeto = True Then
            eBndIrdeto = False
            RutaReportes = DameRutaReportes(1)
            ConfigureCrystalReportsNew6(eOpIrdeto.ToString)
        End If
        If bndreporte = True Then
            bndreporte = False
            MandaReportes()
        End If
        '================================TERMINACION DE SEGURIDAD
        If GloBndSelBanco = True Then
            GloBndSelBanco = False
            RutaReportes = DameRutaReportes(1)
            ConfigureCrystalReports(op, Titulo)
        End If
        If bndReportA = True Then
            bndReportA = False
            'ConfigureCrystalReports(op, Titulo)
            RutaReportes = DameRutaReportes(1)
            ReporteAntiguedad()
        End If
        If GloBndEtiqueta = True Then
            GloBndEtiqueta = False
            RutaReportes = DameRutaReportes(1)
            ConfigureCrystalReportsEtiquetas(GloOpEtiqueta)
        End If
        If bndfechareport = True Then
            bndfechareport = False
            RutaReportes = DameRutaReportes(1)
            ConfigureCrystalReportsNew(op)
        End If
        If bndReport = True Then
            bndReport = False
            RutaReportes = DameRutaReportes(1)
            ConfigureCrystalReportNew1(op)
        End If
        If bndReport2 = True Then
            bndReport2 = False
            RutaReportes = DameRutaReportes(1)
            ConfigureCrystalReportNew2(op)
        End If
        If bndReportC = True Then
            bndReportC = False
            RutaReportes = DameRutaReportes(1)
            ConfigureCrystalReportNew3(op)
        End If
        If bndAvisos2 = True Then
            bndAvisos2 = False
            RutaReportes = DameRutaReportes(1)
            ConfigureCrystalReportNew4(GloOpEtiqueta)
        End If
        If LocServicios = True Then
            LocServicios = False
            RutaReportes = DameRutaReportes(1)
            ConfigureCrystalReportsNew5(op)
        End If
        If Locreportcity = True Then
            Locreportcity = False
            RutaReportes = DameRutaReportes(1)
            ConfigureCrystalReportsNew7(op)
        End If
        If bndfechareport2 = True Then
            bndfechareport2 = False
            RutaReportes = DameRutaReportes(1)
            ConfigureCrystalReportsNew8(op)
        End If

        If eBndReportePPE = True Then
            eBndReportePPE = False
            RutaReportes = DameRutaReportes(1)
            ConfigureCrystalReportsNewPPE()
        End If
        If Locbndrepcontspago = True Then
            Locbndrepcontspago = False
            RutaReportes = DameRutaReportes(1)
            ConfigureCrystalReportscontratacionessinpago()
        End If
        If LocreportEstado = True Then
            LocreportEstado = False
            RutaReportes = DameRutaReportes(1)
            ConLidia.Open()
            Dim CMd As New SqlClient.SqlCommand
            With CMd

                '@orden int,@Habilita int,@periodo1 bit,@periodo2 bit) 
                .CommandText = "Reporte_TiposCliente_Ciudad "
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = ConLidia
                Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                Dim prm1 As New SqlParameter("@op", SqlDbType.Int)
                Dim prm2 As New SqlParameter("@conectado", SqlDbType.Bit)
                Dim prm3 As New SqlParameter("@baja", SqlDbType.Bit)
                Dim prm4 As New SqlParameter("@insta", SqlDbType.Bit)
                Dim prm5 As New SqlParameter("@desconect", SqlDbType.Bit)
                Dim prm6 As New SqlParameter("@susp", SqlDbType.Bit)
                Dim prm7 As New SqlParameter("@fuera", SqlDbType.Bit)
                Dim prm8 As New SqlParameter("@desctmp", SqlDbType.Bit)
                Dim prm9 As New SqlParameter("@orden", SqlDbType.Int)
                Dim prm10 As New SqlParameter("@habilita", SqlDbType.Int)
                Dim prm11 As New SqlParameter("@Periodo1", SqlDbType.Bit)
                Dim prm12 As New SqlParameter("@Periodo2", SqlDbType.Bit)
                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Input
                prm3.Direction = ParameterDirection.Input
                prm4.Direction = ParameterDirection.Input
                prm5.Direction = ParameterDirection.Input
                prm6.Direction = ParameterDirection.Input
                prm7.Direction = ParameterDirection.Input
                prm8.Direction = ParameterDirection.Input
                prm9.Direction = ParameterDirection.Input
                prm10.Direction = ParameterDirection.Input
                prm11.Direction = ParameterDirection.Input
                prm12.Direction = ParameterDirection.Input
                prm.Value = LocClv_session
                prm1.Value = GloClv_tipser2
                prm2.Value = LocBndC
                prm3.Value = LocBndB
                prm4.Value = LocBndI
                prm5.Value = LocBndD
                prm6.Value = LocBndS
                prm7.Value = LocBndF
                prm8.Value = LocBndDT
                prm9.Value = 1
                prm10.Value = LocValidaHab
                prm11.Value = LocPeriodo1
                prm12.Value = LocPeriodo2
                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)
                .Parameters.Add(prm4)
                .Parameters.Add(prm5)
                .Parameters.Add(prm6)
                .Parameters.Add(prm7)
                .Parameters.Add(prm8)
                .Parameters.Add(prm9)
                .Parameters.Add(prm10)
                .Parameters.Add(prm11)
                .Parameters.Add(prm12)
                Dim i As Integer = CMd.ExecuteNonQuery()
            End With
            ConLidia.Close()

            ConfigureCrystalReportsEstadodeCuenta(LocClv_session)
        End If
        If bnd_Canc_Sin_Mens_buena = True Then
            bnd_Canc_Sin_Mens_buena = False
            RutaReportes = DameRutaReportes(1)
            Me.ConfigureCrystalReports_Cancelaciones_SinMens(0)
        End If
        'End If
    End Sub

    Private Sub FrmImprimir_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim CON As New SqlConnection(MiConexion)
        If locbandrep = True Then
            Borra_Rel_Telefono_Report()
            Me.Borrar_Session_ServiciosTableAdapter.Connection = CON
            Me.Borrar_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.Borrar_Session_Servicios, LocClv_session)
            Me.Borra_temporalesTableAdapter.Connection = CON
            Me.Borra_temporalesTableAdapter.Fill(Me.ProcedimientosArnoldo2.Borra_temporales, LocClv_session)
            Me.Borra_Rel_Reportes_SoloInternetTableAdapter.Connection = CON
            Me.Borra_Rel_Reportes_SoloInternetTableAdapter.Fill(Me.ProcedimientosArnoldo2.Borra_Rel_Reportes_SoloInternet, LocClv_session)
            'Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
            'Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
        End If
        CON.Close()
    End Sub


    Private Sub FrmImprimir_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        locbndrepcancelaciones = False
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetarnoldo.Borra_Archivo_Irdeto' Puede moverla o quitarla seg�n sea necesario.
        Me.Borra_Archivo_IrdetoTableAdapter.Connection = CON
        Me.Borra_Archivo_IrdetoTableAdapter.Fill(Me.DataSetarnoldo.Borra_Archivo_Irdeto)
        colorea(Me, Me.Name)
        Me.Valida_periodo_reportesTableAdapter.Connection = CON
        Me.Valida_periodo_reportesTableAdapter.Fill(Me.DataSetarnoldo.Valida_periodo_reportes, LocValidaHab)
        Dim princ As String
        If IsNumeric(ComboBox4.SelectedValue) = True Then
            GloClv_tipser2 = ComboBox4.SelectedValue
        Else
            GloClv_tipser2 = glotiposervicioppal
        End If
        princ = glotiposervicioppal
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla seg�n sea necesario.
        Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.CatalogodeReportes' Puede moverla o quitarla seg�n sea necesario.
        princ = Me.ComboBox4.SelectedValue.ToString
        Select Case princ
            Case "1"
                Me.CatalogodeReportesTableAdapter.Connection = CON
                Me.CatalogodeReportesTableAdapter.Fill(Me.NewSofTvDataSet.CatalogodeReportes, Me.ComboBox4.SelectedValue.ToString, 0)
            Case "2"
                Me.CatalogodeReportesTableAdapter.Connection = CON
                Me.CatalogodeReportesTableAdapter.Fill(Me.NewSofTvDataSet.CatalogodeReportes, Me.ComboBox4.SelectedValue.ToString, 0)
            Case "3"
                Me.CatalogodeReportesTableAdapter.Connection = CON
                Me.CatalogodeReportesTableAdapter.Fill(Me.NewSofTvDataSet.CatalogodeReportes, Me.ComboBox4.SelectedValue.ToString, 0)
        End Select
        CON.Close()

        'ConfigureCrystalReports()
        'Dim op As String = nothing
        'Dim Titulo As String = nothing

        'If IsNumeric(DataGridView1.SelectedCells(0).Value) = True Then
        '    op = CStr(DataGridView1.SelectedCells(0).Value)
        '    Titulo = CStr(DataGridView1.SelectedCells(1).Value)
        '    ConfigureCrystalReports(op, Titulo)
        'End If

    End Sub


    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub

    'Private Sub SetCurrentValuesForParameterField(ByVal myReportDocument As ReportDocument)
    ' Dim currentParameterValues As ParameterValues = New ParameterValues()
    'For Each submittedValue As Object In myArrayList
    'Dim myParameterDiscreteValue As ParameterDiscreteValue = New ParameterDiscreteValue()
    'MsgBox(submittedValue.ToString())
    'myParameterDiscreteValue.Value = submittedValue.ToString()
    'currentParameterValues.Add(myParameterDiscreteValue)
    'Next
    'Dim myParameterFieldDefinitions As ParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
    'Dim myParameterFieldDefinition As ParameterFieldDefinition = myParameterFieldDefinitions(PARAMETER_FIELD_NAME)
    'myParameterFieldDefinition.ApplyCurrentValues(currentParameterValues)
    'Dim myParameterFieldDefinitions As ParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
    'Dim myParameterFieldDefinition As ParameterFieldDefinition
    'Dim myParameterDiscreteValue As ParameterDiscreteValue = New ParameterDiscreteValue()
    '   myParameterDiscreteValue.Value = 0'

    'For Each myParameterFieldDefinition In myParameterFieldDefinitions
    '   With myParameterFieldDefinition
    '      Select Case .ParameterFieldName
    '         Case "@Op"
    '            .CurrentValues.Add(myParameterDiscreteValue)

    '   End Select
    'End With
    'Next
    'End Sub



    'Private Sub CoIMPRIMR_Click()
    '    Dim CRXReport As ImprimeServicios
    '    Dim CRXTable As CRAXDRT.DatabaseTable

    '    Dim crpParamDefs As CRAXDRT.ParameterFieldDefinitions
    '    Dim crpParamDef As CRAXDRT.ParameterFieldDefinition

    '    'Desconectados
    '    If CoClvTipServ.ListIndex <> -1 Then
    '        CRXReport = New ImprimeServicios
    '        crpParamDefs = CRXReport.ParameterFields
    '        For Each CRXTable In CRXReport.Database.Tables
    '            CRXTable.SetLogOnInfo(GloNameBaseDatos, GloNameBaseDatos, Login, Pass)
    '        Next
    '        For Each crpParamDef In crpParamDefs
    '            With crpParamDef
    '                Select Case .ParameterFieldName
    '                    Case "@Clv_TipSer"
    '                        .SetCurrentValue(CInt(CoClvTipServ.Text))
    '                End Select
    '            End With
    '        Next


    '        misql = ""

    '        For Each CRXTable In CRXReport.Database.Tables
    '            CRXTable.SetLogOnInfo(GloNameBaseDatos, GloNameBaseDatos, Login, Pass)
    '        Next
    '        CRXReport.EnableParameterPrompting = False
    '        CRXReport.FormulaFields.GetItemByName("Sucursal").Text = "'" & GloSucursal & "'"
    '        CRXReport.FormulaFields.GetItemByName("subtitulo").Text = "'" & CoTipServ.Text & "'"
    '        FrmImprimir.CRViewer1.ReportSource = CRXReport
    '        FrmImprimir.CRViewer1.DisplayToolbar = True
    '        FrmImprimir.CRViewer1.DisplayGroupTree = False
    '        FrmImprimir.CRViewer1.EnableExportButton = True
    '        FrmImprimir.CRViewer1.ViewReport()
    '        FrmImprimir.CRViewer1.Zoom(100)
    '        FrmImprimir.Show(1, Me)
    '        CRXReport = Nothing
    '    End If
    'End Sub


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If locbandrep = True Then
            Me.Borrar_Session_ServiciosTableAdapter.Connection = CON
            Me.Borrar_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.Borrar_Session_Servicios, LocClv_session)
            Me.Borra_temporalesTableAdapter.Connection = CON
            Me.Borra_temporalesTableAdapter.Fill(Me.ProcedimientosArnoldo2.Borra_temporales, LocClv_session)
            Me.Borra_Rel_Reportes_SoloInternetTableAdapter.Connection = CON
            Me.Borra_Rel_Reportes_SoloInternetTableAdapter.Fill(Me.ProcedimientosArnoldo2.Borra_Rel_Reportes_SoloInternet, LocClv_session)
            Borra_Rel_Telefono_Report()
            'Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
            'Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
        End If
        If locbandrep2 = True Then
            Me.Borra_Archivo_IrdetoTableAdapter.Connection = CON
            Me.Borra_Archivo_IrdetoTableAdapter.Fill(Me.DataSetarnoldo.Borra_Archivo_Irdeto)
        End If
        CON.Close()
        Me.Close()
    End Sub



    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If LocClv_session > 0 Then
            Borra_Rel_Telefono_Report()
            Me.Borrar_Session_ServiciosTableAdapter.Connection = CON
            Me.Borrar_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.Borrar_Session_Servicios, LocClv_session)
            Me.Borra_temporalesTableAdapter.Connection = CON
            Me.Borra_temporalesTableAdapter.Fill(Me.ProcedimientosArnoldo2.Borra_temporales, LocClv_session)
            Me.Borra_Rel_Reportes_SoloInternetTableAdapter.Connection = CON
            Me.Borra_Rel_Reportes_SoloInternetTableAdapter.Fill(Me.ProcedimientosArnoldo2.Borra_Rel_Reportes_SoloInternet, LocClv_session)
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
        Else
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
        End If
        MandaReportes()
        CON.Close()

    End Sub



    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.ComboBox4.SelectedValue) = True Then
            GloClv_tipser2 = ComboBox4.SelectedValue
            If GloClv_tipser2 = 2 Then
                Panel4.Visible = True
            ElseIf GloClv_tipser2 <> 2 Then
                Panel4.Visible = False
            End If
            Me.CatalogodeReportesTableAdapter.Connection = CON
            Me.CatalogodeReportesTableAdapter.Fill(Me.NewSofTvDataSet.CatalogodeReportes, Me.ComboBox4.SelectedValue, 0)
            If locbandrep = True Then
                Me.Borrar_Session_ServiciosTableAdapter.Connection = CON
                Me.Borrar_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.Borrar_Session_Servicios, LocClv_session)
                Me.Borra_temporalesTableAdapter.Connection = CON
                Me.Borra_temporalesTableAdapter.Fill(Me.ProcedimientosArnoldo2.Borra_temporales, LocClv_session)
                Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
                Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            End If
        End If
        CON.Close()
    End Sub
    Private Sub Checa_SoloInternet()
        Dim con As New SqlConnection(MiConexion)
        con.Open()
        If Me.CheckBox1.CheckState = CheckState.Checked Then
            solointernet = 1
        ElseIf Me.CheckBox1.CheckState = CheckState.Unchecked Then
            solointernet = 0
        End If

        If solointernet = 1 Then
            'Procedimiento Para insertar a la tabla de solo internet
            Me.Inserta_Rel_Reportes_SoloInternetTableAdapter.Connection = con
            Me.Inserta_Rel_Reportes_SoloInternetTableAdapter.Fill(Me.ProcedimientosArnoldo2.Inserta_Rel_Reportes_SoloInternet, LocClv_session, solointernet)
        End If
        con.Close()
    End Sub
    Private Sub ConfigureCrystalReportsEstadodeCuenta(ByVal Session As Integer)
        Try
            Dim impresora As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\ReportEstado_Cuenta.rpt"
            customersByCityReport.Load(reportPath)

            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Contrato 
            customersByCityReport.SetParameterValue(0, CStr(Session))
            customersByCityReport.SetParameterValue(1, IdSistema)
            customersByCityReport.SetParameterValue(2, "1")
            customersByCityReport.SetParameterValue(3, "1")
            CrystalReportViewer1.ReportSource = customersByCityReport
            'SetDBLogonForReport2(connectionInfo)
            'CrystalReportViewer1.ShowPrintButton = True

            'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)
            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            CrystalReportViewer1.Zoom(75)

            'customersByCityReport.PrintOptions.PrinterName = ImpresoraEstado
            'customersByCityReport.PrintToPrinter(1, True, 1, 1)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub


    Public Sub New()

        ' Llamada necesaria para el Dise�ador de Windows Forms.
        InitializeComponent()

        ' Agregue cualquier inicializaci�n despu�s de la llamada a InitializeComponent().

    End Sub



    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub ReporteAntiguedad()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword



            Dim reportPath As String = Nothing
            Dim Titulo As String = String.Empty

            If eRepAnt = "L" Then
                reportPath = RutaReportes + "\ReportAntiguedadListado.rpt"
                Titulo = "Reporte de Antig�edad - Listado"
            ElseIf eRepAnt = "R" Then
                reportPath = RutaReportes + "\ReportAntiguedadResumen.rpt"
                Titulo = "Reporte de Antig�edad - Resumen"
            End If




            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Clv_Session
            customersByCityReport.SetParameterValue(0, LocClv_session)
            '@Clv_TipSer
            customersByCityReport.SetParameterValue(1, GloClv_tipser2)
            '@AntIni
            customersByCityReport.SetParameterValue(2, eAntIni)
            '@AntFin
            customersByCityReport.SetParameterValue(3, eAntFin)
            '@Periodo1
            customersByCityReport.SetParameterValue(4, LocPeriodo1)
            '@Periodo2
            customersByCityReport.SetParameterValue(5, LocPeriodo2)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)
            SetDBLogonForReport2(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged

    End Sub

    Private Function REPORTECortesias(ByVal Clv_Session As Integer, ByVal Fecha1 As DateTime, ByVal Fecha2 As DateTime, ByVal Op As Integer, ByVal Orden As Integer, ByVal Habilita As Integer, ByVal Periodo1 As Boolean, ByVal Periodo2 As Boolean, ByVal Activo As Boolean, ByVal Inactivo As Boolean) As DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("REPORTECortesias")
        tableNameList.Add("CALLES")
        tableNameList.Add("CIUDADES")
        tableNameList.Add("CLIENTES")
        tableNameList.Add("COLONIAS")
        tableNameList.Add("Parametros")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@fecha1", SqlDbType.DateTime, Fecha1)
        BaseII.CreateMyParameter("@fecha2", SqlDbType.DateTime, Fecha2)
        BaseII.CreateMyParameter("@op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Orden", SqlDbType.Int, Orden)
        BaseII.CreateMyParameter("@Habilita", SqlDbType.Int, Habilita)
        BaseII.CreateMyParameter("@periodo1", SqlDbType.Bit, Periodo1)
        BaseII.CreateMyParameter("@periodo2", SqlDbType.Bit, Periodo2)
        BaseII.CreateMyParameter("@ACTIVO", SqlDbType.Bit, Activo)
        BaseII.CreateMyParameter("@INACTIVO", SqlDbType.Bit, Inactivo)
        Return BaseII.ConsultaDS("REPORTECortesias", tableNameList)
    End Function

    Private Function REPORTECortesiasTEL(ByVal Clv_Session As Integer, ByVal Fecha1 As DateTime, ByVal Fecha2 As DateTime, ByVal Op As Integer, ByVal Orden As Integer, ByVal Habilita As Integer, ByVal Periodo1 As Boolean, ByVal Periodo2 As Boolean, ByVal Nrep As Integer, ByVal Activo As Boolean, ByVal Inactivo As Boolean) As DataSet

        Dim tableNameList As New List(Of String)
        tableNameList.Add("REPORTECortesiasTEL")
        tableNameList.Add("CALLES")
        tableNameList.Add("CIUDADES")
        tableNameList.Add("CLIENTES")
        tableNameList.Add("COLONIAS")
        tableNameList.Add("Parametros")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@fecha1", SqlDbType.DateTime, Fecha1)
        BaseII.CreateMyParameter("@fecha2", SqlDbType.DateTime, Fecha2)
        BaseII.CreateMyParameter("@op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Orden", SqlDbType.Int, Orden)
        BaseII.CreateMyParameter("@Habilita", SqlDbType.Int, Habilita)
        BaseII.CreateMyParameter("@periodo1", SqlDbType.Bit, Periodo1)
        BaseII.CreateMyParameter("@periodo2", SqlDbType.Bit, Periodo2)
        BaseII.CreateMyParameter("@nrep", SqlDbType.Int, Nrep)
        BaseII.CreateMyParameter("@ACTIVO", SqlDbType.Bit, Activo)
        BaseII.CreateMyParameter("@INACTIVO", SqlDbType.Bit, Inactivo)
        Return BaseII.ConsultaDS("REPORTECortesiasTEL", tableNameList)
    End Function

End Class