﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmReporteComisiones
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CMBTipo = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.RBRangoSegundo = New System.Windows.Forms.RadioButton()
        Me.RBRangoPrimer = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.CMBAnio = New System.Windows.Forms.ComboBox()
        Me.CMBMes = New System.Windows.Forms.ComboBox()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.BtnAceptar = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CMBVendedores = New System.Windows.Forms.ComboBox()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'CMBTipo
        '
        Me.CMBTipo.DisplayMember = "TIPO"
        Me.CMBTipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTipo.FormattingEnabled = True
        Me.CMBTipo.Location = New System.Drawing.Point(176, 12)
        Me.CMBTipo.Name = "CMBTipo"
        Me.CMBTipo.Size = New System.Drawing.Size(160, 23)
        Me.CMBTipo.TabIndex = 0
        Me.CMBTipo.ValueMember = "CLAVE"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(114, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Tipo : "
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(36, 83)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(367, 15)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Seleccione el rango de la comisión que desee consultar"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.RBRangoSegundo)
        Me.GroupBox1.Controls.Add(Me.RBRangoPrimer)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(108, 117)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(204, 48)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Rango"
        '
        'RBRangoSegundo
        '
        Me.RBRangoSegundo.AutoSize = True
        Me.RBRangoSegundo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RBRangoSegundo.Location = New System.Drawing.Point(134, 19)
        Me.RBRangoSegundo.Name = "RBRangoSegundo"
        Me.RBRangoSegundo.Size = New System.Drawing.Size(70, 19)
        Me.RBRangoSegundo.TabIndex = 1
        Me.RBRangoSegundo.TabStop = True
        Me.RBRangoSegundo.Text = "16 - 31"
        Me.RBRangoSegundo.UseVisualStyleBackColor = True
        '
        'RBRangoPrimer
        '
        Me.RBRangoPrimer.AutoSize = True
        Me.RBRangoPrimer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RBRangoPrimer.Location = New System.Drawing.Point(37, 19)
        Me.RBRangoPrimer.Name = "RBRangoPrimer"
        Me.RBRangoPrimer.Size = New System.Drawing.Size(62, 19)
        Me.RBRangoPrimer.TabIndex = 0
        Me.RBRangoPrimer.TabStop = True
        Me.RBRangoPrimer.Text = "1 - 15"
        Me.RBRangoPrimer.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.CMBAnio)
        Me.GroupBox2.Controls.Add(Me.CMBMes)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(76, 186)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(296, 81)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Mes y Año"
        '
        'CMBAnio
        '
        Me.CMBAnio.DisplayMember = "ANIO"
        Me.CMBAnio.FormattingEnabled = True
        Me.CMBAnio.Location = New System.Drawing.Point(166, 38)
        Me.CMBAnio.Name = "CMBAnio"
        Me.CMBAnio.Size = New System.Drawing.Size(121, 23)
        Me.CMBAnio.TabIndex = 1
        '
        'CMBMes
        '
        Me.CMBMes.DisplayMember = "MESES"
        Me.CMBMes.FormattingEnabled = True
        Me.CMBMes.Location = New System.Drawing.Point(10, 38)
        Me.CMBMes.Name = "CMBMes"
        Me.CMBMes.Size = New System.Drawing.Size(121, 23)
        Me.CMBMes.TabIndex = 0
        Me.CMBMes.ValueMember = "ID"
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(285, 273)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(136, 36)
        Me.btnSalir.TabIndex = 69
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'BtnAceptar
        '
        Me.BtnAceptar.BackColor = System.Drawing.Color.DarkOrange
        Me.BtnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnAceptar.ForeColor = System.Drawing.Color.Black
        Me.BtnAceptar.Location = New System.Drawing.Point(135, 273)
        Me.BtnAceptar.Name = "BtnAceptar"
        Me.BtnAceptar.Size = New System.Drawing.Size(136, 36)
        Me.BtnAceptar.TabIndex = 70
        Me.BtnAceptar.Text = "&Aceptar"
        Me.BtnAceptar.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(70, 44)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(91, 15)
        Me.Label3.TabIndex = 72
        Me.Label3.Text = "Vendedores :"
        '
        'CMBVendedores
        '
        Me.CMBVendedores.DisplayMember = "NOMBRE"
        Me.CMBVendedores.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBVendedores.FormattingEnabled = True
        Me.CMBVendedores.Location = New System.Drawing.Point(176, 41)
        Me.CMBVendedores.Name = "CMBVendedores"
        Me.CMBVendedores.Size = New System.Drawing.Size(160, 23)
        Me.CMBVendedores.TabIndex = 71
        Me.CMBVendedores.ValueMember = "CLAVE"
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'FrmReporteComisiones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(432, 319)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.CMBVendedores)
        Me.Controls.Add(Me.BtnAceptar)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CMBTipo)
        Me.Name = "FrmReporteComisiones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Comisiones"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBTipo As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents RBRangoSegundo As System.Windows.Forms.RadioButton
    Friend WithEvents RBRangoPrimer As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents CMBAnio As System.Windows.Forms.ComboBox
    Friend WithEvents CMBMes As System.Windows.Forms.ComboBox
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents BtnAceptar As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CMBVendedores As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
