﻿Public Class FrmComisionRecontratacion
    Dim MSG As String = Nothing
#Region "Salir"
    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
#End Region

#Region "Eventos"
    Private Sub FrmComisionRecontratacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Label1.ForeColor = Color.Black
        Label2.ForeColor = Color.Black
        Label3.ForeColor = Color.Black
        Label4.ForeColor = Color.Black
        Label5.ForeColor = Color.Black
        Label6.ForeColor = Color.Black
        Label7.ForeColor = Color.Black
        Label8.ForeColor = Color.Black

        UspLlenaNivelUsuario()
        UspLlenaServicios()
        UspLlenaGrupo()
        If opcion = "N" Then
            Me.cmbGrupoVentas.SelectedValue = 0
            Me.cmbNivelUsuario.SelectedValue = 99
            Me.cmbServicio.SelectedValue = 0
        ElseIf opcion = "C" Then
            Me.cmbGrupoVentas.Enabled = False
            Me.cmbNivelUsuario.Enabled = False
            Me.cmbServicio.Enabled = False
            txtPorcentaje.Enabled = False
            txtMesesBajaIni.Enabled = False
            txtMesesPagadosIni.Enabled = False
            txtMesesBajaIni.Enabled = False
            txtMesesPagadosIni.Enabled = False
            txtMesesBajaFin.Enabled = False
            txtMesesPagadosFin.Enabled = False
            btnAgregar.Enabled = False
            btnBorrar.Enabled = False
            btnGuardar.Enabled = False
            btnModificar.Enabled = False
            dgCalculoBonos.Enabled = False

            UspMuestraComisionRecontratacion(GloClaveCalculoBono)
        ElseIf opcion = "M" Then
            UspMuestraComisionRecontratacion(GloClaveCalculoBono)
            Me.cmbGrupoVentas.Enabled = False
            Me.cmbNivelUsuario.Enabled = False
            Me.cmbServicio.Enabled = False
            If Me.dgCalculoBonos.Rows.Count > 0 Then
                Me.btnBorrar.Enabled = True
                Me.btnGuardar.Enabled = False
                Me.btnModificar.Enabled = True
                Me.btnAgregar.Enabled = True
            Else
                Me.btnBorrar.Enabled = False
                Me.btnGuardar.Enabled = False
                Me.btnModificar.Enabled = False
                Me.btnAgregar.Enabled = True
            End If
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub txtMesesPagadosIni_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMesesPagadosIni.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtMesesPagadosFin_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMesesPagadosFin.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtMesesBajaIni_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMesesBajaIni.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtMesesBajaFin_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMesesBajaFin.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtPorcentaje_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPorcentaje.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub
 
#End Region

#Region "Funciones"
    Private Sub UspLlenaNivelUsuario()
        Try
            BaseII.limpiaParametros()
            cmbNivelUsuario.DataSource = BaseII.ConsultaDT("UspLlenaNivelUsuario")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspLlenaServicios()
        Try
            BaseII.limpiaParametros()
            cmbServicio.DataSource = BaseII.ConsultaDT("UspLlenaServicios")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspLlenaGrupo()
        Try
            BaseII.limpiaParametros()
            cmbGrupoVentas.DataSource = BaseII.ConsultaDT("UspLlenaGrupo")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMuestraComisionRecontratacion(ByVal prmClave As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVCOMISION", SqlDbType.Int, prmClave)
            dgCalculoBonos.DataSource = BaseII.ConsultaDT("UspMuestraComisionRecontratacion")
            If dgCalculoBonos.Rows.Count > 0 Then
                Me.cmbServicio.SelectedValue = CInt(Me.dgCalculoBonos(1, 0).Value)
                Me.cmbNivelUsuario.SelectedValue = CInt(dgCalculoBonos(3, 0).Value)
                Me.cmbGrupoVentas.SelectedValue = CInt(dgCalculoBonos(2, 0).Value)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspBorrarComisionRecontratacion(ByVal prmClave As Integer)
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVCOMISIONRECONTRATACION", SqlDbType.Int, prmClave)
            DT = BaseII.ConsultaDT("UspBorrarComisionRecontratacion")
            GloClaveCalculoBono = DT.Rows(0)(0).ToString()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspAgregaComisionRecontratacion(ByVal prmClvServicio As Integer, ByVal prmPorcentaje As Integer, ByVal prmEsSupervisor As Integer, ByVal prmClvGrupo As Integer, ByVal prmMesesPagadosInicial As Integer, ByVal prmMesesPagadosFinal As Integer, ByVal prmMesesBajoInicial As Integer, ByVal prmMesesBajoFinal As Integer)
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSERVICIO", SqlDbType.Int, prmClvServicio)
            BaseII.CreateMyParameter("@PORCENTAJE", SqlDbType.Int, prmPorcentaje)
            BaseII.CreateMyParameter("@ESSUPERVISOR", SqlDbType.Int, prmEsSupervisor)
            BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, prmClvGrupo)
            BaseII.CreateMyParameter("@MESESPAGADOSINICIAL", SqlDbType.Int, prmMesesPagadosInicial)
            BaseII.CreateMyParameter("@MESESPAGADOSFINAL", SqlDbType.Int, prmMesesPagadosFinal)
            BaseII.CreateMyParameter("@MESESBAJAINICIAL", SqlDbType.Int, prmMesesBajoInicial)
            BaseII.CreateMyParameter("@MESESBAJAFINAL", SqlDbType.Int, prmMesesBajoFinal)

            DT = BaseII.ConsultaDT("UspAgregaComionRecontratacion")
            MSG = DT.Rows(0)(0).ToString
            If DT.Rows(0)(1).ToString <> "0" Then
                GloClaveCalculoBono = DT.Rows(0)(1).ToString
            End If
            MsgBox(MSG)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspGuardarComisionRecontratacion(ByVal prmClvComisionRecontratacion As Integer, ByVal prmClvServicio As Integer, ByVal prmPorcentaje As Integer, ByVal prmEsSupervisor As Integer, ByVal prmClvGrupo As Integer, ByVal prmMesesPagadosInicial As Integer, ByVal prmMesesPagadosFinal As Integer, ByVal prmMesesBajoInicial As Integer, ByVal prmMesesBajoFinal As Integer)
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVCOMISION", SqlDbType.Int, prmClvComisionRecontratacion)
            BaseII.CreateMyParameter("@CLVSERVICIO", SqlDbType.Int, prmClvServicio)
            BaseII.CreateMyParameter("@PORCENTAJE", SqlDbType.Int, prmPorcentaje)
            BaseII.CreateMyParameter("@ESSUPERVISOR", SqlDbType.Int, prmEsSupervisor)
            BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, prmClvGrupo)
            BaseII.CreateMyParameter("@MESESPAGADOSINICIAL", SqlDbType.Int, prmMesesPagadosInicial)
            BaseII.CreateMyParameter("@MESESPAGADOSFINAL", SqlDbType.Int, prmMesesPagadosFinal)
            BaseII.CreateMyParameter("@MESESBAJAINICIAL", SqlDbType.Int, prmMesesBajoInicial)
            BaseII.CreateMyParameter("@MESESBAJAFINAL", SqlDbType.Int, prmMesesBajoFinal)
            DT = BaseII.ConsultaDT("UspGuardarComisionRecontratacion")
            MSG = DT.Rows(0)(0).ToString
            MsgBox(MSG)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890.", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function
#End Region

#Region "Agregar"
    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        If cmbServicio.SelectedValue = 0 Then
            MsgBox("No tiene seleccionado ningun paquete")
            Exit Sub
        End If
        If cmbNivelUsuario.SelectedValue = 99 Then
            MsgBox("No tiene seleccionado ningun Nivel de Usuario")
            Exit Sub
        End If
        If cmbGrupoVentas.SelectedValue = 0 Then
            MsgBox("No tiene seleccionado ningun Grupo de Ventas")
            Exit Sub
        End If
        If Len(txtPorcentaje.Text) = 0 Then
            MsgBox("El Porcentaje esta en blanco, Favor de verificar el dato")
            Exit Sub
        End If
        If Len(txtMesesBajaIni.Text) = 0 Then
            MsgBox("El número de meses en baja inicial esta en blanco, Favor de verificar el dato")
            Exit Sub
        End If
        If Len(txtMesesBajaFin.Text) = 0 Then
            MsgBox("El número de meses en baja final esta en blanco, Favor de verificar el dato")
            Exit Sub
        End If
        If Len(txtMesesPagadosIni.Text) = 0 Then
            MsgBox("El número de meses pagados inicial estan en blanco, Favor de verificar el dato")
            Exit Sub
        End If
        If Len(txtMesesPagadosFin.Text) = 0 Then
            MsgBox("El número de meses pagados final estan en blanco, Favor de verificar el dato")
            Exit Sub
        End If

        UspAgregaComisionRecontratacion(Me.cmbServicio.SelectedValue, Me.txtPorcentaje.Text, Me.cmbNivelUsuario.SelectedValue, Me.cmbGrupoVentas.SelectedValue, Me.txtMesesPagadosIni.Text, Me.txtMesesPagadosFin.Text, Me.txtMesesBajaIni.Text, Me.txtMesesBajaFin.Text)
        Me.btnModificar.Enabled = True
        Me.btnGuardar.Enabled = False
        UspMuestraComisionRecontratacion(GloClaveCalculoBono)
        txtPorcentaje.Text = ""
        txtMesesBajaIni.Text = ""
        txtMesesBajaFin.Text = ""
        txtMesesPagadosIni.Text = ""
        txtMesesPagadosFin.Text = ""
        Me.cmbGrupoVentas.Enabled = False
        Me.cmbNivelUsuario.Enabled = False
        Me.cmbServicio.Enabled = False
    End Sub
#End Region

#Region "Borrar"
    Private Sub btnBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBorrar.Click
        If Me.dgCalculoBonos.SelectedRows.Count > 0 Then
            GloClaveCalculoBono = dgCalculoBonos.SelectedCells(0).Value
            UspBorrarComisionRecontratacion(GloClaveCalculoBono)
            UspMuestraComisionRecontratacion(GloClaveCalculoBono)
        End If
    End Sub
#End Region
   
#Region "Modificar"
    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        If Me.dgCalculoBonos.SelectedRows.Count > 0 Then
            GloClaveCalculoBono = dgCalculoBonos.SelectedCells(0).Value
            txtPorcentaje.Text = dgCalculoBonos.SelectedCells(4).Value
            txtPorcentaje.Text = Replace(txtPorcentaje.Text, "%", "")
            txtMesesPagadosIni.Text = dgCalculoBonos.SelectedCells(5).Value
            txtMesesPagadosFin.Text = dgCalculoBonos.SelectedCells(6).Value
            txtMesesBajaIni.Text = dgCalculoBonos.SelectedCells(7).Value
            txtMesesBajaFin.Text = dgCalculoBonos.SelectedCells(8).Value
            Me.btnAgregar.Enabled = False
            Me.btnModificar.Enabled = False
            Me.btnGuardar.Enabled = True
        End If
    End Sub
#End Region

#Region "Guardar"
    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
         If cmbServicio.SelectedValue = 0 Then
            MsgBox("No tiene seleccionado ningun paquete")
            Exit Sub
        End If
        If cmbNivelUsuario.SelectedValue = 99 Then
            MsgBox("No tiene seleccionado ningun Nivel de Usuario")
            Exit Sub
        End If
        If cmbGrupoVentas.SelectedValue = 0 Then
            MsgBox("No tiene seleccionado ningun Grupo de Ventas")
            Exit Sub
        End If
        If Len(txtPorcentaje.Text) = 0 Then
            MsgBox("El Porcentaje esta en blanco, Favor de verificar el dato")
            Exit Sub
        End If
        If Len(txtMesesBajaIni.Text) = 0 Then
            MsgBox("El número de meses en baja inicial esta en blanco, Favor de verificar el dato")
            Exit Sub
        End If
        If Len(txtMesesBajaFin.Text) = 0 Then
            MsgBox("El número de meses en baja final esta en blanco, Favor de verificar el dato")
            Exit Sub
        End If
        If Len(txtMesesPagadosIni.Text) = 0 Then
            MsgBox("El número de meses pagados inicial estan en blanco, Favor de verificar el dato")
            Exit Sub
        End If
        If Len(txtMesesPagadosFin.Text) = 0 Then
            MsgBox("El número de meses pagados final estan en blanco, Favor de verificar el dato")
            Exit Sub
        End If

        UspGuardarComisionRecontratacion(GloClaveCalculoBono, Me.cmbServicio.SelectedValue, Me.txtPorcentaje.Text, Me.cmbNivelUsuario.SelectedValue, Me.cmbGrupoVentas.SelectedValue, Me.txtMesesPagadosIni.Text, Me.txtMesesPagadosFin.Text, Me.txtMesesBajaIni.Text, Me.txtMesesBajaFin.Text)
        Me.btnModificar.Enabled = True
        Me.btnGuardar.Enabled = False
        Me.btnAgregar.Enabled = True
        UspMuestraComisionRecontratacion(GloClaveCalculoBono)
        txtPorcentaje.Text = ""
        txtMesesBajaIni.Text = ""
        txtMesesBajaFin.Text = ""
        txtMesesPagadosIni.Text = ""
        txtMesesPagadosFin.Text = ""
        Me.cmbGrupoVentas.Enabled = False
        Me.cmbNivelUsuario.Enabled = False
        Me.cmbServicio.Enabled = False
    End Sub
#End Region

#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabCtrl = New TabControl
                tabCtrl = ctl
                bwrTabControlPerfilDesactiva(tabCtrl, formulario, NomInterno)
                tabCtrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabpg = New TabPage
                tabpg = ctl
                bwrTabPagePerfilDesactiva(tabpg, formulario, NomInterno)
                tabpg = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabCtrl = New TabControl
                tabCtrl = ctl
                bwrTabControlPerfilDesactiva(tabCtrl, formulario, NomInterno)
                tabCtrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabpg = New TabPage
                tabpg = ctl
                bwrTabPagePerfilDesactiva(tabpg, formulario, NomInterno)
                tabpg = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabCtrl = New TabControl
                tabCtrl = ctl
                bwrTabControlPerfilDesactiva(tabCtrl, formulario, NomInterno)
                tabCtrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabpg = New TabPage
                tabpg = ctl
                bwrTabPagePerfilDesactiva(tabpg, formulario, NomInterno)
                tabpg = Nothing
            End If

        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabCtrl = New TabControl
                tabCtrl = ctl
                bwrTabControlPerfilDesactiva(tabCtrl, formulario, NomInterno)
                tabCtrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabpg = New TabPage
                tabpg = ctl
                bwrTabPagePerfilDesactiva(tabpg, formulario, NomInterno)
                tabpg = Nothing
            End If
        Next
    End Sub

    Public Sub bwrTabControlPerfilDesactiva(ByVal tabctrl1 As TabControl, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage

        For Each ctl As Control In tabctrl1.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabctrl = New TabControl
                tabctrl = ctl
                bwrTabControlPerfilDesactiva(tabctrl, formulario, NomInterno)
                tabctrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabpg = New TabPage
                tabpg = ctl
                bwrTabPagePerfilDesactiva(tabpg, formulario, NomInterno)
                tabpg = Nothing
            End If
        Next
    End Sub

    Public Sub bwrTabPagePerfilDesactiva(ByVal tabPG1 As TabPage, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox
        Dim tabCtrl As TabControl
        Dim tabpg As TabPage

        For Each ctl As Control In tabPG1.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                tabCtrl = New TabControl
                tabCtrl = ctl
                bwrTabControlPerfilDesactiva(tabCtrl, formulario, NomInterno)
                tabCtrl = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                tabPG = New TabPage
                tabPG = ctl
                bwrTabPagePerfilDesactiva(tabPG, formulario, NomInterno)
                tabPG = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region
    
End Class