﻿Public Class FrmTelmexEncuesta

    Dim idMedioFtth As Integer
    Dim InstalaraServicios As Boolean
    Dim bndHayFtth As Boolean
    'Dim siTieneServicioTelmex As Boolean = False
    Dim yaTieneServicioTelmex As Boolean = False


    'La primera vez que se instala un servicio se hacen todas las preguntas de la encuesta y se guardan
    'Las siguientes veces solo se pregunta el tipo de servicio y no se guarda

    Private Sub FrmEncuestaTelmex_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ColoreaFrm(Me)
        'colorea(Me, Me.Name)

        BindingNavigatorDeleteItem.Visible = False
        bndContestoEncueesta = False
        Button5.Visible = False

        inicializar()

        If opcion = "N" Then
            Panel3.Visible = False
        End If

    End Sub


    Private Sub DimeSiHayMedioFtth()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@bndHayOno", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.CreateMyParameter("@IdFtth", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("DimeSiHayMedioFTTH")
        bndHayFtth = BaseII.dicoPar("@bndHayOno")
        idMedioFtth = BaseII.dicoPar("@IdFtth")
    End Sub


    Private Sub inicializar()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, Contrato)
        BaseII.CreateMyParameter("@yaTieneTelmex", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("VALIDA_ServicioTelmex")
        yaTieneServicioTelmex = BaseII.dicoPar("@yaTieneTelmex")

        bndInstalaraOnoTelmex = False
        llenarComboInstalacion()

        'If yaTieneServicioTelmex = False Then 'Para mostrar u ocultar el plan de pago para FTTH
        '    DimeSiHayMedioFtth()
        'End If

        Panel3.Visible = False
        Panel4.Visible = False

        tipoInstalacionCmb.Enabled = False
        cantidadExtensionesNUD.Enabled = False

        si1_RadioBtn.Checked = False
        no1_RadioBtn.Checked = True ' no1_RadioBtn.Checked = True
        si2_RadioBtn.Checked = False
        no2_RadioBtn.Checked = False

        cantidadExtensionesNUD.Value = 0

    End Sub



    Private Sub Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Click


        If yaTieneServicioTelmex = True Or no1_RadioBtn.Checked = True Then
            bndInstalaraOnoTelmex = si1_RadioBtn.Checked 'Para saber qué servicios mostrar
            bndContestoEncueesta = True
            Me.Close()
        Else

            If (si1_RadioBtn.Checked = False And no1_RadioBtn.Checked = False) Or (si1_RadioBtn.Checked = True And (si2_RadioBtn.Checked = False And no2_RadioBtn.Checked = False)) Then
                MessageBox.Show("Seleccione una respuesta")
                Exit Sub
            End If

            If tipoInstalacionCmb.SelectedValue() > 0 Then

                buscarServicio_ConidMedioTelmex = tipoInstalacionCmb.SelectedValue()

                Dim pregunta1 As Boolean = False
                Dim pregunta2 As Boolean = False

                If si2_RadioBtn.Checked = True Then
                    pregunta1 = True
                Else
                    pregunta1 = False
                End If

                bndInstalaraOnoTelmex = si1_RadioBtn.Checked
                bndContestoEncueesta = True

                If si1_RadioBtn.Checked = True Then
                    pregunta1 = True
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@CantExtension", SqlDbType.Int, cantidadExtensionesNUD.Text)
                    BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, contratoConTelmex)
                    BaseII.CreateMyParameter("@CuentaTelmex", SqlDbType.Bit, pregunta1)      '@CuentaTelmex
                    BaseII.CreateMyParameter("@PlanPago", SqlDbType.Int, planPagoMesesNUD.Value)         '@PlanPago
                    BaseII.CreateMyParameter("@TipoInstalacion", SqlDbType.BigInt, buscarServicio_ConidMedioTelmex) '@TipoInstalacion
                    Dim resultado As Integer
                    Dim dt As DataTable
                    dt = BaseII.ConsultaDT("AddClientesTelTelmex_Temporal") 'AddClientesTelTelmex
                    Dim dr As DataRow
                    For Each dr In dt.Rows
                        resultado = dr("Resultado")
                    Next
                Else
                    pregunta1 = False
                    'NO GUARDAMOS NADA
                End If

            Else
                MessageBox.Show("Seleccione un tipo de instalación")
                Exit Sub
            End If

        End If

        'End If

        Me.Close()
    End Sub


    Private Sub llenarComboInstalacion()

        BaseII.limpiaParametros()
        tipoInstalacionCmb.DataSource = BaseII.ConsultaDT("MedioList")

    End Sub


    Private Sub Button5_Click(sender As Object, e As EventArgs)
        Me.Close()
    End Sub


    Private Sub FrmCatalogoMarcas_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed

    End Sub

    Private Sub muestraPlanPago()

        Panel4.Visible = True 'MOSTRAR PLAN DE PAGO SIEMPRE

        ' Esto es para mostrar el PLAN DE PAGO unicamente para FTTH
        'If si1_RadioBtn.Checked = True And no2_RadioBtn.Checked = True Then
        '    If tipoInstalacionCmb.SelectedValue = idMedioFtth Then ' debe ser de ftth
        '        Panel4.Visible = True
        '    Else
        '        Panel4.Visible = False
        '        planPagoMesesNUD.Value = 0
        '    End If
        'Else
        '    Panel4.Visible = False
        '    planPagoMesesNUD.Value = 0
        'End If


    End Sub


    Private Sub si1_RadioBtn_CheckedChanged(sender As Object, e As EventArgs) Handles si1_RadioBtn.CheckedChanged

        muestraPlanPago()

        'Permitimos llenar la encuesta
        no2_RadioBtn.Enabled = True
        si2_RadioBtn.Enabled = True
        tipoInstalacionCmb.Enabled = True
        cantidadExtensionesNUD.Enabled = True

        If yaTieneServicioTelmex = False Then
            Panel3.Visible = True
        Else
            Panel3.Visible = False
        End If

        tipoInstalacionCmb.SelectedValue = 1


    End Sub


    Private Sub no1_RadioBtn_CheckedChanged(sender As Object, e As EventArgs) Handles no1_RadioBtn.CheckedChanged
        bloquearEncuesta()
        muestraPlanPago()

    End Sub


    Private Sub bloquearEncuesta()
        no2_RadioBtn.Checked = False
        si2_RadioBtn.Checked = False

        cantidadExtensionesNUD.Text = 0
        tipoInstalacionCmb.SelectedValue = 0

        'Bloqueamos la encuesta y quitamos las selecciones

        no2_RadioBtn.Enabled = False
        si2_RadioBtn.Enabled = False
        tipoInstalacionCmb.Enabled = False
        cantidadExtensionesNUD.Enabled = False
        planPagoMesesNUD.Value = 0

        Panel3.Visible = False
        Panel4.Visible = False

    End Sub


    Private Sub BindingNavigatorDeleteItem_Click(sender As Object, e As EventArgs) Handles BindingNavigatorDeleteItem.Click

    End Sub

 

   
    Private Sub tipoInstalacionCmb_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tipoInstalacionCmb.SelectedIndexChanged
        'muestraPlanPago()
    End Sub

    
  
    Private Sub si2_RadioBtn_CheckedChanged(sender As Object, e As EventArgs) Handles si2_RadioBtn.CheckedChanged
        muestraPlanPago()
    End Sub

    Private Sub no2_RadioBtn_CheckedChanged(sender As Object, e As EventArgs) Handles no2_RadioBtn.CheckedChanged
        muestraPlanPago()
    End Sub



End Class