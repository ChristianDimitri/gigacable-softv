Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.VisualBasic
Imports System.IO

Public Class FrmSelCablemodemsDisponibles

    Public Sub MUESTRACABLEMODEMS_disponibles_2_IAPAR(ByVal MOp As Long, ByVal MClv_Tecnico As Long, ByVal MClv_Orden As Long)
        '@Op BIGINT,@Clv_Tecnico bigint,@Clv_Orden bigint
        Dim conn As New SqlConnection(MiConexion)
        Try

            Dim reader As SqlDataReader
            Dim cmd As New SqlCommand("MUESTRACABLEMODEMS_disponibles_2_IAPAR", conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@Op", SqlDbType.BigInt).Value = MOp
            cmd.Parameters.Add("@Clv_Tecnico", SqlDbType.BigInt).Value = MClv_Tecnico
            cmd.Parameters.Add("@Clv_Orden", SqlDbType.BigInt).Value = MClv_Orden
            conn.Open()
            reader = cmd.ExecuteReader()

            Using reader
                While reader.Read
                    Me.ComboBox2.Items.Add(reader.GetValue(0))
                    'Me.ComboBox1.Items.Add(reader.GetValue(1))        TENGO DUDAS
                    'Me.Clave.Text = reader.GetValue(1)
                    'Me.Descripcion.Text = reader.GetValue(2)
                End While
            End Using
            conn.Close()
        Catch ex As Exception
            If conn.State <> ConnectionState.Closed Then conn.Close()
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        GloBndClv_CablemodemSel = False
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If IsNumeric(Me.CajasComboBox.SelectedValue) = True Then
            GloClv_CablemodemSel = Me.ComboBox2.SelectedValue
            GloMacCablemodemSel = Me.ComboBox2.Text
            GloClvCajaDig = Me.CajasComboBox.Text
            GloBndClv_CablemodemSel = True
        ElseIf IsNumeric(Me.ComboBox2.SelectedValue) = True Then
            GloClv_CablemodemSel = Me.ComboBox2.SelectedValue
            GloMacCablemodemSel = Me.ComboBox2.Text
            GloClvCajaDig = Me.CajasComboBox.Text
            GloBndClv_CablemodemSel = True
        Else
            GloClv_CablemodemSel = 0
            GloClvCajaDig = 0
            GloMacCablemodemSel = ""
            GloBndClv_CablemodemSel = False
            If IdSistema <> "LO" And IdSistema <> "YU" Then
                MsgBox("No se Seleccionado un Cablemodem", MsgBoxStyle.Information)
            ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                MsgBox("No se Seleccionado un ATA", MsgBoxStyle.Information)
            End If
        End If
        If GloClv_TipSer = 3 Then
            If GLOTRABAJO <> "CAPAR" Then
                If IsNumeric(Me.CajasComboBox.SelectedValue) = True Then
                    GloClvCajaDig = Me.CajasComboBox.Text
                    GloBndClv_CablemodemSel = True
                Else
                    MsgBox("No ha seleccionado una caja digital", MsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
        End If
        Me.Close()
    End Sub

    Private Sub FrmSelCablemodemsDisponibles_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If GloClv_TipSer = 2 Then
            Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Connection = CON
            Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Fill(Me.DataSetEdgarRev2.MUESTRACABLEMODEMS_disponibles_2, 0, Locclv_tec)
            Me.Label1.Visible = False
            Me.CajasComboBox.Visible = False
            CON.Close()
        ElseIf GloClv_TipSer = 3 Then
            If GLOTRABAJO = "CCAJA" Then
                Me.ComboBox2.Visible = False
                Me.CajasComboBox.Visible = True
                Me.Label5.Visible = False
                Me.Label1.Visible = True
                MuestraCajasDisponibles()
            ElseIf GLOTRABAJO = "CAPAR" Then
                Me.ComboBox2.Visible = True
                Me.CajasComboBox.Visible = False
                Me.Label5.Visible = True
                Me.Label1.Visible = False
            End If
           
                Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Connection = CON
                Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Fill(Me.DataSetEdgarRev2.MUESTRACABLEMODEMS_disponibles_2, 3, Locclv_tec)


            If GLOTRABAJO = "IAPAR" Or GLOTRABAJO = "RIAPA" Or GLOTRABAJO = "CCAJA" Then
                Me.CajasComboBox.Visible = True
                MuestraCajasDisponibles()
                'Else
                '    Me.CajasComboBox.Visible = False
            End If
        ElseIf GloClv_TipSer = 5 Then
            'Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Connection = CON
            'Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Fill(Me.DataSetEdgarRev2.MUESTRACABLEMODEMS_disponibles_2, 3, Locclv_tec)
            Me.Text = "Seleccione ATA"
            Label5.Text = "ATAs disponibles :"
            Me.Label1.Visible = False
            Me.CajasComboBox.Visible = False
            Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Connection = CON
            Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Fill(Me.DataSetEdgarRev2.MUESTRACABLEMODEMS_disponibles_2, 20, Locclv_tec)

            CON.Close()
        End If
        CON.Close()
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If IsNumeric(Me.ComboBox2.SelectedValue) = True Then
            GloClv_CablemodemSel = Me.ComboBox2.SelectedValue
            GloMacCablemodemSel = Me.ComboBox2.Text
        End If
    End Sub

    Private Sub MuestraCajasDisponibles()
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSql As New StringBuilder

        StrSql.Append("EXEC MuestraCajasDisponibles ")
        StrSql.Append(CStr(Locclv_tec) & ",")
        StrSql.Append(CStr(ContratoNetCajasDig))

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(StrSql.ToString(), CON)
        'Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            'BS.DataSource = DT
            Me.CajasComboBox.DataSource = DT
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
End Class