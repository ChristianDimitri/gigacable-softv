﻿Public Class FrmComisionPorNumVentaFechas


#Region "Eventos"
    Private Sub FrmComisionPorNumVentaFechas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        lblServicioPrincipal.ForeColor = Color.Black
        Label5.ForeColor = Color.Black
        UspLlenaGrupo()
        UspLlenaServPrincipales()
        Me.DTPFechaInicial.Value = GloFechaInicialComision
        Me.DTPFechaFinal.Value = GloFechaFinalComision
        Me.cmbServicioPrincipal.Enabled = True
        Me.cmbGrupoVentas.Enabled = True
        Me.dgImportesCombos.Enabled = True
        Me.btnGuardar.Enabled = True
        Me.btnAgregar.Enabled = True
        Me.Panel1.Enabled = True
        Me.Panel1.BackColor = Me.BackColor
        Me.Label1.BackColor = Me.BackColor
        Me.Label2.BackColor = Me.BackColor
       

        If opcion = "M" Then
            Me.cmbGrupoVentas.SelectedValue = GloClvGrupoVentas
            Me.cmbServicioPrincipal.SelectedValue = GloClvServicioPrincipal

            UspMuestraImportesNumVentasFechas()
            Me.cmbServicioPrincipal.Enabled = False
            Me.cmbGrupoVentas.Enabled = False
            Me.DTPFechaInicial.Enabled = False
            Me.DTPFechaFinal.Enabled = False
        ElseIf opcion = "C" Then
            Me.cmbGrupoVentas.SelectedValue = GloClvGrupoVentas
            Me.cmbServicioPrincipal.SelectedValue = GloClvServicioPrincipal

            UspMuestraImportesNumVentasFechas()
            Me.cmbServicioPrincipal.Enabled = False
            Me.cmbGrupoVentas.Enabled = False
            Me.dgImportesCombos.Enabled = False
            Me.btnGuardar.Enabled = False
            Me.btnAgregar.Enabled = False
            Panel1.Enabled = False
        ElseIf opcion = "N" Then
            Me.DTPFechaInicial.MaxDate = Me.DTPFechaFinal.Value
            Me.DTPFechaFinal.MinDate = Me.DTPFechaInicial.Value
        End If

        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub DTPFechaInicial_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DTPFechaInicial.ValueChanged
        Me.DTPFechaFinal.MinDate = Me.DTPFechaInicial.Value
        If opcion = "N" Then
            GloFechaInicialComision = DTPFechaInicial.Value
        End If
    End Sub

    Private Sub DTPFechaFinal_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DTPFechaFinal.ValueChanged
        Me.DTPFechaInicial.MaxDate = Me.DTPFechaFinal.Value
        If opcion = "N" Then
            GloFechaFinalComision = DTPFechaFinal.Value
        End If
    End Sub
#End Region

#Region "Funciones"
    Private Sub UspLlenaGrupo()
        Try
            BaseII.limpiaParametros()
            cmbGrupoVentas.DataSource = BaseII.ConsultaDT("UspLlenaGrupo")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspLlenaServPrincipales()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Var", SqlDbType.VarChar, opcion, 5)
            BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, Me.cmbGrupoVentas.SelectedValue)
            cmbServicioPrincipal.DataSource = BaseII.ConsultaDT("UspLlenaServPrincipalesNumVentas")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMuestraImportesNumVentasFechas()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSERVICIOPRINCIPAL", SqlDbType.Int, Me.cmbServicioPrincipal.SelectedValue)
            BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, Me.cmbGrupoVentas.SelectedValue)
            BaseII.CreateMyParameter("@FECHAINICIAL", SqlDbType.DateTime, Me.DTPFechaInicial.Value)
            BaseII.CreateMyParameter("@FECHAFINAL", SqlDbType.DateTime, Me.DTPFechaFinal.Value)
            dgImportesCombos.DataSource = BaseII.ConsultaDT("UspMuestraImportesNumVentasFechas")
            If dgImportesCombos.Rows.Count > 0 Then
                CBVigente.Checked = CBool(dgImportesCombos(6, 0).Value)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function UspAgregarServicioVentasFechas() As DataTable
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSERVICIOPRINCIPAL", SqlDbType.Int, Me.cmbServicioPrincipal.SelectedValue)
            BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, Me.cmbGrupoVentas.SelectedValue)
            BaseII.CreateMyParameter("@FECHAINICIAL", SqlDbType.DateTime, Me.DTPFechaInicial.Value)
            BaseII.CreateMyParameter("@FECHAFINAL", SqlDbType.DateTime, Me.DTPFechaFinal.Value)
            BaseII.CreateMyParameter("@OPCION", SqlDbType.VarChar, opcion, 1)
            UspAgregarServicioVentasFechas = BaseII.ConsultaDT("UspAgregarServicioVentasFechas")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub UspGuardaComisionNumVentasFechas()
        Try
            Dim I, Vigente As Integer

            If CBVigente.Checked = True Then
                Vigente = 1
            Else
                Vigente = 0
            End If

            For I = 0 To dgImportesCombos.Rows.Count - 1
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLVSERVICIOPRINCIPAL", SqlDbType.Int, CInt(Me.cmbServicioPrincipal.SelectedValue))
                BaseII.CreateMyParameter("@CLVNUMVENTA", SqlDbType.Int, CInt(Me.dgImportesCombos(1, I).Value))
                BaseII.CreateMyParameter("@COMISION", SqlDbType.Money, CDec(Me.dgImportesCombos(3, I).Value))
                BaseII.CreateMyParameter("@CLVGRUPO", SqlDbType.Int, CInt(Me.cmbGrupoVentas.SelectedValue))
                BaseII.CreateMyParameter("@VIGENTE", SqlDbType.Int, Vigente)
                BaseII.CreateMyParameter("@FECHAINICIAL", SqlDbType.DateTime, Me.DTPFechaInicial.Value)
                BaseII.CreateMyParameter("@FECHAFINAL", SqlDbType.DateTime, Me.DTPFechaFinal.Value)
                BaseII.Inserta("UspGuardaComisionNumVentasFechas")
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Agregar"
    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        If CBVigente.Checked = False Then
            MsgBox("Debe de Tener activado la casilla de vigente")
            Exit Sub
        End If
        If Me.cmbGrupoVentas.SelectedValue = 0 Then
            MsgBox("Seleccione el grupo de ventas", MsgBoxStyle.Information, "Comisión por número de ventas")
            Exit Sub
        End If
        Dim DT As New DataTable
        DT = UspAgregarServicioVentasFechas()
        If DT.Rows.Count > 0 Then
            If DT.Rows(0)(0).ToString = "1" Then
                MsgBox(DT.Rows(0)(1).ToString, MsgBoxStyle.Information, "Comisión por número de ventas")
                Exit Sub
            End If
        End If
        UspMuestraImportesNumVentasFechas()
        Me.cmbServicioPrincipal.Enabled = False
        Me.cmbGrupoVentas.Enabled = False
        Me.DTPFechaInicial.Enabled = False
        Me.DTPFechaFinal.Enabled = False
        
    End Sub
#End Region

#Region "Salir"
    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
#End Region

#Region "Guardar"
    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        UspGuardaComisionNumVentasFechas()
        UspMuestraImportesNumVentasFechas()
    End Sub
#End Region

#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region
   
    
   
   
   
   
End Class