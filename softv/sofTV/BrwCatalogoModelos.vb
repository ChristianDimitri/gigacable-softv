﻿Public Class BrwCatalogoModelos

    Private Sub bNuevo_Click(sender As Object, e As EventArgs) Handles bNuevo.Click

        bndCatalogoTelmex = "N"
        IdModeloT = 0
        FrmCatalogoModelosT.ShowDialog()
        TraerTodosModelos()

    End Sub

    Private Sub BrwCatalogoModelos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name) '  ColoreaFrm(Me)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)

        IdModeloT = 0

        llenarComboMarcas() 'Trae todas las marcas 

        TraerTodosModelos()

        LlenarLabels()
    End Sub

    Private Sub llenarComboMarcas()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdMarca", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, "")
        BaseII.CreateMyParameter("@Opcion", SqlDbType.Int, 0) 'Todas las marcas
        marcaCombo.DataSource = BaseII.ConsultaDT("FiltroMarcas")

    End Sub


    Private Sub bModificar_Click(sender As Object, e As EventArgs) Handles bModificar.Click
        bndCatalogoTelmex = "M"
        IdModeloT = Convert.ToInt64(gridModelos("IdModelo", gridModelos.CurrentRow.Index).Value)

        FrmCatalogoModelosT.ShowDialog()
        TraerTodosModelos()
    End Sub

    Private Sub BrwCatalogoIP_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated

    End Sub

    Private Sub TraerTodosModelos()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdModelo", SqlDbType.BigInt, IdModeloT)
        BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, "")
        BaseII.CreateMyParameter("@IdMarca", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Opcion", SqlDbType.Int, 0) 'Todos los modelos
        Dim dt As DataTable
        dt = BaseII.ConsultaDT("FiltroModelos")
        Dim dr As DataRow
        gridModelos.Rows.Clear()
        For Each dr In dt.Rows
            gridModelos.Rows.Add()
            gridModelos("IdModelo", gridModelos.Rows.Count - 1).Value = dr("IdModelo").ToString()
            gridModelos("Modelo", gridModelos.Rows.Count - 1).Value = dr("Modelo").ToString()
            gridModelos("IdMarca", gridModelos.Rows.Count - 1).Value = dr("IdMarca").ToString()
            gridModelos("Marca", gridModelos.Rows.Count - 1).Value = dr("Marca").ToString()
            gridModelos("Precio", gridModelos.Rows.Count - 1).Value = dr("Precio").ToString()
        Next
    End Sub


    'Private Sub TraerModelos_ByMarca(DescripcionMarca As String)

    '    BaseII.limpiaParametros()
    '    BaseII.CreateMyParameter("@IdModelo", SqlDbType.BigInt, 0) 'CHECAR
    '    BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, DescripcionMarca)
    '    BaseII.CreateMyParameter("@Opcion", SqlDbType.Int, 2)
    '    Dim dt As DataTable
    '    dt = BaseII.ConsultaDT("FiltroModelos")
    '    Dim dr As DataRow
    '    gridModelos.Rows.Clear()
    '    For Each dr In dt.Rows
    '        gridModelos.Rows.Add()
    '        gridModelos("IdModelo", gridModelos.Rows.Count - 1).Value = dr("IdModelo").ToString()
    '        gridModelos("Modelo", gridModelos.Rows.Count - 1).Value = dr("Modelo").ToString()
    '        gridModelos("IdMarca", gridModelos.Rows.Count - 1).Value = dr("IdMarca").ToString()
    '        gridModelos("Marca", gridModelos.Rows.Count - 1).Value = dr("Marca").ToString()
    '        gridModelos("Precio", gridModelos.Rows.Count - 1).Value = dr("Precio").ToString()
    '    Next

    'End Sub


    'Private Sub TraerModelos_By_IdMarca(IdMarcaCombo As String)   ' YA

    '    BaseII.limpiaParametros()
    '    BaseII.CreateMyParameter("@IdModelo", SqlDbType.BigInt, 0) 'CHECAR
    '    BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, "")
    '    BaseII.CreateMyParameter("@IdMarca", SqlDbType.BigInt, IdMarcaCombo)
    '    BaseII.CreateMyParameter("@Opcion", SqlDbType.Int, 1)
    '    Dim dt As DataTable
    '    dt = BaseII.ConsultaDT("FiltroModelos")
    '    Dim dr As DataRow
    '    gridModelos.Rows.Clear()
    '    For Each dr In dt.Rows
    '        gridModelos.Rows.Add()
    '        gridModelos("IdModelo", gridModelos.Rows.Count - 1).Value = dr("IdModelo").ToString()
    '        gridModelos("Modelo", gridModelos.Rows.Count - 1).Value = dr("Modelo").ToString()
    '        gridModelos("IdMarca", gridModelos.Rows.Count - 1).Value = dr("IdMarca").ToString()
    '        gridModelos("Marca", gridModelos.Rows.Count - 1).Value = dr("Marca").ToString()
    '        gridModelos("Precio", gridModelos.Rows.Count - 1).Value = dr("Precio").ToString()
    '    Next

    'End Sub

    'Private Sub TraerModelos_By_IdModelo(Id_Modelo As String)   ' YA

    '    BaseII.limpiaParametros()
    '    BaseII.CreateMyParameter("@IdModelo", SqlDbType.BigInt, Id_Modelo)
    '    BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, "")
    '    BaseII.CreateMyParameter("@IdMarca", SqlDbType.BigInt, 0)
    '    BaseII.CreateMyParameter("@Opcion", SqlDbType.Int, 2)
    '    Dim dt As DataTable
    '    dt = BaseII.ConsultaDT("FiltroModelos")
    '    Dim dr As DataRow
    '    gridModelos.Rows.Clear()
    '    For Each dr In dt.Rows
    '        gridModelos.Rows.Add()
    '        gridModelos("IdModelo", gridModelos.Rows.Count - 1).Value = dr("IdModelo").ToString()
    '        gridModelos("Modelo", gridModelos.Rows.Count - 1).Value = dr("Modelo").ToString()
    '        gridModelos("IdMarca", gridModelos.Rows.Count - 1).Value = dr("IdMarca").ToString()
    '        gridModelos("Marca", gridModelos.Rows.Count - 1).Value = dr("Marca").ToString()
    '        gridModelos("Precio", gridModelos.Rows.Count - 1).Value = dr("Precio").ToString()
    '    Next

    'End Sub


    Private Sub TraerModelos_By_Descripcion(Id_Modelo As String)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdModelo", SqlDbType.BigInt, 0) 'CHECAR
        BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, modeloTxt.Text)
        BaseII.CreateMyParameter("@IdMarca", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Opcion", SqlDbType.Int, 3)
        Dim dt As DataTable
        dt = BaseII.ConsultaDT("FiltroModelos")
        Dim dr As DataRow
        gridModelos.Rows.Clear()
        For Each dr In dt.Rows
            gridModelos.Rows.Add()
            gridModelos("IdModelo", gridModelos.Rows.Count - 1).Value = dr("IdModelo").ToString()
            gridModelos("Modelo", gridModelos.Rows.Count - 1).Value = dr("Modelo").ToString()
            gridModelos("IdMarca", gridModelos.Rows.Count - 1).Value = dr("IdMarca").ToString()
            gridModelos("Marca", gridModelos.Rows.Count - 1).Value = dr("Marca").ToString()
            gridModelos("Precio", gridModelos.Rows.Count - 1).Value = dr("Precio").ToString()
        Next

    End Sub





    Private Sub LlenarLabels()
        If gridModelos.Rows.Count = 0 Then
            elIdModeloLbl.Text = ""
            elModeloLbl.Text = ""
            Exit Sub
        End If
        If gridModelos(0, 0).Value <> Nothing Then
            'elIdModeloLbl.Text = gridModelos("IdMarca", gridModelos.CurrentRow.Index).Value.ToString()
            'elModeloLbl.Text 

            elIdModeloLbl.Text = gridModelos("IdModelo", gridModelos.CurrentRow.Index).Value.ToString()
            elModeloLbl.Text = gridModelos("Modelo", gridModelos.CurrentRow.Index).Value.ToString()
            laMarcaLbl.Text = gridModelos("Marca", gridModelos.CurrentRow.Index).Value.ToString()
            elPrecioLbl.Text = gridModelos("Precio", gridModelos.CurrentRow.Index).Value.ToString()

            'gridModelos("IdModelo", gridModelos.Rows.Count - 1).Value = dr("IdModelo").ToString()
            'gridModelos("Modelo", gridModelos.Rows.Count - 1).Value = dr("Modelo").ToString()
            'gridModelos("IdMarca", gridModelos.Rows.Count - 1).Value = dr("IdMarca").ToString()
            'gridModelos("Marca", gridModelos.Rows.Count - 1).Value = dr("Marca").ToString()
            'gridModelos("Precio", gridModelos.Rows.Count - 1).Value = dr("Precio").ToString()

        End If
    End Sub

    Private Sub gridIP_SelectionChanged(sender As Object, e As EventArgs) Handles gridModelos.SelectionChanged
        LlenarLabels()
    End Sub

    Private Sub bSalir_Click(sender As Object, e As EventArgs) Handles bSalir.Click
        Me.Close()
    End Sub

    Private Sub bConsultar_Click(sender As Object, e As EventArgs) Handles bConsultar.Click
        bndCatalogoTelmex = "C"

        If gridModelos.CurrentCell.Value Is Nothing Then
            IdModeloT = 0
        Else
            IdModeloT = Convert.ToInt64(gridModelos("IdModelo", gridModelos.CurrentRow.Index).Value)
        End If

        FrmCatalogoModelosT.ShowDialog()
        'TraerTodasMarcas()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If idModeloTxt.Text.Trim() = "" Then
            TraerTodosModelos()
            Exit Sub
        Else
            IdModeloT = Convert.ToInt32(idModeloTxt.Text.Trim())
            TraerModelos_By_Opcion(IdModeloT, "", 0, 2) ' Por id modelo
            'TraerModelos_By_IdModelo(IdModeloT)
        End If

        LlenarLabels()
    End Sub

    Private Sub btnBuscarPorNombre_Click(sender As Object, e As EventArgs) Handles btnBuscarPorNombre.Click
        If modeloTxt.Text.Trim() = "" Then
            TraerTodosModelos()
            Exit Sub
        Else
            'TraerModelos_By_Descripcion(modeloTxt.Text.Trim())
            TraerModelos_By_Opcion(0, modeloTxt.Text.Trim(), 0, 3) ' Por modelo descripcion
        End If

        LlenarLabels()
    End Sub


    Private Sub MarcaBtn_Click(sender As Object, e As EventArgs) Handles MarcaBtn.Click

        If String.IsNullOrEmpty(marcaCombo.Text) Then
            TraerTodosModelos()
            MessageBox.Show("Seleccione una Marca")
            Exit Sub
        Else
            'TraerModelos_By_Opcion(marcaCombo.SelectedValue)
            TraerModelos_By_Opcion(0, " ", marcaCombo.SelectedValue, 1) ' Por id marca
        End If

        LlenarLabels()

    End Sub


    Private Sub TraerModelos_By_Opcion(IdModeloO As Long, Descripcion As String, IdMarcaO As Long, Opcion As Integer)   ' YA

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdModelo", SqlDbType.BigInt, IdModeloO) 'CHECAR
        BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, Descripcion)
        BaseII.CreateMyParameter("@IdMarca", SqlDbType.BigInt, IdMarcaO)
        BaseII.CreateMyParameter("@Opcion", SqlDbType.Int, Opcion)
        Dim dt As DataTable
        dt = BaseII.ConsultaDT("FiltroModelos")
        Dim dr As DataRow
        gridModelos.Rows.Clear()
        For Each dr In dt.Rows
            gridModelos.Rows.Add()
            gridModelos("IdModelo", gridModelos.Rows.Count - 1).Value = dr("IdModelo").ToString()
            gridModelos("Modelo", gridModelos.Rows.Count - 1).Value = dr("Modelo").ToString()
            gridModelos("IdMarca", gridModelos.Rows.Count - 1).Value = dr("IdMarca").ToString()
            gridModelos("Marca", gridModelos.Rows.Count - 1).Value = dr("Marca").ToString()
            gridModelos("Precio", gridModelos.Rows.Count - 1).Value = dr("Precio").ToString()
        Next

    End Sub



End Class