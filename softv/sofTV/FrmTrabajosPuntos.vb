﻿Public Class FrmTrabajosPuntos

#Region "Eventos"
    Private Sub FrmTrabajosPuntos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Label1.ForeColor = Color.Black
        Label2.ForeColor = Color.Black
        Label3.ForeColor = Color.Black
        cmbTipo.ForeColor = Color.Black
        UspMostrarTipSerPuntosBase()
        UspMostrarTipoPuntosBase()

        If opcion <> "C" And opcion <> "M" Then
            DateTimePicker2.MinDate = DateTimePicker1.Value
            DateTimePicker1.MaxDate = DateTimePicker2.Value
        End If
      

        If opcion = "C" Then
            cmbTrabajos.Enabled = False
            ComboBox1.Enabled = False
            cmbTipServ.Enabled = False
            GroupBox1.Enabled = False
            btnGuardar.Enabled = False
            txtPorcentajeIni.Enabled = False
            UspMostrarPuntosTrabajosFechas()
        ElseIf opcion = "M" Then
            cmbTrabajos.Enabled = False
            ComboBox1.Enabled = False
            cmbTipServ.Enabled = False
            UspMostrarPuntosTrabajosFechas()
        End If

        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If opcion <> "C" And opcion <> "M" Then
            UspLlenaTrabajosPuntosFechas()
        End If
    End Sub

    Private Sub cmbTipServ_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTipServ.SelectedIndexChanged
        If opcion <> "C" And opcion <> "M" Then
            UspLlenaTrabajosPuntosFechas()
        End If
    End Sub

    Private Sub cmbTrabajos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTrabajos.SelectedIndexChanged
        GloClvTrabajo = cmbTrabajos.SelectedValue
        UspVerificarRangoTV()
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        DateTimePicker2.MinDate = DateTimePicker1.Value
    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        DateTimePicker1.MaxDate = DateTimePicker2.Value
    End Sub
#End Region

#Region "Funciones"
    Private Sub UspMostrarTipoPuntosBase()
        Try
            BaseII.limpiaParametros()
            Me.ComboBox1.DataSource = BaseII.ConsultaDT("UspMostrarTipoPuntosBase")

            Me.ComboBox1.SelectedIndex = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMostrarTipSerPuntosBase()
        Try
            BaseII.limpiaParametros()
            cmbTipServ.DataSource = BaseII.ConsultaDT("UspMostrarTipSerPuntosBase")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspLlenaTrabajosPuntosFechas()
        Try
            If Len(ComboBox1.Text) > 0 And Len(cmbTipServ.Text) > 0 Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLVTIPSER", SqlDbType.Int, Me.cmbTipServ.SelectedValue)
                BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, Me.ComboBox1.SelectedValue, 1)
                cmbTrabajos.DataSource = BaseII.ConsultaDT("UspLlenaTrabajosPuntosFechas")
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspVerificarRangoTV()
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVTRABAJO", SqlDbType.Int, GloClvTrabajo)
            DT = BaseII.ConsultaDT("UspVerificarRangoTV")
            If DT.Rows.Count > 0 Then
                If DT.Rows(0)(0).ToString = "1" Then
                    Me.btnPuntosRangoTV.Visible = True
                    Me.txtPorcentajeIni.Enabled = False
                    Me.txtPorcentajeIni.Text = "0"
                Else
                    Me.btnPuntosRangoTV.Visible = False
                    Me.txtPorcentajeIni.Enabled = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspGuardarPuntosTrabajoFechas()
        Try
            Dim vigente As Integer
            Dim dt As New DataTable
            If CheckBox1.Checked = True Then
                vigente = 1
            Else
                vigente = 0
            End If
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ID", SqlDbType.Int, GloClvPuntosTrabajosFechas)
            BaseII.CreateMyParameter("@CLVTRABAJO", SqlDbType.Int, CInt(Me.cmbTrabajos.SelectedValue))
            BaseII.CreateMyParameter("@FECHAINICIAL", SqlDbType.DateTime, DateTimePicker1.Value)
            BaseII.CreateMyParameter("@FECHAFINAL", SqlDbType.DateTime, DateTimePicker2.Value)
            BaseII.CreateMyParameter("@VIGENTE", SqlDbType.Int, vigente)
            BaseII.CreateMyParameter("@PUNTOS", SqlDbType.Decimal, CDec(Me.txtPorcentajeIni.Text))
            dt = BaseII.ConsultaDT("UspGuardarPuntosTrabajoFechas")
            If (dt.Rows.Count) > 0 Then
                MsgBox(dt.Rows(0)(0).ToString, MsgBoxStyle.Information, "Puntos de Trabajos")
                GloClvPuntosTrabajosFechas = CInt(dt.Rows(0)(1).ToString)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMostrarPuntosTrabajosFechas()
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ID", SqlDbType.Int, GloClvPuntosTrabajosFechas)
            DT = BaseII.ConsultaDT("UspMostrarPuntosTrabajosFechas")
            If (DT.Rows.Count) > 0 Then

                Me.DateTimePicker1.Value = DT.Rows(0)(2).ToString
                Me.DateTimePicker2.Value = DT.Rows(0)(3).ToString
                If DT.Rows(0)(4).ToString = "1" Then
                    CheckBox1.Checked = True
                Else
                    CheckBox1.Checked = False
                End If
                Me.txtPorcentajeIni.Text = DT.Rows(0)(5).ToString
                Me.ComboBox1.SelectedValue = DT.Rows(0)(7).ToString
                Me.cmbTipServ.SelectedValue = CInt(DT.Rows(0)(6).ToString)
                UspLlenaTrabajosPuntosFechas()
                Me.cmbTrabajos.SelectedValue = CInt(DT.Rows(0)(1).ToString)
                GloClvTrabajo = CInt(DT.Rows(0)(1).ToString)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region

#Region "Salir"
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.close()
    End Sub
#End Region

#Region "Guardar"
    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If Len(Me.txtPorcentajeIni.Text) = 0 Then
            MsgBox("Los puntos estan en blanco, por favor agregar los puntos", MsgBoxStyle.Information, "Puntos de Trabajos")
            Exit Sub
        End If
        UspGuardarPuntosTrabajoFechas()
        cmbTrabajos.Enabled = False
        ComboBox1.Enabled = False
        cmbTipServ.Enabled = False
    End Sub
#End Region
   
#Region "Rango TVs"
    Private Sub btnPuntosRangoTV_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPuntosRangoTV.Click
        If opcion <> "C" And opcion <> "M" Then
            If (Me.txtPorcentajeIni.Text) = 0 Then
                MsgBox("Los puntos estan en blanco, por favor agregar los puntos", MsgBoxStyle.Information, "Puntos de Trabajos")
                Exit Sub
            End If
            UspGuardarPuntosTrabajoFechas()
            cmbTrabajos.Enabled = False
            ComboBox1.Enabled = False
            cmbTipServ.Enabled = False
        End If
        frmPuntosPorRangoTVTrabajosFechas.ShowDialog()
    End Sub
#End Region

    
   
End Class