<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmClientesTel
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ContratonetLabel As System.Windows.Forms.Label
        Dim ContratoLabel As System.Windows.Forms.Label
        Dim StatusLabel As System.Windows.Forms.Label
        Dim Clv_cablemodemLabel As System.Windows.Forms.Label
        Dim Fecha_ActivacionLabel As System.Windows.Forms.Label
        Dim Fecha_SuspensionLabel As System.Windows.Forms.Label
        Dim Fecha_BajaLabel As System.Windows.Forms.Label
        Dim ObsLabel As System.Windows.Forms.Label
        Dim Clv_unicanetLabel As System.Windows.Forms.Label
        Dim ContratonetLabel1 As System.Windows.Forms.Label
        Dim Referencia_pagoLabel As System.Windows.Forms.Label
        Dim StatusLabel1 As System.Windows.Forms.Label
        Dim Fecha_SolicitudLabel As System.Windows.Forms.Label
        Dim Fecha_InstalacionLabel As System.Windows.Forms.Label
        Dim Fecha_suspensionLabel1 As System.Windows.Forms.Label
        Dim Fecha_BajaLabel1 As System.Windows.Forms.Label
        Dim Fecha_Ult_pagoLabel As System.Windows.Forms.Label
        Dim PrimerMensualidadLabel As System.Windows.Forms.Label
        Dim FacturaLabel As System.Windows.Forms.Label
        Dim Clv_promocionLabel As System.Windows.Forms.Label
        Dim ObsLabel1 As System.Windows.Forms.Label
        Dim Clv_motcanLabel As System.Windows.Forms.Label
        Dim CortesiaLabel As System.Windows.Forms.Label
        Dim No_extensionesLabel1 As System.Windows.Forms.Label
        Dim CMBLabel40 As System.Windows.Forms.Label
        Dim CMBLabel1 As System.Windows.Forms.Label
        Dim CMBLabel2 As System.Windows.Forms.Label
        Dim CMBTextBox4 As System.Windows.Forms.TextBox
        Dim CMBTextBox3 As System.Windows.Forms.TextBox
        Dim CMBTextBox2 As System.Windows.Forms.TextBox
        Dim CMBLabel4 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim Label9 As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim Clv_servicioLabel As System.Windows.Forms.Label
        Dim Clv_unicanetLabel1 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim CMBLabel46 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim LblPlazoForzoso As System.Windows.Forms.Label
        Dim CMBTelUsuarioCapturo As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmClientesTel))
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.BajaTelTextBox1 = New System.Windows.Forms.MaskedTextBox()
        Me.Consulta_ClientesTelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia2 = New sofTV.DataSetLidia2()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.StatusMTABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SuspensionTelTextBox5 = New System.Windows.Forms.MaskedTextBox()
        Me.MacCablemodemTextBox = New System.Windows.Forms.TextBox()
        Me.ActivacionTextBox2 = New System.Windows.Forms.MaskedTextBox()
        Me.No_extensionesNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.CMBTextBox1 = New System.Windows.Forms.TextBox()
        Me.BindingNavigator3 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem2 = New System.Windows.Forms.ToolStripButton()
        Me.GuardarToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ObsTextBox = New System.Windows.Forms.TextBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.FueraAreaMaskedTextBox1 = New System.Windows.Forms.MaskedTextBox()
        Me.Consulta_ContTelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.cbTelUsuarioCapturo = New System.Windows.Forms.ComboBox()
        Me.LblPlazoForzoso1 = New System.Windows.Forms.Label()
        Me.ComboBox3 = New System.Windows.Forms.ComboBox()
        Me.MuestraMotivoCancelacionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia = New sofTV.DataSetLidia()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Clv_telefonoXtraTextBox = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.BajaTextBox6 = New System.Windows.Forms.MaskedTextBox()
        Me.Ult_PAgoTextBox3 = New System.Windows.Forms.MaskedTextBox()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.SuspensionTextBox4 = New System.Windows.Forms.MaskedTextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.ConRelCteDescuentoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.InstalacionTextBox1 = New System.Windows.Forms.MaskedTextBox()
        Me.Numero_telTextBox = New System.Windows.Forms.TextBox()
        Me.SolicitudTextBox1 = New System.Windows.Forms.MaskedTextBox()
        Me.ComboBox5 = New System.Windows.Forms.ComboBox()
        Me.MuestraTipoPromocionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.StatusTelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.Clv_servicioTextBox = New System.Windows.Forms.TextBox()
        Me.Referencia_pagoTextBox = New System.Windows.Forms.TextBox()
        Me.BindingNavigator4 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem3 = New System.Windows.Forms.ToolStripButton()
        Me.GuardarToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.CMBTextBox28 = New System.Windows.Forms.TextBox()
        Me.Ultimo_MesTextBox = New System.Windows.Forms.TextBox()
        Me.Ultimo_anioTextBox = New System.Windows.Forms.TextBox()
        Me.FacturaTextBox = New System.Windows.Forms.TextBox()
        Me.ObsTextBox1 = New System.Windows.Forms.TextBox()
        Me.CortesiaCheckBox = New System.Windows.Forms.CheckBox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.ComboBox6 = New System.Windows.Forms.ComboBox()
        Me.Consulta_ConTTelAdicBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRA_TiposPaqBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BajaAdicTextBox1 = New System.Windows.Forms.MaskedTextBox()
        Me.InstalacionAdicTextBox3 = New System.Windows.Forms.MaskedTextBox()
        Me.SolicitudAdicTextBox2 = New System.Windows.Forms.MaskedTextBox()
        Me.SuspensionAdicTextBox4 = New System.Windows.Forms.MaskedTextBox()
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.CMBTextBox6 = New System.Windows.Forms.TextBox()
        Me.BindingNavigator6 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripButton()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.BindingNavigator5 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigator2 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem1 = New System.Windows.Forms.ToolStripButton()
        Me.GuardarToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.GuardarToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.Clv_telefonoTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_cablemodemTextBox = New System.Windows.Forms.TextBox()
        Me.Muestra_ClientesTelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.StatusTextBox = New System.Windows.Forms.TextBox()
        Me.Procedimientosarnoldo4 = New sofTV.Procedimientosarnoldo4()
        Me.ContratonetTextBox = New System.Windows.Forms.TextBox()
        Me.ContratoTextBox = New System.Windows.Forms.TextBox()
        Me.StatusTextBox1 = New System.Windows.Forms.TextBox()
        Me.Clv_unicanetTextBox = New System.Windows.Forms.TextBox()
        Me.ContratonetTextBox1 = New System.Windows.Forms.TextBox()
        Me.PrimerMensualidadTextBox = New System.Windows.Forms.TextBox()
        Me.MUESTRACONTTELBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACONTTELTableAdapter = New sofTV.DataSetLidia2TableAdapters.MUESTRACONTTELTableAdapter()
        Me.Muestra_ClientesTelTableAdapter = New sofTV.DataSetLidia2TableAdapters.Muestra_ClientesTelTableAdapter()
        Me.StatusMTATableAdapter = New sofTV.DataSetLidia2TableAdapters.StatusMTATableAdapter()
        Me.StatusTelTableAdapter = New sofTV.DataSetLidia2TableAdapters.StatusTelTableAdapter()
        Me.MuestraMotivoCancelacionTableAdapter = New sofTV.DataSetLidiaTableAdapters.MuestraMotivoCancelacionTableAdapter()
        Me.Consulta_ContTelTableAdapter = New sofTV.DataSetLidia2TableAdapters.Consulta_ContTelTableAdapter()
        Me.Consulta_ClientesTelTableAdapter = New sofTV.DataSetLidia2TableAdapters.Consulta_ClientesTelTableAdapter()
        Me.Consulta_ConTTelAdicTableAdapter = New sofTV.DataSetLidia2TableAdapters.Consulta_ConTTelAdicTableAdapter()
        Me.Clv_servicioTextBox1 = New System.Windows.Forms.TextBox()
        Me.Clv_unicanetTextBox1 = New System.Windows.Forms.TextBox()
        Me.MUESTRAPAQ_ADICBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRAPAQ_ADICTableAdapter = New sofTV.DataSetLidia2TableAdapters.MUESTRAPAQ_ADICTableAdapter()
        Me.Muestra_EquiposTelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Muestra_EquiposTelTableAdapter = New sofTV.DataSetLidia2TableAdapters.Muestra_EquiposTelTableAdapter()
        Me.MuestraTipoPromocionTableAdapter = New sofTV.DataSetLidia2TableAdapters.MuestraTipoPromocionTableAdapter()
        Me.ChecaRelCteDescuentoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ChecaRelCteDescuentoTableAdapter = New sofTV.DataSetEricTableAdapters.ChecaRelCteDescuentoTableAdapter()
        Me.ConRelCteDescuentoTableAdapter = New sofTV.DataSetEricTableAdapters.ConRelCteDescuentoTableAdapter()
        Me.MUESTRA_TiposPaqTableAdapter = New sofTV.DataSetLidia2TableAdapters.MUESTRA_TiposPaqTableAdapter()
        Me.DataSetEdgarRev2 = New sofTV.DataSetEdgarRev2()
        Me.VerAcceso_ChecaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VerAcceso_ChecaTableAdapter = New sofTV.DataSetEdgarRev2TableAdapters.VerAcceso_ChecaTableAdapter()
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.Dame_clvtipusuarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_clvtipusuarioTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Dame_clvtipusuarioTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        ContratonetLabel = New System.Windows.Forms.Label()
        ContratoLabel = New System.Windows.Forms.Label()
        StatusLabel = New System.Windows.Forms.Label()
        Clv_cablemodemLabel = New System.Windows.Forms.Label()
        Fecha_ActivacionLabel = New System.Windows.Forms.Label()
        Fecha_SuspensionLabel = New System.Windows.Forms.Label()
        Fecha_BajaLabel = New System.Windows.Forms.Label()
        ObsLabel = New System.Windows.Forms.Label()
        Clv_unicanetLabel = New System.Windows.Forms.Label()
        ContratonetLabel1 = New System.Windows.Forms.Label()
        Referencia_pagoLabel = New System.Windows.Forms.Label()
        StatusLabel1 = New System.Windows.Forms.Label()
        Fecha_SolicitudLabel = New System.Windows.Forms.Label()
        Fecha_InstalacionLabel = New System.Windows.Forms.Label()
        Fecha_suspensionLabel1 = New System.Windows.Forms.Label()
        Fecha_BajaLabel1 = New System.Windows.Forms.Label()
        Fecha_Ult_pagoLabel = New System.Windows.Forms.Label()
        PrimerMensualidadLabel = New System.Windows.Forms.Label()
        FacturaLabel = New System.Windows.Forms.Label()
        Clv_promocionLabel = New System.Windows.Forms.Label()
        ObsLabel1 = New System.Windows.Forms.Label()
        Clv_motcanLabel = New System.Windows.Forms.Label()
        CortesiaLabel = New System.Windows.Forms.Label()
        No_extensionesLabel1 = New System.Windows.Forms.Label()
        CMBLabel40 = New System.Windows.Forms.Label()
        CMBLabel1 = New System.Windows.Forms.Label()
        CMBLabel2 = New System.Windows.Forms.Label()
        CMBTextBox4 = New System.Windows.Forms.TextBox()
        CMBTextBox3 = New System.Windows.Forms.TextBox()
        CMBTextBox2 = New System.Windows.Forms.TextBox()
        CMBLabel4 = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        Label9 = New System.Windows.Forms.Label()
        Label11 = New System.Windows.Forms.Label()
        Clv_servicioLabel = New System.Windows.Forms.Label()
        Clv_unicanetLabel1 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        CMBLabel46 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        LblPlazoForzoso = New System.Windows.Forms.Label()
        CMBTelUsuarioCapturo = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Me.Panel3.SuspendLayout()
        CType(Me.Consulta_ClientesTelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusMTABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.No_extensionesNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingNavigator3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.Consulta_ContTelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraMotivoCancelacionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConRelCteDescuentoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipoPromocionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusTelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingNavigator4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.Consulta_ConTTelAdicBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRA_TiposPaqBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingNavigator6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator6.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.BindingNavigator5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator5.SuspendLayout()
        CType(Me.BindingNavigator2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator2.SuspendLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        CType(Me.Muestra_ClientesTelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACONTTELBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAPAQ_ADICBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Muestra_EquiposTelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChecaRelCteDescuentoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VerAcceso_ChecaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_clvtipusuarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ContratonetLabel
        '
        ContratonetLabel.AutoSize = True
        ContratonetLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContratonetLabel.Location = New System.Drawing.Point(843, 639)
        ContratonetLabel.Name = "ContratonetLabel"
        ContratonetLabel.Size = New System.Drawing.Size(83, 15)
        ContratonetLabel.TabIndex = 0
        ContratonetLabel.Text = "contratonet:"
        '
        'ContratoLabel
        '
        ContratoLabel.AutoSize = True
        ContratoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContratoLabel.Location = New System.Drawing.Point(792, 668)
        ContratoLabel.Name = "ContratoLabel"
        ContratoLabel.Size = New System.Drawing.Size(63, 15)
        ContratoLabel.TabIndex = 2
        ContratoLabel.Text = "contrato:"
        '
        'StatusLabel
        '
        StatusLabel.AutoSize = True
        StatusLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        StatusLabel.ForeColor = System.Drawing.Color.LightSlateGray
        StatusLabel.Location = New System.Drawing.Point(95, 67)
        StatusLabel.Name = "StatusLabel"
        StatusLabel.Size = New System.Drawing.Size(51, 15)
        StatusLabel.TabIndex = 6
        StatusLabel.Text = "Status:"
        '
        'Clv_cablemodemLabel
        '
        Clv_cablemodemLabel.AutoSize = True
        Clv_cablemodemLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_cablemodemLabel.Location = New System.Drawing.Point(778, 766)
        Clv_cablemodemLabel.Name = "Clv_cablemodemLabel"
        Clv_cablemodemLabel.Size = New System.Drawing.Size(117, 15)
        Clv_cablemodemLabel.TabIndex = 8
        Clv_cablemodemLabel.Text = "Clv cablemodem:"
        '
        'Fecha_ActivacionLabel
        '
        Fecha_ActivacionLabel.AutoSize = True
        Fecha_ActivacionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_ActivacionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_ActivacionLabel.Location = New System.Drawing.Point(234, 152)
        Fecha_ActivacionLabel.Name = "Fecha_ActivacionLabel"
        Fecha_ActivacionLabel.Size = New System.Drawing.Size(118, 15)
        Fecha_ActivacionLabel.TabIndex = 12
        Fecha_ActivacionLabel.Text = "Fecha Activacion:"
        '
        'Fecha_SuspensionLabel
        '
        Fecha_SuspensionLabel.AutoSize = True
        Fecha_SuspensionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_SuspensionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_SuspensionLabel.Location = New System.Drawing.Point(223, 176)
        Fecha_SuspensionLabel.Name = "Fecha_SuspensionLabel"
        Fecha_SuspensionLabel.Size = New System.Drawing.Size(129, 15)
        Fecha_SuspensionLabel.TabIndex = 14
        Fecha_SuspensionLabel.Text = "Fecha Suspension:"
        '
        'Fecha_BajaLabel
        '
        Fecha_BajaLabel.AutoSize = True
        Fecha_BajaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_BajaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_BajaLabel.Location = New System.Drawing.Point(269, 200)
        Fecha_BajaLabel.Name = "Fecha_BajaLabel"
        Fecha_BajaLabel.Size = New System.Drawing.Size(83, 15)
        Fecha_BajaLabel.TabIndex = 16
        Fecha_BajaLabel.Text = "Fecha Baja:"
        '
        'ObsLabel
        '
        ObsLabel.AutoSize = True
        ObsLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ObsLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ObsLabel.Location = New System.Drawing.Point(15, 218)
        ObsLabel.Name = "ObsLabel"
        ObsLabel.Size = New System.Drawing.Size(105, 15)
        ObsLabel.TabIndex = 18
        ObsLabel.Text = "Observaciones:"
        '
        'Clv_unicanetLabel
        '
        Clv_unicanetLabel.AutoSize = True
        Clv_unicanetLabel.Location = New System.Drawing.Point(832, 589)
        Clv_unicanetLabel.Name = "Clv_unicanetLabel"
        Clv_unicanetLabel.Size = New System.Drawing.Size(68, 13)
        Clv_unicanetLabel.TabIndex = 0
        Clv_unicanetLabel.Text = "clv unicanet:"
        '
        'ContratonetLabel1
        '
        ContratonetLabel1.AutoSize = True
        ContratonetLabel1.Location = New System.Drawing.Point(832, 615)
        ContratonetLabel1.Name = "ContratonetLabel1"
        ContratonetLabel1.Size = New System.Drawing.Size(64, 13)
        ContratonetLabel1.TabIndex = 2
        ContratonetLabel1.Text = "contratonet:"
        '
        'Referencia_pagoLabel
        '
        Referencia_pagoLabel.AutoSize = True
        Referencia_pagoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Referencia_pagoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Referencia_pagoLabel.Location = New System.Drawing.Point(33, 153)
        Referencia_pagoLabel.Name = "Referencia_pagoLabel"
        Referencia_pagoLabel.Size = New System.Drawing.Size(121, 15)
        Referencia_pagoLabel.TabIndex = 4
        Referencia_pagoLabel.Text = "Referencia pago :"
        '
        'StatusLabel1
        '
        StatusLabel1.AutoSize = True
        StatusLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        StatusLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        StatusLabel1.Location = New System.Drawing.Point(99, 102)
        StatusLabel1.Name = "StatusLabel1"
        StatusLabel1.Size = New System.Drawing.Size(55, 15)
        StatusLabel1.TabIndex = 8
        StatusLabel1.Text = "Status :"
        '
        'Fecha_SolicitudLabel
        '
        Fecha_SolicitudLabel.AutoSize = True
        Fecha_SolicitudLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_SolicitudLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_SolicitudLabel.Location = New System.Drawing.Point(332, 158)
        Fecha_SolicitudLabel.Name = "Fecha_SolicitudLabel"
        Fecha_SolicitudLabel.Size = New System.Drawing.Size(110, 15)
        Fecha_SolicitudLabel.TabIndex = 10
        Fecha_SolicitudLabel.Text = "Fecha Solicitud:"
        '
        'Fecha_InstalacionLabel
        '
        Fecha_InstalacionLabel.AutoSize = True
        Fecha_InstalacionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_InstalacionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_InstalacionLabel.Location = New System.Drawing.Point(318, 181)
        Fecha_InstalacionLabel.Name = "Fecha_InstalacionLabel"
        Fecha_InstalacionLabel.Size = New System.Drawing.Size(124, 15)
        Fecha_InstalacionLabel.TabIndex = 12
        Fecha_InstalacionLabel.Text = "Fecha Instalacion:"
        '
        'Fecha_suspensionLabel1
        '
        Fecha_suspensionLabel1.AutoSize = True
        Fecha_suspensionLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_suspensionLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_suspensionLabel1.Location = New System.Drawing.Point(315, 202)
        Fecha_suspensionLabel1.Name = "Fecha_suspensionLabel1"
        Fecha_suspensionLabel1.Size = New System.Drawing.Size(127, 15)
        Fecha_suspensionLabel1.TabIndex = 14
        Fecha_suspensionLabel1.Text = "Fecha suspension:"
        '
        'Fecha_BajaLabel1
        '
        Fecha_BajaLabel1.AutoSize = True
        Fecha_BajaLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_BajaLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_BajaLabel1.Location = New System.Drawing.Point(359, 249)
        Fecha_BajaLabel1.Name = "Fecha_BajaLabel1"
        Fecha_BajaLabel1.Size = New System.Drawing.Size(83, 15)
        Fecha_BajaLabel1.TabIndex = 16
        Fecha_BajaLabel1.Text = "Fecha Baja:"
        '
        'Fecha_Ult_pagoLabel
        '
        Fecha_Ult_pagoLabel.AutoSize = True
        Fecha_Ult_pagoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_Ult_pagoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_Ult_pagoLabel.Location = New System.Drawing.Point(309, 274)
        Fecha_Ult_pagoLabel.Name = "Fecha_Ult_pagoLabel"
        Fecha_Ult_pagoLabel.Size = New System.Drawing.Size(133, 15)
        Fecha_Ult_pagoLabel.TabIndex = 18
        Fecha_Ult_pagoLabel.Text = "Fecha Último Pago:"
        '
        'PrimerMensualidadLabel
        '
        PrimerMensualidadLabel.AutoSize = True
        PrimerMensualidadLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PrimerMensualidadLabel.ForeColor = System.Drawing.Color.LightSlateGray
        PrimerMensualidadLabel.Location = New System.Drawing.Point(9, 182)
        PrimerMensualidadLabel.Name = "PrimerMensualidadLabel"
        PrimerMensualidadLabel.Size = New System.Drawing.Size(145, 15)
        PrimerMensualidadLabel.TabIndex = 20
        PrimerMensualidadLabel.Text = "Primer Mensualidad :"
        '
        'FacturaLabel
        '
        FacturaLabel.AutoSize = True
        FacturaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FacturaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        FacturaLabel.Location = New System.Drawing.Point(344, 109)
        FacturaLabel.Name = "FacturaLabel"
        FacturaLabel.Size = New System.Drawing.Size(75, 15)
        FacturaLabel.TabIndex = 26
        FacturaLabel.Text = "# Factura :"
        '
        'Clv_promocionLabel
        '
        Clv_promocionLabel.AutoSize = True
        Clv_promocionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_promocionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_promocionLabel.Location = New System.Drawing.Point(-1, 129)
        Clv_promocionLabel.Name = "Clv_promocionLabel"
        Clv_promocionLabel.Size = New System.Drawing.Size(158, 15)
        Clv_promocionLabel.TabIndex = 28
        Clv_promocionLabel.Text = "Cobro de Contratación :"
        '
        'ObsLabel1
        '
        ObsLabel1.AutoSize = True
        ObsLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ObsLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        ObsLabel1.Location = New System.Drawing.Point(9, 253)
        ObsLabel1.Name = "ObsLabel1"
        ObsLabel1.Size = New System.Drawing.Size(109, 15)
        ObsLabel1.TabIndex = 30
        ObsLabel1.Text = "Observaciones :"
        '
        'Clv_motcanLabel
        '
        Clv_motcanLabel.AutoSize = True
        Clv_motcanLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_motcanLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_motcanLabel.Location = New System.Drawing.Point(9, 304)
        Clv_motcanLabel.Name = "Clv_motcanLabel"
        Clv_motcanLabel.Size = New System.Drawing.Size(152, 15)
        Clv_motcanLabel.TabIndex = 32
        Clv_motcanLabel.Text = "Motivo de Cancelación"
        '
        'CortesiaLabel
        '
        CortesiaLabel.AutoSize = True
        CortesiaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CortesiaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CortesiaLabel.Location = New System.Drawing.Point(394, 25)
        CortesiaLabel.Name = "CortesiaLabel"
        CortesiaLabel.Size = New System.Drawing.Size(68, 15)
        CortesiaLabel.TabIndex = 34
        CortesiaLabel.Text = "Cortesía :"
        '
        'No_extensionesLabel1
        '
        No_extensionesLabel1.AutoSize = True
        No_extensionesLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        No_extensionesLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        No_extensionesLabel1.Location = New System.Drawing.Point(33, 104)
        No_extensionesLabel1.Name = "No_extensionesLabel1"
        No_extensionesLabel1.Size = New System.Drawing.Size(111, 15)
        No_extensionesLabel1.TabIndex = 48
        No_extensionesLabel1.Text = "No Extensiones:"
        No_extensionesLabel1.Visible = False
        '
        'CMBLabel40
        '
        CMBLabel40.AutoSize = True
        CMBLabel40.BackColor = System.Drawing.Color.Transparent
        CMBLabel40.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel40.ForeColor = System.Drawing.Color.Black
        CMBLabel40.Location = New System.Drawing.Point(9, 34)
        CMBLabel40.Name = "CMBLabel40"
        CMBLabel40.Size = New System.Drawing.Size(137, 18)
        CMBLabel40.TabIndex = 32
        CMBLabel40.Text = "Datos del Equipo"
        '
        'CMBLabel1
        '
        CMBLabel1.AutoSize = True
        CMBLabel1.BackColor = System.Drawing.Color.Transparent
        CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        CMBLabel1.Location = New System.Drawing.Point(9, 52)
        CMBLabel1.Name = "CMBLabel1"
        CMBLabel1.Size = New System.Drawing.Size(148, 16)
        CMBLabel1.TabIndex = 51
        CMBLabel1.Text = "Número Telefónico :"
        '
        'CMBLabel2
        '
        CMBLabel2.AutoSize = True
        CMBLabel2.BackColor = System.Drawing.Color.Transparent
        CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        CMBLabel2.Location = New System.Drawing.Point(47, 30)
        CMBLabel2.Name = "CMBLabel2"
        CMBLabel2.Size = New System.Drawing.Size(110, 16)
        CMBLabel2.TabIndex = 52
        CMBLabel2.Text = "Plan Tarifario :"
        '
        'CMBTextBox4
        '
        CMBTextBox4.BackColor = System.Drawing.Color.Silver
        CMBTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None
        CMBTextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBTextBox4.ForeColor = System.Drawing.Color.Black
        CMBTextBox4.Location = New System.Drawing.Point(455, 64)
        CMBTextBox4.Name = "CMBTextBox4"
        CMBTextBox4.ReadOnly = True
        CMBTextBox4.Size = New System.Drawing.Size(69, 15)
        CMBTextBox4.TabIndex = 55
        CMBTextBox4.TabStop = False
        CMBTextBox4.Text = "Año"
        CMBTextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CMBTextBox3
        '
        CMBTextBox3.BackColor = System.Drawing.Color.Silver
        CMBTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None
        CMBTextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBTextBox3.ForeColor = System.Drawing.Color.Black
        CMBTextBox3.Location = New System.Drawing.Point(394, 64)
        CMBTextBox3.Name = "CMBTextBox3"
        CMBTextBox3.ReadOnly = True
        CMBTextBox3.Size = New System.Drawing.Size(62, 15)
        CMBTextBox3.TabIndex = 54
        CMBTextBox3.TabStop = False
        CMBTextBox3.Text = "Mes"
        CMBTextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CMBTextBox2
        '
        CMBTextBox2.BackColor = System.Drawing.Color.Silver
        CMBTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        CMBTextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBTextBox2.ForeColor = System.Drawing.Color.Black
        CMBTextBox2.Location = New System.Drawing.Point(394, 49)
        CMBTextBox2.Name = "CMBTextBox2"
        CMBTextBox2.ReadOnly = True
        CMBTextBox2.Size = New System.Drawing.Size(130, 15)
        CMBTextBox2.TabIndex = 53
        CMBTextBox2.TabStop = False
        CMBTextBox2.Text = "Último Pagado"
        CMBTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CMBLabel4
        '
        CMBLabel4.AutoSize = True
        CMBLabel4.BackColor = System.Drawing.Color.Transparent
        CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel4.ForeColor = System.Drawing.Color.Black
        CMBLabel4.Location = New System.Drawing.Point(5, 36)
        CMBLabel4.Name = "CMBLabel4"
        CMBLabel4.Size = New System.Drawing.Size(219, 18)
        CMBLabel4.TabIndex = 32
        CMBLabel4.Text = "Datos del Paquete Adicional"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Label6.Location = New System.Drawing.Point(167, 67)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(55, 15)
        Label6.TabIndex = 6
        Label6.Text = "Status :"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label7.ForeColor = System.Drawing.Color.LightSlateGray
        Label7.Location = New System.Drawing.Point(104, 176)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(110, 15)
        Label7.TabIndex = 12
        Label7.Text = "Fecha Solicitud:"
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Label8.Location = New System.Drawing.Point(93, 213)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(124, 15)
        Label8.TabIndex = 14
        Label8.Text = "Fecha Instalación:"
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Label9.Location = New System.Drawing.Point(134, 286)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(83, 15)
        Label9.TabIndex = 16
        Label9.Text = "Fecha Baja:"
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label11.ForeColor = System.Drawing.Color.LightSlateGray
        Label11.Location = New System.Drawing.Point(88, 249)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(129, 15)
        Label11.TabIndex = 53
        Label11.Text = "Fecha Suspensión:"
        '
        'Clv_servicioLabel
        '
        Clv_servicioLabel.AutoSize = True
        Clv_servicioLabel.Location = New System.Drawing.Point(832, 714)
        Clv_servicioLabel.Name = "Clv_servicioLabel"
        Clv_servicioLabel.Size = New System.Drawing.Size(63, 13)
        Clv_servicioLabel.TabIndex = 53
        Clv_servicioLabel.Text = "clv servicio:"
        '
        'Clv_unicanetLabel1
        '
        Clv_unicanetLabel1.AutoSize = True
        Clv_unicanetLabel1.Location = New System.Drawing.Point(827, 740)
        Clv_unicanetLabel1.Name = "Clv_unicanetLabel1"
        Clv_unicanetLabel1.Size = New System.Drawing.Size(68, 13)
        Clv_unicanetLabel1.TabIndex = 54
        Clv_unicanetLabel1.Text = "clv unicanet:"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(104, 102)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(118, 15)
        Label1.TabIndex = 67
        Label1.Text = "Tipo de Servicio :"
        '
        'CMBLabel46
        '
        CMBLabel46.AutoSize = True
        CMBLabel46.BackColor = System.Drawing.Color.Transparent
        CMBLabel46.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel46.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        CMBLabel46.Location = New System.Drawing.Point(3, 76)
        CMBLabel46.Name = "CMBLabel46"
        CMBLabel46.Size = New System.Drawing.Size(149, 15)
        CMBLabel46.TabIndex = 63
        CMBLabel46.Text = "Número Telefónico 2 :"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(41, 208)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(111, 15)
        Label2.TabIndex = 85
        Label2.Text = "No Extensiones:"
        '
        'LblPlazoForzoso
        '
        LblPlazoForzoso.AutoSize = True
        LblPlazoForzoso.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        LblPlazoForzoso.ForeColor = System.Drawing.Color.LightSlateGray
        LblPlazoForzoso.Location = New System.Drawing.Point(46, 233)
        LblPlazoForzoso.Name = "LblPlazoForzoso"
        LblPlazoForzoso.Size = New System.Drawing.Size(106, 15)
        LblPlazoForzoso.TabIndex = 88
        LblPlazoForzoso.Text = "Plazo Forzoso :"
        '
        'CMBTelUsuarioCapturo
        '
        CMBTelUsuarioCapturo.AutoSize = True
        CMBTelUsuarioCapturo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBTelUsuarioCapturo.ForeColor = System.Drawing.Color.LightSlateGray
        CMBTelUsuarioCapturo.Location = New System.Drawing.Point(288, 304)
        CMBTelUsuarioCapturo.Name = "CMBTelUsuarioCapturo"
        CMBTelUsuarioCapturo.Size = New System.Drawing.Size(57, 15)
        CMBTelUsuarioCapturo.TabIndex = 90
        CMBTelUsuarioCapturo.Text = "Capturó"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Label3.Location = New System.Drawing.Point(332, 224)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(101, 15)
        Label3.TabIndex = 92
        Label3.Text = "Fuera de Área:"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.BajaTelTextBox1)
        Me.Panel3.Controls.Add(Me.ComboBox1)
        Me.Panel3.Controls.Add(Me.SuspensionTelTextBox5)
        Me.Panel3.Controls.Add(Me.MacCablemodemTextBox)
        Me.Panel3.Controls.Add(Me.ActivacionTextBox2)
        Me.Panel3.Controls.Add(CMBLabel40)
        Me.Panel3.Controls.Add(No_extensionesLabel1)
        Me.Panel3.Controls.Add(Me.No_extensionesNumericUpDown)
        Me.Panel3.Controls.Add(Me.CMBTextBox1)
        Me.Panel3.Controls.Add(Me.BindingNavigator3)
        Me.Panel3.Controls.Add(StatusLabel)
        Me.Panel3.Controls.Add(Fecha_ActivacionLabel)
        Me.Panel3.Controls.Add(Fecha_SuspensionLabel)
        Me.Panel3.Controls.Add(Fecha_BajaLabel)
        Me.Panel3.Controls.Add(ObsLabel)
        Me.Panel3.Controls.Add(Me.ObsTextBox)
        Me.Panel3.Location = New System.Drawing.Point(10, 4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(546, 312)
        Me.Panel3.TabIndex = 0
        '
        'BajaTelTextBox1
        '
        Me.BajaTelTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BajaTelTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ClientesTelBindingSource, "Fecha_Baja", True))
        Me.BajaTelTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BajaTelTextBox1.Location = New System.Drawing.Point(358, 198)
        Me.BajaTelTextBox1.Mask = "00/00/0000"
        Me.BajaTelTextBox1.Name = "BajaTelTextBox1"
        Me.BajaTelTextBox1.Size = New System.Drawing.Size(100, 21)
        Me.BajaTelTextBox1.TabIndex = 62
        Me.BajaTelTextBox1.ValidatingType = GetType(Date)
        '
        'Consulta_ClientesTelBindingSource
        '
        Me.Consulta_ClientesTelBindingSource.DataMember = "Consulta_ClientesTel"
        Me.Consulta_ClientesTelBindingSource.DataSource = Me.DataSetLidia2
        '
        'DataSetLidia2
        '
        Me.DataSetLidia2.DataSetName = "DataSetLidia2"
        Me.DataSetLidia2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ComboBox1
        '
        Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.Consulta_ClientesTelBindingSource, "status", True))
        Me.ComboBox1.DataSource = Me.StatusMTABindingSource
        Me.ComboBox1.DisplayMember = "Concepto"
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(161, 67)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(180, 23)
        Me.ComboBox1.TabIndex = 52
        Me.ComboBox1.ValueMember = "Clv_StatusMTA"
        '
        'StatusMTABindingSource
        '
        Me.StatusMTABindingSource.DataMember = "StatusMTA"
        Me.StatusMTABindingSource.DataSource = Me.DataSetLidia2
        '
        'SuspensionTelTextBox5
        '
        Me.SuspensionTelTextBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SuspensionTelTextBox5.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ClientesTelBindingSource, "Fecha_Suspension", True))
        Me.SuspensionTelTextBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuspensionTelTextBox5.Location = New System.Drawing.Point(358, 174)
        Me.SuspensionTelTextBox5.Mask = "00/00/0000"
        Me.SuspensionTelTextBox5.Name = "SuspensionTelTextBox5"
        Me.SuspensionTelTextBox5.Size = New System.Drawing.Size(100, 21)
        Me.SuspensionTelTextBox5.TabIndex = 61
        Me.SuspensionTelTextBox5.ValidatingType = GetType(Date)
        '
        'MacCablemodemTextBox
        '
        Me.MacCablemodemTextBox.BackColor = System.Drawing.Color.White
        Me.MacCablemodemTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MacCablemodemTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ClientesTelBindingSource, "maccablemodem", True))
        Me.MacCablemodemTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MacCablemodemTextBox.ForeColor = System.Drawing.Color.DarkRed
        Me.MacCablemodemTextBox.Location = New System.Drawing.Point(161, 33)
        Me.MacCablemodemTextBox.Name = "MacCablemodemTextBox"
        Me.MacCablemodemTextBox.ReadOnly = True
        Me.MacCablemodemTextBox.Size = New System.Drawing.Size(290, 19)
        Me.MacCablemodemTextBox.TabIndex = 50
        '
        'ActivacionTextBox2
        '
        Me.ActivacionTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ActivacionTextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ClientesTelBindingSource, "Fecha_Activacion", True))
        Me.ActivacionTextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ActivacionTextBox2.Location = New System.Drawing.Point(358, 150)
        Me.ActivacionTextBox2.Mask = "00/00/0000"
        Me.ActivacionTextBox2.Name = "ActivacionTextBox2"
        Me.ActivacionTextBox2.Size = New System.Drawing.Size(100, 21)
        Me.ActivacionTextBox2.TabIndex = 58
        Me.ActivacionTextBox2.ValidatingType = GetType(Date)
        '
        'No_extensionesNumericUpDown
        '
        Me.No_extensionesNumericUpDown.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.Consulta_ClientesTelBindingSource, "no_extensiones", True))
        Me.No_extensionesNumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.No_extensionesNumericUpDown.Location = New System.Drawing.Point(161, 102)
        Me.No_extensionesNumericUpDown.Name = "No_extensionesNumericUpDown"
        Me.No_extensionesNumericUpDown.Size = New System.Drawing.Size(46, 21)
        Me.No_extensionesNumericUpDown.TabIndex = 49
        Me.No_extensionesNumericUpDown.Visible = False
        '
        'CMBTextBox1
        '
        Me.CMBTextBox1.BackColor = System.Drawing.Color.Gray
        Me.CMBTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox1.ForeColor = System.Drawing.Color.Black
        Me.CMBTextBox1.Location = New System.Drawing.Point(250, 123)
        Me.CMBTextBox1.Name = "CMBTextBox1"
        Me.CMBTextBox1.ReadOnly = True
        Me.CMBTextBox1.Size = New System.Drawing.Size(208, 19)
        Me.CMBTextBox1.TabIndex = 48
        Me.CMBTextBox1.TabStop = False
        Me.CMBTextBox1.Text = "Fechas de "
        Me.CMBTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BindingNavigator3
        '
        Me.BindingNavigator3.AddNewItem = Nothing
        Me.BindingNavigator3.CountItem = Nothing
        Me.BindingNavigator3.DeleteItem = Me.BindingNavigatorDeleteItem2
        Me.BindingNavigator3.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem2, Me.GuardarToolStripButton2})
        Me.BindingNavigator3.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator3.MoveFirstItem = Nothing
        Me.BindingNavigator3.MoveLastItem = Nothing
        Me.BindingNavigator3.MoveNextItem = Nothing
        Me.BindingNavigator3.MovePreviousItem = Nothing
        Me.BindingNavigator3.Name = "BindingNavigator3"
        Me.BindingNavigator3.PositionItem = Nothing
        Me.BindingNavigator3.Size = New System.Drawing.Size(546, 25)
        Me.BindingNavigator3.TabIndex = 20
        Me.BindingNavigator3.Text = "BindingNavigator3"
        '
        'BindingNavigatorDeleteItem2
        '
        Me.BindingNavigatorDeleteItem2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.BindingNavigatorDeleteItem2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem2.Image = CType(resources.GetObject("BindingNavigatorDeleteItem2.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem2.Name = "BindingNavigatorDeleteItem2"
        Me.BindingNavigatorDeleteItem2.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem2.Size = New System.Drawing.Size(68, 22)
        Me.BindingNavigatorDeleteItem2.Text = "Cancelar"
        '
        'GuardarToolStripButton2
        '
        Me.GuardarToolStripButton2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.GuardarToolStripButton2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GuardarToolStripButton2.Image = CType(resources.GetObject("GuardarToolStripButton2.Image"), System.Drawing.Image)
        Me.GuardarToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.GuardarToolStripButton2.Name = "GuardarToolStripButton2"
        Me.GuardarToolStripButton2.Size = New System.Drawing.Size(80, 22)
        Me.GuardarToolStripButton2.Text = "&Guardar"
        '
        'ObsTextBox
        '
        Me.ObsTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ClientesTelBindingSource, "Obs", True))
        Me.ObsTextBox.Location = New System.Drawing.Point(14, 236)
        Me.ObsTextBox.Multiline = True
        Me.ObsTextBox.Name = "ObsTextBox"
        Me.ObsTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.ObsTextBox.Size = New System.Drawing.Size(462, 64)
        Me.ObsTextBox.TabIndex = 19
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.FueraAreaMaskedTextBox1)
        Me.Panel4.Controls.Add(Label3)
        Me.Panel4.Controls.Add(Me.cbTelUsuarioCapturo)
        Me.Panel4.Controls.Add(CMBTelUsuarioCapturo)
        Me.Panel4.Controls.Add(Me.LblPlazoForzoso1)
        Me.Panel4.Controls.Add(LblPlazoForzoso)
        Me.Panel4.Controls.Add(Me.ComboBox3)
        Me.Panel4.Controls.Add(Label2)
        Me.Panel4.Controls.Add(Me.NumericUpDown1)
        Me.Panel4.Controls.Add(Me.TextBox2)
        Me.Panel4.Controls.Add(Me.Clv_telefonoXtraTextBox)
        Me.Panel4.Controls.Add(Me.TextBox1)
        Me.Panel4.Controls.Add(CMBLabel46)
        Me.Panel4.Controls.Add(Me.BajaTextBox6)
        Me.Panel4.Controls.Add(Me.Ult_PAgoTextBox3)
        Me.Panel4.Controls.Add(Me.TextBox11)
        Me.Panel4.Controls.Add(Me.Button2)
        Me.Panel4.Controls.Add(Me.SuspensionTextBox4)
        Me.Panel4.Controls.Add(Me.Label39)
        Me.Panel4.Controls.Add(Me.TextBox7)
        Me.Panel4.Controls.Add(Me.Button1)
        Me.Panel4.Controls.Add(Me.InstalacionTextBox1)
        Me.Panel4.Controls.Add(Me.Numero_telTextBox)
        Me.Panel4.Controls.Add(Me.SolicitudTextBox1)
        Me.Panel4.Controls.Add(Me.ComboBox5)
        Me.Panel4.Controls.Add(CMBLabel1)
        Me.Panel4.Controls.Add(Me.ComboBox2)
        Me.Panel4.Controls.Add(Me.CheckBox1)
        Me.Panel4.Controls.Add(CMBTextBox4)
        Me.Panel4.Controls.Add(CMBTextBox3)
        Me.Panel4.Controls.Add(Me.Clv_servicioTextBox)
        Me.Panel4.Controls.Add(CMBTextBox2)
        Me.Panel4.Controls.Add(CMBLabel2)
        Me.Panel4.Controls.Add(Me.Referencia_pagoTextBox)
        Me.Panel4.Controls.Add(Fecha_Ult_pagoLabel)
        Me.Panel4.Controls.Add(PrimerMensualidadLabel)
        Me.Panel4.Controls.Add(Referencia_pagoLabel)
        Me.Panel4.Controls.Add(Me.BindingNavigator4)
        Me.Panel4.Controls.Add(Me.CMBTextBox28)
        Me.Panel4.Controls.Add(StatusLabel1)
        Me.Panel4.Controls.Add(Fecha_SolicitudLabel)
        Me.Panel4.Controls.Add(Fecha_InstalacionLabel)
        Me.Panel4.Controls.Add(Fecha_suspensionLabel1)
        Me.Panel4.Controls.Add(Fecha_BajaLabel1)
        Me.Panel4.Controls.Add(Me.Ultimo_MesTextBox)
        Me.Panel4.Controls.Add(Me.Ultimo_anioTextBox)
        Me.Panel4.Controls.Add(FacturaLabel)
        Me.Panel4.Controls.Add(Me.FacturaTextBox)
        Me.Panel4.Controls.Add(Clv_promocionLabel)
        Me.Panel4.Controls.Add(ObsLabel1)
        Me.Panel4.Controls.Add(Me.ObsTextBox1)
        Me.Panel4.Controls.Add(Clv_motcanLabel)
        Me.Panel4.Controls.Add(CortesiaLabel)
        Me.Panel4.Controls.Add(Me.CortesiaCheckBox)
        Me.Panel4.Location = New System.Drawing.Point(6, 4)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(550, 343)
        Me.Panel4.TabIndex = 20
        '
        'FueraAreaMaskedTextBox1
        '
        Me.FueraAreaMaskedTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FueraAreaMaskedTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ContTelBindingSource, "Fecha_suspension", True))
        Me.FueraAreaMaskedTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FueraAreaMaskedTextBox1.Location = New System.Drawing.Point(448, 222)
        Me.FueraAreaMaskedTextBox1.Mask = "00/00/0000"
        Me.FueraAreaMaskedTextBox1.Name = "FueraAreaMaskedTextBox1"
        Me.FueraAreaMaskedTextBox1.Size = New System.Drawing.Size(96, 21)
        Me.FueraAreaMaskedTextBox1.TabIndex = 93
        Me.FueraAreaMaskedTextBox1.ValidatingType = GetType(Date)
        '
        'Consulta_ContTelBindingSource
        '
        Me.Consulta_ContTelBindingSource.DataMember = "Consulta_ContTel"
        Me.Consulta_ContTelBindingSource.DataSource = Me.DataSetLidia2
        '
        'cbTelUsuarioCapturo
        '
        Me.cbTelUsuarioCapturo.DisplayMember = "NOMBRE"
        Me.cbTelUsuarioCapturo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbTelUsuarioCapturo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTelUsuarioCapturo.FormattingEnabled = True
        Me.cbTelUsuarioCapturo.Location = New System.Drawing.Point(291, 319)
        Me.cbTelUsuarioCapturo.Name = "cbTelUsuarioCapturo"
        Me.cbTelUsuarioCapturo.Size = New System.Drawing.Size(241, 21)
        Me.cbTelUsuarioCapturo.TabIndex = 91
        Me.cbTelUsuarioCapturo.ValueMember = "CLAVE"
        '
        'LblPlazoForzoso1
        '
        Me.LblPlazoForzoso1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPlazoForzoso1.ForeColor = System.Drawing.Color.Red
        Me.LblPlazoForzoso1.Location = New System.Drawing.Point(156, 233)
        Me.LblPlazoForzoso1.Name = "LblPlazoForzoso1"
        Me.LblPlazoForzoso1.Size = New System.Drawing.Size(151, 15)
        Me.LblPlazoForzoso1.TabIndex = 89
        '
        'ComboBox3
        '
        Me.ComboBox3.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.Consulta_ContTelBindingSource, "clv_motcan", True))
        Me.ComboBox3.DataSource = Me.MuestraMotivoCancelacionBindingSource
        Me.ComboBox3.DisplayMember = "MOTCAN"
        Me.ComboBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(12, 319)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(269, 21)
        Me.ComboBox3.TabIndex = 57
        Me.ComboBox3.ValueMember = "Clv_MOTCAN"
        '
        'MuestraMotivoCancelacionBindingSource
        '
        Me.MuestraMotivoCancelacionBindingSource.DataMember = "MuestraMotivoCancelacion"
        Me.MuestraMotivoCancelacionBindingSource.DataSource = Me.DataSetLidia
        '
        'DataSetLidia
        '
        Me.DataSetLidia.DataSetName = "DataSetLidia"
        Me.DataSetLidia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.Consulta_ClientesTelBindingSource, "no_extensiones", True))
        Me.NumericUpDown1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown1.Location = New System.Drawing.Point(159, 205)
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(46, 21)
        Me.NumericUpDown1.TabIndex = 86
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.Color.White
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(394, 80)
        Me.TextBox2.MaxLength = 3
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(55, 21)
        Me.TextBox2.TabIndex = 84
        Me.TextBox2.TabStop = False
        Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Clv_telefonoXtraTextBox
        '
        Me.Clv_telefonoXtraTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ContTelBindingSource, "clv_telefonoXtra", True))
        Me.Clv_telefonoXtraTextBox.Location = New System.Drawing.Point(189, 347)
        Me.Clv_telefonoXtraTextBox.Name = "Clv_telefonoXtraTextBox"
        Me.Clv_telefonoXtraTextBox.Size = New System.Drawing.Size(64, 20)
        Me.Clv_telefonoXtraTextBox.TabIndex = 65
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.White
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ContTelBindingSource, "num_extra", True))
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.ForeColor = System.Drawing.Color.DarkRed
        Me.TextBox1.Location = New System.Drawing.Point(159, 76)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(209, 15)
        Me.TextBox1.TabIndex = 64
        '
        'BajaTextBox6
        '
        Me.BajaTextBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BajaTextBox6.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ContTelBindingSource, "Fecha_Baja", True))
        Me.BajaTextBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BajaTextBox6.Location = New System.Drawing.Point(448, 244)
        Me.BajaTextBox6.Mask = "00/00/0000"
        Me.BajaTextBox6.Name = "BajaTextBox6"
        Me.BajaTextBox6.Size = New System.Drawing.Size(96, 21)
        Me.BajaTextBox6.TabIndex = 62
        Me.BajaTextBox6.ValidatingType = GetType(Date)
        '
        'Ult_PAgoTextBox3
        '
        Me.Ult_PAgoTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Ult_PAgoTextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ContTelBindingSource, "Fecha_Ult_pago", True))
        Me.Ult_PAgoTextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ult_PAgoTextBox3.Location = New System.Drawing.Point(448, 268)
        Me.Ult_PAgoTextBox3.Mask = "00/00/0000"
        Me.Ult_PAgoTextBox3.Name = "Ult_PAgoTextBox3"
        Me.Ult_PAgoTextBox3.Size = New System.Drawing.Size(96, 21)
        Me.Ult_PAgoTextBox3.TabIndex = 59
        Me.Ult_PAgoTextBox3.ValidatingType = GetType(Date)
        '
        'TextBox11
        '
        Me.TextBox11.BackColor = System.Drawing.Color.White
        Me.TextBox11.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox11.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ContTelBindingSource, "descripcion", True))
        Me.TextBox11.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox11.ForeColor = System.Drawing.Color.DarkRed
        Me.TextBox11.Location = New System.Drawing.Point(159, 28)
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.ReadOnly = True
        Me.TextBox11.Size = New System.Drawing.Size(209, 17)
        Me.TextBox11.TabIndex = 56
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(491, 22)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(33, 22)
        Me.Button2.TabIndex = 61
        Me.Button2.Text = "..."
        Me.Button2.UseVisualStyleBackColor = True
        '
        'SuspensionTextBox4
        '
        Me.SuspensionTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SuspensionTextBox4.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ContTelBindingSource, "Fecha_suspension", True))
        Me.SuspensionTextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuspensionTextBox4.Location = New System.Drawing.Point(448, 199)
        Me.SuspensionTextBox4.Mask = "00/00/0000"
        Me.SuspensionTextBox4.Name = "SuspensionTextBox4"
        Me.SuspensionTextBox4.Size = New System.Drawing.Size(96, 21)
        Me.SuspensionTextBox4.TabIndex = 60
        Me.SuspensionTextBox4.ValidatingType = GetType(Date)
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label39.Location = New System.Drawing.Point(298, 185)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(19, 15)
        Me.Label39.TabIndex = 56
        Me.Label39.Text = "%"
        Me.Label39.Visible = False
        '
        'TextBox7
        '
        Me.TextBox7.BackColor = System.Drawing.Color.White
        Me.TextBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox7.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConRelCteDescuentoBindingSource, "Descuento", True))
        Me.TextBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox7.Location = New System.Drawing.Point(243, 181)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.ReadOnly = True
        Me.TextBox7.Size = New System.Drawing.Size(53, 20)
        Me.TextBox7.TabIndex = 60
        '
        'ConRelCteDescuentoBindingSource
        '
        Me.ConRelCteDescuentoBindingSource.DataMember = "ConRelCteDescuento"
        Me.ConRelCteDescuentoBindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(165, 178)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(77, 22)
        Me.Button1.TabIndex = 59
        Me.Button1.Text = "Descuento"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'InstalacionTextBox1
        '
        Me.InstalacionTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.InstalacionTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ContTelBindingSource, "Fecha_Instalacion", True))
        Me.InstalacionTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.InstalacionTextBox1.Location = New System.Drawing.Point(448, 175)
        Me.InstalacionTextBox1.Mask = "00/00/0000"
        Me.InstalacionTextBox1.Name = "InstalacionTextBox1"
        Me.InstalacionTextBox1.Size = New System.Drawing.Size(96, 21)
        Me.InstalacionTextBox1.TabIndex = 57
        Me.InstalacionTextBox1.ValidatingType = GetType(Date)
        '
        'Numero_telTextBox
        '
        Me.Numero_telTextBox.BackColor = System.Drawing.Color.White
        Me.Numero_telTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Numero_telTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ContTelBindingSource, "numero_tel", True))
        Me.Numero_telTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Numero_telTextBox.ForeColor = System.Drawing.Color.DarkRed
        Me.Numero_telTextBox.Location = New System.Drawing.Point(159, 49)
        Me.Numero_telTextBox.Name = "Numero_telTextBox"
        Me.Numero_telTextBox.ReadOnly = True
        Me.Numero_telTextBox.Size = New System.Drawing.Size(209, 19)
        Me.Numero_telTextBox.TabIndex = 56
        '
        'SolicitudTextBox1
        '
        Me.SolicitudTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SolicitudTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ContTelBindingSource, "Fecha_Solicitud", True))
        Me.SolicitudTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SolicitudTextBox1.Location = New System.Drawing.Point(448, 152)
        Me.SolicitudTextBox1.Mask = "00/00/0000"
        Me.SolicitudTextBox1.Name = "SolicitudTextBox1"
        Me.SolicitudTextBox1.Size = New System.Drawing.Size(96, 21)
        Me.SolicitudTextBox1.TabIndex = 56
        Me.SolicitudTextBox1.ValidatingType = GetType(Date)
        '
        'ComboBox5
        '
        Me.ComboBox5.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.Consulta_ContTelBindingSource, "clv_promocion", True))
        Me.ComboBox5.DataSource = Me.MuestraTipoPromocionBindingSource
        Me.ComboBox5.DisplayMember = "Descripcion"
        Me.ComboBox5.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox5.FormattingEnabled = True
        Me.ComboBox5.Location = New System.Drawing.Point(159, 124)
        Me.ComboBox5.Name = "ComboBox5"
        Me.ComboBox5.Size = New System.Drawing.Size(178, 23)
        Me.ComboBox5.TabIndex = 58
        Me.ComboBox5.ValueMember = "clave"
        '
        'MuestraTipoPromocionBindingSource
        '
        Me.MuestraTipoPromocionBindingSource.DataMember = "MuestraTipoPromocion"
        Me.MuestraTipoPromocionBindingSource.DataSource = Me.DataSetLidia2
        '
        'ComboBox2
        '
        Me.ComboBox2.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.Consulta_ContTelBindingSource, "status", True))
        Me.ComboBox2.DataSource = Me.StatusTelBindingSource
        Me.ComboBox2.DisplayMember = "Concepto"
        Me.ComboBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(159, 97)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(178, 23)
        Me.ComboBox2.TabIndex = 53
        Me.ComboBox2.ValueMember = "Clv_Statustel"
        '
        'StatusTelBindingSource
        '
        Me.StatusTelBindingSource.DataMember = "StatusTel"
        Me.StatusTelBindingSource.DataSource = Me.DataSetLidia2
        '
        'CheckBox1
        '
        Me.CheckBox1.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.Consulta_ContTelBindingSource, "PrimerMensualidad", True))
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(150, 178)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(24, 24)
        Me.CheckBox1.TabIndex = 56
        '
        'Clv_servicioTextBox
        '
        Me.Clv_servicioTextBox.BackColor = System.Drawing.Color.White
        Me.Clv_servicioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_servicioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ContTelBindingSource, "clv_servicio", True))
        Me.Clv_servicioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_servicioTextBox.Location = New System.Drawing.Point(197, 321)
        Me.Clv_servicioTextBox.Name = "Clv_servicioTextBox"
        Me.Clv_servicioTextBox.ReadOnly = True
        Me.Clv_servicioTextBox.Size = New System.Drawing.Size(56, 17)
        Me.Clv_servicioTextBox.TabIndex = 7
        '
        'Referencia_pagoTextBox
        '
        Me.Referencia_pagoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Referencia_pagoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Referencia_pagoTextBox.Location = New System.Drawing.Point(159, 151)
        Me.Referencia_pagoTextBox.Name = "Referencia_pagoTextBox"
        Me.Referencia_pagoTextBox.Size = New System.Drawing.Size(167, 21)
        Me.Referencia_pagoTextBox.TabIndex = 5
        '
        'BindingNavigator4
        '
        Me.BindingNavigator4.AddNewItem = Nothing
        Me.BindingNavigator4.CountItem = Nothing
        Me.BindingNavigator4.DeleteItem = Me.BindingNavigatorDeleteItem3
        Me.BindingNavigator4.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem3, Me.GuardarToolStripButton3})
        Me.BindingNavigator4.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator4.MoveFirstItem = Nothing
        Me.BindingNavigator4.MoveLastItem = Nothing
        Me.BindingNavigator4.MoveNextItem = Nothing
        Me.BindingNavigator4.MovePreviousItem = Nothing
        Me.BindingNavigator4.Name = "BindingNavigator4"
        Me.BindingNavigator4.PositionItem = Nothing
        Me.BindingNavigator4.Size = New System.Drawing.Size(550, 25)
        Me.BindingNavigator4.TabIndex = 48
        Me.BindingNavigator4.Text = "BindingNavigator4"
        '
        'BindingNavigatorDeleteItem3
        '
        Me.BindingNavigatorDeleteItem3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.BindingNavigatorDeleteItem3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem3.Image = CType(resources.GetObject("BindingNavigatorDeleteItem3.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem3.Name = "BindingNavigatorDeleteItem3"
        Me.BindingNavigatorDeleteItem3.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem3.Size = New System.Drawing.Size(68, 22)
        Me.BindingNavigatorDeleteItem3.Text = "Cancelar"
        '
        'GuardarToolStripButton3
        '
        Me.GuardarToolStripButton3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.GuardarToolStripButton3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GuardarToolStripButton3.Image = CType(resources.GetObject("GuardarToolStripButton3.Image"), System.Drawing.Image)
        Me.GuardarToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.GuardarToolStripButton3.Name = "GuardarToolStripButton3"
        Me.GuardarToolStripButton3.Size = New System.Drawing.Size(80, 22)
        Me.GuardarToolStripButton3.Text = "&Guardar"
        '
        'CMBTextBox28
        '
        Me.CMBTextBox28.BackColor = System.Drawing.Color.Silver
        Me.CMBTextBox28.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox28.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox28.ForeColor = System.Drawing.Color.Black
        Me.CMBTextBox28.Location = New System.Drawing.Point(355, 130)
        Me.CMBTextBox28.Name = "CMBTextBox28"
        Me.CMBTextBox28.ReadOnly = True
        Me.CMBTextBox28.Size = New System.Drawing.Size(189, 19)
        Me.CMBTextBox28.TabIndex = 47
        Me.CMBTextBox28.TabStop = False
        Me.CMBTextBox28.Text = "Fechas de "
        Me.CMBTextBox28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Ultimo_MesTextBox
        '
        Me.Ultimo_MesTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Ultimo_MesTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ContTelBindingSource, "Ultimo_Mes", True))
        Me.Ultimo_MesTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ultimo_MesTextBox.Location = New System.Drawing.Point(394, 80)
        Me.Ultimo_MesTextBox.Name = "Ultimo_MesTextBox"
        Me.Ultimo_MesTextBox.Size = New System.Drawing.Size(55, 21)
        Me.Ultimo_MesTextBox.TabIndex = 23
        '
        'Ultimo_anioTextBox
        '
        Me.Ultimo_anioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Ultimo_anioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ContTelBindingSource, "Ultimo_anio", True))
        Me.Ultimo_anioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ultimo_anioTextBox.Location = New System.Drawing.Point(455, 80)
        Me.Ultimo_anioTextBox.Name = "Ultimo_anioTextBox"
        Me.Ultimo_anioTextBox.Size = New System.Drawing.Size(69, 21)
        Me.Ultimo_anioTextBox.TabIndex = 25
        '
        'FacturaTextBox
        '
        Me.FacturaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FacturaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ContTelBindingSource, "factura", True))
        Me.FacturaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FacturaTextBox.Location = New System.Drawing.Point(421, 107)
        Me.FacturaTextBox.Name = "FacturaTextBox"
        Me.FacturaTextBox.Size = New System.Drawing.Size(103, 21)
        Me.FacturaTextBox.TabIndex = 27
        '
        'ObsTextBox1
        '
        Me.ObsTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.ObsTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ContTelBindingSource, "obs", True))
        Me.ObsTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ObsTextBox1.Location = New System.Drawing.Point(12, 267)
        Me.ObsTextBox1.Multiline = True
        Me.ObsTextBox1.Name = "ObsTextBox1"
        Me.ObsTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.ObsTextBox1.Size = New System.Drawing.Size(269, 35)
        Me.ObsTextBox1.TabIndex = 31
        '
        'CortesiaCheckBox
        '
        Me.CortesiaCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.Consulta_ContTelBindingSource, "cortesia", True))
        Me.CortesiaCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CortesiaCheckBox.Location = New System.Drawing.Point(468, 22)
        Me.CortesiaCheckBox.Name = "CortesiaCheckBox"
        Me.CortesiaCheckBox.Size = New System.Drawing.Size(21, 24)
        Me.CortesiaCheckBox.TabIndex = 35
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.ComboBox6)
        Me.Panel5.Controls.Add(Label1)
        Me.Panel5.Controls.Add(Me.BajaAdicTextBox1)
        Me.Panel5.Controls.Add(Me.InstalacionAdicTextBox3)
        Me.Panel5.Controls.Add(Me.SolicitudAdicTextBox2)
        Me.Panel5.Controls.Add(Me.SuspensionAdicTextBox4)
        Me.Panel5.Controls.Add(Label11)
        Me.Panel5.Controls.Add(Me.ComboBox4)
        Me.Panel5.Controls.Add(Me.TextBox5)
        Me.Panel5.Controls.Add(CMBLabel4)
        Me.Panel5.Controls.Add(Me.CMBTextBox6)
        Me.Panel5.Controls.Add(Me.BindingNavigator6)
        Me.Panel5.Controls.Add(Label6)
        Me.Panel5.Controls.Add(Label7)
        Me.Panel5.Controls.Add(Label8)
        Me.Panel5.Controls.Add(Label9)
        Me.Panel5.Location = New System.Drawing.Point(10, 4)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(538, 325)
        Me.Panel5.TabIndex = 53
        '
        'ComboBox6
        '
        Me.ComboBox6.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.Consulta_ConTTelAdicBindingSource, "tipo_Servicio", True))
        Me.ComboBox6.DataSource = Me.MUESTRA_TiposPaqBindingSource
        Me.ComboBox6.DisplayMember = "nonmbre"
        Me.ComboBox6.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox6.FormattingEnabled = True
        Me.ComboBox6.Location = New System.Drawing.Point(230, 99)
        Me.ComboBox6.Name = "ComboBox6"
        Me.ComboBox6.Size = New System.Drawing.Size(180, 23)
        Me.ComboBox6.TabIndex = 68
        Me.ComboBox6.ValueMember = "clave"
        '
        'Consulta_ConTTelAdicBindingSource
        '
        Me.Consulta_ConTTelAdicBindingSource.DataMember = "Consulta_ConTTelAdic"
        Me.Consulta_ConTTelAdicBindingSource.DataSource = Me.DataSetLidia2
        '
        'MUESTRA_TiposPaqBindingSource
        '
        Me.MUESTRA_TiposPaqBindingSource.DataMember = "MUESTRA_TiposPaq"
        Me.MUESTRA_TiposPaqBindingSource.DataSource = Me.DataSetLidia2
        '
        'BajaAdicTextBox1
        '
        Me.BajaAdicTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BajaAdicTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ConTTelAdicBindingSource, "Fecha_Baja", True))
        Me.BajaAdicTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BajaAdicTextBox1.Location = New System.Drawing.Point(238, 284)
        Me.BajaAdicTextBox1.Mask = "00/00/0000"
        Me.BajaAdicTextBox1.Name = "BajaAdicTextBox1"
        Me.BajaAdicTextBox1.Size = New System.Drawing.Size(100, 21)
        Me.BajaAdicTextBox1.TabIndex = 63
        Me.BajaAdicTextBox1.ValidatingType = GetType(Date)
        '
        'InstalacionAdicTextBox3
        '
        Me.InstalacionAdicTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.InstalacionAdicTextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ConTTelAdicBindingSource, "Fecha_instalacion", True))
        Me.InstalacionAdicTextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.InstalacionAdicTextBox3.Location = New System.Drawing.Point(238, 211)
        Me.InstalacionAdicTextBox3.Mask = "00/00/0000"
        Me.InstalacionAdicTextBox3.Name = "InstalacionAdicTextBox3"
        Me.InstalacionAdicTextBox3.Size = New System.Drawing.Size(100, 21)
        Me.InstalacionAdicTextBox3.TabIndex = 65
        Me.InstalacionAdicTextBox3.ValidatingType = GetType(Date)
        '
        'SolicitudAdicTextBox2
        '
        Me.SolicitudAdicTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SolicitudAdicTextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ConTTelAdicBindingSource, "Fecha_solicitud", True))
        Me.SolicitudAdicTextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SolicitudAdicTextBox2.Location = New System.Drawing.Point(238, 174)
        Me.SolicitudAdicTextBox2.Mask = "00/00/0000"
        Me.SolicitudAdicTextBox2.Name = "SolicitudAdicTextBox2"
        Me.SolicitudAdicTextBox2.Size = New System.Drawing.Size(100, 21)
        Me.SolicitudAdicTextBox2.TabIndex = 64
        Me.SolicitudAdicTextBox2.ValidatingType = GetType(Date)
        '
        'SuspensionAdicTextBox4
        '
        Me.SuspensionAdicTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SuspensionAdicTextBox4.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ConTTelAdicBindingSource, "Fecha_suspension", True))
        Me.SuspensionAdicTextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuspensionAdicTextBox4.Location = New System.Drawing.Point(238, 247)
        Me.SuspensionAdicTextBox4.Mask = "00/00/0000"
        Me.SuspensionAdicTextBox4.Name = "SuspensionAdicTextBox4"
        Me.SuspensionAdicTextBox4.Size = New System.Drawing.Size(100, 21)
        Me.SuspensionAdicTextBox4.TabIndex = 66
        Me.SuspensionAdicTextBox4.ValidatingType = GetType(Date)
        '
        'ComboBox4
        '
        Me.ComboBox4.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.Consulta_ConTTelAdicBindingSource, "status", True))
        Me.ComboBox4.DataSource = Me.StatusTelBindingSource
        Me.ComboBox4.DisplayMember = "Concepto"
        Me.ComboBox4.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(230, 67)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(180, 23)
        Me.ComboBox4.TabIndex = 52
        Me.ComboBox4.ValueMember = "Clv_Statustel"
        '
        'TextBox5
        '
        Me.TextBox5.BackColor = System.Drawing.Color.White
        Me.TextBox5.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox5.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ConTTelAdicBindingSource, "descripcion", True))
        Me.TextBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox5.ForeColor = System.Drawing.Color.DarkRed
        Me.TextBox5.Location = New System.Drawing.Point(230, 32)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ReadOnly = True
        Me.TextBox5.Size = New System.Drawing.Size(234, 22)
        Me.TextBox5.TabIndex = 50
        '
        'CMBTextBox6
        '
        Me.CMBTextBox6.BackColor = System.Drawing.Color.Gray
        Me.CMBTextBox6.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox6.ForeColor = System.Drawing.Color.Black
        Me.CMBTextBox6.Location = New System.Drawing.Point(142, 136)
        Me.CMBTextBox6.Name = "CMBTextBox6"
        Me.CMBTextBox6.ReadOnly = True
        Me.CMBTextBox6.Size = New System.Drawing.Size(208, 19)
        Me.CMBTextBox6.TabIndex = 48
        Me.CMBTextBox6.TabStop = False
        Me.CMBTextBox6.Text = "Fechas de "
        Me.CMBTextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BindingNavigator6
        '
        Me.BindingNavigator6.AddNewItem = Nothing
        Me.BindingNavigator6.CountItem = Nothing
        Me.BindingNavigator6.DeleteItem = Me.ToolStripButton3
        Me.BindingNavigator6.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton3, Me.ToolStripButton4})
        Me.BindingNavigator6.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator6.MoveFirstItem = Nothing
        Me.BindingNavigator6.MoveLastItem = Nothing
        Me.BindingNavigator6.MoveNextItem = Nothing
        Me.BindingNavigator6.MovePreviousItem = Nothing
        Me.BindingNavigator6.Name = "BindingNavigator6"
        Me.BindingNavigator6.PositionItem = Nothing
        Me.BindingNavigator6.Size = New System.Drawing.Size(538, 25)
        Me.BindingNavigator6.TabIndex = 20
        Me.BindingNavigator6.Text = "BindingNavigator6"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton3.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton3.Size = New System.Drawing.Size(68, 22)
        Me.ToolStripButton3.Text = "Cancelar"
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton4.Image = CType(resources.GetObject("ToolStripButton4.Image"), System.Drawing.Image)
        Me.ToolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton4.Name = "ToolStripButton4"
        Me.ToolStripButton4.Size = New System.Drawing.Size(80, 22)
        Me.ToolStripButton4.Text = "&Guardar"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.BindingNavigator5)
        Me.Panel2.Controls.Add(Me.BindingNavigator2)
        Me.Panel2.Controls.Add(Me.BindingNavigator1)
        Me.Panel2.Controls.Add(Me.TreeView1)
        Me.Panel2.Location = New System.Drawing.Point(927, 208)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(274, 334)
        Me.Panel2.TabIndex = 0
        '
        'BindingNavigator5
        '
        Me.BindingNavigator5.AddNewItem = Nothing
        Me.BindingNavigator5.CountItem = Nothing
        Me.BindingNavigator5.DeleteItem = Me.ToolStripButton1
        Me.BindingNavigator5.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.ToolStripButton2})
        Me.BindingNavigator5.Location = New System.Drawing.Point(0, 50)
        Me.BindingNavigator5.MoveFirstItem = Nothing
        Me.BindingNavigator5.MoveLastItem = Nothing
        Me.BindingNavigator5.MoveNextItem = Nothing
        Me.BindingNavigator5.MovePreviousItem = Nothing
        Me.BindingNavigator5.Name = "BindingNavigator5"
        Me.BindingNavigator5.PositionItem = Nothing
        Me.BindingNavigator5.Size = New System.Drawing.Size(274, 25)
        Me.BindingNavigator5.TabIndex = 3
        Me.BindingNavigator5.Text = "BindingNavigator5"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton1.Size = New System.Drawing.Size(170, 22)
        Me.ToolStripButton1.Text = "Eliminar Plan &Adicional"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(202, 20)
        Me.ToolStripButton2.Text = "Agregar Paquete &Adicional"
        '
        'BindingNavigator2
        '
        Me.BindingNavigator2.AddNewItem = Nothing
        Me.BindingNavigator2.CountItem = Nothing
        Me.BindingNavigator2.DeleteItem = Me.BindingNavigatorDeleteItem1
        Me.BindingNavigator2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem1, Me.GuardarToolStripButton1})
        Me.BindingNavigator2.Location = New System.Drawing.Point(0, 25)
        Me.BindingNavigator2.MoveFirstItem = Nothing
        Me.BindingNavigator2.MoveLastItem = Nothing
        Me.BindingNavigator2.MoveNextItem = Nothing
        Me.BindingNavigator2.MovePreviousItem = Nothing
        Me.BindingNavigator2.Name = "BindingNavigator2"
        Me.BindingNavigator2.PositionItem = Nothing
        Me.BindingNavigator2.Size = New System.Drawing.Size(274, 25)
        Me.BindingNavigator2.TabIndex = 2
        Me.BindingNavigator2.Text = "BindingNavigator2"
        '
        'BindingNavigatorDeleteItem1
        '
        Me.BindingNavigatorDeleteItem1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem1.Image = CType(resources.GetObject("BindingNavigatorDeleteItem1.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem1.Name = "BindingNavigatorDeleteItem1"
        Me.BindingNavigatorDeleteItem1.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem1.Size = New System.Drawing.Size(166, 22)
        Me.BindingNavigatorDeleteItem1.Text = "Eliminar Plan &Tarifario"
        '
        'GuardarToolStripButton1
        '
        Me.GuardarToolStripButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.GuardarToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GuardarToolStripButton1.Image = CType(resources.GetObject("GuardarToolStripButton1.Image"), System.Drawing.Image)
        Me.GuardarToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.GuardarToolStripButton1.Name = "GuardarToolStripButton1"
        Me.GuardarToolStripButton1.Size = New System.Drawing.Size(171, 20)
        Me.GuardarToolStripButton1.Text = "Agregar Plan &Tarifario"
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Nothing
        Me.BindingNavigator1.CountItem = Nothing
        Me.BindingNavigator1.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.GuardarToolStripButton})
        Me.BindingNavigator1.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator1.MoveFirstItem = Nothing
        Me.BindingNavigator1.MoveLastItem = Nothing
        Me.BindingNavigator1.MoveNextItem = Nothing
        Me.BindingNavigator1.MovePreviousItem = Nothing
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Nothing
        Me.BindingNavigator1.Size = New System.Drawing.Size(274, 25)
        Me.BindingNavigator1.TabIndex = 1
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(123, 22)
        Me.BindingNavigatorDeleteItem.Text = "&Eliminar Equipo"
        '
        'GuardarToolStripButton
        '
        Me.GuardarToolStripButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.GuardarToolStripButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GuardarToolStripButton.Image = CType(resources.GetObject("GuardarToolStripButton.Image"), System.Drawing.Image)
        Me.GuardarToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.GuardarToolStripButton.Name = "GuardarToolStripButton"
        Me.GuardarToolStripButton.Size = New System.Drawing.Size(128, 22)
        Me.GuardarToolStripButton.Text = "&Agregar Equipo"
        '
        'TreeView1
        '
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.Location = New System.Drawing.Point(5, 78)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(442, 253)
        Me.TreeView1.TabIndex = 0
        '
        'Clv_telefonoTextBox
        '
        Me.Clv_telefonoTextBox.BackColor = System.Drawing.Color.White
        Me.Clv_telefonoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_telefonoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ContTelBindingSource, "clv_telefono", True))
        Me.Clv_telefonoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_telefonoTextBox.Location = New System.Drawing.Point(257, 806)
        Me.Clv_telefonoTextBox.Name = "Clv_telefonoTextBox"
        Me.Clv_telefonoTextBox.ReadOnly = True
        Me.Clv_telefonoTextBox.Size = New System.Drawing.Size(82, 19)
        Me.Clv_telefonoTextBox.TabIndex = 11
        '
        'Clv_cablemodemTextBox
        '
        Me.Clv_cablemodemTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ClientesTelBindingSource, "Clv_cablemodem", True))
        Me.Clv_cablemodemTextBox.Location = New System.Drawing.Point(901, 763)
        Me.Clv_cablemodemTextBox.Name = "Clv_cablemodemTextBox"
        Me.Clv_cablemodemTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_cablemodemTextBox.TabIndex = 9
        '
        'Muestra_ClientesTelBindingSource
        '
        Me.Muestra_ClientesTelBindingSource.DataMember = "Muestra_ClientesTel"
        Me.Muestra_ClientesTelBindingSource.DataSource = Me.DataSetLidia2
        '
        'StatusTextBox
        '
        Me.StatusTextBox.Location = New System.Drawing.Point(983, 666)
        Me.StatusTextBox.Name = "StatusTextBox"
        Me.StatusTextBox.Size = New System.Drawing.Size(49, 20)
        Me.StatusTextBox.TabIndex = 7
        '
        'Procedimientosarnoldo4
        '
        Me.Procedimientosarnoldo4.DataSetName = "Procedimientosarnoldo4"
        Me.Procedimientosarnoldo4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ContratonetTextBox
        '
        Me.ContratonetTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ClientesTelBindingSource, "contratonet", True))
        Me.ContratonetTextBox.Location = New System.Drawing.Point(965, 638)
        Me.ContratonetTextBox.Name = "ContratonetTextBox"
        Me.ContratonetTextBox.Size = New System.Drawing.Size(59, 20)
        Me.ContratonetTextBox.TabIndex = 1
        '
        'ContratoTextBox
        '
        Me.ContratoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ClientesTelBindingSource, "contrato", True))
        Me.ContratoTextBox.Location = New System.Drawing.Point(915, 664)
        Me.ContratoTextBox.Name = "ContratoTextBox"
        Me.ContratoTextBox.Size = New System.Drawing.Size(59, 20)
        Me.ContratoTextBox.TabIndex = 3
        '
        'StatusTextBox1
        '
        Me.StatusTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.StatusTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusTextBox1.Location = New System.Drawing.Point(944, 559)
        Me.StatusTextBox1.Name = "StatusTextBox1"
        Me.StatusTextBox1.Size = New System.Drawing.Size(21, 21)
        Me.StatusTextBox1.TabIndex = 9
        '
        'Clv_unicanetTextBox
        '
        Me.Clv_unicanetTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_unicanetTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ContTelBindingSource, "clv_unicanet", True))
        Me.Clv_unicanetTextBox.Location = New System.Drawing.Point(940, 586)
        Me.Clv_unicanetTextBox.Name = "Clv_unicanetTextBox"
        Me.Clv_unicanetTextBox.Size = New System.Drawing.Size(104, 20)
        Me.Clv_unicanetTextBox.TabIndex = 1
        '
        'ContratonetTextBox1
        '
        Me.ContratonetTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ContratonetTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ContTelBindingSource, "contratonet", True))
        Me.ContratonetTextBox1.Location = New System.Drawing.Point(940, 612)
        Me.ContratonetTextBox1.Name = "ContratonetTextBox1"
        Me.ContratonetTextBox1.Size = New System.Drawing.Size(104, 20)
        Me.ContratonetTextBox1.TabIndex = 3
        '
        'PrimerMensualidadTextBox
        '
        Me.PrimerMensualidadTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PrimerMensualidadTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PrimerMensualidadTextBox.Location = New System.Drawing.Point(994, 559)
        Me.PrimerMensualidadTextBox.Name = "PrimerMensualidadTextBox"
        Me.PrimerMensualidadTextBox.Size = New System.Drawing.Size(41, 21)
        Me.PrimerMensualidadTextBox.TabIndex = 21
        '
        'MUESTRACONTTELBindingSource
        '
        Me.MUESTRACONTTELBindingSource.DataMember = "MUESTRACONTTEL"
        Me.MUESTRACONTTELBindingSource.DataSource = Me.DataSetLidia2
        '
        'MUESTRACONTTELTableAdapter
        '
        Me.MUESTRACONTTELTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ClientesTelTableAdapter
        '
        Me.Muestra_ClientesTelTableAdapter.ClearBeforeFill = True
        '
        'StatusMTATableAdapter
        '
        Me.StatusMTATableAdapter.ClearBeforeFill = True
        '
        'StatusTelTableAdapter
        '
        Me.StatusTelTableAdapter.ClearBeforeFill = True
        '
        'MuestraMotivoCancelacionTableAdapter
        '
        Me.MuestraMotivoCancelacionTableAdapter.ClearBeforeFill = True
        '
        'Consulta_ContTelTableAdapter
        '
        Me.Consulta_ContTelTableAdapter.ClearBeforeFill = True
        '
        'Consulta_ClientesTelTableAdapter
        '
        Me.Consulta_ClientesTelTableAdapter.ClearBeforeFill = True
        '
        'Consulta_ConTTelAdicTableAdapter
        '
        Me.Consulta_ConTTelAdicTableAdapter.ClearBeforeFill = True
        '
        'Clv_servicioTextBox1
        '
        Me.Clv_servicioTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ConTTelAdicBindingSource, "clv_servicio", True))
        Me.Clv_servicioTextBox1.Location = New System.Drawing.Point(901, 711)
        Me.Clv_servicioTextBox1.Name = "Clv_servicioTextBox1"
        Me.Clv_servicioTextBox1.Size = New System.Drawing.Size(100, 20)
        Me.Clv_servicioTextBox1.TabIndex = 54
        '
        'Clv_unicanetTextBox1
        '
        Me.Clv_unicanetTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ConTTelAdicBindingSource, "clv_unicanet", True))
        Me.Clv_unicanetTextBox1.Location = New System.Drawing.Point(901, 737)
        Me.Clv_unicanetTextBox1.Name = "Clv_unicanetTextBox1"
        Me.Clv_unicanetTextBox1.Size = New System.Drawing.Size(100, 20)
        Me.Clv_unicanetTextBox1.TabIndex = 55
        '
        'MUESTRAPAQ_ADICBindingSource
        '
        Me.MUESTRAPAQ_ADICBindingSource.DataMember = "MUESTRAPAQ_ADIC"
        Me.MUESTRAPAQ_ADICBindingSource.DataSource = Me.DataSetLidia2
        '
        'MUESTRAPAQ_ADICTableAdapter
        '
        Me.MUESTRAPAQ_ADICTableAdapter.ClearBeforeFill = True
        '
        'Muestra_EquiposTelBindingSource
        '
        Me.Muestra_EquiposTelBindingSource.DataMember = "Muestra_EquiposTel"
        Me.Muestra_EquiposTelBindingSource.DataSource = Me.DataSetLidia2
        '
        'Muestra_EquiposTelTableAdapter
        '
        Me.Muestra_EquiposTelTableAdapter.ClearBeforeFill = True
        '
        'MuestraTipoPromocionTableAdapter
        '
        Me.MuestraTipoPromocionTableAdapter.ClearBeforeFill = True
        '
        'ChecaRelCteDescuentoBindingSource
        '
        Me.ChecaRelCteDescuentoBindingSource.DataMember = "ChecaRelCteDescuento"
        Me.ChecaRelCteDescuentoBindingSource.DataSource = Me.DataSetEric
        '
        'ChecaRelCteDescuentoTableAdapter
        '
        Me.ChecaRelCteDescuentoTableAdapter.ClearBeforeFill = True
        '
        'ConRelCteDescuentoTableAdapter
        '
        Me.ConRelCteDescuentoTableAdapter.ClearBeforeFill = True
        '
        'MUESTRA_TiposPaqTableAdapter
        '
        Me.MUESTRA_TiposPaqTableAdapter.ClearBeforeFill = True
        '
        'DataSetEdgarRev2
        '
        Me.DataSetEdgarRev2.DataSetName = "DataSetEdgarRev2"
        Me.DataSetEdgarRev2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'VerAcceso_ChecaBindingSource
        '
        Me.VerAcceso_ChecaBindingSource.DataMember = "VerAcceso_Checa"
        Me.VerAcceso_ChecaBindingSource.DataSource = Me.DataSetEdgarRev2
        '
        'VerAcceso_ChecaTableAdapter
        '
        Me.VerAcceso_ChecaTableAdapter.ClearBeforeFill = True
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Dame_clvtipusuarioBindingSource
        '
        Me.Dame_clvtipusuarioBindingSource.DataMember = "Dame_clvtipusuario"
        Me.Dame_clvtipusuarioBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Dame_clvtipusuarioTableAdapter
        '
        Me.Dame_clvtipusuarioTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'FrmClientesTel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(563, 349)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Clv_unicanetLabel1)
        Me.Controls.Add(Me.Clv_unicanetTextBox1)
        Me.Controls.Add(Clv_servicioLabel)
        Me.Controls.Add(Me.Clv_servicioTextBox1)
        Me.Controls.Add(Me.PrimerMensualidadTextBox)
        Me.Controls.Add(Me.StatusTextBox)
        Me.Controls.Add(Me.Clv_telefonoTextBox)
        Me.Controls.Add(Me.ContratonetTextBox1)
        Me.Controls.Add(ContratonetLabel1)
        Me.Controls.Add(Me.Clv_unicanetTextBox)
        Me.Controls.Add(ContratonetLabel)
        Me.Controls.Add(Clv_cablemodemLabel)
        Me.Controls.Add(Clv_unicanetLabel)
        Me.Controls.Add(Me.Clv_cablemodemTextBox)
        Me.Controls.Add(Me.ContratonetTextBox)
        Me.Controls.Add(Me.StatusTextBox1)
        Me.Controls.Add(ContratoLabel)
        Me.Controls.Add(Me.ContratoTextBox)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel3)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Location = New System.Drawing.Point(445, 330)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmClientesTel"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Telefonía"
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.Consulta_ClientesTelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusMTABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.No_extensionesNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingNavigator3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator3.ResumeLayout(False)
        Me.BindingNavigator3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.Consulta_ContTelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraMotivoCancelacionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConRelCteDescuentoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipoPromocionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusTelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingNavigator4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator4.ResumeLayout(False)
        Me.BindingNavigator4.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.Consulta_ConTTelAdicBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRA_TiposPaqBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingNavigator6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator6.ResumeLayout(False)
        Me.BindingNavigator6.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.BindingNavigator5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator5.ResumeLayout(False)
        Me.BindingNavigator5.PerformLayout()
        CType(Me.BindingNavigator2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator2.ResumeLayout(False)
        Me.BindingNavigator2.PerformLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        CType(Me.Muestra_ClientesTelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACONTTELBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAPAQ_ADICBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Muestra_EquiposTelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChecaRelCteDescuentoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VerAcceso_ChecaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_clvtipusuarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents GuardarToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigator2 As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents GuardarToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents Procedimientosarnoldo4 As sofTV.Procedimientosarnoldo4
    Friend WithEvents ContratonetTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents StatusTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_cablemodemTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_telefonoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ObsTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Clv_unicanetTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContratonetTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Referencia_pagoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_servicioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents StatusTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents PrimerMensualidadTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Ultimo_MesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Ultimo_anioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FacturaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ObsTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CortesiaCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents CMBTextBox28 As System.Windows.Forms.TextBox
    Friend WithEvents BindingNavigator3 As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CMBTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents GuardarToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents No_extensionesNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents BindingNavigator4 As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents GuardarToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents DataSetLidia2 As sofTV.DataSetLidia2
    Friend WithEvents MUESTRACONTTELBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACONTTELTableAdapter As sofTV.DataSetLidia2TableAdapters.MUESTRACONTTELTableAdapter
    Friend WithEvents Muestra_ClientesTelBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_ClientesTelTableAdapter As sofTV.DataSetLidia2TableAdapters.Muestra_ClientesTelTableAdapter
    Friend WithEvents MacCablemodemTextBox As System.Windows.Forms.TextBox
    Friend WithEvents BindingNavigator5 As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents StatusMTABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents StatusMTATableAdapter As sofTV.DataSetLidia2TableAdapters.StatusMTATableAdapter
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents StatusTelBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents StatusTelTableAdapter As sofTV.DataSetLidia2TableAdapters.StatusTelTableAdapter
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents MuestraMotivoCancelacionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraMotivoCancelacionTableAdapter As sofTV.DataSetLidiaTableAdapters.MuestraMotivoCancelacionTableAdapter
    Friend WithEvents Consulta_ContTelBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_ContTelTableAdapter As sofTV.DataSetLidia2TableAdapters.Consulta_ContTelTableAdapter
    Friend WithEvents Consulta_ClientesTelBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_ClientesTelTableAdapter As sofTV.DataSetLidia2TableAdapters.Consulta_ClientesTelTableAdapter
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents CMBTextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents BindingNavigator6 As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton4 As System.Windows.Forms.ToolStripButton
    Friend WithEvents Consulta_ConTTelAdicBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_ConTTelAdicTableAdapter As sofTV.DataSetLidia2TableAdapters.Consulta_ConTTelAdicTableAdapter
    Friend WithEvents Clv_servicioTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Clv_unicanetTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents MUESTRAPAQ_ADICBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAPAQ_ADICTableAdapter As sofTV.DataSetLidia2TableAdapters.MUESTRAPAQ_ADICTableAdapter
    Friend WithEvents Muestra_EquiposTelBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_EquiposTelTableAdapter As sofTV.DataSetLidia2TableAdapters.Muestra_EquiposTelTableAdapter
    Friend WithEvents ComboBox5 As System.Windows.Forms.ComboBox
    Friend WithEvents MuestraTipoPromocionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipoPromocionTableAdapter As sofTV.DataSetLidia2TableAdapters.MuestraTipoPromocionTableAdapter
    Friend WithEvents Numero_telTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ChecaRelCteDescuentoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ChecaRelCteDescuentoTableAdapter As sofTV.DataSetEricTableAdapters.ChecaRelCteDescuentoTableAdapter
    Friend WithEvents ConRelCteDescuentoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConRelCteDescuentoTableAdapter As sofTV.DataSetEricTableAdapters.ConRelCteDescuentoTableAdapter
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents SolicitudTextBox1 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents InstalacionTextBox1 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents ActivacionTextBox2 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Ult_PAgoTextBox3 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents SuspensionTextBox4 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents SuspensionTelTextBox5 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents BajaTextBox6 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents BajaTelTextBox1 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents BajaAdicTextBox1 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents InstalacionAdicTextBox3 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents SolicitudAdicTextBox2 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents SuspensionAdicTextBox4 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents ComboBox6 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRA_TiposPaqBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRA_TiposPaqTableAdapter As sofTV.DataSetLidia2TableAdapters.MUESTRA_TiposPaqTableAdapter
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Clv_telefonoXtraTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents DataSetEdgarRev2 As sofTV.DataSetEdgarRev2
    Friend WithEvents VerAcceso_ChecaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VerAcceso_ChecaTableAdapter As sofTV.DataSetEdgarRev2TableAdapters.VerAcceso_ChecaTableAdapter
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Dame_clvtipusuarioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_clvtipusuarioTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Dame_clvtipusuarioTableAdapter
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents LblPlazoForzoso1 As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents cbTelUsuarioCapturo As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents FueraAreaMaskedTextBox1 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
