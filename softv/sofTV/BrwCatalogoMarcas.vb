﻿Public Class BrwCatalogoMarcas

    Private Sub bNuevo_Click(sender As Object, e As EventArgs) Handles bNuevo.Click

        bndCatalogoTelmex = "N"
        FrmMedioTelmexEliminar.ShowDialog()
        TraerTodasMarcas()

    End Sub

    Private Sub BrwCatalogoMarcas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name) '  ColoreaFrm(Me)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)

        IdMarcaT = 0

        TraerTodasMarcas()
        LlenarLabels()
    End Sub

    Private Sub bModificar_Click(sender As Object, e As EventArgs) Handles bModificar.Click
        bndCatalogoTelmex = "M"
        IdMarcaT = Convert.ToInt64(gridMarcas("IdMarca", gridMarcas.CurrentRow.Index).Value)

        FrmMedioTelmexEliminar.ShowDialog()
        TraerTodasMarcas()
    End Sub

    Private Sub BrwCatalogoIP_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated

    End Sub

    Private Sub TraerTodasMarcas()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdMarca", SqlDbType.BigInt, IdMarcaT)
        BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, "")
        BaseII.CreateMyParameter("@Opcion", SqlDbType.Int, 0)
        Dim dt As DataTable
        dt = BaseII.ConsultaDT("FiltroMarcas")
        Dim dr As DataRow
        gridMarcas.Rows.Clear()
        For Each dr In dt.Rows
            gridMarcas.Rows.Add()
            gridMarcas("IdMarca", gridMarcas.Rows.Count - 1).Value = dr("IdMarca").ToString()
            gridMarcas("Marca", gridMarcas.Rows.Count - 1).Value = dr("Marca").ToString()
        Next
    End Sub

    Private Sub TraerMarcas_ById(id_Marca As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdMarca", SqlDbType.BigInt, id_Marca)
        BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, "")
        BaseII.CreateMyParameter("@Opcion", SqlDbType.Int, 1)
        Dim dt As DataTable
        dt = BaseII.ConsultaDT("FiltroMarcas")
        Dim dr As DataRow
        gridMarcas.Rows.Clear()
        For Each dr In dt.Rows
            gridMarcas.Rows.Add()
            gridMarcas("IdMarca", gridMarcas.Rows.Count - 1).Value = dr("IdMarca").ToString()
            gridMarcas("Marca", gridMarcas.Rows.Count - 1).Value = dr("Marca").ToString()
        Next

    End Sub

    Private Sub TraerMarcas_ByMarca(DescripcionMarca As String)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdMarca", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, DescripcionMarca)
        BaseII.CreateMyParameter("@Opcion", SqlDbType.Int, 2)
        Dim dt As DataTable
        dt = BaseII.ConsultaDT("FiltroMarcas")
        Dim dr As DataRow
        gridMarcas.Rows.Clear()
        For Each dr In dt.Rows
            gridMarcas.Rows.Add()
            gridMarcas("IdMarca", gridMarcas.Rows.Count - 1).Value = dr("IdMarca").ToString()
            gridMarcas("Marca", gridMarcas.Rows.Count - 1).Value = dr("Marca").ToString()
        Next

    End Sub

    Private Sub LlenarLabels()
        If gridMarcas.Rows.Count = 0 Then
            elIdMarcaLbl.Text = ""
            laMarcaLbl.Text = ""
            Exit Sub
        End If
        If gridMarcas(0, 0).Value <> Nothing Then
            elIdMarcaLbl.Text = gridMarcas("IdMarca", gridMarcas.CurrentRow.Index).Value.ToString()
            laMarcaLbl.Text = gridMarcas("Marca", gridMarcas.CurrentRow.Index).Value.ToString()
        End If
    End Sub

    Private Sub gridIP_SelectionChanged(sender As Object, e As EventArgs) Handles gridMarcas.SelectionChanged
        LlenarLabels()
    End Sub

    Private Sub bSalir_Click(sender As Object, e As EventArgs) Handles bSalir.Click
        Me.Close()
    End Sub

    Private Sub bConsultar_Click(sender As Object, e As EventArgs) Handles bConsultar.Click
        bndCatalogoTelmex = "C"

        If gridMarcas.CurrentCell.Value Is Nothing Then
            IdMarcaT = 0
        Else
            IdMarcaT = Convert.ToInt64(gridMarcas("IdMarca", gridMarcas.CurrentRow.Index).Value)
        End If

        FrmMedioTelmexEliminar.ShowDialog()
        TraerTodasMarcas()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If idMarcaTxt.Text.Trim() = "" Then
            TraerTodasMarcas()
            Exit Sub
        End If

        IdMarcaT = Convert.ToInt32(idMarcaTxt.Text.Trim())
        TraerMarcas_ById(IdMarcaT)
        LlenarLabels()
    End Sub

    Private Sub btnBuscarPorNombre_Click(sender As Object, e As EventArgs) Handles btnBuscarPorNombre.Click
        If marcaTxt.Text.Trim() = "" Then
            TraerTodasMarcas()
            Exit Sub
        End If

        TraerMarcas_ByMarca(marcaTxt.Text.Trim())
        LlenarLabels()
    End Sub
End Class