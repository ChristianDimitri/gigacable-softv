﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTelmexMediosV1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim MarcaLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTelmexMediosV1))
        Me.Button5 = New System.Windows.Forms.Button()
        Me.CMBPanel1 = New System.Windows.Forms.Panel()
        Me.gridMediosPrecios = New System.Windows.Forms.DataGridView()
        Me.tipoCombo = New System.Windows.Forms.ComboBox()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Consulta_Cat_Num_TelefonoBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.colIdMedio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colChecked = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colPrecio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colGuardaPrecio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        MarcaLabel = New System.Windows.Forms.Label()
        Me.CMBPanel1.SuspendLayout()
        CType(Me.gridMediosPrecios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Cat_Num_TelefonoBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'MarcaLabel
        '
        MarcaLabel.AutoSize = True
        MarcaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MarcaLabel.Location = New System.Drawing.Point(19, 16)
        MarcaLabel.Name = "MarcaLabel"
        MarcaLabel.Size = New System.Drawing.Size(48, 16)
        MarcaLabel.TabIndex = 53
        MarcaLabel.Text = "Tipo :"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(512, 435)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(147, 36)
        Me.Button5.TabIndex = 26
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'CMBPanel1
        '
        Me.CMBPanel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMBPanel1.Controls.Add(Me.gridMediosPrecios)
        Me.CMBPanel1.Controls.Add(Me.tipoCombo)
        Me.CMBPanel1.Controls.Add(MarcaLabel)
        Me.CMBPanel1.Location = New System.Drawing.Point(12, 28)
        Me.CMBPanel1.Name = "CMBPanel1"
        Me.CMBPanel1.Size = New System.Drawing.Size(647, 401)
        Me.CMBPanel1.TabIndex = 24
        '
        'gridMediosPrecios
        '
        Me.gridMediosPrecios.AllowUserToAddRows = False
        Me.gridMediosPrecios.AllowUserToDeleteRows = False
        Me.gridMediosPrecios.AllowUserToResizeRows = False
        Me.gridMediosPrecios.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gridMediosPrecios.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.gridMediosPrecios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridMediosPrecios.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIdMedio, Me.colNombre, Me.colChecked, Me.colPrecio, Me.colGuardaPrecio})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gridMediosPrecios.DefaultCellStyle = DataGridViewCellStyle4
        Me.gridMediosPrecios.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.gridMediosPrecios.Location = New System.Drawing.Point(22, 58)
        Me.gridMediosPrecios.Name = "gridMediosPrecios"
        Me.gridMediosPrecios.ReadOnly = True
        Me.gridMediosPrecios.RowHeadersVisible = False
        Me.gridMediosPrecios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.gridMediosPrecios.Size = New System.Drawing.Size(608, 328)
        Me.gridMediosPrecios.TabIndex = 61
        Me.gridMediosPrecios.TabStop = False
        '
        'tipoCombo
        '
        Me.tipoCombo.DisplayMember = "DESCRIPCION"
        Me.tipoCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tipoCombo.FormattingEnabled = True
        Me.tipoCombo.Location = New System.Drawing.Point(89, 16)
        Me.tipoCombo.Name = "tipoCombo"
        Me.tipoCombo.Size = New System.Drawing.Size(331, 21)
        Me.tipoCombo.TabIndex = 60
        Me.tipoCombo.ValueMember = "CLV_TIPOCLIENTE"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(77, 22)
        Me.BindingNavigatorDeleteItem.Text = "Eliminar"
        '
        'Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem
        '
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Image = CType(resources.GetObject("Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Name = "Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem"
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 22)
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'Consulta_Cat_Num_TelefonoBindingNavigator
        '
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.AddNewItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.CountItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem})
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.MoveFirstItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.MoveLastItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.MoveNextItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.MovePreviousItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Name = "Consulta_Cat_Num_TelefonoBindingNavigator"
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.PositionItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Size = New System.Drawing.Size(699, 25)
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.TabIndex = 25
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Text = "BindingNavigator1"
        '
        'colIdMedio
        '
        Me.colIdMedio.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colIdMedio.FillWeight = 50.76141!
        Me.colIdMedio.HeaderText = "IdMedio"
        Me.colIdMedio.MinimumWidth = 20
        Me.colIdMedio.Name = "colIdMedio"
        Me.colIdMedio.ReadOnly = True
        '
        'colNombre
        '
        Me.colNombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colNombre.FillWeight = 149.2385!
        Me.colNombre.HeaderText = "Nombre"
        Me.colNombre.MinimumWidth = 50
        Me.colNombre.Name = "colNombre"
        Me.colNombre.ReadOnly = True
        '
        'colChecked
        '
        Me.colChecked.DataPropertyName = "Checked"
        Me.colChecked.HeaderText = "Checked"
        Me.colChecked.Name = "colChecked"
        Me.colChecked.ReadOnly = True
        '
        'colPrecio
        '
        Me.colPrecio.HeaderText = "Precio"
        Me.colPrecio.Name = "colPrecio"
        Me.colPrecio.ReadOnly = True
        '
        'colGuardaPrecio
        '
        Me.colGuardaPrecio.HeaderText = "Guarda Precio"
        Me.colGuardaPrecio.Name = "colGuardaPrecio"
        Me.colGuardaPrecio.ReadOnly = True
        '
        'FrmTelmexMedios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(699, 483)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Consulta_Cat_Num_TelefonoBindingNavigator)
        Me.Controls.Add(Me.CMBPanel1)
        Me.Name = "FrmTelmexMedios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Telmex"
        Me.CMBPanel1.ResumeLayout(False)
        Me.CMBPanel1.PerformLayout()
        CType(Me.gridMediosPrecios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Cat_Num_TelefonoBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.ResumeLayout(False)
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents CMBPanel1 As System.Windows.Forms.Panel
    Friend WithEvents tipoCombo As System.Windows.Forms.ComboBox
    Friend WithEvents gridMediosPrecios As System.Windows.Forms.DataGridView
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Consulta_Cat_Num_TelefonoBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents colIdMedio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colNombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colChecked As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colPrecio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colGuardaPrecio As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
