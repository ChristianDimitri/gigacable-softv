Imports System.Data.SqlClient
Public Class FrmSeleccionaTel

    Dim tel_extra As Integer
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim ConSof As New SqlConnection(MiConexion)



        If optTel = 2 Then
            Clv_EquipoTel = Me.ComboBox1.SelectedValue
            globndTel = True
            frmctr.Activar()
        ElseIf optTel = 3 Then

            If ComboBox2.Text.Length = 0 Then
                MessageBox.Show("Selecciona un Paquete Adicional.")
                Exit Sub
            End If

            clv_paqAdic = Me.ComboBox2.SelectedValue
            GloBndAdic = True
            frmctr.Activar()
        ElseIf optTel = 4 Then

            If ComboBox2.Text.Length = 0 Then
                MessageBox.Show("Selecciona un Servicio Digital.")
                Exit Sub
            End If

            gloclv_servicioDigital = Me.ComboBox2.SelectedValue
            GloBndAdic = True
            frmctr.Activar()
        ElseIf optTel = 5 And Me.CMBRadioButton1.Checked = True Then
            If Checa_Lineas() = 2 Then
                Asigna_Telefono(3)
            Else
                Asigna_Telefono(2)
            End If
            GloBndNum = True
            frmctr.Activar()
        ElseIf optTel = 5 And Me.CMDRadioButton2.Checked = True Then
            Me.Panel1.Show()
            Me.Panel2.Hide()
            Me.Panel3.Hide()
            ConSof.Open()
            Me.Muestra_EquiposTelTableAdapter.Connection = ConSof
            Me.Muestra_EquiposTelTableAdapter.Fill(Me.DataSetLidia2.Muestra_EquiposTel, 1)
            ConSof.Close()
            Me.Text = "Selecciona N�mero Telef�nico"
            Me.ComboBox1.Text = ""
            If Checa_Lineas() = 2 Then
                Me.Label3.Visible = True
                Me.ComboBox3.Visible = True
                Me.ComboBox3.Text = ""
            End If
            optTel = 1
            Exit Sub

            'ElseIf optTel = 5 And Me.TextBox1.Text = "" Then
            '    MsgBox("Es Necesario que se Capture un N�mero Tel�fonico V�lido", MsgBoxStyle.Information)
            '    Exit Sub
        End If
        If optTel = 1 Then
            Clv_NumTelefonico = Me.ComboBox1.SelectedValue
            If Me.ComboBox3.Visible = True Then
                tel_extra = Me.ComboBox3.SelectedValue
                Asigna_Telefono(4)
            Else
                Asigna_Telefono(1)
            End If

            GloBndNum = True
            frmctr.Activar()
        End If
        Me.Close()
    End Sub
    Private Function Checa_Lineas() As Integer
        Dim ConCheck As New SqlConnection(MiConexion)
        Dim Cmd As New SqlCommand
        ConCheck.Open()
        With Cmd
            .CommandText = "Checa_LineasTel" ' (@clv_servicio bigint,@lineas int output)"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = ConCheck
            Dim pmt As New SqlParameter("@clv_servicio", SqlDbType.Int)
            Dim pmt2 As New SqlParameter("@lineas", SqlDbType.Int)

            pmt.Direction = ParameterDirection.Input
            pmt2.Direction = ParameterDirection.Output

            pmt.Value = GloClv_servTel
            pmt2.Value = 0
            .Parameters.Add(pmt)
            .Parameters.Add(pmt2)

            .ExecuteNonQuery()
            Checa_Lineas = pmt2.Value

        End With
        ConCheck.Close()
    End Function

    Private Sub Asigna_Telefono(ByVal opt As Integer)
        Dim conSoftv2 As New SqlConnection(MiConexion)
        Dim Cmd As New SqlCommand
        conSoftv2.Open()
        With Cmd
            .CommandText = "Asigna_Telefono"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = conSoftv2
            Dim para As New SqlParameter("@Clv_Telefono", SqlDbType.BigInt)
            Dim para2 As New SqlParameter("@contrato", SqlDbType.BigInt)
            Dim Para3 As New SqlParameter("@opc", SqlDbType.BigInt)
            Dim Para4 As New SqlParameter("@Clv_telnew", SqlDbType.BigInt)
            Dim Para5 As New SqlParameter("@Clv_UnicaNet", SqlDbType.BigInt)
            Dim Para6 As New SqlParameter("@Clv_telnew2", SqlDbType.BigInt)

            para.Direction = ParameterDirection.Input
            para2.Direction = ParameterDirection.Input
            Para3.Direction = ParameterDirection.Input
            Para4.Direction = ParameterDirection.Output
            Para5.Direction = ParameterDirection.Input
            Para6.Direction = ParameterDirection.Input


            para.Value = Clv_NumTelefonico
            para2.Value = Contrato
            Para3.Value = opt
            Para4.Value = 0
            Para5.Value = Clv_UnicaTel
            Para6.Value = tel_extra

            .Parameters.Add(para)
            .Parameters.Add(para2)
            .Parameters.Add(Para3)
            .Parameters.Add(Para4)
            .Parameters.Add(Para5)
            .Parameters.Add(Para6)

            .ExecuteNonQuery()
            If opt = 2 Then
                Clv_NumTelefonico = Para4.Value
            End If
        End With
        conSoftv2.Close()
        
    End Sub

    Private Sub Guardar_NumTel()
        Dim conex As New SqlConnection(MiConexion)
        Dim Cmd As New SqlCommand
        conex.Open()
        With Cmd
            .CommandText = "Nuevo_Cat_Num_Telefono"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = conex
            '@clv_telefono bigint output,@notelefono varchar(max),@status varchar(1),@Ultima_fecha_Asig 
            'datetime,@Ultima_fecha_Liberacion datetime,@Ultimo_cliente_asigno bigint,@Fecha_cuarentena 'datetime)
            Dim par1 As New SqlParameter("@Clv_Telefono", SqlDbType.BigInt)
            Dim par2 As New SqlParameter("@NoTelefono", SqlDbType.VarChar, 1000)
            Dim par3 As New SqlParameter("@Status", SqlDbType.VarChar, 1)
            Dim par4 As New SqlParameter("@Ultima_Fecha_asig", SqlDbType.DateTime)
            Dim par5 As New SqlParameter("@Ultima_Fecha_Liberacion", SqlDbType.DateTime)
            Dim par6 As New SqlParameter("@Ultimo_cliente_asigno", SqlDbType.BigInt)
            Dim par7 As New SqlParameter("@Fecha_Cuarentena", SqlDbType.DateTime)
            par1.Direction = ParameterDirection.Output
            par2.Direction = ParameterDirection.Input
            par3.Direction = ParameterDirection.Input
            par4.Direction = ParameterDirection.Input
            par5.Direction = ParameterDirection.Input
            par6.Direction = ParameterDirection.Input
            par7.Direction = ParameterDirection.Input
            par1.Value = 0
            par2.Value = Me.TextBox1.Text
            par3.Value = "A"
            par4.Value = fecha_Hoy
            par5.Value = "01/01/1900"
            par6.Value = Contrato
            par7.Value = "01/01/1900"
            .Parameters.Add(par1)
            .Parameters.Add(par2)
            .Parameters.Add(par3)
            .Parameters.Add(par4)
            .Parameters.Add(par5)
            .Parameters.Add(par6)
            .Parameters.Add(par7)
            Dim a As Integer = .ExecuteNonQuery
            Clv_NumTelefonico = par1.Value
        End With
    End Sub
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        optTel = 0
        Me.Close()
    End Sub

    Private Sub FrmSeleccionaTel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        frmctr.MdiParent = FrmClientes
        Dim cone As New SqlClient.SqlConnection(MiConexion)
        
        If optTel = 1 Or optTel = 2 Then
            Me.Panel1.Show()
            Me.Panel2.Hide()
            Me.Panel3.Hide()
            cone.Open()
            Me.Muestra_EquiposTelTableAdapter.Connection = cone
            Me.Muestra_EquiposTelTableAdapter.Fill(Me.DataSetLidia2.Muestra_EquiposTel, optTel)
            cone.Close()
            Me.Text = "Selecciona N�mero Telef�nico"
        ElseIf optTel = 3 Or optTel = 4 Then
            Me.Panel2.Show()
            Me.Panel1.Hide()
            Me.Panel3.Hide()
            Me.Label4.Text = maccablemodem
            cone.Open()
            Me.Muestra_Paquetes_AdicTableAdapter.Connection = cone
            Me.Muestra_Paquetes_AdicTableAdapter.Fill(Me.DataSetLidia2.Muestra_Paquetes_Adic, Clv_UnicaTel, Tipo_Servicio, optTel)
            cone.Close()
            Me.ComboBox2.SelectedValue = 0
            Me.ComboBox2.Text = ""
        ElseIf optTel = 5 Then
            Me.Panel1.Hide()
            Me.Panel2.Hide()
            Me.Panel3.Show()
            Me.Text = "Asignaci�n de N�mero Telef�nico"
        End If
        If optTel = 3 Then
            Me.Label2.Text = "Seleccionar el Plan Adicional a la Mac :"
            Me.Text = "Selecciona un Plan Adicional"
        ElseIf optTel = 4 Then
            Me.Label2.Text = "Seleccionar el Servicio Digital a la Mac :"
            Me.Text = "Selecciona un Servicio Digital"
        End If
    End Sub

End Class