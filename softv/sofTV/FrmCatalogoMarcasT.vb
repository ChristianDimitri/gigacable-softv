﻿Public Class FrmMedioTelmexEliminar

    Private Sub FrmCatalogoMarcasT_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'ColoreaFrm(Me)
        colorea(Me, Me.Name)

        If bndCatalogoTelmex = "M" Then
            Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Enabled = True
            BindingNavigatorDeleteItem.Enabled = True
            marcaTxt.Enabled = True

        ElseIf bndCatalogoTelmex = "C" Then
            Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Enabled = False
            BindingNavigatorDeleteItem.Enabled = False
            marcaTxt.Enabled = False

        ElseIf bndCatalogoTelmex = "N" Then
            Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Enabled = True
            BindingNavigatorDeleteItem.Enabled = False
            marcaTxt.Enabled = True

        End If

        If bndCatalogoTelmex = "M" Or bndCatalogoTelmex = "C" Then

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IdMarca", SqlDbType.BigInt, IdMarcaT)
            Dim dt As DataTable
            dt = BaseII.ConsultaDT("GetMarcasByIdMarca")
            Dim dr As DataRow
            For Each dr In dt.Rows
                IdTxt.Text = dr("IdMarca")
                marcaTxt.Text = dr("Marca")
            Next
        End If

    End Sub

    Private Sub Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Click
        Dim yaExiste As Boolean = 0
        'Dim idMarca As Long = 0

        If marcaTxt.Text.Trim() = "" Then
            MessageBox.Show("Ingrese la Marca")
            Exit Sub
        End If
        'If cbStatus.Text.Trim() = "" Then
        '    MessageBox.Show("Ingrese el status")
        '    Exit Sub
        'End If

        If bndCatalogoTelmex = "N" Then

            'BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, marcaTxt.Text, 250)
            'BotonesDesactivar = BaseII.ConsultaDT("AddMarcas")
            'yaExiste = CBool(BotonesDesactivar.Rows(0)(1).ToString)



            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, marcaTxt.Text, 250)
            Dim dt As DataTable
            dt = BaseII.ConsultaDT("AddMarcas")
            Dim dr As DataRow
            For Each dr In dt.Rows
                ' idMarca = dr("IdMarca")
                yaExiste = dr("Error")
            Next

            If yaExiste = True Then
                MessageBox.Show("Ya existe esta Marca")
            Else
                MessageBox.Show("Marca agregada correctamente")
            End If
        End If
        If bndCatalogoTelmex = "M" Then

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IdMarca", SqlDbType.BigInt, Convert.ToInt64(IdTxt.Text))
            BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, marcaTxt.Text, 250)
            Dim dt As DataTable
            dt = BaseII.ConsultaDT("UpdateMarcas")
            Dim dr As DataRow
            For Each dr In dt.Rows
                'idMarca = dr("IdMarca")
                yaExiste = dr("Error")
            Next

            If yaExiste = True Then
                MessageBox.Show("No se ha actualizado")
            Else
                MessageBox.Show("Marca actualizada correctamente")
            End If
        End If
        Me.Close()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    Private Sub FrmCatalogoMarcas_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        IdTxt.Text = ""
        marcaTxt.Text = ""

    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(sender As Object, e As EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim err As Integer = 0

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdMarca", SqlDbType.VarChar, Convert.ToInt64(IdTxt.Text))
        Dim dt As DataTable
        dt = BaseII.ConsultaDT("DeleteMarcas")
        Dim dr As DataRow
        For Each dr In dt.Rows
            'idMarca = dr("IdMarca")
            err = dr("Error")
        Next

        If err = 0 Then
            MessageBox.Show("Marca eliminada correctamente")
        ElseIf err = 1 Then
            MessageBox.Show("No se pudo eliminar la marca porque está relacionada a un modelo")
        ElseIf err = 2 Then
            MessageBox.Show("No se pudo eliminar la marca porque está relacionada a un cliente")
        End If
        Me.Close()
    End Sub

End Class