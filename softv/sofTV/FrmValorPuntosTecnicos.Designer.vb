﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmValorPuntosTecnicos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cbRangoPuntos = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.gbDatosRangoHorario = New System.Windows.Forms.GroupBox()
        Me.txtValor = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtPuntoFinal = New System.Windows.Forms.TextBox()
        Me.txtPuntoInicial = New System.Windows.Forms.TextBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnBuscarRango = New System.Windows.Forms.Button()
        Me.dgValorPuntos = New System.Windows.Forms.DataGridView()
        Me.CLAVE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PUNTOINICIAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PUNTOFINAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VALOR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1.SuspendLayout()
        Me.gbDatosRangoHorario.SuspendLayout()
        CType(Me.dgValorPuntos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(647, 432)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(113, 33)
        Me.btnSalir.TabIndex = 22
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModificar.Location = New System.Drawing.Point(647, 65)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(113, 33)
        Me.btnModificar.TabIndex = 21
        Me.btnModificar.Text = "Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar.Location = New System.Drawing.Point(647, 22)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(113, 33)
        Me.btnAgregar.TabIndex = 20
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cbRangoPuntos)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.gbDatosRangoHorario)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.btnBuscarRango)
        Me.Panel1.Controls.Add(Me.dgValorPuntos)
        Me.Panel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(9, 18)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(632, 450)
        Me.Panel1.TabIndex = 19
        '
        'cbRangoPuntos
        '
        Me.cbRangoPuntos.DisplayMember = "Puntos"
        Me.cbRangoPuntos.FormattingEnabled = True
        Me.cbRangoPuntos.Location = New System.Drawing.Point(4, 66)
        Me.cbRangoPuntos.Name = "cbRangoPuntos"
        Me.cbRangoPuntos.Size = New System.Drawing.Size(197, 23)
        Me.cbRangoPuntos.TabIndex = 19
        Me.cbRangoPuntos.ValueMember = "ID"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(1, 48)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(121, 15)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Rango de Puntos:"
        '
        'gbDatosRangoHorario
        '
        Me.gbDatosRangoHorario.Controls.Add(Me.txtValor)
        Me.gbDatosRangoHorario.Controls.Add(Me.Label2)
        Me.gbDatosRangoHorario.Controls.Add(Me.txtPuntoFinal)
        Me.gbDatosRangoHorario.Controls.Add(Me.txtPuntoInicial)
        Me.gbDatosRangoHorario.Controls.Add(Me.btnCancelar)
        Me.gbDatosRangoHorario.Controls.Add(Me.Label5)
        Me.gbDatosRangoHorario.Controls.Add(Me.Label4)
        Me.gbDatosRangoHorario.Controls.Add(Me.Label1)
        Me.gbDatosRangoHorario.Controls.Add(Me.btnGuardar)
        Me.gbDatosRangoHorario.Location = New System.Drawing.Point(240, 150)
        Me.gbDatosRangoHorario.Name = "gbDatosRangoHorario"
        Me.gbDatosRangoHorario.Size = New System.Drawing.Size(317, 129)
        Me.gbDatosRangoHorario.TabIndex = 6
        Me.gbDatosRangoHorario.TabStop = False
        Me.gbDatosRangoHorario.Text = "Valor de Puntos por Rango"
        '
        'txtValor
        '
        Me.txtValor.Location = New System.Drawing.Point(142, 73)
        Me.txtValor.Name = "txtValor"
        Me.txtValor.Size = New System.Drawing.Size(100, 21)
        Me.txtValor.TabIndex = 28
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(92, 79)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 15)
        Me.Label2.TabIndex = 27
        Me.Label2.Text = "Valor:"
        '
        'txtPuntoFinal
        '
        Me.txtPuntoFinal.Location = New System.Drawing.Point(195, 46)
        Me.txtPuntoFinal.Name = "txtPuntoFinal"
        Me.txtPuntoFinal.Size = New System.Drawing.Size(100, 21)
        Me.txtPuntoFinal.TabIndex = 26
        '
        'txtPuntoInicial
        '
        Me.txtPuntoInicial.Location = New System.Drawing.Point(3, 47)
        Me.txtPuntoInicial.Name = "txtPuntoInicial"
        Me.txtPuntoInicial.Size = New System.Drawing.Size(100, 21)
        Me.txtPuntoInicial.TabIndex = 25
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(236, 100)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 24
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(139, 49)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(12, 15)
        Me.Label5.TabIndex = 23
        Me.Label5.Text = "-"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(192, 29)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(84, 15)
        Me.Label4.TabIndex = 22
        Me.Label4.Text = "Punto Final:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(0, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(91, 15)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Punto inicial:"
        '
        'btnGuardar
        '
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.Location = New System.Drawing.Point(155, 100)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 7
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(3, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(126, 15)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Buscar Rango por:"
        '
        'btnBuscarRango
        '
        Me.btnBuscarRango.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscarRango.Location = New System.Drawing.Point(126, 95)
        Me.btnBuscarRango.Name = "btnBuscarRango"
        Me.btnBuscarRango.Size = New System.Drawing.Size(75, 23)
        Me.btnBuscarRango.TabIndex = 1
        Me.btnBuscarRango.Text = "Buscar"
        Me.btnBuscarRango.UseVisualStyleBackColor = True
        '
        'dgValorPuntos
        '
        Me.dgValorPuntos.AllowUserToAddRows = False
        Me.dgValorPuntos.AllowUserToDeleteRows = False
        Me.dgValorPuntos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgValorPuntos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgValorPuntos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CLAVE, Me.PUNTOINICIAL, Me.PUNTOFINAL, Me.VALOR})
        Me.dgValorPuntos.Location = New System.Drawing.Point(207, 13)
        Me.dgValorPuntos.MultiSelect = False
        Me.dgValorPuntos.Name = "dgValorPuntos"
        Me.dgValorPuntos.ReadOnly = True
        Me.dgValorPuntos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgValorPuntos.Size = New System.Drawing.Size(406, 424)
        Me.dgValorPuntos.TabIndex = 1
        '
        'CLAVE
        '
        Me.CLAVE.DataPropertyName = "CLAVE"
        Me.CLAVE.HeaderText = "Clave"
        Me.CLAVE.Name = "CLAVE"
        Me.CLAVE.ReadOnly = True
        '
        'PUNTOINICIAL
        '
        Me.PUNTOINICIAL.DataPropertyName = "PUNTOINICIAL"
        Me.PUNTOINICIAL.HeaderText = "Punto inicial"
        Me.PUNTOINICIAL.Name = "PUNTOINICIAL"
        Me.PUNTOINICIAL.ReadOnly = True
        '
        'PUNTOFINAL
        '
        Me.PUNTOFINAL.DataPropertyName = "PUNTOFINAL"
        Me.PUNTOFINAL.HeaderText = "Punto final"
        Me.PUNTOFINAL.Name = "PUNTOFINAL"
        Me.PUNTOFINAL.ReadOnly = True
        '
        'VALOR
        '
        Me.VALOR.DataPropertyName = "VALOR"
        Me.VALOR.HeaderText = "Valor"
        Me.VALOR.Name = "VALOR"
        Me.VALOR.ReadOnly = True
        '
        'FrmValorPuntosTecnicos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(768, 487)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmValorPuntosTecnicos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Valor de Puntos de Técnicos"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.gbDatosRangoHorario.ResumeLayout(False)
        Me.gbDatosRangoHorario.PerformLayout()
        CType(Me.dgValorPuntos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cbRangoPuntos As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents gbDatosRangoHorario As System.Windows.Forms.GroupBox
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnBuscarRango As System.Windows.Forms.Button
    Friend WithEvents dgValorPuntos As System.Windows.Forms.DataGridView
    Friend WithEvents txtValor As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtPuntoFinal As System.Windows.Forms.TextBox
    Friend WithEvents txtPuntoInicial As System.Windows.Forms.TextBox
    Friend WithEvents CLAVE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PUNTOINICIAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PUNTOFINAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VALOR As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
