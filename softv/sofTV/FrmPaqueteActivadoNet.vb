﻿Public Class FrmPaqueteActivadoNet

    Private Sub FrmPaqueteActivadoNet_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        If Len(TextBox1.Text) > 0 And IsNumeric(TextBox1.Text) Then
            

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, TextBox1.Text)
            MAcCableModemComboBox.DataSource = BaseII.ConsultaDT("DameTarjetasClientesNet")
            MAcCableModemComboBox.DisplayMember = "MAcCableModem"
            MAcCableModemComboBox.ValueMember = "Clv_UnicaNet"
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            If IsNumeric(Me.TextBox1.Text) = True Then
                If Me.MAcCableModemComboBox.Text.Length > 0 Then
                    If Me.DescripcionComboBox.Text.Length > 0 Then


                        BaseII.limpiaParametros()
                        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, TextBox1.Text)
                        BaseII.CreateMyParameter("@CLV_UNICANET", SqlDbType.BigInt, Me.MAcCableModemComboBox.SelectedValue)
                        BaseII.CreateMyParameter("@mac_address", SqlDbType.VarChar, Me.MAcCableModemComboBox.Text, 50)
                        BaseII.CreateMyParameter("@paquete", SqlDbType.VarChar, Me.DescripcionComboBox.SelectedValue, 500)
                        BaseII.CreateMyParameter("@comando", SqlDbType.VarChar, "", 50)
                        BaseII.CreateMyParameter("@resultado", SqlDbType.Int, 0)
                        BaseII.CreateMyParameter("@Clv_Orden", SqlDbType.BigInt, 0)
                        BaseII.CreateMyParameter("@Aplica", SqlDbType.Int, 1)
                        BaseII.CreateMyParameter("@Clv_Usuario", SqlDbType.BigInt, eClv_Usuario)
                        BaseII.CreateMyParameter("@Dias", SqlDbType.Int, Me.NumericUpDown1.Value)
                        BaseII.Inserta("MANDA_CNR_DIG_2Net")
                        
                        'Me.MANDA_CNR_DIG_2TableAdapter.Connection = CON
                        'Me.MANDA_CNR_DIG_2TableAdapter.Fill(Me.DataSetEric.MANDA_CNR_DIG_2, Me.TextBox1.Text, 0, Me.MAcCableModemComboBox.SelectedValue, Me.DescripcionComboBox.SelectedValue, "SCENTL", 0, 0, 1, eClv_Usuario, Me.NumericUpDown1.Value)
                        bitsist(GloUsuario, CLng(Me.TextBox1.Text), LocGloSistema, Me.Name, "Se Activo Un Paquete De Prueba", "Cable modem: " + Me.MAcCableModemComboBox.Text, "Se Activo Un Paquete De Prueba: " + Me.DescripcionComboBox.Text, LocClv_Ciudad)
                        MsgBox("Orden Efectuada.")


                        Me.TextBox1.Text = "0"
                        Me.NumericUpDown1.Value = 1
                    Else
                        MsgBox("Selecciona un Paquete.", , "Atención")
                    End If
                Else
                    MsgBox("Selecciona el cable modem.", , "Atención")
                End If
            Else
                MsgBox("Captura el Contrato.", , "Atención")
            End If

        Catch
            MsgBox("Se ha producido un Error.")
        End Try
    End Sub

    Private Sub MAcCableModemComboBox_SelectedValueChanged(sender As Object, e As EventArgs) Handles MAcCableModemComboBox.SelectedValueChanged
        If IsNumeric(MAcCableModemComboBox.SelectedValue) Then
            If MAcCableModemComboBox.SelectedValue > 0 Then
                DescripcionComboBox.Text = ""
                MAcCableModemComboBox.Text = ""
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clv_unicanet", SqlDbType.BigInt, MAcCableModemComboBox.SelectedValue)
                DescripcionComboBox.DataSource = BaseII.ConsultaDT("DamePaquetesNetPrueba")
                DescripcionComboBox.DisplayMember = "Descripcion"
                DescripcionComboBox.ValueMember = "Clv_Txt"
            End If
        End If
        
    End Sub
End Class