Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Public Class FrmEquipoVenta


    Private Sub Consultar_EquiposVenta_Costo()
        Dim Con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        'Contrato
        If Contrato = Nothing Then Contrato = 0
        Try
            'Muestra_Plazo_Forzozo (@contrato bigint,@Clv_unicanet bigint,@Plazo varchar(10) output)
            cmd = New SqlCommand()
            Con.Open()
            With cmd
                .CommandText = "Consultar_EquiposVenta_Costo"
                .Connection = Con
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@Clv_equipo", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = Me.Clv_EquipoTextBox.Text
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Costo", SqlDbType.Money)
                prm1.Direction = ParameterDirection.Output
                'If IsNumeric(Me.TextCosto.Text) = True Then
                'prm1.Value = Me.TextCosto.Text
                'Else
                'prm1.Value = 0
                'End If

                .Parameters.Add(prm1)

                Dim ia As Integer = .ExecuteNonQuery()

                Me.TextCosto.Text = Microsoft.VisualBasic.FormatCurrency(prm1.Value, 2)

            End With
            Con.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Modifica_EquiposVenta_Costo()
        Dim Con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        'Contrato
        If Contrato = Nothing Then Contrato = 0
        Try
            'Muestra_Plazo_Forzozo (@contrato bigint,@Clv_unicanet bigint,@Plazo varchar(10) output)
            cmd = New SqlCommand()
            Con.Open()
            With cmd
                .CommandText = "Modifica_EquiposVenta_Costo"
                .Connection = Con
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@Clv_equipo", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = Me.Clv_EquipoTextBox.Text
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Costo", SqlDbType.Money)
                prm1.Direction = ParameterDirection.Input
                If IsNumeric(Me.TextCosto.Text) = True Then
                    prm1.Value = Me.TextCosto.Text
                Else
                    prm1.Value = 0
                End If

                .Parameters.Add(prm1)

                Dim ia As Integer = .ExecuteNonQuery()

            End With
            Con.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If IsNumeric(Me.TextBox3.Text) = True And IsNumeric(GloClv_Servicio) = True Then
            Cambiar_Relacion(1)
        Else
            MsgBox("Capture los Datos Adecuados a Agregar", MsgBoxStyle.Information)
        End If

    End Sub
    Private Sub Cambiar_Relacion(ByVal opt As Integer)
        Dim Cmd As New SqlCommand
        Dim Nombre As String
        Dim Consoft As New SqlConnection(MiConexion)
        Dim cuantosenelgrid As Integer = 0
        cuantosenelgrid = Me.Muestra_Rel_EquipoPlanDataGridView.RowCount
        Consoft.Open()
        If opt = 1 Then
            Nombre = "Nueva_Rel_EquipoPlan"
        ElseIf opt = 2 Then
            Nombre = "Modifica_Rel_EquipoPlan"
        ElseIf opt = 3 Then
            Nombre = "Borra_Rel_EquipoPlan"
        End If
        With Cmd
            .CommandText = Nombre
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = Consoft

            Dim Pmt As New SqlParameter("@clv_equipo", SqlDbType.BigInt)
            Dim Pmt2 As New SqlParameter("@clv_Servicio", SqlDbType.BigInt)
            Dim Pmt25 As New SqlParameter("@clv_tipser", SqlDbType.BigInt)
            Dim Pmt3 As New SqlParameter("@Costo", SqlDbType.Money)

            Pmt.Direction = ParameterDirection.Input
            Pmt2.Direction = ParameterDirection.Input
            Pmt25.Direction = ParameterDirection.Input
            pmt3.Direction = ParameterDirection.Input

            Pmt.Value = Clv_Equipo
            If opt <> 3 Then
                Pmt2.Value = GloClv_Servicio
            ElseIf opt = 3 Then
                Pmt2.Value = Me.TextBox1.Text
            End If
            Pmt25.Value = 5
            pmt3.Value = Me.TextBox3.Text
            .Parameters.Add(Pmt)
            .Parameters.Add(Pmt2)
            If opt <> 3 Then
                .Parameters.Add(Pmt25)
                .Parameters.Add(Pmt3)
            End If
            Dim i As Integer = .ExecuteNonQuery
        End With
        Consoft.Close()
        Consoft.Open()
        Me.Muestra_Rel_EquipoPlanTableAdapter.Connection = Consoft
        Me.Muestra_Rel_EquipoPlanTableAdapter.Fill(Me.DataSetLidia2.Muestra_Rel_EquipoPlan, Clv_Equipo, 0)
        Consoft.Close()
        If opt <> 3 Then
            If Me.Muestra_Rel_EquipoPlanDataGridView.RowCount > cuantosenelgrid Then
                MsgBox("Se Ha Guardado con �xito", MsgBoxStyle.Information)
            End If
        ElseIf opt = 3 Then
            MsgBox("Se Ha Eliminado con �xito", MsgBoxStyle.Information)
        End If
    End Sub
    Private Sub Activa(ByVal modo As Boolean)
        Me.Clv_EquipoTextBox.ReadOnly = Not (modo)
        Me.NombreTextBox.ReadOnly = Not (modo)
        Me.DescripcionTextBox.ReadOnly = Not (modo)
        Me.ComboBox1.Enabled = modo
        Me.TextBox3.ReadOnly = Not (modo)
        Me.Button2.Enabled = modo
        Me.Button4.Enabled = modo
    End Sub
    Private Sub FrmEquipoVenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim Consoft As New SqlConnection(MiConexion)
        If opcion = "N" Then
            Me.Consulta_EquiposVentaBindingSource.AddNew()
            Activa(True)
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
            Me.Consulta_EquiposVentaBindingNavigatorSaveItem.Enabled = True
            Me.BindingNavigatorDeleteItem.Enabled = False
        ElseIf opcion = "C" Or opcion = "M" Then
            Consoft.Open()
            Me.Consulta_EquiposVentaTableAdapter.Connection = Consoft
            Me.Consulta_EquiposVentaTableAdapter.Fill(Me.DataSetLidia2.Consulta_EquiposVenta, Clv_Equipo)
            Me.Muestra_Rel_EquipoPlanTableAdapter.Connection = Consoft
            Me.Muestra_Rel_EquipoPlanTableAdapter.Fill(Me.DataSetLidia2.Muestra_Rel_EquipoPlan, Me.Clv_EquipoTextBox.Text, 0)
            Consoft.Close()
            Consultar_EquiposVenta_Costo()
        End If
        Consoft.Open()
        Me.MuestraServiciosTableAdapter.Connection = Consoft
        Me.MuestraServiciosTableAdapter.Fill(Me.DataSetLidia2.MuestraServicios, 5)
        Consoft.Close()
        If opcion = "C" Then
            Me.Consulta_EquiposVentaBindingNavigatorSaveItem.Enabled = False
            Me.BindingNavigatorDeleteItem.Enabled = False
            Me.TextCosto.Enabled = False
            Me.Clv_EquipoTextBox.Enabled = False
            Activa(False)
        ElseIf opcion = "M" Then
            Activa(True)
        End If
        Me.ComboBox1.Text = ""
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Cambiar_Relacion(3)
    End Sub

    Private Sub Consulta_EquiposVentaBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Consulta_EquiposVentaBindingNavigatorSaveItem.Click
        Dim ConSoft2 As New SqlConnection(MiConexion)
        If Me.NombreTextBox.Text.Trim.Length = 0 Then
            MsgBox("Se Debe de Captura un Nombre")
            Exit Sub
        Else
            Me.Validate()
            Me.Consulta_EquiposVentaBindingSource.EndEdit()
            ConSoft2.Open()
            Me.Consulta_EquiposVentaTableAdapter.Connection = ConSoft2
            Me.Consulta_EquiposVentaTableAdapter.Update(Me.DataSetLidia2.Consulta_EquiposVenta)
            ConSoft2.Close()
            Modifica_EquiposVenta_Costo()
            MsgBox("Se Ha Guardado con �xito", MsgBoxStyle.Information)
            bec_bnd = True
            Me.Button2.Enabled = True
            Me.Button4.Enabled = True
            'ConSoft2.Open()
            'Me.Consulta_EquiposVentaTableAdapter.Connection = ConSoft2
            'Me.Consulta_EquiposVentaTableAdapter.Fill(Me.DataSetLidia2.Consulta_EquiposVenta, Me.Clv_EquipoTextBox.Text)
            'ConSoft2.Close()
        End If

    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim condel As New SqlConnection(MiConexion)
        condel.Open()
        Me.Consulta_EquiposVentaTableAdapter.Connection = condel
        Me.Consulta_EquiposVentaTableAdapter.Delete(Me.Clv_EquipoTextBox.Text)
        condel.Close()
        MsgBox("Se Ha Eliminado con �xito", MsgBoxStyle.Information)
        bec_bnd = True
        Me.Close()
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If IsNumeric(Me.ComboBox1.SelectedValue) = True Then
            GloClv_Servicio = Me.ComboBox1.SelectedValue
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub Clv_EquipoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_EquipoTextBox.TextChanged
        If IsNumeric(Me.Clv_EquipoTextBox.Text) = True Then
            Clv_Equipo = Me.Clv_EquipoTextBox.Text
        End If
    End Sub

    Private Sub TextCosto_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextCosto.TextChanged

    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox3, Asc(LCase(e.KeyChar)), "N")))
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(BwrEquipoVenta.SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub

    Private Sub TextBox3_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox3.LostFocus
        If IsNumeric(TextBox3.Text) = True Then
            If Me.TextBox3.Text > 100 Then
                Me.TextBox3.Text = 100
            ElseIf Me.TextBox3.Text < 0 Then
                Me.TextBox3.Text = 0
            End If
        End If
    End Sub

   
    Private Sub TextCosto_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextCosto.KeyPress

        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(BwrEquipoVenta.SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub
End Class