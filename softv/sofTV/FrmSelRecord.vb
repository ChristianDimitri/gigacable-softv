Imports System.Data.SqlClient
Public Class FrmSelRecord
    Private Sub FrmSelRecord_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Muestra_RecordatorioTableAdapter.Connection = con
        Me.Muestra_RecordatorioTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Recordatorio)
        Me.NombreComboBox.Text = ""
        CON.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.NombreComboBox.Text = "" Then
            MsgBox("Seleccione por favor un Recordatorio", MsgBoxStyle.Information)
        Else
            LocClave_txt_aviso = Me.NombreComboBox.SelectedValue
            bndAvisos2 = True
            Me.Close()
        End If
    End Sub
End Class