﻿Imports CrystalDecisions.CrystalReports.Engine
Imports System.Collections.Generic

Public Class FrmComisionesGrupoVentas

#Region "Eventos"
    Private Sub FrmComisionesGrupoVentas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        MuestraAniosComisiones()
        UspMesesComisiones()
        Label2.ForeColor = Color.Black
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub
#End Region

#Region "Funciones"
    Private Sub MuestraAniosComisiones()
        Try
            BaseII.limpiaParametros()
            CMBAnio.DataSource = BaseII.ConsultaDT("MuestraAniosComisiones")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMesesComisiones()
        Try

            BaseII.limpiaParametros()

            CMBMes.DataSource = BaseII.ConsultaDT("UspMesesComisiones")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function UspReporteComisones() As DataSet
        Try
            Dim Rango As Integer
            If RBRangoPrimer.Checked = True Then
                Rango = 1
            ElseIf RBRangoSegundo.Checked = True Then
                Rango = 2
            End If

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@RANGO", SqlDbType.Int, Rango)
            BaseII.CreateMyParameter("@MES", SqlDbType.Int, Me.CMBMes.SelectedValue)
            BaseII.CreateMyParameter("@ANIO", SqlDbType.Int, CInt(Me.CMBAnio.Text))

            Dim tableNameList As New List(Of String)
            tableNameList.Add("UspReporteVentasSupervisores")
            'tableNameList.Add("UspReporteVentasVendedores")

            Return BaseII.ConsultaDS("UspReporteVentasProGrupo", tableNameList)
        Catch ex As Exception

        End Try
    End Function

    Private Function UspReporteComisonesGenera()
        Try
            Dim Rango As Integer
            If RBRangoPrimer.Checked = True Then
                Rango = 1
            ElseIf RBRangoSegundo.Checked = True Then
                Rango = 2
            End If

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@RANGO", SqlDbType.Int, Rango)
            BaseII.CreateMyParameter("@MES", SqlDbType.Int, Me.CMBMes.SelectedValue)
            BaseII.CreateMyParameter("@ANIO", SqlDbType.Int, CInt(Me.CMBAnio.Text))
            BaseII.Inserta("UspReporteComisonesGenera")
            'Dim tableNameList As New List(Of String)
            'tableNameList.Add("UspReporteVentasSupervisores")
            'tableNameList.Add("UspReporteVentasVendedores")

            'Return BaseII.ConsultaDS("UspReporteComisonesGenera", tableNameList)
        Catch ex As Exception

        End Try
    End Function
#End Region

#Region "Aceptar"
    Private Sub BtnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAceptar.Click

        Dim DS As New DataSet
        Dim rDocument As New ReportDocument
        Dim Dia, Mes As Integer

        UspReporteComisonesGenera()

        DS = UspReporteComisones()

        rDocument.Load(RutaReportes + "\ReporteComisionesPorGrupoVentas.rpt")
        rDocument.SetDataSource(DS)

        SetDBReport(DS, rDocument)
        Dim fechas As String = Nothing
        Mes = CMBMes.SelectedValue
        Select Case Mes
            Case 1, 3, 5, 7, 8, 10, 12
                Dia = 31
            Case 4, 6, 9, 11
                Dia = 30
            Case 2
                Dia = 28
        End Select

        If RBRangoPrimer.Checked = True Then
            fechas = "De la Fecha: 01 de " + LCase(CMBMes.Text) + " del " + CMBAnio.Text + " A la Fecha: 15 de " + LCase(CMBMes.Text) + " del " + CMBAnio.Text
        Else
            fechas = "De la Fecha: 16 de " + LCase(CMBMes.Text) + " del " + CMBAnio.Text + " A la Fecha: " + CStr(Dia) + " de " + LCase(CMBMes.Text) + " del " + CMBAnio.Text
        End If


        Dim mySelectFormula As String = Nothing
        mySelectFormula = "Comisiones por Grupo de Ventas"
        rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
        rDocument.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        rDocument.DataDefinition.FormulaFields("SubTitulo").Text = "'" & fechas & "'"
        'mySelectFormula = "Quejas " + Me.ComboBox1.Text
        'rDocument.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
        Dim Imprimir As New FrmImprimirCentralizada
        Imprimir.rd = rDocument
        Imprimir.ShowDialog()

        rDocument = Nothing

    End Sub
#End Region

#Region "Salir"
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
#End Region
End Class