﻿Public Class FrmCatalogoModelosT

    Private Sub FrmCatalogoModelosT_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'ColoreaFrm(Me)
        colorea(Me, Me.Name)

        llenarComboMarcas()

        If bndCatalogoTelmex = "M" Then
            Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Enabled = True
            BindingNavigatorDeleteItem.Enabled = True

            marcaCombo2.Enabled = True
            modeloTxt.Enabled = True
            precioTxt.Enabled = True

        ElseIf bndCatalogoTelmex = "C" Then
            Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Enabled = False
            BindingNavigatorDeleteItem.Enabled = False

            marcaCombo2.Enabled = False
            modeloTxt.Enabled = False
            precioTxt.Enabled = False

        ElseIf bndCatalogoTelmex = "N" Then
            Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Enabled = True
            BindingNavigatorDeleteItem.Enabled = False

            marcaCombo2.Enabled = True
            modeloTxt.Enabled = True
            precioTxt.Enabled = True

            IdModeloTxt.Text = ""
            modeloTxt.Text = ""
            precioTxt.Text = 0


        End If

        If bndCatalogoTelmex = "M" Or bndCatalogoTelmex = "C" Then

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IdModelo", SqlDbType.BigInt, IdModeloT)
            Dim dt As DataTable
            dt = BaseII.ConsultaDT("GetModelosByIdModelo")
            Dim dr As DataRow
            For Each dr In dt.Rows
                IdModeloTxt.Text = dr("IdModelo")
                modeloTxt.Text = dr("Modelo")
                marcaCombo2.SelectedValue = dr("IdMarca")
                'Marca
                precioTxt.Text = dr("Precio")
            Next

        End If

    End Sub

    Private Sub llenarComboMarcas()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdMarca", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, "")
        BaseII.CreateMyParameter("@Opcion", SqlDbType.Int, 0) 'Todas las marcas
        marcaCombo2.DataSource = BaseII.ConsultaDT("FiltroMarcas")

    End Sub

    Private Sub Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Click
        Dim yaExiste As Integer

        If modeloTxt.Text.Trim() = "" Then
            MessageBox.Show("Ingrese el Modelo")
            Exit Sub
        End If
        If precioTxt.Text.Trim() = "" Then
            MessageBox.Show("Ingrese el Precio")
            Exit Sub
        End If

        Dim idMarcaCombo As Long = marcaCombo2.SelectedValue()

        If bndCatalogoTelmex = "N" Then

            'Dim idMarcaCombo As Long = marcaCombo2.SelectedValue()

            If idMarcaCombo > 0 Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, modeloTxt.Text, 250)
                BaseII.CreateMyParameter("@IdMarca", SqlDbType.BigInt, idMarcaCombo)
                BaseII.CreateMyParameter("@Precio", SqlDbType.Money, precioTxt.Text)
                Dim dt As DataTable
                dt = BaseII.ConsultaDT("AddModelos")
                Dim dr As DataRow
                For Each dr In dt.Rows
                    ' idMarca = dr("IdMarca")
                    yaExiste = dr("Error")
                Next

                If yaExiste = True Then
                    MessageBox.Show("Ya existe este Modelo")
                Else
                    MessageBox.Show("Modelo agregado correctamente")
                End If
            Else
                MessageBox.Show("Seleccione una Marca")
                Exit Sub
            End If
        End If

        If bndCatalogoTelmex = "M" Then

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IdModelo", SqlDbType.BigInt, IdModeloTxt.Text)
            BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, modeloTxt.Text, 500)
            BaseII.CreateMyParameter("@IdMarca", SqlDbType.BigInt, idMarcaCombo)
            BaseII.CreateMyParameter("@Precio", SqlDbType.Money, precioTxt.Text)
            Dim dt As DataTable
            dt = BaseII.ConsultaDT("UpdateModelos")
            Dim dr As DataRow
            For Each dr In dt.Rows
                yaExiste = dr("Error")
            Next


            If yaExiste = 1 Then
                MessageBox.Show("No se ha actualizado")
            ElseIf yaExiste = 2 Then
                MessageBox.Show("No se ha actualizado")
            Else
                MessageBox.Show("Modelo actualizado correctamente")
            End If
        End If
        Me.Close()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    Private Sub FrmCatalogoMarcas_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        ' IdTxt.Text = ""
        modeloTxt.Text = ""

    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(sender As Object, e As EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim err As Integer = 0

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdModelo", SqlDbType.VarChar, Convert.ToInt64(IdModeloTxt.Text))
        Dim dt As DataTable
        dt = BaseII.ConsultaDT("DeleteModelos")
        Dim dr As DataRow
        For Each dr In dt.Rows
            err = dr("Error")
        Next

        If err = 0 Then
            MessageBox.Show("Modelo eliminado correctamente")
        Else
            MessageBox.Show("No se pudo eliminar el modelo porque está relacionado a un contrato")
        End If

        Me.Close()
    End Sub




    Private Sub precioTxt_KeyPress(sender As Object, e As KeyPressEventArgs) Handles precioTxt.KeyPress

        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
            If Asc(e.KeyChar) = 46 Then
                e.Handled = False   ' <<< Para que admita el punto.
            End If
        End If

    End Sub

End Class