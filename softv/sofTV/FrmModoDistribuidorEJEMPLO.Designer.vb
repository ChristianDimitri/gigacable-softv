﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmModoDistribuidorEJEMPLO
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CheckedListBox1 = New System.Windows.Forms.CheckedListBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'CheckedListBox1
        '
        Me.CheckedListBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.5!)
        Me.CheckedListBox1.FormattingEnabled = True
        Me.CheckedListBox1.Location = New System.Drawing.Point(19, 69)
        Me.CheckedListBox1.Name = "CheckedListBox1"
        Me.CheckedListBox1.Size = New System.Drawing.Size(514, 480)
        Me.CheckedListBox1.TabIndex = 107
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold)
        Me.Button1.Location = New System.Drawing.Point(257, 557)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(134, 45)
        Me.Button1.TabIndex = 108
        Me.Button1.Text = "&Guardar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.5!)
        Me.CheckBox1.Location = New System.Drawing.Point(22, 47)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(241, 20)
        Me.CheckBox1.TabIndex = 109
        Me.CheckBox1.Text = "Seleccionar todos los distribuidores"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(162, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(95, 18)
        Me.Label2.TabIndex = 111
        Me.Label2.Text = "Distribuidor"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.SystemColors.Desktop
        Me.Label1.Location = New System.Drawing.Point(19, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(122, 18)
        Me.Label1.TabIndex = 112
        Me.Label1.Text = "Modo Auditoria"
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(398, 557)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(135, 45)
        Me.Button3.TabIndex = 113
        Me.Button3.Text = "&CERRAR"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'FrmModoDistribuidor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(555, 607)
        Me.ControlBox = False
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.CheckedListBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmModoDistribuidor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Menu Principal"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    'Friend WithEvents Valida_servicioTvTableAdapter1 As Reportes.ProcedimientosArnoldo2TableAdapters.Valida_servicioTvTableAdapter
    'Friend WithEvents Valida_servicioTvTableAdapter2 As Reportes.ProcedimientosArnoldo2TableAdapters.Valida_servicioTvTableAdapter
    'Friend WithEvents Valida_servicioTvTableAdapter3 As Reportes.ProcedimientosArnoldo2TableAdapters.Valida_servicioTvTableAdapter
    'Friend WithEvents Valida_servicioTvTableAdapter4 As Reportes.ProcedimientosArnoldo2TableAdapters.Valida_servicioTvTableAdapter
    'Friend WithEvents Valida_servicioTvTableAdapter5 As Reportes.ProcedimientosArnoldo2TableAdapters.Valida_servicioTvTableAdapter
    'Friend WithEvents Valida_servicioTvTableAdapter6 As Reportes.ProcedimientosArnoldo2TableAdapters.Valida_servicioTvTableAdapter
    'Friend WithEvents Valida_servicioTvTableAdapter7 As Reportes.ProcedimientosArnoldo2TableAdapters.Valida_servicioTvTableAdapter
    'Friend WithEvents Valida_servicioTvTableAdapter8 As Reportes.ProcedimientosArnoldo2TableAdapters.Valida_servicioTvTableAdapter
    'Friend WithEvents Valida_servicioTvTableAdapter9 As Reportes.ProcedimientosArnoldo2TableAdapters.Valida_servicioTvTableAdapter
    'Friend WithEvents Valida_servicioTvTableAdapter10 As Reportes.ProcedimientosArnoldo2TableAdapters.Valida_servicioTvTableAdapter
    'Friend WithEvents Valida_servicioTvTableAdapter11 As Reportes.ProcedimientosArnoldo2TableAdapters.Valida_servicioTvTableAdapter
    'Friend WithEvents Valida_servicioTvTableAdapter12 As Reportes.ProcedimientosArnoldo2TableAdapters.Valida_servicioTvTableAdapter
    'Friend WithEvents Valida_servicioTvTableAdapter13 As Reportes.ProcedimientosArnoldo2TableAdapters.Valida_servicioTvTableAdapter
    'Friend WithEvents Valida_servicioTvTableAdapter14 As Reportes.ProcedimientosArnoldo2TableAdapters.Valida_servicioTvTableAdapter
    'Friend WithEvents Valida_servicioTvTableAdapter15 As Reportes.ProcedimientosArnoldo2TableAdapters.Valida_servicioTvTableAdapter
    'Friend WithEvents Valida_servicioTvTableAdapter16 As Reportes.ProcedimientosArnoldo2TableAdapters.Valida_servicioTvTableAdapter
    'Friend WithEvents Valida_servicioTvTableAdapter17 As Reportes.ProcedimientosArnoldo2TableAdapters.Valida_servicioTvTableAdapter
    'Friend WithEvents Valida_servicioTvTableAdapter18 As Reportes.ProcedimientosArnoldo2TableAdapters.Valida_servicioTvTableAdapter
    Friend WithEvents CheckedListBox1 As System.Windows.Forms.CheckedListBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
End Class
