﻿Public Class FrmTelmexMediosV1

    Dim indexBotonEditar As Integer = 4
    Dim indexCheckbox As Integer = 3

    Private Sub FrmTelmexMediosV1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ColoreaFrm(Me)
        'colorea(Me, Me.Name)
        llenarComboTipo()
        llenarGrid()

        BindingNavigatorDeleteItem.Visible = False
        Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Enabled = True


    End Sub

    Private Sub llenarGrid()


        '   If opcion = "N" Then
        ' gridMediosPrecios.ReadOnly = False
        'ElseIf opcion = "C" Or opcion = "M" Then

        '   gridMediosPrecios.ReadOnly = True  
        'End If


        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_Servicio", SqlDbType.BigInt, GloClv_Servicio)
        Dim dt As DataTable
        dt = BaseII.ConsultaDT("dameModoDistribuidor_PROCEJEMPLO")
        Dim dr As DataRow
        gridMediosPrecios.Rows.Clear()

        For Each dr In dt.Rows

            gridMediosPrecios.Rows.Add()
            gridMediosPrecios("colIdMedio", gridMediosPrecios.Rows.Count - 1).Value = dr("IdMedio").ToString()
            gridMediosPrecios("colNombre", gridMediosPrecios.Rows.Count - 1).Value = dr("Nombre").ToString()
            ' gridMediosPrecios("colChecked", gridMediosPrecios.Rows.Count - 1).Value = True
            gridMediosPrecios("colPrecio", gridMediosPrecios.Rows.Count - 1).Value = dr("Precio")
            gridMediosPrecios("colChecked", gridMediosPrecios.Rows.Count - 1).Value = dr("Checked")

            If dr("Checked") = False Then

                Dim cell As New DataGridViewTextBoxCell   'Replace the ButtonCell for a TextCell'
                cell.Value = " "                 'Set the value again'
                gridMediosPrecios("colPrecioGuarda", gridMediosPrecios.Rows.Count - 1) = cell

            Else

                Dim cellBtn As New DataGridViewButtonCell   'Replace the TextCell for a ButtonCell'
                cellBtn.UseColumnTextForButtonValue = True
                'Set the value again'
                cellBtn.Value = "Guarda precio"
                gridMediosPrecios("colPrecioGuarda", gridMediosPrecios.Rows.Count - 1) = cellBtn

            End If

        Next


        'gridMediosPrecios.Rows.Add(1, "Nombre", True, 50)
        'gridMediosPrecios.Rows.Add(2, "Nombre", False, 60)
        'gridMediosPrecios.Rows.Add(3, "Nombre", True, 70)


        If opcion = "N" Or opcion = "M" Then
            gridMediosPrecios.ReadOnly = False

        ElseIf opcion = "C" Then
            gridMediosPrecios.ReadOnly = True
        End If




        'DataGridView1.ColumnCount = 3
        'DataGridView1.Columns(0).Name = "Product ID"
        'DataGridView1.Columns(1).Name = "Product Name"
        'DataGridView1.Columns(2).Name = "Product_Price"

        'Dim row As String() = New String() {"1", "Product 1", "1000"}
        'DataGridView1.Rows.Add(row)
        'row = New String() {"2", "Product 2", "2000"}
        'DataGridView1.Rows.Add(row)
        'row = New String() {"3", "Product 3", "3000"}
        'DataGridView1.Rows.Add(row)
        'row = New String() {"4", "Product 4", "4000"}
        'DataGridView1.Rows.Add(row)

        'Dim chk As New DataGridViewCheckBoxColumn()
        'DataGridView1.Columns.Add(chk)
        'chk.HeaderText = "Check Data"
        'chk.Name = "chk"
        'DataGridView1.Rows(2).Cells(3).Value = True


    End Sub









    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub



    Private Sub llenarComboTipo()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, 0) 'Todas las marcas
        tipoCombo.DataSource = BaseII.ConsultaDT("MUESTRA_TIPOCLIENTES")

    End Sub




    Private Sub Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Click

        'refrescamos grid


        'Revisamos si es un servicio nuevo.

        If opcion = "N" Then

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, GloClv_Servicio)
            BaseII.CreateMyParameter("@idmedio", SqlDbType.Int, 1)
            BaseII.CreateMyParameter("@precio", SqlDbType.Int, 30)
            BaseII.CreateMyParameter("@TipoSucriptor", SqlDbType.Int, 1)
            BaseII.CreateMyParameter("@checked", SqlDbType.Bit, 1)
            BaseII.CreateMyParameter("@ClvSessionTempo", ParameterDirection.Output, SqlDbType.BigInt)
            BaseII.ProcedimientoOutPut("Add_RelServicioMedio_Telmex_Temporal1")
            ClvSessionTelmexTempo = BaseII.dicoPar("@ClvSessionTempo").ToString
            MessageBox.Show("Cambios guardados con ClvSessionTempo " + ClvSessionTelmexTempo.ToString())

        ElseIf opcion = "C" Or opcion = "M" Then


            For Each row As DataGridViewRow In gridMediosPrecios.Rows

                Dim obj(row.Cells.Count - 1) As Object

                'For i = 0 To row.Cells.Count - 1
                '   ' obj(i) = row.Cells(i).Value
                '    obj(i) = row.Cells(2).FormattedValue
                'Next

                Dim idMedio = row.Cells("colIdMedio").Value
                Dim descripcionMedio = row.Cells("colNombre").Value
                Dim precio = row.Cells("colPrecio").Value

                Dim checked As Boolean
                checked = row.Cells("colChecked").Value

                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, GloClv_Servicio)
                BaseII.CreateMyParameter("@idmedio", SqlDbType.Int, idMedio)
                BaseII.CreateMyParameter("@precio", SqlDbType.Money, precio)
                BaseII.CreateMyParameter("@TipoSucriptor", SqlDbType.Int, 1) 'FALTA DEFINIR
                BaseII.CreateMyParameter("@checked", SqlDbType.Bit, checked)
                BaseII.Inserta("UPDATE_RelClvServicioMedioTelmex")  'guardaModoDistribuidor_PROCDEMUESTRA

            Next


            For i As Integer = 0 To gridMediosPrecios.Rows.Count() - 1

                Dim c As Boolean
                c = gridMediosPrecios.Rows(i).Cells(3).Value
                ' if the checkbox cell is checked

                MessageBox.Show(c)

            Next



            MessageBox.Show("Cambios guardados")
            llenarGrid()



        End If



        'llenar_RelMedioServ()


        'Dim yaExiste As Integer

        'If modeloTxt.Text.Trim() = "" Then
        '    MessageBox.Show("Ingrese el Modelo")
        '    Exit Sub
        'End If
        'If precioTxt.Text.Trim() = "" Then
        '    MessageBox.Show("Ingrese el Precio")
        '    Exit Sub
        'End If

        'Dim idMarcaCombo As Long = tipoCombo.SelectedValue()

        'If bndCatalogoTelmex = "N" Then
        '    If idMarcaCombo > 0 Then
        '        BaseII.limpiaParametros()
        '        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.BigInt, 1)
        '        BaseII.CreateMyParameter("@idmedio", SqlDbType.BigInt, 2)
        '        BaseII.CreateMyParameter("@precio", SqlDbType.Money, 80)
        '        BaseII.CreateMyParameter("@Select", SqlDbType.Bit, True)
        '        BaseII.CreateMyParameter("@TipoSucriptor", SqlDbType.BigInt, 1)
        '        Dim dt As DataTable
        '        dt = BaseII.ConsultaDT("AddRelClvServicioMedioTelmex")
        '        Dim dr As DataRow
        '        For Each dr In dt.Rows
        '            yaExiste = dr("Error")
        '        Next

        '        If yaExiste = True Then
        '            MessageBox.Show("Ya existe este Modelo")
        '        Else
        '            MessageBox.Show("Modelo agregado correctamente")
        '        End If
        '    Else
        '        MessageBox.Show("Seleccione una Marca")
        '        Exit Sub
        '    End If
        'End If

        'If bndCatalogoTelmex = "M" Then

        '    BaseII.limpiaParametros()
        '    BaseII.CreateMyParameter("@IdModelo", SqlDbType.BigInt, IdModeloTxt.Text)
        '    BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, modeloTxt.Text, 500)
        '    BaseII.CreateMyParameter("@IdMarca", SqlDbType.BigInt, idMarcaCombo)
        '    BaseII.CreateMyParameter("@Precio", SqlDbType.Money, precioTxt.Text)
        '    Dim dt As DataTable
        '    dt = BaseII.ConsultaDT("UpdateModelos")
        '    Dim dr As DataRow
        '    For Each dr In dt.Rows
        '        yaExiste = dr("Error")
        '    Next


        '    If yaExiste = 1 Then
        '        MessageBox.Show("No se ha actualizado")
        '    ElseIf yaExiste = 2 Then
        '        MessageBox.Show("No se ha actualizado")
        '    Else
        '        MessageBox.Show("Modelo actualizado correctamente")
        '    End If
        'End If
        Me.Close()
    End Sub




    'Private Sub gridMediosPrecios_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles gridMediosPrecios.CellContentClick

    '    MessageBox.Show("SE HA PRESIONADO")



    'End Sub


    Private Sub gridMediosPrecios_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles gridMediosPrecios.CellValueChanged

    End Sub




    'Private Sub gridMediosPrecios_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) 



    'End Sub



    Private Sub gridMediosPrecios_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles gridMediosPrecios.CellContentClick

        If e.ColumnIndex = 2 Then
            MsgBox("yes" + gridMediosPrecios.Item(e.ColumnIndex, e.RowIndex).Value.ToString())


            Dim checkboxValue As Boolean
            checkboxValue = gridMediosPrecios.Rows(e.RowIndex).Cells(e.ColumnIndex).Value

            If checkboxValue = True Then
                gridMediosPrecios.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = False
                'tambien bloqueamos el botón de editar
                '  gridMediosPrecios.Rows(e.RowIndex).Cells(indexBotonEditar).ReadOnly = True

                'convierte en celda de texto
                Dim cell As New DataGridViewTextBoxCell   'Replace the ButtonCell for a TextCell'
                cell.Value = ""                 'Set the value again'
                gridMediosPrecios.Rows(e.RowIndex).Cells(4) = cell              'Override the cell'

            Else
                gridMediosPrecios.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = True
                ' gridMediosPrecios.Rows(e.RowIndex).Cells(indexBotonEditar).ReadOnly = False

                Dim cellBtn As New DataGridViewButtonCell   'Replace the TextCell for a ButtonCell'
                cellBtn.UseColumnTextForButtonValue = True
                'Set the value again'
                cellBtn.Value = "Guarda precio"
                gridMediosPrecios.Rows(e.RowIndex).Cells(4) = cellBtn              'Override the cell'

            End If

        End If




        If e.ColumnIndex = 4 Then   'si presiona el botón y el checkbox = true 
            If gridMediosPrecios.Rows(e.RowIndex).Cells(indexCheckbox).Value = True Then
                MsgBox("El precio será actualizado")
                '        MsgBox("MODIFICAR PRECIO")
                '        gridMediosPrecios.Item(3, e.RowIndex).Value = 100.0
            Else
                MsgBox("Para actualizar el precio primero agregue el medio")
            End If


        End If


    End Sub




End Class