Imports System.Data.SqlClient
Public Class FrmSelFechas

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        GloBndSelFecha = False
        locbndrepcancelaciones = False
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If LocOp = 3 Then
            If locbndrepcancelaciones = True Then

                If lbDer.Items.Count = 0 Then
                    MessageBox.Show("Selecciona un motivo de cancelación.")
                    Exit Sub
                End If

                locbndrepcancelaciones = False


                If LocValidaHab = 0 Then
                    GloFecha_Ini = Me.DateTimePicker1.Text
                    GloFecha_Fin = Me.DateTimePicker2.Text
                    FrmSelPeriodo.Show()
                Else
                    GloFecha_Ini = Me.DateTimePicker1.Text
                    GloFecha_Fin = Me.DateTimePicker2.Text
                    bndfechareport = True
                End If

                'locbndrepcancelaciones = False
                'If Me.ComboBox1.Text = "" Then
                '    MsgBox("Seleccione Un Motivo De Cancelacion", MsgBoxStyle.Information)
                '    Exit Sub
                'ElseIf Me.ComboBox1.Text <> "" Then
                '    Dim con As New SqlConnection(MiConexion)
                '    Dim cmd As New SqlClient.SqlCommand()
                '    cmd = New SqlClient.SqlCommand()
                '    con.Open()
                '    With cmd
                '        .Connection = con
                '        .CommandText = "Inserta_Rel_Motcan_Rep_ciudad"
                '        .CommandType = CommandType.StoredProcedure
                '        .CommandTimeout = 0

                '        '(@clv_session bigint,@clv_motcan int)
                '        Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                '        Dim prm1 As New SqlParameter("@clv_motcan", SqlDbType.Int)

                '        prm.Direction = ParameterDirection.Input
                '        prm1.Direction = ParameterDirection.Input

                '        prm.Value = LocClv_session
                '        prm1.Value = CLng(Me.ComboBox1.SelectedValue)

                '        .Parameters.Add(prm)
                '        .Parameters.Add(prm1)

                '        Dim i As Integer = cmd.ExecuteNonQuery()

                '    End With
                '    con.Close()

                '    If LocValidaHab = 0 Then
                '        GloFecha_Ini = Me.DateTimePicker1.Text
                '        GloFecha_Fin = Me.DateTimePicker2.Text
                '        FrmSelPeriodo.Show()
                '    Else
                '        GloFecha_Ini = Me.DateTimePicker1.Text
                '        GloFecha_Fin = Me.DateTimePicker2.Text
                '        bndfechareport = True
                '    End If
                'End If

            End If
        If LocValidaHab = 0 Then
            GloFecha_Ini = Me.DateTimePicker1.Text
            GloFecha_Fin = Me.DateTimePicker2.Text
            FrmSelPeriodo.Show()
        Else
            GloFecha_Ini = Me.DateTimePicker1.Text
            GloFecha_Fin = Me.DateTimePicker2.Text
            If bnd_Canc_Sin_Mens = False Then
                bndfechareport = True
            ElseIf bnd_Canc_Sin_Mens = True Then
                bnd_Canc_Sin_Mens = False
                bnd_Canc_Sin_Mens_buena = True
            End If
        End If
        Else
        GloBndSelFecha = True
        GloFecha_Ini = Me.DateTimePicker1.Text
        GloFecha_Fin = Me.DateTimePicker2.Text
        End If
        If LocOp = 10 Then
            If LocValidaHab = 0 Then
                GloFecha_Ini = Me.DateTimePicker1.Text
                GloFecha_Fin = Me.DateTimePicker2.Text
                FrmSelEstado.Show()
            Else
                GloFecha_Ini = Me.DateTimePicker1.Text
                GloFecha_Fin = Me.DateTimePicker2.Text
                bndfechareport = True
            End If
        Else

            GloBndSelFecha = True
            GloFecha_Ini = Me.DateTimePicker1.Text
            GloFecha_Fin = Me.DateTimePicker2.Text
        End If
        If LocOp = 21 Then
            If LocValidaHab = 0 Then
                GloFecha_Ini = Me.DateTimePicker1.Text
                GloFecha_Fin = Me.DateTimePicker2.Text
                FrmSelPeriodo.Show()
            Else
                GloFecha_Ini = Me.DateTimePicker1.Text
                GloFecha_Fin = Me.DateTimePicker2.Text
                bndfechareport2 = True
            End If
        Else
            GloBndSelFecha = True
            GloFecha_Ini = Me.DateTimePicker1.Text
            GloFecha_Fin = Me.DateTimePicker2.Text
        End If

        If LocOp = 23 Then
            eFechaIni = Me.DateTimePicker1.Text
            eFechaFin = Me.DateTimePicker2.Text
            eBndReportePPE = True
        End If
        If LocOp = 35 Then
            eFechaIni = Me.DateTimePicker1.Text
            eFechaFin = Me.DateTimePicker2.Text
            Locbndrepcontspago = True
        End If

        If LocbndPolizaCiudad = True Then
            eFechaIni = Me.DateTimePicker1.Text
            eFechaFin = Me.DateTimePicker2.Text
            LocbndPolizaCiudad2 = True
            FrmPoliza.Show()
        End If
        If LocOp = 1 Then
            GloFecha_Ini = Me.DateTimePicker1.Text
            GloFecha_Fin = Me.DateTimePicker2.Text
            If LocValidaHab = 0 Then
                FrmSelPeriodo.Show()
            Else
                bndReportC = True
            End If
        End If
        Me.Close()
    End Sub



    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged

    End Sub

    Private Sub FrmSelFechas_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        locbndrepcancelaciones = False
    End Sub

    Private Sub FrmSelFechas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        If LocOp = 35 Then
            Me.GroupBox1.Visible = False
        End If
        If locbndrepcancelaciones = True Then
            Me.GroupBox1.Visible = True
            'Me.Size = New Size(240, Me.Size.Height)
            'Me.Panel2.Location.Y


            'CON.Open()
            'Me.Muestra_MotCanc_ReporteTableAdapter.Connection = CON
            'Me.Muestra_MotCanc_ReporteTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_MotCanc_Reporte)
            'CON.Close()

            CONReporteCancelacionesMotcan(0, LocClv_session)

        ElseIf locbndrepcancelaciones = False Then
            Me.GroupBox1.Visible = False
            'Me.Size = New Size(323, Me.Size.Height)

        End If
        colorea(Me, Me.Name)

        'Dim CON As New SqlConnection(MiConexion)
        'If LocOp = 35 Then
        '    Me.GroupBox1.Visible = False
        'End If

        'If locbndrepcancelaciones = True Then

        '    Me.GroupBox1.Visible = True
        '    Me.Size = New Size(240, Me.Size.Height)
        '    Me.Panel2.Location.Y()


        '    CON.Open()
        '    Me.Muestra_MotCanc_ReporteTableAdapter.Connection = CON
        '    Me.Muestra_MotCanc_ReporteTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_MotCanc_Reporte)
        '    CON.Close()
        'ElseIf locbndrepcancelaciones = False Then
        '    Me.GroupBox1.Visible = False
        '    Me.Size = New Size(323, Me.Size.Height)
        'End If



        ''Me.GroupBox1.Visible = False


        'colorea(Me, Me.Name)
    End Sub

    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

    End Sub

    Private Sub CONReporteCancelacionesMotcan(ByVal OP As Integer, ByVal CLV_SESSION As Integer)
        Dim dTable As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, OP)
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, CLV_SESSION)
        dTable = BaseII.ConsultaDT("CONReporteCancelacionesMotcan")
        If OP = 0 Then lbIzq.DataSource = dTable
        If OP = 1 Then lbDer.DataSource = dTable
    End Sub

    Private Sub NUEReporteCancelacionesMotcan(ByVal OP As Integer, ByVal CLV_SESSION As Integer, ByVal CLV_MOTCAN As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, OP)
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, CLV_SESSION)
        BaseII.CreateMyParameter("@CLV_MOTCAN", SqlDbType.Int, CLV_MOTCAN)
        BaseII.Inserta("NUEReporteCancelacionesMotcan")
    End Sub

    Private Sub BORReporteCancelacionesMotcan(ByVal OP As Integer, ByVal CLV_SESSION As Integer, ByVal CLV_MOTCAN As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, OP)
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, CLV_SESSION)
        BaseII.CreateMyParameter("@CLV_MOTCAN", SqlDbType.Int, CLV_MOTCAN)
        BaseII.Inserta("BORReporteCancelacionesMotcan")
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If lbIzq.Items.Count = 0 Then Exit Sub
        NUEReporteCancelacionesMotcan(0, LocClv_session, lbIzq.SelectedValue.ToString)
        CONReporteCancelacionesMotcan(0, LocClv_session)
        CONReporteCancelacionesMotcan(1, LocClv_session)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If lbIzq.Items.Count = 0 Then Exit Sub
        NUEReporteCancelacionesMotcan(1, LocClv_session, 0)
        CONReporteCancelacionesMotcan(0, LocClv_session)
        CONReporteCancelacionesMotcan(1, LocClv_session)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If lbDer.Items.Count = 0 Then Exit Sub
        BORReporteCancelacionesMotcan(0, LocClv_session, lbDer.SelectedValue.ToString)
        CONReporteCancelacionesMotcan(0, LocClv_session)
        CONReporteCancelacionesMotcan(1, LocClv_session)
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If lbDer.Items.Count = 0 Then Exit Sub
        BORReporteCancelacionesMotcan(1, LocClv_session, 0)
        CONReporteCancelacionesMotcan(0, LocClv_session)
        CONReporteCancelacionesMotcan(1, LocClv_session)
    End Sub
End Class