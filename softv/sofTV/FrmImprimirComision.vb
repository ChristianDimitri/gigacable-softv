Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic

Public Class FrmImprimirComision

    Private customersByCityReport As ReportDocument
    Private op As String = Nothing
    Private Titulo As String = Nothing
    Delegate Sub Reporte()
    Delegate Sub Forms(ByVal Mycontrol As FrmImprimirComision, ByVal Text As String)

    Private Sub DelegadoForm(ByVal MyControl As FrmImprimirComision, ByVal text As String)
        MyControl.Text = text
    End Sub


    Private Sub ConfigureCrystalReports()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim reportPath As String = Nothing
            eTituloComision = ""


            If eOpVentas = 1 Then

                Titulo = "Reporte de Comisiones de " & eGrupo
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Comisiones de " & eGrupo
                End If
                reportPath = RutaReportes + "\ReportComisiones.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(1, eFechaFin)
                '@Op
                customersByCityReport.SetParameterValue(2, eOp)
                '@Clv_Session
                customersByCityReport.SetParameterValue(3, eClv_Session)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 2 Then
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de N�mero de Ventas"
                End If


                reportPath = RutaReportes + "\ReportNumeroVentas.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(1, eFechaFin)
                '@Op
                customersByCityReport.SetParameterValue(2, eOp)
                '@Clv_Session
                customersByCityReport.SetParameterValue(3, eClv_Session)
                '@ServicioPadre
                customersByCityReport.SetParameterValue(4, eTipSer)

                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Servicio").Text = "'" & eServicio & "'"



            ElseIf eOpVentas = 3 Then
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Status de Ventas"
                End If



                reportPath = RutaReportes + "\ReportStatusVentas.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(1, eFechaFin)
                '@Op
                customersByCityReport.SetParameterValue(2, eOp)
                '@ServicioPadre
                customersByCityReport.SetParameterValue(3, eTipSer)
                '@Contratado
                customersByCityReport.SetParameterValue(4, eCont)
                '@Instalado
                customersByCityReport.SetParameterValue(5, eInst)
                '@Desconectado
                customersByCityReport.SetParameterValue(6, eDesc)
                '@Suspendido
                customersByCityReport.SetParameterValue(7, eSusp)
                '@Baja
                customersByCityReport.SetParameterValue(8, eBaja)
                '@Fuera de Area
                customersByCityReport.SetParameterValue(9, eFuera)
                '@Clv_SessionVendedores
                customersByCityReport.SetParameterValue(10, eClv_Session)

                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Servicio").Text = "'" & eServicio & "'"
                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eTituloComision & "'"


            ElseIf eOpVentas = 4 Then
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Folios Faltantes"
                End If



                reportPath = RutaReportes + "\ReportFolios.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)
                '@FechaInicial
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@FechaFinal
                customersByCityReport.SetParameterValue(1, eFechaFin)
                '@Clv_Session
                customersByCityReport.SetParameterValue(2, eClv_Session)

                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eTituloComision & "'"




            ElseIf eOpVentas = 11 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica - Consolidado de Ventas"
                End If

                If eServicio <> "Todos" Then
                    eServicio = "Gr�fica Consolidado de Ventas de Servicio " & eServicio
                Else
                    eServicio = "Gr�fica Consolidado de Ventas"
                End If
                reportPath = RutaReportes + "\ReporteGrafica_1.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(1, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(2, eFechaFin)

                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eServicio & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 12 Then


                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica - De Sucursales"
                End If


                If eServicio <> "Todos" Then
                    eServicio = "Gr�fica De Sucursales de Servicio  " & eServicio
                Else
                    eServicio = "Gr�fica De Sucursales de Varios Tipos de Servicio"
                End If
                reportPath = RutaReportes + "\ReporteGrafica_2.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(2, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(3, eFechaFin)

                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eServicio & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 13 Then


                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica - De Sucursales"
                End If

                If eServicio <> "Todos" Then
                    eServicio = "Gr�fica De Sucursales Por Servicios de " & eServicio
                Else
                    eServicio = "Gr�fica De Sucursales Por Varios Servicios"
                End If
                reportPath = RutaReportes + "\ReporteGrafica_3.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(2, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(3, eFechaFin)

                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eServicio & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 14 Then


                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica - Del Depto. de Ventas"
                End If

                If eServicio <> "Todos" Then
                    eServicio = "Gr�fica Del Depto. de Ventas Por Vendedores y por Servicio de " & eServicio
                Else
                    If eBndOpVentas = True Then
                        eBndOpVentas = False
                        eServicio = "Gr�fica Del Depto. de Ventas Por Vendedores y por Varios Tipos de Servicio"
                    Else
                        eServicio = "Gr�fica Del Depto. de Ventas Por Vendedores y por Varios Servicios"
                    End If
                End If
                reportPath = RutaReportes + "\ReporteGrafica_4.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(2, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(3, eFechaFin)

                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eServicio & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            ElseIf eOpVentas = 15 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica - Del Depto. de Ventas"
                End If

                If eServicio <> "Todos" Then
                    eServicio = "Gr�fica Del Depto. de Ventas de Servicios de " & eServicio
                Else
                    eServicio = "Gr�fica Del Depto. de Ventas de Varios Servicios"
                End If
                reportPath = RutaReportes + "\ReporteGrafica_5.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(2, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(3, eFechaFin)

                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eServicio & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 16 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica - De Sucursales"
                End If

                If eServicio <> "Todos" Then
                    eServicio = "Gr�fica De Sucursales con Servicios de" & eServicio
                Else
                    eServicio = "Gr�fica De Sucursales con Varios Servicios"
                End If
                reportPath = RutaReportes + "\ReporteGrafica_6.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(2, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(3, eFechaFin)

                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eServicio & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 20 Then

                Titulo = "Detalle de Vendedores del Servicio " & eServicio

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte - " & Titulo
                End If
                reportPath = RutaReportes + "\ReportMetasVen.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(2, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(3, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(4, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(5, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 21 Then
                Titulo = "Detalle de Sucursales del Servicio " & eServicio

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte - " & Titulo
                End If

                reportPath = RutaReportes + "\ReportMetasSuc.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(2, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(3, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(4, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(5, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 22 Then
                Titulo = "Gr�fica de Medidores de Vendedores del Servicio" & eServicio

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Medidores de Vendedores"
                End If

                reportPath = RutaReportes + "\ReportMetasVenGraf.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(2, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(3, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(4, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(5, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 23 Then
                Titulo = "Gr�fica de Medidores de Sucursales del Servicio" & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Medidores de Sucursales"
                End If

                reportPath = RutaReportes + "\ReportMetasSucGraf.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(2, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(3, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(4, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(5, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 30 Then
                Titulo = "Medidores de Ingresos"

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Medidores de Ingresos"
                End If


                reportPath = RutaReportes + "\ReportMetasIng.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(2, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(3, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(4, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(5, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 31 Then
                Titulo = "Gr�fica Medidores de Ingresos del Servicio de " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica Medidores de Ingresos"
                End If

                reportPath = RutaReportes + "\ReportMetasIngGraf.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(2, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(3, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(4, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(5, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 32 Then
                Titulo = "Reporte de Ingresos Por Punto de Cobro"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Ingresos Por Punto de Cobro"
                End If

                reportPath = RutaReportes + "\ReportMetasIngSuc.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(2, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(3, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(4, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(5, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 33 Then
                Titulo = "Gr�fica de Ingresos Por Punto de Cobro del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Ingresos Por Punto de Cobro"
                End If

                reportPath = RutaReportes + "\ReportMetasIngSucGraf.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(2, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(3, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(4, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(5, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 34 Then
                Titulo = "Reporte Medidores de Cartera"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte Medidores de Cartera"
                End If

                reportPath = RutaReportes + "\ReportMetasCartera.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)


                '@MesIni
                customersByCityReport.SetParameterValue(0, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(1, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(2, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(3, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 40 Then

                Dim dtable As New DataTable

                dtable = ReporteContratoF(eTipSer, eOpContratoF)

                reportPath = RutaReportes + "\ReportContratoF.rpt"

                customersByCityReport.Load(reportPath)
                customersByCityReport.SetDataSource(dtable)

                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

                'Titulo = "Clientes con Contrato a Plazo Forzoso de " & eServicio
                'If Me.InvokeRequired Then
                '    Dim d As New Forms(AddressOf DelegadoForm)
                '    Me.Invoke(d, New Object() {Me, ""})
                'Else
                '    Me.Text = Titulo
                'End If

                ''reportPath = RutaReportes + "\ReportContratoF.rpt"
                'reportPath = RutaReportes + "\ReportPlazoForzoso.rpt"
                'customersByCityReport.Load(reportPath)
                'SetDBLogonForReport(connectionInfo, customersByCityReport)

                ''@Clv_TipSer
                'customersByCityReport.SetParameterValue(0, eTipSer)
                ''@Orden
                ''customersByCityReport.SetParameterValue(1, eOpContratoF)
                '''@MesIni
                ''customersByCityReport.SetParameterValue(2, eMesIni)
                '''@AnioIni
                ''customersByCityReport.SetParameterValue(3, eAnioIni)
                '''@MesFin
                ''customersByCityReport.SetParameterValue(4, eMesFin)
                '''@AnioFin
                ''customersByCityReport.SetParameterValue(5, eAnioFin)

                ''eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                'customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                'customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                ''customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                'customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 41 Then
                Titulo = "Detalle de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Medidor de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportMetasGrupo1.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Grupo
                customersByCityReport.SetParameterValue(0, eClv_Grupo)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(2, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(3, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(4, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(5, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(6, eAnioFin)
                '@TipoGrupo
                customersByCityReport.SetParameterValue(7, "")

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 42 Then

                Titulo = "Detalle de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Medidor de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportMetasGrupo2.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Grupo
                customersByCityReport.SetParameterValue(0, eClv_Grupo)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(2, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(3, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(4, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(5, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(6, eAnioFin)
                '@TipoGrupo
                customersByCityReport.SetParameterValue(7, "")

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 43 Then

                Titulo = "Detalle de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Medidor de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportMetasGrupoN.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Grupo
                customersByCityReport.SetParameterValue(0, eClv_Grupo)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(2, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(3, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(4, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(5, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(6, eAnioFin)
                '@TipoGrupo
                customersByCityReport.SetParameterValue(7, "")

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 44 Then

                Titulo = "Gr�fica de Medidores de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Medidores"
                End If

                reportPath = RutaReportes + "\ReportMetasGrupo1Graf.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Grupo
                customersByCityReport.SetParameterValue(0, eClv_Grupo)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(2, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(3, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(4, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(5, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(6, eAnioFin)
                '@TipoGrupo
                customersByCityReport.SetParameterValue(7, "")

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 45 Then

                Titulo = "Gr�fica de Medidores de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Medidores"
                End If

                reportPath = RutaReportes + "\ReportMetasGrupo2Graf.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Grupo
                customersByCityReport.SetParameterValue(0, eClv_Grupo)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(2, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(3, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(4, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(5, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(6, eAnioFin)
                '@TipoGrupo
                customersByCityReport.SetParameterValue(7, "")

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 46 Then

                Titulo = "Gr�fica de Medidores de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Medidores"
                End If

                reportPath = RutaReportes + "\ReportMetasGrupoNGraf.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Grupo
                customersByCityReport.SetParameterValue(0, eClv_Grupo)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(2, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(3, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(4, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(5, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(6, eAnioFin)
                '@TipoGrupo
                customersByCityReport.SetParameterValue(7, "")

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 47 Then

                Titulo = "Detallado de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Detallado de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportMetasGrupo1Det.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Grupo
                customersByCityReport.SetParameterValue(0, eClv_Grupo)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(2, eClv_Session)
                '@TipoGrupo
                customersByCityReport.SetParameterValue(3, "")
                '@FechaIni
                customersByCityReport.SetParameterValue(4, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(5, eFechaFin)


                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 48 Then

                Titulo = "Detallado de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Detallado de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportMetasGrupoNDet.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Grupo
                customersByCityReport.SetParameterValue(0, eClv_Grupo)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(2, eClv_Session)
                '@TipoGrupo
                customersByCityReport.SetParameterValue(3, "")
                '@FechaIni
                customersByCityReport.SetParameterValue(4, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(5, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 49 Then
                Titulo = "Reporte de Comisiones de " & eGrupo
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Comisiones de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportComisionesN.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Grupo
                customersByCityReport.SetParameterValue(0, eClv_Grupo)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@FechaIni
                customersByCityReport.SetParameterValue(2, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(3, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 50 Then
                Titulo = "Gr�fica de Atenci�n Telef�nica por Servicios al Cliente de " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Atenci�n Telef�nica"
                End If

                reportPath = RutaReportes + "\ReportAtenTelGraf50.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@FechaIni
                customersByCityReport.SetParameterValue(2, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(3, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 51 Then
                Titulo = "Gr�fica de Atenci�n Telef�nica por Usuarios y Servicios al Cliente de " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Atenci�n Telef�nica"
                End If

                reportPath = RutaReportes + "\ReportAtenTelGraf51.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@FechaIni
                customersByCityReport.SetParameterValue(2, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(3, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 52 Then
                Titulo = "Gr�fica de Llamadas de Atenci�n Telef�nica del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Atenci�n Telef�nica"
                End If

                reportPath = RutaReportes + "\ReportAtenTelGraf52.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@FechaIni
                customersByCityReport.SetParameterValue(2, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(3, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 53 Then
                Titulo = "Reporte de Status de Ventas de " & eGrupo
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Status de Ventas"
                End If


                reportPath = RutaReportes + "\ReportStatusVentasN.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)
                'Clv_Grupo
                customersByCityReport.SetParameterValue(0, eClv_Grupo)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(1, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(2, eFechaFin)
                '@ServicioPadre
                customersByCityReport.SetParameterValue(3, eTipSer)
                '@Contratado
                customersByCityReport.SetParameterValue(4, eCont)
                '@Instalado
                customersByCityReport.SetParameterValue(5, eInst)
                '@Desconectado
                customersByCityReport.SetParameterValue(6, eDesc)
                '@Suspendido
                customersByCityReport.SetParameterValue(7, eSusp)
                '@Baja
                customersByCityReport.SetParameterValue(8, eBaja)
                '@Fuera de Area
                customersByCityReport.SetParameterValue(9, eFuera)
                '@Temporal
                customersByCityReport.SetParameterValue(10, eTempo)
                '@Clv_SessionVendedores
                customersByCityReport.SetParameterValue(11, eClv_Session)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 54 Then
                Titulo = "Reporte de PPV"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de PPV"
                End If


                reportPath = RutaReportes + "\ReportPPV.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)
                'Contrato
                customersByCityReport.SetParameterValue(0, eContrato)
                '@FechaIni
                customersByCityReport.SetParameterValue(1, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(2, eFechaFin)
                '@Res
                customersByCityReport.SetParameterValue(3, 0)
                '@OP1
                customersByCityReport.SetParameterValue(4, eOP1)
                '@OP2
                customersByCityReport.SetParameterValue(5, eOP2)
                '@OP3
                customersByCityReport.SetParameterValue(6, eOP3)


                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 55 Then
                Titulo = "Reporte de Servicios Contratados"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Servicios Contratados"
                End If


                reportPath = RutaReportes + "\ReportVentas.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@FechaIni
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(1, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 56 Then
                Titulo = "Reporte de Ventas en Baja de todos los Grupos de Ventas"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Ventas en Baja"
                End If


                reportPath = RutaReportes + "\ReportVentasBaja.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@FechaIni
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(1, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 57 Then
                Titulo = "Bit�cora de Correo(s)"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Bit�cora de Correo(s)"
                End If


                reportPath = RutaReportes + "\ReportCorreo.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clave
                customersByCityReport.SetParameterValue(0, eClaveCorreo)
                '@FechaIni
                customersByCityReport.SetParameterValue(1, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(2, eFechaFin)
                '@Op
                customersByCityReport.SetParameterValue(3, eOpCorreo)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 58 Then
                Titulo = "Reporte de Comisiones de " & eGrupo
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Comisiones de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportComisiones0.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Session
                customersByCityReport.SetParameterValue(0, eClv_Session)
                '@FechaIni
                customersByCityReport.SetParameterValue(1, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(2, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 59 Then
                Titulo = "Detallado de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Detallado de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportMetasGrupo0Det.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)


                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@FechaIni
                customersByCityReport.SetParameterValue(2, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(3, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 60 Then
                Titulo = "Reporte de Status de Ventas de " & eGrupo

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Status de Ventas"
                End If


                reportPath = RutaReportes + "\ReportStatusVentas0.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Fecha_Ini
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(1, eFechaFin)
                '@ServicioPadre
                customersByCityReport.SetParameterValue(2, eTipSer)
                '@Contratado
                customersByCityReport.SetParameterValue(3, eCont)
                '@Instalado
                customersByCityReport.SetParameterValue(4, eInst)
                '@Desconectado
                customersByCityReport.SetParameterValue(5, eDesc)
                '@Suspendido
                customersByCityReport.SetParameterValue(6, eSusp)
                '@Baja
                customersByCityReport.SetParameterValue(7, eBaja)
                '@Fuera de Area
                customersByCityReport.SetParameterValue(8, eFuera)
                '@Temporal
                customersByCityReport.SetParameterValue(9, eTempo)
                '@Clv_SessionVendedores
                customersByCityReport.SetParameterValue(10, eClv_Session)


                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 61 Then
                Titulo = "Reporte Ventas Totales de " & eServicio

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte Ventas Totales"
                End If


                reportPath = RutaReportes + "\ReportVentasTotales.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@ServicioPadre
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(1, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(2, eFechaFin)


                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 62 Then
                Titulo = "Reporte de Interfaz " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Interfaz"
                End If


                reportPath = RutaReportes + "\ReportCNR_CNRDig.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@ServicioPadre
                customersByCityReport.SetParameterValue(0, eOpCNRCNRDIG)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(1, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(2, eFechaFin)


                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 63 Then
                Titulo = "Reporte de Resumen de Clientes"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Resumen de Clientes"
                End If


                reportPath = RutaReportes + "\ReportResumenDeClientes.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)


                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 64 Then
                Titulo = "Reporte Auxiliar"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte Auxiliar"
                End If


                reportPath = RutaReportes + "\ReportTelefonia.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Fecha_Ini
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(1, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 65 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Hoteles con Fecha de �ltimo Pago"
                End If


                reportPath = RutaReportes + "\ReportHoteles.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 66 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte De Ordenes"
                End If


                reportPath = RutaReportes + "\ReportOrdenes.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Fecha_Ini
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(1, eFechaFin)
                '@Op
                customersByCityReport.SetParameterValue(2, eOp)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                'customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 67 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte Gerencial"
                End If

                reportPath = RutaReportes + "\ReportGerencial.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 68 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte Resumen de Cancelaciones"
                End If

                reportPath = RutaReportes + "\ReportCancelaciones.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Fecha_Ini
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(1, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 69 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Permanencia"
                End If

                reportPath = RutaReportes + "\ReportPermanencia.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Session
                customersByCityReport.SetParameterValue(0, eClv_Session)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@MesIni
                customersByCityReport.SetParameterValue(2, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(3, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(4, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(5, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"




            ElseIf eOpVentas = 70 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte Resumen de Ventas por Status de " & eServicio
                End If

                reportPath = RutaReportes + "\ReportVentasPorStatus.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Session
                customersByCityReport.SetParameterValue(0, eClv_Session)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@MesIni
                customersByCityReport.SetParameterValue(2, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(3, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(4, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(5, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 71 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Valor de Cartera"
                End If

                reportPath = RutaReportes + "\ReportCarteraVsIngreso.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Fecha
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Op
                customersByCityReport.SetParameterValue(1, eBnd)

                eTituloComision = "Del " & eFechaIni
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

                'Dim conexion As New SqlConnection(MiConexion)

                'Dim cmd As New SqlCommand("ReporteCarteraVsIngreso", conexion)
                'cmd.CommandTimeout = 0
                'cmd.CommandType = CommandType.StoredProcedure

                'cmd.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = eFechaIni

                'Dim dataAdapter As New SqlDataAdapter(cmd)
                'Dim dataTable As New DataSet

                'dataAdapter.Fill(dataTable, "Consulta")



                'If Me.InvokeRequired Then
                '    Dim d As New Forms(AddressOf DelegadoForm)
                '    Me.Invoke(d, New Object() {Me, ""})
                'Else
                '    Me.Text = "Reporte de Valor de Cartera"
                'End If

                'reportPath = "\Reportes\ReportReportCarteraVsIngreso.rpt"

                'customersByCityReport.Load(reportPath)
                'customersByCityReport.SetDataSource(dataTable)



                ''SetDBLogonForReport(connectionInfo, customersByCityReport)

                '''@Fecha
                ''customersByCityReport.SetParameterValue(0, eFechaIni)

                ''eTituloComision = "Del " & eFechaIni
                ''customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                ''customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text & "'"
                ''customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                ''customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 72 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Antig�edad de Cancelaciones"
                End If

                reportPath = RutaReportes + "\ReportAntiguedadDeCancelaciones.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Session
                customersByCityReport.SetParameterValue(0, eClv_Session)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@MesIni
                customersByCityReport.SetParameterValue(2, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(3, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(4, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(5, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 73 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Cancelaciones por Ejecutivo"
                End If

                reportPath = RutaReportes + "\ReportBajaDePrincipales.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Session
                customersByCityReport.SetParameterValue(0, eClv_Session)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@FechaIni
                customersByCityReport.SetParameterValue(2, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(3, eFechaFin)


                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 74 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Plazo Cubierto"
                End If

                reportPath = RutaReportes + "\ReportPlazoCubierto.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Session
                customersByCityReport.SetParameterValue(0, eClv_Session)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@Contratado
                customersByCityReport.SetParameterValue(2, eC)
                '@Instalado
                customersByCityReport.SetParameterValue(3, eI)
                '@Desconectado
                customersByCityReport.SetParameterValue(4, eD)
                '@Suspendido
                customersByCityReport.SetParameterValue(5, eS)
                '@Baja
                customersByCityReport.SetParameterValue(6, eB)
                '@Cubierto
                customersByCityReport.SetParameterValue(7, eTipo)



                'eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text & "'"
                'customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 75 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Adeudo Pagado"
                End If

                reportPath = RutaReportes + "\ReportAdeudoPagado.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Session
                customersByCityReport.SetParameterValue(0, eClv_Session)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@Pagado
                customersByCityReport.SetParameterValue(2, eTipo)


                'eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text & "'"
                'customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"



            ElseIf eOpVentas = 76 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Encuestas"
                End If

                reportPath = RutaReportes + "\ReportEncuestas.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@IDEncuesta
                customersByCityReport.SetParameterValue(0, eIDEncuesta)

                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            ElseIf eOpVentas = 77 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Recontrataciones"
                End If

                reportPath = RutaReportes + "\ReportRecontratacion.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@FechaIni
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(1, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 78 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Clientes con Pagos Diferidos"
                End If

                reportPath = RutaReportes + "\ReportPagosDif.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Primer
                customersByCityReport.SetParameterValue(1, ePrimero)
                '@Segundo
                customersByCityReport.SetParameterValue(2, eSegundo)
                '@Instalados
                customersByCityReport.SetParameterValue(3, eInst)
                '@Suspensi�n Temporal
                customersByCityReport.SetParameterValue(4, eTempo)


                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eTitulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            ElseIf eOpVentas = 79 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de n�mero/tipo de Decodificadores por Cliente"
                End If

                reportPath = RutaReportes + "\ReportDecodificadores.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Session
                customersByCityReport.SetParameterValue(0, LocClv_session)
                'Cont
                customersByCityReport.SetParameterValue(1, eC)
                'Inst
                customersByCityReport.SetParameterValue(2, eI)
                'Desc
                customersByCityReport.SetParameterValue(3, eD)
                'Susp
                customersByCityReport.SetParameterValue(4, eS)
                'Baja
                customersByCityReport.SetParameterValue(5, eB)
                'Fuera
                customersByCityReport.SetParameterValue(6, eF)



                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 80 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Valor de Cartera"
                End If

                reportPath = RutaReportes + "\ReportCarteraVsIngresoDet.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Fecha
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Op
                customersByCityReport.SetParameterValue(1, eBnd)
                '@Op
                customersByCityReport.SetParameterValue(2, LocClv_session)


                eTituloComision = "Del " & eFechaIni
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            ElseIf eOpVentas = 81 Then


                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Retiro de Aparatos"
                End If

                Dim dSet As New DataSet

                dSet = ReporteRetiroAparatos(eMesIni, eAnioIni, eMesFin, eAnioFin, eTrabajo)

                reportPath = RutaReportes + "\ReporteRetiroAparatos.rpt"
                customersByCityReport.Load(reportPath)
                customersByCityReport.SetDataSource(dSet)

                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eTitulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"




                'Yahve -------------------------------
            ElseIf eOpVentas = 100 Then
                Titulo = "Resumen de Puntos de Antig�edad"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Resumen de Puntos de Antig�edad "
                End If


                reportPath = RutaReportes + "\RepAntiguedadResumen.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Fecha_inicial
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Fecha_Final
                customersByCityReport.SetParameterValue(1, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & eTituloComision & "'"
                'customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"


            ElseIf eOpVentas = 500 Then
                Titulo = "Reporte de dias festivos"
                Dim DS As DataSet = New DataSet
                Dim LT As New List(Of String)
                LT.Add("tblDiasFest")
                BaseII.limpiaParametros()
                DS = BaseII.ConsultaDS("uspReportDiaFest", LT)
                reportPath = RutaReportes + "\ReporteDiasFest.rpt"
                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = ""

            ElseIf eOpVentas = 1002 Then
                Titulo = "Reporte Diario I"
                CheckForIllegalCrossThreadCalls = False
                Dim SetI As New DataSet
                customersByCityReport = New ReportDocument
                Me.Text = "Reporte Diario I "
                reportPath = RutaReportes + "\Reporte_Diario.rpt"

                Dim listaTablas As New List(Of String)
                listaTablas.Add("InstalacionesPorDia")
                listaTablas.Add("ServiciosDiarios")
                listaTablas.Add("QuejasPorDia")
                listaTablas.Add("VentasDiarias")
                listaTablas.Add("CancelacionesAcumuladas")
                listaTablas.Add("PorRecuperar")
                listaTablas.Add("PorPagar")
                listaTablas.Add("BonificacionesDiarias")
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Fecha", SqlDbType.DateTime, FechaDiaria)
                SetI = BaseII.ConsultaDS("MuestraInformacionDiaria", listaTablas)
                'ReportPruebaReportexsd = BaseII.ConsultaDS("ClientesCONRangoFecha2", listaTablas)
                customersByCityReport.Load(reportPath)
                SetDBReport(SetI, customersByCityReport)
                'customersByCityReport.SetDataSource(SetI)
                'CrystalReportViewer1.ReportSource = customersByCityReport
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                'customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = CDate(FechaDiaria)


            ElseIf eOpVentas = 1003 Then
                Titulo = "Reporte Diario II"
                CheckForIllegalCrossThreadCalls = False
                Dim SetII As New DataSet
                customersByCityReport = New ReportDocument
                Me.Text = "Reporte Diario II "
                reportPath = RutaReportes + "\Reporte_DiarioII.rpt"

                Dim listaTablas As New List(Of String)
                listaTablas.Add("InstalacionesPorDia")
                listaTablas.Add("ServiciosDiarios")
                listaTablas.Add("QuejasPorDia")
                listaTablas.Add("VentasDiarias")
                listaTablas.Add("CancelacionesAcumuladas")
                listaTablas.Add("PorRecuperar")
                listaTablas.Add("PorPagar")
                listaTablas.Add("BonificacionesDiarias")
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Fecha", SqlDbType.DateTime, FechaDiaria)
                SetII = BaseII.ConsultaDS("MuestraInformacionDiaria", listaTablas)
                'ReportPruebaReportexsd = BaseII.ConsultaDS("ClientesCONRangoFecha2", listaTablas)
                customersByCityReport.Load(reportPath)
                ' customersByCityReport.SetDataSource(SetII)
                SetDBReport(SetII, customersByCityReport)
                'CrystalReportViewer1.ReportSource = customersByCityReport
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                ' customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = CDate(FechaDiaria)


            End If

            If eOpVentas = 101 Then
                Dim d1Set As New DataSet
                customersByCityReport = New ReportDocument
                Me.Text = "Reporte Contrato de Privacidad "
                reportPath = RutaReportes + "\ReporteContratoPrivacidad.rpt"



                Dim listaTablas As New List(Of String)
                listaTablas.Add("EMPRESA")
                listaTablas.Add("REPORTE")
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@ACTIVO", SqlDbType.BigInt, privacidad)
                d1Set = BaseII.ConsultaDS("ContratoPrivacidad", listaTablas)
                'ReportPruebaReportexsd = BaseII.ConsultaDS("ClientesCONRangoFecha2", listaTablas)
                customersByCityReport.Load(reportPath)
                customersByCityReport.SetDataSource(d1Set)
                CrystalReportViewer1.ReportSource = customersByCityReport
            End If





            If eBndGraf = True Or eOpVentas = 20 Or eOpVentas = 21 Or eOpVentas = 22 Or eOpVentas = 23 Or eOpVentas = 31 Or eOpVentas = 33 Or eOpVentas = 57 Then
                eBndGraf = False
                eBndVen = True
                Me.customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            End If

            If Me.CrystalReportViewer1.InvokeRequired Then
                Dim d As New Reporte(AddressOf ConfigureCrystalReports)
                Me.Invoke(d, New Object() {})
            Else
                CrystalReportViewer1.ReportSource = customersByCityReport
            End If

            customersByCityReport = Nothing
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
            'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub FrmImprimirComision_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'PantallaProcesando.Show()
        If eOpVentas = 101 Then
            ConfigureCrystalReports()
        ElseIf eOpVentas <> 101 Then
            Me.BackgroundWorker1.RunWorkerAsync()

        End If

    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        ConfigureCrystalReports()
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        PantallaProcesando.Close()
    End Sub

    Private Function ReporteContratoF(ByVal Clv_Tipser As Integer, ByVal Orden As Integer)
        Dim con As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC ReporteContratoF " + Clv_Tipser.ToString() + ", " + Orden.ToString())
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), con)
        Dim dTable As New DataTable
        dAdapter.Fill(dTable)
        Return dTable
    End Function

    Private Function ReporteRetiroAparatos(ByVal MesIni As Integer, ByVal AnioIni As Integer, ByVal MesFin As Integer, ByVal AnioFin As Integer, ByVal Trabajo As String) As DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("ReporteRetiroAparatos")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@MESINI", SqlDbType.Int, MesIni)
        BaseII.CreateMyParameter("@ANIOINI", SqlDbType.Int, AnioIni)
        BaseII.CreateMyParameter("@MESFIN", SqlDbType.Int, MesFin)
        BaseII.CreateMyParameter("@ANIOFIN", SqlDbType.Int, AnioFin)
        BaseII.CreateMyParameter("@TRABAJO", SqlDbType.VarChar, Trabajo, 50)
        Return BaseII.ConsultaDS("ReporteRetiroAparatos", tableNameList)
    End Function

End Class