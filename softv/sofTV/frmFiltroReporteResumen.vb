﻿Public Class frmFiltroReporteResumen

#Region "EVENTOS"
    Private Sub frmFiltroReporteResumen_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.Label1.ForeColor = Color.Black
        Me.Label2.ForeColor = Color.Black
        Me.Label3.ForeColor = Color.Black

        UspSeleccionTrabajosNuevo()
        UspMostrarSeleccionTrabajoTMP()
        UspMostrarSeleccionTrabajo()

    End Sub
#End Region

#Region "CONTROLES"
    Private Sub btSeleccionaUno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btSeleccionaUno.Click
        If Me.lbTrabajosASeleccionar.Items.Count > 0 Then
            UspSeleccionaUnTrabajo()
            UspMostrarSeleccionTrabajoTMP()
            UspMostrarSeleccionTrabajo()
        End If
    End Sub
    Private Sub btSeleccionarTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btSeleccionarTodos.Click
        If Me.lbTrabajosASeleccionar.Items.Count > 0 Then
            UspSeleccionaTodosTrabajos()
            UspMostrarSeleccionTrabajoTMP()
            UspMostrarSeleccionTrabajo()
        End If
    End Sub
    Private Sub btDeseleccionaUno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btDeseleccionaUno.Click
        If Me.lbTrabajosSeleccionados.Items.Count > 0 Then
            UspDeseleccionaUnTrabajo()
            UspMostrarSeleccionTrabajoTMP()
            UspMostrarSeleccionTrabajo()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.lbTrabajosSeleccionados.Items.Count > 0 Then
            UspDeseleccionaUnTrabajo()
            UspMostrarSeleccionTrabajoTMP()
            UspMostrarSeleccionTrabajo()
        End If
    End Sub


    Private Sub btAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAceptar.Click
        eFechaIni = DateTimePicker1.Value
        eFechaFin = DateTimePicker2.Value
        GloBNDReporteNuevo = True
        FrmImprimirContrato.Show()
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
#End Region

#Region "METODOS"
    Private Sub UspSeleccionTrabajosNuevo()
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            DT = BaseII.ConsultaDT("UspSeleccionTrabajosNuevo")
            If DT.Rows.Count > 0 Then
                LocClv_session = CLng(DT.Rows(0)(0).ToString)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMostrarSeleccionTrabajoTMP()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSESSION", SqlDbType.BigInt, LocClv_session)
            lbTrabajosASeleccionar.DataSource = BaseII.ConsultaDT("UspMostrarSeleccionTrabajoTMP")
        Catch ex As Exception
            Throw EX
        End Try
    End Sub

    Private Sub UspMostrarSeleccionTrabajo()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSESSION", SqlDbType.BigInt, LocClv_session)
            lbTrabajosSeleccionados.DataSource = BaseII.ConsultaDT("UspMostrarSeleccionTrabajo")
        Catch ex As Exception
            Throw EX
        End Try
    End Sub

    Private Sub UspSeleccionaUnTrabajo()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSESSION", SqlDbType.BigInt, LocClv_session)
            BaseII.CreateMyParameter("@CLVTRABAJO", SqlDbType.Int, CInt(Me.lbTrabajosASeleccionar.SelectedValue))
            BaseII.Inserta("UspSeleccionaUnTrabajo")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspSeleccionaTodosTrabajos()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSESSION", SqlDbType.BigInt, LocClv_session)
            BaseII.Inserta("UspSeleccionaTodosTrabajos")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspDeseleccionaUnTrabajo()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSESSION", SqlDbType.BigInt, LocClv_session)
            BaseII.CreateMyParameter("@CLVTRABAJO", SqlDbType.Int, CInt(Me.lbTrabajosASeleccionar.SelectedValue))
            BaseII.Inserta("UspDeseleccionaUnTrabajo")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspDeseeleccionaTodosTrabajos()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSESSION", SqlDbType.BigInt, LocClv_session)
            BaseII.Inserta("UspDeseeleccionaTodosTrabajos")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region


    
    
   
End Class