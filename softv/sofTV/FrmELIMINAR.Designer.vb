﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmELIMINAR
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Label11 As System.Windows.Forms.Label
        Dim Label13 As System.Windows.Forms.Label
        Dim Label15 As System.Windows.Forms.Label
        Dim Label17 As System.Windows.Forms.Label
        Dim Label16 As System.Windows.Forms.Label
        Dim Label18 As System.Windows.Forms.Label
        Dim Label20 As System.Windows.Forms.Label
        Me.ObservacionesTxt = New System.Windows.Forms.TextBox()
        Me.ProveedorPlataformaTxt = New System.Windows.Forms.TextBox()
        Me.TipoEntregaModemCmb = New System.Windows.Forms.ComboBox()
        Me.ModeloModemCmb = New System.Windows.Forms.ComboBox()
        Me.MarcaModemCmb = New System.Windows.Forms.ComboBox()
        Me.SerieModemTxt = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Label11 = New System.Windows.Forms.Label()
        Label13 = New System.Windows.Forms.Label()
        Label15 = New System.Windows.Forms.Label()
        Label17 = New System.Windows.Forms.Label()
        Label16 = New System.Windows.Forms.Label()
        Label18 = New System.Windows.Forms.Label()
        Label20 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label11.ForeColor = System.Drawing.Color.LightSlateGray
        Label11.Location = New System.Drawing.Point(281, 128)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(105, 15)
        Label11.TabIndex = 110
        Label11.Text = "Observaciones:"
        '
        'Label13
        '
        Label13.AutoSize = True
        Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label13.ForeColor = System.Drawing.Color.LightSlateGray
        Label13.Location = New System.Drawing.Point(286, 75)
        Label13.Name = "Label13"
        Label13.Size = New System.Drawing.Size(169, 15)
        Label13.TabIndex = 113
        Label13.Text = "Proveedor de plataforma:"
        '
        'Label15
        '
        Label15.AutoSize = True
        Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label15.ForeColor = System.Drawing.Color.LightSlateGray
        Label15.Location = New System.Drawing.Point(25, 22)
        Label15.Name = "Label15"
        Label15.Size = New System.Drawing.Size(184, 15)
        Label15.TabIndex = 121
        Label15.Text = "Tipo de entrega de modem:"
        '
        'Label17
        '
        Label17.AutoSize = True
        Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label17.ForeColor = System.Drawing.Color.LightSlateGray
        Label17.Location = New System.Drawing.Point(25, 176)
        Label17.Name = "Label17"
        Label17.Size = New System.Drawing.Size(131, 15)
        Label17.TabIndex = 125
        Label17.Text = "Modelo de modem:"
        '
        'Label16
        '
        Label16.AutoSize = True
        Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label16.ForeColor = System.Drawing.Color.LightSlateGray
        Label16.Location = New System.Drawing.Point(25, 98)
        Label16.Name = "Label16"
        Label16.Size = New System.Drawing.Size(123, 15)
        Label16.TabIndex = 123
        Label16.Text = "Marca de modem:"
        '
        'Label18
        '
        Label18.AutoSize = True
        Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label18.ForeColor = System.Drawing.Color.LightSlateGray
        Label18.Location = New System.Drawing.Point(25, 256)
        Label18.Name = "Label18"
        Label18.Size = New System.Drawing.Size(117, 15)
        Label18.TabIndex = 129
        Label18.Text = "Serie de modem:"
        '
        'Label20
        '
        Label20.AutoSize = True
        Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label20.ForeColor = System.Drawing.Color.LightSlateGray
        Label20.Location = New System.Drawing.Point(286, 20)
        Label20.Name = "Label20"
        Label20.Size = New System.Drawing.Size(39, 15)
        Label20.TabIndex = 131
        Label20.Text = "SKU:"
        '
        'ObservacionesTxt
        '
        Me.ObservacionesTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ObservacionesTxt.Location = New System.Drawing.Point(284, 146)
        Me.ObservacionesTxt.MaxLength = 250
        Me.ObservacionesTxt.Multiline = True
        Me.ObservacionesTxt.Name = "ObservacionesTxt"
        Me.ObservacionesTxt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.ObservacionesTxt.Size = New System.Drawing.Size(224, 157)
        Me.ObservacionesTxt.TabIndex = 112
        '
        'ProveedorPlataformaTxt
        '
        Me.ProveedorPlataformaTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ProveedorPlataformaTxt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ProveedorPlataformaTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ProveedorPlataformaTxt.Location = New System.Drawing.Point(284, 93)
        Me.ProveedorPlataformaTxt.Name = "ProveedorPlataformaTxt"
        Me.ProveedorPlataformaTxt.Size = New System.Drawing.Size(224, 21)
        Me.ProveedorPlataformaTxt.TabIndex = 114
        '
        'TipoEntregaModemCmb
        '
        Me.TipoEntregaModemCmb.DisplayMember = "TipoEntrega"
        Me.TipoEntregaModemCmb.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.TipoEntregaModemCmb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TipoEntregaModemCmb.ForeColor = System.Drawing.Color.Black
        Me.TipoEntregaModemCmb.FormattingEnabled = True
        Me.TipoEntregaModemCmb.Location = New System.Drawing.Point(28, 39)
        Me.TipoEntregaModemCmb.Name = "TipoEntregaModemCmb"
        Me.TipoEntregaModemCmb.Size = New System.Drawing.Size(224, 21)
        Me.TipoEntregaModemCmb.TabIndex = 120
        Me.TipoEntregaModemCmb.ValueMember = "IdTipoEntrega"
        '
        'ModeloModemCmb
        '
        Me.ModeloModemCmb.DisplayMember = "Modelo"
        Me.ModeloModemCmb.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ModeloModemCmb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ModeloModemCmb.ForeColor = System.Drawing.Color.Black
        Me.ModeloModemCmb.FormattingEnabled = True
        Me.ModeloModemCmb.Location = New System.Drawing.Point(28, 194)
        Me.ModeloModemCmb.Name = "ModeloModemCmb"
        Me.ModeloModemCmb.Size = New System.Drawing.Size(224, 21)
        Me.ModeloModemCmb.TabIndex = 124
        Me.ModeloModemCmb.ValueMember = "IdModelo"
        '
        'MarcaModemCmb
        '
        Me.MarcaModemCmb.DisplayMember = "Marca"
        Me.MarcaModemCmb.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MarcaModemCmb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MarcaModemCmb.ForeColor = System.Drawing.Color.Black
        Me.MarcaModemCmb.FormattingEnabled = True
        Me.MarcaModemCmb.Location = New System.Drawing.Point(28, 116)
        Me.MarcaModemCmb.Name = "MarcaModemCmb"
        Me.MarcaModemCmb.Size = New System.Drawing.Size(224, 21)
        Me.MarcaModemCmb.TabIndex = 122
        Me.MarcaModemCmb.ValueMember = "IdMarca"
        '
        'SerieModemTxt
        '
        Me.SerieModemTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SerieModemTxt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.SerieModemTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SerieModemTxt.Location = New System.Drawing.Point(28, 274)
        Me.SerieModemTxt.MaxLength = 50
        Me.SerieModemTxt.Name = "SerieModemTxt"
        Me.SerieModemTxt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SerieModemTxt.Size = New System.Drawing.Size(224, 22)
        Me.SerieModemTxt.TabIndex = 128
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(284, 39)
        Me.TextBox1.MaxLength = 50
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TextBox1.Size = New System.Drawing.Size(224, 22)
        Me.TextBox1.TabIndex = 416
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Orange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(218, 318)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 417
        Me.Button2.Text = "&Aceptar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Orange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(371, 318)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(136, 36)
        Me.Button3.TabIndex = 418
        Me.Button3.Text = "Cancelar"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'FrmTelmexDCInstalaServicios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(538, 373)
        Me.Controls.Add(Label15)
        Me.Controls.Add(Label11)
        Me.Controls.Add(Me.ObservacionesTxt)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.ProveedorPlataformaTxt)
        Me.Controls.Add(Me.ModeloModemCmb)
        Me.Controls.Add(Label20)
        Me.Controls.Add(Label16)
        Me.Controls.Add(Label13)
        Me.Controls.Add(Label17)
        Me.Controls.Add(Label18)
        Me.Controls.Add(Me.MarcaModemCmb)
        Me.Controls.Add(Me.TipoEntregaModemCmb)
        Me.Controls.Add(Me.SerieModemTxt)
        Me.Name = "FrmTelmexDCInstalaServicios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Asignación de Servicios Telmex"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ObservacionesTxt As System.Windows.Forms.TextBox
    Friend WithEvents ProveedorPlataformaTxt As System.Windows.Forms.TextBox
    Friend WithEvents TipoEntregaModemCmb As System.Windows.Forms.ComboBox
    Friend WithEvents ModeloModemCmb As System.Windows.Forms.ComboBox
    Friend WithEvents MarcaModemCmb As System.Windows.Forms.ComboBox
    Friend WithEvents SerieModemTxt As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
End Class
