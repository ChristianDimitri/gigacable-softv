Imports System.Data.SqlClient
Public Class FrmAtenTel

    'Private WithEvents Documento As New Printing.PrintDocument
    Private Sub FrmAtenTel_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        If eOpcion = "N" Then
            Me.BindingNavigatorAddNewItem.Enabled = False
            Nuevo()
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)

        If eOpcion = "C" Then
            Consultar()
            UspDesactivaBotones(Me, Me.Name)
        End If

    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        BuscaCte()
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If Me.RadioButton1.Checked = True Then
            Me.TextBox3.Enabled = True
            Me.Button1.Enabled = True
            Me.TextBox4.ReadOnly = True
            Me.TextBox5.ReadOnly = True
            Me.NombreComboBox.Enabled = False
            Me.GroupBox2.Text = "Datos del Cliente"
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        Dim CON As New SqlConnection(MiConexion)
        If Me.RadioButton2.Checked = True Then
            Me.TextBox3.Enabled = False
            Me.Button1.Enabled = False
            Me.TextBox4.ReadOnly = False
            Me.TextBox5.ReadOnly = False
            Me.NombreComboBox.Enabled = True
            Me.TextBox3.Clear()
            Me.TextBox4.Clear()
            Me.TextBox5.Clear()
            CON.Open()
            Me.MuestraColoniaSecTableAdapter.Connection = CON
            Me.MuestraColoniaSecTableAdapter.Fill(Me.DataSetEric.MuestraColoniaSec, 0, 0, 0)
            CON.Close()
            Me.GroupBox2.Text = "Datos de la Persona"
        End If
    End Sub
    Private Sub BuscaCte()
        Dim CON As New SqlConnection(MiConexion)
        Dim eRes As Integer = 0
        Dim eMsg As String = Nothing
        Dim eRes2 As Integer = 0
        Dim eMsg2 As String = Nothing
        If IsNumeric(Me.TextBox3.Text) = True Then
            CON.Open()
            Me.ConAtenTelCteTableAdapter.Connection = CON
            Me.ConAtenTelCteTableAdapter.Fill(Me.DataSetEric.ConAtenTelCte, CLng(Me.TextBox3.Text), eRes, eMsg)
            CON.Close()
            If eRes = 1 Then
                Me.TextBox3.Clear()
                Me.TextBox4.Clear()
                Me.TextBox5.Clear()
                CON.Open()
                Me.MuestraColoniaSecTableAdapter.Connection = CON
                Me.MuestraColoniaSecTableAdapter.Fill(Me.DataSetEric.MuestraColoniaSec, 0, 0, 0)
                CON.Close()
                
                MsgBox(eMsg)
            Else

                If eOpcion = "N" Then
                    CON.Open()
                    Me.BuscaSiTieneQuejaTableAdapter.Connection = CON
                    Me.BuscaSiTieneQuejaTableAdapter.Fill(Me.DataSetEric2.BuscaSiTieneQueja, 0, CLng(Me.TextBox3.Text), eRes2, eMsg2)
                    CON.Close()
                    If eRes2 = 1 Then
                        Me.ObservacionTextBox.Text = eMsg2
                        Me.ObservacionTextBox.ForeColor = Color.Red
                    Else
                        Me.ObservacionTextBox.Text = ""
                        Me.ObservacionTextBox.ForeColor = Color.Black
                    End If
                End If

                Me.TextBox4.Text = Me.NombreTextBox2.Text
                Me.TextBox5.Text = Me.TelefonoTextBox1.Text
                CON.Open()
                Me.MuestraColoniaSecTableAdapter.Connection = CON
                Me.MuestraColoniaSecTableAdapter.Fill(Me.DataSetEric.MuestraColoniaSec, CLng(Me.Clv_ColoniaTextBox1.Text), 0, 3)
                CON.Close()
            End If
        End If
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            BuscaCte()
        End If
    End Sub


    Private Sub ConAtenTelBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConAtenTelBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        Try
            If IsNumeric(Me.TextBox3.Text) = False Then
                Me.TextBox3.Text = "0"
            End If

            If Me.TextBox4.Text.Length = 0 Then
                MsgBox("Captuta el Nombre de la Persona.")
                Exit Sub
            End If

            If Me.TextBox5.Text.Length = 0 Then
                Me.TextBox5.Text = "Tel�fono No Registrado."
            End If

            CON.Open()
            Me.ConAtenTelTableAdapter.Connection = CON
            Me.ConAtenTelTableAdapter.Insert(CLng(Me.TextBox3.Text), CInt(Me.ConceptoComboBox.SelectedValue), CInt(Me.NombreComboBox.SelectedValue), Me.TextBox5.Text, CInt(Me.DescripcionComboBox.SelectedValue), Me.HoraDateTimePicker1.Value, Me.FechaDateTimePicker1.Value, Me.ObservacionTextBox.Text, CInt(eClv_Usuario), Me.TextBox4.Text)
            CON.Close()

            MsgBox(mensaje5)

            Me.BindingNavigatorAddNewItem.Enabled = True
            Me.ConAtenTelBindingNavigatorSaveItem.Enabled = False

        Catch ex As Exception

        End Try
    End Sub
    Private Sub Nuevo()
        Dim CON As New SqlConnection(MiConexion)


        CON.Open()
        Me.DameFechaHoraTableAdapter.Connection = CON
        Me.DameFechaHoraTableAdapter.Fill(Me.DataSetEric.DameFechaHora)
        Me.CONUSUARIOSTableAdapter.Connection = CON
        Me.CONUSUARIOSTableAdapter.Fill(Me.DataSetEric.CONUSUARIOS, eClv_Usuario)
        Me.MuestraTipServEricTableAdapter.Connection = CON
        Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric.MuestraTipServEric, 0, 0)
        Me.MuestraColoniaSecTableAdapter.Connection = CON
        Me.MuestraColoniaSecTableAdapter.Fill(Me.DataSetEric.MuestraColoniaSec, 0, 0, 0)
        Me.ConMotAtenTelTableAdapter.Connection = CON
        Me.ConMotAtenTelTableAdapter.Fill(Me.DataSetEric.ConMotAtenTel, 0, "", 0)

        Me.TextBox1.Text = CStr(Me.FechaDateTimePicker1.Value.Date)
        Me.TextBox2.Text = CStr(Me.HoraDateTimePicker1.Text)

        Me.TextBox3.Clear()
        Me.TextBox4.Clear()
        Me.TextBox5.Clear()

        CON.Close()

        Me.RadioButton2.Checked = True

    End Sub

    Private Sub BindingNavigatorAddNewItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorAddNewItem.Click
        Nuevo()
        Me.BindingNavigatorAddNewItem.Enabled = False
        Me.ConAtenTelBindingNavigatorSaveItem.Enabled = True
    End Sub

    Private Sub Consultar()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConAtenTelTableAdapter.Connection = CON
        Me.ConAtenTelTableAdapter.Fill(Me.DataSetEric.ConAtenTel, eClave, "", "", 0, 0, Today, 6)
        Me.TextBox1.Text = CStr(Me.FechaDateTimePicker.Value.Date)
        Me.TextBox2.Text = Me.HoraDateTimePicker.Text
        Me.CONUSUARIOSTableAdapter.Connection = CON
        Me.CONUSUARIOSTableAdapter.Fill(Me.DataSetEric.CONUSUARIOS, CInt(Me.Clv_UsuarioTextBox.Text))
        Me.MuestraTipServEricTableAdapter.Connection = CON
        Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric.MuestraTipServEric, CInt(Me.Clv_TipSerTextBox.Text), 0)
        Me.MuestraColoniaSecTableAdapter.Connection = CON
        Me.MuestraColoniaSecTableAdapter.Fill(Me.DataSetEric.MuestraColoniaSec, CLng(Me.Clv_ColoniaTextBox.Text), 0, 3)
        Me.ConMotAtenTelTableAdapter.Connection = CON
        Me.ConMotAtenTelTableAdapter.Fill(Me.DataSetEric.ConMotAtenTel, CInt(Me.Clv_MotivoTextBox.Text), "", 2)
        Me.TextBox3.Text = Me.ContratoTextBox.Text
        Me.TextBox4.Text = Me.NombreTextBox.Text
        Me.TextBox5.Text = Me.TelefonoTextBox.Text

        CON.Close()

        If Me.ContratoTextBox.Text > 0 Then

            Me.RadioButton1.Checked = True
        Else
            Me.RadioButton2.Checked = False
        End If

        Me.GroupBox1.Enabled = False
        Me.GroupBox2.Enabled = False
        Me.GroupBox4.Enabled = False
        Me.ConAtenTelBindingNavigator.Enabled = False

    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
    
    Private Sub ConceptoComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConceptoComboBox.SelectedIndexChanged
        If Me.ConceptoComboBox.SelectedValue = 1 Then
            Me.WebBrowser1.Navigate(RutaReportes + "\Basico.doc")
        End If

        If Me.ConceptoComboBox.SelectedValue = 2 Then
            Me.WebBrowser1.Navigate(RutaReportes + "\Internet.doc")
        End If

        If Me.ConceptoComboBox.SelectedValue = 3 Then
            Me.WebBrowser1.Navigate(RutaReportes + "\Digital.doc")
        End If
    End Sub


#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region
    
End Class