﻿Public Class FrmRepCortesia

    Private Sub FrmRepCortesia_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
    End Sub

    Private Sub bnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bnAceptar.Click
        If cbActivo.Checked = False And cbInactivo.Checked = False Then
            MessageBox.Show("Selecciona un status de la cortesía.")
            Exit Sub
        End If

        GloRepCortesiaActivo = cbActivo.Checked
        GloRepCortesiaInactivo = cbInactivo.Checked

        bndReportC = True
        Me.Close()
    End Sub

    Private Sub bnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles bnCancelar.Click
        bndReportC = False
        Me.Close()
    End Sub

End Class