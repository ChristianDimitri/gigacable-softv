﻿Public Class BrwTrabajoPuntos

#Region "Buscar"
    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        UspBuscarPuntosTrabajoFechas(CInt(Me.txtClave.Text), CInt(Me.cmbTrabajos.SelectedValue))
    End Sub
#End Region
  

#Region "Eventos"
    Private Sub BrwTrabajoPuntos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        UspMostrarTipSerPuntosBase()
        UspMostrarTipoPuntosBase()
        UspBuscarPuntosTrabajoFechas(0, 0)

        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub BrwTrabajoPuntos_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        UspBuscarPuntosTrabajoFechas(0, 0)
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        UspLlenaTrabajosPuntosFechas()
    End Sub

    Private Sub cmbTipServ_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTipServ.SelectedIndexChanged
        UspLlenaTrabajosPuntosFechas()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub dgTrabajosPuntos_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgTrabajosPuntos.SelectionChanged
        If (dgTrabajosPuntos.SelectedRows.Count) > 0 Then
            Me.lblClave.Text = dgTrabajosPuntos.SelectedCells(0).Value.ToString
            Me.lblTrabajo.Text = dgTrabajosPuntos.SelectedCells(2).Value.ToString
            Me.lblFechaIni.Text = dgTrabajosPuntos.SelectedCells(3).Value.ToString
            Me.lblFechaFin.Text = dgTrabajosPuntos.SelectedCells(4).Value.ToString
            Me.lblVigente.Text = dgTrabajosPuntos.SelectedCells(5).Value.ToString
            Me.lblPuntos.Text = dgTrabajosPuntos.SelectedCells(6).Value.ToString
        End If
    End Sub
#End Region

#Region "Funciones"
    Private Sub UspBuscarPuntosTrabajoFechas(ByVal prmID As Integer, ByVal prmClvTrabajo As Integer)
        Try
            If Len(prmID) = 0 Then
                prmID = 0
            End If
            If Len(prmClvTrabajo) = 0 Then
                prmClvTrabajo = 0
            End If

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ID", SqlDbType.Int, prmID)
            BaseII.CreateMyParameter("@CLVTRABAJO", SqlDbType.Int, prmClvTrabajo)
            dgTrabajosPuntos.DataSource = BaseII.ConsultaDT("UspBuscarPuntosTrabajoFechas")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMostrarTipoPuntosBase()
        Try
            BaseII.limpiaParametros()
            Me.ComboBox1.DataSource = BaseII.ConsultaDT("UspMostrarTipoPuntosBase")

            Me.ComboBox1.SelectedIndex = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMostrarTipSerPuntosBase()
        Try
            BaseII.limpiaParametros()
            cmbTipServ.DataSource = BaseII.ConsultaDT("UspMostrarTipSerPuntosBase")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspLlenaTrabajosPuntosFechas()
        Try
            If Len(ComboBox1.Text) > 0 And Len(cmbTipServ.Text) > 0 Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLVTIPSER", SqlDbType.Int, Me.cmbTipServ.SelectedValue)
                BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, Me.ComboBox1.SelectedValue, 1)
                cmbTrabajos.DataSource = BaseII.ConsultaDT("UspLlenaTrabajosPuntosFechas")
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region

#Region "Nuevo"
    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        GloClvPuntosTrabajosFechas = 0
        opcion = "N"
        FrmTrabajosPuntos.Show()
    End Sub
#End Region


#Region "Consultar"
    Private Sub BtnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnConsultar.Click
        If (dgTrabajosPuntos.SelectedRows.Count) > 0 Then
            GloClvPuntosTrabajosFechas = CInt(dgTrabajosPuntos.SelectedCells(0).Value.ToString)
            opcion = "C"
            FrmTrabajosPuntos.Show()
        Else
            MsgBox("No hay ningun trabajo que consultar", MsgBoxStyle.Information, "Puntos de Trabajos")
        End If
    End Sub
#End Region
   
#Region "Modificar"
    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        If (dgTrabajosPuntos.SelectedRows.Count) > 0 Then
            GloClvPuntosTrabajosFechas = CInt(dgTrabajosPuntos.SelectedCells(0).Value.ToString)
            opcion = "M"
            FrmTrabajosPuntos.Show()
        Else
            MsgBox("No hay ningun trabajo que modificar", MsgBoxStyle.Information, "Puntos de Trabajos")
        End If
    End Sub
#End Region
   
   
End Class