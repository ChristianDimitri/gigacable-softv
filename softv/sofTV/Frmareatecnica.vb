
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.Data.SqlClient
Imports System.Collections.Generic

Public Class Frmareatecnica
    Delegate Sub Reporte(ByVal op As String, ByVal Titulop As String)
    Public customersByCityReport As ReportDocument
    Dim op As String = Nothing
    Public bnd As Boolean = False
    Public bnd2 As Boolean = False
    Dim Titulo As String = Nothing
    Private opreporte As Integer = 0


    Private Sub ConfigureCrystalReports(ByVal op As String, ByVal Titulo As String)
        Try
            RutaReportes = DameRutaReportes(1)
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Status As String = Nothing

            If Me.PendientesCheckBox.CheckState = CheckState.Checked Then
                StatusPen = "1"
                If Me.EjecutadasCheckBox.Checked = True Or Me.VisitaCheckBox.Checked = True Then
                    Status = Status & "Pendientes,"
                Else
                    Status = "Pendientes"
                End If
            End If
            If Me.EjecutadasCheckBox.CheckState = CheckState.Checked Then
                StatusEje = "1"
                If Me.PendientesCheckBox.Checked = True Or Me.VisitaCheckBox.Checked = True Then
                    If Me.PendientesCheckBox.Checked = True Then
                        Status = Status & "Ejecutadas"
                    Else
                        Status = Status & "Ejecutadas,"
                    End If
                Else
                    Status = "Ejecutadas"
                End If
            End If
            If Me.VisitaCheckBox.CheckState = CheckState.Checked Then
                StatusVis = "1"
                If Me.PendientesCheckBox.Checked = True Or Me.EjecutadasCheckBox.Checked = True Then
                    Status = Status & "Visita"
                Else
                    Status = "Visita"
                End If
            End If

            'If Me.CheckBox1.CheckState = CheckState.Checked Then
            '    Op1 = "1"
            'End If
            'If Me.CheckBox2.CheckState = CheckState.Checked Then
            '    Op2 = "1"
            'End If
            'If Me.CheckBox3.CheckState = CheckState.Checked Then
            '    Op3 = "1"
            'End If
            'If Me.CheckBox4.CheckState = CheckState.Checked Then
            '    Op4 = "1"
            'End If
            'If Me.CheckBox5.CheckState = CheckState.Checked Then
            '    Op5 = "1"
            'End If

            If Me.NUMINILbl.Text.Length > 0 Then
                If IsNumeric(Me.NUMINILbl.Text) = True Then
                    If CLng(Me.NUMINILbl.Text) > 0 Then
                        Op1 = "1"
                    End If
                End If
            End If
            If Me.FECSOLINI.Text.Length > 0 Then

                Op2 = "1"
            End If
            If Me.FECEJEINI.Text.Length > 0 Then
                Op3 = "1"
            End If
            If Me.NOMTRABAJO.Text.Length > 0 Then
                Op4 = "1"
            End If
            If Me.NOMCOLONIA.Text.Length > 0 Then
                Op5 = "1"
            End If

            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"
            If Me.RadioButton1.Checked = True Then
                OpOrdenar = "0"
            ElseIf Me.RadioButton2.Checked = True Then
                OpOrdenar = "1"
            ElseIf Me.RadioButton4.Checked = True Then
                OpOrdenar = "2"
            End If

            Dim reportPath As String = Nothing
            'Select Case op
            '    Case 7
            'reportPath = Application.StartupPath + "\Reportes\" + "RepAntiguedad.rpt"
            '    Case 8
            'reportPath = Application.StartupPath + "\Reportes\" + "RepBancos.rpt"
            '    Case 15
            'reportPath = Application.StartupPath + "\Reportes\" + "RepReconexionesPendientes.rpt"
            '    Case Else
            'reportPath = Application.StartupPath + "\Reportes\" + "ReporteBasicoTv.rpt"
            'End Select
            'C:\Documents and Settings\Edgar\Mis documentos\Visual Studio 2005\Projects\sofTV.root\sofTV\sofTV\bin\Reportes\

            If op = 0 Then
                If IdSistema <> "SA" And IdSistema <> "VA" Then
                    reportPath = RutaReportes + "\ReporteOrdenesListadoNuevo.rpt"
                ElseIf IdSistema = "SA" Or IdSistema = "VA" Then
                    reportPath = RutaReportes + "\ReporteOrdenesListadoNuevo_SA.rpt"
                End If
            ElseIf op = 1 Then
                If IdSistema = "TO" Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCabStar.rpt"
                ElseIf IdSistema = "AG" Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBueno.rpt"
                ElseIf IdSistema = "SA" Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoTvRey.rpt"
                ElseIf IdSistema = "VA" Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCosmo.rpt"
                ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoLogitel.rpt"
                End If

            End If

            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            'MsgBox(reportPath)

            '@Clv_TipSer int
            customersByCityReport.SetParameterValue(0, 0)
            ',@op1 smallint
            customersByCityReport.SetParameterValue(1, Op1)
            ',@op2 smallint
            customersByCityReport.SetParameterValue(2, Op2)
            ',@op3 smallint
            customersByCityReport.SetParameterValue(3, Op3)
            ',@op4 smallint,
            customersByCityReport.SetParameterValue(4, Op4)
            '@op5 smallint
            customersByCityReport.SetParameterValue(5, Op5)
            ',@StatusPen bit
            customersByCityReport.SetParameterValue(6, StatusPen)
            ',@StatusEje bit
            customersByCityReport.SetParameterValue(7, StatusEje)
            ',@StatusVis bit,
            customersByCityReport.SetParameterValue(8, StatusVis)
            '@Clv_OrdenIni bigint
            If IsNumeric(Me.NUMINILbl.Text) = True Then Num1 = CStr(Me.NUMINILbl.Text)
            customersByCityReport.SetParameterValue(9, CLng(Num1))
            ',@Clv_OrdenFin bigint
            If IsNumeric(Me.NUMFINLBL.Text) = True Then Num2 = CStr(Me.NUMFINLBL.Text)
            customersByCityReport.SetParameterValue(10, CLng(Num2))
            ',@Fec1Ini Datetime
            If IsDate(Me.FECSOLINI.Text) = True Then Fec1Ini = Me.FECSOLINI.Text
            customersByCityReport.SetParameterValue(11, Fec1Ini)
            ',@Fec1Fin Datetime,
            If IsDate(Me.FECSOLFIN.Text) = True Then Fec1Fin = Me.FECSOLFIN.Text
            customersByCityReport.SetParameterValue(12, Fec1Fin)
            '@Fec2Ini Datetime
            If IsDate(Me.FECEJEINI.Text) = True Then Fec2Ini = Me.FECEJEINI.Text
            customersByCityReport.SetParameterValue(13, Fec2Ini)
            ',@Fec2Fin Datetime
            If IsDate(Me.FECEJEFIN.Text) = True Then Fec2Fin = Me.FECEJEFIN.Text
            customersByCityReport.SetParameterValue(14, Fec2Fin)
            ',@Clv_Trabajo int
            If IsNumeric(Me.CLV_TRABAJO.Text) = True Then nclv_trabajo = CStr(Me.CLV_TRABAJO.Text)
            customersByCityReport.SetParameterValue(15, nclv_trabajo)
            ',@Clv_Colonia int
            If IsNumeric(Me.CLV_COLONIA.Text) = True Then nClv_colonia = CStr(Me.CLV_COLONIA.Text)
            customersByCityReport.SetParameterValue(16, nClv_colonia)
            ',@OpOrden int
            customersByCityReport.SetParameterValue(17, OpOrdenar)


            'Select Case op
            '    Case 0, 2, 3, 6, 12
            '        mySelectFormula = "Reporte de Clientes " & mySelectFormula
            '    Case 1, 5, 11, 15, 14, 8
            '        mySelectFormula = "Reporte de " & mySelectFormula
            '    Case 4, 7, 9, 10, 13, 16
            '        mySelectFormula = "Resumen de " & mySelectFormula
            'End Select

            'Select Case op
            '    Case 4, 6
            '        customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            '    Case Else
            '        customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            'End Select

            mySelectFormula = "Listado de Ordenes de Servicio"
            If op = 0 Then
                customersByCityReport.DataDefinition.FormulaFields("empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
                customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"
                'customersByCityReport.DataDefinition.FormulaFields("nomservicio").Text = "'" & Me.ComboBox4.Text & " con status " & Status & "'"
            ElseIf op = 1 Then
                mySelectFormula = "Orden De Servicio: "
                customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
            End If

            '--SetDBLogonForReport(connectionInfo)

            If Me.CrystalReportViewer1.InvokeRequired Then
                Dim d As New Reporte(AddressOf ConfigureCrystalReports)
                Me.Invoke(d, New Object() {op, Titulo})
            Else
                CrystalReportViewer1.ReportSource = customersByCityReport
            End If


            'CrystalReportViewer1.ReportSource = customersByCityReport

            customersByCityReport = Nothing
            bnd = True

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ConfigureCrystalReportsNewOrden(ByVal op As String, ByVal Titulo As String)
        Try
            RutaReportes = DameRutaReportes(1)
            customersByCityReport = New ReportDocument
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Status As String = Nothing
            Dim dSet As New DataSet

            If Me.PendientesCheckBox.CheckState = CheckState.Checked Then
                StatusPen = "1"
                If Me.EjecutadasCheckBox.Checked = True Or Me.VisitaCheckBox.Checked = True Then
                    Status = Status & "Pendientes,"
                Else
                    Status = "Pendientes"
                End If
            End If
            If Me.EjecutadasCheckBox.CheckState = CheckState.Checked Then
                StatusEje = "1"
                If Me.PendientesCheckBox.Checked = True Or Me.VisitaCheckBox.Checked = True Then
                    If Me.PendientesCheckBox.Checked = True Then
                        Status = Status & "Ejecutadas"
                    Else
                        Status = Status & "Ejecutadas,"
                    End If
                Else
                    Status = "Ejecutadas"
                End If
            End If
            If Me.VisitaCheckBox.CheckState = CheckState.Checked Then
                StatusVis = "1"
                If Me.PendientesCheckBox.Checked = True Or Me.EjecutadasCheckBox.Checked = True Then
                    Status = Status & "Visita"
                Else
                    Status = "Visita"
                End If
            End If

            If Me.NUMINILbl.Text.Length > 0 Then
                If IsNumeric(Me.NUMINILbl.Text) = True Then
                    If CLng(Me.NUMINILbl.Text) > 0 Then
                        Op1 = "1"
                    End If
                End If
            End If
            If Me.FECSOLINI.Text.Length > 0 Then

                Op2 = "1"
            End If
            If Me.FECEJEINI.Text.Length > 0 Then
                Op3 = "1"
            End If
            If Me.NOMTRABAJO.Text.Length > 0 Then
                Op4 = "1"
            End If
            If Me.NOMCOLONIA.Text.Length > 0 Then
                Op5 = "1"
            End If

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"
            If Me.RadioButton1.Checked = True Then
                OpOrdenar = "0"
            ElseIf Me.RadioButton2.Checked = True Then
                OpOrdenar = "1"
            ElseIf Me.RadioButton4.Checked = True Then
                OpOrdenar = "2"
            End If

            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\rptReporteOrdenesDeServicio.rpt"

            If IsNumeric(Me.NUMINILbl.Text) = True Then Num1 = CStr(Me.NUMINILbl.Text)
            If IsNumeric(Me.NUMFINLBL.Text) = True Then Num2 = CStr(Me.NUMFINLBL.Text)
            If IsDate(Me.FECSOLINI.Text) = True Then Fec1Ini = Me.FECSOLINI.Text
            If IsDate(Me.FECSOLFIN.Text) = True Then Fec1Fin = Me.FECSOLFIN.Text
            If IsDate(Me.FECEJEINI.Text) = True Then Fec2Ini = Me.FECEJEINI.Text
            If IsDate(Me.FECEJEFIN.Text) = True Then Fec2Fin = Me.FECEJEFIN.Text
            If IsNumeric(Me.CLV_TRABAJO.Text) = True Then nclv_trabajo = CStr(Me.CLV_TRABAJO.Text)
            If IsNumeric(Me.CLV_COLONIA.Text) = True Then nClv_colonia = CStr(Me.CLV_COLONIA.Text)

            dSet = uspReporteOrdenesDeServicio(0, Op1, Op2, Op3, Op4, Op5, StatusPen, StatusEje, StatusVis, CLng(Num1), CLng(Num2), Fec1Ini, Fec1Fin, Fec2Ini, Fec2Fin, nclv_trabajo, nClv_colonia, OpOrdenar)

            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(dSet)

            mySelectFormula = "Listado de Ordenes de Servicio"
            If op = 0 Then
                customersByCityReport.DataDefinition.FormulaFields("empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
                customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"
                'customersByCityReport.DataDefinition.FormulaFields("nomservicio").Text = "'" & Me.ComboBox4.Text & " con status " & Status & "'"
            ElseIf op = 1 Then
                mySelectFormula = "Orden De Servicio: "
                customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
            End If

            CrystalReportViewer1.ReportSource = customersByCityReport

            customersByCityReport = Nothing
            bnd = True
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Frmareatecnica_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBusca_NumOrden = True Then
            RutaReportes = DameRutaReportes(1)
            GloBusca_NumOrden = False
            Me.NUMFINLBL.Text = GLONUMCLV_ORDEN_FIN
            Me.NUMINILbl.Text = GLONUMCLV_ORDEN_INI
            GLONUMCLV_ORDEN_FIN = 0
            GLONUMCLV_ORDEN_INI = 0
        End If
        If op = "1" And GloBndSelFecha = True Then
            RutaReportes = DameRutaReportes(1)
            GloBndSelFecha = False
            Me.FECSOLINI.Text = GloFecha_Ini
            Me.FECSOLFIN.Text = GloFecha_Fin
            GloFecha_Ini = "01/01/1900"
            GloFecha_Fin = "01/01/1900"
        End If
        If op = "2" And GloBndSelFecha = True Then
            RutaReportes = DameRutaReportes(1)
            GloBndSelFecha = False
            Me.FECEJEINI.Text = GloFecha_Ini
            Me.FECEJEFIN.Text = GloFecha_Fin
            GloFecha_Ini = "01/01/1900"
            GloFecha_Fin = "01/01/1900"
        End If
        If GloBndTrabajo = True Then
            RutaReportes = DameRutaReportes(1)
            GloBndTrabajo = False
            Me.CLV_TRABAJO.Text = GloNumClv_Trabajo
            Me.NOMTRABAJO.Text = GLONOMTRABAJO
        End If
        If GlobndClv_Colonia = True Then
            RutaReportes = DameRutaReportes(1)
            GlobndClv_Colonia = False
            Me.CLV_COLONIA.Text = GloNumClv_Colonia
            Me.NOMCOLONIA.Text = GLONOMCOLONIA
        End If
        'If bnd = True And Me.ActiveForm.ToString = "Frmareatecnica" Then
        '    bnd = False
        '    PantallaProcesando.Close()
        'End If
    End Sub





    Private Sub Frmareatecnica_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        ''TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla seg�n sea necesario.
        'Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        'Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
        'If IsNumeric(Me.ComboBox4.SelectedValue) = True Then
        '    GloClv_TipSer = Me.ComboBox4.SelectedValue
        '    LIMPIA()
        'Else
        '    GloClv_TipSer = glotiposervicioppal
        'End If

        '--Me.CatalogodeReportesAreaTecnicaTableAdapter.Fill(Me.NewsoftvDataSet1.CatalogodeReportesAreaTecnica, Me.ComboBox4.SelectedValue, 0)
        '--Me.CatalogodeReportesAreaTecnicaTableAdapter.Fill(Me.NewsoftvDataSet1.CatalogodeReportesAreaTecnica, 1, 0)
        CON.Close()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub


    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub

    Private Sub LIMPIA()
        Me.CheckBox1.CheckState = CheckState.Unchecked
        Me.CheckBox2.CheckState = CheckState.Unchecked
        Me.CheckBox3.CheckState = CheckState.Unchecked
        Me.CheckBox5.CheckState = CheckState.Unchecked
        Me.CheckBox4.CheckState = CheckState.Unchecked
        Me.NUMFINLBL.Text = 0
        Me.NUMINILbl.Text = 0
        Me.FECSOLFIN.Text = ""
        Me.FECSOLINI.Text = ""
        Me.FECEJEFIN.Text = ""
        Me.FECEJEINI.Text = ""
        Me.CLV_TRABAJO.Text = 0
        Me.NOMTRABAJO.Text = ""
        Me.CLV_COLONIA.Text = 0
        Me.NOMCOLONIA.Text = ""
    End Sub

    'Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
    '    If IsNumeric(Me.ComboBox4.SelectedValue) = True Then
    '        GloClv_TipSer = Me.ComboBox4.SelectedValue
    '        LIMPIA()
    '        '--Me.CatalogodeReportesAreaTecnicaTableAdapter.Fill(Me.NewsoftvDataSet1.CatalogodeReportesAreaTecnica, Me.ComboBox4.SelectedValue, 0)
    '    End If
    'End Sub



    Private Sub PendientesCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PendientesCheckBox.CheckedChanged
        If Me.PendientesCheckBox.CheckState = CheckState.Checked Then
            GloPendientes = 1
            Me.EjecutadasCheckBox.CheckState = CheckState.Unchecked
            Me.VisitaCheckBox.CheckState = CheckState.Unchecked
        Else
            GloPendientes = 0
        End If
    End Sub

    Private Sub EjecutadasCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EjecutadasCheckBox.CheckedChanged
        If Me.EjecutadasCheckBox.CheckState = CheckState.Checked Then
            GloEjecutadas = 1
            Me.PendientesCheckBox.CheckState = CheckState.Unchecked
            Me.VisitaCheckBox.CheckState = CheckState.Unchecked
        Else
            GloEjecutadas = 0
        End If
    End Sub

    Private Sub VisitaCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VisitaCheckBox.CheckedChanged
        If Me.VisitaCheckBox.CheckState = CheckState.Checked Then
            GloVisita = 1
            Me.EjecutadasCheckBox.CheckState = CheckState.Unchecked
            Me.PendientesCheckBox.CheckState = CheckState.Unchecked
        Else
            GloVisita = 0
        End If
    End Sub

    'Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
    '    If Me.ComboBox4.SelectedValue = 1 Then
    '        If IsNumeric(DataGridView1.SelectedCells(0).Value) = True Then
    '            op = CStr(DataGridView1.SelectedCells(0).Value)
    '            Titulo = CStr(DataGridView1.SelectedCells(1).Value)
    '            GloOpRep = op
    '            If op = "0" Then
    '                FrmSelOrdSer.Show()
    '            ElseIf op = "1" Then
    '                FrmSelFechas.Show()
    '            ElseIf op = "2" Then
    '                FrmSelFechas.Show()
    '            ElseIf op = "3" Then
    '                FrmSelTrabajo.Show()
    '            ElseIf op = "4" Then
    '                FrmSelColonia.Show()
    '            End If
    '        End If
    '    ElseIf Me.ComboBox4.SelectedValue = 2 Then
    '        If IsNumeric(DataGridView1.SelectedCells(0).Value) = True Then
    '            op = CStr(DataGridView1.SelectedCells(0).Value)
    '            Titulo = CStr(DataGridView1.SelectedCells(1).Value)
    '            GloOpRep = op
    '            If op = "0" Then
    '                FrmSelOrdSer.Show()
    '            ElseIf op = "1" Then
    '                FrmSelFechas.Show()
    '            ElseIf op = "2" Then
    '                FrmSelFechas.Show()
    '            ElseIf op = "3" Then
    '                FrmSelTrabajo.Show()
    '            ElseIf op = "4" Then
    '                FrmSelColonia.Show()
    '            End If
    '        End If
    '    ElseIf Me.ComboBox4.SelectedValue = 3 Then
    '        If IsNumeric(DataGridView1.SelectedCells(0).Value) = True Then
    '            op = CStr(DataGridView1.SelectedCells(0).Value)
    '            Titulo = CStr(DataGridView1.SelectedCells(1).Value)
    '            GloOpRep = op
    '            If op = "0" Then
    '                FrmSelOrdSer.Show()
    '            ElseIf op = "1" Then
    '                FrmSelFechas.Show()
    '            ElseIf op = "2" Then
    '                FrmSelFechas.Show()
    '            ElseIf op = "3" Then
    '                FrmSelTrabajo.Show()
    '            ElseIf op = "4" Then
    '                FrmSelColonia.Show()
    '            End If
    '        End If
    '    End If
    'End Sub

    'Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)

    'End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.CheckBox1.CheckState = CheckState.Checked Then
            op = "0"
            FrmSelOrdSer.Show()
        Else
            Me.NUMINILbl.Text = 0
            Me.NUMFINLBL.Text = 0
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If Me.CheckBox2.CheckState = CheckState.Checked Then
            op = "1"
            FrmSelFechas.Show()
        Else
            Me.FECSOLFIN.Text = ""
            Me.FECSOLINI.Text = ""
        End If
    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If Me.CheckBox3.CheckState = CheckState.Checked Then
            op = "2"
            FrmSelFechas.Show()
        Else
            Me.FECEJEINI.Text = ""
            Me.FECEJEFIN.Text = ""
        End If
    End Sub

    Private Sub CheckBox4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox4.CheckedChanged
        If Me.CheckBox4.CheckState = CheckState.Checked Then
            op = "3"
            FrmSelTrabajo.Show()
        Else
            Me.CLV_TRABAJO.Text = 0
            Me.NOMTRABAJO.Text = ""
        End If
    End Sub

    Private Sub CheckBox5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox5.CheckedChanged
        If Me.CheckBox5.CheckState = CheckState.Checked Then
            op = "4"
            FrmSelColonia.Show()
        Else
            Me.CLV_COLONIA.Text = 0
            Me.NOMCOLONIA.Text = ""
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        opreporte = 1
        'Me.BackgroundWorker1.RunWorkerAsync()
        'PantallaProcesando.Show()
        ConfigureCrystalReports(0, "")
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opreporte = 2
        'Me.BackgroundWorker1.RunWorkerAsync()
        'PantallaProcesando.Show()
        'ConfigureCrystalReports(1, "")
        ConfigureCrystalReportsNewOrden(1, "")
    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Try
            Select Case opreporte
                Case 1
                    ConfigureCrystalReports(0, "")
                Case 2
                    'ConfigureCrystalReports(1, "")
                    ConfigureCrystalReportsNewOrden(1, "")
            End Select
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        PantallaProcesando.Close()
    End Sub

#Region "Reporte �rdenes de Servicio"
    Private Function uspReporteOrdenesDeServicio(ByVal clvTipSer As Integer, ByVal op1 As Integer, ByVal op2 As Integer, ByVal op3 As Integer, ByVal op4 As Integer, ByVal op5 As Integer,
                                              ByVal statusPen As Boolean, ByVal statusEje As Boolean, ByVal statusVis As Boolean, ByVal clvOrdenIni As Integer, ByVal clvOrdenFin As Integer,
                                              ByVal fecha1Ini As DateTime, ByVal fecha1Fin As DateTime, ByVal fecha2Ini As DateTime, ByVal fecha2Fin As DateTime, ByVal clvTrabajo As Integer,
                                              ByVal clvColonia As Integer, ByVal opOrden As Integer) As DataSet

        Dim tableNameList As New List(Of String)
        tableNameList.Add("CALLES")
        tableNameList.Add("CIUDADES")
        tableNameList.Add("CLIENTES")
        tableNameList.Add("COLONIAS")
        tableNameList.Add("Comentarios_DetalleOrden")
        tableNameList.Add("DetOrdSer")
        tableNameList.Add("OrdSer")
        tableNameList.Add("Rel_Contrato_Macs")
        tableNameList.Add("Rel_Contrato_NoInt")
        tableNameList.Add("ReporteAreaTecnicaOrdSer")
        tableNameList.Add("Servicio_Clientes")
        tableNameList.Add("Trabajos")
        tableNameList.Add("ServiciosActivosClientes")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clvTipSer", SqlDbType.Int, clvTipSer)
        BaseII.CreateMyParameter("@op1", SqlDbType.Int, op1)
        BaseII.CreateMyParameter("@op2", SqlDbType.Int, op2)
        BaseII.CreateMyParameter("@op3", SqlDbType.Int, op3)
        BaseII.CreateMyParameter("@op4", SqlDbType.Int, op4)
        BaseII.CreateMyParameter("@op5", SqlDbType.Int, op5)
        BaseII.CreateMyParameter("@statusPen", SqlDbType.Bit, statusPen)
        BaseII.CreateMyParameter("@statusEje", SqlDbType.Bit, statusEje)
        BaseII.CreateMyParameter("@statusVis", SqlDbType.Bit, statusVis)
        BaseII.CreateMyParameter("@clvOrdenIni", SqlDbType.Int, clvOrdenIni)
        BaseII.CreateMyParameter("@clvOrdenFin", SqlDbType.Int, clvOrdenFin)
        BaseII.CreateMyParameter("@fecha1Ini", SqlDbType.DateTime, fecha1Ini)
        BaseII.CreateMyParameter("@fecha1Fin", SqlDbType.DateTime, fecha1Fin)
        BaseII.CreateMyParameter("@fecha2Ini", SqlDbType.DateTime, fecha2Ini)
        BaseII.CreateMyParameter("@fecha2Fin", SqlDbType.DateTime, fecha2Fin)
        BaseII.CreateMyParameter("@clvTrabajo", SqlDbType.Int, clvTrabajo)
        BaseII.CreateMyParameter("@clvColonia", SqlDbType.Int, clvColonia)
        BaseII.CreateMyParameter("@opOrden", SqlDbType.Int, opOrden)
        Return BaseII.ConsultaDS("uspReporteOrdenesDeServicio", tableNameList)
    End Function
#End Region


#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region
End Class