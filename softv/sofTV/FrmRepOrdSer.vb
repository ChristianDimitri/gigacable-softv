﻿Public Class FrmRepOrdSer

    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        eOpVentas = 0
        Me.Close()
    End Sub

    Private Sub bnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnAceptar.Click

        eMesIni = dtpFechaIni.Value.Month
        eAnioIni = dtpFechaIni.Value.Year
        eMesFin = dtpFechaFin.Value.Month
        eAnioFin = dtpFechaFin.Value.Year
        eTrabajo = ""
        eTitulo = ""

        eTrabajo = cbTrabajos.SelectedValue
        eTitulo = cbTrabajos.Text

        eOpVentas = 81
        FrmImprimirComision.Show()
    End Sub

    Private Sub MUESTRATrabajosReporte(ByVal Op As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, Op)
        cbTrabajos.DataSource = BaseII.ConsultaDT("MUESTRATrabajosReporte")
    End Sub

    Private Sub FrmRepOrdSer_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        MUESTRATrabajosReporte(0)
    End Sub
End Class