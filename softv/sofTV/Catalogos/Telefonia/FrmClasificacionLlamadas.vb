Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports System.IO
Imports System.Xml

Public Class FrmClasificacionLlamadas

    Public diccionario As Dictionary(Of String, Codigos) = New Dictionary(Of String, Codigos)
    Dim listaI As New List(Of Codigos)
    Dim listaD As New List(Of Codigos)
    Dim paises As Codigos = Nothing

    Dim id As Long = 0
    Dim clv_sessionPais As Long = 0
    Dim descripicion As String = ""
    Dim clv_lada As Integer = 0
    Dim LocOpcion As Integer = 0
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub FrmClasificacionLlamadas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'If Me.CodigoPaisCheckBox.Checked = True Then
        '    muestra()
        'End If
    End Sub

    Private Sub FrmClasificacionLlamadas_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        diccionario.Clear()
        listaD.Clear()
        listaI.Clear()
    End Sub

    Private Sub FrmClasificacionLlamadas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If OpcionClasificacionLlamadas = "N" Then
            habilitarCampos()
            CONCLASIFICACIONLLAMADAS()
        ElseIf OpcionClasificacionLlamadas = "M" Then
            ConsultaRelClasificacionPais(ID_ClasificacionLlamadas)
            CONCLASIFICACIONLLAMADAS()
            Carga_Datos()
            habilitarCampos()
           
        ElseIf OpcionClasificacionLlamadas = "C" Then
            ConsultaRelClasificacionPais(ID_ClasificacionLlamadas)
            CONCLASIFICACIONLLAMADAS()
            Carga_Datos()
            bloqueaCampos()
           
        Else
            bloqueaCampos()
        End If
        'dameClvSession()

        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub
    Public Sub Carga_Datos()
        txtNombreClasificacion.Text = NombreClasificacion
        txtDescripcionClasificacion.Text = DescClasificacion
        Me.chbxEs_LD.Checked = es_LD
        TipoLlamadasCombo.SelectedValue = Clv_Clasificacion
        '=
        '=es_LD 
    End Sub
    Public Sub bloqueaCampos()
        Me.gboxNuevaClasificacio.Enabled = False
    End Sub
    Public Sub habilitarCampos()
        Me.gboxNuevaClasificacio.Enabled = True
    End Sub

    Private Sub txtNombreClasificacion_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNombreClasificacion.TextChanged
        If Me.txtNombreClasificacion.Text <> "" Then
            Me.btnGuardar.Enabled = True
        Else
            Me.btnGuardar.Enabled = False
        End If
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click



        If OpcionClasificacionLlamadas = "N" Then
            If Me.validaCampos() Then
                If agrega_clasificacion_llamadas() Then
                    guardaDatos()
                    MsgBox("La Nueva Clasificacion ha sido agregada con �xito: " & Me.txtNombreClasificacion.Text, MsgBoxStyle.Information)
                    diccionario.Clear()
                    listaD.Clear()
                    listaI.Clear()
                    Me.Close()
                Else
                    MsgBox("No se pudo agregar la nueva Clasificaci�n, es probable que ya exista una Clasificaci�n con las m�smas especificaciones", MsgBoxStyle.Exclamation)
                End If
            Else
                MsgBox("Debes especificar un Nombre V�lido para la nueva Clasificaci�n", MsgBoxStyle.Exclamation)
            End If
        ElseIf OpcionClasificacionLlamadas = "M" Then
            If Me.validaCampos() Then
                If modifica_clasificacion_llamadas() Then
                    guardaDatos()
                    MsgBox("La Clasificacion ha sido modificada con �xito: " & Me.txtNombreClasificacion.Text, MsgBoxStyle.Information)
                    diccionario.Clear()
                    listaD.Clear()
                    listaI.Clear()
                    Me.Close()
                Else
                    MsgBox("No se pudo modificar la Clasificaci�n.", MsgBoxStyle.Exclamation)
                End If
            Else
                MsgBox("Debes especificar un Nombre V�lido para la nueva Clasificaci�n", MsgBoxStyle.Exclamation)
            End If
        End If


    End Sub
    Private Sub guardaDatos()
        Dim sw As New StringWriter()
        Dim w As New XmlTextWriter(sw)
        Dim i As Integer
        Dim cod As Codigos

        w.Formatting = Formatting.Indented
        w.WriteStartElement("Codigos")
        w.WriteAttributeString("clasificacion", Me.txtNombreClasificacion.Text)

        For Each cod In listaD
            w.WriteStartElement("Codigo")
            w.WriteAttributeString("lada", cod.lada)
            w.WriteAttributeString("poblacion", cod.Pais)
            w.WriteAttributeString("tipo", cod.tipo)
            w.WriteEndElement()
        Next
        w.WriteEndElement()
        w.Close()
        'sw.ToString()

        Guarda_RelClasificacionPaises(sw.ToString())
    End Sub
    Private Function validaCampos() As Boolean
        If Me.txtNombreClasificacion.Text = "" Then
            Me.txtNombreClasificacion.BackColor = Color.Red
            Return False
        Else
            Return True
        End If
    End Function
    Private Function agrega_clasificacion_llamadas() As Boolean

        Dim valido As Integer = 0
        Dim regresar As Boolean = False

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()
        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "sp_agrega_clasificacion_tipo_llamadas"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 8

                Dim prmNombre As New SqlParameter("@nombre_clasificacion", SqlDbType.VarChar, 250)
                prmNombre.Value = Me.txtNombreClasificacion.Text
                .Parameters.Add(prmNombre)

                Dim prmDescripcion As New SqlParameter("@descripcion", SqlDbType.VarChar, 250)
                If Me.txtDescripcionClasificacion.Text = "" Then
                    Me.txtDescripcionClasificacion.Text = ""
                End If
                prmDescripcion.Value = Me.txtDescripcionClasificacion.Text
                .Parameters.Add(prmDescripcion)

                Dim prmEsLargaDistancia As New SqlParameter("@es_LD", SqlDbType.VarChar, 250)
                If Me.chbxEs_LD.CheckState = CheckState.Checked Then
                    prmEsLargaDistancia.Value = 1
                Else
                    prmEsLargaDistancia.Value = 0
                End If
                .Parameters.Add(prmEsLargaDistancia)

                Dim prmClvClasificacion As New SqlParameter("@Clv_Clasificacion", SqlDbType.Int)
                prmClvClasificacion.Direction = ParameterDirection.Input
                prmClvClasificacion.Value = Me.TipoLlamadasCombo.SelectedValue
                CMD.Parameters.Add(prmClvClasificacion)

                Dim prmValido As New SqlParameter("@valido", SqlDbType.Int)
                prmValido.Direction = ParameterDirection.Output
                prmValido.Value = 0
                .Parameters.Add(prmValido)

                Dim i As Integer = .ExecuteNonQuery()

                valido = prmValido.Value
                If valido = 1 Then
                    Return True

                Else
                    Return False
                End If
            End With
            CON80.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function
    Private Function modifica_clasificacion_llamadas() As Boolean

        Dim regresar As Boolean = False

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()
        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "sp_modifica_clasificacion_llamadas"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 8

                Dim prmID As New SqlParameter("@id", SqlDbType.Int, 50)
                prmID.Value = ID_ClasificacionLlamadas
                .Parameters.Add(prmID)

                Dim prmNombre As New SqlParameter("@nombre", SqlDbType.VarChar, 250)
                prmNombre.Value = Me.txtNombreClasificacion.Text
                .Parameters.Add(prmNombre)

                Dim prmES_LD As New SqlParameter("@es_LD", SqlDbType.Bit)
                If Me.chbxEs_LD.CheckState = CheckState.Checked Then
                    prmES_LD.Value = True
                Else
                    prmES_LD.Value = False
                End If
                .Parameters.Add(prmES_LD)

                Dim prmDescripcion As New SqlParameter("@descripcion", SqlDbType.VarChar, 250)
                If Me.txtDescripcionClasificacion.Text = "" Then
                    Me.txtDescripcionClasificacion.Text = ""
                End If
                prmDescripcion.Value = Me.txtDescripcionClasificacion.Text
                .Parameters.Add(prmDescripcion)

                Dim prmClvClasificacion As New SqlParameter("@Clv_Clasificacion", SqlDbType.Int)
                prmClvClasificacion.Direction = ParameterDirection.Input
                prmClvClasificacion.Value = Me.TipoLlamadasCombo.SelectedValue
                CMD.Parameters.Add(prmClvClasificacion)

                Dim i As Integer = .ExecuteNonQuery()

                Return True
            End With
            CON80.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try
    End Function
    'Private Sub MuestraCodigosMexico2(ByVal lada As String, ByVal op As Integer)
    '    Dim I As Integer = 0
    '    Dim Fila2 As DataRow
    '    Dim conexion As New SqlConnection(MiConexion)
    '    Dim strSQL As New StringBuilder
    '    Dim oDataSet As New DataSet
    '    LocOpcion = op
    '    Try
    '        Dim cmd As New SqlCommand("MuestraCodigosMexico", conexion)
    '        cmd.CommandType = CommandType.StoredProcedure
    '        cmd.Parameters.AddWithValue("@lada", CStr(lada))
    '        cmd.Parameters.AddWithValue("@op", CStr(op))

    '        Dim listacodigos As New List(Of Codigos)

    '        Dim dr As SqlDataReader
    '        conexion.Open()
    '        dr = cmd.ExecuteReader()
    '        'diccionario.Clear()
    '        listaI.Clear()

    '        Using dr
    '            While dr.Read
    '                If diccionario.ContainsKey(dr("id").ToString().Trim()) Then
    '                    If Not listaI.Contains(diccionario(dr("id").ToString().Trim())) Then
    '                        listaD.Add(diccionario(dr("id").ToString().Trim()))
    '                    End If

    '                Else
    '                    paises = New Codigos()
    '                    paises.id = dr("id").ToString().Trim()
    '                    paises.lada = dr("lada").ToString().Trim()
    '                    paises.Pais = dr("poblacion").ToString().Trim()
    '                    paises.tipo = dr("tipo").ToString().Trim()
    '                    diccionario.Add(paises.id, paises)
    '                    listaD.Add(paises)
    '                End If
    '            End While
    '        End Using
    '        Me.ListBox1.DataSource = Nothing
    '        Me.ListBox1.DataSource = listaI
    '        Me.ListBox1.DisplayMember = "NombreLada"
    '        Me.ListBox1.ValueMember = "id"




    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Exclamation)
    '    Finally
    '        conexion.Close()
    '        conexion.Dispose()
    '    End Try
    'End Sub

    Private Sub MuestraCodigosMexico(ByVal lada As String, ByVal op As Integer, ByVal MClv_Clasificacion As Integer)

        Dim I As Integer = 0
        Dim Fila2 As DataRow
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        Dim oDataSet As New DataSet
        LocOpcion = op
        Try
            Dim cmd As New SqlCommand("MuestraCodigosMexico_PorClasificacion", conexion)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@lada", CStr(lada))
            cmd.Parameters.AddWithValue("@op", CStr(op))
            cmd.Parameters.AddWithValue("@Clv_Clasificacion", MClv_Clasificacion)

            Dim listacodigos As New List(Of Codigos)

            Dim dr As SqlDataReader
            conexion.Open()
            dr = cmd.ExecuteReader()
            'diccionario.Clear()
            listaI.Clear()

            Using dr
                While dr.Read
                    If diccionario.ContainsKey(dr("id").ToString().Trim()) Then
                        If Not listaD.Contains(diccionario(dr("id").ToString().Trim())) Then
                            listaI.Add(diccionario(dr("id").ToString().Trim()))
                        End If

                    Else
                        paises = New Codigos()
                        paises.id = dr("id").ToString().Trim()
                        paises.lada = dr("lada").ToString().Trim()
                        paises.Pais = dr("poblacion").ToString().Trim()
                        paises.tipo = dr("tipo").ToString().Trim()
                        diccionario.Add(paises.id, paises)
                        listaI.Add(paises)
                    End If
                End While
            End Using
            Me.ListBox1.DataSource = Nothing
            Me.ListBox1.DataSource = listaI
            Me.ListBox1.DisplayMember = "NombreLada"
            Me.ListBox1.ValueMember = "id"

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub ConsultaRelClasificacionPais(ByVal clave As Integer)

        Dim I As Integer = 0
        Dim Fila2 As DataRow
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        Dim oDataSet As New DataSet

        Try
            Dim cmd As New SqlCommand("ConsultaRelClasificacionPais", conexion)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@clv_clasificacion", CStr(clave))


            Dim dr As SqlDataReader
            conexion.Open()
            dr = cmd.ExecuteReader()
            'diccionario.Clear()
            listaD.Clear()
            Using dr
                While dr.Read

                    paises = New Codigos()
                    paises.id = dr("id").ToString().Trim()
                    paises.lada = dr("lada").ToString().Trim()
                    paises.Pais = dr("poblacion").ToString().Trim()
                    paises.tipo = dr("tipo").ToString().Trim()
                    diccionario.Add(paises.id, paises)
                    listaD.Add(paises)
                End While
            End Using

            Me.ListBox2.DataSource = Nothing
            Me.ListBox2.DataSource = listaD
            Me.ListBox2.DisplayMember = "NombreLada"
            Me.ListBox2.ValueMember = "id"
            Me.Refresh()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BuscarButton.Click
        'muestra()
        If IsNumeric(Me.TipoLlamadasCombo.SelectedValue) = True Then
            'If Me.TipoLlamadasCombo.SelectedValue = 1 Or Me.TipoLlamadasCombo.SelectedValue = 2 Or Me.TipoLlamadasCombo.SelectedValue = 3 Or Me.TipoLlamadasCombo.SelectedValue = 4 Then
            '    'listaD.Clear()
            '    'ListBox1.Refresh()
            MuestraCodigosMexico(Me.LadaTextBox.Text, Me.TipoLlamadasCombo.SelectedValue, Me.TipoLlamadasCombo.SelectedValue)
            '    'muestra()
            'ElseIf Me.TipoLlamadasCombo.SelectedValue = 5 Or Me.TipoLlamadasCombo.SelectedValue = 6 Then
            '    'listaD.Clear()
            '    'ListBox1.Refresh()
            '    MuestraCodigosMexico(Me.LadaTextBox.Text, 2, Me.TipoLlamadasCombo.SelectedValue)
            '    'muestra()
            'ElseIf Me.TipoLlamadasCombo.SelectedValue = 7 Then
            '    listaD.Clear()
            '    ListBox1.DataSource = Nothing
            '    ListBox1.DataSource = listaD
            'End If
        End If

    End Sub


    'Private Sub CodigoPaisCheckBox_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CodigoPaisCheckBox.CheckStateChanged
    '    If Me.CodigMxCheckBox.Checked = True Then
    '        Me.CodigMxCheckBox.Checked = False
    '    End If

    '    If Me.CodigoPaisCheckBox.CheckState = CheckState.Checked Then
    '        'muestra()
    '        'Else
    '        '    listaD.Clear()
    '        '    ListBox1.Refresh()
    '    End If

    'End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click

        'If OpcionClasificacionLlamadas = "M" Then
        If Me.ListBox1.SelectedValue <> Nothing Then

            For Each paises In ListBox1.SelectedItems
                listaD.Add(paises)
                listaI.Remove(paises)
            Next

            enlase()
        End If
        ''End If

        'If Me.ListBox1.SelectedValue <> Nothing Then
        '    listaI.Add(diccionario(ListBox1.SelectedValue.ToString()))
        '    listaD.Remove(diccionario(ListBox1.SelectedValue.ToString()))

        '    enlase()
        'End If

    End Sub


    'Private Sub muestra()

    '    If Me.CodigMxCheckBox.Checked = True Then
    '        If Me.TodosCheck.Checked = True Then
    '            MuestraCodigosMexico("", 3)
    '        Else
    '            MuestraCodigosMexico(Me.LadaTextBox.Text, 1)
    '        End If
    '    ElseIf Me.CodigoPaisCheckBox.Checked = True Then
    '        If Me.TodosCheck.Checked = True Then
    '            MuestraCodigosMexico(Me.LadaTextBox.Text, 4)
    '        Else
    '            MuestraCodigosMexico(Me.LadaTextBox.Text, 2)
    '        End If

    '    End If
    '    Me.LadaTextBox.Text = ""
    'End Sub
    'Private Sub muestra2()

    '    If Me.CodigMxCheckBox.Checked = True Then
    '        If Me.TodosCheck.Checked = True Then
    '            MuestraCodigosMexico("", 3)
    '        Else
    '            MuestraCodigosMexico(Me.TextBox1.Text, 1)
    '        End If
    '    ElseIf Me.CodigoPaisCheckBox.Checked = True Then
    '        If Me.TodosCheck.Checked = True Then
    '            MuestraCodigosMexico(Me.TextBox1.Text, 4)
    '        Else
    '            MuestraCodigosMexico(Me.TextBox1.Text, 2)
    '        End If

    '    End If
    '    Me.TextBox1.Text = ""
    'End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        'If OpcionClasificacionLlamadas = "M" Then
        'Dim i As ListBox
        If Me.ListBox2.SelectedValue <> Nothing Then

            For Each paises In ListBox2.SelectedItems
                listaI.Add(paises)
                listaD.Remove(paises)
            Next

            enlase()
        End If

        'End If
        'If Me.ListBox2.SelectedValue <> Nothing Then
        '    listaI.Add(diccionario(ListBox2.SelectedValue.ToString()))
        '    listaD.Remove(diccionario(ListBox2.SelectedValue.ToString()))

        '    enlase()
        'End If

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click

        If ListBox2.Items.Count > 0 Then

            For Each paises In listaD
                listaI.Add(paises)
            Next
            listaD.Clear()
            enlase()
        End If

    End Sub
    Private Sub Guarda_RelClasificacionPaises(ByVal x As String)
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Guarda_RelClasificacionPaises", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@x", SqlDbType.Xml)
        par1.Direction = ParameterDirection.Input
        par1.Value = x
        com.Parameters.Add(par1)
        Try
            con.Open()
            com.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try

    End Sub

    Private Sub CONCLASIFICACIONLLAMADAS()

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder

        strSQL.Append("EXEC CONCLASIFICACIONLLAMADAS ")

        Dim dataTable As New DataTable
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            TipoLlamadasCombo.DataSource = bindingSource.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If ListBox1.Items.Count > 0 Then

            For Each paises In listaI
                listaD.Add(paises)
            Next
            listaI.Clear()
            enlase()
        End If
    End Sub
    Public Sub enlase()

        ListBox2.DataSource = Nothing
        ListBox2.DataSource = listaD
        ListBox2.DisplayMember = "NombreLada"
        ListBox2.ValueMember = "id"
        ListBox1.DataSource = Nothing
        ListBox1.DataSource = listaI
        ListBox1.DisplayMember = "NombreLada"
        ListBox1.ValueMember = "id"
        'ListBox1.Refresh()

    End Sub




    'Private Sub CodigMxCheckBox_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CodigMxCheckBox.CheckStateChanged
    '    If Me.CodigoPaisCheckBox.Checked = True Then
    '        Me.CodigoPaisCheckBox.Checked = False
    '    End If

    '    If CodigMxCheckBox.CheckState = CheckState.Checked Then
    '        muestra()
    '    Else
    '        listaD.Clear()
    '        ListBox1.Refresh()
    '    End If
    'End Sub

   

    Private Sub TipoLlamadasCombo_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TipoLlamadasCombo.SelectedValueChanged

        If IsNumeric(Me.TipoLlamadasCombo.SelectedValue) = True Then
            'If Me.TipoLlamadasCombo.SelectedValue = 1 Or Me.TipoLlamadasCombo.SelectedValue = 2 Or Me.TipoLlamadasCombo.SelectedValue = 3 Or Me.TipoLlamadasCombo.SelectedValue = 4 Then
            '    'listaD.Clear()
            'ListBox1.Refresh()
            MuestraCodigosMexico(Me.LadaTextBox.Text, Me.TipoLlamadasCombo.SelectedValue, Me.TipoLlamadasCombo.SelectedValue)
            'muestra()
            'ElseIf Me.TipoLlamadasCombo.SelectedValue = 5 Or Me.TipoLlamadasCombo.SelectedValue = 6 Then
            '    'listaD.Clear()
            '    'ListBox1.Refresh()
            '    MuestraCodigosMexico(Me.LadaTextBox.Text, 4, Me.TipoLlamadasCombo.SelectedValue)
            '    'muestra()
            'ElseIf Me.TipoLlamadasCombo.SelectedValue = 7 Then
            '    listaD.Clear()
            '    ListBox1.DataSource = Nothing
            '    ListBox1.DataSource = listaD
            'End If

        End If
    End Sub

    Private Sub TipoLlamadasCombo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipoLlamadasCombo.SelectedIndexChanged

    End Sub

#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region
End Class
Public Class Codigos
    Dim _nombre As String
    Dim _lada As String
    Dim _id As String
    Dim _tipo As String
    Public Property id() As String
        Get
            Return _id
        End Get
        Set(ByVal Value As String)
            _id = Value
        End Set
    End Property
    Public Property Pais() As String
        Get
            Return _nombre
        End Get
        Set(ByVal Value As String)
            _nombre = Value
        End Set
    End Property
    Public Property lada() As String
        Get
            Return _lada
        End Get
        Set(ByVal Value As String)
            _lada = Value
        End Set
    End Property
    Public Property tipo() As String
        Get
            Return _tipo
        End Get
        Set(ByVal Value As String)
            _tipo = Value
        End Set
    End Property
    Public ReadOnly Property NombreLada() As String
        Get
            Return _lada + " _ " + _nombre
        End Get
    End Property

    Public Sub Codigos(ByVal id As String, ByVal pais As String, ByVal lada As String, ByVal tipo As String)
        Me.id = id
        Me.Pais = pais
        Me.lada = lada
        Me.tipo = tipo
    End Sub



End Class