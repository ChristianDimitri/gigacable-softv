Imports System.Data.SqlClient
Public Class BrwClasificacionLlamadas
    Dim ID_eliminando As Integer = 0
    Private Sub BrwClasificacionLlamadas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Try
            If Me.txtID.Text = "" And Me.txtNombre.Text = "" Then
                cargaReales()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub BrwClasificacionLlamadas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        cargaReales()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub
    Public Sub cargaReales()
        Try
            Dim conn As New SqlConnection(MiConexion)
            conn.Open()
            Dim comando As New SqlClient.SqlCommand("sp_carga_clasificacion_llamadas", conn)
            comando.CommandType = CommandType.StoredProcedure
            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            'Agrego el parámetro
            Adaptador.SelectCommand.Parameters.Add("@op", SqlDbType.SmallInt).Value = 1

            Dim Dataset As New DataSet
            'Creamos nuestros BidingSource
            Dim Bs As New BindingSource
            'Llenamos el Adaptador con el DataSet
            Adaptador.Fill(Dataset, "resultadoClasificacion")
            Bs.DataSource = Dataset.Tables("resultadoClasificacion")
            'dgvResultadosClasificacion.DataSource = Bs
            Me.dgvResultadosClasificacion.DataSource = Bs
            conn.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        OpcionClasificacionLlamadas = "N"
        FrmClasificacionLlamadas.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If (Me.dgvResultadosClasificacion.RowCount) > 0 Then
            OpcionClasificacionLlamadas = "M"
            FrmClasificacionLlamadas.Show()
        End If
       
    End Sub

    Private Sub dgvResultadosClasificacion_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvResultadosClasificacion.CurrentCellChanged
        Try
        
            ID_ClasificacionLlamadas = Me.dgvResultadosClasificacion.CurrentRow.Cells(0).Value
            NombreClasificacion = Me.dgvResultadosClasificacion.CurrentRow.Cells(1).Value
            DescClasificacion = Me.dgvResultadosClasificacion.CurrentRow.Cells(2).Value
            es_LD = Me.dgvResultadosClasificacion.CurrentRow.Cells(3).Value
            Clv_Clasificacion = Me.dgvResultadosClasificacion.CurrentRow.Cells(4).Value


        Catch ex As Exception
        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click

        Dim resp As Integer = 0
        If (Me.dgvResultadosClasificacion.RowCount) > 0 Then
            ID_eliminando = Me.dgvResultadosClasificacion.CurrentRow.Cells.Item(0).Value
            resp = MsgBox("¿Confirmas que deseas borrar la Clasificación: " & Me.dgvResultadosClasificacion.CurrentRow.Cells.Item(1).Value.ToString & " con ID " & Me.dgvResultadosClasificacion.CurrentRow.Cells.Item(0).Value.ToString & "?", MsgBoxStyle.OkCancel)
            If resp = 1 Then
                If eliminar_clasificacion_llamadas() = True Then

                    MsgBox("La Clasificación " & Me.dgvResultadosClasificacion.CurrentRow.Cells(1).Value.ToString & " se ha eliminado correctamente.", MsgBoxStyle.Information)
                Else
                    MsgBox("No se ha eliminado, la Clasificación por que está asignada con alguna tarifa de cobro.", MsgBoxStyle.Exclamation)
                End If
                cargaReales()
            End If
        End If

        OpcionClasificacionLlamadas = ""
    End Sub
    Private Function eliminar_clasificacion_llamadas() As Boolean

        Dim Resp As Integer = 8
        Dim regresar As Boolean = False

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()
        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "sp_elimina_clasificacion_llamadas"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 8

                Dim prmID As New SqlParameter("@id", SqlDbType.Int, 50)
                prmID.Value = ID_eliminando
                .Parameters.Add(prmID)

                Dim prmResp As New SqlParameter("@resp", SqlDbType.Int, 50)
                prmResp.Direction = ParameterDirection.Output
                prmResp.Value = 0
                .Parameters.Add(prmResp)

                Dim i As Integer = .ExecuteNonQuery()

                Resp = prmResp.Value

                If Resp = 1 Then
                    Return True
                ElseIf Resp = 0 Then
                    Return False
                End If

            End With
            CON80.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try
    End Function

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        busca_Clasificaciones()
    End Sub
    Private Sub busca_Clasificaciones()

        Try
            Dim conn As New SqlConnection(MiConexion)
            conn.Open()
            Dim comando As New SqlClient.SqlCommand("sp_busca_clasificacion_llamadas", conn)
            comando.CommandType = CommandType.StoredProcedure

            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando



            If Me.txtID.Text <> "" Then
                Adaptador.SelectCommand.Parameters.Add("@ID", SqlDbType.VarChar, 250).Value = Me.txtID.Text
            Else
                Adaptador.SelectCommand.Parameters.Add("@ID", SqlDbType.VarChar, 250).Value = ""
            End If

            If Me.txtNombre.Text <> "" Then
                Adaptador.SelectCommand.Parameters.Add("@Nombre", SqlDbType.VarChar, 250).Value = Me.txtNombre.Text
            Else
                Adaptador.SelectCommand.Parameters.Add("@Nombre", SqlDbType.VarChar, 250).Value = ""
            End If

            If Me.chbxES_LD.Checked = True Then
                Adaptador.SelectCommand.Parameters.Add("@es_LD", SqlDbType.Int, 50).Value = 1
            Else
                Adaptador.SelectCommand.Parameters.Add("@es_LD", SqlDbType.Int, 50).Value = 0
            End If

            Dim Dataset As New DataSet
            Dim Bs As New BindingSource
            Adaptador.Fill(Dataset, "resultado_Clasificaciones")
            Bs.DataSource = Dataset.Tables("resultado_Clasificaciones")
            Me.dgvResultadosClasificacion.DataSource = Bs

            conn.Close()
        Catch ex As Exception
            Exit Sub
        End Try
        'Dim Resp As Integer = 8
        'Dim regresar As Boolean = False

        'Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        'Dim CMD As New SqlClient.SqlCommand()
        'Try
        '    CMD = New SqlClient.SqlCommand()
        '    CON80.Open()
        '    With CMD
        '        .CommandText = "sp_busca_clasificacion_llamadas"
        '        .CommandType = CommandType.StoredProcedure
        '        .Connection = CON80
        '        .CommandTimeout = 8

        '        Dim prmID As New SqlParameter("@ID", SqlDbType.VarChar, 250)
        '        If Me.txtID.Text <> "" Then
        '            prmID.Value = Me.txtID.Text
        '        Else
        '            prmID.Value = ""

        '        End If

        '        .Parameters.Add(prmID)

        '        Dim prmNombre As New SqlParameter("@nombre", SqlDbType.VarChar, 250)
        '        prmNombre.Value = Me.txtNombre.Text
        '        .Parameters.Add(prmNombre)

        '        Dim prmES_LD As New SqlParameter("@es_LD", SqlDbType.Int, 50)
        '        If Me.chbxES_LD.Checked = True Then
        '            prmES_LD.Value = 1
        '        Else
        '            prmES_LD.Value = 0
        '        End If

        '        .Parameters.Add(prmES_LD)


        '        Dim i As Integer = .ExecuteNonQuery()

        '    End With
        '    CON80.Close()
        'Catch ex As Exception
        '    MsgBox(ex.Message)

        'End Try
    End Sub

#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region
End Class