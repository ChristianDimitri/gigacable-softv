﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMTAsAsignacionNumTel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxMTA = New System.Windows.Forms.GroupBox()
        Me.txtMAC = New System.Windows.Forms.TextBox()
        Me.lblMAC = New System.Windows.Forms.Label()
        Me.lblNumerosDisponibles = New System.Windows.Forms.Label()
        Me.cmbNumerosDisponibles = New System.Windows.Forms.ComboBox()
        Me.txtMACMTA = New System.Windows.Forms.TextBox()
        Me.txtIDAparato = New System.Windows.Forms.TextBox()
        Me.lblIDAparato = New System.Windows.Forms.Label()
        Me.lblMACMTA = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.gbxAsignacionDeNumeros = New System.Windows.Forms.GroupBox()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.dgvNumerosAsignados = New System.Windows.Forms.DataGridView()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.gbxMTA.SuspendLayout()
        Me.gbxAsignacionDeNumeros.SuspendLayout()
        CType(Me.dgvNumerosAsignados, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbxMTA
        '
        Me.gbxMTA.Controls.Add(Me.txtMAC)
        Me.gbxMTA.Controls.Add(Me.lblMAC)
        Me.gbxMTA.Controls.Add(Me.lblNumerosDisponibles)
        Me.gbxMTA.Controls.Add(Me.cmbNumerosDisponibles)
        Me.gbxMTA.Controls.Add(Me.txtMACMTA)
        Me.gbxMTA.Controls.Add(Me.txtIDAparato)
        Me.gbxMTA.Controls.Add(Me.lblIDAparato)
        Me.gbxMTA.Controls.Add(Me.lblMACMTA)
        Me.gbxMTA.Location = New System.Drawing.Point(12, 2)
        Me.gbxMTA.Name = "gbxMTA"
        Me.gbxMTA.Size = New System.Drawing.Size(820, 111)
        Me.gbxMTA.TabIndex = 0
        Me.gbxMTA.TabStop = False
        '
        'txtMAC
        '
        Me.txtMAC.BackColor = System.Drawing.Color.White
        Me.txtMAC.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMAC.Location = New System.Drawing.Point(192, 82)
        Me.txtMAC.Name = "txtMAC"
        Me.txtMAC.ReadOnly = True
        Me.txtMAC.Size = New System.Drawing.Size(266, 22)
        Me.txtMAC.TabIndex = 127
        Me.txtMAC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblMAC
        '
        Me.lblMAC.AutoSize = True
        Me.lblMAC.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMAC.Location = New System.Drawing.Point(313, 64)
        Me.lblMAC.Name = "lblMAC"
        Me.lblMAC.Size = New System.Drawing.Size(31, 15)
        Me.lblMAC.TabIndex = 126
        Me.lblMAC.Text = "ATA"
        '
        'lblNumerosDisponibles
        '
        Me.lblNumerosDisponibles.AutoSize = True
        Me.lblNumerosDisponibles.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumerosDisponibles.Location = New System.Drawing.Point(498, 38)
        Me.lblNumerosDisponibles.Name = "lblNumerosDisponibles"
        Me.lblNumerosDisponibles.Size = New System.Drawing.Size(145, 15)
        Me.lblNumerosDisponibles.TabIndex = 125
        Me.lblNumerosDisponibles.Text = "Números Disponibles"
        '
        'cmbNumerosDisponibles
        '
        Me.cmbNumerosDisponibles.FormattingEnabled = True
        Me.cmbNumerosDisponibles.Location = New System.Drawing.Point(501, 54)
        Me.cmbNumerosDisponibles.Name = "cmbNumerosDisponibles"
        Me.cmbNumerosDisponibles.Size = New System.Drawing.Size(300, 21)
        Me.cmbNumerosDisponibles.TabIndex = 124
        '
        'txtMACMTA
        '
        Me.txtMACMTA.BackColor = System.Drawing.Color.White
        Me.txtMACMTA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMACMTA.Location = New System.Drawing.Point(192, 30)
        Me.txtMACMTA.Name = "txtMACMTA"
        Me.txtMACMTA.ReadOnly = True
        Me.txtMACMTA.Size = New System.Drawing.Size(266, 22)
        Me.txtMACMTA.TabIndex = 123
        Me.txtMACMTA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtIDAparato
        '
        Me.txtIDAparato.BackColor = System.Drawing.Color.White
        Me.txtIDAparato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIDAparato.Location = New System.Drawing.Point(17, 52)
        Me.txtIDAparato.Name = "txtIDAparato"
        Me.txtIDAparato.ReadOnly = True
        Me.txtIDAparato.Size = New System.Drawing.Size(126, 22)
        Me.txtIDAparato.TabIndex = 122
        Me.txtIDAparato.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblIDAparato
        '
        Me.lblIDAparato.AutoSize = True
        Me.lblIDAparato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIDAparato.Location = New System.Drawing.Point(40, 34)
        Me.lblIDAparato.Name = "lblIDAparato"
        Me.lblIDAparato.Size = New System.Drawing.Size(74, 15)
        Me.lblIDAparato.TabIndex = 120
        Me.lblIDAparato.Text = "ID Aparato"
        '
        'lblMACMTA
        '
        Me.lblMACMTA.AutoSize = True
        Me.lblMACMTA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMACMTA.Location = New System.Drawing.Point(298, 14)
        Me.lblMACMTA.Name = "lblMACMTA"
        Me.lblMACMTA.Size = New System.Drawing.Size(64, 15)
        Me.lblMACMTA.TabIndex = 121
        Me.lblMACMTA.Text = "MAC ATA"
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(704, 359)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(100, 40)
        Me.btnSalir.TabIndex = 118
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'gbxAsignacionDeNumeros
        '
        Me.gbxAsignacionDeNumeros.Controls.Add(Me.btnEliminar)
        Me.gbxAsignacionDeNumeros.Controls.Add(Me.btnSalir)
        Me.gbxAsignacionDeNumeros.Controls.Add(Me.dgvNumerosAsignados)
        Me.gbxAsignacionDeNumeros.Controls.Add(Me.btnAgregar)
        Me.gbxAsignacionDeNumeros.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxAsignacionDeNumeros.Location = New System.Drawing.Point(12, 119)
        Me.gbxAsignacionDeNumeros.Name = "gbxAsignacionDeNumeros"
        Me.gbxAsignacionDeNumeros.Size = New System.Drawing.Size(820, 421)
        Me.gbxAsignacionDeNumeros.TabIndex = 119
        Me.gbxAsignacionDeNumeros.TabStop = False
        Me.gbxAsignacionDeNumeros.Text = "Listado de números asignados"
        '
        'btnEliminar
        '
        Me.btnEliminar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar.ForeColor = System.Drawing.Color.Black
        Me.btnEliminar.Location = New System.Drawing.Point(698, 71)
        Me.btnEliminar.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(100, 40)
        Me.btnEliminar.TabIndex = 122
        Me.btnEliminar.Text = "&ELIMINAR"
        Me.btnEliminar.UseVisualStyleBackColor = False
        '
        'dgvNumerosAsignados
        '
        Me.dgvNumerosAsignados.AllowUserToAddRows = False
        Me.dgvNumerosAsignados.AllowUserToDeleteRows = False
        Me.dgvNumerosAsignados.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvNumerosAsignados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvNumerosAsignados.Location = New System.Drawing.Point(17, 20)
        Me.dgvNumerosAsignados.Name = "dgvNumerosAsignados"
        Me.dgvNumerosAsignados.ReadOnly = True
        Me.dgvNumerosAsignados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvNumerosAsignados.Size = New System.Drawing.Size(662, 382)
        Me.dgvNumerosAsignados.TabIndex = 121
        '
        'btnAgregar
        '
        Me.btnAgregar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar.ForeColor = System.Drawing.Color.Black
        Me.btnAgregar.Location = New System.Drawing.Point(698, 20)
        Me.btnAgregar.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(100, 40)
        Me.btnAgregar.TabIndex = 120
        Me.btnAgregar.Text = "&AGREGAR"
        Me.btnAgregar.UseVisualStyleBackColor = False
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmMTAsAsignacionNumTel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(844, 545)
        Me.Controls.Add(Me.gbxAsignacionDeNumeros)
        Me.Controls.Add(Me.gbxMTA)
        Me.Name = "FrmMTAsAsignacionNumTel"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Asignacion de número(s) de teléfono"
        Me.gbxMTA.ResumeLayout(False)
        Me.gbxMTA.PerformLayout()
        Me.gbxAsignacionDeNumeros.ResumeLayout(False)
        CType(Me.dgvNumerosAsignados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxMTA As System.Windows.Forms.GroupBox
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents lblNumerosDisponibles As System.Windows.Forms.Label
    Friend WithEvents cmbNumerosDisponibles As System.Windows.Forms.ComboBox
    Friend WithEvents txtMACMTA As System.Windows.Forms.TextBox
    Friend WithEvents txtIDAparato As System.Windows.Forms.TextBox
    Friend WithEvents lblIDAparato As System.Windows.Forms.Label
    Friend WithEvents lblMACMTA As System.Windows.Forms.Label
    Friend WithEvents gbxAsignacionDeNumeros As System.Windows.Forms.GroupBox
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents dgvNumerosAsignados As System.Windows.Forms.DataGridView
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents txtMAC As System.Windows.Forms.TextBox
    Friend WithEvents lblMAC As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
