Imports System.Data.SqlClient
Imports System.Diagnostics
Imports Microsoft.VisualBasic
Imports System.Windows.Forms
Public Class FrmServiciosTelefonia
    Dim ban As Boolean = False
    Dim trazo As TraceListener
    Dim Clv_ServicioDigital As Integer
    Dim Tipo_Servicio_Tel As String
    Dim Tipo_Serv_Res As String

    Dim Paquete As New DAPaqueteAdicional
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
    Private Sub Guarda_Lineas(ByVal opx As Integer)
        Dim ConLineas As New SqlConnection(MiConexion)
        Dim CMd As New SqlCommand
        ConLineas.Open()
        trazo = New TextWriterTraceListener("...\Res.log")

        Try
            With CMd
                '[Guarda_RelPlanLineas](@Clv_servicio int,@Lineas int,@opc int)
                .CommandText = "Guarda_RelPlanLineas"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = ConLineas
                Dim pmt As New SqlParameter("@clv_servicio", SqlDbType.BigInt)
                Dim Pmt2 As New SqlParameter("@Lineas", SqlDbType.Int)
                Dim Pmt3 As New SqlParameter("@opc", SqlDbType.Int)

                pmt.Direction = ParameterDirection.Input
                Pmt2.Direction = ParameterDirection.InputOutput
                Pmt3.Direction = ParameterDirection.Input

                pmt.Value = Me.Clv_ServicioTextBox.Text
                Pmt2.Value = Me.NumericUpDown5.Value
                Pmt3.Value = opx

                .Parameters.Add(pmt)
                .Parameters.Add(Pmt2)
                .Parameters.Add(Pmt3)

                .ExecuteNonQuery()
                If opx = 3 Then
                    Me.NumericUpDown5.Value = Pmt2.Value
                End If

            End With
            ConLineas.Close()
        Catch ex As Exception
            trazo.WriteLine("Error ")
            trazo.WriteLine(ex.Message)
            trazo.Flush()
        End Try

    End Sub
    Private Sub CONSERVICIOSBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONSERVICIOSBindingNavigatorSaveItem.Click
        DimeSi_ServicioTieneMedios_Telmex()

        If tieneMedios = True And EsTelmex_CheckBox.Checked = False Then

            Dim Respuesta = MsgBox("�Est� seguro de que desea que el servicio DEJE DE SER Servicio de Telmex? Al aceptar, se eliminar� la relaci�n con los medios de este servicio", MsgBoxStyle.YesNoCancel, "�Eliminar relaci�n del medio con el servicio?")

            If Respuesta = MsgBoxResult.No Then
                EsTelmex_CheckBox.Checked = True
            End If

        End If


        Dim Cone7 As New SqlClient.SqlConnection(MiConexion)
        If Me.DescripcionTextBox.Text.Trim.Length = 0 Then
            MsgBox("Se Debe Capturar una Descripci�n del Servicio", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.Clv_TxtTextBox.Text.Trim.Length = 0 Then
            MsgBox("Se Debe Capturar una Clave para el Servicio", MsgBoxStyle.Information)
            Exit Sub
        End If
        If (Me.Sale_en_CarteraCheckBox1.Checked = True) Then
            If (Me.CheckBox9.Checked = False And Me.CheckBox10.Checked = False) Then
                MsgBox("Se Debe de Seleccionar un Tipo de Servicio", MsgBoxStyle.Information)
                Exit Sub
            End If
            Agrega_Tipo_Servicio_Telefonia()
        Else
            Elimina_Tipo_Servicio_Telefonia()
        End If
        

        If Me.Genera_OrdenCheckBox.CheckState <> CheckState.Checked Then
            BorrarRel_Trabajos_NoCobroMensual()
        Else
            GUARDARel_Trabajos_NoCobroMensualGUARDA()
        End If
        Me.Validate()
        Cone7.Open()
        Me.CONSERVICIOSBindingSource.EndEdit()
        Me.CONSERVICIOSTableAdapter.Connection = Cone7
        Me.CONSERVICIOSTableAdapter.Update(Me.DataSetLidia2.CONSERVICIOS)
        Cone7.Close()
        Guarda_Lineas(0)

        Dim ClvServ As Long = CInt(Me.Clv_ServicioTextBox.Text)

        'Telmex 
        If GloClv_TipSer = 5 Then 'Or GloClv_TipSer = 3 Then
            GuardaSiEs_Telmex(ClvServ, Me.EsTelmex_CheckBox.Checked) 'Guardamos si es servicio telmex o no SIEMPRE 
            GuardaTelmex_RelMedioServicio(ClvServ) ' Guardamos la relacion medio servicio SOLO cuando el servicio es NUEVO
        End If


        MsgBox("Se Ha Guardado con �xito", MsgBoxStyle.Information)

        'Habilitamos los botones de los Paquetes Adicionales.
        Me.btnAgregarPaqAdic.Enabled = True
        Me.btnEliminarPaqAdic.Enabled = True
        '-------

        Me.Panel9.Enabled = True
        activa(True)
        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Guardar de Servicios", "Se Dio de Alta Servicio", "Clve del Servicio" + CStr(Me.Clv_ServicioTextBox.Text), GloCiudad)
        GloBnd = True
        Me.Close()
    End Sub
    Private Sub GUARDARel_Trabajos_NoCobroMensualGUARDA()
        Try
            Dim cmd As New SqlClient.SqlCommand
            Dim CON4 As New SqlConnection(MiConexion)
            CON4.Open()
            If IsNumeric(Me.Clv_ServicioTextBox.Text) = True Then
                If Len(Trim(Me.ComboBox5.Text)) > 0 And IsNumeric(Me.ComboBox5.SelectedValue) = True Then
                    With cmd
                        .CommandText = "GuardaRel_trabajos_NoCobroMensual"
                        .CommandTimeout = 0
                        .CommandType = CommandType.StoredProcedure
                        .Connection = CON4
                        Dim prm As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
                        Dim prm2 As New SqlParameter("@Clv_Trabajo", SqlDbType.Int)
                        prm.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Input
                        prm.Value = Me.Clv_ServicioTextBox.Text
                        prm2.Value = Me.ComboBox5.SelectedValue
                        .Parameters.Add(prm)
                        .Parameters.Add(prm2)
                        Dim i As Integer = .ExecuteNonQuery

                    End With
                   
                End If
            End If
            CON4.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub BorrarRel_Trabajos_NoCobroMensual()
        Dim cmd As New SqlClient.SqlCommand
        Dim CON5 As New SqlConnection(MiConexion)
        CON5.Open()
        Try
            If IsNumeric(Me.Clv_ServicioTextBox.Text) = True Then
                With cmd
                    .CommandText = "BORRel_Trabajos_NoCobroMensual"
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure
                    .Connection = CON5
                    Dim prm As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
                    prm.Direction = ParameterDirection.Input
                    prm.Value = Me.Clv_ServicioTextBox.Text
                    .Parameters.Add(prm)
                    Dim i As Integer = .ExecuteNonQuery
                End With

            End If
            CON5.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub cobromensual()
        If Me.Sale_en_CarteraCheckBox1.Checked = True Then
            Me.Panel2.Visible = False
            Me.Sale_en_CarteraCheckBox.Checked = False
            Me.Sale_en_CarteraCheckBox.Visible = False
            Me.Genera_OrdenCheckBox.Checked = False
            Me.Genera_OrdenCheckBox.Visible = False
            Me.Panel3.Visible = False
            Me.TabControl1.Enabled = True
            Me.NumericUpDown5.Visible = True
            Me.Label6.Visible = True
            Me.CheckBox9.Visible = True
            Me.CheckBox10.Visible = True
            Me.Label29.Visible = True
        Else
            Me.NumericUpDown5.Visible = False
            Me.Label6.Visible = False
            Me.Panel2.Visible = True
            Me.Sale_en_CarteraCheckBox.Visible = True
            Me.Genera_OrdenCheckBox.Visible = True
            Me.Panel3.Visible = True
            Me.TabControl1.Enabled = False
            Me.CheckBox9.Visible = False
            Me.CheckBox10.Visible = False
            Me.Label29.Visible = False
        End If
    End Sub

    Private Sub FrmServiciosTelefonia_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        

        coloreaSer_Tel()


        TextBox9.BackColor = CMBTextBox28.BackColor
        TextBox10.BackColor = CMBTextBox28.BackColor
        TextBox21.BackColor = CMBTextBox28.BackColor

        Dim cone As New SqlClient.SqlConnection(MiConexion)
        cone.Open()
        Me.MuestraTipoClientesTableAdapter.Connection = cone
        Me.MuestraTipoClientesTableAdapter.Fill(Me.DataSetLidia2.MuestraTipoClientes, 0, 0)
        Me.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter.Connection = cone
        Me.MUESTRATRABAJOS_NOCOBROMENSUALTableAdapter.Fill(Me.DataSetEDGAR.MUESTRATRABAJOS_NOCOBROMENSUAL, GloClv_TipSer, 0)
        Me.Muestra_ServiciosDigitalesTableAdapter.Connection = cone
        Me.Muestra_ServiciosDigitalesTableAdapter.Fill(Me.DataSetyahve.Muestra_ServiciosDigitales)
        Me.Muestra_Dias_FinalTableAdapter.Connection = cone
        Me.Muestra_Dias_FinalTableAdapter.Fill(Me.DataSetyahve.Muestra_Dias_Final)
        Me.Muestra_Dias_inicialTableAdapter.Connection = cone
        Me.Muestra_Dias_inicialTableAdapter.Fill(Me.DataSetyahve.Muestra_Dias_inicial)
        cone.Close()
        Me.ComboBox4.Text = Nothing
        Me.BindingNavigatorAddNewItem.Enabled = False
        Me.ToolStripButton4.Enabled = False
        Me.CheckBox4.CheckState = CheckState.Unchecked

        Me.DameClv_Session()

       If opcion = "N" Then
            Me.Panel9.Enabled = False
            Me.CONSERVICIOSBindingSource.AddNew()
            Me.CLV_SERVICIOTextBox1.Text = GloClv_TipSer
            Me.Sale_en_CarteraCheckBox1.Checked = False
            Me.Es_PrincipalCheckBox.Checked = False
            Me.CheckBox1.Checked = False
            Me.CheckBox2.Checked = False
            Me.CheckBox3.Checked = False
            Me.CheckBox4.Checked = False
            Me.CheckBox5.Checked = False

            Me.EsTelmex_CheckBox.Checked = False  'nuevo
            Me.Button3.Enabled = True 'nuevo

        End If
        If opcion = "C" Or opcion = "M" Then
            Me.Panel9.Enabled = True
            cone.Open()
            Me.CONSERVICIOSTableAdapter.Connection = cone
            Me.CONSERVICIOSTableAdapter.Fill(Me.DataSetLidia2.CONSERVICIOS, GloClv_Servicio)
            cone.Close()
            LLena_Puntos()
            Guarda_Lineas(3)
            activa(True)
            If Me.Genera_OrdenCheckBox.Checked = True Then
                cone.Open()
                Me.CONRel_Trabajos_NoCobroMensualTableAdapter.Connection = cone
                Me.CONRel_Trabajos_NoCobroMensualTableAdapter.Fill(Me.DataSetEDGAR.CONRel_Trabajos_NoCobroMensual, Me.Clv_ServicioTextBox.Text)
                cone.Close()
            End If
            Consulta_Tipo_Servicio_Telefonia()
            DimeSiEs_Telmex() 'nuevo
        End If
        If opcion = "C" Then
            Me.ConServiciosTelBindingNavigator.Enabled = False
            CONSERVICIOSBindingNavigator.Enabled = False
            Punto_Pronto_PagoNumericUpDown.Enabled = False
            Puntos3NumericUpDown.Enabled = False
            Puntos6NumericUpDown.Enabled = False
            Puntos11NumericUpDown.Enabled = False
            Me.CheckBox10.Enabled = False
            Me.CheckBox9.Enabled = False
            NumericUpDown5.Enabled = False
            DescripcionTextBox.Enabled = False
        End If

        EnlazaDatos()

        Me.btnAgregarPaqAdic.Enabled = False
        Me.btnEliminarPaqAdic.Enabled = False
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        Dim Cone2 As New SqlClient.SqlConnection(MiConexion)
        If IsNumeric(Me.ComboBox4.SelectedValue) = True Then
            Me.BindingNavigatorAddNewItem.Enabled = False
            Me.ToolStripButton4.Enabled = False
            Cone2.Open()

            CONSULTA_Dias_Aplica()

            Me.Consultar_Rel_ServiciosDigitales_Tel_TMPTableAdapter.Connection = Cone2

            If Me.Clv_ServicioTextBox.Text = "" Then
                Me.Consultar_Rel_ServiciosDigitales_Tel_TMPTableAdapter.Fill(Me.DataSetyahve.Consultar_Rel_ServiciosDigitales_Tel_TMP, CInt(Me.TxtClv_Session.Text), 0, CInt(Me.ComboBox4.SelectedValue))
            Else
                Me.Consultar_Rel_ServiciosDigitales_Tel_TMPTableAdapter.Fill(Me.DataSetyahve.Consultar_Rel_ServiciosDigitales_Tel_TMP, CInt(Me.TxtClv_Session.Text), CInt(Me.Clv_ServicioTextBox.Text), CInt(Me.ComboBox4.SelectedValue))
            End If

            Me.ConServiciosTelTableAdapter.Connection = Cone2
            Me.ConServiciosTelTableAdapter.Fill(Me.DataSetLidia2.ConServiciosTel, Me.Clv_ServicioTextBox.Text, Me.ComboBox4.SelectedValue)
            Cone2.Close()
            ConServiciosTelRecontratacion(Me.Clv_ServicioTextBox.Text, Me.ComboBox4.SelectedValue)
            If IsNumeric(Me.TextBox5.Text) = True Then
                If Me.TextBox5.Text > 0 Then
                    Me.BindingNavigatorAddNewItem.Enabled = False
                    Me.ToolStripButton4.Enabled = True
                Else
                    activa(True)
                End If
      
            End If
            Me.TextBox5.Text = Me.Clv_ServicioTextBox.Text
            Me.Clv_TipoClienteTextBox.Text = Me.ComboBox4.SelectedValue
            Me.Panel21.Visible = True
            Consulta_Minutos_Cel()

        Else
            Cone2.Open()
            Me.ConServiciosTelTableAdapter.Connection = Cone2
            Me.ConServiciosTelTableAdapter.Fill(Me.DataSetLidia2.ConServiciosTel, 0, 0)
            Cone2.Close()
            ConServiciosTelRecontratacion(0, 0)
            Me.BindingNavigatorAddNewItem.Enabled = True
            Me.ToolStripButton4.Enabled = True
            Me.Panel21.Visible = False
        End If

    End Sub
    Private Sub Agrega_Minutos_Cel()
        Dim conexion As New SqlConnection(MiConexion)
        Dim command As New SqlCommand("Agrega_Minutos_Cel", conexion)
        command.CommandType = CommandType.StoredProcedure

        Dim parametro1 As New SqlParameter("@Clv_Servicio", SqlDbType.BigInt)
        Dim parametro2 As New SqlParameter("@Clv_TipoCobro", SqlDbType.BigInt)
        Dim parametro3 As New SqlParameter("@Minutos_044", SqlDbType.Int)
        Dim parametro4 As New SqlParameter("@Adic_044", SqlDbType.Money)
        Dim parametro5 As New SqlParameter("@Minutos_045", SqlDbType.Int)
        Dim parametro6 As New SqlParameter("@Adic_045", SqlDbType.Money)
        Dim parametro7 As New SqlParameter("@Ilimitados_044", SqlDbType.Bit)
        Dim parametro8 As New SqlParameter("@Ilimitados_045", SqlDbType.Bit)

        parametro1.Direction = ParameterDirection.Input
        parametro2.Direction = ParameterDirection.Input
        parametro3.Direction = ParameterDirection.Input
        parametro4.Direction = ParameterDirection.Input
        parametro5.Direction = ParameterDirection.Input
        parametro6.Direction = ParameterDirection.Input
        parametro7.Direction = ParameterDirection.Input
        parametro8.Direction = ParameterDirection.Input

        If Me.TextBox15.Text.Length = 0 Then
            Me.TextBox15.Text = 0
        End If

        If Me.TextBox16.Text.Length = 0 Then
            Me.TextBox16.Text = 0
        End If

        parametro1.Value = CInt(Me.Clv_ServicioTextBox.Text)
        parametro2.Value = CInt(Me.ComboBox4.SelectedValue)
        parametro3.Value = Me.NumericUpDown6.Value
        parametro4.Value = Me.TextBox15.Text
        parametro5.Value = Me.NumericUpDown9.Value
        parametro6.Value = Me.TextBox16.Text
        parametro7.Value = Me.CheckBox7.CheckState
        parametro8.Value = Me.CheckBox8.CheckState

        command.Parameters.Add(parametro1)
        command.Parameters.Add(parametro2)
        command.Parameters.Add(parametro3)
        command.Parameters.Add(parametro4)
        command.Parameters.Add(parametro5)
        command.Parameters.Add(parametro6)
        command.Parameters.Add(parametro7)
        command.Parameters.Add(parametro8)

        Try
            conexion.Open()
            command.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Elimina_Minutos_Cel(ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim command As New SqlCommand("Elimina_Minutos_Cel", conexion)
        command.CommandType = CommandType.StoredProcedure

        Dim parametro1 As New SqlParameter("@Clv_Servicio", SqlDbType.BigInt)
        Dim parametro2 As New SqlParameter("@Clv_TipoCobro", SqlDbType.BigInt)
        Dim parametro3 As New SqlParameter("@Op", SqlDbType.Int)

        parametro1.Direction = ParameterDirection.Input
        parametro2.Direction = ParameterDirection.Input
        parametro3.Direction = ParameterDirection.Input

        parametro1.Value = CInt(Me.Clv_ServicioTextBox.Text)
        parametro2.Value = CInt(Me.ComboBox4.SelectedValue)
        parametro3.Value = Op

        command.Parameters.Add(parametro1)
        command.Parameters.Add(parametro2)
        command.Parameters.Add(parametro3)

        Try
            conexion.Open()
            command.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Elimina_Tipo_Servicio_Telefonia()
        Dim conexion As New SqlConnection(MiConexion)
        Dim command As New SqlCommand("Elimina_Tipo_Servicio_Telefonia", conexion)
        command.CommandType = CommandType.StoredProcedure

        Dim parametro1 As New SqlParameter("@Clv_Servicio", SqlDbType.BigInt)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = CInt(Me.Clv_ServicioTextBox.Text)
        command.Parameters.Add(parametro1)

        Try
            conexion.Open()
            command.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Agrega_Tipo_Servicio_Telefonia()
        Dim conexion As New SqlConnection(MiConexion)
        Dim command As New SqlCommand("Agrega_Tipo_Servicio_Telefonia", conexion)
        command.CommandType = CommandType.StoredProcedure

        Dim parametro1 As New SqlParameter("@Clv_Servicio", SqlDbType.BigInt)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = CInt(Me.Clv_ServicioTextBox.Text)
        command.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@Residencial", SqlDbType.Bit)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Me.CheckBox9.CheckState
        command.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Empresarial", SqlDbType.Bit)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Me.CheckBox10.CheckState
        command.Parameters.Add(parametro3)

        Try
            conexion.Open()
            command.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Consulta_Tipo_Servicio_Telefonia()
        Dim conexion As New SqlConnection(MiConexion)
        Dim command As New SqlCommand("Consulta_Tipo_Servicio_Telefonia", conexion)
        command.CommandType = CommandType.StoredProcedure

        Dim parametro1 As New SqlParameter("@Clv_Servicio", SqlDbType.BigInt)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = CInt(Me.Clv_ServicioTextBox.Text)
        command.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@Residencial", SqlDbType.Bit)
        parametro2.Direction = ParameterDirection.Output
        command.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Empresarial", SqlDbType.Bit)
        parametro3.Direction = ParameterDirection.Output
        command.Parameters.Add(parametro3)

        Try
            conexion.Open()
            command.ExecuteNonQuery()
            Me.CheckBox9.Checked = parametro2.Value
            Me.CheckBox10.Checked = parametro3.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Consulta_Minutos_Cel()
        Dim conexion As New SqlConnection(MiConexion)
        Dim command As New SqlCommand("Consulta_Minutos_Cel", conexion)
        command.CommandType = CommandType.StoredProcedure

        Dim parametro1 As New SqlParameter("@Clv_Servicio", SqlDbType.BigInt)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = CInt(Me.Clv_ServicioTextBox.Text)
        command.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@Clv_TipoCobro", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = CInt(Me.ComboBox4.SelectedValue)
        command.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Minutos_044", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        command.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Adic_044", SqlDbType.Money)
        parametro4.Direction = ParameterDirection.Output
        command.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Minutos_045", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Output
        command.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Adic_045", SqlDbType.Money)
        parametro6.Direction = ParameterDirection.Output
        command.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@Ilimitado_044", SqlDbType.Bit)
        parametro7.Direction = ParameterDirection.Output
        command.Parameters.Add(parametro7)

        Dim parametro8 As New SqlParameter("@Ilimitado_045", SqlDbType.Bit)
        parametro8.Direction = ParameterDirection.Output
        command.Parameters.Add(parametro8)
        
        Try
            conexion.Open()
            command.ExecuteNonQuery()

            Me.NumericUpDown6.Value = parametro3.Value
            Me.TextBox15.Text = Format(CDec(parametro4.Value), "##,##0.00")
            Me.NumericUpDown9.Value = parametro5.Value
            Me.TextBox16.Text = Format(CDec(parametro6.Value), "##,##0.00")
            Me.CheckBox7.Checked = parametro7.Value
            Me.CheckBox8.Checked = parametro8.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub CONSULTA_Dias_Aplica()

        Dim sw As Integer = 0
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("CONSULTA_Dias_Aplica", con)

        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New Data.SqlClient.SqlParameter( _
                "@Clv_Servicio", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.Clv_ServicioTextBox.Text)
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                  "@Clv_Tipocobro", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox4.SelectedValue)
        cmd.Parameters.Add(prm)

        con.Open()

        Dim reader As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Try
            While reader.Read()

                'Llenamos los TxtBox con los datos respectivos
                Me.ComboBox2.SelectedValue = CInt(reader(0).ToString)
                Me.ComboBox3.SelectedValue = CInt(reader(1).ToString)
                sw = 1

            End While
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            reader.Close()
        End Try
        con.Close()
        If sw = 0 Then
            Me.ComboBox2.SelectedValue = 0
            Me.ComboBox3.SelectedValue = 0
        End If

    End Sub


    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim cone5 As New SqlClient.SqlConnection(MiConexion)
        cone5.Open()
        Me.CONSERVICIOSTableAdapter.Connection = cone5
        Me.CONSERVICIOSTableAdapter.Delete(Me.Clv_ServicioTextBox.Text)
        cone5.Close()
        Borra_Datos()
        Guarda_Lineas(1)
        Elimina_Minutos_Cel(2)
        Elimina_Tipo_Servicio_Telefonia()
        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Eliminar en Servicios", "Se Borr� Servicio de Telefonia", "Clave del Servicio" + CStr(Me.Clv_ServicioTextBox.Text), GloCiudad)
        MsgBox("Se Ha Borrado con �xito", MsgBoxStyle.Information)
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.CONSERVICIOSBindingSource.CancelEdit()

    End Sub

    Private Sub Sale_en_CarteraCheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Sale_en_CarteraCheckBox1.CheckedChanged
        cobromensual()
    End Sub
    Private Sub LLena_Puntos()
        Dim reader As SqlDataReader
        Dim CON As New SqlConnection(MiConexion)
        Dim previousConnectionState As ConnectionState = CON.State
        Try
            If CON.State = ConnectionState.Closed Then
                CON.Open()
            End If
            Dim comando As SqlClient.SqlCommand
            comando = New SqlClient.SqlCommand
            If Me.Clv_ServicioTextBox.Text = "" Then Me.Clv_ServicioTextBox.Text = 0
            With comando
                .Connection = CON
                .CommandText = "exec BUSCAPuntos_Pago_Adelantado " & Me.Clv_ServicioTextBox.Text & ",0"
                .CommandType = CommandType.Text
                .CommandTimeout = 0
                reader = comando.ExecuteReader()
                Using reader
                    While reader.Read
                        ' Process SprocResults datareader here.
                        Me.Puntos3NumericUpDown.Value = reader.GetValue(1)
                        Me.Puntos6NumericUpDown.Value = reader.GetValue(2)
                        Me.Puntos11NumericUpDown.Value = reader.GetValue(3)
                        Me.Punto_Pronto_PagoNumericUpDown.Value = reader.GetValue(4)
                    End While
                End Using
            End With
            CON.Close()

        Finally
            If previousConnectionState = ConnectionState.Closed Then
                CON.Close()
            End If
        End Try

    End Sub


    Private Sub Genera_OrdenCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Genera_OrdenCheckBox.CheckedChanged
        If Me.Genera_OrdenCheckBox.CheckState = CheckState.Checked Then
            Panel18.Visible = True
        Else
            Panel18.Visible = False
        End If
    End Sub

    Private Sub BindingNavigatorAddNewItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorAddNewItem.Click
        Me.Panel9.Enabled = True
        activa(False)
        Me.TabControl2.Enabled = True
        Me.ConServiciosTelBindingNavigatorSaveItem.Enabled = True
        Me.CMBPanel4.Enabled = True
        Me.ConServiciosTelTableAdapter.Fill(Me.DataSetLidia2.ConServiciosTel, 0, 0)
        Me.TextBox5.Text = Me.Clv_ServicioTextBox.Text
        Me.Clv_TipoClienteTextBox.Text = Me.ComboBox4.SelectedValue
        Me.BindingNavigatorAddNewItem.Enabled = True
        Me.ToolStripButton3.Enabled = True
        Me.Panel15.Enabled = False
        Me.Panel21.Enabled = True

        If Me.cmbPaqAdic.Items.Count > 0 Then
            Me.btnAgregarPaqAdic.Enabled = True
        Else
            Me.btnAgregarPaqAdic.Enabled = False
        End If

        If Me.dgPaquetesAdicionales.RowCount > 0 Then
            Me.btnEliminarPaqAdic.Enabled = True
        Else
            Me.btnEliminarPaqAdic.Enabled = False
        End If


    End Sub
    Private Sub activa(ByVal opt As Boolean)
        Me.Button1.Enabled = Not (opt)
        Me.Button2.Enabled = Not (opt)
        Me.TxtBoxCosto.ReadOnly = opt
        Me.ComboBox1.Enabled = Not (opt)
        Me.ComboBox2.Enabled = Not (opt)
        Me.ComboBox3.Enabled = Not (opt)


        Me.TextBox1.ReadOnly = opt
        Me.TextBox2.ReadOnly = opt
        Me.TextBox3.ReadOnly = opt
        Me.TextBox4.ReadOnly = opt
        Me.TextBox7.ReadOnly = opt
        Me.TextBox8.ReadOnly = opt
        Me.CheckBox1.Enabled = Not (opt)
        Me.CheckBox2.Enabled = Not (opt)
        Me.CheckBox3.Enabled = Not (opt)
        Me.CheckBox4.Enabled = Not (opt)
        Me.CheckBox5.Enabled = Not (opt)

        Me.NumericUpDown6.Enabled = Not (opt)
        Me.CheckBox7.Enabled = Not (opt)
        Me.TextBox15.Enabled = Not (opt)
        Me.NumericUpDown9.Enabled = Not (opt)
        Me.CheckBox8.Enabled = Not (opt)
        Me.TextBox16.Enabled = Not (opt)

        'Me.TextBox9.ReadOnly = opt
        'Me.TextBox10.ReadOnly = opt
        Me.TextBox17.ReadOnly = opt
        Me.TextBox18.ReadOnly = opt
        Me.TextBox12.ReadOnly = opt
        Me.TextBox11.ReadOnly = opt
        txtRecontratacion.ReadOnly = opt
        Me.TextBox14.ReadOnly = opt
        'Me.TextBox15.ReadOnly = opt
        Me.NumericUpDown1.ReadOnly = opt
        'Me.NumericUpDown6.ReadOnly = opt
        'Me.NumericUpDown5.ReadOnly = opt
        Me.NumericUpDown4.ReadOnly = opt
        Me.NumericUpDown3.ReadOnly = opt
        Me.NumericUpDown2.ReadOnly = opt
        Me.NumericUpDown8.ReadOnly = opt
        Me.NumericUpDown7.ReadOnly = opt
        'Me.NumericUpDown9.ReadOnly = opt
    End Sub
    Private Sub ToolStripButton4_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton4.Click
        activa(False)
        Me.Panel21.Enabled = True
        Me.ConServiciosTelBindingNavigatorSaveItem.Enabled = True
        Me.CMBPanel4.Enabled = True
        Me.BindingNavigatorAddNewItem.Enabled = False
        Me.ToolStripButton3.Enabled = True
        Me.Panel15.Enabled = False
        ban = True

        If Me.cmbPaqAdic.Items.Count > 0 Then
            Me.btnAgregarPaqAdic.Enabled = True
        Else
            Me.btnAgregarPaqAdic.Enabled = False
        End If

        If Me.dgPaquetesAdicionales.RowCount > 0 Then
            Me.btnEliminarPaqAdic.Enabled = True
        Else
            Me.btnEliminarPaqAdic.Enabled = False
        End If



    End Sub

    Private Sub ConServiciosTelBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConServiciosTelBindingNavigatorSaveItem.Click
        Dim cone6 As New SqlClient.SqlConnection(MiConexion)
        If Me.TextBox8.Text.Trim.Length > 0 And IsNumeric(Me.TextBox8.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Contrataci�n", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox8.Text.Trim.Length = 0 Then
            Me.TextBox8.Text = 0
        End If
        If Me.TextBox3.Text.Trim.Length > 0 And IsNumeric(Me.TextBox3.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Mensualidad", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox3.Text.Trim.Length = 0 Then
            Me.TextBox3.Text = 0
        End If
        If Me.TextBox11.Text.Trim.Length > 0 And IsNumeric(Me.TextBox11.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Instalaci�n", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox11.Text.Trim.Length = 0 Then
            Me.TextBox11.Text = 0
        End If
        If txtRecontratacion.Text.Trim.Length > 0 And IsNumeric(txtRecontratacion.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Instalaci�n", MsgBoxStyle.Information)
            Exit Sub
        ElseIf txtRecontratacion.Text.Trim.Length = 0 Then
            txtRecontratacion.Text = 0
        End If
        If Me.TextBox12.Text.Trim.Length > 0 And IsNumeric(Me.TextBox12.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Extensi�n Adicional", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox12.Text.Trim.Length = 0 Then
            Me.TextBox12.Text = 0
        End If
        If Me.TextBox17.Text.Trim.Length > 0 And IsNumeric(Me.TextBox17.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Inal�mbrico", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox17.Text.Trim.Length = 0 Then
            Me.TextBox17.Text = 0
        End If
        If Me.TextBox18.Text.Trim.Length > 0 And IsNumeric(Me.TextBox18.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Al�mbrico", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox18.Text.Trim.Length = 0 Then
            Me.TextBox18.Text = 0
        End If
        If Me.TextBox14.Text.Trim.Length > 0 And IsNumeric(Me.TextBox14.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Llamada Adicional a Mismo Fraccionamiento", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox14.Text.Trim.Length = 0 Then
            Me.TextBox14.Text = 0
        End If
        'If Me.TextBox9.Text.Trim.Length > 0 And IsNumeric(Me.TextBox9.Text) = False Then
        '    MsgBox("Se Debe de Capturar un Costo de Minuto Adicional a Celular", MsgBoxStyle.Information)
        '    Exit Sub
        'ElseIf Me.TextBox9.Text.Trim.Length = 0 Then
        '    Me.TextBox9.Text = 0
        'End If
        'If Me.TextBox10.Text.Trim.Length > 0 And IsNumeric(Me.TextBox10.Text) = False Then
        '    MsgBox("Se Debe de Capturar un Costo de Minuto Adicional a 045", MsgBoxStyle.Information)
        '    Exit Sub
        'ElseIf Me.TextBox10.Text.Trim.Length = 0 Then
        '    Me.TextBox10.Text = 0
        'End If
        If Me.TextBox1.Text.Trim.Length > 0 And IsNumeric(Me.TextBox1.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Llamada Adicional De Llamada Local", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox1.Text.Trim.Length = 0 Then
            Me.TextBox1.Text = 0
        End If
        If Me.TextBox2.Text.Trim.Length > 0 And IsNumeric(Me.TextBox2.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Minuto Adicional De Larga Distancia Nacional", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox2.Text.Trim.Length = 0 Then
            Me.TextBox2.Text = 0
        End If
        If Me.TextBox4.Text.Trim.Length > 0 And IsNumeric(Me.TextBox4.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Minuto Adicional De Larga Distancia Internacional", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox4.Text.Trim.Length = 0 Then
            Me.TextBox4.Text = 0
        End If
        If Me.TextBox7.Text.Trim.Length > 0 And IsNumeric(Me.TextBox7.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo de Minuto Adicional a Estados Unidos y C�nada", MsgBoxStyle.Information)
            Exit Sub
        ElseIf Me.TextBox7.Text.Trim.Length = 0 Then
            Me.TextBox7.Text = 0
        End If

        'If Me.TextBox15.Text.Trim.Length > 0 And IsNumeric(Me.TextBox15.Text) = False Then
        '    MsgBox("Se Debe de Capturar un Costo de Llamada Adicional a Numeros 01-900", MsgBoxStyle.Information)
        '    Exit Sub
        'ElseIf Me.TextBox15.Text.Trim.Length = 0 Then
        '    Me.TextBox15.Text = 0
        'End If


        'If Me.ComboBox2.SelectedValue = 0 Then
        '    MsgBox("Se Debe de Seleccionar un D�a inicial", MsgBoxStyle.Information)
        '    Exit Sub
        'End If

        'If Me.ComboBox3.SelectedValue = 0 Then
        '    MsgBox("Se Debe de Seleccionar un D�a Final", MsgBoxStyle.Information)
        '    Exit Sub
        'End If

        If ComboBox2.SelectedValue > Me.ComboBox3.SelectedValue Then
            MsgBox("Se Debe de Seleccionar un D�a Final Menor al Inicial", MsgBoxStyle.Information)
            Exit Sub
        End If

        Guarda_ServiciosDigitales_Tel()
        Agrega_Minutos_Cel()

        Me.TextBox5.Text = Me.Clv_ServicioTextBox.Text
        Me.Clv_TipoClienteTextBox.Text = Me.ComboBox4.SelectedValue
        Me.Validate()
        If ban = False Then
            cone6.Open()
            Me.ConServiciosTelTableAdapter.Connection = cone6
            Me.ConServiciosTelTableAdapter.Insert(Me.TextBox5.Text, Me.Clv_TipoClienteTextBox.Text, "", Me.TextBox8.Text, Me.TextBox3.Text, Me.NumericUpDown2.Value, Me.NumericUpDown1.Value, Me.NumericUpDown7.Value, Me.NumericUpDown3.Value, Me.NumericUpDown4.Value, Me.NumericUpDown8.Value, Me.CheckBox1.CheckState, Me.TextBox1.Text, Me.TextBox2.Text, Me.TextBox4.Text, Me.CheckBox2.CheckState, Me.TextBox7.Text, Me.TextBox14.Text, Me.CheckBox3.CheckState, Me.TextBox11.Text, Me.TextBox12.Text, Me.CheckBox5.CheckState, Me.CheckBox4.CheckState)
            cone6.Close()
            ModServiciosTelRecontratacion(TextBox5.Text, Clv_TipoClienteTextBox.Text, txtRecontratacion.Text)
            bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Guardar En Precios por Tipo de Cobro", "Se Dio de Alta Cobro de Servicio" + CStr(Me.Clv_ServicioTextBox.Text), "En el Cobro de " + CStr(Me.ComboBox4.Text), GloCiudad)
            Nuevo_Dias_Aplica()
        ElseIf ban = True Then
            cone6.Open()
            Me.CONSERVICIOSBindingSource.EndEdit()
            Me.ConServiciosTelTableAdapter.Connection = cone6
            'MsgBox(Me.TextBox5.Text & "," & Me.Clv_TipoClienteTextBox.Text & "," & "" & "," & Me.TextBox8.Text & "," & Me.TextBox3.Text & "," & Me.NumericUpDown2.Value & "," & Me.NumericUpDown1.Value & "," & Me.NumericUpDown7.Value & "," & Me.NumericUpDown3.Value & "," & Me.NumericUpDown4.Value & "," & Me.NumericUpDown8.Value & "," & Me.CheckBox1.CheckState & "," & Me.TextBox1.Text & "," & Me.TextBox2.Text & "," & Me.TextBox4.Text & "," & Me.CheckBox2.CheckState & "," & Me.TextBox7.Text & "," & Me.TextBox14.Text & "," & Me.CheckBox3.CheckState & "," & Me.TextBox11.Text & "," & Me.TextBox12.Text & "," & Me.CheckBox5.CheckState & "," & Me.CheckBox4.CheckState)
            Me.ConServiciosTelTableAdapter.Update(Me.TextBox5.Text, Me.Clv_TipoClienteTextBox.Text, "", Me.TextBox8.Text, Me.TextBox3.Text, Me.NumericUpDown2.Value, Me.NumericUpDown1.Value, Me.NumericUpDown7.Value, Me.NumericUpDown3.Value, Me.NumericUpDown4.Value, Me.NumericUpDown8.Value, Me.CheckBox1.CheckState, Me.TextBox1.Text, Me.TextBox2.Text, Me.TextBox4.Text, Me.CheckBox2.CheckState, Me.TextBox7.Text, Me.TextBox14.Text, Me.CheckBox3.CheckState, Me.TextBox11.Text, Me.TextBox12.Text, Me.CheckBox5.CheckState, Me.CheckBox4.CheckState)
            cone6.Close()
            ModServiciosTelRecontratacion(TextBox5.Text, Clv_TipoClienteTextBox.Text, txtRecontratacion.Text)
            bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Guardar En Precios por Tipo de Cobro", "Se Modific� Cobro de Servicio " + CStr(Me.Clv_ServicioTextBox.Text), "En el Cobro de " + CStr(Me.ComboBox4.Text), GloCiudad)
            Modifica_Dias_Aplica()
        End If
        If Me.CheckBox6.CheckState = CheckState.Checked Then
            Me.Inserta_Todos()
        End If
        Me.BindingNavigatorAddNewItem.Enabled = True
        MsgBox("Se Ha Guardado con �xito", MsgBoxStyle.Information)
        activa(True)
        Me.Panel21.Enabled = False
        Me.Panel15.Enabled = True
        Me.CMBPanel4.Enabled = False
        Me.ConServiciosTelBindingNavigatorSaveItem.Enabled = False
        limpia()

    End Sub

    Private Sub limpia()
        Me.TxtBoxCosto.Text = ""
        Me.ComboBox1.SelectedValue = 0

    End Sub

    Private Sub Inserta_Todos()
        Dim Cmd As New SqlCommand
        Dim ConAll As New SqlConnection(MiConexion)
        ConAll.Open()
        With Cmd
            .CommandText = "NueServiciosTel_Todos"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = ConAll
            Dim Pmt As New SqlParameter("@Clv_servicio ", SqlDbType.BigInt)
            Pmt.Direction = ParameterDirection.Input
            Pmt.Value = Me.TextBox5.Text
            .Parameters.Add(Pmt)
            Dim Pmt2 As New SqlParameter("@Clv_tipocliente", SqlDbType.Int)
            Pmt2.Direction = ParameterDirection.Input
            Pmt2.Value = Me.Clv_TipoClienteTextBox.Text
            .Parameters.Add(Pmt2)
            Dim Pmt3 As New SqlParameter("@Descripcion", SqlDbType.VarChar, 800)
            Pmt3.Direction = ParameterDirection.Input
            Pmt3.Value = ""
            .Parameters.Add(Pmt3)
            Dim Pmt4 As New SqlParameter("@Precio_Contratacion", SqlDbType.Money)
            Pmt4.Direction = ParameterDirection.Input
            Pmt4.Value = Me.TextBox8.Text
            .Parameters.Add(Pmt4)
            Dim Pmt5 As New SqlParameter("@Mensualidad", SqlDbType.Money)
            Pmt5.Direction = ParameterDirection.Input
            Pmt5.Value = Me.TextBox3.Text
            .Parameters.Add(Pmt5)
            Dim Pmt6 As New SqlParameter("@Extensiones_SinCosto", SqlDbType.Int)
            Pmt6.Direction = ParameterDirection.Input
            Pmt6.Value = Me.NumericUpDown2.Value
            .Parameters.Add(Pmt6)
            Dim Pmt7 As New SqlParameter("@Llamadas_Locales", SqlDbType.Int)
            Pmt7.Direction = ParameterDirection.Input
            Pmt7.Value = Me.NumericUpDown1.Value
            .Parameters.Add(Pmt7)
            Dim Pmt8 As New SqlParameter("@Minutos_EUA_Can", SqlDbType.Int)
            Pmt8.Direction = ParameterDirection.Input
            Pmt8.Value = Me.NumericUpDown7.Value
            .Parameters.Add(Pmt8)
            Dim Pmt9 As New SqlParameter("@Minutos_LDN", SqlDbType.Int)
            Pmt9.Direction = ParameterDirection.Input
            Pmt9.Value = Me.NumericUpDown3.Value
            .Parameters.Add(Pmt9)
            Dim Pmt10 As New SqlParameter("@Minutos_LDI", SqlDbType.Int)
            Pmt10.Direction = ParameterDirection.Input
            Pmt10.Value = Me.NumericUpDown4.Value
            .Parameters.Add(Pmt10)
            Dim Pmt11 As New SqlParameter("@Minutos_Fraccionamiento", SqlDbType.Int)
            Pmt11.Direction = ParameterDirection.Input
            Pmt11.Value = Me.NumericUpDown8.Value
            .Parameters.Add(Pmt11)
            Dim Pmt12 As New SqlParameter("@Ilimitadas_Loc", SqlDbType.Bit)
            Pmt12.Direction = ParameterDirection.Input
            Pmt12.Value = Me.CheckBox1.CheckState
            .Parameters.Add(Pmt12)
            Dim Pmt13 As New SqlParameter("@Costo_Lamada_Adic", SqlDbType.Money)
            Pmt13.Direction = ParameterDirection.Input
            Pmt13.Value = Me.TextBox1.Text
            .Parameters.Add(Pmt13)
            Dim Pmt14 As New SqlParameter("@Costo_Min_LDN_Adic", SqlDbType.Money)
            Pmt14.Direction = ParameterDirection.Input
            Pmt14.Value = Me.TextBox2.Text
            .Parameters.Add(Pmt14)
            Dim Pmt15 As New SqlParameter("@Costo_Min_LDI_Adic", SqlDbType.Money)
            Pmt15.Direction = ParameterDirection.Input
            Pmt15.Value = Me.TextBox4.Text
            .Parameters.Add(Pmt15)
            Dim Pmt16 As New SqlParameter("@Ilimitadas_Frac", SqlDbType.Bit)
            Pmt16.Direction = ParameterDirection.Input
            Pmt16.Value = Me.CheckBox2.CheckState
            .Parameters.Add(Pmt16)
            Dim Pmt17 As New SqlParameter("@Costo_Min_EUA_Can", SqlDbType.Money)
            Pmt17.Direction = ParameterDirection.Input
            Pmt17.Value = Me.TextBox7.Text
            .Parameters.Add(Pmt17)
            Dim Pmt18 As New SqlParameter("@Costo_fraccionamiento", SqlDbType.Money)
            Pmt18.Direction = ParameterDirection.Input
            Pmt18.Value = Me.TextBox14.Text
            .Parameters.Add(Pmt18)
            Dim Pmt19 As New SqlParameter("@Ilimitadas_LDN", SqlDbType.Bit)
            Pmt19.Direction = ParameterDirection.Input
            Pmt19.Value = Me.CheckBox3.CheckState
            .Parameters.Add(Pmt19)
            Dim Pmt20 As New SqlParameter("@Costo_Instalacion", SqlDbType.Money)
            Pmt20.Direction = ParameterDirection.Input
            Pmt20.Value = Me.TextBox11.Text
            .Parameters.Add(Pmt20)
            Dim Pmt21 As New SqlParameter("@Costo_Extension_Adic", SqlDbType.Money)
            Pmt21.Direction = ParameterDirection.Input
            Pmt21.Value = Me.TextBox12.Text
            .Parameters.Add(Pmt21)
            Dim Pmt22 As New SqlParameter("@Ilimitadas_LDI ", SqlDbType.Bit)
            Pmt22.Direction = ParameterDirection.Input
            Pmt22.Value = Me.CheckBox5.CheckState
            .Parameters.Add(Pmt22)
            Dim Pmt23 As New SqlParameter("@Ilimitadas_Eua", SqlDbType.Bit)
            Pmt23.Direction = ParameterDirection.Input
            Pmt23.Value = Me.CheckBox4.CheckState
            .Parameters.Add(Pmt23)
            .ExecuteNonQuery()
        End With
        ConAll.Close()
        ModServiciosTelRecontratacion(TextBox5.Text, 0, txtRecontratacion.Text)

    End Sub


    Private Sub Inserta_Uno()
        Dim Cmd As New SqlCommand
        Dim ConAll As New SqlConnection(MiConexion)
        ConAll.Open()
        With Cmd
            .CommandText = "NueServiciosTel_Todos"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = ConAll
            Dim Pmt As New SqlParameter("@Clv_servicio ", SqlDbType.BigInt)
            Pmt.Direction = ParameterDirection.Input
            Pmt.Value = Me.TextBox5.Text
            .Parameters.Add(Pmt)
            Dim Pmt2 As New SqlParameter("@Clv_tipocliente", SqlDbType.Int)
            Pmt2.Direction = ParameterDirection.Input
            Pmt2.Value = Me.Clv_TipoClienteTextBox.Text
            .Parameters.Add(Pmt2)
            Dim Pmt3 As New SqlParameter("@Descripcion", SqlDbType.VarChar, 800)
            Pmt3.Direction = ParameterDirection.Input
            Pmt3.Value = ""
            .Parameters.Add(Pmt3)
            Dim Pmt4 As New SqlParameter("@Precio_Contratacion", SqlDbType.Money)
            Pmt4.Direction = ParameterDirection.Input
            Pmt4.Value = Me.TextBox8.Text
            .Parameters.Add(Pmt4)
            Dim Pmt5 As New SqlParameter("@Mensualidad", SqlDbType.Money)
            Pmt5.Direction = ParameterDirection.Input
            Pmt5.Value = Me.TextBox3.Text
            .Parameters.Add(Pmt5)
            Dim Pmt6 As New SqlParameter("@Extensiones_SinCosto", SqlDbType.Int)
            Pmt6.Direction = ParameterDirection.Input
            Pmt6.Value = Me.NumericUpDown2.Value
            .Parameters.Add(Pmt6)
            Dim Pmt7 As New SqlParameter("@Llamadas_Locales", SqlDbType.Int)
            Pmt7.Direction = ParameterDirection.Input
            Pmt7.Value = Me.NumericUpDown1.Value
            .Parameters.Add(Pmt7)
            Dim Pmt8 As New SqlParameter("@Minutos_EUA_Can", SqlDbType.Int)
            Pmt8.Direction = ParameterDirection.Input
            Pmt8.Value = Me.NumericUpDown7.Value
            .Parameters.Add(Pmt8)
            Dim Pmt9 As New SqlParameter("@Minutos_LDN", SqlDbType.Int)
            Pmt9.Direction = ParameterDirection.Input
            Pmt9.Value = Me.NumericUpDown3.Value
            .Parameters.Add(Pmt9)
            Dim Pmt10 As New SqlParameter("@Minutos_LDI", SqlDbType.Int)
            Pmt10.Direction = ParameterDirection.Input
            Pmt10.Value = Me.NumericUpDown4.Value
            .Parameters.Add(Pmt10)
            Dim Pmt11 As New SqlParameter("@Minutos_Fraccionamiento", SqlDbType.Int)
            Pmt11.Direction = ParameterDirection.Input
            Pmt11.Value = Me.NumericUpDown8.Value
            .Parameters.Add(Pmt11)
            Dim Pmt12 As New SqlParameter("@Ilimitadas_Loc", SqlDbType.Bit)
            Pmt12.Direction = ParameterDirection.Input
            Pmt12.Value = Me.CheckBox1.CheckState
            .Parameters.Add(Pmt12)
            Dim Pmt13 As New SqlParameter("@Costo_Lamada_Adic", SqlDbType.Money)
            Pmt13.Direction = ParameterDirection.Input
            Pmt13.Value = Me.TextBox1.Text
            .Parameters.Add(Pmt13)
            Dim Pmt14 As New SqlParameter("@Costo_Min_LDN_Adic", SqlDbType.Money)
            Pmt14.Direction = ParameterDirection.Input
            Pmt14.Value = Me.TextBox2.Text
            .Parameters.Add(Pmt14)
            Dim Pmt15 As New SqlParameter("@Costo_Min_LDI_Adic", SqlDbType.Money)
            Pmt15.Direction = ParameterDirection.Input
            Pmt15.Value = Me.TextBox4.Text
            .Parameters.Add(Pmt15)
            Dim Pmt16 As New SqlParameter("@Ilimitadas_Frac", SqlDbType.Bit)
            Pmt16.Direction = ParameterDirection.Input
            Pmt16.Value = Me.CheckBox2.CheckState
            .Parameters.Add(Pmt16)
            Dim Pmt17 As New SqlParameter("@Costo_Min_EUA_Can", SqlDbType.Money)
            Pmt17.Direction = ParameterDirection.Input
            Pmt17.Value = Me.TextBox7.Text
            .Parameters.Add(Pmt17)
            Dim Pmt18 As New SqlParameter("@Costo_fraccionamiento", SqlDbType.Money)
            Pmt18.Direction = ParameterDirection.Input
            Pmt18.Value = Me.TextBox14.Text
            .Parameters.Add(Pmt18)
            Dim Pmt19 As New SqlParameter("@Ilimitadas_LDN", SqlDbType.Bit)
            Pmt19.Direction = ParameterDirection.Input
            Pmt19.Value = Me.CheckBox3.CheckState
            .Parameters.Add(Pmt19)
            Dim Pmt20 As New SqlParameter("@Costo_Instalacion", SqlDbType.Money)
            Pmt20.Direction = ParameterDirection.Input
            Pmt20.Value = Me.TextBox11.Text
            .Parameters.Add(Pmt20)
            Dim Pmt21 As New SqlParameter("@Costo_Extension_Adic", SqlDbType.Money)
            Pmt21.Direction = ParameterDirection.Input
            Pmt21.Value = Me.TextBox12.Text
            .Parameters.Add(Pmt21)
            Dim Pmt22 As New SqlParameter("@Ilimitadas_LDI ", SqlDbType.Bit)
            Pmt22.Direction = ParameterDirection.Input
            Pmt22.Value = Me.CheckBox5.CheckState
            .Parameters.Add(Pmt22)
            Dim Pmt23 As New SqlParameter("@Ilimitadas_Eua", SqlDbType.Bit)
            Pmt23.Direction = ParameterDirection.Input
            Pmt23.Value = Me.CheckBox4.CheckState
            .Parameters.Add(Pmt23)
            .ExecuteNonQuery()
        End With
        ConAll.Close()
        ModServiciosTelRecontratacion(TextBox5.Text, Clv_TipoClienteTextBox.Text, txtRecontratacion.Text)
    End Sub

    Private Sub ToolStripButton3_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        Me.ConServiciosTelBindingSource.CancelEdit()
        Me.ConServiciosTelBindingNavigatorSaveItem.Enabled = False
        Me.CMBPanel4.Enabled = False
        Me.ComboBox4.Text = Nothing
        Me.BindingNavigatorAddNewItem.Enabled = True
        Me.ToolStripButton3.Enabled = False
        Me.Panel15.Enabled = True
        Me.Panel21.Enabled = False
        activa(True)
    End Sub

    Private Sub ToolStripButton2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        Dim cone1 As New SqlClient.SqlConnection(MiConexion)
        If IsNumeric(Me.ComboBox4.SelectedValue) = True And Me.ComboBox4.SelectedValue > 0 Then
            cone1.Open()
            elimina_serv()
            elimina_Dias()
            Me.ConServiciosTelTableAdapter.Connection = cone1
            Me.ConServiciosTelTableAdapter.Delete(Me.Clv_ServicioTextBox.Text, Me.ComboBox4.SelectedValue)
            cone1.Close()
            BorServiciosTelRecontratacion(Me.Clv_ServicioTextBox.Text, Me.ComboBox4.SelectedValue)
            bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Eliminar en Precios por Tipo de Cobro", "Se Elimin� Cobro de Servicio " + CStr(Me.Clv_ServicioTextBox.Text), "En el Cobro de " + CStr(Me.ComboBox4.Text), GloCiudad)
            MsgBox("Se Ha Borrado con �xito", MsgBoxStyle.Information)
            Me.Panel15.Enabled = True
            Me.Panel21.Enabled = False
            activa(True)
            Elimina_Minutos_Cel(1)
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If Me.CheckBox2.CheckState = CheckState.Checked Then
            Me.NumericUpDown8.Enabled = False
            'Me.CheckBox2.Checked = True
        Else
            'Me.CheckBox2.Checked = False
            Me.NumericUpDown8.Enabled = True
        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.CheckBox1.CheckState = CheckState.Checked Then
            ' Me.CheckBox1.Checked = False
            Me.NumericUpDown1.Enabled = False

        Else
            ' Me.CheckBox1.Checked = True
            Me.NumericUpDown1.Enabled = True
        End If
    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If Me.CheckBox3.CheckState = CheckState.Checked Then
            'Me.CheckBox3.Checked = False
            Me.NumericUpDown3.Enabled = False
        Else
            'Me.CheckBox3.Checked = True
            Me.NumericUpDown3.Enabled = True
        End If
    End Sub

    Private Sub CheckBox4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox4.CheckedChanged
        If Me.CheckBox4.CheckState = CheckState.Checked Then
            'Me.CheckBox4.Checked = False
            Me.NumericUpDown7.Enabled = False
        Else
            'Me.CheckBox4.Checked = True
            Me.NumericUpDown7.Enabled = True
        End If
    End Sub

    Private Sub CheckBox5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox5.CheckedChanged
        If Me.CheckBox5.CheckState = CheckState.Checked Then
            'Me.CheckBox5.Checked = False
            Me.NumericUpDown4.Enabled = False
        Else
            'Me.CheckBox5.Checked = True
            Me.NumericUpDown4.Enabled = True
        End If
    End Sub

    '**************************
    Protected Sub DameClv_Session()
        'Se genera una nueva clave de session 
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("DAME_Session_ServiciosDigitales", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New SqlParameter( _
                  "@Clv_Session", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Output
        prm.Value = 0
        cmd.Parameters.Add(prm)

        Try
            con.Open()
            Dim i As Integer = cmd.ExecuteNonQuery()
            Dim gloclave As Integer = 0
            'Asignamos el valor al TxtClv_Session
            TxtClv_Session.Text = prm.Value
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            If con IsNot Nothing Then
                con.Close()
            End If
        End Try
        con.Close()
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

        If CInt(Me.ComboBox1.SelectedValue) = 0 Then
            'Limpio los textBox
            Me.TxtBoxCosto.Text = ""
        Else
            'Llena los TextBox
            DameDatos()

        End If

    End Sub

    Protected Sub Modifica_Dias_Aplica()

        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("MODIFICA_Dias_Aplica", con)

        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New Data.SqlClient.SqlParameter( _
                 "@Clv_Servicio", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.Clv_ServicioTextBox.Text)
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                  "@Clv_Tipocobro", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox4.SelectedValue)
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
               "@Dia_Inicio", SqlDbType.Int)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox2.SelectedValue)
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
              "@Dia_Final", SqlDbType.Int)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox3.SelectedValue)
        cmd.Parameters.Add(prm)

        Try
            con.Open()

            Dim i As Integer = cmd.ExecuteNonQuery()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            If con IsNot Nothing Then
                con.Close()
            End If
        End Try
        con.Close()

    End Sub

    Protected Sub Nuevo_Dias_Aplica()

        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("NUEVO_Dias_Aplica", con)

        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New Data.SqlClient.SqlParameter( _
                 "@Clv_Servicio", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.Clv_ServicioTextBox.Text)
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                  "@Clv_Tipocobro", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox4.SelectedValue)
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
               "@Dia_Inicio", SqlDbType.Int)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox2.SelectedValue)
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
              "@Dia_Final", SqlDbType.Int)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox3.SelectedValue)
        cmd.Parameters.Add(prm)

        Try
            con.Open()

            Dim i As Integer = cmd.ExecuteNonQuery()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            If con IsNot Nothing Then
                con.Close()
            End If
        End Try
        con.Close()

    End Sub
    Protected Sub DameDatos()

        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("Consulta_ServiciosDigitales", con)

        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New Data.SqlClient.SqlParameter( _
                  "@clv_ServicioDigital", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox1.SelectedValue)
        cmd.Parameters.Add(prm)

        con.Open()

        Dim reader As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Try
            While reader.Read()

                'Llenamos los TxtBox con los datos respectivos
                Me.TxtBoxCosto.Text = reader(2).ToString 'Costo

            End While
        Finally
            reader.Close()
        End Try
        con.Close()

    End Sub
    Private Sub Agregar()
        '******** BOTON DE AGREGAR ********
        'Validaci�n de los camp�s a Agregar
        If CInt(Me.ComboBox4.SelectedValue) = 0 Then
            MsgBox("SELECCIONE EL TIPO DE COBRO", MsgBoxStyle.Information)
            Me.ComboBox4.Focus()
            Exit Sub
        End If

        If CInt(Me.ComboBox1.SelectedValue) = 0 Then
            MsgBox("SELECCIONE EL SERVICIO DIGITAL", MsgBoxStyle.Information)
            Me.ComboBox1.Focus()
            Exit Sub
        End If

        If IsNumeric(Me.TxtBoxCosto.Text) = False Then
            MsgBox("INGRESE EL IMPORTE", MsgBoxStyle.Information)
            Me.TxtBoxCosto.Text = ""
            Me.TxtBoxCosto.Focus()
            Exit Sub
        End If

        'If CDec(Me.TxtBoxCosto.Text) <= 0 Then
        '    MsgBox("INGRESE EL IMPORTE", MsgBoxStyle.Information)
        '    Me.TxtBoxCosto.Text = ""
        '    Me.TxtBoxCosto.Focus()
        '    Exit Sub
        'End If

        'Cuando AGREGA Inserta los datos en la tabla PreServiciosDigitales_Telefonia
        'y todavia NO GUARDA en ServiciosDigitales_Telefonia

        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("Nuevo_Rel_ServiciosDigitales_Tel_TMP", con)
        cmd.CommandType = CommandType.StoredProcedure

        '@Clv_Session bigint, @Clv_Servicio bigint, @Clv_Tipocobro bigint, @Clv_ServicioDigital bigint, @Costo MONEY

        Dim prm As New SqlParameter( _
                  "@Clv_Session", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        If Me.TxtClv_Session.Text = "" Then
            prm.Value = 0
        Else
            prm.Value = CInt(Me.TxtClv_Session.Text)
        End If
        cmd.Parameters.Add(prm)


        prm = New SqlParameter( _
                  "@Clv_Servicio", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        If Me.Clv_ServicioTextBox.Text = "" Then
            prm.Value = 0
        Else
            prm.Value = CInt(Me.Clv_ServicioTextBox.Text)
        End If
        cmd.Parameters.Add(prm)


        prm = New SqlParameter( _
                 "@Clv_Tipocobro", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox4.SelectedValue)
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                "@Clv_ServicioDigital", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox1.SelectedValue)
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                 "@Costo", SqlDbType.Money)
        prm.Direction = ParameterDirection.Input
        prm.Value = CDec(Me.TxtBoxCosto.Text)
        cmd.Parameters.Add(prm)

        Try
            con.Open()

            Dim i As Integer = cmd.ExecuteNonQuery()

            'Limpio los campos.
            Me.TxtBoxCosto.Text = ""
            Me.ComboBox1.SelectedValue = 0

            'Llamado a busca para llenar el Grid
            busca_Servicios()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            If con IsNot Nothing Then
                con.Close()
            End If
        End Try
        con.Close()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Agregar()
    End Sub

    Protected Sub busca_Servicios()

        Dim con As New SqlClient.SqlConnection(MiConexion)
        con.Open()

        Me.Consultar_Rel_ServiciosDigitales_Tel_TMPTableAdapter.Connection = con

        If Me.Clv_ServicioTextBox.Text = "" Then
            Me.Consultar_Rel_ServiciosDigitales_Tel_TMPTableAdapter.Fill(Me.DataSetyahve.Consultar_Rel_ServiciosDigitales_Tel_TMP, CInt(Me.TxtClv_Session.Text), 0, CInt(Me.ComboBox4.SelectedValue))
        Else
            Me.Consultar_Rel_ServiciosDigitales_Tel_TMPTableAdapter.Fill(Me.DataSetyahve.Consultar_Rel_ServiciosDigitales_Tel_TMP, CInt(Me.TxtClv_Session.Text), CInt(Me.Clv_ServicioTextBox.Text), CInt(Me.ComboBox4.SelectedValue))
        End If

        con.Close()

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        Borra_PreServicios()
    End Sub

    Protected Sub Borra_Datos()

        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("Borra_Datos_Tel", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New SqlParameter( _
                  "@Clv_Servicio", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        If Me.Clv_ServicioTextBox.Text = "" Then
            prm.Value = 0
        Else
            prm.Value = CInt(Me.Clv_ServicioTextBox.Text)
        End If
        cmd.Parameters.Add(prm)


        Try
            con.Open()

            Dim i As Integer = cmd.ExecuteNonQuery()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            If con IsNot Nothing Then
                con.Close()
            End If
        End Try
        con.Close()

        CONSULTA_Dias_Aplica()
        'Llamado a busca para llenar el Grid
        busca_Servicios()

    End Sub

    Protected Sub Borra_PreServicios()
        Try
            Dim con As New SqlConnection(MiConexion)
            con.Open()

            Me.Borrar_Rel_ServiciosDigitales_Tel_TMPTableAdapter.Connection = con
            If IsNumeric(Me.Clv_ServicioTextBox.Text) = False Then
                Me.Clv_ServicioTextBox.Text = 0
            End If

            If IsNumeric(Me.Clv_ServicioDigitalTextBox.Text) = True Then
                Me.Borrar_Rel_ServiciosDigitales_Tel_TMPTableAdapter.Fill(Me.DataSetyahve.Borrar_Rel_ServiciosDigitales_Tel_TMP, CInt(Me.TxtClv_Session.Text), CInt(Me.Clv_ServicioTextBox.Text), CInt(Me.ComboBox4.SelectedValue), CInt(Me.Clv_ServicioDigitalTextBox.Text))
            End If
            con.Close()
            'Llamado a busca para llenar el Grid
            busca_Servicios()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Protected Sub Guarda_ServiciosDigitales_Tel()
        'Cuando GUARDA Inserta los datos en la tabla ServiciosDigitales_Telefonia

        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("Guarda_Rel_ServiciosDigitales_Tel", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New SqlParameter( _
                  "@Clv_session", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        If Me.TxtClv_Session.Text = "" Then
            prm.Value = 0
        Else
            prm.Value = CInt(Me.TxtClv_Session.Text)
        End If
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                  "@Clv_Servicio", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        If Me.Clv_ServicioTextBox.Text = "" Then
            prm.Value = 0
        Else
            prm.Value = CInt(Me.Clv_ServicioTextBox.Text)
        End If
        cmd.Parameters.Add(prm)

        Try
            con.Open()

            Dim i As Integer = cmd.ExecuteNonQuery()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            If con IsNot Nothing Then
                con.Close()
            End If
        End Try
        con.Close()
    End Sub
    Protected Sub elimina_Dias()

        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("Elimina_Dias_Aplica", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New SqlParameter( _
                  "@Clv_Servicio", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        If Me.Clv_ServicioTextBox.Text = "" Then
            prm.Value = 0
        Else
            prm.Value = CInt(Me.Clv_ServicioTextBox.Text)
        End If
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                 "@Clv_Tipocobro", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox4.SelectedValue)
        cmd.Parameters.Add(prm)

        Try
            con.Open()

            Dim i As Integer = cmd.ExecuteNonQuery()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            If con IsNot Nothing Then
                con.Close()
            End If
        End Try
        con.Close()
        CONSULTA_Dias_Aplica()
    End Sub

    Protected Sub elimina_serv()
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("Elimina_ServiciosDigitales_tel", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New SqlParameter( _
                  "@Clv_Servicio", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        If Me.Clv_ServicioTextBox.Text = "" Then
            prm.Value = 0
        Else
            prm.Value = CInt(Me.Clv_ServicioTextBox.Text)
        End If
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                 "@Clv_Tipocobro", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox4.SelectedValue)
        cmd.Parameters.Add(prm)

        Try
            con.Open()

            Dim i As Integer = cmd.ExecuteNonQuery()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            If con IsNot Nothing Then
                con.Close()
            End If
        End Try
        con.Close()

        'Llamado a busca para llenar el Grid
        busca_Servicios()
    End Sub

    Private Sub TabPage5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabPage5.Click

    End Sub

    Private Sub TxtBoxCosto_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtBoxCosto.KeyPress
        e.KeyChar = ChrW(ValidaKey(TxtBoxCosto, Asc(e.KeyChar), "L"))
        If Asc(e.KeyChar) = 13 Then
            Agregar()
        End If
    End Sub

    Private Sub CostoTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CostoTextBox.TextChanged
        If Me.ComboBox1.Enabled = True Then
            Me.ComboBox1.SelectedValue = CInt(Me.Clv_ServicioDigitalTextBox.Text)
            Me.TxtBoxCosto.Text = Me.CostoTextBox.Text
        End If
    End Sub

    Private Sub Panel9_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel9.Paint

    End Sub

    Private Sub TextBox15_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox15.KeyPress
        e.KeyChar = ChrW(ValidaKey(TextBox15, Asc(e.KeyChar), "L"))
    End Sub

    Private Sub TextBox15_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox15.TextChanged
        If (Me.CheckBox7.CheckState = True) Then
            Me.NumericUpDown6.Enabled = False
            Me.TextBox15.Enabled = False
        Else
            Me.NumericUpDown6.Enabled = True
            Me.TextBox15.Enabled = True
        End If
    End Sub

    Private Sub NumericUpDown6_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDown6.ValueChanged

    End Sub

    Private Sub CheckBox7_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox7.CheckedChanged
        If (Me.CheckBox7.Checked = True) Then
            Me.NumericUpDown6.Enabled = False
            Me.NumericUpDown6.Value = 0
            Me.TextBox15.Enabled = False
            Me.TextBox15.Text = 0
        Else
            Me.NumericUpDown6.Enabled = True
            Me.TextBox15.Enabled = True
        End If
    End Sub

    Private Sub CheckBox8_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox8.CheckedChanged

        If (Me.CheckBox8.Checked = True) Then
            Me.NumericUpDown9.Enabled = False
            Me.NumericUpDown9.Value = 0
            Me.TextBox16.Enabled = False
            Me.TextBox16.Text = 0
        Else
            Me.NumericUpDown9.Enabled = True
            Me.TextBox16.Enabled = True
        End If
    End Sub

    Private Sub TextBox16_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox16.KeyPress
        e.KeyChar = ChrW(ValidaKey(TextBox16, Asc(e.KeyChar), "L"))
    End Sub
    Private Sub CheckBox9_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox9.CheckedChanged
        If Me.CheckBox10.Checked = True Then
            Me.CheckBox10.Checked = False
        End If
    End Sub

    Private Sub CheckBox10_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox10.CheckedChanged
        If Me.CheckBox9.Checked = True Then
            Me.CheckBox9.Checked = False
        End If
    End Sub
    Private Sub Guarda_Servicios_Poliza()
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Guarda_Servicios_Poliza", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@cuenta", SqlDbType.VarChar, 100)
        par1.Direction = ParameterDirection.Input
        par1.Value = Me.TextBox20.Text
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@posicion", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = CInt(Me.TextBox19.Text)
        com.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@clv_txt", SqlDbType.VarChar, 20)
        par3.Direction = ParameterDirection.Input
        par3.Value = Me.Clv_TxtTextBox.Text
        com.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@clv_tipserv", SqlDbType.Int)
        par4.Direction = ParameterDirection.Input
        par4.Value = GloClv_TipSer
        com.Parameters.Add(par4)

        Try
            con.Open()
            com.ExecuteNonQuery()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            con.Close()
            con.Dispose()

        End Try
    End Sub

    Private Sub Es_PrincipalCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Es_PrincipalCheckBox.CheckedChanged
        If Me.Es_PrincipalCheckBox.Checked = True Then

            Me.Label31.Visible = False
            Me.Label30.Visible = False
            Me.TextBox20.Visible = False
            Me.TextBox19.Visible = False
        Else
            Me.Label31.Visible = True
            Me.Label30.Visible = True
            Me.TextBox20.Visible = True
            Me.TextBox19.Visible = True
        End If


    End Sub
    Private Sub guarda_cuanta()

        If Len(Me.TextBox20.Text) > 0 Then

            If IsNumeric(Me.TextBox19.Text) = False Then
                MsgBox("El valor de la posicion debe ser numerico")
            Else
                Guarda_Servicios_Poliza()
            End If

        End If

    End Sub
    Private Sub Muestra_Cuenta()
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Muestra_Cuenta", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@clv_text", SqlDbType.VarChar, 50)
        par1.Direction = ParameterDirection.Input
        par1.Value = Me.Clv_TxtTextBox.Text
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@cuenta", SqlDbType.VarChar, 100)
        par2.Direction = ParameterDirection.Output
        com.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@posicion", SqlDbType.Int)
        par3.Direction = ParameterDirection.Output
        com.Parameters.Add(par3)

        Try
            con.Open()
            com.ExecuteNonQuery()
            Me.TextBox20.Text = par2.Value.ToString
            If IsDBNull(par2.Value) Then
                par2.Value = 0
            End If
            Me.TextBox19.Text = CStr(par3.Value)

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            con.Close()
            con.Dispose()

        End Try
    End Sub

    Private Sub NumericUpDown5_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDown5.ValueChanged

    End Sub

    Private Sub ModServiciosTelRecontratacion(ByVal Clv_Servicio As Integer, ByVal Clv_TipoCliente As Integer, ByVal Costo As Decimal)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ModServiciosTelRecontratacion", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        par.Direction = ParameterDirection.Input
        par.Value = Clv_Servicio
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@Clv_TipoCliente", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_TipoCliente
        comando.Parameters.Add(par2)


        Dim par3 As New SqlParameter("@Costo", SqlDbType.Decimal)
        par3.Direction = ParameterDirection.Input
        par3.Value = Costo
        comando.Parameters.Add(par3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Close()
        End Try
    End Sub

    Private Sub ConServiciosTelRecontratacion(ByVal Clv_Servicio As Integer, ByVal Clv_TipoCliente As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConServiciosTelRecontratacion", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        Dim reader As SqlDataReader

        Dim par As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        par.Direction = ParameterDirection.Input
        par.Value = Clv_Servicio
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@Clv_TipoCliente", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_TipoCliente
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            reader = comando.ExecuteReader
            txtRecontratacion.Text = ""
            While (reader.Read())
                txtRecontratacion.Text = reader(0).ToString
            End While

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Close()
        End Try

    End Sub

    Private Sub BorServiciosTelRecontratacion(ByVal Clv_Servicio As Integer, ByVal Clv_TipoCliente As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorServiciosTelRecontratacion", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0


        Dim par As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        par.Direction = ParameterDirection.Input
        par.Value = Clv_Servicio
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@Clv_TipoCliente", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_TipoCliente
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Close()
        End Try

    End Sub

  
    Private Sub btnEliminarPaqAdic_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim Respuesta = MsgBox("�Deseas eliminar el Paquete Adicional?", MsgBoxStyle.YesNoCancel, "�BORRAR PAQUETE?")

        If Respuesta = MsgBoxResult.Yes Then
            If Me.dgPaquetesAdicionales.RowCount = 0 Then
                MsgBox("No hay Paquetes Adicionales que eliminar.", MsgBoxStyle.Information, "NO HAY PAQUES QUE ELIMINAR")
                Exit Sub
            End If

            Dim idRel As Integer = 0
            idRel = Me.dgPaquetesAdicionales.SelectedCells(0).Value

            Paquete.Delete_RelPaqueteAdicionalServTel(idRel)
            EnlazaDatos()

        End If



    End Sub

    Private Sub EnlazaDatos()

        Try

            Me.cmbPaqAdic.DataSource = Nothing
            Dim dsPaquetesAdicionales As New DataSet

            dsPaquetesAdicionales = Paquete.GetPaquetesDisponiblesYAsignados(CType(Me.Clv_ServicioTextBox.Text, Integer))
            dsPaquetesAdicionales.Tables(0).TableName = "PaquetesDisponibles"
            dsPaquetesAdicionales.Tables(1).TableName = "PaquetesAsignados"

            Me.cmbPaqAdic.DataSource = dsPaquetesAdicionales.Tables("PaquetesDisponibles")
            Me.cmbPaqAdic.DisplayMember = "NOMBRE"
            Me.cmbPaqAdic.ValueMember = "ID"

            Me.dgPaquetesAdicionales.DataSource = dsPaquetesAdicionales.Tables("PaquetesAsignados")
            Me.dgPaquetesAdicionales.Columns(0).Visible = False
            Me.dgPaquetesAdicionales.Columns(1).Visible = False
            Me.dgPaquetesAdicionales.Columns(2).HeaderText = "Nombre del Paquete"
            Me.dgPaquetesAdicionales.Columns(3).HeaderText = "Contratacion:"
            Me.dgPaquetesAdicionales.Columns(4).HeaderText = "Mensualidad:"
            Me.dgPaquetesAdicionales.Columns(5).HeaderText = "Costo Adicional:"
            Me.dgPaquetesAdicionales.Columns(6).HeaderText = "Aplica Tarifa Preferencial:"

            Me.dgPaquetesAdicionales.Columns(2).Width = 310
            Me.dgPaquetesAdicionales.Columns(3).Width = 90
            Me.dgPaquetesAdicionales.Columns(4).Width = 90
            Me.dgPaquetesAdicionales.Columns(5).Width = 90

            If Me.cmbPaqAdic.Items.Count = 0 Then
                Me.cmbPaqAdic.Enabled = False
                Me.txtContratacion.Text = "0"
                Me.txtMensualidad.Text = "0"
                Me.txtCostoAdicional.Text = "0"
                Me.btnAgregarPaqAdic.Enabled = False
            Else
                Me.cmbPaqAdic.Enabled = True
                Me.txtContratacion.Text = "0"
                Me.txtMensualidad.Text = "0"
                Me.txtCostoAdicional.Text = "0"
                Me.btnAgregarPaqAdic.Enabled = True
            End If

        Catch ex As Exception

        End Try

        

    End Sub

    Private Sub pbAyuda01_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbAyuda01.Click
        MsgBox("Los costos que aqu� se establescan ser�n tomados al momento que se asigne el Plan Tarifario al cliente.", MsgBoxStyle.Information, "PRECIOS CON PLAN TARIFARIO")
    End Sub

    Private Sub btnAgregarPaqAdic_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarPaqAdic.Click
        If Len(Me.txtContratacion.Text) = 0 Or IsNumeric(Me.txtContratacion.Text) = False Or Len(Me.txtMensualidad.Text) = 0 Or IsNumeric(Me.txtMensualidad.Text) = False Or Len(Me.txtCostoAdicional.Text) = 0 Or IsNumeric(Me.txtCostoAdicional.Text) = False Then
            MsgBox("El Costo del Paquete Adicional NO es un costo v�lido.", MsgBoxStyle.Information, "COSTO NO V�LIDO")
            Exit Sub
        End If

        Dim IdRel As Integer = 0
        IdRel = Paquete.Add_RelPaqueteAdicionalServTel(CType(Me.Clv_ServicioTextBox.Text, Integer), Me.cmbPaqAdic.SelectedValue, Me.txtContratacion.Text, Me.txtMensualidad.Text, txtCostoAdicional.Text, Me.chbxAplicaTarifa_PaqAdic.Checked)
        EnlazaDatos()
    End Sub

    Private Sub btnEliminarPaqAdic_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminarPaqAdic.Click
        Dim Respuesta = MsgBox("�Deseas eliminar el Paquete Adicional?", MsgBoxStyle.YesNoCancel, "�BORRAR PAQUETE?")

        If Respuesta = MsgBoxResult.Yes Then
            If Me.dgPaquetesAdicionales.RowCount = 0 Then
                MsgBox("No hay Paquetes Adicionales que eliminar.", MsgBoxStyle.Information, "NO HAY PAQUES QUE ELIMINAR")
                Exit Sub
            End If

            Dim idRel As Integer = 0
            idRel = Me.dgPaquetesAdicionales.SelectedCells(0).Value

            Paquete.Delete_RelPaqueteAdicionalServTel(idRel)
            EnlazaDatos()

        End If

    End Sub

    Private Sub chbxAplicaTarifa_PaqAdic_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chbxAplicaTarifa_PaqAdic.CheckedChanged
        If Me.chbxAplicaTarifa_PaqAdic.CheckState = CheckState.Checked Then

            Me.txtCostoAdicional.Enabled = True

        Else

            Me.txtCostoAdicional.Enabled = False
            Me.txtCostoAdicional.Text = 0

        End If
    End Sub



    Private Sub bnCostosContratacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnCostosContratacion.Click

        'SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        SoftvMod.VariablesGlobales.Clv_Servicio = Clv_ServicioTextBox.Text
        SoftvMod.VariablesGlobales.Opcion = opcion
        SoftvMod.VariablesGlobales.Clv_Usuario = GloTipoUsuario
        SoftvMod.VariablesGlobales.Clv_TipoCliente = ComboBox4.SelectedValue
        Dim frm As New SoftvMod.FrmCostoContratacionTelefonia
        frm.Show()

    End Sub

    Private Sub bnCostosRecontratacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnCostosRecontratacion.Click
       
        'SoftvMod.VariablesGlobales.Clv_TipoCliente = GloTipoUsuario
        SoftvMod.VariablesGlobales.GloClaveMenu = GloClaveMenus
        SoftvMod.VariablesGlobales.MiConexion = MiConexion
        SoftvMod.VariablesGlobales.Clv_Servicio = Clv_ServicioTextBox.Text
        SoftvMod.VariablesGlobales.Opcion = opcion
        SoftvMod.VariablesGlobales.Clv_Usuario = GloTipoUsuario
        SoftvMod.VariablesGlobales.Clv_TipoCliente = ComboBox4.SelectedValue
        Dim frm As New SoftvMod.FrmCostoRecontratacionTelefonia
        frm.Show()

    End Sub


    Private Sub GuardaSiEs_Telmex(ByVal ClvServ As Integer, ByVal BndEsOnoTelmex As Boolean)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.BigInt, ClvServ)
        BaseII.CreateMyParameter("@BndEsOnoTelmex", SqlDbType.Bit, BndEsOnoTelmex)
        BaseII.Inserta("GuardaSiEs_Telmex")
    End Sub

    Private Sub GuardaTelmex_RelMedioServicio(ByVal ClvServ As Integer)

        Try
            If opcion = "N" Then
                'And Me.CheckBox4.Checked = True Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@ClvSessionTelmexTempo", SqlDbType.BigInt, ClvSessionTelmexTempo)
                BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.BigInt, ClvServ)
                BaseII.Inserta("Add_RelServicioMedio_Telmex_SrvNuevo2")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub DimeSiEs_Telmex()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, GloClv_Servicio)
        BaseII.CreateMyParameter("@bndEsTelmex", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("DimeSiEs_Telmex")
        EsTelmex_CheckBox.Checked = BaseII.dicoPar("@bndEsTelmex")

    End Sub

    Private Sub DimeSi_ServicioTieneMedios_Telmex()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, GloClv_Servicio)
        BaseII.CreateMyParameter("@bndSiOnoTiene", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("DimeSi_ServicioTieneMedios_Telmex")
        tieneMedios = BaseII.dicoPar("@bndSiOnoTiene")

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        FrmTelmexMedios.Show()
    End Sub


    Private Sub EsTelmex_CheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles EsTelmex_CheckBox.CheckedChanged
        If EsTelmex_CheckBox.Checked = True Then
            Button3.Visible = True
        Else
            Button3.Visible = False
        End If
    End Sub







#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region
End Class

