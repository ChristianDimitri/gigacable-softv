﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwMTAsAsignacionNumTel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dgvMTAs = New System.Windows.Forms.DataGridView()
        Me.id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.detalle1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.detalle2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.detalle3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.detalle4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.detalle5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnBucar = New System.Windows.Forms.Button()
        Me.txtMACMTA = New System.Windows.Forms.TextBox()
        Me.lblMACMTA = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtIDAparato = New System.Windows.Forms.TextBox()
        Me.gbxBusqueda = New System.Windows.Forms.GroupBox()
        Me.txtMAC = New System.Windows.Forms.TextBox()
        Me.lblMAC = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvMTAs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxBusqueda.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvMTAs)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(211, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(489, 578)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Listado de ATAs"
        '
        'dgvMTAs
        '
        Me.dgvMTAs.AllowUserToAddRows = False
        Me.dgvMTAs.AllowUserToDeleteRows = False
        Me.dgvMTAs.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvMTAs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMTAs.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.id, Me.detalle1, Me.detalle2, Me.detalle3, Me.detalle4, Me.detalle5})
        Me.dgvMTAs.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvMTAs.Location = New System.Drawing.Point(3, 17)
        Me.dgvMTAs.Name = "dgvMTAs"
        Me.dgvMTAs.ReadOnly = True
        Me.dgvMTAs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvMTAs.Size = New System.Drawing.Size(483, 558)
        Me.dgvMTAs.TabIndex = 0
        '
        'id
        '
        Me.id.DataPropertyName = "id"
        Me.id.HeaderText = "ID Aparato"
        Me.id.Name = "id"
        Me.id.ReadOnly = True
        '
        'detalle1
        '
        Me.detalle1.DataPropertyName = "detalle1"
        Me.detalle1.HeaderText = "ATA"
        Me.detalle1.Name = "detalle1"
        Me.detalle1.ReadOnly = True
        '
        'detalle2
        '
        Me.detalle2.DataPropertyName = "detalle2"
        Me.detalle2.HeaderText = "MAC MTA"
        Me.detalle2.Name = "detalle2"
        Me.detalle2.ReadOnly = True
        '
        'detalle3
        '
        Me.detalle3.DataPropertyName = "detalle3"
        Me.detalle3.HeaderText = "detalle3"
        Me.detalle3.Name = "detalle3"
        Me.detalle3.ReadOnly = True
        Me.detalle3.Visible = False
        '
        'detalle4
        '
        Me.detalle4.DataPropertyName = "detalle4"
        Me.detalle4.HeaderText = "detalle4"
        Me.detalle4.Name = "detalle4"
        Me.detalle4.ReadOnly = True
        Me.detalle4.Visible = False
        '
        'detalle5
        '
        Me.detalle5.DataPropertyName = "detalle5"
        Me.detalle5.HeaderText = "detalle5"
        Me.detalle5.Name = "detalle5"
        Me.detalle5.ReadOnly = True
        Me.detalle5.Visible = False
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(726, 549)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(106, 38)
        Me.btnSalir.TabIndex = 117
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'btnModificar
        '
        Me.btnModificar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModificar.ForeColor = System.Drawing.Color.Black
        Me.btnModificar.Location = New System.Drawing.Point(720, 27)
        Me.btnModificar.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(106, 38)
        Me.btnModificar.TabIndex = 116
        Me.btnModificar.Text = "MODIFICAR"
        Me.btnModificar.UseVisualStyleBackColor = False
        '
        'btnBucar
        '
        Me.btnBucar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnBucar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBucar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBucar.ForeColor = System.Drawing.Color.Black
        Me.btnBucar.Location = New System.Drawing.Point(5, 204)
        Me.btnBucar.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnBucar.Name = "btnBucar"
        Me.btnBucar.Size = New System.Drawing.Size(100, 40)
        Me.btnBucar.TabIndex = 118
        Me.btnBucar.Text = "BUSCAR"
        Me.btnBucar.UseVisualStyleBackColor = False
        '
        'txtMACMTA
        '
        Me.txtMACMTA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMACMTA.Location = New System.Drawing.Point(6, 144)
        Me.txtMACMTA.Name = "txtMACMTA"
        Me.txtMACMTA.Size = New System.Drawing.Size(187, 22)
        Me.txtMACMTA.TabIndex = 119
        '
        'lblMACMTA
        '
        Me.lblMACMTA.AutoSize = True
        Me.lblMACMTA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMACMTA.Location = New System.Drawing.Point(6, 128)
        Me.lblMACMTA.Name = "lblMACMTA"
        Me.lblMACMTA.Size = New System.Drawing.Size(67, 13)
        Me.lblMACMTA.TabIndex = 120
        Me.lblMACMTA.Text = "MAC MTA:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(7, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 13)
        Me.Label1.TabIndex = 122
        Me.Label1.Text = "ID Aparato:"
        '
        'txtIDAparato
        '
        Me.txtIDAparato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIDAparato.Location = New System.Drawing.Point(7, 44)
        Me.txtIDAparato.Name = "txtIDAparato"
        Me.txtIDAparato.Size = New System.Drawing.Size(118, 22)
        Me.txtIDAparato.TabIndex = 121
        '
        'gbxBusqueda
        '
        Me.gbxBusqueda.Controls.Add(Me.txtMAC)
        Me.gbxBusqueda.Controls.Add(Me.lblMAC)
        Me.gbxBusqueda.Controls.Add(Me.txtIDAparato)
        Me.gbxBusqueda.Controls.Add(Me.Label1)
        Me.gbxBusqueda.Controls.Add(Me.txtMACMTA)
        Me.gbxBusqueda.Controls.Add(Me.lblMACMTA)
        Me.gbxBusqueda.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxBusqueda.Location = New System.Drawing.Point(5, 12)
        Me.gbxBusqueda.Name = "gbxBusqueda"
        Me.gbxBusqueda.Size = New System.Drawing.Size(200, 185)
        Me.gbxBusqueda.TabIndex = 123
        Me.gbxBusqueda.TabStop = False
        Me.gbxBusqueda.Text = "Filtros"
        '
        'txtMAC
        '
        Me.txtMAC.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMAC.Location = New System.Drawing.Point(6, 88)
        Me.txtMAC.Name = "txtMAC"
        Me.txtMAC.Size = New System.Drawing.Size(187, 22)
        Me.txtMAC.TabIndex = 123
        '
        'lblMAC
        '
        Me.lblMAC.AutoSize = True
        Me.lblMAC.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMAC.Location = New System.Drawing.Point(6, 72)
        Me.lblMAC.Name = "lblMAC"
        Me.lblMAC.Size = New System.Drawing.Size(35, 13)
        Me.lblMAC.TabIndex = 124
        Me.lblMAC.Text = "ATA:"
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'BrwMTAsAsignacionNumTel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(844, 602)
        Me.Controls.Add(Me.gbxBusqueda)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnBucar)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.GroupBox1)
        Me.MaximizeBox = False
        Me.Name = "BrwMTAsAsignacionNumTel"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MTAs para asignación de número telefónico"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgvMTAs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxBusqueda.ResumeLayout(False)
        Me.gbxBusqueda.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvMTAs As System.Windows.Forms.DataGridView
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnBucar As System.Windows.Forms.Button
    Friend WithEvents txtMACMTA As System.Windows.Forms.TextBox
    Friend WithEvents lblMACMTA As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtIDAparato As System.Windows.Forms.TextBox
    Friend WithEvents gbxBusqueda As System.Windows.Forms.GroupBox
    Friend WithEvents txtMAC As System.Windows.Forms.TextBox
    Friend WithEvents lblMAC As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents detalle1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents detalle2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents detalle3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents detalle4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents detalle5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
