﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Xml
Imports System.Text
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System
Imports System.IO.StreamReader
Imports System.IO.File
Public Class BwrCliPortabilidad
    Private customersByCityReport As ReportDocument
    Dim Impresora_Tarjetas As String = Nothing
    Dim Impresora_Contratos As String = Nothing

    Private Sub BwrCliPortabilidad_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

        'If Me.rdbPendiente.Checked = True Then
        '    VerPorStatusPortabilidad(2)
        'ElseIf Me.rdbAvtiva.Checked = True Then
        '    VerPorStatusPortabilidad(1)
        'ElseIf Me.rdbAmbas.Checked = True Then
        '    VerPorStatusPortabilidad(3)
        'End If
        'DataGridView1.Refresh()

    End Sub
    Private Sub BwrCliPortabilidad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)
        MuestraClientesPortabilidad()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub
    Private Sub MuestraClientesPortabilidad()

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraClientesPortabilidad ")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            
            Me.DataGridView1.DataSource = bindingSource

            Me.DataGridView1.Columns(0).Width = 100
            Me.DataGridView1.Columns(1).Width = 350
            Me.DataGridView1.Columns(2).Width = 100

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
    Private Sub datos()
        Try
            Me.folio.Text = (Me.DataGridView1.Item(0, Me.DataGridView1.CurrentRow.Index).Value)
            Me.contrato.Text = (Me.DataGridView1.Item(2, Me.DataGridView1.CurrentRow.Index).Value)
            Me.nombre.Text = (Me.DataGridView1.Item(1, Me.DataGridView1.CurrentRow.Index).Value)
           
        Catch ex As Exception

        End Try


    End Sub
    Private Sub DataGridView1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        datos()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If IsNumeric(Me.folio.Text) = True Then
            opcionport = "C"
            folioport = CLng(Me.folio.Text)
            contraoport = CLng(Me.contrato.Text)
            FrmDatosPortabilidad.Show()
        Else
            MsgBox("Debe seleccionar un renglon valido", MsgBoxStyle.Information)
        End If

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If IsNumeric(Me.folio.Text) = True Then
            opcionport = "M"
            folioport = CLng(Me.folio.Text)
            contraoport = CLng(Me.contrato.Text)
            FrmDatosPortabilidad.Show()
        Else
            MsgBox("Debe seleccionar un renglon valido", MsgBoxStyle.Information)
        End If

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        If IsNumeric(Me.TextBox1.Text) = True Then
            BuscarCliente("", CLng(Me.TextBox1.Text), 0)
        End If
        limpia()
    End Sub

    Private Sub BuscarCliente(ByVal nombre As String, ByVal contrato As Long, ByVal folio As Long)

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC BuscarCliente ")
        strSQL.Append("'" & CStr(nombre) & "'" & ",")
        strSQL.Append(CStr(contrato) & ",")
        strSQL.Append(CStr(folio))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable

            Me.DataGridView1.DataSource = bindingSource

            Me.DataGridView1.Columns(0).Width = 100
            Me.DataGridView1.Columns(1).Width = 350
            Me.DataGridView1.Columns(2).Width = 100

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If IsNumeric(Me.TextBox2.Text) = True Then
            BuscarCliente("", 0, CLng(Me.TextBox2.Text))
        End If
        limpia()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Len(Me.TextBox3.Text) > 0 Then
            BuscarCliente(Me.TextBox3.Text, 0, 0)
        End If
        limpia()
    End Sub
    Private Sub limpia()
        Me.TextBox1.Clear()
        Me.TextBox2.Clear()
        Me.TextBox3.Clear()
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        If IsNumeric(Me.folio.Text) = True Then
            folioport = CLng(Me.folio.Text)
            'Selecciona_Impresora_Sucursal(GloClv_Sucursal)
            'ConfigureCrystalReportsPortabilidad(folioport)
            FrmImprimirAux.Show()
        Else
            MsgBox("Debe seleccionar un renglon valido", MsgBoxStyle.Information)
        End If
       
    End Sub
    Private Sub ConfigureCrystalReportsPortabilidad_NewXml(ByVal FolioS As Long)

        Dim impresora As String = Nothing
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo

        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\repotrtePortabilidad.rpt"
        'reportPath = "D:\Exes\Softv\reportes\repotrtePortabilidad.rpt"
        Dim cnn As New SqlConnection(MiConexion)

        Dim cmd As New SqlCommand("ReportePortabilidad", cnn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Folio", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = FolioS
        cmd.Parameters.Add(parametro)


        Dim da As New SqlDataAdapter(cmd)

        Dim ds As New DataSet()

        Dim data1 As New DataTable()
        Dim data2 As New DataTable()
        Dim data3 As New DataTable()
        Dim data4 As New DataTable()

        Dim cmd2 As New SqlCommand("RPTrelClienTelPortabilidad", cnn)
        cmd2.CommandType = CommandType.StoredProcedure

        Dim parametro1 As New SqlParameter("@Folio", SqlDbType.BigInt)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = FolioS
        cmd2.Parameters.Add(parametro1)
        Dim da2 As New SqlDataAdapter(cmd2)


        Dim cmd3 As New SqlCommand("Select * from General", cnn)
        cmd3.CommandType = CommandType.Text
        Dim da3 As New SqlDataAdapter(cmd3)

        Dim cmd4 As New SqlCommand("select * from IntevaloClienTelPortabilidad", cnn)
        cmd4.CommandType = CommandType.Text
        Dim da4 As New SqlDataAdapter(cmd4)

        da.Fill(data1)
        da2.Fill(data2)
        da3.Fill(data3)
        da4.Fill(data4)

        Try
            customersByCityReport.PrintOptions.PrinterName = Impresora_Contratos
            customersByCityReport.PrintToPrinter(0, True, 1, 1)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try



        'customersByCityReport = Nothing


    End Sub
    Private Sub ConfigureCrystalReportsPortabilidad(ByVal FolioS As Long)

        Dim impresora As String = Nothing
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo

        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\repotrtePortabilidad.rpt"
        'reportPath = "D:\Exes\Softv\reportes\repotrtePortabilidad.rpt"
        customersByCityReport.Load(reportPath)


        SetDBLogonForReport(connectionInfo, customersByCityReport)

        '@Contrato 
        customersByCityReport.SetParameterValue(0, FolioS)
        'CrystalReportViewer1.ReportSource = customersByCityReport
        'CrystalReportViewer1.ShowPrintButton = True

        'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)
        Try
            customersByCityReport.PrintOptions.PrinterName = Impresora_Contratos
            customersByCityReport.PrintToPrinter(0, True, 1, 1)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
       


        'customersByCityReport = Nothing


    End Sub
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub
    Private Sub Selecciona_Impresora_Sucursal(ByVal sucursal As Integer)

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Selecciona_Impresora_Sucursal", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@clv_sucursal", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = sucursal
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Impresora_Tarjetas", SqlDbType.VarChar, 50)
        par2.Direction = ParameterDirection.Output
        com.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Impresora_Contratos", SqlDbType.VarChar, 50)
        par3.Direction = ParameterDirection.Output
        com.Parameters.Add(par3)

        Try
            con.Open()
            com.ExecuteNonQuery()
            Impresora_Tarjetas = CStr(par2.Value.ToString)
            Impresora_Contratos = CStr(par3.Value.ToString)
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try

    End Sub
    Public Sub VerPorStatusPortabilidad(ByVal op As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC VerPorStatusPortabilidad ")
        strSQL.Append(CStr(op))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable

            Me.DataGridView1.DataSource = bindingSource

            Me.DataGridView1.Columns(0).Width = 100
            Me.DataGridView1.Columns(1).Width = 350
            Me.DataGridView1.Columns(2).Width = 100

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub rdbPendiente_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbPendiente.CheckedChanged
        If Me.rdbPendiente.Checked = True Then
            VerPorStatusPortabilidad(2)
        End If
    End Sub

    Private Sub rdbAvtiva_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbAvtiva.CheckedChanged
        If Me.rdbAvtiva.Checked = True Then
            VerPorStatusPortabilidad(1)
        End If
    End Sub

    Private Sub rdbAmbas_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbAmbas.CheckedChanged
        If Me.rdbAmbas.Checked = True Then
            VerPorStatusPortabilidad(3)
        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region
End Class