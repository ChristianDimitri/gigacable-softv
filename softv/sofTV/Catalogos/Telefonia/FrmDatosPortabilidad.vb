﻿Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.IO
Imports System.Xml
Imports System.Text
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System
Imports System.IO.StreamReader
Imports System.IO.File
Public Class FrmDatosPortabilidad
    Private customersByCityReport As ReportDocument
    Dim Impresora_Tarjetas As String = Nothing
    Dim Impresora_Contratos As String = Nothing
    Dim tipo As String = Nothing
    Dim empresanew As String = Nothing
    Dim bnd As Boolean = True
    Dim serviciof As String = Nothing
    Dim serviciom As String = Nothing
    Dim noGeo As String = Nothing
    Dim Empresa As String = Nothing
    Dim BndEmpresa As Boolean = False
    Dim cont As Integer = 0
    Dim empOld As Integer = 0
    Dim EmpresOld As String = Nothing
    Dim folio As Long = 0
    Dim imprimir As Boolean = False
    Dim OP As Integer
    Dim errorA As Integer = Nothing

    Public diccionario As Dictionary(Of String, NumeroTelefonico) = New Dictionary(Of String, NumeroTelefonico)
    Public diccionario1 As Dictionary(Of String, NumeroTelefonico) = New Dictionary(Of String, NumeroTelefonico)
    Public diccionario2 As Dictionary(Of String, NumeroTelefonico) = New Dictionary(Of String, NumeroTelefonico)

    Dim listaI As New List(Of NumeroTelefonico)
    Dim listaD As New List(Of NumeroTelefonico)
    Dim listaF As New List(Of NumeroTelefonico)
    Dim NumerosTel As NumeroTelefonico = Nothing
    Dim Datos As String
    Dim contAux As Integer = 0
    Dim contAux1 As Integer = 0



    Private Sub btnAgrega1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgrega1.Click

        Try
           

            If IsNumeric(Me.txtTelefono.Text) = True And Len(Me.txtTelefono.Text) = 10 Then

                ValidaTelefonosPortabilidad(CLng(Me.txtTelefono.Text), 0, 1)

                If OP = 1 Then
                    Exit Sub
                End If

                contAux = contAux + 1
                NumerosTel = New NumeroTelefonico()
                NumerosTel.Numeros = Me.txtTelefono.Text
                NumerosTel.ID = CStr(contAux)
                diccionario.Add(NumerosTel.ID, NumerosTel)
                listaI.Add(NumerosTel)
                enlase()
            Else
                MsgBox("El Numero De Telefeno Debe Ser De 10 Digitos")
            End If
        Catch ex As Exception
            MsgBox("El numero ya existe en la Lista")
        End Try

        'If IsNumeric(Me.txtTelefono.Text) = True Then
        '    Me.ListBox4.Items.Add(Me.txtTelefono.Text).ToString()
        'Else
        '    MsgBox("Introdusca un valor correcto")
        'End If

        limpia()
    End Sub

    Private Sub btnQuitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuitar.Click
        If Me.ListBox4.SelectedValue <> Nothing Then
            Datos = (Me.ListBox4.SelectedValue.ToString)
            'MsgBox(Datos)
            listaI.Remove(diccionario(Datos))
            enlase()
        Else
            MsgBox("Seleccione un numero de la lista")
        End If
        limpia()
    End Sub

    Private Sub btnAgregar2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar2.Click

        

        Try
            If (IsNumeric(Me.txtTelDel.Text) = True And IsNumeric(Me.txtTelAl.Text)) And (Len(Me.txtTelAl.Text) = 10 And Len(Me.txtTelDel.Text) = 10) Then

                ValidaTelefonosPortabilidad(CLng(Me.txtTelDel.Text), CLng(Me.txtTelAl.Text), 0)

                If OP = 1 Then
                    Exit Sub
                End If

                contAux1 = contAux1 + 1
                NumerosTel = New NumeroTelefonico()
                NumerosTel.Numeros = Me.txtTelDel.Text
                NumerosTel.ID = CStr(contAux1)
                diccionario1.Add(NumerosTel.ID, NumerosTel)
                listaD.Add(NumerosTel)

                NumerosTel = New NumeroTelefonico()
                NumerosTel.Numeros = Me.txtTelAl.Text
                NumerosTel.ID = CStr(contAux1)
                diccionario2.Add(NumerosTel.ID, NumerosTel)
                listaF.Add(NumerosTel)
                enlase2()
            End If
        Catch ex As Exception
            MsgBox("El numero ya existe en la Lista")
        End Try
        'If IsNumeric(Me.txtTelDel.Text) = True And IsNumeric(Me.txtTelAl.Text) Then
        '    Me.ListBox2.Items.Add(Me.txtTelDel.Text)
        '    Me.ListBox3.Items.Add(Me.txtTelAl.Text)
        'Else
        '    MsgBox("Introdusca un valor correcto")
        'End If
        limpia()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.ListBox3.SelectedValue <> Nothing Then
            'Me.ListBox2.SetSelected(Me.ListBox3.SelectedIndex, True)
            'MsgBox(Datos) 
            listaD.Remove(diccionario1(Me.ListBox3.SelectedValue.ToString))
            listaF.Remove(diccionario2(Me.ListBox3.SelectedValue.ToString))
            enlase2()
        ElseIf Me.ListBox2.SelectedValue <> Nothing Then
            'Me.ListBox3.SetSelected(Me.ListBox2.SelectedIndex, True)
            'MsgBox(Datos) 
            listaD.Remove(diccionario1(Me.ListBox2.SelectedValue.ToString))
            listaF.Remove(diccionario2(Me.ListBox2.SelectedValue.ToString))
            enlase2()
        Else
            MsgBox("Seleccione un numero de la lista")
        End If
        limpia()
    End Sub

   

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click

        If Len(Me.NombreText.Text) = 0 Then
            MsgBox("Debes escribir un nombre")
            Exit Sub
        ElseIf cmbFijo.Text = "Ninguno" And cmbMovil.Text = "Ninguno" And cmbNoGeo.Text = "Ninguno" Then
            MsgBox("Debe seleccionar un tipo de servicio")
            Exit Sub
        End If

        If cmbEmpresaA.Text = "Ninguno" Then
            MsgBox("Debe seleccionar el proveedor del servicio")
            Exit Sub
        End If

        If Len(ListBox4.Text) = 0 And Len(ListBox2.Text) = 0 And Len(ListBox3.Text) = 0 Then
            MsgBox("Debe Introduccir A Lo Menos Un Numero Telefonico")
            Exit Sub
        End If


        If chkGobierno.Checked = True Then
            tipo = "G"
        ElseIf chkPerMoral.Checked = True Then
            tipo = "M"
        ElseIf chkPerFisica.Checked = True Then
            tipo = "F"
        End If

        EmpresOld = Me.cmbEmpresaA.SelectedText
        empOld = Me.cmbEmpresaA.SelectedValue


        guardaDatos(Me.NombreText.Text, tipo, cmbFijo.Text, cmbMovil.Text, cmbNoGeo.Text, empOld, EmpresOld)

        MsgBox("Los datos Fueron guardados correctamente")

        'FrmCtrl_ServiciosCli.Crea_arbolTel()

        If imprimir = True Then
            'ConfigureCrystalReportsPortabilidad(folio)
            folioport = folio
            FrmImprimirAux.Show()
            imprimir = False
            Me.Close()
        End If

        'frmTelefonia.muestraTelefonosClientes(GloContratonet)
        opcionport = ""
        Me.Dispose()
        Me.Close()

    End Sub
    Private Sub ConfigureCrystalReportsPortabilidad(ByVal FolioS As Long)

        Dim impresora As String = Nothing
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo

        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\repotrtePortabilidad.rpt"
        'reportPath = "D:\Exes\Softv\reportes\repotrtePortabilidad.rpt"
        customersByCityReport.Load(reportPath)


        SetDBLogonForReport(connectionInfo, customersByCityReport)

        '@Contrato 
        customersByCityReport.SetParameterValue(0, FolioS)
        'CrystalReportViewer1.ReportSource = customersByCityReport
        'CrystalReportViewer1.ShowPrintButton = True

        'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)


        customersByCityReport.PrintOptions.PrinterName = Impresora_Contratos
        customersByCityReport.PrintToPrinter(0, True, 1, 1)


        'customersByCityReport = Nothing


    End Sub
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub
    Private Sub GuardaClientePortabilidad(ByVal cadena As String)

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("GuardaClientePortabilidad", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@CADENA", SqlDbType.Xml)
        par1.Direction = ParameterDirection.Input       
        par1.Value = cadena
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@folio", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Output
        com.Parameters.Add(par2)

        Try
            con.Open()
            com.ExecuteNonQuery()
            folio = CInt(par2.Value)
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()

            diccionario.Clear()
            diccionario1.Clear()
            diccionario2.Clear()
            listaD.Clear()
            listaI.Clear()
            listaF.Clear()
        End Try

    End Sub

    Private Sub guardaDatos(ByVal nombre As String, ByVal tipo As String, ByVal serviciofijo As String, ByVal serviciomovil As String, ByVal NumNoGeografico As String, ByVal empresaold As Integer, ByVal EmpreOld As String)

        Dim sw As New StringWriter()
        Dim w As New XmlTextWriter(sw)
        Dim i As Integer
        Dim cod As NumeroTelefonico
        Dim cod2 As NumeroTelefonico

        w.Formatting = Formatting.Indented
        'w.WriteStartElement("Telefono")
        'w.WriteAttributeString("clasificacion", Me.txtNombreClasificacion.Text)
        w.WriteStartElement("DatosGenerales")

        w.WriteStartElement("Datos")
        w.WriteAttributeString("ID", folio)
        w.WriteAttributeString("nomnbre", nombre)
        w.WriteAttributeString("tipo", tipo)
        w.WriteAttributeString("serviciofijo", serviciofijo)
        w.WriteAttributeString("serviciomovil", serviciomovil)
        w.WriteAttributeString("NumNoGeografico", NumNoGeografico)
        w.WriteAttributeString("empresaold", Me.cmbEmpresaA.SelectedValue)
        w.WriteAttributeString("empresanew", empresanew)
        w.WriteAttributeString("contrato", Contrato)
        w.WriteAttributeString("BndEmpresa", BndEmpresa)
        w.WriteAttributeString("EmpreOld", EmpreOld)
        w.WriteAttributeString("GloContratonet", GloContratonet)
        w.WriteEndElement()
        cont = 1
        w.WriteStartElement("Telefonos")
        For Each cod In listaI
            w.WriteStartElement("Telefono")
            w.WriteAttributeString("numtelefono", cod.Numeros)
            w.WriteAttributeString("cont", cont)
            cont += 1
            w.WriteEndElement()
        Next

        w.WriteEndElement()

        w.WriteStartElement("Intervalos")
        For Each cod In listaD
            w.WriteStartElement("Intervalo1")
            w.WriteAttributeString("teldel", cod.Numeros)
            w.WriteEndElement()
        Next
        For Each cod In listaF
            w.WriteStartElement("Intervalo2")
            w.WriteAttributeString("telal", cod.Numeros)
            w.WriteEndElement()
        Next
        'For i = 0 To Me.ListBox2.Items.Count
        '    w.WriteStartElement("Intervalo")
        '    If (Len(Me.ListBox2.Text) > 0 And Len(Me.ListBox3.Text) > 0) Then
        '        If ((ListBox2.Items.Count > 0 And ListBox3.Items.Count > 0) Or (Len(Me.ListBox2.Text) > 0 And Len(Me.ListBox3.Text) > 0)) Then
        '            w.WriteAttributeString("teldel", Me.ListBox2.Items(i).ToString)
        '            w.WriteAttributeString("telal", Me.ListBox3.Items(i).ToString)
        '            'Else
        '            '    w.WriteAttributeString("teldel", "0")
        '            '    w.WriteAttributeString("telal", "0")
        '        End If
        '    End If
        '    w.WriteEndElement()
        'Next

        w.WriteEndElement()

        w.WriteEndElement()
        w.Close()

        GuardaClientePortabilidad(sw.ToString())
    End Sub

    Private Sub cmbFijo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFijo.SelectedIndexChanged

        If cmbFijo.Text <> "Ninguno" Then
            cmbMovil.Text = "Ninguno"
            cmbNoGeo.Text = "Ninguno"
        End If

    End Sub

    Private Sub cmbMovil_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbMovil.SelectedIndexChanged
        If cmbMovil.Text <> "Ninguno" Then
            cmbFijo.Text = "Ninguno"
            cmbNoGeo.Text = "Ninguno"
        End If
    End Sub

    Private Sub cmbNoGeo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbNoGeo.SelectedIndexChanged
        If cmbNoGeo.Text <> "Ninguno" Then
            cmbFijo.Text = "Ninguno"
            cmbMovil.Text = "Ninguno"
        End If
    End Sub

    Private Sub FrmDatosPortabilidad_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        diccionario.Clear()
        listaD.Clear()
        listaI.Clear()
        opcionport = ""
    End Sub

    
    Private Sub FrmDatosPortabilidad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)

        If opcionport = "N" Then
            muestraProveedorDisponible(0)
        End If

        If opcionport = "C" Then
         
            muestraClientePortabilidad(contraoport)
            MuestraRelClientePortabilidad()
            MuestraInterClientePortabilidad()
            muestraProveedorDisponible(folioport)
            blokea()
        End If

        If opcionport = "M" Then
            Me.Button1.Visible = True
            Me.btnActiva.Visible = True
            muestraClientePortabilidad(contraoport)
            MuestraRelClientePortabilidad()
            MuestraInterClientePortabilidad()
            'Me.Panel2.Enabled = False
            Me.Panel1.Enabled = False
            'Me.Button3.Visible = True
            'Me.Button4.Visible = True
            Me.btnAgrega1.Enabled = False
            Me.btnAgregar2.Enabled = False
            muestraProveedorDisponible(folioport)
            dimeSiActivo(folioport)
            If errorA = 1 Then
                btnActiva.Enabled = False
                blokea()
            End If
        End If

        If (opcionport <> "M" And opcionport <> "C") Then
            folioport = 0
            imprimir = True
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub
    Private Sub blokea()
        Me.Button1.Visible = True
        Me.Panel2.Enabled = False
        Me.Panel1.Enabled = False
        Me.btnActiva.Visible = False
        Me.btnGuardar.Visible = False
        Me.btnAgrega1.Enabled = False
        Me.btnAgregar2.Enabled = False
    End Sub
    Private Sub muestraProveedorDisponible(ByVal FOLIO As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC muestraProveedorDisponible ")
        strSQL.Append(CStr(FOLIO))


        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable


            Me.cmbEmpresaA.DisplayMember = "NOMBRE"
            Me.cmbEmpresaA.ValueMember = "CLAVE"
            Me.cmbEmpresaA.DataSource = bindingSource


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        opcionport = ""
        Me.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActiva.Click
        If (MsgBox("Se Generaran Las Ordenes Correspondientes Al Contrato", MsgBoxStyle.YesNo)).ToString = "Yes" Then
            generaOrdenPortabilidad(contraoport)
        Else
            Exit Sub
        End If
        opcionport = ""
        Me.Close()
    End Sub
    Private Sub generaOrdenPortabilidad(ByVal contrato As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("generaOrdenPortabilidad", con)

        com.CommandType = CommandType.StoredProcedure
        Dim par1 As New SqlParameter("@contrato", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = contrato

        Dim par2 As New SqlParameter("@msg", SqlDbType.VarChar, 250)
        par2.Direction = ParameterDirection.Output

        com.Parameters.Add(par1)
        com.Parameters.Add(par2)

        Try
            con.Open()
            com.ExecuteNonQuery()
            MsgBox(par2.Value.ToString)
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try

    End Sub
    Private Sub limpia()
        Me.txtTelAl.Clear()
        Me.txtTelDel.Clear()
        Me.txtTelefono.Clear()
    End Sub
    Private Sub muestraClientePortabilidad(ByVal contrato As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("muestraClientePortabilidad", con)

        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@contrato", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = contrato
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@id", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Output
        com.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Tipo", SqlDbType.VarChar, 250)
        par3.Direction = ParameterDirection.Output
        com.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@nombre", SqlDbType.VarChar, 250)
        par4.Direction = ParameterDirection.Output
        com.Parameters.Add(par4)

        Dim par5 As New SqlParameter("@servicioF", SqlDbType.VarChar, 250)
        par5.Direction = ParameterDirection.Output
        com.Parameters.Add(par5)

        Dim par6 As New SqlParameter("@servicioM", SqlDbType.VarChar, 250)
        par6.Direction = ParameterDirection.Output
        com.Parameters.Add(par6)

        Dim par7 As New SqlParameter("@numGeo", SqlDbType.VarChar, 250)
        par7.Direction = ParameterDirection.Output
        com.Parameters.Add(par7)

        'Dim par8 As New SqlParameter("@EmpresaO", SqlDbType.VarChar, 250)
        'par8.Direction = ParameterDirection.Output
        'com.Parameters.Add(par8)

        Dim par9 As New SqlParameter("@bndEmpresa", SqlDbType.Bit)
        par9.Direction = ParameterDirection.Output
        com.Parameters.Add(par9)

        Dim par10 As New SqlParameter("@EmpresaOld", SqlDbType.VarChar, 250)
        par10.Direction = ParameterDirection.Output
        com.Parameters.Add(par10)

        Try
            con.Open()
            com.ExecuteNonQuery()

            Me.NombreText.Text = par4.Value.ToString
            tipo = CStr(par3.Value.ToString)
            serviciof = CStr(par5.Value.ToString)
            serviciom = CStr(par6.Value.ToString)
            noGeo = CStr(par7.Value.ToString)
            'Empresa = CStr(par8.Value.ToString)
            BndEmpresa = CBool(par9.Value)
            folio = CLng(par2.Value)
            EmpresOld = CStr(par10.Value.ToString)

            If tipo = "G" Then
                chkGobierno.Checked = True
                chkPerFisica.Checked = False
                chkPerMoral.Checked = False
            ElseIf tipo = "F" Then
                chkGobierno.Checked = False
                chkPerFisica.Checked = True
                chkPerMoral.Checked = False
            ElseIf tipo = "M" Then
                chkGobierno.Checked = False
                chkPerFisica.Checked = False
                chkPerMoral.Checked = True
            End If

            Me.cmbFijo.Text = serviciof
            Me.cmbMovil.Text = serviciom
            Me.cmbNoGeo.Text = noGeo

           
            'Me.cmbEmpresaA.Text = Empresa



        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try

    End Sub
    Private Sub MuestraRelClientePortabilidad()
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        

        Dim I As Integer = 0
        Dim Fila2 As DataRow
        Dim oDataSet As New DataSet

        Try
            Dim cmd As New SqlCommand("MuestraRelClientePortabilidad", conexion)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@contrato", CLng(contraoport))

            Dim listacodigos As New List(Of Codigos)

            Dim dr As SqlDataReader
            conexion.Open()
            dr = cmd.ExecuteReader()
            diccionario.Clear()
            listaI.Clear()

            Using dr
                While dr.Read
                    NumerosTel = New NumeroTelefonico()
                    NumerosTel.Numeros = dr("numTelefono").ToString().Trim()
                    NumerosTel.ID = dr("ID").ToString().Trim()
                    diccionario.Add(NumerosTel.ID, NumerosTel)
                    listaI.Add(NumerosTel)

                End While
            End Using

            Me.ListBox4.DataSource = Nothing
            Me.ListBox4.DataSource = listaI
            Me.ListBox4.DisplayMember = "Numer"
            Me.ListBox4.ValueMember = "ID"




        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub
    Private Sub MuestraInterClientePortabilidad()
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder


        Dim I As Integer = 0
        Dim Fila2 As DataRow
        Dim oDataSet As New DataSet

        Try
            Dim cmd As New SqlCommand("MuestraInterClientePortabilidad", conexion)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@contrato", CLng(contraoport))

            Dim listacodigos As New List(Of Codigos)

            Dim dr As SqlDataReader
            conexion.Open()
            dr = cmd.ExecuteReader()
            'diccionario.Clear()
            listaD.Clear()
            listaF.Clear()

            Using dr
                While dr.Read
                    NumerosTel = New NumeroTelefonico()
                    NumerosTel.Numeros = dr("numDel").ToString().Trim()
                    NumerosTel.ID = dr("ID").ToString().Trim()
                    diccionario1.Add(NumerosTel.ID, NumerosTel)
                    listaD.Add(NumerosTel)
                    NumerosTel = New NumeroTelefonico()
                    NumerosTel.Numeros = dr("numAl").ToString().Trim()
                    NumerosTel.ID = dr("ID").ToString().Trim()
                    diccionario2.Add(NumerosTel.ID, NumerosTel)
                    listaF.Add(NumerosTel)

                End While
            End Using

            Me.ListBox2.DataSource = Nothing
            Me.ListBox2.DataSource = listaD
            Me.ListBox2.DisplayMember = "Numer"
            Me.ListBox2.ValueMember = "ID"
            Me.ListBox3.DataSource = Nothing
            Me.ListBox3.DataSource = listaF
            Me.ListBox3.DisplayMember = "Numer"
            Me.ListBox3.ValueMember = "ID"
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub Button3_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'If IsNumeric(folio) = True Then
        '    BorraRelacion(folio)
        'Me.Button3.Visible = False
        'Me.btnAgrega1.Enabled = True

        'End If
    End Sub
    Private Sub BorraRelacion(ByVal clave As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("BorraRelacion", con)

        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@id", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = clave

        com.Parameters.Add(par1)

        Try
            con.Open()
            com.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()

        End Try

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If IsNumeric(folioport) = True Then
            BorraIntervalo(folioport)
        End If
    End Sub
    Private Sub BorraIntervalo(ByVal clave As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("BorraIntervalo", con)

        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@id", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = clave

        com.Parameters.Add(par1)

        Try
            con.Open()
            com.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()

        End Try
    End Sub
    Public Sub enlase()

        ListBox4.DataSource = Nothing
        ListBox4.DataSource = listaI
        ListBox4.DisplayMember = "Numer"
        ListBox4.ValueMember = "ID"
        ListBox4.Refresh()

    End Sub
    Public Sub enlase2()

        ListBox2.DataSource = Nothing
        ListBox2.DataSource = listaD
        ListBox2.DisplayMember = "Numer"
        ListBox2.ValueMember = "ID"
        ListBox2.Refresh()

        ListBox3.DataSource = Nothing
        ListBox3.DataSource = listaF
        ListBox3.DisplayMember = "Numer"
        ListBox3.ValueMember = "ID"
        ListBox3.Refresh()

    End Sub

    Private Sub Selecciona_Impresora_Sucursal(ByVal sucursal As Integer)

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Selecciona_Impresora_Sucursal", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@clv_sucursal", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = sucursal
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Impresora_Tarjetas", SqlDbType.VarChar, 50)
        par2.Direction = ParameterDirection.Output
        com.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Impresora_Contratos", SqlDbType.VarChar, 50)
        par3.Direction = ParameterDirection.Output
        com.Parameters.Add(par3)

        Try
            con.Open()
            com.ExecuteNonQuery()
            Impresora_Tarjetas = CStr(par2.Value.ToString)
            Impresora_Contratos = CStr(par3.Value.ToString)
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try

    End Sub
    Private Sub ValidaTelefonosPortabilidad(ByVal numtel As Long, ByVal numtel2 As Long, ByVal opcion As Integer)

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("ValidaTelefonosPortabilidad", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@numtel", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = numtel
        com.Parameters.Add(par1)

        Dim par4 As New SqlParameter("@numtel2", SqlDbType.BigInt)
        par4.Direction = ParameterDirection.Input
        par4.Value = numtel2
        com.Parameters.Add(par4)

        Dim par5 As New SqlParameter("@op", SqlDbType.Int)
        par5.Direction = ParameterDirection.Input
        par5.Value = opcion
        com.Parameters.Add(par5)

        Dim par2 As New SqlParameter("@msg", SqlDbType.VarChar, 100)
        par2.Direction = ParameterDirection.Output
        com.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@ERROR", SqlDbType.Int)
        par3.Direction = ParameterDirection.Output
        com.Parameters.Add(par3)

        Try
            con.Open()
            com.ExecuteNonQuery()
            OP = CInt(par3.Value)
            If OP = 1 Then
                MsgBox(CStr(par2.Value.ToString))
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try
    End Sub

    Private Sub dimeSiActivo(ByVal folio As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("dimeSiActivo", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@folio", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = folio
        com.Parameters.Add(par1)

        Dim par4 As New SqlParameter("@error", SqlDbType.Int)
        par4.Direction = ParameterDirection.Output
        com.Parameters.Add(par4)

        Try
            con.Open()
            com.ExecuteNonQuery()
            errorA = CInt(par4.Value)
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try
    End Sub

#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesCliente")

            UspDesactivarBotonesCliente = CBool(BotonesDesactivar.Rows(0)(0).ToString)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region

End Class
Public Class NumeroTelefonico
    Dim _numero As String
    Dim _clave As String
    Public Property Numeros() As String
        Get
            Return _numero
        End Get
        Set(ByVal Value As String)
            _numero = Value
        End Set
    End Property
    Public Property ID() As String
        Get
            Return _clave
        End Get
        Set(ByVal Value As String)
            _clave = Value
        End Set
    End Property
    Public ReadOnly Property Numer() As String
        Get
            Return _numero
        End Get
    End Property

    Public Sub NumeroTelefonico(ByVal num As String, ByVal Clv As String)
        Me.Numeros = num
        Me.ID = Clv
    End Sub
End Class