﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmComisionRecontratacion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtMesesBajaFin = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtMesesPagadosFin = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.dgCalculoBonos = New System.Windows.Forms.DataGridView()
        Me.btnBorrar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.txtMesesPagadosIni = New System.Windows.Forms.TextBox()
        Me.txtPorcentaje = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtMesesBajaIni = New System.Windows.Forms.TextBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmbServicio = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbGrupoVentas = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmbNivelUsuario = New System.Windows.Forms.ComboBox()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvServicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvGrupo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EsSupervisor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Porcentaje = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MesesPagadosInicial = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MesesPagadosFinal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MesesBajaInicial = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MesesBajaFinal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgCalculoBonos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Controls.Add(Me.txtMesesBajaFin)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.txtMesesPagadosFin)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.btnAgregar)
        Me.GroupBox3.Controls.Add(Me.dgCalculoBonos)
        Me.GroupBox3.Controls.Add(Me.btnBorrar)
        Me.GroupBox3.Controls.Add(Me.btnGuardar)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.btnModificar)
        Me.GroupBox3.Controls.Add(Me.txtMesesPagadosIni)
        Me.GroupBox3.Controls.Add(Me.txtPorcentaje)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.txtMesesBajaIni)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(4, 158)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(572, 378)
        Me.GroupBox3.TabIndex = 69
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Detalle de la comisión de recontratación"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(310, 45)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(15, 15)
        Me.Label8.TabIndex = 56
        Me.Label8.Text = "a"
        '
        'txtMesesBajaFin
        '
        Me.txtMesesBajaFin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMesesBajaFin.Location = New System.Drawing.Point(333, 42)
        Me.txtMesesBajaFin.Name = "txtMesesBajaFin"
        Me.txtMesesBajaFin.Size = New System.Drawing.Size(73, 21)
        Me.txtMesesBajaFin.TabIndex = 55
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(91, 45)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(15, 15)
        Me.Label7.TabIndex = 54
        Me.Label7.Text = "a"
        '
        'txtMesesPagadosFin
        '
        Me.txtMesesPagadosFin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMesesPagadosFin.Location = New System.Drawing.Point(113, 42)
        Me.txtMesesPagadosFin.Name = "txtMesesPagadosFin"
        Me.txtMesesPagadosFin.Size = New System.Drawing.Size(73, 21)
        Me.txtMesesPagadosFin.TabIndex = 53
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(445, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(84, 15)
        Me.Label4.TabIndex = 44
        Me.Label4.Text = "Porcentaje :"
        '
        'btnAgregar
        '
        Me.btnAgregar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar.ForeColor = System.Drawing.Color.Black
        Me.btnAgregar.Location = New System.Drawing.Point(4, 71)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(136, 36)
        Me.btnAgregar.TabIndex = 36
        Me.btnAgregar.Text = "&Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = False
        '
        'dgCalculoBonos
        '
        Me.dgCalculoBonos.AllowUserToAddRows = False
        Me.dgCalculoBonos.AllowUserToDeleteRows = False
        Me.dgCalculoBonos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgCalculoBonos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.dgCalculoBonos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgCalculoBonos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.ClvServicio, Me.ClvGrupo, Me.EsSupervisor, Me.Porcentaje, Me.MesesPagadosInicial, Me.MesesPagadosFinal, Me.MesesBajaInicial, Me.MesesBajaFinal})
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgCalculoBonos.DefaultCellStyle = DataGridViewCellStyle12
        Me.dgCalculoBonos.Location = New System.Drawing.Point(4, 113)
        Me.dgCalculoBonos.Name = "dgCalculoBonos"
        Me.dgCalculoBonos.ReadOnly = True
        Me.dgCalculoBonos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgCalculoBonos.Size = New System.Drawing.Size(562, 256)
        Me.dgCalculoBonos.TabIndex = 52
        '
        'btnBorrar
        '
        Me.btnBorrar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnBorrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBorrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBorrar.ForeColor = System.Drawing.Color.Black
        Me.btnBorrar.Location = New System.Drawing.Point(430, 71)
        Me.btnBorrar.Name = "btnBorrar"
        Me.btnBorrar.Size = New System.Drawing.Size(136, 36)
        Me.btnBorrar.TabIndex = 37
        Me.btnBorrar.Text = "&Borrar"
        Me.btnBorrar.UseVisualStyleBackColor = False
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.Color.Black
        Me.btnGuardar.Location = New System.Drawing.Point(146, 71)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(136, 36)
        Me.btnGuardar.TabIndex = 51
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(191, 15)
        Me.Label2.TabIndex = 40
        Me.Label2.Text = "Número de Meses pagados :"
        '
        'btnModificar
        '
        Me.btnModificar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModificar.ForeColor = System.Drawing.Color.Black
        Me.btnModificar.Location = New System.Drawing.Point(288, 71)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(136, 36)
        Me.btnModificar.TabIndex = 46
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.UseVisualStyleBackColor = False
        '
        'txtMesesPagadosIni
        '
        Me.txtMesesPagadosIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMesesPagadosIni.Location = New System.Drawing.Point(11, 42)
        Me.txtMesesPagadosIni.Name = "txtMesesPagadosIni"
        Me.txtMesesPagadosIni.Size = New System.Drawing.Size(73, 21)
        Me.txtMesesPagadosIni.TabIndex = 41
        '
        'txtPorcentaje
        '
        Me.txtPorcentaje.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPorcentaje.Location = New System.Drawing.Point(448, 42)
        Me.txtPorcentaje.Name = "txtPorcentaje"
        Me.txtPorcentaje.Size = New System.Drawing.Size(73, 21)
        Me.txtPorcentaje.TabIndex = 45
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(226, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(184, 15)
        Me.Label3.TabIndex = 42
        Me.Label3.Text = "Número de meses en baja :"
        '
        'txtMesesBajaIni
        '
        Me.txtMesesBajaIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMesesBajaIni.Location = New System.Drawing.Point(229, 42)
        Me.txtMesesBajaIni.Name = "txtMesesBajaIni"
        Me.txtMesesBajaIni.Size = New System.Drawing.Size(73, 21)
        Me.txtMesesBajaIni.TabIndex = 43
        '
        'btnCancelar
        '
        Me.btnCancelar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.ForeColor = System.Drawing.Color.Black
        Me.btnCancelar.Location = New System.Drawing.Point(431, 542)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(136, 36)
        Me.btnCancelar.TabIndex = 68
        Me.btnCancelar.Text = "&Salir"
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmbServicio)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.cmbGrupoVentas)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.cmbNivelUsuario)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(85, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(405, 126)
        Me.GroupBox2.TabIndex = 67
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Datos de la comisión de recontratación"
        '
        'cmbServicio
        '
        Me.cmbServicio.DisplayMember = "DESCRIPCION"
        Me.cmbServicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbServicio.FormattingEnabled = True
        Me.cmbServicio.Location = New System.Drawing.Point(155, 37)
        Me.cmbServicio.Name = "cmbServicio"
        Me.cmbServicio.Size = New System.Drawing.Size(229, 23)
        Me.cmbServicio.TabIndex = 34
        Me.cmbServicio.ValueMember = "CLVSERVICIO"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(81, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 15)
        Me.Label1.TabIndex = 38
        Me.Label1.Text = "Paquete :"
        '
        'cmbGrupoVentas
        '
        Me.cmbGrupoVentas.DisplayMember = "DESCRIPCION"
        Me.cmbGrupoVentas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbGrupoVentas.FormattingEnabled = True
        Me.cmbGrupoVentas.Location = New System.Drawing.Point(155, 66)
        Me.cmbGrupoVentas.Name = "cmbGrupoVentas"
        Me.cmbGrupoVentas.Size = New System.Drawing.Size(229, 23)
        Me.cmbGrupoVentas.TabIndex = 47
        Me.cmbGrupoVentas.ValueMember = "CLVGRUPO"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(29, 69)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(121, 15)
        Me.Label5.TabIndex = 48
        Me.Label5.Text = "Grupo de Ventas :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(28, 98)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(121, 15)
        Me.Label6.TabIndex = 50
        Me.Label6.Text = "Nivel de Usuario :"
        '
        'cmbNivelUsuario
        '
        Me.cmbNivelUsuario.DisplayMember = "DESCRIPCION"
        Me.cmbNivelUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbNivelUsuario.FormattingEnabled = True
        Me.cmbNivelUsuario.Location = New System.Drawing.Point(155, 95)
        Me.cmbNivelUsuario.Name = "cmbNivelUsuario"
        Me.cmbNivelUsuario.Size = New System.Drawing.Size(229, 23)
        Me.cmbNivelUsuario.TabIndex = 49
        Me.cmbNivelUsuario.ValueMember = "CLVNIVEL"
        '
        'ID
        '
        Me.ID.DataPropertyName = "ID"
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        Me.ID.Visible = False
        '
        'ClvServicio
        '
        Me.ClvServicio.DataPropertyName = "ClvServicio"
        Me.ClvServicio.HeaderText = "ClvServicio"
        Me.ClvServicio.Name = "ClvServicio"
        Me.ClvServicio.ReadOnly = True
        Me.ClvServicio.Visible = False
        '
        'ClvGrupo
        '
        Me.ClvGrupo.DataPropertyName = "ClvGrupo"
        Me.ClvGrupo.HeaderText = "ClvGrupo"
        Me.ClvGrupo.Name = "ClvGrupo"
        Me.ClvGrupo.ReadOnly = True
        Me.ClvGrupo.Visible = False
        '
        'EsSupervisor
        '
        Me.EsSupervisor.DataPropertyName = "EsSupervisor"
        Me.EsSupervisor.HeaderText = "EsSupervisor"
        Me.EsSupervisor.Name = "EsSupervisor"
        Me.EsSupervisor.ReadOnly = True
        Me.EsSupervisor.Visible = False
        '
        'Porcentaje
        '
        Me.Porcentaje.DataPropertyName = "Porcentaje"
        Me.Porcentaje.HeaderText = "Porcentaje"
        Me.Porcentaje.Name = "Porcentaje"
        Me.Porcentaje.ReadOnly = True
        '
        'MesesPagadosInicial
        '
        Me.MesesPagadosInicial.DataPropertyName = "MesesPagadosInicial"
        Me.MesesPagadosInicial.HeaderText = "# Meses de Pagados Inicial"
        Me.MesesPagadosInicial.Name = "MesesPagadosInicial"
        Me.MesesPagadosInicial.ReadOnly = True
        '
        'MesesPagadosFinal
        '
        Me.MesesPagadosFinal.DataPropertyName = "MesesPagadosFinal"
        Me.MesesPagadosFinal.HeaderText = "# Meses de Pagados Final"
        Me.MesesPagadosFinal.Name = "MesesPagadosFinal"
        Me.MesesPagadosFinal.ReadOnly = True
        '
        'MesesBajaInicial
        '
        Me.MesesBajaInicial.DataPropertyName = "MesesBajaInicial"
        Me.MesesBajaInicial.HeaderText = "# Meses de Baja Inicial"
        Me.MesesBajaInicial.Name = "MesesBajaInicial"
        Me.MesesBajaInicial.ReadOnly = True
        '
        'MesesBajaFinal
        '
        Me.MesesBajaFinal.DataPropertyName = "MesesBajaFinal"
        Me.MesesBajaFinal.HeaderText = "# Meses de Baja Final"
        Me.MesesBajaFinal.Name = "MesesBajaFinal"
        Me.MesesBajaFinal.ReadOnly = True
        '
        'FrmComisionRecontratacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(579, 583)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.btnCancelar)
        Me.Name = "FrmComisionRecontratacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Comisiones por Recontratación"
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.dgCalculoBonos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents dgCalculoBonos As System.Windows.Forms.DataGridView
    Friend WithEvents btnBorrar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents txtMesesPagadosIni As System.Windows.Forms.TextBox
    Friend WithEvents txtPorcentaje As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtMesesBajaIni As System.Windows.Forms.TextBox
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbServicio As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbGrupoVentas As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmbNivelUsuario As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtMesesPagadosFin As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtMesesBajaFin As System.Windows.Forms.TextBox
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvServicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvGrupo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EsSupervisor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Porcentaje As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MesesPagadosInicial As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MesesPagadosFinal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MesesBajaInicial As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MesesBajaFinal As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
