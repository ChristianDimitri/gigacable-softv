Imports System.Data.SqlClient
Public Class FrmLoginCte

    Private Sub FrmLoginCte_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        If OpcionCli <> "C" Then
            Me.Clv_UsuarioTextBox.Enabled = True
            Me.PasaporteTextBox.Enabled = True
        End If
        'Me.ConsultaControl_ClientesTableAdapter.Connection = CON
        'Me.ConsultaControl_ClientesTableAdapter.Fill(Me.DataSetarnoldo.ConsultaControl_Clientes, Contrato)
        'CON.Close()
        CONSULTACONTROL_CLIENTES(Contrato)
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()
    End Sub
    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        Try

            'Dim CON As New SqlConnection(MiConexion)

            If OpcionCli = "C" Or TextBox1.Visible = False Then
                Me.Close()
            Else
                If Me.PasaporteTextBox.Text = Me.TextBox1.Text Then
                    GUARDACONTROL_CLIENTES(Contrato, Clv_UsuarioTextBox.Text, PasaporteTextBox.Text)
                    'CON.Open()
                    'Me.GuardaControl_ClientesTableAdapter.Connection = CON
                    'Me.GuardaControl_ClientesTableAdapter.Fill(Me.DataSetarnoldo.GuardaControl_Clientes, Contrato, Me.Clv_UsuarioTextBox.Text, Me.PasaporteTextBox.Text)
                    'CON.Close()
                    MsgBox("Se ha guardado con �xito.", MsgBoxStyle.Information)
                    Me.Close()
                Else
                    MsgBox("La confirmaci�n no coindide.", MsgBoxStyle.Information)
                End If

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub PasaporteTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PasaporteTextBox.KeyPress
        Me.CMBLabel1.Visible = True
        Me.TextBox1.Visible = True
    End Sub

    Private Sub FrmLoginCte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
    End Sub

    Private Sub CONSULTACONTROL_CLIENTES(ByVal CONTRATO As Integer)
        Try
            Dim dTable As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.Int, CONTRATO)
            dTable = BaseII.ConsultaDT("CONSULTACONTROL_CLIENTES")
            If dTable.Rows.Count > 0 Then
                Clv_UsuarioTextBox.Text = dTable.Rows(0)(0).ToString
                PasaporteTextBox.Text = dTable.Rows(0)(1).ToString
            End If
        Catch ex As Exception
            MsgBox(ex)
        End Try
    End Sub

    Private Sub GUARDACONTROL_CLIENTES(ByVal CONTRATO As Integer, ByVal LOGIN As String, ByVal PASAPORTE As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.Int, CONTRATO)
        BaseII.CreateMyParameter("@LOGIN", SqlDbType.VarChar, LOGIN, 50)
        BaseII.CreateMyParameter("@PASAPORTE", SqlDbType.VarChar, PASAPORTE, 20)
        BaseII.Inserta("GUARDACONTROL_CLIENTES")
    End Sub

End Class
