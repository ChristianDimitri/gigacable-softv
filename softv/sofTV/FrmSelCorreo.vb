﻿Public Class FrmSelCorreo
    Public Op As Integer
    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If cbxConCorreo.Checked = False And cbxSinCorreo.Checked = False Then
            MsgBox("Seleccione al menos una Opción", MsgBoxStyle.Information)
            Exit Sub
        End If
        If cbxConCorreo.Checked = True And cbxSinCorreo.Checked = True Then
            Op = 0
        ElseIf cbxConCorreo.Checked = True Then
            Op = 1
        Else
            Op = 2
        End If
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub FrmSelCorreo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        cbxConCorreo.Checked = False
        cbxSinCorreo.Checked = False
        Op = 0
    End Sub
End Class