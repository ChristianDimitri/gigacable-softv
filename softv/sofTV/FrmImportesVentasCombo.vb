﻿Public Class FrmImportesVentasCombo

#Region "Eventos"

    Private Sub FrmImportesVentasCombo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        lblServicioPrincipal.ForeColor = Color.Black
        Label1.ForeColor = Color.Black
        UspLlenaServPrincipales()
        Me.cmbServicioPrincipal.Enabled = True
        Me.cmbServicioSecundario.Enabled = True
        Me.dgImportesCombos.Enabled = True
        Me.btnGuardar.Enabled = True
        Me.btnAgregar.Enabled = True
        Me.btnBorrar.Enabled = True
        If opcion = "M" Then
            Me.cmbServicioPrincipal.SelectedValue = GloClvServicioPrincipal
            UspLlenaServSecundario()
            UspMuestraImportesCombo()
            Me.cmbServicioPrincipal.Enabled = False
        ElseIf opcion = "C" Then
            UspLlenaServSecundario()
            Me.cmbServicioPrincipal.SelectedValue = GloClvServicioPrincipal
            UspMuestraImportesCombo()
            Me.cmbServicioPrincipal.Enabled = False
            Me.cmbServicioSecundario.Enabled = False
            Me.dgImportesCombos.Enabled = False
            Me.btnGuardar.Enabled = False
            Me.btnAgregar.Enabled = False
            Me.btnBorrar.Enabled = False

        ElseIf opcion = "N" Then
            UspLlenaServSecundario()
        End If
    End Sub

    Private Sub cmbServicioPrincipal_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbServicioPrincipal.SelectedIndexChanged
        UspLlenaServSecundario()
    End Sub
#End Region

#Region "Salir"

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

#End Region

#Region "Agregar"

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        Me.cmbServicioPrincipal.Enabled = False
        UspAgregarServicioImporteCombo()
        UspLlenaServSecundario()
        UspMuestraImportesCombo()
    End Sub

#End Region

#Region "Borrar"

    Private Sub btnBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBorrar.Click
        If Me.dgImportesCombos.Rows.Count > 0 Then
            UspBorrarServicioImporteCombo()
            UspLlenaServSecundario()
            UspMuestraImportesCombo()
        End If
     
    End Sub

#End Region

#Region "Metodos"

    Private Sub UspLlenaServPrincipales()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Var", SqlDbType.VarChar, opcion, 5)
            cmbServicioPrincipal.DataSource = BaseII.ConsultaDT("UspLlenaServPrincipales")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspLlenaServSecundario()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Var", SqlDbType.VarChar, opcion, 5)
            BaseII.CreateMyParameter("@ClvServPrin", SqlDbType.Int, CInt(Me.cmbServicioPrincipal.SelectedValue))
            cmbServicioSecundario.DataSource = BaseII.ConsultaDT("UspLlenaServSecundario")

            If cmbServicioSecundario.Items.Count = 0 Then
                cmbServicioSecundario.Text = ""
                Me.btnAgregar.Enabled = False
            Else
                Me.btnAgregar.Enabled = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspAgregarServicioImporteCombo()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSERVICIOPRINCIPAL", SqlDbType.Int, Me.cmbServicioPrincipal.SelectedValue)
            BaseII.CreateMyParameter("@CLVSERVIVIOSECUNDARIO", SqlDbType.Int, Me.cmbServicioSecundario.SelectedValue)
            BaseII.ConsultaDT("UspAgregarServicioImporteCombo")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspBorrarServicioImporteCombo()
        Try
            Dim row As DataGridViewRow = dgImportesCombos.CurrentRow

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSERVICIOPRINCIPAL", SqlDbType.Int, Me.cmbServicioPrincipal.SelectedValue)
            BaseII.CreateMyParameter("@CLVSERVIVIOSECUNDARIO", SqlDbType.Int, row.Cells(1).Value)
            BaseII.Inserta("UspBorrarServicioImporteCombo")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMuestraImportesCombo()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSERVICIOPRINCIPAL", SqlDbType.Int, Me.cmbServicioPrincipal.SelectedValue)
            dgImportesCombos.DataSource = BaseII.ConsultaDT("UspMuestraImportesCombo")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspGuardaComisionCombo()
        Try
            Dim I As Integer

            For I = 0 To dgImportesCombos.Rows.Count - 1
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLVSERVICIOPRINCIPAL", SqlDbType.Int, Me.cmbServicioPrincipal.SelectedValue)
                BaseII.CreateMyParameter("@CLVSERVICIOSECUNDARIO", SqlDbType.Int, Me.dgImportesCombos(1, I).Value)
                BaseII.CreateMyParameter("@COMISIONGRUPO1", SqlDbType.Money, Me.dgImportesCombos(4, I).Value)
                BaseII.CreateMyParameter("@COMISIONGRUPO2", SqlDbType.Money, Me.dgImportesCombos(6, I).Value)
                BaseII.CreateMyParameter("@COMISIONGRUPO3", SqlDbType.Money, Me.dgImportesCombos(8, I).Value)
                BaseII.CreateMyParameter("@COMISIONGRUPO4", SqlDbType.Money, Me.dgImportesCombos(10, I).Value)
                BaseII.CreateMyParameter("@COMISIONGRUPO5", SqlDbType.Money, Me.dgImportesCombos(12, I).Value)
                BaseII.CreateMyParameter("@COMISIONGRUPO6", SqlDbType.Money, Me.dgImportesCombos(14, I).Value)
                BaseII.CreateMyParameter("@COMISIONGRUPO7", SqlDbType.Money, Me.dgImportesCombos(16, I).Value)
                BaseII.Inserta("UspGuardaComisionCombo")
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Guardar"
    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        UspGuardaComisionCombo()
        UspMuestraImportesCombo()
    End Sub
#End Region
   
   
   
   
   
End Class