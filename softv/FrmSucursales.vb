﻿Imports System.Data.SqlClient
Public Class FrmSucursales

    Dim res As Integer = 0
    Dim msj As String = String.Empty

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub




    Private Sub CONSUCURSALESBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONSUCURSALESBindingNavigatorSaveItem.Click
        Dim locerror As Integer = 0
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            If IsNumeric(Me.UltimoFolioUsadoTextBox.Text) = False Then Me.UltimoFolioUsadoTextBox.Text = 0
            If IsNumeric(Me.No_folioTextBox.Text) = False Then Me.No_folioTextBox.Text = 0
            Me.Validate()
            Me.CONSUCURSALESBindingSource.EndEdit()
            Me.CONSUCURSALESTableAdapter.Connection = CON
            Me.CONSUCURSALESTableAdapter.Update(Me.NewSofTvDataSet.CONSUCURSALES)
            Me.Inserta_impresora_sucursalTableAdapter1.Connection = CON
            Me.Inserta_impresora_sucursalTableAdapter1.Fill(Me.ProcedimientosArnoldo2.inserta_impresora_sucursal, CInt(Me.Clv_SucursalTextBox.Text), Me.Impresora_ContratosTextBox.Text, Me.Impresora_TarjetasTextBox.Text, Me.TextBox1.Text)

            If SerieTextBox1.Text.Length = 0 Then SerieTextBox1.Text = ""
            If No_folioTextBox.Text.Length = 0 Then No_folioTextBox.Text = 0

            Me.Inserta_Generales_FacturaGlobalTableAdapter.Connection = CON
            Me.Inserta_Generales_FacturaGlobalTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Generales_FacturaGlobal, CInt(Me.Clv_SucursalTextBox.Text), Me.SerieTextBox1.Text, CInt(Me.No_folioTextBox.Text), locerror)

            If Me.TextBoxHomologacion.Text.Length > 0 Then
                Me.NueRelSucursalesHomologacion(Me.Clv_SucursalTextBox.Text, Me.TextBoxHomologacion.Text)

                If res = 1 Then
                    MsgBox(msj, MsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            'NueRelSucursalesDatosFiscalesCFD(Clv_SucursalTextBox.Text, tbCalle.Text, tbNumeroExt.Text, tbNumeroInterior.Text, tbColonia.Text, tbLocalidad.Text, tbReferencia.Text, tbMunicipio.Text, tbEstado.Text, tbPais.Text, tbCp.Text, tbSerie.Text, tbFolio.Text)


            bitsist(GloUsuario, 0, LocGloSistema, "Catálogo de Sucursales ", "", "Creación Sucursal", "Clave de la Sucursal: " + Me.Clv_SucursalTextBox.Text, LocClv_Ciudad)

            If locerror = 1 Then
                MsgBox("El folio de factura global ya existe; folio y serie globales no se han guardado.", MsgBoxStyle.Exclamation)
            Else
                MsgBox(mensaje5, MsgBoxStyle.Information)
            End If

            GloBnd = True
            CON.Close()
            Me.Close()


        Catch ex As System.Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub BUSCA(ByVal CLAVE As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONSUCURSALESTableAdapter.Connection = CON
            Me.CONSUCURSALESTableAdapter.Fill(Me.NewSofTvDataSet.CONSUCURSALES, New System.Nullable(Of Integer)(CType(CLAVE, Integer)))
            Me.Consulta_Impresora_SucursalTableAdapter1.Connection = CON
            Me.Consulta_Impresora_SucursalTableAdapter1.Fill(Me.ProcedimientosArnoldo2.Consulta_Impresora_Sucursal, CLAVE)
            Me.Consulta_Generales_FacturasGlobalesTableAdapter.Connection = CON
            Me.Consulta_Generales_FacturasGlobalesTableAdapter.Fill(Me.DataSetarnoldo.Consulta_Generales_FacturasGlobales, CLAVE)
            ConRelSucursalesHomologacion(CLAVE)
            CON.Close()

            'ConRelSucursalesDatosFiscalesCFD(CLAVE)

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub FrmSucursales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If opcion = "N" Then
            Me.CONSUCURSALESBindingSource.AddNew()
            Me.TextBoxHomologacion.Text = 0
            Panel1.Enabled = True
        ElseIf opcion = "C" Then
            Panel1.Enabled = False
            BUSCA(gloClave)
        ElseIf opcion = "M" Then
            Panel1.Enabled = True
            BUSCA(gloClave)
        End If
    End Sub

    Private Sub Clv_SucursalTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_SucursalTextBox.TextChanged
        gloClave = Me.Clv_SucursalTextBox.Text
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.CONSUCURSALESTableAdapter.Connection = CON
        Me.CONSUCURSALESTableAdapter.Delete(gloClave)
        Me.Borra_Impresora_SucursalesTableAdapter.Connection = CON
        Me.Borra_Impresora_SucursalesTableAdapter.Fill(Me.DataSetarnoldo.Borra_Impresora_Sucursales, gloClave)
        Me.Borra_Generales_FacturasGlobalesTableAdapter.Connection = CON
        Me.Borra_Generales_FacturasGlobalesTableAdapter.Fill(Me.DataSetarnoldo.Borra_Generales_FacturasGlobales, gloClave)
        BorRelSucursalesHomologacion(gloClave)

        'BorRelSucursalesDatosFiscalesCFD(gloClave)

        bitsist(GloUsuario, 0, LocGloSistema, "Catálogo de Sucursales ", "", "Eliminó Sucursal", "Clave de la Sucursal: " + Me.Clv_SucursalTextBox.Text, LocClv_Ciudad)
        CON.Close()

        GloBnd = True
        Me.Close()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub NombreTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NombreTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(NombreTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub


    Private Sub ConRelSucursalesHomologacion(ByVal Clv_Sucursal As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConRelSucursalesHomologacion", conexion)
        comando.CommandType = CommandType.StoredProcedure
        Dim reader As SqlDataReader

        Dim parametro As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Sucursal
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            reader = comando.ExecuteReader()
            While (reader.Read())
                Me.TextBoxHomologacion.Text = reader(1).ToString()
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub NueRelSucursalesHomologacion(ByVal Clv_Sucursal As Integer, ByVal NoHomologacion As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueRelSucursalesHomologacion", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Sucursal
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@NoHomologacion", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = NoHomologacion
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Res", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)

        Try
            res = 0
            msj = ""

            conexion.Open()
            comando.ExecuteNonQuery()

            res = parametro3.Value.ToString()
            msj = parametro4.Value.ToString()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub BorRelSucursalesHomologacion(ByVal Clv_Sucursal As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorRelSucursalesHomologacion", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Sucursal
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub TextBoxHomologacion_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxHomologacion.KeyPress
        e.KeyChar = Chr(ValidaKey(TextBoxHomologacion, Asc(e.KeyChar), "N"))
    End Sub

    'Private Sub ConRelSucursalesDatosFiscalesCFD(ByVal Clv_Sucursal As Integer)
    '    Dim conexion As New SqlConnection(MiConexion)
    '    Dim comando As New SqlCommand("ConRelSucursalesDatosFiscalesCFD", conexion)
    '    comando.CommandType = CommandType.StoredProcedure
    '    comando.CommandTimeout = 0
    '    Dim reader As SqlDataReader

    '    Dim par1 As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
    '    par1.Direction = ParameterDirection.Input
    '    par1.Value = Clv_Sucursal
    '    comando.Parameters.Add(par1)

    '    Try
    '        conexion.Open()
    '        reader = comando.ExecuteReader

    '        While (reader.Read())
    '            tbCalle.Text = reader(1).ToString
    '            tbNumeroExt.Text = reader(2).ToString
    '            tbNumeroInterior.Text = reader(3).ToString
    '            tbColonia.Text = reader(4).ToString
    '            tbLocalidad.Text = reader(5).ToString
    '            tbReferencia.Text = reader(6).ToString
    '            tbMunicipio.Text = reader(7).ToString
    '            tbEstado.Text = reader(8).ToString
    '            tbPais.Text = reader(9).ToString
    '            tbCp.Text = reader(10).ToString
    '            tbSerie.Text = reader(11).ToString
    '            tbFolio.Text = reader(12).ToString
    '        End While

    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    Finally
    '        conexion.Close()
    '        conexion.Dispose()
    '    End Try

    'End Sub

    'Private Sub NueRelSucursalesDatosFiscalesCFD(ByVal Clv_Sucursal As Integer, ByVal Calle As String, ByVal NoExterior As String, ByVal NoInterior As String, ByVal Colonia As String, ByVal Localidad As String, ByVal Referencia As String, ByVal Municipio As String, ByVal Estado As String, ByVal Pais As String, ByVal CodigoPostal As String, ByVal Serie As String, ByVal Folio As Integer)
    '    Dim conexion As New SqlConnection(MiConexion)
    '    Dim comando As New SqlCommand("NueRelSucursalesDatosFiscalesCFD", conexion)
    '    comando.CommandType = CommandType.StoredProcedure
    '    comando.CommandTimeout = 0

    '    Dim par1 As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
    '    par1.Direction = ParameterDirection.Input
    '    par1.Value = Clv_Sucursal
    '    comando.Parameters.Add(par1)

    '    Dim par3 As New SqlParameter("@Calle", SqlDbType.VarChar, 250)
    '    par3.Direction = ParameterDirection.Input
    '    par3.Value = Calle
    '    comando.Parameters.Add(par3)

    '    Dim par4 As New SqlParameter("@NoExterior", SqlDbType.VarChar, 50)
    '    par4.Direction = ParameterDirection.Input
    '    par4.Value = NoExterior
    '    comando.Parameters.Add(par4)

    '    Dim par5 As New SqlParameter("@NoInterior", SqlDbType.VarChar, 50)
    '    par5.Direction = ParameterDirection.Input
    '    par5.Value = NoExterior
    '    comando.Parameters.Add(par5)

    '    Dim par6 As New SqlParameter("@Colonia", SqlDbType.VarChar, 150)
    '    par6.Direction = ParameterDirection.Input
    '    par6.Value = Colonia
    '    comando.Parameters.Add(par6)

    '    Dim par7 As New SqlParameter("@Localidad", SqlDbType.VarChar, 150)
    '    par7.Direction = ParameterDirection.Input
    '    par7.Value = Localidad
    '    comando.Parameters.Add(par7)

    '    Dim par8 As New SqlParameter("@Referencia", SqlDbType.VarChar, 150)
    '    par8.Direction = ParameterDirection.Input
    '    par8.Value = Referencia
    '    comando.Parameters.Add(par8)

    '    Dim par9 As New SqlParameter("@Municipio", SqlDbType.VarChar, 150)
    '    par9.Direction = ParameterDirection.Input
    '    par9.Value = Municipio
    '    comando.Parameters.Add(par9)

    '    Dim par10 As New SqlParameter("@Estado", SqlDbType.VarChar, 150)
    '    par10.Direction = ParameterDirection.Input
    '    par10.Value = Estado
    '    comando.Parameters.Add(par10)

    '    Dim par11 As New SqlParameter("@Pais", SqlDbType.VarChar, 150)
    '    par11.Direction = ParameterDirection.Input
    '    par11.Value = Pais
    '    comando.Parameters.Add(par11)

    '    Dim par12 As New SqlParameter("@CodigoPostal", SqlDbType.VarChar, 50)
    '    par12.Direction = ParameterDirection.Input
    '    par12.Value = CodigoPostal
    '    comando.Parameters.Add(par12)

    '    Dim par13 As New SqlParameter("@Serie", SqlDbType.VarChar, 50)
    '    par13.Direction = ParameterDirection.Input
    '    par13.Value = Serie
    '    comando.Parameters.Add(par13)

    '    Dim par14 As New SqlParameter("@Folio", SqlDbType.Int)
    '    par14.Direction = ParameterDirection.Input
    '    par14.Value = Folio
    '    comando.Parameters.Add(par14)

    '    Try
    '        conexion.Open()
    '        comando.ExecuteNonQuery()

    '        'MsgBox(mensaje5, MsgBoxStyle.Information)
    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Exclamation)
    '    Finally
    '        conexion.Close()
    '        conexion.Dispose()
    '    End Try

    'End Sub

    'Private Sub BorRelSucursalesDatosFiscalesCFD(ByVal Clv_Sucursal As Integer)
    '    Dim conexion As New SqlConnection(MiConexion)
    '    Dim comando As New SqlCommand("BorRelSucursalesDatosFiscalesCFD", conexion)
    '    comando.CommandType = CommandType.StoredProcedure
    '    comando.CommandTimeout = 0


    '    Dim par1 As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
    '    par1.Direction = ParameterDirection.Input
    '    par1.Value = Clv_Sucursal
    '    comando.Parameters.Add(par1)

    '    Try
    '        conexion.Open()
    '        comando.ExecuteNonQuery()

    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    Finally
    '        conexion.Close()
    '        conexion.Dispose()
    '    End Try

    'End Sub

End Class