Imports System.Data.SqlClient

Public Class FrmSectore
    Private Desc_sector As String = Nothing
    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing

    Private Sub damedatosbitacora()
        Try
            If eOpcion = "M" Then
                Desc_sector = Me.DescripcionTextBox.Text
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub guarda_bitacora(ByVal op As Integer)
        Try
            Select Case op
                Case 0
                    If eOpcion = "N" Then
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Nuevo Sector", " ", "Nuevo Sector: " + Me.Clv_TxtTextBox.Text, LocClv_Ciudad)
                    ElseIf eOpcion = "M" Then
                        'Desc_sector = Me.DescripcionTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.DescripcionTextBox.Name, Desc_sector, Me.DescripcionTextBox.Text, LocClv_Ciudad)
                    End If
                Case 1
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Nueva Colonia Para un Sector", " ", "Nueva una Colonia Para El Sector: " + Me.NombreComboBox.Text, LocClv_Ciudad)
                Case 2
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Elimino Colonia Para un Sector", " ", "Elimino una Colonia Para El Sector: " + Me.Clv_ColoniaTextBox.Text, LocClv_Ciudad)
                Case 3
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Elimino un Sector", " ", "Se Elimino el Sector: " + Me.Clv_TxtTextBox.Text, LocClv_Ciudad)
            End Select
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub FrmSectore_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraColoniaSecTableAdapter.Connection = CON
        Me.MuestraColoniaSecTableAdapter.Fill(Me.DataSetEric.MuestraColoniaSec, 0, 0, 0)

        If eOpcion = "N" Then
            Me.ToolStripButton2.Enabled = False
            Me.NombreComboBox.Enabled = False
            Me.Button1.Enabled = False
            Me.Button2.Enabled = False
        End If

        If eOpcion = "C" Then
            Me.ConSectorTableAdapter.Connection = CON
            Me.ConSectorTableAdapter.Fill(Me.DataSetEric.ConSector, eClv_Sector, "", "", 3)
            Me.ConRelSectorColoniaTableAdapter.Connection = CON
            Me.ConRelSectorColoniaTableAdapter.Fill(Me.DataSetEric.ConRelSectorColonia, eClv_Sector)
            Me.BindingNavigator1.Enabled = False
            Me.Clv_TxtTextBox.ReadOnly = True
            Me.DescripcionTextBox.ReadOnly = True
            Me.Button1.Enabled = False
            Me.Button2.Enabled = False
            Me.NombreComboBox.Enabled = False
        End If

        If eOpcion = "M" Then
            Me.ConSectorTableAdapter.Connection = CON
            Me.ConSectorTableAdapter.Fill(Me.DataSetEric.ConSector, eClv_Sector, "", "", 3)
            Me.ConRelSectorColoniaTableAdapter.Connection = CON
            Me.ConRelSectorColoniaTableAdapter.Fill(Me.DataSetEric.ConRelSectorColonia, eClv_Sector)
            Me.Clv_TxtTextBox.Enabled = False
            damedatosbitacora()
        End If
        CON.Close()
    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click

        eRes = 0
        eMsg = ""

        If Me.Clv_TxtTextBox.Text.Length = 0 Then
            MsgBox("Captura la Clave.", , "Atenci�n")
            Exit Sub
        End If
        If Me.DescripcionTextBox.Text.Length = 0 Then
            MsgBox("Captura la Descripci�n.", , "Atenci�n")
            Exit Sub
        End If



        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If eOpcion = "M" Then

                Me.ModSectorTableAdapter.Connection = CON
                Me.ModSectorTableAdapter.Fill(Me.DataSetEric.ModSector, eClv_Sector, Me.Clv_TxtTextBox.Text, Me.DescripcionTextBox.Text, eRes, eMsg)
                If eRes = 1 Then
                    MsgBox(eMsg)
                Else
                    MsgBox(mensaje5)
                    guarda_bitacora(0)
                End If
            End If


            If eOpcion = "N" Then
                Me.NueSectorTableAdapter.Connection = CON
                Me.NueSectorTableAdapter.Fill(Me.DataSetEric.NueSector, Me.Clv_TxtTextBox.Text, Me.DescripcionTextBox.Text, eRes, eMsg, eClv_Sector)
                If eRes = 1 Then
                    MsgBox(eMsg)

                Else
                    Me.ConSectorTableAdapter.Connection = CON
                    Me.ConSectorTableAdapter.Fill(Me.DataSetEric.ConSector, eClv_Sector, "", "", 3)
                    Me.ConRelSectorColoniaTableAdapter.Connection = CON
                    Me.ConRelSectorColoniaTableAdapter.Fill(Me.DataSetEric.ConRelSectorColonia, eClv_Sector)
                    Me.BindingNavigator1.Enabled = False
                    Me.Clv_TxtTextBox.ReadOnly = True
                    Me.DescripcionTextBox.ReadOnly = True
                    Me.NombreComboBox.Enabled = True
                    Me.Button1.Enabled = True
                    Me.Button2.Enabled = True
                    eOpcion = "M"
                    MsgBox(mensaje5)
                    guarda_bitacora(0)
                End If
            End If


            CON.Close()
        Catch

        End Try
    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click

        eRes = 0
        eMsg = ""
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.BorSectorTableAdapter.Connection = CON
            Me.BorSectorTableAdapter.Fill(Me.DataSetEric.BorSector, Me.Clv_SectorTextBox.Text, eRes, eMsg)
            If eRes = 1 Then
                MsgBox(eMsg)
            Else
                guarda_bitacora(3)
                Me.ConRelSectorColoniaTableAdapter.Connection = CON
                Me.ConRelSectorColoniaTableAdapter.Fill(Me.DataSetEric.ConRelSectorColonia, eClv_Sector)
                MsgBox(mensaje6)
                Me.Close()
            End If
            CON.Close()
        Catch

        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Agregar()
    End Sub



    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        If Me.ConRelSectorColoniaDataGridView.RowCount > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.BorRelSectorColoniaTableAdapter.Connection = CON
            Me.BorRelSectorColoniaTableAdapter.Fill(Me.DataSetEric.BorRelSectorColonia, Me.Clv_ColoniaTextBox.Text, eRes, eMsg)
            If eRes = 1 Then
                MsgBox(eMsg)
            Else
                Me.ConRelSectorColoniaTableAdapter.Connection = CON
                Me.ConRelSectorColoniaTableAdapter.Fill(Me.DataSetEric.ConRelSectorColonia, eClv_Sector)
                guarda_bitacora(2)
            End If
            CON.Close()
        Else
            MsgBox("Seleccione una Colonia a Eliminar", , "Atenci�n")
        End If
    End Sub

    Private Sub NombreComboBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            Agregar()
        End If
    End Sub

    Private Sub Agregar()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        eRes = 0
        eMsg = ""
        Me.NueRelSectorColoniaTableAdapter.Connection = CON
        Me.NueRelSectorColoniaTableAdapter.Fill(Me.DataSetEric.NueRelSectorColonia, eClv_Sector, CLng(Me.NombreComboBox.SelectedValue), eRes, eMsg)
        If eRes = 1 Then
            MsgBox(eMsg)
        Else
            Me.ConRelSectorColoniaTableAdapter.Connection = CON
            Me.ConRelSectorColoniaTableAdapter.Fill(Me.DataSetEric.ConRelSectorColonia, eClv_Sector)
            guarda_bitacora(1)
        End If
        CON.Close()
    End Sub

    Private Sub NombreComboBox_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NombreComboBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Agregar()
        End If
    End Sub
End Class