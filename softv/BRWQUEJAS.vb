﻿Imports System.Data.SqlClient
Imports System.Text

Public Class BRWQUEJAS
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        GloClv_TipSer = 0
        FrmQueja.Show()
    End Sub

    Private Sub consultar()
        If gloClave > 0 Then
            opcion = "C"
            gloClave = Me.Clv_calleLabel2.Text
            GloClv_TipSer = Me.Label9.Text
            FrmQueja.Show()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        consultar()
    End Sub

    Private Sub modificar()
        If gloClave > 0 Then
            opcion = "M"
            gloClave = Me.Clv_calleLabel2.Text
            GloClv_TipSer = Me.Label9.Text
            FrmQueja.Show()
        Else
            MsgBox("Seleccione algun Tipo de Servicio")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BuscaBloqueadoTableAdapter.Connection = CON
        Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, Me.ContratoLabel1.Text, NUM, num2)
        If NUM = 0 Then
            modificar()
        ElseIf num2 = 1 Then
            MsgBox("El Cliente " + Me.ContratoLabel1.Text + " Ha Sido Bloqueado por lo que no se Podrá Ejecutar la Orden ", MsgBoxStyle.Exclamation)
        End If
        CON.Close()
    End Sub

    'Private Sub Busca(ByVal op As Integer)
    '    Dim sTATUS As String = "P"
    '    Dim CON As New SqlConnection(MiConexion)
    '    CON.Open()
    '    Try
    '        If Me.RadioButton1.Checked = True Then
    '            sTATUS = "P"
    '        ElseIf Me.RadioButton2.Checked = True Then
    '            sTATUS = "E"
    '        ElseIf Me.RadioButton3.Checked = True Then
    '            sTATUS = "V"
    '        End If


    '        If op = 0 Then 'contrato
    '            If IsNumeric(Me.TextBox1.Text) = True Then
    '                Me.BUSCAQUEJASTableAdapter.Connection = CON
    '                Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, New System.Nullable(Of Integer)(CType(Me.ComboBox1.SelectedValue, Integer)), 0, Me.TextBox1.Text, "", "", "", New System.Nullable(Of Integer)(CType(0, Integer)))
    '                'Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, 0, 0, Me.TextBox1.Text, "", "", "", New System.Nullable(Of Integer)(CType(0, Integer)))
    '            Else
    '                MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
    '            End If

    '        ElseIf op = 1 Then
    '            If Len(Trim(Me.TextBox2.Text)) > 0 Then
    '                Me.BUSCAQUEJASTableAdapter.Connection = CON
    '                Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, New System.Nullable(Of Integer)(CType(Me.ComboBox1.SelectedValue, Integer)), 0, 0, Me.TextBox2.Text, "", "", New System.Nullable(Of Integer)(CType(1, Integer)))
    '                'Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, 0, 0, 0, Me.TextBox2.Text, "", "", New System.Nullable(Of Integer)(CType(1, Integer)))
    '            Else
    '                MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
    '            End If
    '        ElseIf op = 2 Then 'Calle y numero
    '            Me.BUSCAQUEJASTableAdapter.Connection = CON
    '            Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, New System.Nullable(Of Integer)(CType(Me.ComboBox1.SelectedValue, Integer)), 0, 0, "", Me.BCALLE.Text, Me.BNUMERO.Text, New System.Nullable(Of Integer)(CType(2, Integer)))
    '            ' Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, 0, 0, 0, "", Me.BCALLE.Text, Me.BNUMERO.Text, New System.Nullable(Of Integer)(CType(2, Integer)))
    '        ElseIf op = 199 Then 'Status
    '            Me.BUSCAQUEJASTableAdapter.Connection = CON
    '            Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, New System.Nullable(Of Integer)(CType(Me.ComboBox1.SelectedValue, Integer)), 0, 0, sTATUS, "", "", New System.Nullable(Of Integer)(CType(199, Integer)))
    '            'Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, 0, 0, 0, sTATUS, "", "", New System.Nullable(Of Integer)(CType(199, Integer)))
    '        ElseIf op = 3 Then 'clv_Orden
    '            If IsNumeric(Me.TextBox3.Text) = True Then
    '                Me.BUSCAQUEJASTableAdapter.Connection = CON
    '                Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, New System.Nullable(Of Integer)(CType(Me.ComboBox1.SelectedValue, Integer)), Me.TextBox3.Text, 0, "", "", "", New System.Nullable(Of Integer)(CType(3, Integer)))
    '                'Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, 0, Me.TextBox3.Text, 0, "", "", "", New System.Nullable(Of Integer)(CType(3, Integer)))

    '            Else
    '                MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
    '            End If

    '        Else
    '            Me.BUSCAQUEJASTableAdapter.Connection = CON
    '            Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, New System.Nullable(Of Integer)(CType(Me.ComboBox1.SelectedValue, Integer)), 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(4, Integer)))
    '            'Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, 0, 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(4, Integer)))
    '        End If
    '        Me.TextBox1.Clear()
    '        Me.TextBox2.Clear()
    '        Me.TextBox3.Clear()
    '        Me.BNUMERO.Clear()
    '        Me.BCALLE.Clear()

    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try
    '    CON.Close()

    'End Sub

    Private Sub Busca(ByVal op As Integer)
        Dim sTATUS As String = "P"
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            If Me.RadioButton1.Checked = True Then
                sTATUS = "P"
            ElseIf Me.RadioButton2.Checked = True Then
                sTATUS = "E"
            ElseIf Me.RadioButton3.Checked = True Then
                sTATUS = "V"
            End If


            If op = 0 Then 'contrato
                If IsNumeric(Me.TextBox1.Text) = True Then

                    BuscaQuejasSeparado(Me.ComboBox1.SelectedValue, 0, Me.TextBox1.Text, "", "", "", "", "", 0)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If

            ElseIf op = 1 Then
                If Len(Trim(Me.TextBox2.Text)) > 0 Or Len(Trim(Me.APaternoTextBox.Text)) > 0 Or Len(Trim(Me.AMaternoTextBox.Text)) > 0 Then

                    BuscaQuejasSeparado(Me.ComboBox1.SelectedValue, 0, 0, Me.TextBox2.Text, Me.APaternoTextBox.Text, Me.AMaternoTextBox.Text, "", "", 1)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If
            ElseIf op = 2 Then 'Calle y numero

                BuscaQuejasSeparado(Me.ComboBox1.SelectedValue, 0, 0, "", "", "", Me.BCALLE.Text, Me.BNUMERO.Text, 2)
            ElseIf op = 199 Then 'Status

                BuscaQuejasSeparado(Me.ComboBox1.SelectedValue, 0, 0, sTATUS, "", "", "", "", 199)
            ElseIf op = 3 Then 'clv_Orden
                If IsNumeric(Me.TextBox3.Text) = True Then

                    BuscaQuejasSeparado(Me.ComboBox1.SelectedValue, Me.TextBox3.Text, 0, "", "", "", "", "", 3)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If

            Else


                BuscaQuejasSeparado(Me.ComboBox1.SelectedValue, 0, 0, "", "", "", "", "", 4)
            End If
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()
            Me.TextBox3.Clear()
            Me.BNUMERO.Clear()
            Me.BCALLE.Clear()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub

    Private Sub BuscaQuejasSeparado(ByVal Clv_TipSerQ As Integer, ByVal Clv_QuejaQ As Long, ByVal ContratoQ As Long, ByVal NombreQ As String, ByVal ApePaternoQ As String, _
                                ByVal ApeMaternoQ As String, ByVal CalleQ As String, ByVal NumeroQ As String, ByVal OpQ As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSQL As New StringBuilder

        StrSQL.Append("EXEC BuscaQuejasSeparado ")
        StrSQL.Append(Clv_TipSerQ & ", ")
        StrSQL.Append(Clv_QuejaQ & ", ")
        StrSQL.Append(ContratoQ & ", ")
        StrSQL.Append("'" & NombreQ & "', ")
        StrSQL.Append("'" & ApePaternoQ & "', ")
        StrSQL.Append("'" & ApeMaternoQ & "', ")
        StrSQL.Append("'" & CalleQ & "', ")
        StrSQL.Append("'" & NumeroQ & "', ")
        StrSQL.Append(OpQ)

        Dim DA As New SqlDataAdapter(StrSQL.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.DataGridView1.DataSource = BS.DataSource

            'gloClave = Me.DataGridView1.SelectedCells(0).Value
            'GloClv_TipSer = Me.DataGridView1.SelectedCells(8).Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Busca(1)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(0)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(3)
        End If
    End Sub



    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Busca(3)
    End Sub



    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        gloClave = Me.Clv_calleLabel2.Text
    End Sub

    Private Sub BRWQUEJAS_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            Me.ComboBox1.SelectedValue = GloClv_TipSer
            Me.ComboBox1.Text = GloNom_TipSer
            Me.ComboBox1.FindString(GloNom_TipSer)
            Me.ComboBox1.Text = GloNom_TipSer
            Busca(4)
        End If
    End Sub

    Private Sub BRWQUEJAS_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load 'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla según sea necesario.


        Dim CON As New SqlConnection(MiConexion)

        colorea(Me, Me.Name)
        'Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        'TODO: esta línea de código carga datos en la tabla 'DataSetLidia2.MuestraTipSerPrincipal2' Puede moverla o quitarla según sea necesario.
        CON.Open()
        Me.MuestraTipSerPrincipal2TableAdapter.Connection = CON
        Me.MuestraTipSerPrincipal2TableAdapter.Fill(Me.DataSetLidia2.MuestraTipSerPrincipal2)
        CON.Close()

        GloClv_TipSer = Me.ComboBox1.SelectedValue
        Busca(4)
        ObtenerClv()
        GloBnd = False

    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Busca(2)
    End Sub

    Private Sub BCALLE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCALLE.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub


    Private Sub BNUMERO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BNUMERO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub


    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button4.Enabled = True Then
            modificar()
        ElseIf Button3.Enabled = True Then
            consultar()
        End If
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        Busca(199)
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        Busca(199)
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        Busca(199)
    End Sub


    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox3.TextChanged

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If Me.ComboBox1.SelectedIndex <> -1 Then
            Busca(4)
        End If
    End Sub

    Private Sub Button1_Click_1(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Busca(1)
    End Sub

    Private Sub ObtenerClv()
        Try
            'Contrato = Integer.Parse(DataGridView1.SelectedRows(0).Cells("Clv_Queja").Value.ToString())
            Clv_calleLabel2.Text = DataGridView1.SelectedRows(0).Cells("Clv_Queja").Value.ToString()
            gloClave = DataGridView1.SelectedRows(0).Cells("Clv_Queja").Value.ToString()
            ContratoLabel1.Text = DataGridView1.SelectedRows(0).Cells("gcContrato").Value.ToString()
            Contrato = Integer.Parse(DataGridView1.SelectedRows(0).Cells("gcContrato").Value.ToString())
            CMBNombreTextBox.Text = DataGridView1.SelectedRows(0).Cells("NOMBRE").Value.ToString() + " " + DataGridView1.SelectedRows(0).Cells("Apellido_Paterno").Value.ToString() + " " + DataGridView1.SelectedRows(0).Cells("Apellido_Materno").Value.ToString()
            CALLELabel1.Text = DataGridView1.SelectedRows(0).Cells("CALLE").Value.ToString()
            NUMEROLabel1.Text = DataGridView1.SelectedRows(0).Cells("NUMERO").Value.ToString()
            Me.Label9.Text = DataGridView1.SelectedRows(0).Cells("Clv_TipSer").Value.ToString()

            GloClv_TipSer = DataGridView1.SelectedRows(0).Cells("Clv_TipSer").Value.ToString()
        Catch ex As Exception
            Contrato = 0
        End Try
    End Sub

    Private Sub DataGridView1_CurrentCellChanged(sender As System.Object, e As System.EventArgs) Handles DataGridView1.CurrentCellChanged
        ObtenerClv()
    End Sub

    Private Sub DataGridView1_DataSourceChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.DataSourceChanged
        ObtenerClv()
    End Sub
End Class