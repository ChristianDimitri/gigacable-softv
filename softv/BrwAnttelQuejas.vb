Imports System.Data.SqlClient
Public Class BrwAnttelQuejas
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        GloClv_TipSer = 0
        FrmQuejasAntTel.Show()
    End Sub

    Private Sub consultar()
        If gloClave > 0 Then
            opcion = "C"
            gloClave = Me.Clv_calleLabel2.Text
            GloClv_TipSer = Me.Label9.Text
            FrmQuejasAntTel.Show()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.DataGridView1.RowCount > 0 Then
            consultar()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub modificar()
        If gloClave > 0 Then
            opcion = "M"
            gloClave = Me.Clv_calleLabel2.Text
            If IsNumeric(Me.Label9.Text) = True Then
                GloClv_TipSer = CLng(Me.Label9.Text)
            Else
                GloClv_TipSer = 0
            End If

            FrmQuejasAntTel.Show()
        Else
            MsgBox("Seleccione algun Tipo de Servicio")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Me.DataGridView1.RowCount > 0 Then
            Me.BuscaBloqueadoTableAdapter.Connection = CON
            Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, Me.ContratoLabel1.Text, NUM, num2)
            If NUM = 0 Or num2 = 0 Then
                modificar()
            Else
                MsgBox("El Cliente " + Me.ContratoLabel1.Text + " Ha Sido Bloqueado por lo que no se Podr� Llevar a cabo la Queja", MsgBoxStyle.Exclamation)
            End If
        Else
            MsgBox(mensaje1)
        End If
        CON.Close()
    End Sub


    Private Sub BrwAnttelQuejas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        'Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        Me.MuestraTipSerPrincipal2TableAdapter.Connection = CON
        Me.MuestraTipSerPrincipal2TableAdapter.Fill(Me.DataSetLidia2.MuestraTipSerPrincipal2)
        Me.MUESTRAUSUARIOSTableAdapter.Connection = CON
        Me.MUESTRAUSUARIOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAUSUARIOS, 2)
        GloClv_TipSer = Me.ComboBox2.SelectedValue
        Busca(4)
        GloBnd = False
        CON.Close()
    End Sub

    Private Sub Busca(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim sTATUS As Integer = 0
        Try

            If Me.CheckBox1.Checked = True And Me.CheckBox2.Checked = False Then
                sTATUS = 1
            ElseIf Me.CheckBox2.Checked = True And Me.CheckBox1.Checked = False Then
                sTATUS = 2
            Else
                sTATUS = 0
            End If

            If IsNumeric(Me.ComboBox2.SelectedValue) = True Then
                If op = 0 Then 'contrato
                    If IsNumeric(Me.TextBox1.Text) = True Then
                        Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                        Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, Me.TextBox1.Text, "", "", "", New System.Nullable(Of Integer)(CType(0, Integer)), 0, sTATUS)
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, Me.TextBox1.Text, "", "", "", New System.Nullable(Of Integer)(CType(0, Integer)), 0, sTATUS)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If

                ElseIf op = 1 Then
                    If Len(Trim(Me.TextBox2.Text)) > 0 Then
                        Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                        Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, Me.TextBox2.Text, "", "", New System.Nullable(Of Integer)(CType(1, Integer)), 0, sTATUS)
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, Me.TextBox2.Text, "", "", New System.Nullable(Of Integer)(CType(1, Integer)), 0, sTATUS)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                ElseIf op = 2 Then 'Calle y numero
                    Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                    Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, "", Me.BCALLE.Text, Me.BNUMERO.Text, New System.Nullable(Of Integer)(CType(2, Integer)), 0, sTATUS)
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, "", Me.BCALLE.Text, Me.BNUMERO.Text, New System.Nullable(Of Integer)(CType(2, Integer)), 0, sTATUS)
                ElseIf op = 11 Then 'Usuario
                    Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                    Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(11, Integer)), Me.ComboBox1.SelectedValue, sTATUS)
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(11, Integer)), Me.ComboBox1.SelectedValue, sTATUS)
                ElseIf op = 12 Then 'con queja
                    Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                    Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(12, Integer)), 0, sTATUS)
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(12, Integer)), 0, sTATUS)
                ElseIf op = 13 Then 'Sin queja
                    Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                    Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(13, Integer)), 0, sTATUS)
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(13, Integer)), 0, sTATUS)
                ElseIf op = 199 Then 'Status
                    Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                    Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, sTATUS, "", "", New System.Nullable(Of Integer)(CType(199, Integer)), 0, sTATUS)
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, sTATUS, "", "", New System.Nullable(Of Integer)(CType(199, Integer)), 0, sTATUS)
                ElseIf op = 3 Then 'clv_Orden
                    If IsNumeric(Me.Clv_llamada.Text) = True Then
                        Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                        Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), Me.Clv_llamada.Text, 0, "", "", "", New System.Nullable(Of Integer)(CType(3, Integer)), 0, sTATUS)
                        'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, Me.Clv_llamada.Text, 0, "", "", "", New System.Nullable(Of Integer)(CType(3, Integer)), 0, sTATUS)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                Else
                    Me.BUSCALLAMADASDEINTERNETTableAdapter.Connection = CON
                    Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, New System.Nullable(Of Integer)(CType(Me.ComboBox2.SelectedValue, Integer)), 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(4, Integer)), 0, sTATUS)
                    'Me.BUSCALLAMADASDEINTERNETTableAdapter.Fill(Me.NewSofTvDataSet.BUSCALLAMADASDEINTERNET, 0, 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(4, Integer)), 0, sTATUS)
                End If
                Me.TextBox1.Clear()
                Me.TextBox2.Clear()
                Me.Clv_llamada.Clear()
                Me.BNUMERO.Clear()
                Me.BCALLE.Clear()
            Else
                MsgBox("Seleccione el Tipo de Servicio")
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub



    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged

        Busca(12)

    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged

        Busca(13)

    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        If IsNumeric(Me.ComboBox1.SelectedValue) = True Then
            Busca(11)
        End If
    End Sub
    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(1)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(0)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Busca(3)
    End Sub


    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        gloClave = Me.Clv_calleLabel2.Text
    End Sub

    Private Sub BrwAnttelQuejas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            Me.ComboBox2.SelectedValue = GloClv_TipSer
            Me.ComboBox2.Text = GloNom_TipSer
            Me.ComboBox2.FindString(GloNom_TipSer)
            Me.ComboBox2.Text = GloNom_TipSer
            Busca(4)


        End If
    End Sub



    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Busca(2)
    End Sub

    Private Sub BCALLE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCALLE.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub

   

    Private Sub BNUMERO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BNUMERO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub


    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button4.Enabled = True Then
            modificar()
        ElseIf Button3.Enabled = True Then
            consultar()
        End If
    End Sub

    Private Sub Clv_llamada_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Clv_llamada.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(3)
        End If
    End Sub


    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If IsNumeric(Me.ComboBox1.SelectedValue) = True Then
            Busca(11)
        End If
    End Sub
    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        If Me.ComboBox2.SelectedIndex <> -1 Then
            GloClv_TipSer = Me.ComboBox2.SelectedValue
            Busca(4)
        End If
    End Sub
End Class