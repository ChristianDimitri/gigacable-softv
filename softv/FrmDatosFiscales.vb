Imports System.Data.SqlClient
Public Class FrmDatosFiscales
    'Variables bitacora
    Private Razon_social As String = Nothing
    Private RFC As String = Nothing
    Private CURP As String = Nothing
    Private Calle As String = Nothing
    Private Numero As String = Nothing
    Private entrecalles As String = Nothing
    Private Colonia As String = Nothing
    Private CodigoPostal As String = Nothing
    Private Estado As String = Nothing
    Private Ciudad As String = Nothing
    Private Telefono As String = Nothing
    Private Fax As String = Nothing
    Private Pais As String = Nothing
    'Fin Variables bitacora 
    Private Sub Guarda_Rel_DatosFiscales_Pais(ByVal contrato As Long, ByVal Pais As String)
        Dim CON01 As New SqlConnection(MiConexion)
        Dim SQL As New SqlCommand()
        Try
            CON01.Open()
            SQL = New SqlCommand()
            With SQL
                .CommandText = "Guarda_Rel_DatosFiscales_Pais"
                .Connection = CON01
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm.Value = contrato
                prm.Direction = ParameterDirection.Input
                .Parameters.Add(prm)

                prm = New SqlParameter("@Pais", SqlDbType.VarChar, 150)
                prm.Value = Pais
                prm.Direction = ParameterDirection.Input
                .Parameters.Add(prm)

                Dim i As Integer = .ExecuteNonQuery()
            End With
            CON01.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Consulta_Pais_Fiscal(ByVal contrato As Long)
        Dim CON01 As New SqlConnection(MiConexion)
        Dim SQL As New SqlCommand()
        Try
            CON01.Open()
            SQL = New SqlCommand()
            With SQL
                .CommandText = "Consulta_Pais_Fiscal"
                .CommandTimeout = 0
                .Connection = CON01
                .CommandType = CommandType.StoredProcedure

                Dim PRM As New SqlParameter("@contrato", SqlDbType.BigInt)
                PRM.Direction = ParameterDirection.Input
                PRM.Value = contrato
                .Parameters.Add(PRM)

                Dim reader As SqlDataReader = .ExecuteReader()

                While (reader.Read)
                    TxtPais.Text = reader.GetValue(1)
                End While
            End With
            CON01.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub damedatosbitacora()
        Try
            If OpcionCli = "M" Then
                Razon_social = Me.RAZON_SOCIALTextBox.Text
                RFC = Me.RFCTextBox.Text
                CURP = Me.CURPTextBox.Text
                Calle = Me.CALLE_RSTextBox.Text
                Numero = Me.NUMERO_RSTextBox.Text
                entrecalles = Me.ENTRECALLESTextBox.Text
                Colonia = Me.COLONIA_RSTextBox.Text
                CodigoPostal = Me.CP_RSTextBox.Text
                Estado = Me.ESTADO_RSTextBox.Text
                Ciudad = Me.CIUDAD_RSTextBox.Text
                Telefono = Me.TELEFONO_RSTextBox.Text
                Fax = Me.FAX_RSTextBox.Text
                Pais = Me.TxtPais.Text
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub guardabitacora(ByVal contrato As Integer)
        Try
            If OpcionCli = "M" Then
                'Razon_social = Me.RAZON_SOCIALTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.RAZON_SOCIALTextBox.Name, Razon_social, Me.RAZON_SOCIALTextBox.Text, LocClv_Ciudad)
                'RFC = Me.RFCTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.RFCTextBox.Name, RFC, Me.RFCTextBox.Text, LocClv_Ciudad)
                'CURP = Me.CURPTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.CURPTextBox.Name, CURP, Me.CURPTextBox.Text, LocClv_Ciudad)
                'Calle = Me.CALLE_RSTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.CALLE_RSTextBox.Name, Calle, Me.CALLE_RSTextBox.Text, LocClv_Ciudad)
                'Numero = Me.NUMERO_RSTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.NUMERO_RSTextBox.Name, Numero, Me.NUMERO_RSTextBox.Text, LocClv_Ciudad)
                'entrecalles = Me.ENTRECALLESTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.ENTRECALLESTextBox.Name, entrecalles, Me.ENTRECALLESTextBox.Text, LocClv_Ciudad)
                'Colonia = Me.COLONIA_RSTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.COLONIA_RSTextBox.Name, Colonia, Me.COLONIA_RSTextBox.Text, LocClv_Ciudad)
                'CodigoPostal = Me.CP_RSTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.CP_RSTextBox.Name, CodigoPostal, Me.CP_RSTextBox.Text, LocClv_Ciudad)
                'Estado = Me.ESTADO_RSTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.ESTADO_RSTextBox.Name, Estado, Me.ESTADO_RSTextBox.Text, LocClv_Ciudad)
                'Ciudad = Me.CIUDAD_RSTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.CIUDAD_RSTextBox.Name, Ciudad, Me.CIUDAD_RSTextBox.Text, LocClv_Ciudad)
                'Pais = Me.txtPais
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, "Pais Fiscal", Pais, Me.TxtPais.Text, LocClv_Ciudad)
                'Telefono = Me.TELEFONO_RSTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.TELEFONO_RSTextBox.Name, Telefono, Me.TELEFONO_RSTextBox.Text, LocClv_Ciudad)
                'Fax = Me.FAX_RSTextBox.Text
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.FAX_RSTextBox.Name, Fax, Me.FAX_RSTextBox.Text, LocClv_Ciudad)
            ElseIf OpcionCli = "N" Then
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, "Se Capturaron Los Datos Fiscales Del Cliente", "", "Se Capturaron Los Datos Fiscales Del Cliente", LocClv_Ciudad)
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CONDatosFiscalesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONDatosFiscalesBindingSource.EndEdit()
        Me.CONDatosFiscalesTableAdapter.Connection = CON
        Me.CONDatosFiscalesTableAdapter.Update(Me.NewSofTvDataSet.CONDatosFiscales)
        CON.Close()
    End Sub


    Private Sub CONDatosFiscalesBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONDatosFiscalesBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.Validate()
            Me.CONDatosFiscalesBindingSource.EndEdit()
            Me.CONDatosFiscalesTableAdapter.Connection = CON
            Me.CONDatosFiscalesTableAdapter.Update(Me.NewSofTvDataSet.CONDatosFiscales)
            Guarda_Rel_DatosFiscales_Pais(Contrato, Me.TxtPais.Text)
            NueRelDatosFiscales(Contrato, tbMunicipio.Text)
            MsgBox(mensaje5)
            guardabitacora(Contrato)
            ConRelDatosFiscales(Contrato)
            Me.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub Busca()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.CONDatosFiscalesTableAdapter.Connection = CON
            Me.CONDatosFiscalesTableAdapter.Fill(Me.NewSofTvDataSet.CONDatosFiscales, New System.Nullable(Of Long)(CType(Contrato, Long)))
            Consulta_Pais_Fiscal(Contrato)
            ConRelDatosFiscales(Contrato)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub

    Private Sub CP_RSTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CP_RSTextBox.TextChanged

    End Sub

    Private Sub CP_RSLabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub FrmDatosFiscales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)

        'colorea(Me, Me.Name)
        If OpcionCli = "C" Then
            Me.CONDatosFiscalesBindingNavigator.Enabled = False
            Me.Panel1.Enabled = False
        ElseIf OpcionCli = "M" Then
            Me.CONDatosFiscalesBindingNavigator.Enabled = True
            Me.Panel1.Enabled = True
        End If

        Busca()
        If IsNumeric(Me.ContratoTextBox.Text) = False Then
            Me.CONDatosFiscalesBindingSource.AddNew()
            Me.ContratoTextBox.Text = Contrato
            Me.IVADESGLOSADOTextBox.Text = 1
        End If
        damedatosbitacora()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.CONDatosFiscalesBindingSource.CancelEdit()
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        MsgBox(mensaje6)
        Me.CONDatosFiscalesTableAdapter.Connection = CON
        Me.CONDatosFiscalesTableAdapter.Delete(Contrato)
        CON.Close()
        Me.Close()

    End Sub

    Private Sub RAZON_SOCIALTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles RAZON_SOCIALTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.RAZON_SOCIALTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub RAZON_SOCIALTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RAZON_SOCIALTextBox.TextChanged

    End Sub

    Private Sub CALLE_RSTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles CALLE_RSTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.CALLE_RSTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub CALLE_RSTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CALLE_RSTextBox.TextChanged

    End Sub

    Private Sub NUMERO_RSTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NUMERO_RSTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.NUMERO_RSTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub NUMERO_RSTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NUMERO_RSTextBox.TextChanged

    End Sub

    Private Sub ENTRECALLESTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ENTRECALLESTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.ENTRECALLESTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub ENTRECALLESTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ENTRECALLESTextBox.TextChanged

    End Sub

    Private Sub COLONIA_RSTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles COLONIA_RSTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.COLONIA_RSTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub COLONIA_RSTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles COLONIA_RSTextBox.TextChanged

    End Sub

    Private Sub ESTADO_RSTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ESTADO_RSTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.ESTADO_RSTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub ESTADO_RSTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ESTADO_RSTextBox.TextChanged

    End Sub

    Private Sub CIUDAD_RSTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles CIUDAD_RSTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.CIUDAD_RSTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub CIUDAD_RSTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CIUDAD_RSTextBox.TextChanged

    End Sub

    Private Sub TELEFONO_RSTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TELEFONO_RSTextBox.TextChanged

    End Sub

    Private Sub CURPTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CURPTextBox.TextChanged

    End Sub

    Private Sub ConRelDatosFiscales(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConRelDatosFiscales", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        Dim reader As SqlDataReader

        Dim par1 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = Contrato
        comando.Parameters.Add(par1)

        Try
            conexion.Open()
            reader = comando.ExecuteReader

            While (reader.Read())
                tbMunicipio.Text = reader(0).ToString()
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
        End Try

    End Sub

    Private Sub NueRelDatosFiscales(ByVal Contrato As Long, ByVal Municipio As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueRelDatosFiscales", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = Contrato
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Municipio", SqlDbType.VarChar, 250)
        par2.Direction = ParameterDirection.Input
        par2.Value = Municipio
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
        End Try

    End Sub

End Class