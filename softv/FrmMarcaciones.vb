Imports System.Data.SqlClient
Public Class FrmMarcaciones

    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function
    Private Sub Consulta_MarcacionesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Consulta_MarcacionesBindingNavigatorSaveItem.Click
        Dim ConSof As New SqlConnection(MiConexion)
        If Me.NumeracionTextBox.Text.Trim.Length = 0 Then
            MsgBox("Se Debe de Capturar una Numeraci�n Correcta", MsgBoxStyle.Exclamation)
        ElseIf IsNumeric(Me.CostoTextBox.Text) = False Then
            MsgBox("Se Debe de Capturar un Costo", MsgBoxStyle.Exclamation)
        Else
            Me.Consulta_MarcacionesTableAdapter.Connection = ConSof
            Me.Validate()
            Me.Consulta_MarcacionesBindingSource.EndEdit()
            Me.Consulta_MarcacionesTableAdapter.Update(Me.DataSetLidia2.Consulta_Marcaciones)
            ConSof.Close()
            MsgBox("Se guardo con �xito", MsgBoxStyle.Information)
            bec_bnd = True
            Me.Close()
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim ConSof As New SqlConnection(MiConexion)
        Me.Consulta_MarcacionesTableAdapter.Connection = ConSof
        Me.Consulta_MarcacionesTableAdapter.Delete(Me.Clv_MarcacionTextBox.Text)
        ConSof.Close()
        MsgBox("La Marcaci�n Ha Sido Eliminada con �xito", MsgBoxStyle.Information)
        bec_bnd = True
        Me.Close()
    End Sub

    Private Sub FrmMarcaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim ConSof As New SqlConnection(MiConexion)
        If opcion = "N" Then
            Me.Consulta_MarcacionesBindingSource.AddNew()
            Activa(False)
        ElseIf (opcion = "C") Then
            ConSof.Open()
            Me.Consulta_MarcacionesTableAdapter.Connection = ConSof
            Me.Consulta_MarcacionesTableAdapter.Fill(Me.DataSetLidia2.Consulta_Marcaciones, Clv_Marcacion)
            ConSof.Close()
            Me.Consulta_MarcacionesBindingNavigator.Enabled = False
        ElseIf (opcion = "M") Then
            ConSof.Open()
            Me.Consulta_MarcacionesTableAdapter.Connection = ConSof
            Me.Consulta_MarcacionesTableAdapter.Fill(Me.DataSetLidia2.Consulta_Marcaciones, Clv_Marcacion)
            ConSof.Close()
            Me.Consulta_MarcacionesBindingNavigator.Enabled = True
        End If
        If opcion = "C" Then
            Activa(True)
        End If

    End Sub
    Private Sub Activa(ByVal ac As Boolean)
        Me.NumeracionTextBox.ReadOnly = ac
        Me.DescripcionTextBox.ReadOnly = ac
        Me.CostoTextBox.ReadOnly = ac
        Me.EventosTextBox.ReadOnly = ac
    End Sub

    Private Sub NumeracionTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NumeracionTextBox.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub

    Private Sub CostoTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles CostoTextBox.KeyPress
        'Dim KeyAscii2 As Short = CShort(Asc(e.KeyChar))
        'KeyAscii2 = CShort(SoloNumeros(KeyAscii2))
        'If KeyAscii2 = 0 Then
        '    e.Handled = True
        'End If
    End Sub

    Private Sub CostoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CostoTextBox.TextChanged

    End Sub
End Class