Imports System.Data.SqlClient
Public Class BrwSelContrato

    Private Sub BrwSelContrato_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        Me.DameClientesActivosTableAdapter.Connection = CON
        Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, "", "", "", "", 3)
        CON.Close()
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.CONTRATOLabel1.Text.Length > 0 Then
            eGloContrato = Me.CONTRATOLabel1.Text
            eGloSubContrato = Me.CONTRATOLabel1.Text
            eContrato = Me.CONTRATOLabel1.Text
        Else
            eGloContrato = 0
            eGloSubContrato = 0
            eContrato = 0
        End If
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        eGloContrato = 0
        eGloSubContrato = 0
        Me.Close()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If IsNumeric(Me.TextBox1.Text) = True Then
                Me.DameClientesActivosTableAdapter.Connection = CON
                Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
            End If
        End If
        CON.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.TextBox1.Text) = True Then
            Me.DameClientesActivosTableAdapter.Connection = CON
            Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
        End If
        CON.Close()
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox2.Text.Length > 0 Then
                Me.DameClientesActivosTableAdapter.Connection = CON
                Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, Me.TextBox2.Text, "", "", "", 1)
            End If
        End If
        CON.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Me.TextBox2.Text.Length > 0 Then
            Me.DameClientesActivosTableAdapter.Connection = CON
            Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, Me.TextBox2.Text, "", "", "", 1)
        End If
        CON.Close()
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
            If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
            If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
            Me.DameClientesActivosTableAdapter.Connection = CON
            Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
        End If
        CON.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
        If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
        If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
        Me.DameClientesActivosTableAdapter.Connection = CON
        Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
        CON.Close()
    End Sub

    Private Sub TextBox4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox4.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
            If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
            If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
            Me.DameClientesActivosTableAdapter.Connection = CON
            Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
        End If
        CON.Close()
    End Sub

    Private Sub TextBox5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox5.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
            If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
            If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
            Me.DameClientesActivosTableAdapter.Connection = CON
            Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
        End If
        CON.Close()
    End Sub
End Class