Imports System.Data.Sql
Imports System.Data.SqlClient


Public Class ProcesoReseteoMasivo

    Dim res As Integer = 0
    Private Sub ProcesoReseteoMasivo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
    End Sub

    Private Sub Ejecutar(ByVal Inicio As Long, ByVal Final As Long)
        'Dim CON As New SqlConnection(MiConexion)
        'Dim comando As SqlCommand = New SqlCommand("ResetServCteTodos", CON)
        'comando.CommandType = CommandType.StoredProcedure
        'comando.CommandTimeout = 0
        'comando.Parameters.Add(New SqlParameter("@inicio", inicio))
        'comando.Parameters.Add(New SqlParameter("@final", fin))
        'comando.Parameters.Add(New SqlParameter("@Res", SqlDbType.Int))
        'comando.Parameters("@Res").Value = 0
        'comando.Parameters("@Res").Direction = ParameterDirection.Output
        'comando.Parameters.Add(New SqlParameter("@Msg", SqlDbType.VarChar))
        'comando.Parameters("@Msg").Value = ""
        'comando.Parameters("@Msg").Direction = ParameterDirection.Output
        'Dim resultado As Integer = 1
        'Dim mensaje As String = String.Empty
        'Try
        '    CON.Open()
        '    comando.ExecuteReader()
        '    resultado = comando.Parameters("@Res").Value.ToString()
        '    mensaje = comando.Parameters("@Msg").Value.ToString
        'Catch ex As Exception
        '    MsgBox(ex.Message, MsgBoxStyle.Exclamation)

        'End Try
        'CON.Close()

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ResetServCteTodos", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        Dim parametro As New SqlParameter("@Inicio", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Inicio
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Final", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Final
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Res", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            If CInt(parametro3.Value.ToString()) = 1 Then
                MsgBox(parametro4.Value.ToString())
            Else
                MsgBox("Proceso realizado con �xito.", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try



    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim inicio As Integer
        Dim fin As Integer
        Try
            inicio = Convert.ToInt64(TextBox1.Text.ToString.Trim)
            fin = Convert.ToInt64(TextBox2.Text.ToString.Trim)
        Catch ex As Exception
            MessageBox.Show("Los datos deben de ser numericos", "Error")
        End Try
        If inicio < fin Then
            Label1.Visible = True
            ProgressBar1.Visible = True
            Me.Refresh()
            ProgressBar1.Maximum = 100
            ProgressBar1.Minimum = 0
            ProgressBar1.Value = 50
            Ejecutar(inicio, fin)
            ProgressBar1.Value = 100
            'If res = 0 Then
            '    MessageBox.Show("El Proceso se Realiz� con �xito.", "Aviso")
            'Else
            '    MessageBox.Show("No se realiz� el Proceso Satisfactoriamente.", "Aviso")
            'End If
        Else
            MessageBox.Show("Los rangos son incorrectos favor de verificarlos", "Error")
        End If
        Label1.Visible = False
        ProgressBar1.Visible = False
        ProgressBar1.Value = 0
        Me.Refresh()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class