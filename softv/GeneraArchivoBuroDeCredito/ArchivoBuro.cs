﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using Softv.BAL;

namespace GeneraArchivoBuroDeCredito
{
    public class ArchivoBuro
    {
        
     
        public string Directorio = "";
        ClienteBuroConfiguracion cbConfiguracion = new ClienteBuroConfiguracion();
        public void GenerarArchivoBuro()
        {
            //Obtenemos el encabezado que se haya configurado en el sistema.
            List<ClienteBuroConfiguracion> listaConfiguraciones = new List<ClienteBuroConfiguracion>();
            listaConfiguraciones = ClienteBuroConfiguracion.GetAll();

            if (listaConfiguraciones.Count > 0)
            {
                cbConfiguracion = listaConfiguraciones[0];

                //Encabezado Utilziado solo 1 Vez
                List<Datos> INTF = new List<Datos>();
                INTF.Add(new Datos("INTF", 4, " ", "", false, false));
                INTF.Add(new Datos("10", 2, " ", "", false, false)); // Verción
                INTF.Add(new Datos(cbConfiguracion.KOB.ToString(), 10, " ", "", false, false)); // Clave del Usuario (KOB)
                INTF.Add(new Datos(cbConfiguracion.MemberKOB.ToString(), 16, " ", "", false, false)); // NombreUsuario
                INTF.Add(new Datos("", 2, " ", "", false, false));//N° Cliclo Vacio
                INTF.Add(new Datos((DateTime.Now.Day.ToString().Length == 2 ? DateTime.Now.Day.ToString() : "0" + DateTime.Now.Day.ToString()) +
                    (DateTime.Now.Month.ToString().Length == 2 ? DateTime.Now.Month.ToString() : "0" + DateTime.Now.Month.ToString()) +
                    (DateTime.Now.Year.ToString().Length == 4 ? DateTime.Now.Year.ToString() : "20" + DateTime.Now.Year.ToString()), 8, " ", "", false, false));// FechaDeReporte DDMMYYYY
                INTF.Add(new Datos("", 10, "0", "", false, false)); // Uso Futuro :S XD se rellena con 0
                INTF.Add(new Datos("", 98, " ", "", false, false)); // Informacion Adicional del usuario

                StringBuilder sb = new StringBuilder();
                sb.Append(new Datos().CompletarDatosINTF(INTF));

                SqlConnection cnn = new SqlConnection(Globals.DataAccess.GlobalConectionString);
                SqlCommand cmd = new SqlCommand("uspCargaListadoBuroGeneracionArchivo", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dc = new DataSet();
                da.Fill(dc);

                foreach (DataRow dr in dc.Tables[0].Rows)
                {
                    GenerarDatosCliente(sb, dr);
                }
                List<Datos> TR = new List<Datos>();
                TR.Add(new Datos("TRLR", 4, "", "", false, false));
                TR.Add(new Datos(int.Parse(dc.Tables[1].Rows[0]["SumaAdeudo"].ToString().Replace(".000", "")).ToString(), 14, "0", "", false, true)); // SAldo Actual
                TR.Add(new Datos(int.Parse(dc.Tables[1].Rows[0]["SumaAdeudo"].ToString().Replace(".000", "")).ToString(), 14, "0", "", false, true)); // SAldo Vencido
                TR.Add(new Datos("1", 3, "0", "", false, true)); // Total  De Segmentos INTF 
                TR.Add(new Datos(int.Parse(dc.Tables[1].Rows[0]["Reportados"].ToString().Replace(".000", "")).ToString(), 9, "0", "", false, true)); // PN
                TR.Add(new Datos(int.Parse(dc.Tables[1].Rows[0]["Reportados"].ToString().Replace(".000", "")).ToString(), 9, "0", "", false, true)); // PA
                TR.Add(new Datos("0", 9, "0", "", false, true)); // PE
                TR.Add(new Datos(int.Parse(dc.Tables[1].Rows[0]["Reportados"].ToString().Replace(".000", "")).ToString(), 9, "0", "", false, true)); // TL
                TR.Add(new Datos("0", 6, "0", "", false, true)); // Contador de Blokes
                TR.Add(new Datos("Prueba", 16, " ", "", false, false)); // Nombre Otorgante
                //TR.Add(new Datos("Prueba", 16, " ", "", false, true)); // Nombre Otorgante
                TR.Add(new Datos("", 160, " ", "", false, true)); // Direccion de la Devolución

                sb.Append(new Datos().CompletarDatosINTF(TR));
                FileStream fs = new FileStream(Directorio + @"\GigaCable_" + (DateTime.Now.Day.ToString().Length == 2 ? DateTime.Now.Day.ToString() : "0" + DateTime.Now.Day.ToString()) +
                    (DateTime.Now.Month.ToString().Length == 2 ? DateTime.Now.Month.ToString() : "0" + DateTime.Now.Month.ToString()) + (DateTime.Now.Year.ToString().Length == 4 ? DateTime.Now.Year.ToString() : "20" + DateTime.Now.Year.ToString()) + ".INTF", FileMode.OpenOrCreate);
                StreamWriter sw = new StreamWriter(fs, Encoding.ASCII);
                sw.Write(sb.ToString());
                sw.Flush();
                MessageBox.Show("Generacción de Archivo Exitosa");

            }
            else
            {
                MessageBox.Show("No se puede generar el archivo ya que no se han encontrado las configuraciones del Clave del Usuario y el Nombre del usuario [ KOB y Member KOB ]");
            }

           
        }


       


        public void GenerarDatosCliente(StringBuilder sb, DataRow dr)
        {
            List<Datos> PN = new List<Datos>();
            PN.Add(new Datos("PN", 2, "", "", false, false));
            PN.Add(new Datos(dr["Nombre"].ToString(), 26, "", "", true, false));
            PN.Add(new Datos(dr["Apellido_Paterno"].ToString(), 26, "", "00", true, false));
            PN.Add(new Datos(dr["Apellido_Materno"].ToString(), 26, "", "02", true, false));//Encaso de no tener "No Proporcionado"
            PN.Add(new Datos(dr["RFC"].ToString(), 23, "", "05", true, false));
            //PM.Add() 
            List<Datos> PA = new List<Datos>();
            PA.Add(new Datos("PA", 2, "", "", false, false));
            PA.Add(new Datos(dr["Direccion"].ToString(), 40, "", "", true, false));// Dirección
            PA.Add(new Datos(dr["Colonia"].ToString(), 40, "", "01", true, false)); // Colonia Poblacion
            PA.Add(new Datos(dr["DelegacionMunicipio"].ToString(), 40, "", "02", true, false)); // Delegacion Municipio
            PA.Add(new Datos(dr["DelegacionMunicipio"].ToString(), 40, "", "03", true, false)); // Ciudad
            PA.Add(new Datos(dr["Estado"].ToString(), 4, "", "04", true, false)); // Estado
            PA.Add(new Datos(dr["CP"].ToString(), 5, "", "05", true, false)); // CP

            List<Datos> TL = new List<Datos>();
            TL.Add(new Datos("TL02TL", 6, "", "", false, false));
            TL.Add(new Datos(cbConfiguracion.KOB.ToString(), 10, " ", "01", true, false)); //Clave Otorgante 
            TL.Add(new Datos(cbConfiguracion.MemberKOB.ToString(), 16, " ", "02", true, false)); // Nombre De Usuario
            TL.Add(new Datos(llenarcerosContratos(dr["Contrato"].ToString()), 25, " ", "04", true, false)); // NUmero De Cuenta
            TL.Add(new Datos("I", 1, " ", "05", true, false)); // Tipo Responsabilidad I = Individual
            TL.Add(new Datos("O", 1, " ", "06", true, false)); // Sin límite preestablecido [Antes tenía Revolvente, cuando no debería, ya que para las Cableras es límite preestablecido]
            TL.Add(new Datos("MI", 2, " ", "07", true, false)); // Otros [MI Aplica para "otros"]
            TL.Add(new Datos("MX", 2, " ", "08", true, false)); // Unidad Monetaria
            TL.Add(new Datos("Z", 1, " ", "11", true, false));  // Abierta sin pago pre establecido
            TL.Add(new Datos(int.Parse(dr["Adeudo"].ToString().Replace(".000", "")).ToString(), 9, " ", "12", true, false)); // Adeudo
            TL.Add(new Datos(dr["FechaApertura"].ToString(), 9, " ", "13", true, false)); // Fecha Apertura
            TL.Add(new Datos(dr["FechaUltimoPago"].ToString(), 9, " ", "14", true, false)); // Fecha Ultimo Pago
            TL.Add(new Datos(dr["FechaUltimaDisposicion"].ToString(), 9, " ", "15", true, false)); // Fecha Ultima Dispocición
            TL.Add(new Datos(int.Parse(dr["Adeudo"].ToString().Replace(".000", "")).ToString(), 9, " ", "21", true, false)); // Credito Maximo utilizado por el cliente
            TL.Add(new Datos(int.Parse(dr["Adeudo"].ToString().Replace(".000", "")).ToString(), 9, " ", "22", true, false)); // Saldo Actual
            TL.Add(new Datos(int.Parse(dr["Adeudo"].ToString().Replace(".000", "")).ToString(), 9, " ", "23", true, false)); // Limite de Credito
            TL.Add(new Datos(int.Parse(dr["Adeudo"].ToString().Replace(".000", "")).ToString(), 9, " ", "24", true, false)); // Saldo Vencido
            TL.Add(new Datos(dr["Clv_Buro"].ToString(), 9, " ", "26", true, false)); // Cuenta al Corriente campo de cuanto tiempo llevan sin pagar 
            TL.Add(new Datos("END", 3, " ", "99", true, false)); // Fin De Archivo

            sb.Append(new Datos().CompletarDatosINTF(PN));
            sb.Append(new Datos().CompletarDatosINTF(PA));
            sb.Append(new Datos().CompletarDatosINTF(TL));

        }

        private String llenarcerosContratos(string Str)
        {
            return Str.Length == 1 ? "000" + Str : (Str.Length == 2 ? "00" + Str : (Str.Length == 3 ? "0" + Str : Str));
        }
    }



    public class Datos
    {
        private String _Informacion;
        public String Informacion
        {
            get { return _Informacion; }
            set { _Informacion = value; }
        }

        private String _Prefijo;
        public String Prefijo
        {
            get { return _Prefijo; }
            set { _Prefijo = value; }
        }

        private bool _OcupaLongitud;
        public bool OcupaLongitud
        {
            get { return _OcupaLongitud; }
            set { _OcupaLongitud = value; }
        }

        private int _Longitud;
        public int Longitud
        {
            get { return _Longitud; }
            set { _Longitud = value; }
        }

        private String _ValorDeRellene;
        public String ValorDeRellene
        {
            get { return _ValorDeRellene; }
            set { _ValorDeRellene = value; }
        }

        private bool _AlineacionDerechaAIzquierda;
        public bool AlineacionDerechaAIzquierda
        {
            get { return _AlineacionDerechaAIzquierda; }
            set { _AlineacionDerechaAIzquierda = value; }
        }

        public Datos()
        {
        }

        public Datos(String Informacion, int Longitud, String ValorDeRellene, String Prefijo, bool OcupaLongitud, bool AlineacionDerechaAIzquierda)
        {
            this.Informacion = Informacion;
            this.Longitud = Longitud;
            this.ValorDeRellene = ValorDeRellene;
            this.Prefijo = Prefijo;
            this.OcupaLongitud = OcupaLongitud;
            this.AlineacionDerechaAIzquierda = AlineacionDerechaAIzquierda;
        }

        public String CompletarDatosINTF(List<Datos> ldatos)
        {
            StringBuilder sb = new StringBuilder();
            foreach (Datos d in ldatos)
            {
                d.Informacion = ValidaString(d.Informacion);
                if (d.OcupaLongitud)
                {


                    sb.Append(d.Prefijo + (d.Informacion.Length.ToString().Length == 2 ? d.Informacion.Length.ToString() : "0" + d.Informacion.Length.ToString()) + d.Informacion.Replace("ñ", "n").Replace("Ñ", "N").Replace("ó", "o").Replace("Ó", "O").Replace("í", "i").Replace("Í", "I").Replace("á", "a").Replace("Á", "A").Replace("é", "e").Replace("É", "E").Replace("Ú", "U").ToUpper());

                }
                else
                {
                    if (d.AlineacionDerechaAIzquierda)
                    {
                        d.Informacion = ValidaString(d.Informacion);

                        if (d.Informacion.Length < d.Longitud)
                        {

                            for (int i = d.Informacion.Length; i < d.Longitud; i++)
                            {
                                sb.Append(d.ValorDeRellene);
                            }
                        }
                        sb.Append(d.Informacion);
                    }
                    else
                    {
                        sb.Append(ValidaString(d.Informacion));
                        if (d.Informacion.Length < d.Longitud)
                        {

                            for (int i = d.Informacion.Length; i < d.Longitud; i++)
                            {
                                sb.Append(d.ValorDeRellene);
                            }


                        }
                    }

                }
            }
            return sb.ToString();

        }

        private String ValidaString(String Str)
        {
            return Str.Replace("ñ", "n").Replace("Ñ", "N").Replace("ó", "o").Replace("Ó", "O").Replace("í", "i").Replace("Í", "I").Replace("á", "a").Replace("Á", "A").Replace("é", "e").Replace("É", "E").Replace("ú", "u").Replace("Ú", "U").ToUpper().Replace("ü", "u").Replace("Ü", "U").ToUpper();
        }
    }
}
