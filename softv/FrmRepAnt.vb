Public Class FrmRepAnt

    
    Private Sub FrmRepAnt_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        eOpVentas = 76
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.RadioButton1.Checked = True Then eRepAnt = "L"
        If Me.RadioButton2.Checked = True Then eRepAnt = "R"

        If IsNumeric(Me.TextBox1.Text) = False Or IsNumeric(Me.TextBox2.Text) = False Then
            MsgBox("Meses de Antig�edad Inv�lidos.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If CInt(Me.TextBox1.Text) > (Me.TextBox2.Text) Then
            MsgBox("Meses de Antig�edad Inv�lidos.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If Me.CheckBox1.Checked = False And Me.CheckBox2.Checked = False Then
            MsgBox("Marca al menos un Periodo.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        eAntIni = CInt(Me.TextBox1.Text)
        eAntFin = CInt(Me.TextBox2.Text)
        LocPeriodo1 = Me.CheckBox1.Checked
        LocPeriodo2 = Me.CheckBox2.Checked
        bndReportA = True
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        bndReportA = False
        Me.Close()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox2, Asc(LCase(e.KeyChar)), "N")))
    End Sub
End Class