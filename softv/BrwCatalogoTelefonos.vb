Imports System.Data.SqlClient
Public Class BrwCatalogoTelefonos
    Public KeyAscii As Short
    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub BrwCatalogoTelefonos_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If LocBndTel = True Then
            LocBndTel = False
            Busca(0)
        End If
    End Sub

    Private Sub BrwCatalogoTelefonos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Busca(0)
    End Sub
    Private Sub Busca(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)

        Select Case op
            Case 0
                CON.Open()
                Me.Busca_Cat_Num_TelefonoTableAdapter.Connection = CON
                Me.Busca_Cat_Num_TelefonoTableAdapter.Fill(Me.Procedimientosarnoldo4.Busca_Cat_Num_Telefono, op, 0, "")
                CON.Close()
            Case 1
                CON.Open()
                Me.Busca_Cat_Num_TelefonoTableAdapter.Connection = CON
                Me.Busca_Cat_Num_TelefonoTableAdapter.Fill(Me.Procedimientosarnoldo4.Busca_Cat_Num_Telefono, op, CLng(Me.TextBox1.Text), "")
                CON.Close()
            Case 2
                CON.Open()
                Me.Busca_Cat_Num_TelefonoTableAdapter.Connection = CON
                Me.Busca_Cat_Num_TelefonoTableAdapter.Fill(Me.Procedimientosarnoldo4.Busca_Cat_Num_Telefono, op, 0, Me.TextBox2.Text)
                CON.Close()
        End Select
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "N")))
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        If Len(Me.TextBox1.Text) > 0 Then
            Busca(1)
        Else
            Busca(0)
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Len(Me.TextBox2.Text) > 0 Then
            Busca(2)
        Else
            Busca(0)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        LocOpTel = "C"
        If IsNumeric(Me.Clv_tipo_paquete_AdiocionalLabel1.Text) = True Then
            LocClv_Tel = CLng(Me.Clv_tipo_paquete_AdiocionalLabel1.Text)
            FrmCatalogoNoTel.Show()
        Else
            MsgBox("No Se Puede Consultar Sin Datos", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        LocOpTel = "M"
        If IsNumeric(Me.Clv_tipo_paquete_AdiocionalLabel1.Text) = True Then
            LocClv_Tel = CLng(Me.Clv_tipo_paquete_AdiocionalLabel1.Text)
            FrmCatalogoNoTel.Show()
        Else
            MsgBox("No Se Puede Modificar Sin Datos", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        LocOpTel = "N"
        FrmCatalogoNoTel.Show()
    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        LocOpTel = "C"
        If IsNumeric(Me.Clv_tipo_paquete_AdiocionalLabel1.Text) = True Then
            LocClv_Tel = CLng(Me.Clv_tipo_paquete_AdiocionalLabel1.Text)
            FrmCatalogoNoTel.Show()
        Else
            MsgBox("No Se Puede Consultar Sin Datos", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub
End Class