<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGenerales_Sistema
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim PortLabel As System.Windows.Forms.Label
        Dim HostLabel As System.Windows.Forms.Label
        Dim PasswordLabel As System.Windows.Forms.Label
        Dim CuentaLabel As System.Windows.Forms.Label
        Dim MesesLabel As System.Windows.Forms.Label
        Dim Label20 As System.Windows.Forms.Label
        Dim Label19 As System.Windows.Forms.Label
        Dim Label22 As System.Windows.Forms.Label
        Dim Label16 As System.Windows.Forms.Label
        Dim Label21 As System.Windows.Forms.Label
        Dim Label15 As System.Windows.Forms.Label
        Dim ImportePorExtrasLabel As System.Windows.Forms.Label
        Dim CargaDeTrabajoLabel As System.Windows.Forms.Label
        Dim Carga_trab_insLabel As System.Windows.Forms.Label
        Dim NumeroExtLabel As System.Windows.Forms.Label
        Dim CostoExtLabel As System.Windows.Forms.Label
        Dim ImpresoraOrdenesLabel As System.Windows.Forms.Label
        Dim Puntos1Label As System.Windows.Forms.Label
        Dim Label24 As System.Windows.Forms.Label
        Dim Label25 As System.Windows.Forms.Label
        Dim Label28 As System.Windows.Forms.Label
        Dim Label27 As System.Windows.Forms.Label
        Dim Label26 As System.Windows.Forms.Label
        Dim Label31 As System.Windows.Forms.Label
        Dim Label30 As System.Windows.Forms.Label
        Dim Label29 As System.Windows.Forms.Label
        Dim Label34 As System.Windows.Forms.Label
        Dim Label33 As System.Windows.Forms.Label
        Dim Label32 As System.Windows.Forms.Label
        Dim Si_se_generaLabel As System.Windows.Forms.Label
        Dim MensajeLabel As System.Windows.Forms.Label
        Dim ActivoLabel As System.Windows.Forms.Label
        Dim Label38 As System.Windows.Forms.Label
        Dim Label41 As System.Windows.Forms.Label
        Dim Label39 As System.Windows.Forms.Label
        Dim CablemodemsLabel As System.Windows.Forms.Label
        Dim PregcablLabel As System.Windows.Forms.Label
        Dim DecodersLabel As System.Windows.Forms.Label
        Dim PregdecoLabel As System.Windows.Forms.Label
        Dim TELefonosLabel As System.Windows.Forms.Label
        Dim RfcLabel As System.Windows.Forms.Label
        Dim CiudadLabel As System.Windows.Forms.Label
        Dim ColoniaLabel As System.Windows.Forms.Label
        Dim DireccionLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim Id_sucursalLabel As System.Windows.Forms.Label
        Dim Fecha_InicioLabel As System.Windows.Forms.Label
        Dim Numero_InicioLabel As System.Windows.Forms.Label
        Dim FormatoLabel As System.Windows.Forms.Label
        Dim RutaLabel As System.Windows.Forms.Label
        Dim Numero_LocalLabel As System.Windows.Forms.Label
        Dim Pais_LocalLabel As System.Windows.Forms.Label
        Dim Clave044Label As System.Windows.Forms.Label
        Dim Clave045Label As System.Windows.Forms.Label
        Dim Clave01800Label As System.Windows.Forms.Label
        Dim Clave01900Label As System.Windows.Forms.Label
        Dim Label46 As System.Windows.Forms.Label
        Dim Nom_ArchivoLabel As System.Windows.Forms.Label
        Dim Fecha_FacturasLabel As System.Windows.Forms.Label
        Dim Periodo_inicialLabel As System.Windows.Forms.Label
        Dim ModuloLabel As System.Windows.Forms.Label
        Dim ConstanteLabel As System.Windows.Forms.Label
        Dim ReferenciaLabel As System.Windows.Forms.Label
        Dim Label48 As System.Windows.Forms.Label
        Dim Label50 As System.Windows.Forms.Label
        Dim Label52 As System.Windows.Forms.Label
        Dim Label53 As System.Windows.Forms.Label
        Dim LblCdClvAdic As System.Windows.Forms.Label
        Dim LblEdoClvAdic As System.Windows.Forms.Label
        Dim lblPPE2 As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim Label61 As System.Windows.Forms.Label
        Dim Label63 As System.Windows.Forms.Label
        Dim Label69 As System.Windows.Forms.Label
        Dim Label72 As System.Windows.Forms.Label
        Dim Label125 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ConsultaCpGeneBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.Consulta_ImpresorasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraTipServEricBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.ConMesesCobroAdeudoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConGeneralCorreoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_ImpresorasTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Consulta_ImpresorasTableAdapter()
        Me.Inserta_Mod_Cp_GenBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Mod_Cp_GenTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Inserta_Mod_Cp_GenTableAdapter()
        Me.Consulta_Cp_GeneTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Consulta_Cp_GeneTableAdapter()
        Me.ConMesesCobroAdeudoTableAdapter = New sofTV.DataSetEricTableAdapters.ConMesesCobroAdeudoTableAdapter()
        Me.MuestraTipServEricTableAdapter = New sofTV.DataSetEricTableAdapters.MuestraTipServEricTableAdapter()
        Me.ConGeneralCorreoTableAdapter = New sofTV.DataSetEricTableAdapters.ConGeneralCorreoTableAdapter()
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.Consulta_Generales_Filtros_OrdenesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_Generales_Filtros_OrdenesTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Consulta_Generales_Filtros_OrdenesTableAdapter()
        Me.Inserta_Generales_Filtros_OrdenesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Generales_Filtros_OrdenesTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Generales_Filtros_OrdenesTableAdapter()
        Me.COnsultaGralfacturaglobalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.COnsulta_Gral_factura_globalTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.COnsulta_Gral_factura_globalTableAdapter()
        Me.Inserta_Gral_factura_globalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Gral_factura_globalTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Gral_factura_globalTableAdapter()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.CuentaTextBox = New System.Windows.Forms.TextBox()
        Me.PasswordTextBox = New System.Windows.Forms.TextBox()
        Me.HostTextBox = New System.Windows.Forms.TextBox()
        Me.PortTextBox = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.DataGridViewCobro = New System.Windows.Forms.DataGridView()
        Me.Clv_TipoCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Meses = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Habilitado = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ComboBoxCobro = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Clv_TipSerTextBox = New System.Windows.Forms.TextBox()
        Me.ConceptoComboBox = New System.Windows.Forms.ComboBox()
        Me.MesesTextBox = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label124 = New System.Windows.Forms.Label()
        Me.tbPuntosAnti = New System.Windows.Forms.TextBox()
        Me.CONSULTAGENERALESDESCBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Procedimientosarnoldo4 = New sofTV.Procedimientosarnoldo4()
        Me.rb30 = New System.Windows.Forms.RadioButton()
        Me.rb15 = New System.Windows.Forms.RadioButton()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.MUESTRAPERIODOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtDiaCorte = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBox15 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.TextBox13 = New System.Windows.Forms.TextBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.ImpresoraFiscalTextBox = New System.Windows.Forms.TextBox()
        Me.ImpresoraTarjetasTextBox = New System.Windows.Forms.TextBox()
        Me.ImpresoraTicketsTextBox = New System.Windows.Forms.TextBox()
        Me.ImpresoraContratosTextBox = New System.Windows.Forms.TextBox()
        Me.PanelBasico = New System.Windows.Forms.Panel()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Puntos1TextBox = New System.Windows.Forms.TextBox()
        Me.ConGeneralAntiguedadBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NumericUpDown2 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown5 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown6 = New System.Windows.Forms.NumericUpDown()
        Me.Puntos2TextBox = New System.Windows.Forms.TextBox()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.Puntos5TextBox = New System.Windows.Forms.TextBox()
        Me.NumericUpDown4 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown8 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown3 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown7 = New System.Windows.Forms.NumericUpDown()
        Me.Puntos10TextBox = New System.Windows.Forms.TextBox()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.ImportePorExtrasTextBox = New System.Windows.Forms.TextBox()
        Me.ConGeneralBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Carga_trab_insTextBox = New System.Windows.Forms.TextBox()
        Me.CargaDeTrabajoTextBox = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.CostoExtTextBox = New System.Windows.Forms.TextBox()
        Me.NumeroExtTextBox = New System.Windows.Forms.TextBox()
        Me.ImpresoraOrdenesTextBox = New System.Windows.Forms.TextBox()
        Me.Consulta_cobrodepositoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.Si_se_generaCheckBox = New System.Windows.Forms.CheckBox()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.ConGeneralAlertaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia2 = New sofTV.DataSetLidia2()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.ReferenciaTextBox1 = New System.Windows.Forms.TextBox()
        Me.CONSULTA_General_HsbcBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetyahve = New sofTV.DataSetyahve()
        Me.ModuloTextBox1 = New System.Windows.Forms.TextBox()
        Me.Button31 = New System.Windows.Forms.Button()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.ReferenciaTextBox = New System.Windows.Forms.TextBox()
        Me.CONSULTA_General_SantanderBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConstanteTextBox = New System.Windows.Forms.TextBox()
        Me.ModuloTextBox = New System.Windows.Forms.TextBox()
        Me.Button30 = New System.Windows.Forms.Button()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.PanelDatosXml = New System.Windows.Forms.Panel()
        Me.TxtCdClvAdic = New System.Windows.Forms.TextBox()
        Me.TxtEdoClvAdic = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.TextBoxfolio = New System.Windows.Forms.TextBox()
        Me.TextBoxserie = New System.Windows.Forms.TextBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Nom_ArchivoTextBox = New System.Windows.Forms.TextBox()
        Me.CONSULTA_General_XmlBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Fecha_InicioDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.Fecha_FacturasDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.Periodo_inicialTextBox = New System.Windows.Forms.TextBox()
        Me.Periodo_finalTextBox = New System.Windows.Forms.TextBox()
        Me.Numero_InicioTextBox = New System.Windows.Forms.TextBox()
        Me.FormatoTextBox = New System.Windows.Forms.TextBox()
        Me.RutaTextBox = New System.Windows.Forms.TextBox()
        Me.Numero_LocalTextBox = New System.Windows.Forms.TextBox()
        Me.Pais_LocalTextBox = New System.Windows.Forms.TextBox()
        Me.Clave044TextBox = New System.Windows.Forms.TextBox()
        Me.Clave045TextBox = New System.Windows.Forms.TextBox()
        Me.Clave01800TextBox = New System.Windows.Forms.TextBox()
        Me.Clave01900TextBox = New System.Windows.Forms.TextBox()
        Me.Button28 = New System.Windows.Forms.Button()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.PanelDatos = New System.Windows.Forms.Panel()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.TextBoxNumeroExt = New System.Windows.Forms.TextBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.TextBoxNumeroInt = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.CiudadTextBox = New System.Windows.Forms.TextBox()
        Me.ColoniaTextBox = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Id_sucursalTextBox = New System.Windows.Forms.TextBox()
        Me.RfcTextBox = New System.Windows.Forms.TextBox()
        Me.DireccionTextBox = New System.Windows.Forms.TextBox()
        Me.NombreTextBox = New System.Windows.Forms.TextBox()
        Me.TELefonosTextBox = New System.Windows.Forms.TextBox()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.Button21 = New System.Windows.Forms.Button()
        Me.Button20 = New System.Windows.Forms.Button()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.MensajeTextBox = New System.Windows.Forms.TextBox()
        Me.ConGeneralMsjTicketsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ActivoCheckBox = New System.Windows.Forms.CheckBox()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.CablemodemsCheckBox = New System.Windows.Forms.CheckBox()
        Me.Consulta_cobrodepositobuenoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PregcablTextBox = New System.Windows.Forms.TextBox()
        Me.DecodersCheckBox = New System.Windows.Forms.CheckBox()
        Me.PregdecoTextBox = New System.Windows.Forms.TextBox()
        Me.Button23 = New System.Windows.Forms.Button()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Button26 = New System.Windows.Forms.Button()
        Me.Button25 = New System.Windows.Forms.Button()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.NumericUpDown9 = New System.Windows.Forms.NumericUpDown()
        Me.Consulta_Rel_PaquetesDigTelDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Consulta_Rel_PaquetesDigTelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConsecutivoTextBox = New System.Windows.Forms.TextBox()
        Me.ConGeneralAlertaTableAdapter = New sofTV.DataSetLidia2TableAdapters.ConGeneralAlertaTableAdapter()
        Me.NueGeneralAlertaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueGeneralAlertaTableAdapter = New sofTV.DataSetLidia2TableAdapters.NueGeneralAlertaTableAdapter()
        Me.Dame_Direcciones_IpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_Direcciones_IpTableAdapter = New sofTV.DataSetLidia2TableAdapters.Dame_Direcciones_IpTableAdapter()
        Me.Consulta_cobrodepositoTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Consulta_cobrodepositoTableAdapter()
        Me.Consulta_cobrodepositobuenoTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Consulta_cobrodepositobuenoTableAdapter()
        Me.Consulta_Rel_PaquetesDigTelTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Consulta_Rel_PaquetesDigTelTableAdapter()
        Me.CONSULTA_General_XmlTableAdapter = New sofTV.DataSetyahveTableAdapters.CONSULTA_General_XmlTableAdapter()
        Me.CONSULTA_General_SantanderTableAdapter = New sofTV.DataSetyahveTableAdapters.CONSULTA_General_SantanderTableAdapter()
        Me.CONSULTA_General_HsbcTableAdapter = New sofTV.DataSetyahveTableAdapters.CONSULTA_General_HsbcTableAdapter()
        Me.Muestra_ciudadTableAdapter1 = New sofTV.Procedimientosarnoldo4TableAdapters.Muestra_ciudadTableAdapter()
        Me.TabPage14 = New System.Windows.Forms.TabPage()
        Me.TabPage13 = New System.Windows.Forms.TabPage()
        Me.TabPage12 = New System.Windows.Forms.TabPage()
        Me.TabPage11 = New System.Windows.Forms.TabPage()
        Me.TabPage10 = New System.Windows.Forms.TabPage()
        Me.TabPage9 = New System.Windows.Forms.TabPage()
        Me.TabPage8 = New System.Windows.Forms.TabPage()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage15 = New System.Windows.Forms.TabPage()
        Me.PnQuejas = New System.Windows.Forms.Panel()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.TreeViewOrdenes = New System.Windows.Forms.TreeView()
        Me.LblNotas = New System.Windows.Forms.Label()
        Me.LblFamilia = New System.Windows.Forms.Label()
        Me.LblTecnico = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.TreViewTecnicos = New System.Windows.Forms.TreeView()
        Me.CmbTecnicos = New System.Windows.Forms.ComboBox()
        Me.MuestraTecnicosByFamiliBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CmBDepto = New System.Windows.Forms.ComboBox()
        Me.MuestratecnicosDepartamentosAlmacenBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TabPage16 = New System.Windows.Forms.TabPage()
        Me.LabelNota = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Clv_Grupo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Grupo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Porcentaje = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Activo = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.TabPuntosPPE = New System.Windows.Forms.TabPage()
        Me.txtPtosMin = New System.Windows.Forms.TextBox()
        Me.btnGuardaPPE = New System.Windows.Forms.Button()
        Me.lblPPE1 = New System.Windows.Forms.Label()
        Me.TabPage17 = New System.Windows.Forms.TabPage()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.DDescoSusp = New System.Windows.Forms.TextBox()
        Me.DContrata = New System.Windows.Forms.TextBox()
        Me.TabPage18 = New System.Windows.Forms.TabPage()
        Me.TextIEPS = New System.Windows.Forms.TextBox()
        Me.TextIva = New System.Windows.Forms.TextBox()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.LblPorIeps = New System.Windows.Forms.Label()
        Me.NumericUpDownIeps = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDownIva = New System.Windows.Forms.NumericUpDown()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.ChkCalculo1 = New System.Windows.Forms.CheckBox()
        Me.LabelIEPS = New System.Windows.Forms.Label()
        Me.CtaIepsText = New System.Windows.Forms.TextBox()
        Me.LabelCtaIEPS = New System.Windows.Forms.Label()
        Me.CheckIEPS = New System.Windows.Forms.CheckBox()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.tpRecontratacion = New System.Windows.Forms.TabPage()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.btnAceptarRecon = New System.Windows.Forms.Button()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.cbAplica = New System.Windows.Forms.CheckBox()
        Me.txtMeses = New System.Windows.Forms.TextBox()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.tpDecodificadores = New System.Windows.Forms.TabPage()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.txtPrecioPrincipal = New System.Windows.Forms.TextBox()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.txtPrecioAdicional = New System.Windows.Forms.TextBox()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.txtEliminarDec = New System.Windows.Forms.Button()
        Me.txtAgregarDec = New System.Windows.Forms.Button()
        Me.dataGridDec = New System.Windows.Forms.DataGridView()
        Me.IdDec = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescripcionDec = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_ServicioDec = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ServicioDec = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrecioPrincipalDec = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrecioAdicionalDec = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtDescripcionDec = New System.Windows.Forms.TextBox()
        Me.comboServicioDec = New System.Windows.Forms.ComboBox()
        Me.tpEstadosCuenta = New System.Windows.Forms.TabPage()
        Me.gbMensaje = New System.Windows.Forms.GroupBox()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.cbDeco = New System.Windows.Forms.CheckBox()
        Me.cbCorreo = New System.Windows.Forms.CheckBox()
        Me.dtHora = New System.Windows.Forms.DateTimePicker()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.tbMensajeDeco = New System.Windows.Forms.TextBox()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.tbMensaje = New System.Windows.Forms.TextBox()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.tbAsunto = New System.Windows.Forms.TextBox()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.txtDiaEdoCuenta = New System.Windows.Forms.TextBox()
        Me.cbPeriodo = New System.Windows.Forms.ComboBox()
        Me.tpDatosFiscales = New System.Windows.Forms.TabPage()
        Me.cbHabilitado = New System.Windows.Forms.CheckBox()
        Me.tbRFCExtranjeros = New System.Windows.Forms.TextBox()
        Me.tbRFCPublicoGeneral = New System.Windows.Forms.TextBox()
        Me.tbCertificado = New System.Windows.Forms.TextBox()
        Me.tbAnoAprobacion = New System.Windows.Forms.TextBox()
        Me.Label107 = New System.Windows.Forms.Label()
        Me.Label106 = New System.Windows.Forms.Label()
        Me.Label105 = New System.Windows.Forms.Label()
        Me.Label104 = New System.Windows.Forms.Label()
        Me.tbNoAprobacion = New System.Windows.Forms.TextBox()
        Me.Label103 = New System.Windows.Forms.Label()
        Me.Label102 = New System.Windows.Forms.Label()
        Me.tbTelefono = New System.Windows.Forms.TextBox()
        Me.Label2000 = New System.Windows.Forms.Label()
        Me.tbCodigoPostal = New System.Windows.Forms.TextBox()
        Me.Label101 = New System.Windows.Forms.Label()
        Me.tbPais = New System.Windows.Forms.TextBox()
        Me.Label100 = New System.Windows.Forms.Label()
        Me.tbEstado = New System.Windows.Forms.TextBox()
        Me.Label99 = New System.Windows.Forms.Label()
        Me.tbMunicipio = New System.Windows.Forms.TextBox()
        Me.Label98 = New System.Windows.Forms.Label()
        Me.tbReferencia = New System.Windows.Forms.TextBox()
        Me.Label97 = New System.Windows.Forms.Label()
        Me.tbrfc = New System.Windows.Forms.TextBox()
        Me.Label96 = New System.Windows.Forms.Label()
        Me.tbNombre = New System.Windows.Forms.TextBox()
        Me.Label95 = New System.Windows.Forms.Label()
        Me.tbCalle = New System.Windows.Forms.TextBox()
        Me.Label94 = New System.Windows.Forms.Label()
        Me.tbNoExt = New System.Windows.Forms.TextBox()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.tbNoInt = New System.Windows.Forms.TextBox()
        Me.Label92 = New System.Windows.Forms.Label()
        Me.tbColonia = New System.Windows.Forms.TextBox()
        Me.Label91 = New System.Windows.Forms.Label()
        Me.tbLocalidad = New System.Windows.Forms.TextBox()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.Button17 = New System.Windows.Forms.Button()
        Me.tbEmail = New System.Windows.Forms.TextBox()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.tbPuntos = New System.Windows.Forms.TabPage()
        Me.Label123 = New System.Windows.Forms.Label()
        Me.tcPuntos = New System.Windows.Forms.TabControl()
        Me.tbPuntosRangos = New System.Windows.Forms.TabPage()
        Me.bnEliminarRango = New System.Windows.Forms.Button()
        Me.bnAgregarRango = New System.Windows.Forms.Button()
        Me.Label109 = New System.Windows.Forms.Label()
        Me.tbRangoFin = New System.Windows.Forms.TextBox()
        Me.dgRangos = New System.Windows.Forms.DataGridView()
        Me.IdRango = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Rango = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label108 = New System.Windows.Forms.Label()
        Me.tbRangoIni = New System.Windows.Forms.TextBox()
        Me.tbPuntosServicios = New System.Windows.Forms.TabPage()
        Me.Label110 = New System.Windows.Forms.Label()
        Me.Label113 = New System.Windows.Forms.Label()
        Me.Label111 = New System.Windows.Forms.Label()
        Me.cbTipSerPuntosAnti = New System.Windows.Forms.ComboBox()
        Me.cbServicioPuntosAnti = New System.Windows.Forms.ComboBox()
        Me.bnEliminarPuntosServicio = New System.Windows.Forms.Button()
        Me.bnAgregarPuntosServicio = New System.Windows.Forms.Button()
        Me.Label112 = New System.Windows.Forms.Label()
        Me.dgPuntosServicios = New System.Windows.Forms.DataGridView()
        Me.IdPuntos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Puntos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tboxPuntosAnti = New System.Windows.Forms.TextBox()
        Me.cbRangosPuntosAnti = New System.Windows.Forms.ComboBox()
        Me.tbPuntosServiciosAdic = New System.Windows.Forms.TabPage()
        Me.Label117 = New System.Windows.Forms.Label()
        Me.Label118 = New System.Windows.Forms.Label()
        Me.tbPuntosAntiServicios = New System.Windows.Forms.TextBox()
        Me.Label114 = New System.Windows.Forms.Label()
        Me.Label115 = New System.Windows.Forms.Label()
        Me.cbTipSerPuntosAntiServicios = New System.Windows.Forms.ComboBox()
        Me.cbServicioPuntosAntiServicios = New System.Windows.Forms.ComboBox()
        Me.bnEliminarPuntosAntiServicios = New System.Windows.Forms.Button()
        Me.bnAgregarPuntosAntiServicios = New System.Windows.Forms.Button()
        Me.Label116 = New System.Windows.Forms.Label()
        Me.dgPagosAntiServAdic = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PagosPuntuales = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PuntosServicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tbPuntosAntiServiciosPagos = New System.Windows.Forms.TextBox()
        Me.cbRangosPuntosAntiServicios = New System.Windows.Forms.ComboBox()
        Me.tbPuntosCajas = New System.Windows.Forms.TabPage()
        Me.tbPuntosAntiArticulos4ta = New System.Windows.Forms.TextBox()
        Me.Label128 = New System.Windows.Forms.Label()
        Me.tbPuntosAntiArticulos2da3er = New System.Windows.Forms.TextBox()
        Me.Label127 = New System.Windows.Forms.Label()
        Me.bnGuardarNumero = New System.Windows.Forms.Button()
        Me.Label126 = New System.Windows.Forms.Label()
        Me.tbCantidad = New System.Windows.Forms.TextBox()
        Me.bnEliminarPuntosAntiArticulos = New System.Windows.Forms.Button()
        Me.bnAgregarPuntosAntiArticulos = New System.Windows.Forms.Button()
        Me.Label121 = New System.Windows.Forms.Label()
        Me.cbArticuloPuntosAntiArticulos = New System.Windows.Forms.ComboBox()
        Me.Label122 = New System.Windows.Forms.Label()
        Me.tbPuntosAntiArticulos = New System.Windows.Forms.TextBox()
        Me.bnAbajoPuntosAntiArticulos = New System.Windows.Forms.Button()
        Me.bnArribaPuntosAntiArticulos = New System.Windows.Forms.Button()
        Me.Label120 = New System.Windows.Forms.Label()
        Me.dgPuntosAntiArticulos = New System.Windows.Forms.DataGridView()
        Me.IdPuntosPuntosAntiArticulos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JerarquiaPuntosAntiArticulos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescripcionPuntosAntiArticulos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PuntosPuntosAntiArticulos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PuntosSegundaYTercerCaja = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PuntosCuartaCaja = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label119 = New System.Windows.Forms.Label()
        Me.cbRangoPuntosAntiArticulos = New System.Windows.Forms.ComboBox()
        Me.tbPromocionAntiguedad = New System.Windows.Forms.TabPage()
        Me.bnGuardarTipoUsuario = New System.Windows.Forms.Button()
        Me.Label131 = New System.Windows.Forms.Label()
        Me.dgvTipoUsuario = New System.Windows.Forms.DataGridView()
        Me.Clv_TipoUsuario = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescripcionTipoUsuario = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Acceso = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.bnGuardarMeses = New System.Windows.Forms.Button()
        Me.tbMesesAntiguedadPromocion = New System.Windows.Forms.TextBox()
        Me.Label130 = New System.Windows.Forms.Label()
        Me.Label129 = New System.Windows.Forms.Label()
        Me.tpPromPriMens = New System.Windows.Forms.TabPage()
        Me.btnGuardarPM = New System.Windows.Forms.Button()
        Me.btnAgregarPM = New System.Windows.Forms.Button()
        Me.Label138 = New System.Windows.Forms.Label()
        Me.gbxMens0 = New System.Windows.Forms.GroupBox()
        Me.Label135 = New System.Windows.Forms.Label()
        Me.txtDescuentoC0 = New System.Windows.Forms.TextBox()
        Me.Label136 = New System.Windows.Forms.Label()
        Me.txtDescuento0 = New System.Windows.Forms.TextBox()
        Me.Label137 = New System.Windows.Forms.Label()
        Me.txtImporte0 = New System.Windows.Forms.TextBox()
        Me.gbxMensN = New System.Windows.Forms.GroupBox()
        Me.Label134 = New System.Windows.Forms.Label()
        Me.txtDescuentoC = New System.Windows.Forms.TextBox()
        Me.Label133 = New System.Windows.Forms.Label()
        Me.txtDescuento = New System.Windows.Forms.TextBox()
        Me.Label132 = New System.Windows.Forms.Label()
        Me.txtImporte = New System.Windows.Forms.TextBox()
        Me.lblServicio = New System.Windows.Forms.Label()
        Me.cbxServicio = New System.Windows.Forms.ComboBox()
        Me.lblTipServ = New System.Windows.Forms.Label()
        Me.cbxTipoServ = New System.Windows.Forms.ComboBox()
        Me.ConsultaRelTecnicosQuejasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConsultaRelQuejasTecFamiliaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ConGeneralTableAdapter1 = New sofTV.Procedimientosarnoldo4TableAdapters.ConGeneralTableAdapter()
        Me.ConGeneralAntiguedadTableAdapter1 = New sofTV.Procedimientosarnoldo4TableAdapters.ConGeneralAntiguedadTableAdapter()
        Me.MUESTRAPERIODOSTableAdapter1 = New sofTV.Procedimientosarnoldo4TableAdapters.MUESTRAPERIODOSTableAdapter()
        Me.CONSULTAGENERALESDESCTableAdapter1 = New sofTV.Procedimientosarnoldo4TableAdapters.CONSULTAGENERALESDESCTableAdapter()
        Me.ConsultaRel_Quejas_Tec_FamiliaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConsultaRel_Quejas_Tec_FamiliaTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.ConsultaRel_Quejas_Tec_FamiliaTableAdapter()
        Me.ConsultaRel_Tecnicos_QuejasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConsultaRel_Tecnicos_QuejasTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.ConsultaRel_Tecnicos_QuejasTableAdapter()
        Me.Muestra_tecnicosDepartamentos_AlmacenTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Muestra_tecnicosDepartamentos_AlmacenTableAdapter()
        Me.Muestra_TecnicosByFamiliTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Muestra_TecnicosByFamiliTableAdapter()
        Me.NueGeneralMsjTicketsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MODIFCAGENERALESDESCBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.NueGeneralBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.NUEGeneralAntiguedadBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter5 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter6 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        PortLabel = New System.Windows.Forms.Label()
        HostLabel = New System.Windows.Forms.Label()
        PasswordLabel = New System.Windows.Forms.Label()
        CuentaLabel = New System.Windows.Forms.Label()
        MesesLabel = New System.Windows.Forms.Label()
        Label20 = New System.Windows.Forms.Label()
        Label19 = New System.Windows.Forms.Label()
        Label22 = New System.Windows.Forms.Label()
        Label16 = New System.Windows.Forms.Label()
        Label21 = New System.Windows.Forms.Label()
        Label15 = New System.Windows.Forms.Label()
        ImportePorExtrasLabel = New System.Windows.Forms.Label()
        CargaDeTrabajoLabel = New System.Windows.Forms.Label()
        Carga_trab_insLabel = New System.Windows.Forms.Label()
        NumeroExtLabel = New System.Windows.Forms.Label()
        CostoExtLabel = New System.Windows.Forms.Label()
        ImpresoraOrdenesLabel = New System.Windows.Forms.Label()
        Puntos1Label = New System.Windows.Forms.Label()
        Label24 = New System.Windows.Forms.Label()
        Label25 = New System.Windows.Forms.Label()
        Label28 = New System.Windows.Forms.Label()
        Label27 = New System.Windows.Forms.Label()
        Label26 = New System.Windows.Forms.Label()
        Label31 = New System.Windows.Forms.Label()
        Label30 = New System.Windows.Forms.Label()
        Label29 = New System.Windows.Forms.Label()
        Label34 = New System.Windows.Forms.Label()
        Label33 = New System.Windows.Forms.Label()
        Label32 = New System.Windows.Forms.Label()
        Si_se_generaLabel = New System.Windows.Forms.Label()
        MensajeLabel = New System.Windows.Forms.Label()
        ActivoLabel = New System.Windows.Forms.Label()
        Label38 = New System.Windows.Forms.Label()
        Label41 = New System.Windows.Forms.Label()
        Label39 = New System.Windows.Forms.Label()
        CablemodemsLabel = New System.Windows.Forms.Label()
        PregcablLabel = New System.Windows.Forms.Label()
        DecodersLabel = New System.Windows.Forms.Label()
        PregdecoLabel = New System.Windows.Forms.Label()
        TELefonosLabel = New System.Windows.Forms.Label()
        RfcLabel = New System.Windows.Forms.Label()
        CiudadLabel = New System.Windows.Forms.Label()
        ColoniaLabel = New System.Windows.Forms.Label()
        DireccionLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        Id_sucursalLabel = New System.Windows.Forms.Label()
        Fecha_InicioLabel = New System.Windows.Forms.Label()
        Numero_InicioLabel = New System.Windows.Forms.Label()
        FormatoLabel = New System.Windows.Forms.Label()
        RutaLabel = New System.Windows.Forms.Label()
        Numero_LocalLabel = New System.Windows.Forms.Label()
        Pais_LocalLabel = New System.Windows.Forms.Label()
        Clave044Label = New System.Windows.Forms.Label()
        Clave045Label = New System.Windows.Forms.Label()
        Clave01800Label = New System.Windows.Forms.Label()
        Clave01900Label = New System.Windows.Forms.Label()
        Label46 = New System.Windows.Forms.Label()
        Nom_ArchivoLabel = New System.Windows.Forms.Label()
        Fecha_FacturasLabel = New System.Windows.Forms.Label()
        Periodo_inicialLabel = New System.Windows.Forms.Label()
        ModuloLabel = New System.Windows.Forms.Label()
        ConstanteLabel = New System.Windows.Forms.Label()
        ReferenciaLabel = New System.Windows.Forms.Label()
        Label48 = New System.Windows.Forms.Label()
        Label50 = New System.Windows.Forms.Label()
        Label52 = New System.Windows.Forms.Label()
        Label53 = New System.Windows.Forms.Label()
        LblCdClvAdic = New System.Windows.Forms.Label()
        LblEdoClvAdic = New System.Windows.Forms.Label()
        lblPPE2 = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        Label61 = New System.Windows.Forms.Label()
        Label63 = New System.Windows.Forms.Label()
        Label69 = New System.Windows.Forms.Label()
        Label72 = New System.Windows.Forms.Label()
        Label125 = New System.Windows.Forms.Label()
        CType(Me.ConsultaCpGeneBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_ImpresorasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConMesesCobroAdeudoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConGeneralCorreoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Mod_Cp_GenBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Generales_Filtros_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Generales_Filtros_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.COnsultaGralfacturaglobalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Gral_factura_globalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.DataGridViewCobro, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.CONSULTAGENERALESDESCBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAPERIODOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.PanelBasico.SuspendLayout()
        Me.Panel13.SuspendLayout()
        CType(Me.ConGeneralAntiguedadBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConGeneralBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.Consulta_cobrodepositoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        CType(Me.ConGeneralAlertaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel12.SuspendLayout()
        CType(Me.CONSULTA_General_HsbcBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetyahve, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel11.SuspendLayout()
        CType(Me.CONSULTA_General_SantanderBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelDatosXml.SuspendLayout()
        CType(Me.CONSULTA_General_XmlBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelDatos.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel7.SuspendLayout()
        CType(Me.ConGeneralMsjTicketsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel9.SuspendLayout()
        CType(Me.Consulta_cobrodepositobuenoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel10.SuspendLayout()
        CType(Me.NumericUpDown9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Rel_PaquetesDigTelDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Rel_PaquetesDigTelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueGeneralAlertaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_Direcciones_IpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage14.SuspendLayout()
        Me.TabPage13.SuspendLayout()
        Me.TabPage12.SuspendLayout()
        Me.TabPage11.SuspendLayout()
        Me.TabPage10.SuspendLayout()
        Me.TabPage9.SuspendLayout()
        Me.TabPage8.SuspendLayout()
        Me.TabPage7.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage15.SuspendLayout()
        Me.PnQuejas.SuspendLayout()
        CType(Me.MuestraTecnicosByFamiliBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestratecnicosDepartamentosAlmacenBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage16.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPuntosPPE.SuspendLayout()
        Me.TabPage17.SuspendLayout()
        Me.TabPage18.SuspendLayout()
        CType(Me.NumericUpDownIeps, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDownIva, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpRecontratacion.SuspendLayout()
        Me.tpDecodificadores.SuspendLayout()
        CType(Me.dataGridDec, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpEstadosCuenta.SuspendLayout()
        Me.gbMensaje.SuspendLayout()
        Me.tpDatosFiscales.SuspendLayout()
        Me.tbPuntos.SuspendLayout()
        Me.tcPuntos.SuspendLayout()
        Me.tbPuntosRangos.SuspendLayout()
        CType(Me.dgRangos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbPuntosServicios.SuspendLayout()
        CType(Me.dgPuntosServicios, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbPuntosServiciosAdic.SuspendLayout()
        CType(Me.dgPagosAntiServAdic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbPuntosCajas.SuspendLayout()
        CType(Me.dgPuntosAntiArticulos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbPromocionAntiguedad.SuspendLayout()
        CType(Me.dgvTipoUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpPromPriMens.SuspendLayout()
        Me.gbxMens0.SuspendLayout()
        Me.gbxMensN.SuspendLayout()
        CType(Me.ConsultaRelTecnicosQuejasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsultaRelQuejasTecFamiliaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsultaRel_Quejas_Tec_FamiliaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsultaRel_Tecnicos_QuejasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueGeneralMsjTicketsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MODIFCAGENERALESDESCBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NueGeneralBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUEGeneralAntiguedadBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PortLabel
        '
        PortLabel.AutoSize = True
        PortLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PortLabel.Location = New System.Drawing.Point(336, 210)
        PortLabel.Name = "PortLabel"
        PortLabel.Size = New System.Drawing.Size(37, 15)
        PortLabel.TabIndex = 35
        PortLabel.Text = "Port:"
        '
        'HostLabel
        '
        HostLabel.AutoSize = True
        HostLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        HostLabel.Location = New System.Drawing.Point(333, 183)
        HostLabel.Name = "HostLabel"
        HostLabel.Size = New System.Drawing.Size(40, 15)
        HostLabel.TabIndex = 33
        HostLabel.Text = "Host:"
        '
        'PasswordLabel
        '
        PasswordLabel.AutoSize = True
        PasswordLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PasswordLabel.Location = New System.Drawing.Point(300, 156)
        PasswordLabel.Name = "PasswordLabel"
        PasswordLabel.Size = New System.Drawing.Size(73, 15)
        PasswordLabel.TabIndex = 31
        PasswordLabel.Text = "Password:"
        '
        'CuentaLabel
        '
        CuentaLabel.AutoSize = True
        CuentaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CuentaLabel.Location = New System.Drawing.Point(317, 129)
        CuentaLabel.Name = "CuentaLabel"
        CuentaLabel.Size = New System.Drawing.Size(56, 15)
        CuentaLabel.TabIndex = 29
        CuentaLabel.Text = "Cuenta:"
        '
        'MesesLabel
        '
        MesesLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MesesLabel.Location = New System.Drawing.Point(728, 554)
        MesesLabel.Name = "MesesLabel"
        MesesLabel.Size = New System.Drawing.Size(21, 10)
        MesesLabel.TabIndex = 4
        MesesLabel.Text = "Número de Meses :"
        MesesLabel.Visible = False
        '
        'Label20
        '
        Label20.AutoSize = True
        Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label20.Location = New System.Drawing.Point(330, 151)
        Label20.Name = "Label20"
        Label20.Size = New System.Drawing.Size(98, 15)
        Label20.TabIndex = 20
        Label20.Text = "Desde el Dia :"
        '
        'Label19
        '
        Label19.AutoSize = True
        Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label19.Location = New System.Drawing.Point(334, 178)
        Label19.Name = "Label19"
        Label19.Size = New System.Drawing.Size(94, 15)
        Label19.TabIndex = 21
        Label19.Text = "Hasta el Dia :"
        '
        'Label22
        '
        Label22.AutoSize = True
        Label22.Location = New System.Drawing.Point(593, 254)
        Label22.Name = "Label22"
        Label22.Size = New System.Drawing.Size(52, 15)
        Label22.TabIndex = 29
        Label22.Text = "Precio:"
        Label22.Visible = False
        '
        'Label16
        '
        Label16.AutoSize = True
        Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label16.Location = New System.Drawing.Point(304, 301)
        Label16.Name = "Label16"
        Label16.Size = New System.Drawing.Size(124, 15)
        Label16.TabIndex = 20
        Label16.Text = "Por Pronto Pago : "
        '
        'Label21
        '
        Label21.AutoSize = True
        Label21.Location = New System.Drawing.Point(582, 292)
        Label21.Name = "Label21"
        Label21.Size = New System.Drawing.Size(132, 15)
        Label21.TabIndex = 30
        Label21.Text = "Precio Suspendido:"
        Label21.Visible = False
        '
        'Label15
        '
        Label15.AutoSize = True
        Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label15.Location = New System.Drawing.Point(334, 448)
        Label15.Name = "Label15"
        Label15.Size = New System.Drawing.Size(95, 15)
        Label15.TabIndex = 21
        Label15.Text = "Día de Corte :"
        '
        'ImportePorExtrasLabel
        '
        ImportePorExtrasLabel.AutoSize = True
        ImportePorExtrasLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ImportePorExtrasLabel.Location = New System.Drawing.Point(286, 61)
        ImportePorExtrasLabel.Name = "ImportePorExtrasLabel"
        ImportePorExtrasLabel.Size = New System.Drawing.Size(306, 15)
        ImportePorExtrasLabel.TabIndex = 16
        ImportePorExtrasLabel.Text = "Importe de la mensualidad de la TV Adicional :"
        ImportePorExtrasLabel.Visible = False
        '
        'CargaDeTrabajoLabel
        '
        CargaDeTrabajoLabel.AutoSize = True
        CargaDeTrabajoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CargaDeTrabajoLabel.Location = New System.Drawing.Point(32, 24)
        CargaDeTrabajoLabel.Name = "CargaDeTrabajoLabel"
        CargaDeTrabajoLabel.Size = New System.Drawing.Size(66, 15)
        CargaDeTrabajoLabel.TabIndex = 0
        CargaDeTrabajoLabel.Text = "General :"
        '
        'Carga_trab_insLabel
        '
        Carga_trab_insLabel.AutoSize = True
        Carga_trab_insLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Carga_trab_insLabel.Location = New System.Drawing.Point(13, 51)
        Carga_trab_insLabel.Name = "Carga_trab_insLabel"
        Carga_trab_insLabel.Size = New System.Drawing.Size(85, 15)
        Carga_trab_insLabel.TabIndex = 2
        Carga_trab_insLabel.Text = "Instalación :"
        '
        'NumeroExtLabel
        '
        NumeroExtLabel.AutoSize = True
        NumeroExtLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NumeroExtLabel.Location = New System.Drawing.Point(17, 21)
        NumeroExtLabel.Name = "NumeroExtLabel"
        NumeroExtLabel.Size = New System.Drawing.Size(160, 15)
        NumeroExtLabel.TabIndex = 0
        NumeroExtLabel.Text = "Numero de Exteciones :"
        NumeroExtLabel.Visible = False
        '
        'CostoExtLabel
        '
        CostoExtLabel.AutoSize = True
        CostoExtLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CostoExtLabel.Location = New System.Drawing.Point(35, 50)
        CostoExtLabel.Name = "CostoExtLabel"
        CostoExtLabel.Size = New System.Drawing.Size(143, 15)
        CostoExtLabel.TabIndex = 2
        CostoExtLabel.Text = "Costo por  Exteción : "
        CostoExtLabel.Visible = False
        '
        'ImpresoraOrdenesLabel
        '
        ImpresoraOrdenesLabel.AutoSize = True
        ImpresoraOrdenesLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ImpresoraOrdenesLabel.Location = New System.Drawing.Point(217, 192)
        ImpresoraOrdenesLabel.Name = "ImpresoraOrdenesLabel"
        ImpresoraOrdenesLabel.Size = New System.Drawing.Size(175, 15)
        ImpresoraOrdenesLabel.TabIndex = 21
        ImpresoraOrdenesLabel.Text = "Impresora Recone&xiones :"
        '
        'Puntos1Label
        '
        Puntos1Label.AutoSize = True
        Puntos1Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Puntos1Label.Location = New System.Drawing.Point(41, 49)
        Puntos1Label.Name = "Puntos1Label"
        Puntos1Label.Size = New System.Drawing.Size(55, 15)
        Puntos1Label.TabIndex = 23
        Puntos1Label.Text = "Puntos "
        '
        'Label24
        '
        Label24.AutoSize = True
        Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label24.Location = New System.Drawing.Point(151, 49)
        Label24.Name = "Label24"
        Label24.Size = New System.Drawing.Size(15, 15)
        Label24.TabIndex = 30
        Label24.Text = "a"
        '
        'Label25
        '
        Label25.AutoSize = True
        Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label25.Location = New System.Drawing.Point(227, 49)
        Label25.Name = "Label25"
        Label25.Size = New System.Drawing.Size(46, 15)
        Label25.TabIndex = 32
        Label25.Text = "años :"
        '
        'Label28
        '
        Label28.AutoSize = True
        Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label28.Location = New System.Drawing.Point(40, 76)
        Label28.Name = "Label28"
        Label28.Size = New System.Drawing.Size(55, 15)
        Label28.TabIndex = 33
        Label28.Text = "Puntos "
        '
        'Label27
        '
        Label27.AutoSize = True
        Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label27.Location = New System.Drawing.Point(151, 76)
        Label27.Name = "Label27"
        Label27.Size = New System.Drawing.Size(15, 15)
        Label27.TabIndex = 35
        Label27.Text = "a"
        '
        'Label26
        '
        Label26.AutoSize = True
        Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label26.Location = New System.Drawing.Point(227, 76)
        Label26.Name = "Label26"
        Label26.Size = New System.Drawing.Size(46, 15)
        Label26.TabIndex = 37
        Label26.Text = "años :"
        '
        'Label31
        '
        Label31.AutoSize = True
        Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label31.Location = New System.Drawing.Point(40, 102)
        Label31.Name = "Label31"
        Label31.Size = New System.Drawing.Size(55, 15)
        Label31.TabIndex = 38
        Label31.Text = "Puntos "
        '
        'Label30
        '
        Label30.AutoSize = True
        Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label30.Location = New System.Drawing.Point(151, 102)
        Label30.Name = "Label30"
        Label30.Size = New System.Drawing.Size(15, 15)
        Label30.TabIndex = 40
        Label30.Text = "a"
        '
        'Label29
        '
        Label29.AutoSize = True
        Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label29.Location = New System.Drawing.Point(227, 102)
        Label29.Name = "Label29"
        Label29.Size = New System.Drawing.Size(46, 15)
        Label29.TabIndex = 42
        Label29.Text = "años :"
        '
        'Label34
        '
        Label34.AutoSize = True
        Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label34.Location = New System.Drawing.Point(40, 128)
        Label34.Name = "Label34"
        Label34.Size = New System.Drawing.Size(55, 15)
        Label34.TabIndex = 43
        Label34.Text = "Puntos "
        '
        'Label33
        '
        Label33.AutoSize = True
        Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label33.Location = New System.Drawing.Point(151, 128)
        Label33.Name = "Label33"
        Label33.Size = New System.Drawing.Size(15, 15)
        Label33.TabIndex = 45
        Label33.Text = "a"
        '
        'Label32
        '
        Label32.AutoSize = True
        Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label32.Location = New System.Drawing.Point(227, 128)
        Label32.Name = "Label32"
        Label32.Size = New System.Drawing.Size(46, 15)
        Label32.TabIndex = 47
        Label32.Text = "años :"
        '
        'Si_se_generaLabel
        '
        Si_se_generaLabel.AutoSize = True
        Si_se_generaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Si_se_generaLabel.Location = New System.Drawing.Point(276, 97)
        Si_se_generaLabel.Name = "Si_se_generaLabel"
        Si_se_generaLabel.Size = New System.Drawing.Size(174, 16)
        Si_se_generaLabel.TabIndex = 52
        Si_se_generaLabel.Text = "Generar Factura Global "
        '
        'MensajeLabel
        '
        MensajeLabel.AutoSize = True
        MensajeLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MensajeLabel.Location = New System.Drawing.Point(210, 120)
        MensajeLabel.Name = "MensajeLabel"
        MensajeLabel.Size = New System.Drawing.Size(66, 15)
        MensajeLabel.TabIndex = 54
        MensajeLabel.Text = "Mensaje:"
        '
        'ActivoLabel
        '
        ActivoLabel.AutoSize = True
        ActivoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ActivoLabel.Location = New System.Drawing.Point(210, 299)
        ActivoLabel.Name = "ActivoLabel"
        ActivoLabel.Size = New System.Drawing.Size(48, 15)
        ActivoLabel.TabIndex = 56
        ActivoLabel.Text = "Activo:"
        '
        'Label38
        '
        Label38.AutoSize = True
        Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label38.Location = New System.Drawing.Point(197, 158)
        Label38.Name = "Label38"
        Label38.Size = New System.Drawing.Size(70, 15)
        Label38.TabIndex = 54
        Label38.Text = "Mensaje :"
        '
        'Label41
        '
        Label41.AutoSize = True
        Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label41.Location = New System.Drawing.Point(197, 102)
        Label41.Name = "Label41"
        Label41.Size = New System.Drawing.Size(99, 15)
        Label41.TabIndex = 58
        Label41.Text = "Monto Límite :"
        '
        'Label39
        '
        Label39.AutoSize = True
        Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label39.Location = New System.Drawing.Point(202, 275)
        Label39.Name = "Label39"
        Label39.Size = New System.Drawing.Size(27, 15)
        Label39.TabIndex = 60
        Label39.Text = "Ip :"
        '
        'CablemodemsLabel
        '
        CablemodemsLabel.AutoSize = True
        CablemodemsLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CablemodemsLabel.Location = New System.Drawing.Point(208, 128)
        CablemodemsLabel.Name = "CablemodemsLabel"
        CablemodemsLabel.Size = New System.Drawing.Size(112, 16)
        CablemodemsLabel.TabIndex = 73
        CablemodemsLabel.Text = "Cablemodems:"
        '
        'PregcablLabel
        '
        PregcablLabel.AutoSize = True
        PregcablLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PregcablLabel.Location = New System.Drawing.Point(458, 160)
        PregcablLabel.Name = "PregcablLabel"
        PregcablLabel.Size = New System.Drawing.Size(194, 16)
        PregcablLabel.TabIndex = 75
        PregcablLabel.Text = "Número Máximo de Pagos:"
        '
        'DecodersLabel
        '
        DecodersLabel.AutoSize = True
        DecodersLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DecodersLabel.Location = New System.Drawing.Point(179, 163)
        DecodersLabel.Name = "DecodersLabel"
        DecodersLabel.Size = New System.Drawing.Size(141, 16)
        DecodersLabel.TabIndex = 77
        DecodersLabel.Text = "Aparatos Digitales:"
        '
        'PregdecoLabel
        '
        PregdecoLabel.AutoSize = True
        PregdecoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PregdecoLabel.Location = New System.Drawing.Point(176, 258)
        PregdecoLabel.Name = "PregdecoLabel"
        PregdecoLabel.Size = New System.Drawing.Size(194, 16)
        PregdecoLabel.TabIndex = 79
        PregdecoLabel.Text = "Número Máximo de Pagos:"
        PregdecoLabel.Visible = False
        '
        'TELefonosLabel
        '
        TELefonosLabel.AutoSize = True
        TELefonosLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TELefonosLabel.Location = New System.Drawing.Point(182, 435)
        TELefonosLabel.Name = "TELefonosLabel"
        TELefonosLabel.Size = New System.Drawing.Size(78, 15)
        TELefonosLabel.TabIndex = 13
        TELefonosLabel.Text = "Teléfonos :"
        '
        'RfcLabel
        '
        RfcLabel.AutoSize = True
        RfcLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        RfcLabel.Location = New System.Drawing.Point(218, 408)
        RfcLabel.Name = "RfcLabel"
        RfcLabel.Size = New System.Drawing.Size(42, 15)
        RfcLabel.TabIndex = 11
        RfcLabel.Text = "RFC :"
        '
        'CiudadLabel
        '
        CiudadLabel.AutoSize = True
        CiudadLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CiudadLabel.Location = New System.Drawing.Point(204, 381)
        CiudadLabel.Name = "CiudadLabel"
        CiudadLabel.Size = New System.Drawing.Size(56, 15)
        CiudadLabel.TabIndex = 9
        CiudadLabel.Text = "Ciudad:"
        '
        'ColoniaLabel
        '
        ColoniaLabel.AutoSize = True
        ColoniaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ColoniaLabel.Location = New System.Drawing.Point(196, 327)
        ColoniaLabel.Name = "ColoniaLabel"
        ColoniaLabel.Size = New System.Drawing.Size(64, 15)
        ColoniaLabel.TabIndex = 7
        ColoniaLabel.Text = "Colonia :"
        '
        'DireccionLabel
        '
        DireccionLabel.AutoSize = True
        DireccionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DireccionLabel.Location = New System.Drawing.Point(188, 204)
        DireccionLabel.Name = "DireccionLabel"
        DireccionLabel.Size = New System.Drawing.Size(76, 15)
        DireccionLabel.TabIndex = 5
        DireccionLabel.Text = "Dirección :"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.Location = New System.Drawing.Point(188, 184)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(72, 15)
        NombreLabel.TabIndex = 3
        NombreLabel.Text = "Empresa :"
        '
        'Id_sucursalLabel
        '
        Id_sucursalLabel.AutoSize = True
        Id_sucursalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Id_sucursalLabel.Location = New System.Drawing.Point(134, 153)
        Id_sucursalLabel.Name = "Id_sucursalLabel"
        Id_sucursalLabel.Size = New System.Drawing.Size(126, 15)
        Id_sucursalLabel.TabIndex = 0
        Id_sucursalLabel.Text = "Clave de la Plaza :"
        '
        'Fecha_InicioLabel
        '
        Fecha_InicioLabel.AutoSize = True
        Fecha_InicioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_InicioLabel.Location = New System.Drawing.Point(63, 134)
        Fecha_InicioLabel.Name = "Fecha_InicioLabel"
        Fecha_InicioLabel.Size = New System.Drawing.Size(85, 13)
        Fecha_InicioLabel.TabIndex = 21
        Fecha_InicioLabel.Text = "Fecha Inicio :"
        '
        'Numero_InicioLabel
        '
        Numero_InicioLabel.AutoSize = True
        Numero_InicioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Numero_InicioLabel.Location = New System.Drawing.Point(63, 216)
        Numero_InicioLabel.Name = "Numero_InicioLabel"
        Numero_InicioLabel.Size = New System.Drawing.Size(93, 13)
        Numero_InicioLabel.TabIndex = 23
        Numero_InicioLabel.Text = "Número Inicio :"
        '
        'FormatoLabel
        '
        FormatoLabel.AutoSize = True
        FormatoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FormatoLabel.Location = New System.Drawing.Point(63, 242)
        FormatoLabel.Name = "FormatoLabel"
        FormatoLabel.Size = New System.Drawing.Size(60, 13)
        FormatoLabel.TabIndex = 25
        FormatoLabel.Text = "Formato :"
        '
        'RutaLabel
        '
        RutaLabel.AutoSize = True
        RutaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        RutaLabel.Location = New System.Drawing.Point(63, 268)
        RutaLabel.Name = "RutaLabel"
        RutaLabel.Size = New System.Drawing.Size(42, 13)
        RutaLabel.TabIndex = 27
        RutaLabel.Text = "Ruta :"
        '
        'Numero_LocalLabel
        '
        Numero_LocalLabel.AutoSize = True
        Numero_LocalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Numero_LocalLabel.Location = New System.Drawing.Point(63, 294)
        Numero_LocalLabel.Name = "Numero_LocalLabel"
        Numero_LocalLabel.Size = New System.Drawing.Size(93, 13)
        Numero_LocalLabel.TabIndex = 29
        Numero_LocalLabel.Text = "Número Local :"
        '
        'Pais_LocalLabel
        '
        Pais_LocalLabel.AutoSize = True
        Pais_LocalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Pais_LocalLabel.Location = New System.Drawing.Point(63, 320)
        Pais_LocalLabel.Name = "Pais_LocalLabel"
        Pais_LocalLabel.Size = New System.Drawing.Size(74, 13)
        Pais_LocalLabel.TabIndex = 31
        Pais_LocalLabel.Text = "Pais Local :"
        '
        'Clave044Label
        '
        Clave044Label.AutoSize = True
        Clave044Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clave044Label.Location = New System.Drawing.Point(63, 346)
        Clave044Label.Name = "Clave044Label"
        Clave044Label.Size = New System.Drawing.Size(72, 13)
        Clave044Label.TabIndex = 33
        Clave044Label.Text = "Clave 044 :"
        '
        'Clave045Label
        '
        Clave045Label.AutoSize = True
        Clave045Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clave045Label.Location = New System.Drawing.Point(63, 372)
        Clave045Label.Name = "Clave045Label"
        Clave045Label.Size = New System.Drawing.Size(72, 13)
        Clave045Label.TabIndex = 35
        Clave045Label.Text = "Clave 045 :"
        '
        'Clave01800Label
        '
        Clave01800Label.AutoSize = True
        Clave01800Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clave01800Label.Location = New System.Drawing.Point(63, 398)
        Clave01800Label.Name = "Clave01800Label"
        Clave01800Label.Size = New System.Drawing.Size(86, 13)
        Clave01800Label.TabIndex = 37
        Clave01800Label.Text = "Clave 01800 :"
        '
        'Clave01900Label
        '
        Clave01900Label.AutoSize = True
        Clave01900Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clave01900Label.Location = New System.Drawing.Point(63, 424)
        Clave01900Label.Name = "Clave01900Label"
        Clave01900Label.Size = New System.Drawing.Size(86, 13)
        Clave01900Label.TabIndex = 39
        Clave01900Label.Text = "Clave 01900 :"
        '
        'Label46
        '
        Label46.AutoSize = True
        Label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label46.Location = New System.Drawing.Point(63, 189)
        Label46.Name = "Label46"
        Label46.Size = New System.Drawing.Size(85, 13)
        Label46.TabIndex = 41
        Label46.Text = "Periodos del :"
        '
        'Nom_ArchivoLabel
        '
        Nom_ArchivoLabel.AutoSize = True
        Nom_ArchivoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Nom_ArchivoLabel.Location = New System.Drawing.Point(63, 107)
        Nom_ArchivoLabel.Name = "Nom_ArchivoLabel"
        Nom_ArchivoLabel.Size = New System.Drawing.Size(127, 13)
        Nom_ArchivoLabel.TabIndex = 19
        Nom_ArchivoLabel.Text = "Nombre de  Archivo :"
        '
        'Fecha_FacturasLabel
        '
        Fecha_FacturasLabel.AutoSize = True
        Fecha_FacturasLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_FacturasLabel.Location = New System.Drawing.Point(63, 162)
        Fecha_FacturasLabel.Name = "Fecha_FacturasLabel"
        Fecha_FacturasLabel.Size = New System.Drawing.Size(181, 13)
        Fecha_FacturasLabel.TabIndex = 47
        Fecha_FacturasLabel.Text = "Fecha de emisión de Facturas:"
        '
        'Periodo_inicialLabel
        '
        Periodo_inicialLabel.AutoSize = True
        Periodo_inicialLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Periodo_inicialLabel.Location = New System.Drawing.Point(305, 188)
        Periodo_inicialLabel.Name = "Periodo_inicialLabel"
        Periodo_inicialLabel.Size = New System.Drawing.Size(17, 13)
        Periodo_inicialLabel.TabIndex = 49
        Periodo_inicialLabel.Text = "al"
        '
        'ModuloLabel
        '
        ModuloLabel.AutoSize = True
        ModuloLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ModuloLabel.Location = New System.Drawing.Point(297, 202)
        ModuloLabel.Name = "ModuloLabel"
        ModuloLabel.Size = New System.Drawing.Size(63, 15)
        ModuloLabel.TabIndex = 21
        ModuloLabel.Text = "Módulo :"
        '
        'ConstanteLabel
        '
        ConstanteLabel.AutoSize = True
        ConstanteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ConstanteLabel.Location = New System.Drawing.Point(281, 171)
        ConstanteLabel.Name = "ConstanteLabel"
        ConstanteLabel.Size = New System.Drawing.Size(79, 15)
        ConstanteLabel.TabIndex = 19
        ConstanteLabel.Text = "Constante :"
        '
        'ReferenciaLabel
        '
        ReferenciaLabel.AutoSize = True
        ReferenciaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ReferenciaLabel.Location = New System.Drawing.Point(232, 140)
        ReferenciaLabel.Name = "ReferenciaLabel"
        ReferenciaLabel.Size = New System.Drawing.Size(128, 15)
        ReferenciaLabel.TabIndex = 17
        ReferenciaLabel.Text = "Referencia Inicial :"
        '
        'Label48
        '
        Label48.AutoSize = True
        Label48.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label48.Location = New System.Drawing.Point(279, 132)
        Label48.Name = "Label48"
        Label48.Size = New System.Drawing.Size(128, 15)
        Label48.TabIndex = 17
        Label48.Text = "Referencia Inicial :"
        '
        'Label50
        '
        Label50.AutoSize = True
        Label50.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label50.Location = New System.Drawing.Point(344, 166)
        Label50.Name = "Label50"
        Label50.Size = New System.Drawing.Size(63, 15)
        Label50.TabIndex = 21
        Label50.Text = "Módulo :"
        '
        'Label52
        '
        Label52.AutoSize = True
        Label52.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label52.Location = New System.Drawing.Point(543, 229)
        Label52.Name = "Label52"
        Label52.Size = New System.Drawing.Size(44, 13)
        Label52.TabIndex = 72
        Label52.Text = "Serie :"
        '
        'Label53
        '
        Label53.AutoSize = True
        Label53.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label53.Location = New System.Drawing.Point(543, 255)
        Label53.Name = "Label53"
        Label53.Size = New System.Drawing.Size(42, 13)
        Label53.TabIndex = 73
        Label53.Text = "Folio :"
        '
        'LblCdClvAdic
        '
        LblCdClvAdic.AutoSize = True
        LblCdClvAdic.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        LblCdClvAdic.Location = New System.Drawing.Point(542, 346)
        LblCdClvAdic.Name = "LblCdClvAdic"
        LblCdClvAdic.Size = New System.Drawing.Size(118, 13)
        LblCdClvAdic.TabIndex = 78
        LblCdClvAdic.Text = "Ciudad / Municipio:"
        '
        'LblEdoClvAdic
        '
        LblEdoClvAdic.AutoSize = True
        LblEdoClvAdic.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        LblEdoClvAdic.Location = New System.Drawing.Point(542, 320)
        LblEdoClvAdic.Name = "LblEdoClvAdic"
        LblEdoClvAdic.Size = New System.Drawing.Size(54, 13)
        LblEdoClvAdic.TabIndex = 77
        LblEdoClvAdic.Text = "Estado :"
        '
        'lblPPE2
        '
        lblPPE2.AutoSize = True
        lblPPE2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        lblPPE2.Location = New System.Drawing.Point(60, 193)
        lblPPE2.Name = "lblPPE2"
        lblPPE2.Size = New System.Drawing.Size(385, 16)
        lblPPE2.TabIndex = 82
        lblPPE2.Text = "Puntos Requeridos Para La Contratación De Peliculas:"
        '
        'Label12
        '
        Label12.Location = New System.Drawing.Point(730, 211)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(100, 23)
        Label12.TabIndex = 41
        AddHandler Label12.Click, AddressOf Me.Label12_Click
        '
        'Label61
        '
        Label61.AutoSize = True
        Label61.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label61.Location = New System.Drawing.Point(132, 339)
        Label61.Name = "Label61"
        Label61.Size = New System.Drawing.Size(351, 15)
        Label61.TabIndex = 25
        Label61.Text = "Dias Proporcionales Para Desconexion y Suspención:"
        '
        'Label63
        '
        Label63.AutoSize = True
        Label63.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label63.Location = New System.Drawing.Point(212, 258)
        Label63.Name = "Label63"
        Label63.Size = New System.Drawing.Size(271, 15)
        Label63.TabIndex = 24
        Label63.Text = "Dias Propoecionales Para Contratación : "
        '
        'Label69
        '
        Label69.AutoSize = True
        Label69.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label69.Location = New System.Drawing.Point(201, 107)
        Label69.Name = "Label69"
        Label69.Size = New System.Drawing.Size(47, 15)
        Label69.TabIndex = 53
        Label69.Text = "I.V.A. :"
        '
        'Label72
        '
        Label72.AutoSize = True
        Label72.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label72.Location = New System.Drawing.Point(371, 482)
        Label72.Name = "Label72"
        Label72.Size = New System.Drawing.Size(58, 15)
        Label72.TabIndex = 45
        Label72.Text = "Cobrar :"
        '
        'Label125
        '
        Label125.AutoSize = True
        Label125.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label125.Location = New System.Drawing.Point(304, 374)
        Label125.Name = "Label125"
        Label125.Size = New System.Drawing.Size(124, 15)
        Label125.TabIndex = 47
        Label125.Text = "Por Pronto Pago : "
        '
        'ConsultaCpGeneBindingSource
        '
        Me.ConsultaCpGeneBindingSource.DataMember = "Consulta_Cp_Gene"
        Me.ConsultaCpGeneBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Consulta_ImpresorasBindingSource
        '
        Me.Consulta_ImpresorasBindingSource.DataMember = "Consulta_Impresoras"
        Me.Consulta_ImpresorasBindingSource.DataSource = Me.DataSetarnoldo
        '
        'MuestraTipServEricBindingSource
        '
        Me.MuestraTipServEricBindingSource.DataMember = "MuestraTipServEric"
        Me.MuestraTipServEricBindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConMesesCobroAdeudoBindingSource
        '
        Me.ConMesesCobroAdeudoBindingSource.DataMember = "ConMesesCobroAdeudo"
        Me.ConMesesCobroAdeudoBindingSource.DataSource = Me.DataSetEric
        '
        'ConGeneralCorreoBindingSource
        '
        Me.ConGeneralCorreoBindingSource.DataMember = "ConGeneralCorreo"
        Me.ConGeneralCorreoBindingSource.DataSource = Me.DataSetEric
        '
        'Consulta_ImpresorasTableAdapter
        '
        Me.Consulta_ImpresorasTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Mod_Cp_GenBindingSource
        '
        Me.Inserta_Mod_Cp_GenBindingSource.DataMember = "Inserta_Mod_Cp_Gen"
        Me.Inserta_Mod_Cp_GenBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Inserta_Mod_Cp_GenTableAdapter
        '
        Me.Inserta_Mod_Cp_GenTableAdapter.ClearBeforeFill = True
        '
        'Consulta_Cp_GeneTableAdapter
        '
        Me.Consulta_Cp_GeneTableAdapter.ClearBeforeFill = True
        '
        'ConMesesCobroAdeudoTableAdapter
        '
        Me.ConMesesCobroAdeudoTableAdapter.ClearBeforeFill = True
        '
        'MuestraTipServEricTableAdapter
        '
        Me.MuestraTipServEricTableAdapter.ClearBeforeFill = True
        '
        'ConGeneralCorreoTableAdapter
        '
        Me.ConGeneralCorreoTableAdapter.ClearBeforeFill = True
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Consulta_Generales_Filtros_OrdenesBindingSource
        '
        Me.Consulta_Generales_Filtros_OrdenesBindingSource.DataMember = "Consulta_Generales_Filtros_Ordenes"
        Me.Consulta_Generales_Filtros_OrdenesBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Consulta_Generales_Filtros_OrdenesTableAdapter
        '
        Me.Consulta_Generales_Filtros_OrdenesTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Generales_Filtros_OrdenesBindingSource
        '
        Me.Inserta_Generales_Filtros_OrdenesBindingSource.DataMember = "Inserta_Generales_Filtros_Ordenes"
        Me.Inserta_Generales_Filtros_OrdenesBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Inserta_Generales_Filtros_OrdenesTableAdapter
        '
        Me.Inserta_Generales_Filtros_OrdenesTableAdapter.ClearBeforeFill = True
        '
        'COnsultaGralfacturaglobalBindingSource
        '
        Me.COnsultaGralfacturaglobalBindingSource.DataMember = "COnsulta_Gral_factura_global"
        Me.COnsultaGralfacturaglobalBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'COnsulta_Gral_factura_globalTableAdapter
        '
        Me.COnsulta_Gral_factura_globalTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Gral_factura_globalBindingSource
        '
        Me.Inserta_Gral_factura_globalBindingSource.DataMember = "Inserta_Gral_factura_global"
        Me.Inserta_Gral_factura_globalBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Inserta_Gral_factura_globalTableAdapter
        '
        Me.Inserta_Gral_factura_globalTableAdapter.ClearBeforeFill = True
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Transparent
        Me.Panel4.Controls.Add(Me.Button12)
        Me.Panel4.Controls.Add(CuentaLabel)
        Me.Panel4.Controls.Add(Me.CuentaTextBox)
        Me.Panel4.Controls.Add(PasswordLabel)
        Me.Panel4.Controls.Add(Me.PasswordTextBox)
        Me.Panel4.Controls.Add(HostLabel)
        Me.Panel4.Controls.Add(Me.HostTextBox)
        Me.Panel4.Controls.Add(PortLabel)
        Me.Panel4.Controls.Add(Me.PortTextBox)
        Me.Panel4.Controls.Add(Me.Label17)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(865, 572)
        Me.Panel4.TabIndex = 32
        '
        'Button12
        '
        Me.Button12.BackColor = System.Drawing.SystemColors.Control
        Me.Button12.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button12.ForeColor = System.Drawing.Color.Black
        Me.Button12.Location = New System.Drawing.Point(694, 543)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(136, 36)
        Me.Button12.TabIndex = 29
        Me.Button12.Text = "&GUARDAR"
        Me.Button12.UseVisualStyleBackColor = False
        '
        'CuentaTextBox
        '
        Me.CuentaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralCorreoBindingSource, "Cuenta", True))
        Me.CuentaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CuentaTextBox.Location = New System.Drawing.Point(379, 126)
        Me.CuentaTextBox.Name = "CuentaTextBox"
        Me.CuentaTextBox.Size = New System.Drawing.Size(208, 21)
        Me.CuentaTextBox.TabIndex = 30
        '
        'PasswordTextBox
        '
        Me.PasswordTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralCorreoBindingSource, "Password", True))
        Me.PasswordTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PasswordTextBox.Location = New System.Drawing.Point(379, 153)
        Me.PasswordTextBox.Name = "PasswordTextBox"
        Me.PasswordTextBox.Size = New System.Drawing.Size(208, 21)
        Me.PasswordTextBox.TabIndex = 32
        '
        'HostTextBox
        '
        Me.HostTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralCorreoBindingSource, "Host", True))
        Me.HostTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HostTextBox.Location = New System.Drawing.Point(379, 180)
        Me.HostTextBox.Name = "HostTextBox"
        Me.HostTextBox.Size = New System.Drawing.Size(208, 21)
        Me.HostTextBox.TabIndex = 34
        '
        'PortTextBox
        '
        Me.PortTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralCorreoBindingSource, "Port", True))
        Me.PortTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PortTextBox.Location = New System.Drawing.Point(379, 207)
        Me.PortTextBox.Name = "PortTextBox"
        Me.PortTextBox.Size = New System.Drawing.Size(127, 21)
        Me.PortTextBox.TabIndex = 36
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.Black
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(50, 32)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(63, 20)
        Me.Label17.TabIndex = 29
        Me.Label17.Text = "Correo"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Transparent
        Me.Panel3.Controls.Add(Me.Label60)
        Me.Panel3.Controls.Add(Me.DataGridViewCobro)
        Me.Panel3.Controls.Add(Me.ComboBoxCobro)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.Button11)
        Me.Panel3.Controls.Add(Me.Clv_TipSerTextBox)
        Me.Panel3.Controls.Add(Me.ConceptoComboBox)
        Me.Panel3.Controls.Add(MesesLabel)
        Me.Panel3.Controls.Add(Me.MesesTextBox)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(865, 572)
        Me.Panel3.TabIndex = 31
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(224, 97)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(110, 15)
        Me.Label60.TabIndex = 25
        Me.Label60.Text = "Tipo de Servicio"
        '
        'DataGridViewCobro
        '
        Me.DataGridViewCobro.AllowUserToAddRows = False
        Me.DataGridViewCobro.AllowUserToDeleteRows = False
        Me.DataGridViewCobro.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DataGridViewCobro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewCobro.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_TipoCliente, Me.Descripcion, Me.Meses, Me.Habilitado})
        Me.DataGridViewCobro.Location = New System.Drawing.Point(227, 158)
        Me.DataGridViewCobro.Name = "DataGridViewCobro"
        Me.DataGridViewCobro.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridViewCobro.Size = New System.Drawing.Size(458, 301)
        Me.DataGridViewCobro.TabIndex = 24
        '
        'Clv_TipoCliente
        '
        Me.Clv_TipoCliente.DataPropertyName = "Clv_TipoCliente"
        Me.Clv_TipoCliente.HeaderText = "Clv_TipoCliente"
        Me.Clv_TipoCliente.Name = "Clv_TipoCliente"
        Me.Clv_TipoCliente.Visible = False
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Tipo Cliente"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.Width = 200
        '
        'Meses
        '
        Me.Meses.DataPropertyName = "Meses"
        Me.Meses.HeaderText = "Meses"
        Me.Meses.Name = "Meses"
        '
        'Habilitado
        '
        Me.Habilitado.DataPropertyName = "Habilitado"
        Me.Habilitado.HeaderText = "Habilitado"
        Me.Habilitado.Name = "Habilitado"
        '
        'ComboBoxCobro
        '
        Me.ComboBoxCobro.DisplayMember = "Concepto"
        Me.ComboBoxCobro.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCobro.FormattingEnabled = True
        Me.ComboBoxCobro.Location = New System.Drawing.Point(227, 115)
        Me.ComboBoxCobro.Name = "ComboBoxCobro"
        Me.ComboBoxCobro.Size = New System.Drawing.Size(262, 23)
        Me.ComboBoxCobro.TabIndex = 23
        Me.ComboBoxCobro.ValueMember = "Clv_TipSer"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Black
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(37, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(246, 20)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "Contratación a Plazo Forzoso"
        '
        'Button11
        '
        Me.Button11.BackColor = System.Drawing.SystemColors.Control
        Me.Button11.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button11.ForeColor = System.Drawing.Color.Black
        Me.Button11.Location = New System.Drawing.Point(692, 528)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(136, 36)
        Me.Button11.TabIndex = 21
        Me.Button11.Text = "&GUARDAR"
        Me.Button11.UseVisualStyleBackColor = False
        '
        'Clv_TipSerTextBox
        '
        Me.Clv_TipSerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraTipServEricBindingSource, "Clv_TipSer", True))
        Me.Clv_TipSerTextBox.Location = New System.Drawing.Point(755, 551)
        Me.Clv_TipSerTextBox.Name = "Clv_TipSerTextBox"
        Me.Clv_TipSerTextBox.ReadOnly = True
        Me.Clv_TipSerTextBox.Size = New System.Drawing.Size(10, 21)
        Me.Clv_TipSerTextBox.TabIndex = 7
        Me.Clv_TipSerTextBox.TabStop = False
        '
        'ConceptoComboBox
        '
        Me.ConceptoComboBox.DataSource = Me.MuestraTipServEricBindingSource
        Me.ConceptoComboBox.DisplayMember = "Concepto"
        Me.ConceptoComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoComboBox.FormattingEnabled = True
        Me.ConceptoComboBox.Location = New System.Drawing.Point(778, 551)
        Me.ConceptoComboBox.Name = "ConceptoComboBox"
        Me.ConceptoComboBox.Size = New System.Drawing.Size(10, 23)
        Me.ConceptoComboBox.TabIndex = 6
        Me.ConceptoComboBox.ValueMember = "Clv_TipSer"
        Me.ConceptoComboBox.Visible = False
        '
        'MesesTextBox
        '
        Me.MesesTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConMesesCobroAdeudoBindingSource, "Meses", True))
        Me.MesesTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MesesTextBox.Location = New System.Drawing.Point(755, 551)
        Me.MesesTextBox.Name = "MesesTextBox"
        Me.MesesTextBox.Size = New System.Drawing.Size(17, 21)
        Me.MesesTextBox.TabIndex = 5
        Me.MesesTextBox.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.Label124)
        Me.Panel1.Controls.Add(Me.tbPuntosAnti)
        Me.Panel1.Controls.Add(Label125)
        Me.Panel1.Controls.Add(Label72)
        Me.Panel1.Controls.Add(Me.rb30)
        Me.Panel1.Controls.Add(Me.rb15)
        Me.Panel1.Controls.Add(Me.Label71)
        Me.Panel1.Controls.Add(Me.CheckBox1)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.txtDiaCorte)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.TextBox15)
        Me.Panel1.Controls.Add(Label21)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.TextBox11)
        Me.Panel1.Controls.Add(Label20)
        Me.Panel1.Controls.Add(Label15)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Label16)
        Me.Panel1.Controls.Add(Me.TextBox14)
        Me.Panel1.Controls.Add(Me.Button6)
        Me.Panel1.Controls.Add(Label12)
        Me.Panel1.Controls.Add(Me.TextBox16)
        Me.Panel1.Controls.Add(Me.TextBox13)
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Me.TextBox7)
        Me.Panel1.Controls.Add(Label22)
        Me.Panel1.Controls.Add(Label19)
        Me.Panel1.Controls.Add(Me.Label23)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(865, 572)
        Me.Panel1.TabIndex = 29
        '
        'Label124
        '
        Me.Label124.BackColor = System.Drawing.Color.Black
        Me.Label124.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label124.ForeColor = System.Drawing.Color.White
        Me.Label124.Location = New System.Drawing.Point(246, 336)
        Me.Label124.Name = "Label124"
        Me.Label124.Size = New System.Drawing.Size(330, 23)
        Me.Label124.TabIndex = 48
        Me.Label124.Text = "Días Limite para Puntos de Antigüedad  :"
        Me.Label124.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tbPuntosAnti
        '
        Me.tbPuntosAnti.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTAGENERALESDESCBindingSource, "DiasPuntosAnt1", True))
        Me.tbPuntosAnti.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbPuntosAnti.Location = New System.Drawing.Point(434, 372)
        Me.tbPuntosAnti.MaxLength = 3
        Me.tbPuntosAnti.Name = "tbPuntosAnti"
        Me.tbPuntosAnti.Size = New System.Drawing.Size(100, 21)
        Me.tbPuntosAnti.TabIndex = 46
        '
        'CONSULTAGENERALESDESCBindingSource
        '
        Me.CONSULTAGENERALESDESCBindingSource.DataMember = "CONSULTAGENERALESDESC"
        Me.CONSULTAGENERALESDESCBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Procedimientosarnoldo4
        '
        Me.Procedimientosarnoldo4.DataSetName = "Procedimientosarnoldo4"
        Me.Procedimientosarnoldo4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'rb30
        '
        Me.rb30.AutoSize = True
        Me.rb30.Location = New System.Drawing.Point(435, 505)
        Me.rb30.Name = "rb30"
        Me.rb30.Size = New System.Drawing.Size(91, 17)
        Me.rb30.TabIndex = 44
        Me.rb30.Text = "Mes completo"
        Me.rb30.UseVisualStyleBackColor = True
        '
        'rb15
        '
        Me.rb15.AutoSize = True
        Me.rb15.Checked = True
        Me.rb15.Location = New System.Drawing.Point(435, 480)
        Me.rb15.Name = "rb15"
        Me.rb15.Size = New System.Drawing.Size(61, 17)
        Me.rb15.TabIndex = 43
        Me.rb15.TabStop = True
        Me.rb15.Text = "15 días"
        Me.rb15.UseVisualStyleBackColor = True
        '
        'Label71
        '
        Me.Label71.BackColor = System.Drawing.Color.Black
        Me.Label71.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label71.ForeColor = System.Drawing.Color.White
        Me.Label71.Location = New System.Drawing.Point(247, 407)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(330, 23)
        Me.Label71.TabIndex = 42
        Me.Label71.Text = "Pagos Diferidos :"
        Me.Label71.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.MUESTRAPERIODOSBindingSource, "Habilitar", True))
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(504, 77)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(104, 19)
        Me.CheckBox1.TabIndex = 11
        Me.CheckBox1.Text = "Deshabilitar"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'MUESTRAPERIODOSBindingSource
        '
        Me.MUESTRAPERIODOSBindingSource.DataMember = "MUESTRAPERIODOS"
        Me.MUESTRAPERIODOSBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.Black
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(34, 29)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(144, 20)
        Me.Label18.TabIndex = 16
        Me.Label18.Text = "Periodo de Corte"
        '
        'txtDiaCorte
        '
        Me.txtDiaCorte.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTAGENERALESDESCBindingSource, "DiasPuntosAnt1", True))
        Me.txtDiaCorte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiaCorte.Location = New System.Drawing.Point(435, 442)
        Me.txtDiaCorte.MaxLength = 3
        Me.txtDiaCorte.Name = "txtDiaCorte"
        Me.txtDiaCorte.Size = New System.Drawing.Size(100, 21)
        Me.txtDiaCorte.TabIndex = 16
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Black
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(246, 263)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(330, 23)
        Me.Label7.TabIndex = 39
        Me.Label7.Text = "Días Limite para Puntos  :"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox15
        '
        Me.TextBox15.Location = New System.Drawing.Point(585, 310)
        Me.TextBox15.MaxLength = 3
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.ReadOnly = True
        Me.TextBox15.Size = New System.Drawing.Size(110, 21)
        Me.TextBox15.TabIndex = 32
        Me.TextBox15.TabStop = False
        Me.TextBox15.Visible = False
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.Black
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(246, 200)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(330, 23)
        Me.Label6.TabIndex = 38
        Me.Label6.Text = "Proceso de Desconexión :"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox11
        '
        Me.TextBox11.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTAGENERALESDESCBindingSource, "DiasPuntosPP1", True))
        Me.TextBox11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox11.Location = New System.Drawing.Point(434, 299)
        Me.TextBox11.MaxLength = 3
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(100, 21)
        Me.TextBox11.TabIndex = 15
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Black
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(247, 115)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(329, 23)
        Me.Label5.TabIndex = 37
        Me.Label5.Text = "Clientes que Contraten :"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox14
        '
        Me.TextBox14.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTAGENERALESDESCBindingSource, "DiaInicial1", True))
        Me.TextBox14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox14.Location = New System.Drawing.Point(434, 148)
        Me.TextBox14.MaxLength = 3
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Size = New System.Drawing.Size(100, 21)
        Me.TextBox14.TabIndex = 12
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.SystemColors.Control
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(694, 515)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 36)
        Me.Button6.TabIndex = 17
        Me.Button6.Text = "&GUARDAR"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'TextBox16
        '
        Me.TextBox16.Location = New System.Drawing.Point(586, 270)
        Me.TextBox16.MaxLength = 3
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.ReadOnly = True
        Me.TextBox16.Size = New System.Drawing.Size(109, 21)
        Me.TextBox16.TabIndex = 31
        Me.TextBox16.TabStop = False
        Me.TextBox16.Visible = False
        '
        'TextBox13
        '
        Me.TextBox13.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTAGENERALESDESCBindingSource, "DiaFinal1", True))
        Me.TextBox13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox13.Location = New System.Drawing.Point(434, 175)
        Me.TextBox13.MaxLength = 3
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.Size = New System.Drawing.Size(100, 21)
        Me.TextBox13.TabIndex = 13
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.MUESTRAPERIODOSBindingSource
        Me.ComboBox1.DisplayMember = "Descripcion"
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(250, 77)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(205, 23)
        Me.ComboBox1.TabIndex = 10
        Me.ComboBox1.ValueMember = "Clv_Periodo"
        '
        'TextBox7
        '
        Me.TextBox7.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTAGENERALESDESCBindingSource, "DiaCorte", True))
        Me.TextBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox7.Location = New System.Drawing.Point(434, 233)
        Me.TextBox7.MaxLength = 3
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(100, 21)
        Me.TextBox7.TabIndex = 14
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Black
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.Location = New System.Drawing.Point(246, 51)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(75, 20)
        Me.Label23.TabIndex = 34
        Me.Label23.Text = "Periodo:"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Panel2.Controls.Add(Me.Label13)
        Me.Panel2.Controls.Add(Me.Button8)
        Me.Panel2.Controls.Add(Me.Label11)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.ImpresoraFiscalTextBox)
        Me.Panel2.Controls.Add(Me.ImpresoraTarjetasTextBox)
        Me.Panel2.Controls.Add(Me.ImpresoraTicketsTextBox)
        Me.Panel2.Controls.Add(Me.ImpresoraContratosTextBox)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(865, 572)
        Me.Panel2.TabIndex = 30
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Black
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(49, 35)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(88, 20)
        Me.Label13.TabIndex = 44
        Me.Label13.Text = "Impresión"
        '
        'Button8
        '
        Me.Button8.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.Button8.BackColor = System.Drawing.SystemColors.Control
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.Black
        Me.Button8.Location = New System.Drawing.Point(694, 543)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(136, 36)
        Me.Button8.TabIndex = 14
        Me.Button8.Text = "&GUARDAR"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.Black
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(291, 370)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(330, 23)
        Me.Label11.TabIndex = 42
        Me.Label11.Text = "Impresora de Contratos:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.Black
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(291, 277)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(330, 23)
        Me.Label10.TabIndex = 41
        Me.Label10.Text = "Impresora de Tickets:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.Black
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(291, 191)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(330, 23)
        Me.Label9.TabIndex = 40
        Me.Label9.Text = "Impresora de Tarjetas:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.Black
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(285, 107)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(330, 23)
        Me.Label8.TabIndex = 39
        Me.Label8.Text = "Impresora Fiscal:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ImpresoraFiscalTextBox
        '
        Me.ImpresoraFiscalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ImpresorasBindingSource, "ImpresoraFiscal", True))
        Me.ImpresoraFiscalTextBox.Location = New System.Drawing.Point(290, 136)
        Me.ImpresoraFiscalTextBox.Name = "ImpresoraFiscalTextBox"
        Me.ImpresoraFiscalTextBox.Size = New System.Drawing.Size(325, 21)
        Me.ImpresoraFiscalTextBox.TabIndex = 10
        '
        'ImpresoraTarjetasTextBox
        '
        Me.ImpresoraTarjetasTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ImpresorasBindingSource, "ImpresoraTarjetas", True))
        Me.ImpresoraTarjetasTextBox.Location = New System.Drawing.Point(291, 223)
        Me.ImpresoraTarjetasTextBox.Name = "ImpresoraTarjetasTextBox"
        Me.ImpresoraTarjetasTextBox.Size = New System.Drawing.Size(330, 21)
        Me.ImpresoraTarjetasTextBox.TabIndex = 11
        '
        'ImpresoraTicketsTextBox
        '
        Me.ImpresoraTicketsTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ImpresorasBindingSource, "ImpresoraTickets", True))
        Me.ImpresoraTicketsTextBox.Location = New System.Drawing.Point(295, 307)
        Me.ImpresoraTicketsTextBox.Name = "ImpresoraTicketsTextBox"
        Me.ImpresoraTicketsTextBox.Size = New System.Drawing.Size(326, 21)
        Me.ImpresoraTicketsTextBox.TabIndex = 12
        '
        'ImpresoraContratosTextBox
        '
        Me.ImpresoraContratosTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_ImpresorasBindingSource, "ImpresoraContratos", True))
        Me.ImpresoraContratosTextBox.Location = New System.Drawing.Point(299, 400)
        Me.ImpresoraContratosTextBox.Name = "ImpresoraContratosTextBox"
        Me.ImpresoraContratosTextBox.Size = New System.Drawing.Size(325, 21)
        Me.ImpresoraContratosTextBox.TabIndex = 13
        '
        'PanelBasico
        '
        Me.PanelBasico.BackColor = System.Drawing.Color.Transparent
        Me.PanelBasico.Controls.Add(Me.Panel13)
        Me.PanelBasico.Controls.Add(Me.CheckBox3)
        Me.PanelBasico.Controls.Add(Me.Label3)
        Me.PanelBasico.Controls.Add(Me.CheckBox2)
        Me.PanelBasico.Controls.Add(Me.Label35)
        Me.PanelBasico.Controls.Add(Me.Panel5)
        Me.PanelBasico.Controls.Add(Me.ImportePorExtrasTextBox)
        Me.PanelBasico.Controls.Add(ImportePorExtrasLabel)
        Me.PanelBasico.Controls.Add(Me.GroupBox2)
        Me.PanelBasico.Controls.Add(Me.Button3)
        Me.PanelBasico.Controls.Add(Me.GroupBox3)
        Me.PanelBasico.Controls.Add(Me.ImpresoraOrdenesTextBox)
        Me.PanelBasico.Controls.Add(ImpresoraOrdenesLabel)
        Me.PanelBasico.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelBasico.Location = New System.Drawing.Point(3, 3)
        Me.PanelBasico.Name = "PanelBasico"
        Me.PanelBasico.Size = New System.Drawing.Size(859, 566)
        Me.PanelBasico.TabIndex = 3
        '
        'Panel13
        '
        Me.Panel13.Controls.Add(Me.Label4)
        Me.Panel13.Controls.Add(Me.Puntos1TextBox)
        Me.Panel13.Controls.Add(Label30)
        Me.Panel13.Controls.Add(Me.NumericUpDown2)
        Me.Panel13.Controls.Add(Me.NumericUpDown5)
        Me.Panel13.Controls.Add(Puntos1Label)
        Me.Panel13.Controls.Add(Label25)
        Me.Panel13.Controls.Add(Me.NumericUpDown6)
        Me.Panel13.Controls.Add(Label24)
        Me.Panel13.Controls.Add(Label29)
        Me.Panel13.Controls.Add(Me.Puntos2TextBox)
        Me.Panel13.Controls.Add(Label28)
        Me.Panel13.Controls.Add(Label31)
        Me.Panel13.Controls.Add(Label34)
        Me.Panel13.Controls.Add(Me.NumericUpDown1)
        Me.Panel13.Controls.Add(Me.Puntos5TextBox)
        Me.Panel13.Controls.Add(Label32)
        Me.Panel13.Controls.Add(Label26)
        Me.Panel13.Controls.Add(Me.NumericUpDown4)
        Me.Panel13.Controls.Add(Me.NumericUpDown8)
        Me.Panel13.Controls.Add(Me.NumericUpDown3)
        Me.Panel13.Controls.Add(Label33)
        Me.Panel13.Controls.Add(Me.NumericUpDown7)
        Me.Panel13.Controls.Add(Me.Puntos10TextBox)
        Me.Panel13.Controls.Add(Label27)
        Me.Panel13.Enabled = False
        Me.Panel13.Location = New System.Drawing.Point(28, 229)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(21, 24)
        Me.Panel13.TabIndex = 52
        Me.Panel13.Visible = False
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Black
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(28, 14)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(358, 20)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "Puntos Por Antigüedad"
        '
        'Puntos1TextBox
        '
        Me.Puntos1TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralAntiguedadBindingSource, "Puntos1", True))
        Me.Puntos1TextBox.Location = New System.Drawing.Point(279, 46)
        Me.Puntos1TextBox.MaxLength = 3
        Me.Puntos1TextBox.Name = "Puntos1TextBox"
        Me.Puntos1TextBox.Size = New System.Drawing.Size(100, 21)
        Me.Puntos1TextBox.TabIndex = 18
        '
        'ConGeneralAntiguedadBindingSource
        '
        Me.ConGeneralAntiguedadBindingSource.DataMember = "ConGeneralAntiguedad"
        Me.ConGeneralAntiguedadBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'NumericUpDown2
        '
        Me.NumericUpDown2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown2.Location = New System.Drawing.Point(171, 46)
        Me.NumericUpDown2.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown2.Name = "NumericUpDown2"
        Me.NumericUpDown2.Size = New System.Drawing.Size(52, 21)
        Me.NumericUpDown2.TabIndex = 17
        '
        'NumericUpDown5
        '
        Me.NumericUpDown5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown5.Location = New System.Drawing.Point(96, 99)
        Me.NumericUpDown5.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown5.Name = "NumericUpDown5"
        Me.NumericUpDown5.Size = New System.Drawing.Size(51, 21)
        Me.NumericUpDown5.TabIndex = 22
        '
        'NumericUpDown6
        '
        Me.NumericUpDown6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown6.Location = New System.Drawing.Point(173, 99)
        Me.NumericUpDown6.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown6.Name = "NumericUpDown6"
        Me.NumericUpDown6.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDown6.TabIndex = 23
        '
        'Puntos2TextBox
        '
        Me.Puntos2TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralAntiguedadBindingSource, "Puntos2", True))
        Me.Puntos2TextBox.Location = New System.Drawing.Point(279, 73)
        Me.Puntos2TextBox.MaxLength = 3
        Me.Puntos2TextBox.Name = "Puntos2TextBox"
        Me.Puntos2TextBox.Size = New System.Drawing.Size(100, 21)
        Me.Puntos2TextBox.TabIndex = 21
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown1.Location = New System.Drawing.Point(96, 46)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(52, 21)
        Me.NumericUpDown1.TabIndex = 16
        '
        'Puntos5TextBox
        '
        Me.Puntos5TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralAntiguedadBindingSource, "Puntos5", True))
        Me.Puntos5TextBox.Location = New System.Drawing.Point(279, 99)
        Me.Puntos5TextBox.MaxLength = 3
        Me.Puntos5TextBox.Name = "Puntos5TextBox"
        Me.Puntos5TextBox.Size = New System.Drawing.Size(100, 21)
        Me.Puntos5TextBox.TabIndex = 24
        '
        'NumericUpDown4
        '
        Me.NumericUpDown4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown4.Location = New System.Drawing.Point(173, 73)
        Me.NumericUpDown4.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown4.Name = "NumericUpDown4"
        Me.NumericUpDown4.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDown4.TabIndex = 20
        '
        'NumericUpDown8
        '
        Me.NumericUpDown8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown8.Location = New System.Drawing.Point(173, 125)
        Me.NumericUpDown8.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown8.Name = "NumericUpDown8"
        Me.NumericUpDown8.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDown8.TabIndex = 26
        '
        'NumericUpDown3
        '
        Me.NumericUpDown3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown3.Location = New System.Drawing.Point(96, 73)
        Me.NumericUpDown3.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown3.Name = "NumericUpDown3"
        Me.NumericUpDown3.Size = New System.Drawing.Size(51, 21)
        Me.NumericUpDown3.TabIndex = 19
        '
        'NumericUpDown7
        '
        Me.NumericUpDown7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown7.Location = New System.Drawing.Point(97, 125)
        Me.NumericUpDown7.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown7.Name = "NumericUpDown7"
        Me.NumericUpDown7.Size = New System.Drawing.Size(51, 21)
        Me.NumericUpDown7.TabIndex = 25
        '
        'Puntos10TextBox
        '
        Me.Puntos10TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralAntiguedadBindingSource, "Puntos10", True))
        Me.Puntos10TextBox.Location = New System.Drawing.Point(279, 125)
        Me.Puntos10TextBox.MaxLength = 3
        Me.Puntos10TextBox.Name = "Puntos10TextBox"
        Me.Puntos10TextBox.Size = New System.Drawing.Size(100, 21)
        Me.Puntos10TextBox.TabIndex = 27
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox3.Location = New System.Drawing.Point(293, 307)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(263, 19)
        Me.CheckBox3.TabIndex = 50
        Me.CheckBox3.Text = "Instalación De Filtro De Solo Internet"
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Black
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(24, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(190, 20)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Servicios de Televisión"
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.Location = New System.Drawing.Point(294, 281)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(232, 19)
        Me.CheckBox2.TabIndex = 49
        Me.CheckBox2.Text = "Retiro De Filtro De Solo Internet"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'Label35
        '
        Me.Label35.BackColor = System.Drawing.Color.Black
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.Color.White
        Me.Label35.Location = New System.Drawing.Point(285, 258)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(339, 20)
        Me.Label35.TabIndex = 48
        Me.Label35.Text = "Generar Orden De Cambio De Clientes"
        '
        'Panel5
        '
        Me.Panel5.Location = New System.Drawing.Point(761, 69)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(72, 100)
        Me.Panel5.TabIndex = 51
        '
        'ImportePorExtrasTextBox
        '
        Me.ImportePorExtrasTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "ImportePorExtras", True))
        Me.ImportePorExtrasTextBox.Location = New System.Drawing.Point(593, 55)
        Me.ImportePorExtrasTextBox.MaxLength = 10
        Me.ImportePorExtrasTextBox.Name = "ImportePorExtrasTextBox"
        Me.ImportePorExtrasTextBox.Size = New System.Drawing.Size(100, 21)
        Me.ImportePorExtrasTextBox.TabIndex = 10
        Me.ImportePorExtrasTextBox.Visible = False
        '
        'ConGeneralBindingSource
        '
        Me.ConGeneralBindingSource.AllowNew = False
        Me.ConGeneralBindingSource.DataMember = "ConGeneral"
        Me.ConGeneralBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Carga_trab_insLabel)
        Me.GroupBox2.Controls.Add(Me.Carga_trab_insTextBox)
        Me.GroupBox2.Controls.Add(CargaDeTrabajoLabel)
        Me.GroupBox2.Controls.Add(Me.CargaDeTrabajoTextBox)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(290, 85)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(302, 84)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Carga de trabajo"
        '
        'Carga_trab_insTextBox
        '
        Me.Carga_trab_insTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "carga_trab_ins", True))
        Me.Carga_trab_insTextBox.Location = New System.Drawing.Point(104, 48)
        Me.Carga_trab_insTextBox.MaxLength = 2
        Me.Carga_trab_insTextBox.Name = "Carga_trab_insTextBox"
        Me.Carga_trab_insTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Carga_trab_insTextBox.TabIndex = 14
        '
        'CargaDeTrabajoTextBox
        '
        Me.CargaDeTrabajoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "CargaDeTrabajo", True))
        Me.CargaDeTrabajoTextBox.Location = New System.Drawing.Point(104, 21)
        Me.CargaDeTrabajoTextBox.MaxLength = 2
        Me.CargaDeTrabajoTextBox.Name = "CargaDeTrabajoTextBox"
        Me.CargaDeTrabajoTextBox.Size = New System.Drawing.Size(100, 21)
        Me.CargaDeTrabajoTextBox.TabIndex = 13
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.SystemColors.Control
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(694, 543)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(136, 36)
        Me.Button3.TabIndex = 20
        Me.Button3.Text = "&GUARDAR"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(CostoExtLabel)
        Me.GroupBox3.Controls.Add(Me.CostoExtTextBox)
        Me.GroupBox3.Controls.Add(NumeroExtLabel)
        Me.GroupBox3.Controls.Add(Me.NumeroExtTextBox)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(314, 348)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(295, 83)
        Me.GroupBox3.TabIndex = 11
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Hoteles"
        Me.GroupBox3.Visible = False
        '
        'CostoExtTextBox
        '
        Me.CostoExtTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "CostoExt", True))
        Me.CostoExtTextBox.Location = New System.Drawing.Point(183, 47)
        Me.CostoExtTextBox.MaxLength = 10
        Me.CostoExtTextBox.Name = "CostoExtTextBox"
        Me.CostoExtTextBox.Size = New System.Drawing.Size(100, 21)
        Me.CostoExtTextBox.TabIndex = 12
        Me.CostoExtTextBox.Visible = False
        '
        'NumeroExtTextBox
        '
        Me.NumeroExtTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "NumeroExt", True))
        Me.NumeroExtTextBox.Location = New System.Drawing.Point(183, 18)
        Me.NumeroExtTextBox.MaxLength = 3
        Me.NumeroExtTextBox.Name = "NumeroExtTextBox"
        Me.NumeroExtTextBox.Size = New System.Drawing.Size(100, 21)
        Me.NumeroExtTextBox.TabIndex = 11
        Me.NumeroExtTextBox.Visible = False
        '
        'ImpresoraOrdenesTextBox
        '
        Me.ImpresoraOrdenesTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "ImpresoraOrdenes", True))
        Me.ImpresoraOrdenesTextBox.Location = New System.Drawing.Point(398, 191)
        Me.ImpresoraOrdenesTextBox.MaxLength = 150
        Me.ImpresoraOrdenesTextBox.Name = "ImpresoraOrdenesTextBox"
        Me.ImpresoraOrdenesTextBox.Size = New System.Drawing.Size(345, 21)
        Me.ImpresoraOrdenesTextBox.TabIndex = 15
        '
        'Consulta_cobrodepositoBindingSource
        '
        Me.Consulta_cobrodepositoBindingSource.DataMember = "Consulta_cobrodeposito"
        Me.Consulta_cobrodepositoBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.Transparent
        Me.Panel6.Controls.Add(Me.RadioButton2)
        Me.Panel6.Controls.Add(Me.RadioButton1)
        Me.Panel6.Controls.Add(Si_se_generaLabel)
        Me.Panel6.Controls.Add(Me.Si_se_generaCheckBox)
        Me.Panel6.Controls.Add(Me.Button15)
        Me.Panel6.Controls.Add(Me.Label36)
        Me.Panel6.Controls.Add(Me.TextBox2)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel6.Location = New System.Drawing.Point(0, 0)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(865, 572)
        Me.Panel6.TabIndex = 52
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.Location = New System.Drawing.Point(462, 145)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(80, 19)
        Me.RadioButton2.TabIndex = 55
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Mensual"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.Location = New System.Drawing.Point(341, 145)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(64, 19)
        Me.RadioButton1.TabIndex = 54
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Diaria"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'Si_se_generaCheckBox
        '
        Me.Si_se_generaCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.COnsultaGralfacturaglobalBindingSource, "si_se_genera", True))
        Me.Si_se_generaCheckBox.Location = New System.Drawing.Point(256, 94)
        Me.Si_se_generaCheckBox.Name = "Si_se_generaCheckBox"
        Me.Si_se_generaCheckBox.Size = New System.Drawing.Size(51, 24)
        Me.Si_se_generaCheckBox.TabIndex = 53
        '
        'Button15
        '
        Me.Button15.BackColor = System.Drawing.SystemColors.Control
        Me.Button15.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button15.ForeColor = System.Drawing.Color.Black
        Me.Button15.Location = New System.Drawing.Point(694, 543)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(136, 36)
        Me.Button15.TabIndex = 50
        Me.Button15.Text = "&GUARDAR"
        Me.Button15.UseVisualStyleBackColor = False
        '
        'Label36
        '
        Me.Label36.BackColor = System.Drawing.Color.Black
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Location = New System.Drawing.Point(42, 27)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(281, 20)
        Me.Label36.TabIndex = 49
        Me.Label36.Text = "Generales de Facturas Globales."
        '
        'TextBox2
        '
        Me.TextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.COnsultaGralfacturaglobalBindingSource, "Tipo", True))
        Me.TextBox2.Location = New System.Drawing.Point(595, 145)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(112, 21)
        Me.TextBox2.TabIndex = 54
        '
        'ConGeneralAlertaBindingSource
        '
        Me.ConGeneralAlertaBindingSource.DataMember = "ConGeneralAlerta"
        Me.ConGeneralAlertaBindingSource.DataSource = Me.DataSetLidia2
        '
        'DataSetLidia2
        '
        Me.DataSetLidia2.DataSetName = "DataSetLidia2"
        Me.DataSetLidia2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Panel12
        '
        Me.Panel12.BackColor = System.Drawing.Color.Transparent
        Me.Panel12.Controls.Add(Me.ReferenciaTextBox1)
        Me.Panel12.Controls.Add(Me.ModuloTextBox1)
        Me.Panel12.Controls.Add(Label48)
        Me.Panel12.Controls.Add(Label50)
        Me.Panel12.Controls.Add(Me.Button31)
        Me.Panel12.Controls.Add(Me.Label51)
        Me.Panel12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel12.Location = New System.Drawing.Point(0, 0)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(865, 572)
        Me.Panel12.TabIndex = 71
        '
        'ReferenciaTextBox1
        '
        Me.ReferenciaTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_HsbcBindingSource, "Referencia", True))
        Me.ReferenciaTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ReferenciaTextBox1.Location = New System.Drawing.Point(425, 126)
        Me.ReferenciaTextBox1.MaxLength = 50
        Me.ReferenciaTextBox1.Name = "ReferenciaTextBox1"
        Me.ReferenciaTextBox1.Size = New System.Drawing.Size(178, 21)
        Me.ReferenciaTextBox1.TabIndex = 22
        '
        'CONSULTA_General_HsbcBindingSource
        '
        Me.CONSULTA_General_HsbcBindingSource.DataMember = "CONSULTA_General_Hsbc"
        Me.CONSULTA_General_HsbcBindingSource.DataSource = Me.DataSetyahve
        '
        'DataSetyahve
        '
        Me.DataSetyahve.DataSetName = "DataSetyahve"
        Me.DataSetyahve.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ModuloTextBox1
        '
        Me.ModuloTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_HsbcBindingSource, "Modulo", True))
        Me.ModuloTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ModuloTextBox1.Location = New System.Drawing.Point(425, 160)
        Me.ModuloTextBox1.Name = "ModuloTextBox1"
        Me.ModuloTextBox1.Size = New System.Drawing.Size(62, 21)
        Me.ModuloTextBox1.TabIndex = 24
        '
        'Button31
        '
        Me.Button31.BackColor = System.Drawing.SystemColors.Control
        Me.Button31.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button31.ForeColor = System.Drawing.Color.Black
        Me.Button31.Location = New System.Drawing.Point(694, 543)
        Me.Button31.Name = "Button31"
        Me.Button31.Size = New System.Drawing.Size(136, 36)
        Me.Button31.TabIndex = 17
        Me.Button31.Text = "&GUARDAR"
        Me.Button31.UseVisualStyleBackColor = False
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.BackColor = System.Drawing.Color.Black
        Me.Label51.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label51.ForeColor = System.Drawing.Color.White
        Me.Label51.Location = New System.Drawing.Point(62, 40)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(248, 20)
        Me.Label51.TabIndex = 15
        Me.Label51.Text = "Datos de la Referencia HSBC"
        '
        'Panel11
        '
        Me.Panel11.BackColor = System.Drawing.Color.Transparent
        Me.Panel11.Controls.Add(ReferenciaLabel)
        Me.Panel11.Controls.Add(Me.ReferenciaTextBox)
        Me.Panel11.Controls.Add(ConstanteLabel)
        Me.Panel11.Controls.Add(Me.ConstanteTextBox)
        Me.Panel11.Controls.Add(ModuloLabel)
        Me.Panel11.Controls.Add(Me.ModuloTextBox)
        Me.Panel11.Controls.Add(Me.Button30)
        Me.Panel11.Controls.Add(Me.Label62)
        Me.Panel11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel11.Location = New System.Drawing.Point(0, 0)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(865, 572)
        Me.Panel11.TabIndex = 70
        '
        'ReferenciaTextBox
        '
        Me.ReferenciaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_SantanderBindingSource, "Referencia", True))
        Me.ReferenciaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ReferenciaTextBox.Location = New System.Drawing.Point(375, 140)
        Me.ReferenciaTextBox.MaxLength = 50
        Me.ReferenciaTextBox.Name = "ReferenciaTextBox"
        Me.ReferenciaTextBox.Size = New System.Drawing.Size(226, 21)
        Me.ReferenciaTextBox.TabIndex = 18
        '
        'CONSULTA_General_SantanderBindingSource
        '
        Me.CONSULTA_General_SantanderBindingSource.DataMember = "CONSULTA_General_Santander"
        Me.CONSULTA_General_SantanderBindingSource.DataSource = Me.DataSetyahve
        '
        'ConstanteTextBox
        '
        Me.ConstanteTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_SantanderBindingSource, "Constante", True))
        Me.ConstanteTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConstanteTextBox.Location = New System.Drawing.Point(375, 168)
        Me.ConstanteTextBox.Name = "ConstanteTextBox"
        Me.ConstanteTextBox.Size = New System.Drawing.Size(74, 21)
        Me.ConstanteTextBox.TabIndex = 20
        '
        'ModuloTextBox
        '
        Me.ModuloTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_SantanderBindingSource, "Modulo", True))
        Me.ModuloTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ModuloTextBox.Location = New System.Drawing.Point(375, 196)
        Me.ModuloTextBox.Name = "ModuloTextBox"
        Me.ModuloTextBox.Size = New System.Drawing.Size(74, 21)
        Me.ModuloTextBox.TabIndex = 22
        '
        'Button30
        '
        Me.Button30.BackColor = System.Drawing.SystemColors.Control
        Me.Button30.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button30.ForeColor = System.Drawing.Color.Black
        Me.Button30.Location = New System.Drawing.Point(694, 543)
        Me.Button30.Name = "Button30"
        Me.Button30.Size = New System.Drawing.Size(136, 36)
        Me.Button30.TabIndex = 17
        Me.Button30.Text = "&GUARDAR"
        Me.Button30.UseVisualStyleBackColor = False
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.BackColor = System.Drawing.Color.Black
        Me.Label62.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label62.ForeColor = System.Drawing.Color.White
        Me.Label62.Location = New System.Drawing.Point(57, 37)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(283, 20)
        Me.Label62.TabIndex = 15
        Me.Label62.Text = "Datos de la Referencia Santander"
        '
        'PanelDatosXml
        '
        Me.PanelDatosXml.BackColor = System.Drawing.Color.Transparent
        Me.PanelDatosXml.Controls.Add(Me.TxtCdClvAdic)
        Me.PanelDatosXml.Controls.Add(Me.TxtEdoClvAdic)
        Me.PanelDatosXml.Controls.Add(LblCdClvAdic)
        Me.PanelDatosXml.Controls.Add(LblEdoClvAdic)
        Me.PanelDatosXml.Controls.Add(Me.Label54)
        Me.PanelDatosXml.Controls.Add(Me.TextBoxfolio)
        Me.PanelDatosXml.Controls.Add(Me.TextBoxserie)
        Me.PanelDatosXml.Controls.Add(Label53)
        Me.PanelDatosXml.Controls.Add(Label52)
        Me.PanelDatosXml.Controls.Add(Me.Label49)
        Me.PanelDatosXml.Controls.Add(Me.Nom_ArchivoTextBox)
        Me.PanelDatosXml.Controls.Add(Me.Fecha_InicioDateTimePicker)
        Me.PanelDatosXml.Controls.Add(Fecha_FacturasLabel)
        Me.PanelDatosXml.Controls.Add(Me.Fecha_FacturasDateTimePicker)
        Me.PanelDatosXml.Controls.Add(Periodo_inicialLabel)
        Me.PanelDatosXml.Controls.Add(Me.Periodo_inicialTextBox)
        Me.PanelDatosXml.Controls.Add(Me.Periodo_finalTextBox)
        Me.PanelDatosXml.Controls.Add(Me.Numero_InicioTextBox)
        Me.PanelDatosXml.Controls.Add(Me.FormatoTextBox)
        Me.PanelDatosXml.Controls.Add(Me.RutaTextBox)
        Me.PanelDatosXml.Controls.Add(Me.Numero_LocalTextBox)
        Me.PanelDatosXml.Controls.Add(Me.Pais_LocalTextBox)
        Me.PanelDatosXml.Controls.Add(Me.Clave044TextBox)
        Me.PanelDatosXml.Controls.Add(Me.Clave045TextBox)
        Me.PanelDatosXml.Controls.Add(Me.Clave01800TextBox)
        Me.PanelDatosXml.Controls.Add(Me.Clave01900TextBox)
        Me.PanelDatosXml.Controls.Add(Label46)
        Me.PanelDatosXml.Controls.Add(Nom_ArchivoLabel)
        Me.PanelDatosXml.Controls.Add(Fecha_InicioLabel)
        Me.PanelDatosXml.Controls.Add(Numero_InicioLabel)
        Me.PanelDatosXml.Controls.Add(FormatoLabel)
        Me.PanelDatosXml.Controls.Add(RutaLabel)
        Me.PanelDatosXml.Controls.Add(Numero_LocalLabel)
        Me.PanelDatosXml.Controls.Add(Pais_LocalLabel)
        Me.PanelDatosXml.Controls.Add(Clave044Label)
        Me.PanelDatosXml.Controls.Add(Clave045Label)
        Me.PanelDatosXml.Controls.Add(Clave01800Label)
        Me.PanelDatosXml.Controls.Add(Clave01900Label)
        Me.PanelDatosXml.Controls.Add(Me.Button28)
        Me.PanelDatosXml.Controls.Add(Me.Label47)
        Me.PanelDatosXml.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelDatosXml.Location = New System.Drawing.Point(0, 0)
        Me.PanelDatosXml.Name = "PanelDatosXml"
        Me.PanelDatosXml.Size = New System.Drawing.Size(865, 572)
        Me.PanelDatosXml.TabIndex = 69
        '
        'TxtCdClvAdic
        '
        Me.TxtCdClvAdic.Location = New System.Drawing.Point(723, 348)
        Me.TxtCdClvAdic.MaxLength = 2
        Me.TxtCdClvAdic.Name = "TxtCdClvAdic"
        Me.TxtCdClvAdic.Size = New System.Drawing.Size(100, 21)
        Me.TxtCdClvAdic.TabIndex = 80
        '
        'TxtEdoClvAdic
        '
        Me.TxtEdoClvAdic.Location = New System.Drawing.Point(723, 321)
        Me.TxtEdoClvAdic.MaxLength = 2
        Me.TxtEdoClvAdic.Name = "TxtEdoClvAdic"
        Me.TxtEdoClvAdic.Size = New System.Drawing.Size(100, 21)
        Me.TxtEdoClvAdic.TabIndex = 79
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.BackColor = System.Drawing.Color.Black
        Me.Label54.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.ForeColor = System.Drawing.Color.White
        Me.Label54.Location = New System.Drawing.Point(501, 294)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(131, 20)
        Me.Label54.TabIndex = 76
        Me.Label54.Text = "Clave Adicional"
        '
        'TextBoxfolio
        '
        Me.TextBoxfolio.Location = New System.Drawing.Point(723, 255)
        Me.TextBoxfolio.MaxLength = 15
        Me.TextBoxfolio.Name = "TextBoxfolio"
        Me.TextBoxfolio.Size = New System.Drawing.Size(100, 21)
        Me.TextBoxfolio.TabIndex = 75
        '
        'TextBoxserie
        '
        Me.TextBoxserie.Location = New System.Drawing.Point(723, 229)
        Me.TextBoxserie.MaxLength = 15
        Me.TextBoxserie.Name = "TextBoxserie"
        Me.TextBoxserie.Size = New System.Drawing.Size(100, 21)
        Me.TextBoxserie.TabIndex = 74
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.BackColor = System.Drawing.Color.Black
        Me.Label49.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.ForeColor = System.Drawing.Color.White
        Me.Label49.Location = New System.Drawing.Point(498, 188)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(292, 20)
        Me.Label49.TabIndex = 71
        Me.Label49.Text = "Serie y Folio de Estados de Cuenta"
        '
        'Nom_ArchivoTextBox
        '
        Me.Nom_ArchivoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Nom_Archivo", True))
        Me.Nom_ArchivoTextBox.Location = New System.Drawing.Point(244, 107)
        Me.Nom_ArchivoTextBox.MaxLength = 250
        Me.Nom_ArchivoTextBox.Name = "Nom_ArchivoTextBox"
        Me.Nom_ArchivoTextBox.Size = New System.Drawing.Size(200, 21)
        Me.Nom_ArchivoTextBox.TabIndex = 44
        '
        'CONSULTA_General_XmlBindingSource
        '
        Me.CONSULTA_General_XmlBindingSource.DataMember = "CONSULTA_General_Xml"
        Me.CONSULTA_General_XmlBindingSource.DataSource = Me.DataSetyahve
        '
        'Fecha_InicioDateTimePicker
        '
        Me.Fecha_InicioDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.CONSULTA_General_XmlBindingSource, "Fecha_Inicio", True))
        Me.Fecha_InicioDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Fecha_InicioDateTimePicker.Location = New System.Drawing.Point(244, 133)
        Me.Fecha_InicioDateTimePicker.Name = "Fecha_InicioDateTimePicker"
        Me.Fecha_InicioDateTimePicker.Size = New System.Drawing.Size(100, 21)
        Me.Fecha_InicioDateTimePicker.TabIndex = 46
        '
        'Fecha_FacturasDateTimePicker
        '
        Me.Fecha_FacturasDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.CONSULTA_General_XmlBindingSource, "Fecha_Facturas", True))
        Me.Fecha_FacturasDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Fecha_FacturasDateTimePicker.Location = New System.Drawing.Point(244, 159)
        Me.Fecha_FacturasDateTimePicker.Name = "Fecha_FacturasDateTimePicker"
        Me.Fecha_FacturasDateTimePicker.Size = New System.Drawing.Size(100, 21)
        Me.Fecha_FacturasDateTimePicker.TabIndex = 48
        '
        'Periodo_inicialTextBox
        '
        Me.Periodo_inicialTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Periodo_inicial", True))
        Me.Periodo_inicialTextBox.Location = New System.Drawing.Point(244, 185)
        Me.Periodo_inicialTextBox.Name = "Periodo_inicialTextBox"
        Me.Periodo_inicialTextBox.Size = New System.Drawing.Size(50, 21)
        Me.Periodo_inicialTextBox.TabIndex = 50
        '
        'Periodo_finalTextBox
        '
        Me.Periodo_finalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Periodo_final", True))
        Me.Periodo_finalTextBox.Location = New System.Drawing.Point(338, 185)
        Me.Periodo_finalTextBox.Name = "Periodo_finalTextBox"
        Me.Periodo_finalTextBox.Size = New System.Drawing.Size(49, 21)
        Me.Periodo_finalTextBox.TabIndex = 52
        '
        'Numero_InicioTextBox
        '
        Me.Numero_InicioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Numero_Inicio", True))
        Me.Numero_InicioTextBox.Location = New System.Drawing.Point(244, 212)
        Me.Numero_InicioTextBox.Name = "Numero_InicioTextBox"
        Me.Numero_InicioTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Numero_InicioTextBox.TabIndex = 54
        '
        'FormatoTextBox
        '
        Me.FormatoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Formato", True))
        Me.FormatoTextBox.Location = New System.Drawing.Point(244, 238)
        Me.FormatoTextBox.Name = "FormatoTextBox"
        Me.FormatoTextBox.Size = New System.Drawing.Size(100, 21)
        Me.FormatoTextBox.TabIndex = 56
        '
        'RutaTextBox
        '
        Me.RutaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Ruta", True))
        Me.RutaTextBox.Location = New System.Drawing.Point(244, 264)
        Me.RutaTextBox.MaxLength = 250
        Me.RutaTextBox.Name = "RutaTextBox"
        Me.RutaTextBox.Size = New System.Drawing.Size(143, 21)
        Me.RutaTextBox.TabIndex = 58
        '
        'Numero_LocalTextBox
        '
        Me.Numero_LocalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Numero_Local", True))
        Me.Numero_LocalTextBox.Location = New System.Drawing.Point(244, 290)
        Me.Numero_LocalTextBox.MaxLength = 50
        Me.Numero_LocalTextBox.Name = "Numero_LocalTextBox"
        Me.Numero_LocalTextBox.Size = New System.Drawing.Size(143, 21)
        Me.Numero_LocalTextBox.TabIndex = 60
        '
        'Pais_LocalTextBox
        '
        Me.Pais_LocalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Pais_Local", True))
        Me.Pais_LocalTextBox.Location = New System.Drawing.Point(244, 316)
        Me.Pais_LocalTextBox.MaxLength = 50
        Me.Pais_LocalTextBox.Name = "Pais_LocalTextBox"
        Me.Pais_LocalTextBox.Size = New System.Drawing.Size(143, 21)
        Me.Pais_LocalTextBox.TabIndex = 62
        '
        'Clave044TextBox
        '
        Me.Clave044TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Clave044", True))
        Me.Clave044TextBox.Location = New System.Drawing.Point(244, 342)
        Me.Clave044TextBox.MaxLength = 50
        Me.Clave044TextBox.Name = "Clave044TextBox"
        Me.Clave044TextBox.Size = New System.Drawing.Size(143, 21)
        Me.Clave044TextBox.TabIndex = 64
        '
        'Clave045TextBox
        '
        Me.Clave045TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Clave045", True))
        Me.Clave045TextBox.Location = New System.Drawing.Point(244, 368)
        Me.Clave045TextBox.MaxLength = 50
        Me.Clave045TextBox.Name = "Clave045TextBox"
        Me.Clave045TextBox.Size = New System.Drawing.Size(143, 21)
        Me.Clave045TextBox.TabIndex = 66
        '
        'Clave01800TextBox
        '
        Me.Clave01800TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Clave01800", True))
        Me.Clave01800TextBox.Location = New System.Drawing.Point(244, 394)
        Me.Clave01800TextBox.MaxLength = 50
        Me.Clave01800TextBox.Name = "Clave01800TextBox"
        Me.Clave01800TextBox.Size = New System.Drawing.Size(143, 21)
        Me.Clave01800TextBox.TabIndex = 68
        '
        'Clave01900TextBox
        '
        Me.Clave01900TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_General_XmlBindingSource, "Clave01900", True))
        Me.Clave01900TextBox.Location = New System.Drawing.Point(244, 420)
        Me.Clave01900TextBox.MaxLength = 50
        Me.Clave01900TextBox.Name = "Clave01900TextBox"
        Me.Clave01900TextBox.Size = New System.Drawing.Size(143, 21)
        Me.Clave01900TextBox.TabIndex = 70
        '
        'Button28
        '
        Me.Button28.BackColor = System.Drawing.SystemColors.Control
        Me.Button28.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button28.ForeColor = System.Drawing.Color.Black
        Me.Button28.Location = New System.Drawing.Point(694, 543)
        Me.Button28.Name = "Button28"
        Me.Button28.Size = New System.Drawing.Size(136, 36)
        Me.Button28.TabIndex = 17
        Me.Button28.Text = "&GUARDAR"
        Me.Button28.UseVisualStyleBackColor = False
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.BackColor = System.Drawing.Color.Black
        Me.Label47.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.ForeColor = System.Drawing.Color.White
        Me.Label47.Location = New System.Drawing.Point(43, 27)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(83, 20)
        Me.Label47.TabIndex = 15
        Me.Label47.Text = "Telefonía"
        '
        'PanelDatos
        '
        Me.PanelDatos.AutoScroll = True
        Me.PanelDatos.BackColor = System.Drawing.Color.Transparent
        Me.PanelDatos.Controls.Add(Me.Label56)
        Me.PanelDatos.Controls.Add(Me.TextBoxNumeroExt)
        Me.PanelDatos.Controls.Add(Me.Label55)
        Me.PanelDatos.Controls.Add(Me.TextBoxNumeroInt)
        Me.PanelDatos.Controls.Add(Me.Label14)
        Me.PanelDatos.Controls.Add(Me.Label2)
        Me.PanelDatos.Controls.Add(ColoniaLabel)
        Me.PanelDatos.Controls.Add(Me.TextBox1)
        Me.PanelDatos.Controls.Add(Me.CiudadTextBox)
        Me.PanelDatos.Controls.Add(Me.ColoniaTextBox)
        Me.PanelDatos.Controls.Add(Me.Button5)
        Me.PanelDatos.Controls.Add(CiudadLabel)
        Me.PanelDatos.Controls.Add(Me.Id_sucursalTextBox)
        Me.PanelDatos.Controls.Add(DireccionLabel)
        Me.PanelDatos.Controls.Add(Id_sucursalLabel)
        Me.PanelDatos.Controls.Add(Me.RfcTextBox)
        Me.PanelDatos.Controls.Add(TELefonosLabel)
        Me.PanelDatos.Controls.Add(Me.DireccionTextBox)
        Me.PanelDatos.Controls.Add(Me.NombreTextBox)
        Me.PanelDatos.Controls.Add(RfcLabel)
        Me.PanelDatos.Controls.Add(Me.TELefonosTextBox)
        Me.PanelDatos.Controls.Add(NombreLabel)
        Me.PanelDatos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelDatos.Location = New System.Drawing.Point(3, 3)
        Me.PanelDatos.Name = "PanelDatos"
        Me.PanelDatos.Size = New System.Drawing.Size(859, 566)
        Me.PanelDatos.TabIndex = 68
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(162, 273)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(103, 15)
        Me.Label56.TabIndex = 23
        Me.Label56.Text = "Num. Exterior :"
        '
        'TextBoxNumeroExt
        '
        Me.TextBoxNumeroExt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxNumeroExt.Location = New System.Drawing.Point(266, 267)
        Me.TextBoxNumeroExt.MaxLength = 20
        Me.TextBoxNumeroExt.Name = "TextBoxNumeroExt"
        Me.TextBoxNumeroExt.Size = New System.Drawing.Size(204, 21)
        Me.TextBoxNumeroExt.TabIndex = 22
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(161, 300)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(99, 15)
        Me.Label55.TabIndex = 21
        Me.Label55.Text = "Num. Interior :"
        '
        'TextBoxNumeroInt
        '
        Me.TextBoxNumeroInt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxNumeroInt.Location = New System.Drawing.Point(266, 294)
        Me.TextBoxNumeroInt.MaxLength = 20
        Me.TextBoxNumeroInt.Name = "TextBoxNumeroInt"
        Me.TextBoxNumeroInt.Size = New System.Drawing.Size(204, 21)
        Me.TextBoxNumeroInt.TabIndex = 20
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(164, 354)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(100, 15)
        Me.Label14.TabIndex = 19
        Me.Label14.Text = "Código Postal:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Black
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(26, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(182, 20)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Datos de la Empresa "
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConsultaCpGeneBindingSource, "Codigo_Postal", True))
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.TextBox1.Location = New System.Drawing.Point(266, 348)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(204, 21)
        Me.TextBox1.TabIndex = 18
        '
        'CiudadTextBox
        '
        Me.CiudadTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CiudadTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "ciudad", True))
        Me.CiudadTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CiudadTextBox.Location = New System.Drawing.Point(266, 375)
        Me.CiudadTextBox.MaxLength = 180
        Me.CiudadTextBox.Name = "CiudadTextBox"
        Me.CiudadTextBox.Size = New System.Drawing.Size(331, 21)
        Me.CiudadTextBox.TabIndex = 14
        '
        'ColoniaTextBox
        '
        Me.ColoniaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ColoniaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "colonia", True))
        Me.ColoniaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ColoniaTextBox.Location = New System.Drawing.Point(266, 321)
        Me.ColoniaTextBox.MaxLength = 150
        Me.ColoniaTextBox.Name = "ColoniaTextBox"
        Me.ColoniaTextBox.Size = New System.Drawing.Size(331, 21)
        Me.ColoniaTextBox.TabIndex = 13
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.SystemColors.Control
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(693, 506)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 17
        Me.Button5.Text = "&GUARDAR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Id_sucursalTextBox
        '
        Me.Id_sucursalTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Id_sucursalTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Id_sucursalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "id_sucursal", True))
        Me.Id_sucursalTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Id_sucursalTextBox.Location = New System.Drawing.Point(266, 151)
        Me.Id_sucursalTextBox.MaxLength = 2
        Me.Id_sucursalTextBox.Name = "Id_sucursalTextBox"
        Me.Id_sucursalTextBox.ReadOnly = True
        Me.Id_sucursalTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Id_sucursalTextBox.TabIndex = 10
        '
        'RfcTextBox
        '
        Me.RfcTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.RfcTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.RfcTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "rfc", True))
        Me.RfcTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RfcTextBox.Location = New System.Drawing.Point(266, 402)
        Me.RfcTextBox.MaxLength = 20
        Me.RfcTextBox.Name = "RfcTextBox"
        Me.RfcTextBox.Size = New System.Drawing.Size(204, 21)
        Me.RfcTextBox.TabIndex = 15
        '
        'DireccionTextBox
        '
        Me.DireccionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DireccionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "direccion", True))
        Me.DireccionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DireccionTextBox.Location = New System.Drawing.Point(266, 204)
        Me.DireccionTextBox.MaxLength = 250
        Me.DireccionTextBox.Multiline = True
        Me.DireccionTextBox.Name = "DireccionTextBox"
        Me.DireccionTextBox.Size = New System.Drawing.Size(488, 57)
        Me.DireccionTextBox.TabIndex = 12
        '
        'NombreTextBox
        '
        Me.NombreTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "nombre", True))
        Me.NombreTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreTextBox.Location = New System.Drawing.Point(266, 178)
        Me.NombreTextBox.MaxLength = 150
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.Size = New System.Drawing.Size(488, 21)
        Me.NombreTextBox.TabIndex = 11
        '
        'TELefonosTextBox
        '
        Me.TELefonosTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TELefonosTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralBindingSource, "TELefonos", True))
        Me.TELefonosTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TELefonosTextBox.Location = New System.Drawing.Point(266, 429)
        Me.TELefonosTextBox.MaxLength = 50
        Me.TELefonosTextBox.Name = "TELefonosTextBox"
        Me.TELefonosTextBox.Size = New System.Drawing.Size(204, 21)
        Me.TELefonosTextBox.TabIndex = 16
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.Transparent
        Me.Panel8.Controls.Add(Me.TreeView1)
        Me.Panel8.Controls.Add(Me.TextBox6)
        Me.Panel8.Controls.Add(Me.Button21)
        Me.Panel8.Controls.Add(Me.Button20)
        Me.Panel8.Controls.Add(Label39)
        Me.Panel8.Controls.Add(Me.TextBox5)
        Me.Panel8.Controls.Add(Label41)
        Me.Panel8.Controls.Add(Me.TextBox4)
        Me.Panel8.Controls.Add(Label38)
        Me.Panel8.Controls.Add(Me.TextBox3)
        Me.Panel8.Controls.Add(Me.CheckBox4)
        Me.Panel8.Controls.Add(Me.Button19)
        Me.Panel8.Controls.Add(Me.Label40)
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel8.Location = New System.Drawing.Point(0, 0)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(865, 572)
        Me.Panel8.TabIndex = 54
        '
        'TreeView1
        '
        Me.TreeView1.Location = New System.Drawing.Point(415, 291)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(312, 127)
        Me.TreeView1.TabIndex = 62
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(505, 332)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(100, 21)
        Me.TextBox6.TabIndex = 74
        '
        'Button21
        '
        Me.Button21.BackColor = System.Drawing.SystemColors.Control
        Me.Button21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button21.ForeColor = System.Drawing.Color.Black
        Me.Button21.Location = New System.Drawing.Point(212, 370)
        Me.Button21.Name = "Button21"
        Me.Button21.Size = New System.Drawing.Size(176, 26)
        Me.Button21.TabIndex = 64
        Me.Button21.Text = "&Quitar Ip de la Lista"
        Me.Button21.UseVisualStyleBackColor = False
        '
        'Button20
        '
        Me.Button20.BackColor = System.Drawing.SystemColors.Control
        Me.Button20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button20.ForeColor = System.Drawing.Color.Black
        Me.Button20.Location = New System.Drawing.Point(212, 332)
        Me.Button20.Name = "Button20"
        Me.Button20.Size = New System.Drawing.Size(174, 26)
        Me.Button20.TabIndex = 63
        Me.Button20.Text = "&Agregar Ip a la Lista"
        Me.Button20.UseVisualStyleBackColor = False
        '
        'TextBox5
        '
        Me.TextBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox5.Location = New System.Drawing.Point(210, 293)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox5.Size = New System.Drawing.Size(110, 21)
        Me.TextBox5.TabIndex = 61
        '
        'TextBox4
        '
        Me.TextBox4.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralAlertaBindingSource, "Monto", True))
        Me.TextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox4.Location = New System.Drawing.Point(205, 120)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox4.Size = New System.Drawing.Size(110, 21)
        Me.TextBox4.TabIndex = 59
        '
        'TextBox3
        '
        Me.TextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralAlertaBindingSource, "Mensaje", True))
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(205, 176)
        Me.TextBox3.Multiline = True
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox3.Size = New System.Drawing.Size(517, 88)
        Me.TextBox3.TabIndex = 55
        '
        'CheckBox4
        '
        Me.CheckBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox4.Location = New System.Drawing.Point(409, 110)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(161, 24)
        Me.CheckBox4.TabIndex = 57
        Me.CheckBox4.Text = "Activar Alerta"
        '
        'Button19
        '
        Me.Button19.BackColor = System.Drawing.SystemColors.Control
        Me.Button19.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button19.ForeColor = System.Drawing.Color.Black
        Me.Button19.Location = New System.Drawing.Point(694, 543)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(136, 36)
        Me.Button19.TabIndex = 54
        Me.Button19.Text = "&GUARDAR"
        Me.Button19.UseVisualStyleBackColor = False
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.BackColor = System.Drawing.Color.Black
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.ForeColor = System.Drawing.Color.White
        Me.Label40.Location = New System.Drawing.Point(46, 26)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(66, 20)
        Me.Label40.TabIndex = 52
        Me.Label40.Text = "Alertas"
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.Transparent
        Me.Panel7.Controls.Add(MensajeLabel)
        Me.Panel7.Controls.Add(Me.MensajeTextBox)
        Me.Panel7.Controls.Add(ActivoLabel)
        Me.Panel7.Controls.Add(Me.ActivoCheckBox)
        Me.Panel7.Controls.Add(Me.Button16)
        Me.Panel7.Controls.Add(Me.Label37)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel7.Location = New System.Drawing.Point(0, 0)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(865, 572)
        Me.Panel7.TabIndex = 53
        '
        'MensajeTextBox
        '
        Me.MensajeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralMsjTicketsBindingSource, "Mensaje", True))
        Me.MensajeTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MensajeTextBox.Location = New System.Drawing.Point(218, 138)
        Me.MensajeTextBox.Multiline = True
        Me.MensajeTextBox.Name = "MensajeTextBox"
        Me.MensajeTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.MensajeTextBox.Size = New System.Drawing.Size(517, 153)
        Me.MensajeTextBox.TabIndex = 55
        '
        'ConGeneralMsjTicketsBindingSource
        '
        Me.ConGeneralMsjTicketsBindingSource.DataMember = "ConGeneralMsjTickets"
        '
        'ActivoCheckBox
        '
        Me.ActivoCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.ConGeneralMsjTicketsBindingSource, "Activo", True))
        Me.ActivoCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ActivoCheckBox.Location = New System.Drawing.Point(263, 295)
        Me.ActivoCheckBox.Name = "ActivoCheckBox"
        Me.ActivoCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.ActivoCheckBox.TabIndex = 57
        '
        'Button16
        '
        Me.Button16.BackColor = System.Drawing.SystemColors.Control
        Me.Button16.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button16.ForeColor = System.Drawing.Color.Black
        Me.Button16.Location = New System.Drawing.Point(694, 543)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(136, 36)
        Me.Button16.TabIndex = 54
        Me.Button16.Text = "&GUARDAR"
        Me.Button16.UseVisualStyleBackColor = False
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.BackColor = System.Drawing.Color.Black
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.ForeColor = System.Drawing.Color.White
        Me.Label37.Location = New System.Drawing.Point(55, 32)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(163, 20)
        Me.Label37.TabIndex = 52
        Me.Label37.Text = "Mensaje en Tickets"
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.Transparent
        Me.Panel9.Controls.Add(CablemodemsLabel)
        Me.Panel9.Controls.Add(Me.CablemodemsCheckBox)
        Me.Panel9.Controls.Add(PregcablLabel)
        Me.Panel9.Controls.Add(Me.PregcablTextBox)
        Me.Panel9.Controls.Add(DecodersLabel)
        Me.Panel9.Controls.Add(Me.DecodersCheckBox)
        Me.Panel9.Controls.Add(PregdecoLabel)
        Me.Panel9.Controls.Add(Me.PregdecoTextBox)
        Me.Panel9.Controls.Add(Me.Button23)
        Me.Panel9.Controls.Add(Me.Label42)
        Me.Panel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel9.Location = New System.Drawing.Point(0, 0)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(865, 572)
        Me.Panel9.TabIndex = 65
        '
        'CablemodemsCheckBox
        '
        Me.CablemodemsCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.Consulta_cobrodepositobuenoBindingSource, "cablemodems", True))
        Me.CablemodemsCheckBox.Location = New System.Drawing.Point(333, 123)
        Me.CablemodemsCheckBox.Name = "CablemodemsCheckBox"
        Me.CablemodemsCheckBox.Size = New System.Drawing.Size(23, 24)
        Me.CablemodemsCheckBox.TabIndex = 74
        '
        'Consulta_cobrodepositobuenoBindingSource
        '
        Me.Consulta_cobrodepositobuenoBindingSource.DataMember = "Consulta_cobrodepositobueno"
        Me.Consulta_cobrodepositobuenoBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'PregcablTextBox
        '
        Me.PregcablTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_cobrodepositobuenoBindingSource, "pregcabl", True))
        Me.PregcablTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PregcablTextBox.Location = New System.Drawing.Point(669, 158)
        Me.PregcablTextBox.Name = "PregcablTextBox"
        Me.PregcablTextBox.Size = New System.Drawing.Size(104, 21)
        Me.PregcablTextBox.TabIndex = 76
        '
        'DecodersCheckBox
        '
        Me.DecodersCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.Consulta_cobrodepositobuenoBindingSource, "decoders", True))
        Me.DecodersCheckBox.Location = New System.Drawing.Point(333, 159)
        Me.DecodersCheckBox.Name = "DecodersCheckBox"
        Me.DecodersCheckBox.Size = New System.Drawing.Size(18, 24)
        Me.DecodersCheckBox.TabIndex = 78
        '
        'PregdecoTextBox
        '
        Me.PregdecoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_cobrodepositobuenoBindingSource, "pregdeco", True))
        Me.PregdecoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PregdecoTextBox.Location = New System.Drawing.Point(387, 257)
        Me.PregdecoTextBox.Name = "PregdecoTextBox"
        Me.PregdecoTextBox.Size = New System.Drawing.Size(104, 21)
        Me.PregdecoTextBox.TabIndex = 80
        Me.PregdecoTextBox.Visible = False
        '
        'Button23
        '
        Me.Button23.BackColor = System.Drawing.SystemColors.Control
        Me.Button23.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button23.ForeColor = System.Drawing.Color.Black
        Me.Button23.Location = New System.Drawing.Point(694, 543)
        Me.Button23.Name = "Button23"
        Me.Button23.Size = New System.Drawing.Size(136, 36)
        Me.Button23.TabIndex = 72
        Me.Button23.Text = "&GUARDAR"
        Me.Button23.UseVisualStyleBackColor = False
        '
        'Label42
        '
        Me.Label42.BackColor = System.Drawing.Color.Black
        Me.Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.ForeColor = System.Drawing.Color.White
        Me.Label42.Location = New System.Drawing.Point(51, 38)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(287, 20)
        Me.Label42.TabIndex = 66
        Me.Label42.Text = "Se Cobran Deposito de Aparatos:"
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.Color.Transparent
        Me.Panel10.Controls.Add(Me.Label45)
        Me.Panel10.Controls.Add(Me.Button26)
        Me.Panel10.Controls.Add(Me.Button25)
        Me.Panel10.Controls.Add(Me.Label44)
        Me.Panel10.Controls.Add(Me.Label43)
        Me.Panel10.Controls.Add(Me.TextBox8)
        Me.Panel10.Controls.Add(Me.NumericUpDown9)
        Me.Panel10.Controls.Add(Me.Consulta_Rel_PaquetesDigTelDataGridView)
        Me.Panel10.Controls.Add(Me.ConsecutivoTextBox)
        Me.Panel10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel10.Location = New System.Drawing.Point(0, 0)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(865, 572)
        Me.Panel10.TabIndex = 52
        '
        'Label45
        '
        Me.Label45.BackColor = System.Drawing.Color.Black
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.ForeColor = System.Drawing.Color.White
        Me.Label45.Location = New System.Drawing.Point(41, 34)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(339, 20)
        Me.Label45.TabIndex = 52
        Me.Label45.Text = "Precios Paquetes Teléfonia"
        '
        'Button26
        '
        Me.Button26.BackColor = System.Drawing.SystemColors.Control
        Me.Button26.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button26.ForeColor = System.Drawing.Color.Black
        Me.Button26.Location = New System.Drawing.Point(560, 233)
        Me.Button26.Name = "Button26"
        Me.Button26.Size = New System.Drawing.Size(124, 30)
        Me.Button26.TabIndex = 53
        Me.Button26.Text = "&BORRAR"
        Me.Button26.UseVisualStyleBackColor = False
        '
        'Button25
        '
        Me.Button25.BackColor = System.Drawing.SystemColors.Control
        Me.Button25.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button25.ForeColor = System.Drawing.Color.Black
        Me.Button25.Location = New System.Drawing.Point(560, 141)
        Me.Button25.Name = "Button25"
        Me.Button25.Size = New System.Drawing.Size(124, 30)
        Me.Button25.TabIndex = 52
        Me.Button25.Text = "&AGREGAR"
        Me.Button25.UseVisualStyleBackColor = False
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.Location = New System.Drawing.Point(329, 163)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(79, 16)
        Me.Label44.TabIndex = 4
        Me.Label44.Text = "Precio ($):"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(291, 134)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(114, 16)
        Me.Label43.TabIndex = 3
        Me.Label43.Text = "# De Paquetes:"
        '
        'TextBox8
        '
        Me.TextBox8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox8.Location = New System.Drawing.Point(424, 160)
        Me.TextBox8.MaxLength = 15
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(109, 22)
        Me.TextBox8.TabIndex = 2
        '
        'NumericUpDown9
        '
        Me.NumericUpDown9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown9.Location = New System.Drawing.Point(424, 133)
        Me.NumericUpDown9.Minimum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.NumericUpDown9.Name = "NumericUpDown9"
        Me.NumericUpDown9.Size = New System.Drawing.Size(59, 22)
        Me.NumericUpDown9.TabIndex = 1
        Me.NumericUpDown9.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        'Consulta_Rel_PaquetesDigTelDataGridView
        '
        Me.Consulta_Rel_PaquetesDigTelDataGridView.AllowUserToAddRows = False
        Me.Consulta_Rel_PaquetesDigTelDataGridView.AllowUserToDeleteRows = False
        Me.Consulta_Rel_PaquetesDigTelDataGridView.AllowUserToOrderColumns = True
        Me.Consulta_Rel_PaquetesDigTelDataGridView.AutoGenerateColumns = False
        Me.Consulta_Rel_PaquetesDigTelDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DarkOrange
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Consulta_Rel_PaquetesDigTelDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.Consulta_Rel_PaquetesDigTelDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3})
        Me.Consulta_Rel_PaquetesDigTelDataGridView.DataSource = Me.Consulta_Rel_PaquetesDigTelBindingSource
        Me.Consulta_Rel_PaquetesDigTelDataGridView.Location = New System.Drawing.Point(300, 233)
        Me.Consulta_Rel_PaquetesDigTelDataGridView.Name = "Consulta_Rel_PaquetesDigTelDataGridView"
        Me.Consulta_Rel_PaquetesDigTelDataGridView.ReadOnly = True
        Me.Consulta_Rel_PaquetesDigTelDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Consulta_Rel_PaquetesDigTelDataGridView.Size = New System.Drawing.Size(254, 220)
        Me.Consulta_Rel_PaquetesDigTelDataGridView.TabIndex = 0
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "consecutivo"
        Me.DataGridViewTextBoxColumn1.HeaderText = "consecutivo"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "nopaquetes"
        Me.DataGridViewTextBoxColumn2.HeaderText = "# Paquetes"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "precio"
        DataGridViewCellStyle2.Format = "C2"
        DataGridViewCellStyle2.NullValue = "0"
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn3.HeaderText = "Precio"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'Consulta_Rel_PaquetesDigTelBindingSource
        '
        Me.Consulta_Rel_PaquetesDigTelBindingSource.DataMember = "Consulta_Rel_PaquetesDigTel"
        Me.Consulta_Rel_PaquetesDigTelBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'ConsecutivoTextBox
        '
        Me.ConsecutivoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_Rel_PaquetesDigTelBindingSource, "consecutivo", True))
        Me.ConsecutivoTextBox.Location = New System.Drawing.Point(388, 372)
        Me.ConsecutivoTextBox.Name = "ConsecutivoTextBox"
        Me.ConsecutivoTextBox.Size = New System.Drawing.Size(100, 21)
        Me.ConsecutivoTextBox.TabIndex = 54
        '
        'ConGeneralAlertaTableAdapter
        '
        Me.ConGeneralAlertaTableAdapter.ClearBeforeFill = True
        '
        'NueGeneralAlertaBindingSource
        '
        Me.NueGeneralAlertaBindingSource.DataMember = "NueGeneralAlerta"
        Me.NueGeneralAlertaBindingSource.DataSource = Me.DataSetLidia2
        '
        'NueGeneralAlertaTableAdapter
        '
        Me.NueGeneralAlertaTableAdapter.ClearBeforeFill = True
        '
        'Dame_Direcciones_IpBindingSource
        '
        Me.Dame_Direcciones_IpBindingSource.DataMember = "Dame_Direcciones_Ip"
        Me.Dame_Direcciones_IpBindingSource.DataSource = Me.DataSetLidia2
        '
        'Dame_Direcciones_IpTableAdapter
        '
        Me.Dame_Direcciones_IpTableAdapter.ClearBeforeFill = True
        '
        'Consulta_cobrodepositoTableAdapter
        '
        Me.Consulta_cobrodepositoTableAdapter.ClearBeforeFill = True
        '
        'Consulta_cobrodepositobuenoTableAdapter
        '
        Me.Consulta_cobrodepositobuenoTableAdapter.ClearBeforeFill = True
        '
        'Consulta_Rel_PaquetesDigTelTableAdapter
        '
        Me.Consulta_Rel_PaquetesDigTelTableAdapter.ClearBeforeFill = True
        '
        'CONSULTA_General_XmlTableAdapter
        '
        Me.CONSULTA_General_XmlTableAdapter.ClearBeforeFill = True
        '
        'CONSULTA_General_SantanderTableAdapter
        '
        Me.CONSULTA_General_SantanderTableAdapter.ClearBeforeFill = True
        '
        'CONSULTA_General_HsbcTableAdapter
        '
        Me.CONSULTA_General_HsbcTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ciudadTableAdapter1
        '
        Me.Muestra_ciudadTableAdapter1.ClearBeforeFill = True
        '
        'TabPage14
        '
        Me.TabPage14.Controls.Add(Me.Panel12)
        Me.TabPage14.Location = New System.Drawing.Point(4, 84)
        Me.TabPage14.Name = "TabPage14"
        Me.TabPage14.Size = New System.Drawing.Size(865, 572)
        Me.TabPage14.TabIndex = 13
        Me.TabPage14.Text = "Referencia HSBC"
        Me.TabPage14.UseVisualStyleBackColor = True
        '
        'TabPage13
        '
        Me.TabPage13.Controls.Add(Me.Panel11)
        Me.TabPage13.Location = New System.Drawing.Point(4, 84)
        Me.TabPage13.Name = "TabPage13"
        Me.TabPage13.Size = New System.Drawing.Size(865, 572)
        Me.TabPage13.TabIndex = 12
        Me.TabPage13.Text = "Referencia Santander"
        Me.TabPage13.UseVisualStyleBackColor = True
        '
        'TabPage12
        '
        Me.TabPage12.Controls.Add(Me.Panel2)
        Me.TabPage12.Location = New System.Drawing.Point(4, 84)
        Me.TabPage12.Name = "TabPage12"
        Me.TabPage12.Size = New System.Drawing.Size(865, 572)
        Me.TabPage12.TabIndex = 11
        Me.TabPage12.Text = "Impresión"
        Me.TabPage12.UseVisualStyleBackColor = True
        '
        'TabPage11
        '
        Me.TabPage11.Controls.Add(Me.Panel10)
        Me.TabPage11.Location = New System.Drawing.Point(4, 84)
        Me.TabPage11.Name = "TabPage11"
        Me.TabPage11.Size = New System.Drawing.Size(865, 572)
        Me.TabPage11.TabIndex = 10
        Me.TabPage11.Text = "Paquetes Digitales Telefonía"
        Me.TabPage11.UseVisualStyleBackColor = True
        '
        'TabPage10
        '
        Me.TabPage10.Controls.Add(Me.Panel9)
        Me.TabPage10.Location = New System.Drawing.Point(4, 84)
        Me.TabPage10.Name = "TabPage10"
        Me.TabPage10.Size = New System.Drawing.Size(865, 572)
        Me.TabPage10.TabIndex = 9
        Me.TabPage10.Text = "Depósitos"
        Me.TabPage10.UseVisualStyleBackColor = True
        '
        'TabPage9
        '
        Me.TabPage9.Controls.Add(Me.PanelDatosXml)
        Me.TabPage9.Location = New System.Drawing.Point(4, 84)
        Me.TabPage9.Name = "TabPage9"
        Me.TabPage9.Size = New System.Drawing.Size(865, 572)
        Me.TabPage9.TabIndex = 8
        Me.TabPage9.Text = "Telefonía"
        Me.TabPage9.UseVisualStyleBackColor = True
        '
        'TabPage8
        '
        Me.TabPage8.Controls.Add(Me.Panel8)
        Me.TabPage8.Location = New System.Drawing.Point(4, 84)
        Me.TabPage8.Name = "TabPage8"
        Me.TabPage8.Size = New System.Drawing.Size(865, 572)
        Me.TabPage8.TabIndex = 7
        Me.TabPage8.Text = "Alertas"
        Me.TabPage8.UseVisualStyleBackColor = True
        '
        'TabPage7
        '
        Me.TabPage7.Controls.Add(Me.Panel7)
        Me.TabPage7.Location = New System.Drawing.Point(4, 84)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Size = New System.Drawing.Size(865, 572)
        Me.TabPage7.TabIndex = 6
        Me.TabPage7.Text = "Mensaje en Tickets"
        Me.TabPage7.UseVisualStyleBackColor = True
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.Panel6)
        Me.TabPage6.Location = New System.Drawing.Point(4, 84)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(865, 572)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "Facturas Globales"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.Panel4)
        Me.TabPage5.Location = New System.Drawing.Point(4, 84)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(865, 572)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Correo"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.Panel3)
        Me.TabPage4.Location = New System.Drawing.Point(4, 84)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(865, 572)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Plazo Forzoso"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Panel1)
        Me.TabPage3.Location = New System.Drawing.Point(4, 84)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(865, 572)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Periodo de Corte"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.PanelBasico)
        Me.TabPage2.Location = New System.Drawing.Point(4, 84)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(865, 572)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Servicios de Televisión"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage1.Controls.Add(Me.PanelDatos)
        Me.TabPage1.Location = New System.Drawing.Point(4, 84)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(865, 572)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Datos de la Empresa"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Controls.Add(Me.TabPage7)
        Me.TabControl1.Controls.Add(Me.TabPage8)
        Me.TabControl1.Controls.Add(Me.TabPage9)
        Me.TabControl1.Controls.Add(Me.TabPage10)
        Me.TabControl1.Controls.Add(Me.TabPage11)
        Me.TabControl1.Controls.Add(Me.TabPage12)
        Me.TabControl1.Controls.Add(Me.TabPage13)
        Me.TabControl1.Controls.Add(Me.TabPage14)
        Me.TabControl1.Controls.Add(Me.TabPage15)
        Me.TabControl1.Controls.Add(Me.TabPage16)
        Me.TabControl1.Controls.Add(Me.TabPuntosPPE)
        Me.TabControl1.Controls.Add(Me.TabPage17)
        Me.TabControl1.Controls.Add(Me.TabPage18)
        Me.TabControl1.Controls.Add(Me.tpRecontratacion)
        Me.TabControl1.Controls.Add(Me.tpDecodificadores)
        Me.TabControl1.Controls.Add(Me.tpEstadosCuenta)
        Me.TabControl1.Controls.Add(Me.tpDatosFiscales)
        Me.TabControl1.Controls.Add(Me.tbPuntos)
        Me.TabControl1.Controls.Add(Me.tbPromocionAntiguedad)
        Me.TabControl1.Controls.Add(Me.tpPromPriMens)
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Multiline = True
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(873, 660)
        Me.TabControl1.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight
        Me.TabControl1.TabIndex = 72
        '
        'TabPage15
        '
        Me.TabPage15.AutoScroll = True
        Me.TabPage15.Controls.Add(Me.PnQuejas)
        Me.TabPage15.Location = New System.Drawing.Point(4, 84)
        Me.TabPage15.Name = "TabPage15"
        Me.TabPage15.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage15.Size = New System.Drawing.Size(865, 572)
        Me.TabPage15.TabIndex = 14
        Me.TabPage15.Text = "Órdenes y Quejas"
        Me.TabPage15.UseVisualStyleBackColor = True
        '
        'PnQuejas
        '
        Me.PnQuejas.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PnQuejas.Controls.Add(Me.Label59)
        Me.PnQuejas.Controls.Add(Me.Button10)
        Me.PnQuejas.Controls.Add(Me.Button9)
        Me.PnQuejas.Controls.Add(Me.TreeViewOrdenes)
        Me.PnQuejas.Controls.Add(Me.LblNotas)
        Me.PnQuejas.Controls.Add(Me.LblFamilia)
        Me.PnQuejas.Controls.Add(Me.LblTecnico)
        Me.PnQuejas.Controls.Add(Me.Label57)
        Me.PnQuejas.Controls.Add(Me.TreViewTecnicos)
        Me.PnQuejas.Controls.Add(Me.CmbTecnicos)
        Me.PnQuejas.Controls.Add(Me.CmBDepto)
        Me.PnQuejas.Controls.Add(Me.Button4)
        Me.PnQuejas.Controls.Add(Me.Button2)
        Me.PnQuejas.Location = New System.Drawing.Point(6, 6)
        Me.PnQuejas.Name = "PnQuejas"
        Me.PnQuejas.Size = New System.Drawing.Size(850, 581)
        Me.PnQuejas.TabIndex = 0
        '
        'Label59
        '
        Me.Label59.BackColor = System.Drawing.Color.Black
        Me.Label59.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.ForeColor = System.Drawing.Color.White
        Me.Label59.Location = New System.Drawing.Point(25, 120)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(304, 23)
        Me.Label59.TabIndex = 66
        Me.Label59.Text = "Técnicos a mostrar en Órdenes"
        Me.Label59.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(335, 184)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(76, 32)
        Me.Button10.TabIndex = 65
        Me.Button10.Text = "&Eliminar"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(335, 146)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(76, 32)
        Me.Button9.TabIndex = 64
        Me.Button9.Text = "&Agregar"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'TreeViewOrdenes
        '
        Me.TreeViewOrdenes.Location = New System.Drawing.Point(25, 146)
        Me.TreeViewOrdenes.Name = "TreeViewOrdenes"
        Me.TreeViewOrdenes.Size = New System.Drawing.Size(304, 365)
        Me.TreeViewOrdenes.TabIndex = 63
        '
        'LblNotas
        '
        Me.LblNotas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNotas.ForeColor = System.Drawing.Color.Red
        Me.LblNotas.Location = New System.Drawing.Point(300, 525)
        Me.LblNotas.Name = "LblNotas"
        Me.LblNotas.Size = New System.Drawing.Size(296, 50)
        Me.LblNotas.TabIndex = 62
        Me.LblNotas.Text = "Nota: Si requiere agregar TODOS los Técnicos de un Departamento, NO seleccione ni" & _
            "ngún Técnico."
        Me.LblNotas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblFamilia
        '
        Me.LblFamilia.AutoSize = True
        Me.LblFamilia.Location = New System.Drawing.Point(196, 37)
        Me.LblFamilia.Name = "LblFamilia"
        Me.LblFamilia.Size = New System.Drawing.Size(98, 15)
        Me.LblFamilia.TabIndex = 61
        Me.LblFamilia.Text = "Departamento"
        '
        'LblTecnico
        '
        Me.LblTecnico.AutoSize = True
        Me.LblTecnico.Location = New System.Drawing.Point(237, 66)
        Me.LblTecnico.Name = "LblTecnico"
        Me.LblTecnico.Size = New System.Drawing.Size(57, 15)
        Me.LblTecnico.TabIndex = 60
        Me.LblTecnico.Text = "Técnico"
        '
        'Label57
        '
        Me.Label57.BackColor = System.Drawing.Color.Black
        Me.Label57.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label57.ForeColor = System.Drawing.Color.White
        Me.Label57.Location = New System.Drawing.Point(449, 120)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(304, 23)
        Me.Label57.TabIndex = 59
        Me.Label57.Text = "Técnicos a mostrar en Quejas"
        Me.Label57.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TreViewTecnicos
        '
        Me.TreViewTecnicos.Location = New System.Drawing.Point(449, 146)
        Me.TreViewTecnicos.Name = "TreViewTecnicos"
        Me.TreViewTecnicos.Size = New System.Drawing.Size(304, 365)
        Me.TreViewTecnicos.TabIndex = 58
        '
        'CmbTecnicos
        '
        Me.CmbTecnicos.DataSource = Me.MuestraTecnicosByFamiliBindingSource
        Me.CmbTecnicos.DisplayMember = "Tecnico"
        Me.CmbTecnicos.FormattingEnabled = True
        Me.CmbTecnicos.Location = New System.Drawing.Point(300, 58)
        Me.CmbTecnicos.Name = "CmbTecnicos"
        Me.CmbTecnicos.Size = New System.Drawing.Size(296, 23)
        Me.CmbTecnicos.TabIndex = 57
        Me.CmbTecnicos.ValueMember = "Clv_tecnico"
        '
        'MuestraTecnicosByFamiliBindingSource
        '
        Me.MuestraTecnicosByFamiliBindingSource.DataMember = "Muestra_TecnicosByFamili"
        Me.MuestraTecnicosByFamiliBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'CmBDepto
        '
        Me.CmBDepto.DataSource = Me.MuestratecnicosDepartamentosAlmacenBindingSource
        Me.CmBDepto.DisplayMember = "Puesto"
        Me.CmBDepto.FormattingEnabled = True
        Me.CmBDepto.Location = New System.Drawing.Point(300, 29)
        Me.CmBDepto.Name = "CmBDepto"
        Me.CmBDepto.Size = New System.Drawing.Size(296, 23)
        Me.CmBDepto.TabIndex = 56
        Me.CmBDepto.ValueMember = "Clv_Puesto"
        '
        'MuestratecnicosDepartamentosAlmacenBindingSource
        '
        Me.MuestratecnicosDepartamentosAlmacenBindingSource.DataMember = "Muestra_tecnicosDepartamentos_Almacen"
        Me.MuestratecnicosDepartamentosAlmacenBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(759, 184)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(76, 32)
        Me.Button4.TabIndex = 55
        Me.Button4.Text = "&Eliminar"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(759, 146)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(76, 32)
        Me.Button2.TabIndex = 54
        Me.Button2.Text = "&Agregar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'TabPage16
        '
        Me.TabPage16.Controls.Add(Me.LabelNota)
        Me.TabPage16.Controls.Add(Me.Label58)
        Me.TabPage16.Controls.Add(Me.Button7)
        Me.TabPage16.Controls.Add(Me.DataGridView1)
        Me.TabPage16.Location = New System.Drawing.Point(4, 84)
        Me.TabPage16.Name = "TabPage16"
        Me.TabPage16.Size = New System.Drawing.Size(865, 572)
        Me.TabPage16.TabIndex = 15
        Me.TabPage16.Text = "Cartera Ejecutiva"
        Me.TabPage16.UseVisualStyleBackColor = True
        '
        'LabelNota
        '
        Me.LabelNota.ForeColor = System.Drawing.Color.Red
        Me.LabelNota.Location = New System.Drawing.Point(226, 399)
        Me.LabelNota.Name = "LabelNota"
        Me.LabelNota.Size = New System.Drawing.Size(452, 22)
        Me.LabelNota.TabIndex = 55
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.BackColor = System.Drawing.Color.Black
        Me.Label58.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label58.ForeColor = System.Drawing.Color.White
        Me.Label58.Location = New System.Drawing.Point(31, 21)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(260, 20)
        Me.Label58.TabIndex = 54
        Me.Label58.Text = "Generales de Cartera Ejecutiva"
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.SystemColors.Control
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.Black
        Me.Button7.Location = New System.Drawing.Point(694, 543)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(136, 36)
        Me.Button7.TabIndex = 18
        Me.Button7.Text = "&GUARDAR"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Grupo, Me.Grupo, Me.Porcentaje, Me.Activo})
        Me.DataGridView1.Location = New System.Drawing.Point(226, 119)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(452, 268)
        Me.DataGridView1.TabIndex = 0
        '
        'Clv_Grupo
        '
        Me.Clv_Grupo.DataPropertyName = "Clv_Grupo"
        Me.Clv_Grupo.HeaderText = "Clv_Grupo"
        Me.Clv_Grupo.Name = "Clv_Grupo"
        Me.Clv_Grupo.Visible = False
        '
        'Grupo
        '
        Me.Grupo.DataPropertyName = "Grupo"
        Me.Grupo.HeaderText = "Grupo"
        Me.Grupo.Name = "Grupo"
        Me.Grupo.Width = 150
        '
        'Porcentaje
        '
        Me.Porcentaje.DataPropertyName = "Porcentaje"
        Me.Porcentaje.HeaderText = "Porcentaje (%)"
        Me.Porcentaje.Name = "Porcentaje"
        Me.Porcentaje.Width = 150
        '
        'Activo
        '
        Me.Activo.DataPropertyName = "Activo"
        Me.Activo.HeaderText = "Activo"
        Me.Activo.Name = "Activo"
        '
        'TabPuntosPPE
        '
        Me.TabPuntosPPE.Controls.Add(lblPPE2)
        Me.TabPuntosPPE.Controls.Add(Me.txtPtosMin)
        Me.TabPuntosPPE.Controls.Add(Me.btnGuardaPPE)
        Me.TabPuntosPPE.Controls.Add(Me.lblPPE1)
        Me.TabPuntosPPE.Location = New System.Drawing.Point(4, 84)
        Me.TabPuntosPPE.Name = "TabPuntosPPE"
        Me.TabPuntosPPE.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPuntosPPE.Size = New System.Drawing.Size(865, 572)
        Me.TabPuntosPPE.TabIndex = 16
        Me.TabPuntosPPE.Text = "Puntos PPE"
        Me.TabPuntosPPE.UseVisualStyleBackColor = True
        '
        'txtPtosMin
        '
        Me.txtPtosMin.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_cobrodepositobuenoBindingSource, "pregdeco", True))
        Me.txtPtosMin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPtosMin.Location = New System.Drawing.Point(451, 190)
        Me.txtPtosMin.Name = "txtPtosMin"
        Me.txtPtosMin.Size = New System.Drawing.Size(104, 21)
        Me.txtPtosMin.TabIndex = 83
        '
        'btnGuardaPPE
        '
        Me.btnGuardaPPE.BackColor = System.Drawing.SystemColors.Control
        Me.btnGuardaPPE.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardaPPE.ForeColor = System.Drawing.Color.Black
        Me.btnGuardaPPE.Location = New System.Drawing.Point(723, 550)
        Me.btnGuardaPPE.Name = "btnGuardaPPE"
        Me.btnGuardaPPE.Size = New System.Drawing.Size(136, 36)
        Me.btnGuardaPPE.TabIndex = 81
        Me.btnGuardaPPE.Text = "&GUARDAR"
        Me.btnGuardaPPE.UseVisualStyleBackColor = False
        '
        'lblPPE1
        '
        Me.lblPPE1.AutoSize = True
        Me.lblPPE1.BackColor = System.Drawing.Color.Black
        Me.lblPPE1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPPE1.ForeColor = System.Drawing.Color.White
        Me.lblPPE1.Location = New System.Drawing.Point(47, 36)
        Me.lblPPE1.Name = "lblPPE1"
        Me.lblPPE1.Size = New System.Drawing.Size(242, 20)
        Me.lblPPE1.TabIndex = 55
        Me.lblPPE1.Text = "Puntos Minimos Servicio PPE"
        '
        'TabPage17
        '
        Me.TabPage17.Controls.Add(Me.Button13)
        Me.TabPage17.Controls.Add(Me.Label64)
        Me.TabPage17.Controls.Add(Me.DDescoSusp)
        Me.TabPage17.Controls.Add(Me.DContrata)
        Me.TabPage17.Controls.Add(Label61)
        Me.TabPage17.Controls.Add(Label63)
        Me.TabPage17.Location = New System.Drawing.Point(4, 84)
        Me.TabPage17.Name = "TabPage17"
        Me.TabPage17.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage17.Size = New System.Drawing.Size(865, 572)
        Me.TabPage17.TabIndex = 17
        Me.TabPage17.Text = "Proporcionales"
        Me.TabPage17.UseVisualStyleBackColor = True
        '
        'Button13
        '
        Me.Button13.BackColor = System.Drawing.SystemColors.Control
        Me.Button13.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button13.ForeColor = System.Drawing.Color.Black
        Me.Button13.Location = New System.Drawing.Point(679, 512)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(136, 36)
        Me.Button13.TabIndex = 27
        Me.Button13.Text = "&GUARDAR"
        Me.Button13.UseVisualStyleBackColor = False
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.BackColor = System.Drawing.Color.Black
        Me.Label64.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label64.ForeColor = System.Drawing.Color.White
        Me.Label64.Location = New System.Drawing.Point(56, 79)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(183, 20)
        Me.Label64.TabIndex = 26
        Me.Label64.Text = "Pagos Proporcionales"
        '
        'DDescoSusp
        '
        Me.DDescoSusp.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTAGENERALESDESCBindingSource, "DiasPuntosAnt1", True))
        Me.DDescoSusp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DDescoSusp.Location = New System.Drawing.Point(489, 336)
        Me.DDescoSusp.MaxLength = 3
        Me.DDescoSusp.Name = "DDescoSusp"
        Me.DDescoSusp.Size = New System.Drawing.Size(100, 21)
        Me.DDescoSusp.TabIndex = 23
        '
        'DContrata
        '
        Me.DContrata.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTAGENERALESDESCBindingSource, "DiasPuntosPP1", True))
        Me.DContrata.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DContrata.Location = New System.Drawing.Point(489, 258)
        Me.DContrata.MaxLength = 3
        Me.DContrata.Name = "DContrata"
        Me.DContrata.Size = New System.Drawing.Size(100, 21)
        Me.DContrata.TabIndex = 22
        '
        'TabPage18
        '
        Me.TabPage18.Controls.Add(Me.TextIEPS)
        Me.TabPage18.Controls.Add(Me.TextIva)
        Me.TabPage18.Controls.Add(Me.Label68)
        Me.TabPage18.Controls.Add(Me.LblPorIeps)
        Me.TabPage18.Controls.Add(Me.NumericUpDownIeps)
        Me.TabPage18.Controls.Add(Me.NumericUpDownIva)
        Me.TabPage18.Controls.Add(Me.Button14)
        Me.TabPage18.Controls.Add(Me.ChkCalculo1)
        Me.TabPage18.Controls.Add(Me.LabelIEPS)
        Me.TabPage18.Controls.Add(Me.CtaIepsText)
        Me.TabPage18.Controls.Add(Me.LabelCtaIEPS)
        Me.TabPage18.Controls.Add(Me.CheckIEPS)
        Me.TabPage18.Controls.Add(Label69)
        Me.TabPage18.Controls.Add(Me.Label67)
        Me.TabPage18.Location = New System.Drawing.Point(4, 84)
        Me.TabPage18.Name = "TabPage18"
        Me.TabPage18.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage18.Size = New System.Drawing.Size(865, 572)
        Me.TabPage18.TabIndex = 18
        Me.TabPage18.Text = "Impuestos"
        Me.TabPage18.UseVisualStyleBackColor = True
        '
        'TextIEPS
        '
        Me.TextIEPS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextIEPS.Enabled = False
        Me.TextIEPS.Location = New System.Drawing.Point(331, 233)
        Me.TextIEPS.Name = "TextIEPS"
        Me.TextIEPS.Size = New System.Drawing.Size(47, 21)
        Me.TextIEPS.TabIndex = 65
        Me.TextIEPS.Visible = False
        '
        'TextIva
        '
        Me.TextIva.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextIva.Enabled = False
        Me.TextIva.Location = New System.Drawing.Point(331, 105)
        Me.TextIva.Name = "TextIva"
        Me.TextIva.Size = New System.Drawing.Size(47, 21)
        Me.TextIva.TabIndex = 64
        Me.TextIva.Visible = False
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Location = New System.Drawing.Point(306, 107)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(19, 15)
        Me.Label68.TabIndex = 63
        Me.Label68.Text = "%"
        '
        'LblPorIeps
        '
        Me.LblPorIeps.AutoSize = True
        Me.LblPorIeps.Location = New System.Drawing.Point(306, 235)
        Me.LblPorIeps.Name = "LblPorIeps"
        Me.LblPorIeps.Size = New System.Drawing.Size(19, 15)
        Me.LblPorIeps.TabIndex = 62
        Me.LblPorIeps.Text = "%"
        Me.LblPorIeps.Visible = False
        '
        'NumericUpDownIeps
        '
        Me.NumericUpDownIeps.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NumericUpDownIeps.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDownIeps.Location = New System.Drawing.Point(254, 233)
        Me.NumericUpDownIeps.Name = "NumericUpDownIeps"
        Me.NumericUpDownIeps.Size = New System.Drawing.Size(46, 22)
        Me.NumericUpDownIeps.TabIndex = 61
        Me.NumericUpDownIeps.Visible = False
        '
        'NumericUpDownIva
        '
        Me.NumericUpDownIva.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NumericUpDownIva.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDownIva.Location = New System.Drawing.Point(254, 105)
        Me.NumericUpDownIva.Name = "NumericUpDownIva"
        Me.NumericUpDownIva.Size = New System.Drawing.Size(46, 22)
        Me.NumericUpDownIva.TabIndex = 60
        '
        'Button14
        '
        Me.Button14.BackColor = System.Drawing.SystemColors.Control
        Me.Button14.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button14.ForeColor = System.Drawing.Color.Black
        Me.Button14.Location = New System.Drawing.Point(376, 386)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(136, 36)
        Me.Button14.TabIndex = 59
        Me.Button14.Text = "&GUARDAR"
        Me.Button14.UseVisualStyleBackColor = False
        '
        'ChkCalculo1
        '
        Me.ChkCalculo1.AutoSize = True
        Me.ChkCalculo1.Location = New System.Drawing.Point(254, 188)
        Me.ChkCalculo1.Name = "ChkCalculo1"
        Me.ChkCalculo1.Size = New System.Drawing.Size(205, 19)
        Me.ChkCalculo1.TabIndex = 58
        Me.ChkCalculo1.Text = "Calcular Iva Incluyendo Ieps"
        Me.ChkCalculo1.UseVisualStyleBackColor = True
        Me.ChkCalculo1.Visible = False
        '
        'LabelIEPS
        '
        Me.LabelIEPS.AutoSize = True
        Me.LabelIEPS.Location = New System.Drawing.Point(202, 235)
        Me.LabelIEPS.Name = "LabelIEPS"
        Me.LabelIEPS.Size = New System.Drawing.Size(46, 15)
        Me.LabelIEPS.TabIndex = 57
        Me.LabelIEPS.Text = "IEPS :"
        '
        'CtaIepsText
        '
        Me.CtaIepsText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CtaIepsText.Location = New System.Drawing.Point(254, 277)
        Me.CtaIepsText.Name = "CtaIepsText"
        Me.CtaIepsText.Size = New System.Drawing.Size(136, 21)
        Me.CtaIepsText.TabIndex = 56
        Me.CtaIepsText.Visible = False
        '
        'LabelCtaIEPS
        '
        Me.LabelCtaIEPS.AutoSize = True
        Me.LabelCtaIEPS.Location = New System.Drawing.Point(173, 279)
        Me.LabelCtaIEPS.Name = "LabelCtaIEPS"
        Me.LabelCtaIEPS.Size = New System.Drawing.Size(75, 15)
        Me.LabelCtaIEPS.TabIndex = 55
        Me.LabelCtaIEPS.Text = "Cta. IEPS :"
        Me.LabelCtaIEPS.Visible = False
        '
        'CheckIEPS
        '
        Me.CheckIEPS.AutoSize = True
        Me.CheckIEPS.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckIEPS.ForeColor = System.Drawing.Color.Red
        Me.CheckIEPS.Location = New System.Drawing.Point(254, 152)
        Me.CheckIEPS.Name = "CheckIEPS"
        Me.CheckIEPS.Size = New System.Drawing.Size(124, 20)
        Me.CheckIEPS.TabIndex = 54
        Me.CheckIEPS.Text = "Habilitar IEPS"
        Me.CheckIEPS.UseVisualStyleBackColor = True
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.BackColor = System.Drawing.Color.Black
        Me.Label67.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label67.ForeColor = System.Drawing.SystemColors.Window
        Me.Label67.Location = New System.Drawing.Point(133, 47)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(115, 18)
        Me.Label67.TabIndex = 0
        Me.Label67.Text = "IMPUESTOS: "
        '
        'tpRecontratacion
        '
        Me.tpRecontratacion.Controls.Add(Me.Label66)
        Me.tpRecontratacion.Controls.Add(Me.btnAceptarRecon)
        Me.tpRecontratacion.Controls.Add(Me.Label70)
        Me.tpRecontratacion.Controls.Add(Me.cbAplica)
        Me.tpRecontratacion.Controls.Add(Me.txtMeses)
        Me.tpRecontratacion.Controls.Add(Me.Label65)
        Me.tpRecontratacion.Location = New System.Drawing.Point(4, 84)
        Me.tpRecontratacion.Name = "tpRecontratacion"
        Me.tpRecontratacion.Size = New System.Drawing.Size(865, 572)
        Me.tpRecontratacion.TabIndex = 19
        Me.tpRecontratacion.Text = "Recontratación"
        Me.tpRecontratacion.UseVisualStyleBackColor = True
        '
        'Label66
        '
        Me.Label66.Location = New System.Drawing.Point(454, 163)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(70, 18)
        Me.Label66.TabIndex = 62
        Me.Label66.Text = "Meses"
        '
        'btnAceptarRecon
        '
        Me.btnAceptarRecon.BackColor = System.Drawing.SystemColors.Control
        Me.btnAceptarRecon.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptarRecon.ForeColor = System.Drawing.Color.Black
        Me.btnAceptarRecon.Location = New System.Drawing.Point(350, 287)
        Me.btnAceptarRecon.Name = "btnAceptarRecon"
        Me.btnAceptarRecon.Size = New System.Drawing.Size(136, 36)
        Me.btnAceptarRecon.TabIndex = 61
        Me.btnAceptarRecon.Text = "&GUARDAR"
        Me.btnAceptarRecon.UseVisualStyleBackColor = False
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.BackColor = System.Drawing.Color.Black
        Me.Label70.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label70.ForeColor = System.Drawing.SystemColors.Window
        Me.Label70.Location = New System.Drawing.Point(41, 37)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(193, 18)
        Me.Label70.TabIndex = 60
        Me.Label70.Text = "RECONTRATACIONES: "
        '
        'cbAplica
        '
        Me.cbAplica.AutoSize = True
        Me.cbAplica.Location = New System.Drawing.Point(556, 163)
        Me.cbAplica.Name = "cbAplica"
        Me.cbAplica.Size = New System.Drawing.Size(151, 19)
        Me.cbAplica.TabIndex = 3
        Me.cbAplica.Text = "Aplica Proporcional"
        Me.cbAplica.UseVisualStyleBackColor = True
        Me.cbAplica.Visible = False
        '
        'txtMeses
        '
        Me.txtMeses.Location = New System.Drawing.Point(350, 163)
        Me.txtMeses.Name = "txtMeses"
        Me.txtMeses.Size = New System.Drawing.Size(98, 21)
        Me.txtMeses.TabIndex = 2
        '
        'Label65
        '
        Me.Label65.Location = New System.Drawing.Point(239, 108)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(383, 52)
        Me.Label65.TabIndex = 0
        Me.Label65.Text = "A partir de la última fecha de Baja, número de meses que deben de pasar para que " & _
            "se tome como Contratación:"
        '
        'tpDecodificadores
        '
        Me.tpDecodificadores.Controls.Add(Me.Label77)
        Me.tpDecodificadores.Controls.Add(Me.txtPrecioPrincipal)
        Me.tpDecodificadores.Controls.Add(Me.Label76)
        Me.tpDecodificadores.Controls.Add(Me.txtPrecioAdicional)
        Me.tpDecodificadores.Controls.Add(Me.Label75)
        Me.tpDecodificadores.Controls.Add(Me.Label74)
        Me.tpDecodificadores.Controls.Add(Me.Label73)
        Me.tpDecodificadores.Controls.Add(Me.txtEliminarDec)
        Me.tpDecodificadores.Controls.Add(Me.txtAgregarDec)
        Me.tpDecodificadores.Controls.Add(Me.dataGridDec)
        Me.tpDecodificadores.Controls.Add(Me.txtDescripcionDec)
        Me.tpDecodificadores.Controls.Add(Me.comboServicioDec)
        Me.tpDecodificadores.Location = New System.Drawing.Point(4, 84)
        Me.tpDecodificadores.Name = "tpDecodificadores"
        Me.tpDecodificadores.Size = New System.Drawing.Size(865, 572)
        Me.tpDecodificadores.TabIndex = 20
        Me.tpDecodificadores.Text = "Decodificadores"
        Me.tpDecodificadores.UseVisualStyleBackColor = True
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Location = New System.Drawing.Point(134, 157)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(113, 15)
        Me.Label77.TabIndex = 65
        Me.Label77.Text = "Precio Principal:"
        '
        'txtPrecioPrincipal
        '
        Me.txtPrecioPrincipal.Location = New System.Drawing.Point(253, 151)
        Me.txtPrecioPrincipal.Name = "txtPrecioPrincipal"
        Me.txtPrecioPrincipal.Size = New System.Drawing.Size(160, 21)
        Me.txtPrecioPrincipal.TabIndex = 64
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Location = New System.Drawing.Point(132, 184)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(115, 15)
        Me.Label76.TabIndex = 63
        Me.Label76.Text = "Precio Adicional:"
        '
        'txtPrecioAdicional
        '
        Me.txtPrecioAdicional.Location = New System.Drawing.Point(253, 178)
        Me.txtPrecioAdicional.Name = "txtPrecioAdicional"
        Me.txtPrecioAdicional.Size = New System.Drawing.Size(160, 21)
        Me.txtPrecioAdicional.TabIndex = 62
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.BackColor = System.Drawing.Color.Black
        Me.Label75.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label75.ForeColor = System.Drawing.SystemColors.Window
        Me.Label75.Location = New System.Drawing.Point(41, 37)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(177, 18)
        Me.Label75.TabIndex = 61
        Me.Label75.Text = "DECODIFICADORES: "
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.Location = New System.Drawing.Point(185, 130)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(62, 15)
        Me.Label74.TabIndex = 6
        Me.Label74.Text = "Servicio:"
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Location = New System.Drawing.Point(147, 101)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(100, 15)
        Me.Label73.TabIndex = 5
        Me.Label73.Text = "Decodificador:"
        '
        'txtEliminarDec
        '
        Me.txtEliminarDec.Location = New System.Drawing.Point(739, 236)
        Me.txtEliminarDec.Name = "txtEliminarDec"
        Me.txtEliminarDec.Size = New System.Drawing.Size(75, 23)
        Me.txtEliminarDec.TabIndex = 4
        Me.txtEliminarDec.Text = "&Eliminar"
        Me.txtEliminarDec.UseVisualStyleBackColor = True
        '
        'txtAgregarDec
        '
        Me.txtAgregarDec.Location = New System.Drawing.Point(578, 121)
        Me.txtAgregarDec.Name = "txtAgregarDec"
        Me.txtAgregarDec.Size = New System.Drawing.Size(75, 23)
        Me.txtAgregarDec.TabIndex = 3
        Me.txtAgregarDec.Text = "&Agregar"
        Me.txtAgregarDec.UseVisualStyleBackColor = True
        '
        'dataGridDec
        '
        Me.dataGridDec.AllowUserToAddRows = False
        Me.dataGridDec.AllowUserToDeleteRows = False
        Me.dataGridDec.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dataGridDec.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dataGridDec.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdDec, Me.DescripcionDec, Me.Clv_ServicioDec, Me.ServicioDec, Me.PrecioPrincipalDec, Me.PrecioAdicionalDec})
        Me.dataGridDec.Location = New System.Drawing.Point(22, 236)
        Me.dataGridDec.Name = "dataGridDec"
        Me.dataGridDec.ReadOnly = True
        Me.dataGridDec.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dataGridDec.Size = New System.Drawing.Size(692, 275)
        Me.dataGridDec.TabIndex = 2
        '
        'IdDec
        '
        Me.IdDec.DataPropertyName = "Id"
        Me.IdDec.HeaderText = "Id"
        Me.IdDec.Name = "IdDec"
        Me.IdDec.ReadOnly = True
        Me.IdDec.Width = 50
        '
        'DescripcionDec
        '
        Me.DescripcionDec.DataPropertyName = "Descripcion"
        Me.DescripcionDec.HeaderText = "Decodificador"
        Me.DescripcionDec.Name = "DescripcionDec"
        Me.DescripcionDec.ReadOnly = True
        Me.DescripcionDec.Width = 150
        '
        'Clv_ServicioDec
        '
        Me.Clv_ServicioDec.DataPropertyName = "Clv_Servicio"
        Me.Clv_ServicioDec.HeaderText = "Clv_Servicio"
        Me.Clv_ServicioDec.Name = "Clv_ServicioDec"
        Me.Clv_ServicioDec.ReadOnly = True
        Me.Clv_ServicioDec.Visible = False
        '
        'ServicioDec
        '
        Me.ServicioDec.DataPropertyName = "Servicio"
        Me.ServicioDec.HeaderText = "Servicio"
        Me.ServicioDec.Name = "ServicioDec"
        Me.ServicioDec.ReadOnly = True
        Me.ServicioDec.Width = 200
        '
        'PrecioPrincipalDec
        '
        Me.PrecioPrincipalDec.DataPropertyName = "PrecioPrincipal"
        DataGridViewCellStyle3.Format = "C2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.PrecioPrincipalDec.DefaultCellStyle = DataGridViewCellStyle3
        Me.PrecioPrincipalDec.HeaderText = "PrecioPrincipal"
        Me.PrecioPrincipalDec.Name = "PrecioPrincipalDec"
        Me.PrecioPrincipalDec.ReadOnly = True
        Me.PrecioPrincipalDec.Width = 120
        '
        'PrecioAdicionalDec
        '
        Me.PrecioAdicionalDec.DataPropertyName = "PrecioAdicional"
        DataGridViewCellStyle4.Format = "C2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.PrecioAdicionalDec.DefaultCellStyle = DataGridViewCellStyle4
        Me.PrecioAdicionalDec.HeaderText = "PrecioAdicional"
        Me.PrecioAdicionalDec.Name = "PrecioAdicionalDec"
        Me.PrecioAdicionalDec.ReadOnly = True
        Me.PrecioAdicionalDec.Width = 120
        '
        'txtDescripcionDec
        '
        Me.txtDescripcionDec.Location = New System.Drawing.Point(253, 95)
        Me.txtDescripcionDec.Name = "txtDescripcionDec"
        Me.txtDescripcionDec.Size = New System.Drawing.Size(319, 21)
        Me.txtDescripcionDec.TabIndex = 1
        '
        'comboServicioDec
        '
        Me.comboServicioDec.DisplayMember = "Descripcion"
        Me.comboServicioDec.FormattingEnabled = True
        Me.comboServicioDec.Location = New System.Drawing.Point(253, 122)
        Me.comboServicioDec.Name = "comboServicioDec"
        Me.comboServicioDec.Size = New System.Drawing.Size(319, 23)
        Me.comboServicioDec.TabIndex = 0
        Me.comboServicioDec.ValueMember = "Clv_Servicio"
        '
        'tpEstadosCuenta
        '
        Me.tpEstadosCuenta.Controls.Add(Me.gbMensaje)
        Me.tpEstadosCuenta.Controls.Add(Me.Label79)
        Me.tpEstadosCuenta.Controls.Add(Me.Label78)
        Me.tpEstadosCuenta.Controls.Add(Me.Label80)
        Me.tpEstadosCuenta.Controls.Add(Me.btnGuardar)
        Me.tpEstadosCuenta.Controls.Add(Me.txtDiaEdoCuenta)
        Me.tpEstadosCuenta.Controls.Add(Me.cbPeriodo)
        Me.tpEstadosCuenta.Location = New System.Drawing.Point(4, 84)
        Me.tpEstadosCuenta.Name = "tpEstadosCuenta"
        Me.tpEstadosCuenta.Size = New System.Drawing.Size(865, 572)
        Me.tpEstadosCuenta.TabIndex = 21
        Me.tpEstadosCuenta.Text = "Estados de Cuenta"
        Me.tpEstadosCuenta.UseVisualStyleBackColor = True
        '
        'gbMensaje
        '
        Me.gbMensaje.Controls.Add(Me.Label88)
        Me.gbMensaje.Controls.Add(Me.Label87)
        Me.gbMensaje.Controls.Add(Me.cbDeco)
        Me.gbMensaje.Controls.Add(Me.cbCorreo)
        Me.gbMensaje.Controls.Add(Me.dtHora)
        Me.gbMensaje.Controls.Add(Me.Label86)
        Me.gbMensaje.Controls.Add(Me.tbMensajeDeco)
        Me.gbMensaje.Controls.Add(Me.Label85)
        Me.gbMensaje.Controls.Add(Me.Label84)
        Me.gbMensaje.Controls.Add(Me.Label83)
        Me.gbMensaje.Controls.Add(Me.tbMensaje)
        Me.gbMensaje.Controls.Add(Me.Label82)
        Me.gbMensaje.Controls.Add(Me.tbAsunto)
        Me.gbMensaje.Controls.Add(Me.Label81)
        Me.gbMensaje.Location = New System.Drawing.Point(19, 168)
        Me.gbMensaje.Name = "gbMensaje"
        Me.gbMensaje.Size = New System.Drawing.Size(830, 344)
        Me.gbMensaje.TabIndex = 42
        Me.gbMensaje.TabStop = False
        Me.gbMensaje.Text = "Mensajes"
        '
        'Label88
        '
        Me.Label88.AutoSize = True
        Me.Label88.Location = New System.Drawing.Point(30, 70)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(52, 15)
        Me.Label88.TabIndex = 52
        Me.Label88.Text = "Activo :"
        '
        'Label87
        '
        Me.Label87.AutoSize = True
        Me.Label87.Location = New System.Drawing.Point(441, 70)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(52, 15)
        Me.Label87.TabIndex = 51
        Me.Label87.Text = "Activo :"
        '
        'cbDeco
        '
        Me.cbDeco.AutoSize = True
        Me.cbDeco.Location = New System.Drawing.Point(499, 70)
        Me.cbDeco.Name = "cbDeco"
        Me.cbDeco.Size = New System.Drawing.Size(15, 14)
        Me.cbDeco.TabIndex = 50
        Me.cbDeco.UseVisualStyleBackColor = True
        '
        'cbCorreo
        '
        Me.cbCorreo.AutoSize = True
        Me.cbCorreo.Location = New System.Drawing.Point(88, 70)
        Me.cbCorreo.Name = "cbCorreo"
        Me.cbCorreo.Size = New System.Drawing.Size(15, 14)
        Me.cbCorreo.TabIndex = 49
        Me.cbCorreo.UseVisualStyleBackColor = True
        '
        'dtHora
        '
        Me.dtHora.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtHora.Location = New System.Drawing.Point(499, 305)
        Me.dtHora.MaxDate = New Date(1900, 1, 1, 22, 0, 0, 0)
        Me.dtHora.MinDate = New Date(1900, 1, 1, 7, 0, 0, 0)
        Me.dtHora.Name = "dtHora"
        Me.dtHora.ShowUpDown = True
        Me.dtHora.Size = New System.Drawing.Size(118, 21)
        Me.dtHora.TabIndex = 48
        Me.dtHora.Value = New Date(1900, 1, 1, 7, 0, 0, 0)
        '
        'Label86
        '
        Me.Label86.Location = New System.Drawing.Point(420, 305)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(73, 21)
        Me.Label86.TabIndex = 47
        Me.Label86.Text = "Hora :"
        Me.Label86.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'tbMensajeDeco
        '
        Me.tbMensajeDeco.Location = New System.Drawing.Point(499, 105)
        Me.tbMensajeDeco.MaxLength = 1000
        Me.tbMensajeDeco.Multiline = True
        Me.tbMensajeDeco.Name = "tbMensajeDeco"
        Me.tbMensajeDeco.Size = New System.Drawing.Size(315, 186)
        Me.tbMensajeDeco.TabIndex = 46
        '
        'Label85
        '
        Me.Label85.AutoSize = True
        Me.Label85.Location = New System.Drawing.Point(423, 105)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(70, 15)
        Me.Label85.TabIndex = 45
        Me.Label85.Text = "Mensaje :"
        '
        'Label84
        '
        Me.Label84.AutoSize = True
        Me.Label84.BackColor = System.Drawing.Color.Black
        Me.Label84.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label84.ForeColor = System.Drawing.Color.White
        Me.Label84.Location = New System.Drawing.Point(492, 17)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(139, 20)
        Me.Label84.TabIndex = 44
        Me.Label84.Text = "Decodificadores"
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.BackColor = System.Drawing.Color.Black
        Me.Label83.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label83.ForeColor = System.Drawing.Color.White
        Me.Label83.Location = New System.Drawing.Point(83, 17)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(63, 20)
        Me.Label83.TabIndex = 43
        Me.Label83.Text = "Correo"
        '
        'tbMensaje
        '
        Me.tbMensaje.Location = New System.Drawing.Point(87, 126)
        Me.tbMensaje.MaxLength = 1000
        Me.tbMensaje.Multiline = True
        Me.tbMensaje.Name = "tbMensaje"
        Me.tbMensaje.Size = New System.Drawing.Size(315, 200)
        Me.tbMensaje.TabIndex = 3
        '
        'Label82
        '
        Me.Label82.AutoSize = True
        Me.Label82.Location = New System.Drawing.Point(12, 126)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(70, 15)
        Me.Label82.TabIndex = 2
        Me.Label82.Text = "Mensaje :"
        '
        'tbAsunto
        '
        Me.tbAsunto.Location = New System.Drawing.Point(87, 99)
        Me.tbAsunto.MaxLength = 150
        Me.tbAsunto.Name = "tbAsunto"
        Me.tbAsunto.Size = New System.Drawing.Size(315, 21)
        Me.tbAsunto.TabIndex = 1
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.Location = New System.Drawing.Point(24, 102)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(58, 15)
        Me.Label81.TabIndex = 0
        Me.Label81.Text = "Asunto :"
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.Location = New System.Drawing.Point(279, 126)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(37, 15)
        Me.Label79.TabIndex = 41
        Me.Label79.Text = "Día :"
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Location = New System.Drawing.Point(251, 99)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(65, 15)
        Me.Label78.TabIndex = 40
        Me.Label78.Text = "Periodo :"
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.BackColor = System.Drawing.Color.Black
        Me.Label80.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label80.ForeColor = System.Drawing.Color.White
        Me.Label80.Location = New System.Drawing.Point(85, 40)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(320, 20)
        Me.Label80.TabIndex = 39
        Me.Label80.Text = "Días de emisión de Estados de Cuenta"
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.SystemColors.Control
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.Color.Black
        Me.btnGuardar.Location = New System.Drawing.Point(713, 521)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(136, 36)
        Me.btnGuardar.TabIndex = 38
        Me.btnGuardar.Text = "&GUARDAR"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'txtDiaEdoCuenta
        '
        Me.txtDiaEdoCuenta.Location = New System.Drawing.Point(322, 120)
        Me.txtDiaEdoCuenta.Name = "txtDiaEdoCuenta"
        Me.txtDiaEdoCuenta.Size = New System.Drawing.Size(100, 21)
        Me.txtDiaEdoCuenta.TabIndex = 37
        '
        'cbPeriodo
        '
        Me.cbPeriodo.DisplayMember = "Descripcion"
        Me.cbPeriodo.FormattingEnabled = True
        Me.cbPeriodo.Location = New System.Drawing.Point(322, 91)
        Me.cbPeriodo.Name = "cbPeriodo"
        Me.cbPeriodo.Size = New System.Drawing.Size(303, 23)
        Me.cbPeriodo.TabIndex = 0
        Me.cbPeriodo.ValueMember = "Clv_Periodo"
        '
        'tpDatosFiscales
        '
        Me.tpDatosFiscales.Controls.Add(Me.cbHabilitado)
        Me.tpDatosFiscales.Controls.Add(Me.tbRFCExtranjeros)
        Me.tpDatosFiscales.Controls.Add(Me.tbRFCPublicoGeneral)
        Me.tpDatosFiscales.Controls.Add(Me.tbCertificado)
        Me.tpDatosFiscales.Controls.Add(Me.tbAnoAprobacion)
        Me.tpDatosFiscales.Controls.Add(Me.Label107)
        Me.tpDatosFiscales.Controls.Add(Me.Label106)
        Me.tpDatosFiscales.Controls.Add(Me.Label105)
        Me.tpDatosFiscales.Controls.Add(Me.Label104)
        Me.tpDatosFiscales.Controls.Add(Me.tbNoAprobacion)
        Me.tpDatosFiscales.Controls.Add(Me.Label103)
        Me.tpDatosFiscales.Controls.Add(Me.Label102)
        Me.tpDatosFiscales.Controls.Add(Me.tbTelefono)
        Me.tpDatosFiscales.Controls.Add(Me.Label2000)
        Me.tpDatosFiscales.Controls.Add(Me.tbCodigoPostal)
        Me.tpDatosFiscales.Controls.Add(Me.Label101)
        Me.tpDatosFiscales.Controls.Add(Me.tbPais)
        Me.tpDatosFiscales.Controls.Add(Me.Label100)
        Me.tpDatosFiscales.Controls.Add(Me.tbEstado)
        Me.tpDatosFiscales.Controls.Add(Me.Label99)
        Me.tpDatosFiscales.Controls.Add(Me.tbMunicipio)
        Me.tpDatosFiscales.Controls.Add(Me.Label98)
        Me.tpDatosFiscales.Controls.Add(Me.tbReferencia)
        Me.tpDatosFiscales.Controls.Add(Me.Label97)
        Me.tpDatosFiscales.Controls.Add(Me.tbrfc)
        Me.tpDatosFiscales.Controls.Add(Me.Label96)
        Me.tpDatosFiscales.Controls.Add(Me.tbNombre)
        Me.tpDatosFiscales.Controls.Add(Me.Label95)
        Me.tpDatosFiscales.Controls.Add(Me.tbCalle)
        Me.tpDatosFiscales.Controls.Add(Me.Label94)
        Me.tpDatosFiscales.Controls.Add(Me.tbNoExt)
        Me.tpDatosFiscales.Controls.Add(Me.Label93)
        Me.tpDatosFiscales.Controls.Add(Me.tbNoInt)
        Me.tpDatosFiscales.Controls.Add(Me.Label92)
        Me.tpDatosFiscales.Controls.Add(Me.tbColonia)
        Me.tpDatosFiscales.Controls.Add(Me.Label91)
        Me.tpDatosFiscales.Controls.Add(Me.tbLocalidad)
        Me.tpDatosFiscales.Controls.Add(Me.Label90)
        Me.tpDatosFiscales.Controls.Add(Me.Button17)
        Me.tpDatosFiscales.Controls.Add(Me.tbEmail)
        Me.tpDatosFiscales.Controls.Add(Me.Label89)
        Me.tpDatosFiscales.Location = New System.Drawing.Point(4, 84)
        Me.tpDatosFiscales.Name = "tpDatosFiscales"
        Me.tpDatosFiscales.Size = New System.Drawing.Size(865, 572)
        Me.tpDatosFiscales.TabIndex = 22
        Me.tpDatosFiscales.Text = "Datos Fiscales CFD"
        Me.tpDatosFiscales.UseVisualStyleBackColor = True
        '
        'cbHabilitado
        '
        Me.cbHabilitado.AutoSize = True
        Me.cbHabilitado.Location = New System.Drawing.Point(714, 21)
        Me.cbHabilitado.Name = "cbHabilitado"
        Me.cbHabilitado.Size = New System.Drawing.Size(92, 19)
        Me.cbHabilitado.TabIndex = 77
        Me.cbHabilitado.Text = "Habilitado"
        Me.cbHabilitado.UseVisualStyleBackColor = True
        '
        'tbRFCExtranjeros
        '
        Me.tbRFCExtranjeros.Location = New System.Drawing.Point(315, 506)
        Me.tbRFCExtranjeros.MaxLength = 50
        Me.tbRFCExtranjeros.Name = "tbRFCExtranjeros"
        Me.tbRFCExtranjeros.Size = New System.Drawing.Size(308, 21)
        Me.tbRFCExtranjeros.TabIndex = 76
        '
        'tbRFCPublicoGeneral
        '
        Me.tbRFCPublicoGeneral.Location = New System.Drawing.Point(315, 479)
        Me.tbRFCPublicoGeneral.MaxLength = 50
        Me.tbRFCPublicoGeneral.Name = "tbRFCPublicoGeneral"
        Me.tbRFCPublicoGeneral.Size = New System.Drawing.Size(308, 21)
        Me.tbRFCPublicoGeneral.TabIndex = 75
        '
        'tbCertificado
        '
        Me.tbCertificado.Location = New System.Drawing.Point(315, 425)
        Me.tbCertificado.MaxLength = 50
        Me.tbCertificado.Name = "tbCertificado"
        Me.tbCertificado.Size = New System.Drawing.Size(308, 21)
        Me.tbCertificado.TabIndex = 74
        '
        'tbAnoAprobacion
        '
        Me.tbAnoAprobacion.Location = New System.Drawing.Point(315, 452)
        Me.tbAnoAprobacion.MaxLength = 50
        Me.tbAnoAprobacion.Name = "tbAnoAprobacion"
        Me.tbAnoAprobacion.Size = New System.Drawing.Size(308, 21)
        Me.tbAnoAprobacion.TabIndex = 73
        '
        'Label107
        '
        Me.Label107.AutoSize = True
        Me.Label107.Location = New System.Drawing.Point(191, 512)
        Me.Label107.Name = "Label107"
        Me.Label107.Size = New System.Drawing.Size(119, 15)
        Me.Label107.TabIndex = 72
        Me.Label107.Text = "RFC Extranjeros :"
        '
        'Label106
        '
        Me.Label106.AutoSize = True
        Me.Label106.Location = New System.Drawing.Point(159, 485)
        Me.Label106.Name = "Label106"
        Me.Label106.Size = New System.Drawing.Size(149, 15)
        Me.Label106.TabIndex = 71
        Me.Label106.Text = "RFC Público General :"
        '
        'Label105
        '
        Me.Label105.AutoSize = True
        Me.Label105.Location = New System.Drawing.Point(226, 431)
        Me.Label105.Name = "Label105"
        Me.Label105.Size = New System.Drawing.Size(84, 15)
        Me.Label105.TabIndex = 70
        Me.Label105.Text = "Certificado :"
        '
        'Label104
        '
        Me.Label104.AutoSize = True
        Me.Label104.Location = New System.Drawing.Point(173, 404)
        Me.Label104.Name = "Label104"
        Me.Label104.Size = New System.Drawing.Size(135, 15)
        Me.Label104.TabIndex = 69
        Me.Label104.Text = "Año de Aprobación :"
        '
        'tbNoAprobacion
        '
        Me.tbNoAprobacion.Location = New System.Drawing.Point(315, 398)
        Me.tbNoAprobacion.MaxLength = 50
        Me.tbNoAprobacion.Name = "tbNoAprobacion"
        Me.tbNoAprobacion.Size = New System.Drawing.Size(308, 21)
        Me.tbNoAprobacion.TabIndex = 68
        '
        'Label103
        '
        Me.Label103.AutoSize = True
        Me.Label103.Location = New System.Drawing.Point(146, 458)
        Me.Label103.Name = "Label103"
        Me.Label103.Size = New System.Drawing.Size(162, 15)
        Me.Label103.TabIndex = 67
        Me.Label103.Text = "Número de Aprobación :"
        '
        'Label102
        '
        Me.Label102.AutoSize = True
        Me.Label102.BackColor = System.Drawing.Color.Black
        Me.Label102.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label102.ForeColor = System.Drawing.Color.White
        Me.Label102.Location = New System.Drawing.Point(28, 21)
        Me.Label102.Name = "Label102"
        Me.Label102.Size = New System.Drawing.Size(169, 20)
        Me.Label102.TabIndex = 66
        Me.Label102.Text = "Datos Fiscales CFD"
        '
        'tbTelefono
        '
        Me.tbTelefono.Location = New System.Drawing.Point(315, 344)
        Me.tbTelefono.MaxLength = 50
        Me.tbTelefono.Name = "tbTelefono"
        Me.tbTelefono.Size = New System.Drawing.Size(209, 21)
        Me.tbTelefono.TabIndex = 12
        '
        'Label2000
        '
        Me.Label2000.AutoSize = True
        Me.Label2000.Location = New System.Drawing.Point(242, 54)
        Me.Label2000.Name = "Label2000"
        Me.Label2000.Size = New System.Drawing.Size(66, 15)
        Me.Label2000.TabIndex = 64
        Me.Label2000.Text = "Nombre :"
        '
        'tbCodigoPostal
        '
        Me.tbCodigoPostal.Location = New System.Drawing.Point(315, 317)
        Me.tbCodigoPostal.MaxLength = 50
        Me.tbCodigoPostal.Name = "tbCodigoPostal"
        Me.tbCodigoPostal.Size = New System.Drawing.Size(209, 21)
        Me.tbCodigoPostal.TabIndex = 11
        '
        'Label101
        '
        Me.Label101.AutoSize = True
        Me.Label101.Location = New System.Drawing.Point(260, 81)
        Me.Label101.Name = "Label101"
        Me.Label101.Size = New System.Drawing.Size(48, 15)
        Me.Label101.TabIndex = 62
        Me.Label101.Text = "Calle :"
        '
        'tbPais
        '
        Me.tbPais.Location = New System.Drawing.Point(315, 290)
        Me.tbPais.MaxLength = 150
        Me.tbPais.Name = "tbPais"
        Me.tbPais.Size = New System.Drawing.Size(308, 21)
        Me.tbPais.TabIndex = 10
        '
        'Label100
        '
        Me.Label100.AutoSize = True
        Me.Label100.Location = New System.Drawing.Point(217, 108)
        Me.Label100.Name = "Label100"
        Me.Label100.Size = New System.Drawing.Size(91, 15)
        Me.Label100.TabIndex = 60
        Me.Label100.Text = "No. Exterior :"
        '
        'tbEstado
        '
        Me.tbEstado.Location = New System.Drawing.Point(315, 263)
        Me.tbEstado.MaxLength = 150
        Me.tbEstado.Name = "tbEstado"
        Me.tbEstado.Size = New System.Drawing.Size(308, 21)
        Me.tbEstado.TabIndex = 9
        '
        'Label99
        '
        Me.Label99.AutoSize = True
        Me.Label99.Location = New System.Drawing.Point(222, 135)
        Me.Label99.Name = "Label99"
        Me.Label99.Size = New System.Drawing.Size(87, 15)
        Me.Label99.TabIndex = 58
        Me.Label99.Text = "No. Interior :"
        '
        'tbMunicipio
        '
        Me.tbMunicipio.Location = New System.Drawing.Point(315, 236)
        Me.tbMunicipio.MaxLength = 150
        Me.tbMunicipio.Name = "tbMunicipio"
        Me.tbMunicipio.Size = New System.Drawing.Size(308, 21)
        Me.tbMunicipio.TabIndex = 8
        '
        'Label98
        '
        Me.Label98.AutoSize = True
        Me.Label98.Location = New System.Drawing.Point(245, 162)
        Me.Label98.Name = "Label98"
        Me.Label98.Size = New System.Drawing.Size(64, 15)
        Me.Label98.TabIndex = 56
        Me.Label98.Text = "Colonia :"
        '
        'tbReferencia
        '
        Me.tbReferencia.Location = New System.Drawing.Point(315, 210)
        Me.tbReferencia.MaxLength = 150
        Me.tbReferencia.Name = "tbReferencia"
        Me.tbReferencia.Size = New System.Drawing.Size(308, 21)
        Me.tbReferencia.TabIndex = 7
        '
        'Label97
        '
        Me.Label97.AutoSize = True
        Me.Label97.Location = New System.Drawing.Point(230, 189)
        Me.Label97.Name = "Label97"
        Me.Label97.Size = New System.Drawing.Size(78, 15)
        Me.Label97.TabIndex = 54
        Me.Label97.Text = "Localidad :"
        '
        'tbrfc
        '
        Me.tbrfc.Location = New System.Drawing.Point(315, 21)
        Me.tbrfc.MaxLength = 50
        Me.tbrfc.Name = "tbrfc"
        Me.tbrfc.Size = New System.Drawing.Size(308, 21)
        Me.tbrfc.TabIndex = 0
        '
        'Label96
        '
        Me.Label96.AutoSize = True
        Me.Label96.Location = New System.Drawing.Point(224, 216)
        Me.Label96.Name = "Label96"
        Me.Label96.Size = New System.Drawing.Size(85, 15)
        Me.Label96.TabIndex = 52
        Me.Label96.Text = "Referencia :"
        '
        'tbNombre
        '
        Me.tbNombre.Location = New System.Drawing.Point(315, 48)
        Me.tbNombre.MaxLength = 150
        Me.tbNombre.Name = "tbNombre"
        Me.tbNombre.Size = New System.Drawing.Size(308, 21)
        Me.tbNombre.TabIndex = 1
        '
        'Label95
        '
        Me.Label95.AutoSize = True
        Me.Label95.Location = New System.Drawing.Point(230, 242)
        Me.Label95.Name = "Label95"
        Me.Label95.Size = New System.Drawing.Size(78, 15)
        Me.Label95.TabIndex = 50
        Me.Label95.Text = "Municipio :"
        '
        'tbCalle
        '
        Me.tbCalle.Location = New System.Drawing.Point(315, 75)
        Me.tbCalle.MaxLength = 250
        Me.tbCalle.Name = "tbCalle"
        Me.tbCalle.Size = New System.Drawing.Size(308, 21)
        Me.tbCalle.TabIndex = 2
        '
        'Label94
        '
        Me.Label94.AutoSize = True
        Me.Label94.Location = New System.Drawing.Point(250, 269)
        Me.Label94.Name = "Label94"
        Me.Label94.Size = New System.Drawing.Size(59, 15)
        Me.Label94.TabIndex = 48
        Me.Label94.Text = "Estado :"
        '
        'tbNoExt
        '
        Me.tbNoExt.Location = New System.Drawing.Point(315, 102)
        Me.tbNoExt.MaxLength = 50
        Me.tbNoExt.Name = "tbNoExt"
        Me.tbNoExt.Size = New System.Drawing.Size(209, 21)
        Me.tbNoExt.TabIndex = 3
        '
        'Label93
        '
        Me.Label93.AutoSize = True
        Me.Label93.Location = New System.Drawing.Point(206, 323)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(104, 15)
        Me.Label93.TabIndex = 46
        Me.Label93.Text = "Código Postal :"
        '
        'tbNoInt
        '
        Me.tbNoInt.Location = New System.Drawing.Point(315, 129)
        Me.tbNoInt.MaxLength = 50
        Me.tbNoInt.Name = "tbNoInt"
        Me.tbNoInt.Size = New System.Drawing.Size(209, 21)
        Me.tbNoInt.TabIndex = 4
        '
        'Label92
        '
        Me.Label92.AutoSize = True
        Me.Label92.Location = New System.Drawing.Point(267, 296)
        Me.Label92.Name = "Label92"
        Me.Label92.Size = New System.Drawing.Size(43, 15)
        Me.Label92.TabIndex = 44
        Me.Label92.Text = "País :"
        '
        'tbColonia
        '
        Me.tbColonia.Location = New System.Drawing.Point(315, 156)
        Me.tbColonia.MaxLength = 150
        Me.tbColonia.Name = "tbColonia"
        Me.tbColonia.Size = New System.Drawing.Size(308, 21)
        Me.tbColonia.TabIndex = 5
        '
        'Label91
        '
        Me.Label91.AutoSize = True
        Me.Label91.Location = New System.Drawing.Point(237, 350)
        Me.Label91.Name = "Label91"
        Me.Label91.Size = New System.Drawing.Size(71, 15)
        Me.Label91.TabIndex = 42
        Me.Label91.Text = "Teléfono :"
        '
        'tbLocalidad
        '
        Me.tbLocalidad.Location = New System.Drawing.Point(315, 183)
        Me.tbLocalidad.MaxLength = 150
        Me.tbLocalidad.Name = "tbLocalidad"
        Me.tbLocalidad.Size = New System.Drawing.Size(308, 21)
        Me.tbLocalidad.TabIndex = 6
        '
        'Label90
        '
        Me.Label90.AutoSize = True
        Me.Label90.Location = New System.Drawing.Point(256, 377)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(52, 15)
        Me.Label90.TabIndex = 40
        Me.Label90.Text = "Email :"
        '
        'Button17
        '
        Me.Button17.BackColor = System.Drawing.SystemColors.Control
        Me.Button17.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button17.ForeColor = System.Drawing.Color.Black
        Me.Button17.Location = New System.Drawing.Point(714, 523)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(136, 36)
        Me.Button17.TabIndex = 14
        Me.Button17.Text = "&GUARDAR"
        Me.Button17.UseVisualStyleBackColor = False
        '
        'tbEmail
        '
        Me.tbEmail.Location = New System.Drawing.Point(315, 371)
        Me.tbEmail.MaxLength = 50
        Me.tbEmail.Name = "tbEmail"
        Me.tbEmail.Size = New System.Drawing.Size(209, 21)
        Me.tbEmail.TabIndex = 13
        '
        'Label89
        '
        Me.Label89.AutoSize = True
        Me.Label89.Location = New System.Drawing.Point(266, 27)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(42, 15)
        Me.Label89.TabIndex = 0
        Me.Label89.Text = "RFC :"
        '
        'tbPuntos
        '
        Me.tbPuntos.Controls.Add(Me.Label123)
        Me.tbPuntos.Controls.Add(Me.tcPuntos)
        Me.tbPuntos.Location = New System.Drawing.Point(4, 84)
        Me.tbPuntos.Name = "tbPuntos"
        Me.tbPuntos.Size = New System.Drawing.Size(865, 572)
        Me.tbPuntos.TabIndex = 23
        Me.tbPuntos.Text = "Puntos de Antigüedad"
        Me.tbPuntos.UseVisualStyleBackColor = True
        '
        'Label123
        '
        Me.Label123.AutoSize = True
        Me.Label123.BackColor = System.Drawing.Color.Black
        Me.Label123.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label123.ForeColor = System.Drawing.Color.White
        Me.Label123.Location = New System.Drawing.Point(34, 18)
        Me.Label123.Name = "Label123"
        Me.Label123.Size = New System.Drawing.Size(187, 20)
        Me.Label123.TabIndex = 67
        Me.Label123.Text = "Puntos de Antigüedad"
        '
        'tcPuntos
        '
        Me.tcPuntos.Controls.Add(Me.tbPuntosRangos)
        Me.tcPuntos.Controls.Add(Me.tbPuntosServicios)
        Me.tcPuntos.Controls.Add(Me.tbPuntosServiciosAdic)
        Me.tcPuntos.Controls.Add(Me.tbPuntosCajas)
        Me.tcPuntos.Location = New System.Drawing.Point(38, 41)
        Me.tcPuntos.Name = "tcPuntos"
        Me.tcPuntos.SelectedIndex = 0
        Me.tcPuntos.Size = New System.Drawing.Size(769, 517)
        Me.tcPuntos.TabIndex = 0
        '
        'tbPuntosRangos
        '
        Me.tbPuntosRangos.Controls.Add(Me.bnEliminarRango)
        Me.tbPuntosRangos.Controls.Add(Me.bnAgregarRango)
        Me.tbPuntosRangos.Controls.Add(Me.Label109)
        Me.tbPuntosRangos.Controls.Add(Me.tbRangoFin)
        Me.tbPuntosRangos.Controls.Add(Me.dgRangos)
        Me.tbPuntosRangos.Controls.Add(Me.Label108)
        Me.tbPuntosRangos.Controls.Add(Me.tbRangoIni)
        Me.tbPuntosRangos.Location = New System.Drawing.Point(4, 24)
        Me.tbPuntosRangos.Name = "tbPuntosRangos"
        Me.tbPuntosRangos.Padding = New System.Windows.Forms.Padding(3)
        Me.tbPuntosRangos.Size = New System.Drawing.Size(761, 489)
        Me.tbPuntosRangos.TabIndex = 0
        Me.tbPuntosRangos.Text = "Rangos"
        Me.tbPuntosRangos.UseVisualStyleBackColor = True
        '
        'bnEliminarRango
        '
        Me.bnEliminarRango.Location = New System.Drawing.Point(581, 152)
        Me.bnEliminarRango.Name = "bnEliminarRango"
        Me.bnEliminarRango.Size = New System.Drawing.Size(75, 23)
        Me.bnEliminarRango.TabIndex = 6
        Me.bnEliminarRango.Text = "&Eliminar"
        Me.bnEliminarRango.UseVisualStyleBackColor = True
        '
        'bnAgregarRango
        '
        Me.bnAgregarRango.Location = New System.Drawing.Point(581, 108)
        Me.bnAgregarRango.Name = "bnAgregarRango"
        Me.bnAgregarRango.Size = New System.Drawing.Size(75, 23)
        Me.bnAgregarRango.TabIndex = 5
        Me.bnAgregarRango.Text = "&Agregar"
        Me.bnAgregarRango.UseVisualStyleBackColor = True
        '
        'Label109
        '
        Me.Label109.AutoSize = True
        Me.Label109.Location = New System.Drawing.Point(232, 116)
        Me.Label109.Name = "Label109"
        Me.Label109.Size = New System.Drawing.Size(54, 15)
        Me.Label109.TabIndex = 4
        Me.Label109.Text = "Al mes:"
        '
        'tbRangoFin
        '
        Me.tbRangoFin.Location = New System.Drawing.Point(292, 110)
        Me.tbRangoFin.Name = "tbRangoFin"
        Me.tbRangoFin.Size = New System.Drawing.Size(268, 21)
        Me.tbRangoFin.TabIndex = 3
        '
        'dgRangos
        '
        Me.dgRangos.AllowUserToAddRows = False
        Me.dgRangos.AllowUserToDeleteRows = False
        Me.dgRangos.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgRangos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgRangos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdRango, Me.Rango})
        Me.dgRangos.Location = New System.Drawing.Point(239, 152)
        Me.dgRangos.Name = "dgRangos"
        Me.dgRangos.ReadOnly = True
        Me.dgRangos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgRangos.Size = New System.Drawing.Size(321, 255)
        Me.dgRangos.TabIndex = 2
        '
        'IdRango
        '
        Me.IdRango.DataPropertyName = "IdRango"
        Me.IdRango.HeaderText = "IdRango"
        Me.IdRango.Name = "IdRango"
        Me.IdRango.ReadOnly = True
        Me.IdRango.Visible = False
        '
        'Rango
        '
        Me.Rango.DataPropertyName = "Rango"
        Me.Rango.HeaderText = "Rangos"
        Me.Rango.Name = "Rango"
        Me.Rango.ReadOnly = True
        Me.Rango.Width = 250
        '
        'Label108
        '
        Me.Label108.AutoSize = True
        Me.Label108.Location = New System.Drawing.Point(222, 87)
        Me.Label108.Name = "Label108"
        Me.Label108.Size = New System.Drawing.Size(64, 15)
        Me.Label108.TabIndex = 1
        Me.Label108.Text = "Del mes:"
        '
        'tbRangoIni
        '
        Me.tbRangoIni.Location = New System.Drawing.Point(292, 81)
        Me.tbRangoIni.Name = "tbRangoIni"
        Me.tbRangoIni.Size = New System.Drawing.Size(268, 21)
        Me.tbRangoIni.TabIndex = 0
        '
        'tbPuntosServicios
        '
        Me.tbPuntosServicios.Controls.Add(Me.Label110)
        Me.tbPuntosServicios.Controls.Add(Me.Label113)
        Me.tbPuntosServicios.Controls.Add(Me.Label111)
        Me.tbPuntosServicios.Controls.Add(Me.cbTipSerPuntosAnti)
        Me.tbPuntosServicios.Controls.Add(Me.cbServicioPuntosAnti)
        Me.tbPuntosServicios.Controls.Add(Me.bnEliminarPuntosServicio)
        Me.tbPuntosServicios.Controls.Add(Me.bnAgregarPuntosServicio)
        Me.tbPuntosServicios.Controls.Add(Me.Label112)
        Me.tbPuntosServicios.Controls.Add(Me.dgPuntosServicios)
        Me.tbPuntosServicios.Controls.Add(Me.tboxPuntosAnti)
        Me.tbPuntosServicios.Controls.Add(Me.cbRangosPuntosAnti)
        Me.tbPuntosServicios.Location = New System.Drawing.Point(4, 24)
        Me.tbPuntosServicios.Name = "tbPuntosServicios"
        Me.tbPuntosServicios.Padding = New System.Windows.Forms.Padding(3)
        Me.tbPuntosServicios.Size = New System.Drawing.Size(761, 489)
        Me.tbPuntosServicios.TabIndex = 1
        Me.tbPuntosServicios.Text = "Servicios"
        Me.tbPuntosServicios.UseVisualStyleBackColor = True
        '
        'Label110
        '
        Me.Label110.AutoSize = True
        Me.Label110.Location = New System.Drawing.Point(165, 34)
        Me.Label110.Name = "Label110"
        Me.Label110.Size = New System.Drawing.Size(83, 15)
        Me.Label110.TabIndex = 21
        Me.Label110.Text = "Antigüedad:"
        '
        'Label113
        '
        Me.Label113.AutoSize = True
        Me.Label113.Location = New System.Drawing.Point(186, 92)
        Me.Label113.Name = "Label113"
        Me.Label113.Size = New System.Drawing.Size(62, 15)
        Me.Label113.TabIndex = 11
        Me.Label113.Text = "Servicio:"
        '
        'Label111
        '
        Me.Label111.AutoSize = True
        Me.Label111.Location = New System.Drawing.Point(134, 63)
        Me.Label111.Name = "Label111"
        Me.Label111.Size = New System.Drawing.Size(114, 15)
        Me.Label111.TabIndex = 10
        Me.Label111.Text = "Tipo de Servicio:"
        '
        'cbTipSerPuntosAnti
        '
        Me.cbTipSerPuntosAnti.DisplayMember = "Concepto"
        Me.cbTipSerPuntosAnti.FormattingEnabled = True
        Me.cbTipSerPuntosAnti.Location = New System.Drawing.Point(254, 55)
        Me.cbTipSerPuntosAnti.Name = "cbTipSerPuntosAnti"
        Me.cbTipSerPuntosAnti.Size = New System.Drawing.Size(310, 23)
        Me.cbTipSerPuntosAnti.TabIndex = 9
        Me.cbTipSerPuntosAnti.ValueMember = "Clv_TipSer"
        '
        'cbServicioPuntosAnti
        '
        Me.cbServicioPuntosAnti.DisplayMember = "Descripcion"
        Me.cbServicioPuntosAnti.FormattingEnabled = True
        Me.cbServicioPuntosAnti.Location = New System.Drawing.Point(254, 84)
        Me.cbServicioPuntosAnti.Name = "cbServicioPuntosAnti"
        Me.cbServicioPuntosAnti.Size = New System.Drawing.Size(310, 23)
        Me.cbServicioPuntosAnti.TabIndex = 8
        Me.cbServicioPuntosAnti.ValueMember = "Clv_Servicio"
        '
        'bnEliminarPuntosServicio
        '
        Me.bnEliminarPuntosServicio.Location = New System.Drawing.Point(592, 157)
        Me.bnEliminarPuntosServicio.Name = "bnEliminarPuntosServicio"
        Me.bnEliminarPuntosServicio.Size = New System.Drawing.Size(75, 23)
        Me.bnEliminarPuntosServicio.TabIndex = 7
        Me.bnEliminarPuntosServicio.Text = "&Eliminar"
        Me.bnEliminarPuntosServicio.UseVisualStyleBackColor = True
        '
        'bnAgregarPuntosServicio
        '
        Me.bnAgregarPuntosServicio.Location = New System.Drawing.Point(592, 115)
        Me.bnAgregarPuntosServicio.Name = "bnAgregarPuntosServicio"
        Me.bnAgregarPuntosServicio.Size = New System.Drawing.Size(75, 23)
        Me.bnAgregarPuntosServicio.TabIndex = 6
        Me.bnAgregarPuntosServicio.Text = "&Agregar"
        Me.bnAgregarPuntosServicio.UseVisualStyleBackColor = True
        '
        'Label112
        '
        Me.Label112.AutoSize = True
        Me.Label112.Location = New System.Drawing.Point(193, 119)
        Me.Label112.Name = "Label112"
        Me.Label112.Size = New System.Drawing.Size(55, 15)
        Me.Label112.TabIndex = 5
        Me.Label112.Text = "Puntos:"
        '
        'dgPuntosServicios
        '
        Me.dgPuntosServicios.AllowUserToAddRows = False
        Me.dgPuntosServicios.AllowUserToDeleteRows = False
        Me.dgPuntosServicios.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgPuntosServicios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgPuntosServicios.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdPuntos, Me.Servicio, Me.Puntos})
        Me.dgPuntosServicios.Location = New System.Drawing.Point(137, 157)
        Me.dgPuntosServicios.Name = "dgPuntosServicios"
        Me.dgPuntosServicios.ReadOnly = True
        Me.dgPuntosServicios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgPuntosServicios.Size = New System.Drawing.Size(427, 282)
        Me.dgPuntosServicios.TabIndex = 4
        '
        'IdPuntos
        '
        Me.IdPuntos.DataPropertyName = "IdPuntos"
        Me.IdPuntos.HeaderText = "IdPuntos"
        Me.IdPuntos.Name = "IdPuntos"
        Me.IdPuntos.ReadOnly = True
        Me.IdPuntos.Visible = False
        '
        'Servicio
        '
        Me.Servicio.DataPropertyName = "Servicio"
        Me.Servicio.HeaderText = "Servicio"
        Me.Servicio.Name = "Servicio"
        Me.Servicio.ReadOnly = True
        Me.Servicio.Width = 250
        '
        'Puntos
        '
        Me.Puntos.DataPropertyName = "Puntos"
        Me.Puntos.HeaderText = "Puntos"
        Me.Puntos.Name = "Puntos"
        Me.Puntos.ReadOnly = True
        '
        'tboxPuntosAnti
        '
        Me.tboxPuntosAnti.Location = New System.Drawing.Point(254, 113)
        Me.tboxPuntosAnti.Name = "tboxPuntosAnti"
        Me.tboxPuntosAnti.Size = New System.Drawing.Size(310, 21)
        Me.tboxPuntosAnti.TabIndex = 2
        '
        'cbRangosPuntosAnti
        '
        Me.cbRangosPuntosAnti.DisplayMember = "Rango"
        Me.cbRangosPuntosAnti.FormattingEnabled = True
        Me.cbRangosPuntosAnti.Location = New System.Drawing.Point(254, 26)
        Me.cbRangosPuntosAnti.Name = "cbRangosPuntosAnti"
        Me.cbRangosPuntosAnti.Size = New System.Drawing.Size(310, 23)
        Me.cbRangosPuntosAnti.TabIndex = 0
        Me.cbRangosPuntosAnti.ValueMember = "IdRango"
        '
        'tbPuntosServiciosAdic
        '
        Me.tbPuntosServiciosAdic.Controls.Add(Me.Label117)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.Label118)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.tbPuntosAntiServicios)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.Label114)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.Label115)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.cbTipSerPuntosAntiServicios)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.cbServicioPuntosAntiServicios)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.bnEliminarPuntosAntiServicios)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.bnAgregarPuntosAntiServicios)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.Label116)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.dgPagosAntiServAdic)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.tbPuntosAntiServiciosPagos)
        Me.tbPuntosServiciosAdic.Controls.Add(Me.cbRangosPuntosAntiServicios)
        Me.tbPuntosServiciosAdic.Location = New System.Drawing.Point(4, 24)
        Me.tbPuntosServiciosAdic.Name = "tbPuntosServiciosAdic"
        Me.tbPuntosServiciosAdic.Size = New System.Drawing.Size(761, 489)
        Me.tbPuntosServiciosAdic.TabIndex = 2
        Me.tbPuntosServiciosAdic.Text = "Servicios Adicionales"
        Me.tbPuntosServiciosAdic.UseVisualStyleBackColor = True
        '
        'Label117
        '
        Me.Label117.AutoSize = True
        Me.Label117.Location = New System.Drawing.Point(215, 29)
        Me.Label117.Name = "Label117"
        Me.Label117.Size = New System.Drawing.Size(83, 15)
        Me.Label117.TabIndex = 25
        Me.Label117.Text = "Antigüedad:"
        '
        'Label118
        '
        Me.Label118.AutoSize = True
        Me.Label118.Location = New System.Drawing.Point(243, 141)
        Me.Label118.Name = "Label118"
        Me.Label118.Size = New System.Drawing.Size(55, 15)
        Me.Label118.TabIndex = 24
        Me.Label118.Text = "Puntos:"
        '
        'tbPuntosAntiServicios
        '
        Me.tbPuntosAntiServicios.Location = New System.Drawing.Point(304, 135)
        Me.tbPuntosAntiServicios.Name = "tbPuntosAntiServicios"
        Me.tbPuntosAntiServicios.Size = New System.Drawing.Size(310, 21)
        Me.tbPuntosAntiServicios.TabIndex = 23
        '
        'Label114
        '
        Me.Label114.AutoSize = True
        Me.Label114.Location = New System.Drawing.Point(236, 87)
        Me.Label114.Name = "Label114"
        Me.Label114.Size = New System.Drawing.Size(62, 15)
        Me.Label114.TabIndex = 22
        Me.Label114.Text = "Servicio:"
        '
        'Label115
        '
        Me.Label115.AutoSize = True
        Me.Label115.Location = New System.Drawing.Point(184, 58)
        Me.Label115.Name = "Label115"
        Me.Label115.Size = New System.Drawing.Size(114, 15)
        Me.Label115.TabIndex = 21
        Me.Label115.Text = "Tipo de Servicio:"
        '
        'cbTipSerPuntosAntiServicios
        '
        Me.cbTipSerPuntosAntiServicios.DisplayMember = "Concepto"
        Me.cbTipSerPuntosAntiServicios.FormattingEnabled = True
        Me.cbTipSerPuntosAntiServicios.Location = New System.Drawing.Point(304, 50)
        Me.cbTipSerPuntosAntiServicios.Name = "cbTipSerPuntosAntiServicios"
        Me.cbTipSerPuntosAntiServicios.Size = New System.Drawing.Size(310, 23)
        Me.cbTipSerPuntosAntiServicios.TabIndex = 20
        Me.cbTipSerPuntosAntiServicios.ValueMember = "Clv_TipSer"
        '
        'cbServicioPuntosAntiServicios
        '
        Me.cbServicioPuntosAntiServicios.DisplayMember = "Descripcion"
        Me.cbServicioPuntosAntiServicios.FormattingEnabled = True
        Me.cbServicioPuntosAntiServicios.Location = New System.Drawing.Point(304, 79)
        Me.cbServicioPuntosAntiServicios.Name = "cbServicioPuntosAntiServicios"
        Me.cbServicioPuntosAntiServicios.Size = New System.Drawing.Size(310, 23)
        Me.cbServicioPuntosAntiServicios.TabIndex = 19
        Me.cbServicioPuntosAntiServicios.ValueMember = "Clv_Servicio"
        '
        'bnEliminarPuntosAntiServicios
        '
        Me.bnEliminarPuntosAntiServicios.Location = New System.Drawing.Point(629, 175)
        Me.bnEliminarPuntosAntiServicios.Name = "bnEliminarPuntosAntiServicios"
        Me.bnEliminarPuntosAntiServicios.Size = New System.Drawing.Size(75, 23)
        Me.bnEliminarPuntosAntiServicios.TabIndex = 18
        Me.bnEliminarPuntosAntiServicios.Text = "&Eliminar"
        Me.bnEliminarPuntosAntiServicios.UseVisualStyleBackColor = True
        '
        'bnAgregarPuntosAntiServicios
        '
        Me.bnAgregarPuntosAntiServicios.Location = New System.Drawing.Point(629, 133)
        Me.bnAgregarPuntosAntiServicios.Name = "bnAgregarPuntosAntiServicios"
        Me.bnAgregarPuntosAntiServicios.Size = New System.Drawing.Size(75, 23)
        Me.bnAgregarPuntosAntiServicios.TabIndex = 17
        Me.bnAgregarPuntosAntiServicios.Text = "&Agregar"
        Me.bnAgregarPuntosAntiServicios.UseVisualStyleBackColor = True
        '
        'Label116
        '
        Me.Label116.AutoSize = True
        Me.Label116.Location = New System.Drawing.Point(179, 114)
        Me.Label116.Name = "Label116"
        Me.Label116.Size = New System.Drawing.Size(119, 15)
        Me.Label116.TabIndex = 16
        Me.Label116.Text = "Pagos Puntuales:"
        '
        'dgPagosAntiServAdic
        '
        Me.dgPagosAntiServAdic.AllowUserToAddRows = False
        Me.dgPagosAntiServAdic.AllowUserToDeleteRows = False
        Me.dgPagosAntiServAdic.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgPagosAntiServAdic.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgPagosAntiServAdic.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.PagosPuntuales, Me.PuntosServicio})
        Me.dgPagosAntiServAdic.Location = New System.Drawing.Point(61, 175)
        Me.dgPagosAntiServAdic.Name = "dgPagosAntiServAdic"
        Me.dgPagosAntiServAdic.ReadOnly = True
        Me.dgPagosAntiServAdic.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgPagosAntiServAdic.Size = New System.Drawing.Size(553, 260)
        Me.dgPagosAntiServAdic.TabIndex = 15
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "IdPuntos"
        Me.DataGridViewTextBoxColumn4.HeaderText = "IdPuntos"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Servicio"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Servicio"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 250
        '
        'PagosPuntuales
        '
        Me.PagosPuntuales.DataPropertyName = "PagosPuntuales"
        Me.PagosPuntuales.HeaderText = "Pagos Puntuales"
        Me.PagosPuntuales.Name = "PagosPuntuales"
        Me.PagosPuntuales.ReadOnly = True
        Me.PagosPuntuales.Width = 150
        '
        'PuntosServicio
        '
        Me.PuntosServicio.DataPropertyName = "Puntos"
        Me.PuntosServicio.HeaderText = "Puntos"
        Me.PuntosServicio.Name = "PuntosServicio"
        Me.PuntosServicio.ReadOnly = True
        '
        'tbPuntosAntiServiciosPagos
        '
        Me.tbPuntosAntiServiciosPagos.Location = New System.Drawing.Point(304, 108)
        Me.tbPuntosAntiServiciosPagos.Name = "tbPuntosAntiServiciosPagos"
        Me.tbPuntosAntiServiciosPagos.Size = New System.Drawing.Size(310, 21)
        Me.tbPuntosAntiServiciosPagos.TabIndex = 14
        '
        'cbRangosPuntosAntiServicios
        '
        Me.cbRangosPuntosAntiServicios.DisplayMember = "Rango"
        Me.cbRangosPuntosAntiServicios.FormattingEnabled = True
        Me.cbRangosPuntosAntiServicios.Location = New System.Drawing.Point(304, 21)
        Me.cbRangosPuntosAntiServicios.Name = "cbRangosPuntosAntiServicios"
        Me.cbRangosPuntosAntiServicios.Size = New System.Drawing.Size(310, 23)
        Me.cbRangosPuntosAntiServicios.TabIndex = 12
        Me.cbRangosPuntosAntiServicios.ValueMember = "IdRango"
        '
        'tbPuntosCajas
        '
        Me.tbPuntosCajas.Controls.Add(Me.tbPuntosAntiArticulos4ta)
        Me.tbPuntosCajas.Controls.Add(Me.Label128)
        Me.tbPuntosCajas.Controls.Add(Me.tbPuntosAntiArticulos2da3er)
        Me.tbPuntosCajas.Controls.Add(Me.Label127)
        Me.tbPuntosCajas.Controls.Add(Me.bnGuardarNumero)
        Me.tbPuntosCajas.Controls.Add(Me.Label126)
        Me.tbPuntosCajas.Controls.Add(Me.tbCantidad)
        Me.tbPuntosCajas.Controls.Add(Me.bnEliminarPuntosAntiArticulos)
        Me.tbPuntosCajas.Controls.Add(Me.bnAgregarPuntosAntiArticulos)
        Me.tbPuntosCajas.Controls.Add(Me.Label121)
        Me.tbPuntosCajas.Controls.Add(Me.cbArticuloPuntosAntiArticulos)
        Me.tbPuntosCajas.Controls.Add(Me.Label122)
        Me.tbPuntosCajas.Controls.Add(Me.tbPuntosAntiArticulos)
        Me.tbPuntosCajas.Controls.Add(Me.bnAbajoPuntosAntiArticulos)
        Me.tbPuntosCajas.Controls.Add(Me.bnArribaPuntosAntiArticulos)
        Me.tbPuntosCajas.Controls.Add(Me.Label120)
        Me.tbPuntosCajas.Controls.Add(Me.dgPuntosAntiArticulos)
        Me.tbPuntosCajas.Controls.Add(Me.Label119)
        Me.tbPuntosCajas.Controls.Add(Me.cbRangoPuntosAntiArticulos)
        Me.tbPuntosCajas.Location = New System.Drawing.Point(4, 24)
        Me.tbPuntosCajas.Name = "tbPuntosCajas"
        Me.tbPuntosCajas.Size = New System.Drawing.Size(761, 489)
        Me.tbPuntosCajas.TabIndex = 3
        Me.tbPuntosCajas.Text = "Cajas"
        Me.tbPuntosCajas.UseVisualStyleBackColor = True
        '
        'tbPuntosAntiArticulos4ta
        '
        Me.tbPuntosAntiArticulos4ta.Location = New System.Drawing.Point(334, 160)
        Me.tbPuntosAntiArticulos4ta.Name = "tbPuntosAntiArticulos4ta"
        Me.tbPuntosAntiArticulos4ta.Size = New System.Drawing.Size(157, 21)
        Me.tbPuntosAntiArticulos4ta.TabIndex = 4
        '
        'Label128
        '
        Me.Label128.AutoSize = True
        Me.Label128.Location = New System.Drawing.Point(138, 166)
        Me.Label128.Name = "Label128"
        Me.Label128.Size = New System.Drawing.Size(190, 15)
        Me.Label128.TabIndex = 37
        Me.Label128.Text = "Puntos 4ta caja en adelante:"
        '
        'tbPuntosAntiArticulos2da3er
        '
        Me.tbPuntosAntiArticulos2da3er.Location = New System.Drawing.Point(334, 133)
        Me.tbPuntosAntiArticulos2da3er.Name = "tbPuntosAntiArticulos2da3er"
        Me.tbPuntosAntiArticulos2da3er.Size = New System.Drawing.Size(157, 21)
        Me.tbPuntosAntiArticulos2da3er.TabIndex = 3
        '
        'Label127
        '
        Me.Label127.AutoSize = True
        Me.Label127.Location = New System.Drawing.Point(177, 140)
        Me.Label127.Name = "Label127"
        Me.Label127.Size = New System.Drawing.Size(149, 15)
        Me.Label127.TabIndex = 35
        Me.Label127.Text = "Puntos 2da y 3er caja:"
        '
        'bnGuardarNumero
        '
        Me.bnGuardarNumero.Location = New System.Drawing.Point(11, 439)
        Me.bnGuardarNumero.Name = "bnGuardarNumero"
        Me.bnGuardarNumero.Size = New System.Drawing.Size(19, 23)
        Me.bnGuardarNumero.TabIndex = 8
        Me.bnGuardarNumero.Text = "&Guardar"
        Me.bnGuardarNumero.UseVisualStyleBackColor = True
        Me.bnGuardarNumero.Visible = False
        '
        'Label126
        '
        Me.Label126.Location = New System.Drawing.Point(8, 443)
        Me.Label126.Name = "Label126"
        Me.Label126.Size = New System.Drawing.Size(26, 35)
        Me.Label126.TabIndex = 33
        Me.Label126.Text = "Número de Cajas que debe tener el Cliente asignadas, cuando la prioridad de estas" & _
            ", no es Alta."
        Me.Label126.Visible = False
        '
        'tbCantidad
        '
        Me.tbCantidad.Location = New System.Drawing.Point(9, 445)
        Me.tbCantidad.Name = "tbCantidad"
        Me.tbCantidad.Size = New System.Drawing.Size(20, 21)
        Me.tbCantidad.TabIndex = 7
        Me.tbCantidad.Visible = False
        '
        'bnEliminarPuntosAntiArticulos
        '
        Me.bnEliminarPuntosAntiArticulos.Location = New System.Drawing.Point(686, 213)
        Me.bnEliminarPuntosAntiArticulos.Name = "bnEliminarPuntosAntiArticulos"
        Me.bnEliminarPuntosAntiArticulos.Size = New System.Drawing.Size(75, 23)
        Me.bnEliminarPuntosAntiArticulos.TabIndex = 6
        Me.bnEliminarPuntosAntiArticulos.Text = "&Eliminar"
        Me.bnEliminarPuntosAntiArticulos.UseVisualStyleBackColor = True
        '
        'bnAgregarPuntosAntiArticulos
        '
        Me.bnAgregarPuntosAntiArticulos.Location = New System.Drawing.Point(683, 162)
        Me.bnAgregarPuntosAntiArticulos.Name = "bnAgregarPuntosAntiArticulos"
        Me.bnAgregarPuntosAntiArticulos.Size = New System.Drawing.Size(75, 23)
        Me.bnAgregarPuntosAntiArticulos.TabIndex = 5
        Me.bnAgregarPuntosAntiArticulos.Text = "&Agregar"
        Me.bnAgregarPuntosAntiArticulos.UseVisualStyleBackColor = True
        '
        'Label121
        '
        Me.Label121.AutoSize = True
        Me.Label121.Location = New System.Drawing.Point(288, 85)
        Me.Label121.Name = "Label121"
        Me.Label121.Size = New System.Drawing.Size(40, 15)
        Me.Label121.TabIndex = 29
        Me.Label121.Text = "Caja:"
        '
        'cbArticuloPuntosAntiArticulos
        '
        Me.cbArticuloPuntosAntiArticulos.DisplayMember = "Descripcion"
        Me.cbArticuloPuntosAntiArticulos.FormattingEnabled = True
        Me.cbArticuloPuntosAntiArticulos.Location = New System.Drawing.Point(334, 77)
        Me.cbArticuloPuntosAntiArticulos.Name = "cbArticuloPuntosAntiArticulos"
        Me.cbArticuloPuntosAntiArticulos.Size = New System.Drawing.Size(310, 23)
        Me.cbArticuloPuntosAntiArticulos.TabIndex = 1
        Me.cbArticuloPuntosAntiArticulos.ValueMember = "NoArticulo"
        '
        'Label122
        '
        Me.Label122.AutoSize = True
        Me.Label122.Location = New System.Drawing.Point(215, 112)
        Me.Label122.Name = "Label122"
        Me.Label122.Size = New System.Drawing.Size(111, 15)
        Me.Label122.TabIndex = 27
        Me.Label122.Text = "Puntos 1er caja:"
        '
        'tbPuntosAntiArticulos
        '
        Me.tbPuntosAntiArticulos.Location = New System.Drawing.Point(334, 106)
        Me.tbPuntosAntiArticulos.Name = "tbPuntosAntiArticulos"
        Me.tbPuntosAntiArticulos.Size = New System.Drawing.Size(157, 21)
        Me.tbPuntosAntiArticulos.TabIndex = 2
        '
        'bnAbajoPuntosAntiArticulos
        '
        Me.bnAbajoPuntosAntiArticulos.Location = New System.Drawing.Point(11, 344)
        Me.bnAbajoPuntosAntiArticulos.Name = "bnAbajoPuntosAntiArticulos"
        Me.bnAbajoPuntosAntiArticulos.Size = New System.Drawing.Size(67, 23)
        Me.bnAbajoPuntosAntiArticulos.TabIndex = 25
        Me.bnAbajoPuntosAntiArticulos.Text = "A&bajo"
        Me.bnAbajoPuntosAntiArticulos.UseVisualStyleBackColor = True
        '
        'bnArribaPuntosAntiArticulos
        '
        Me.bnArribaPuntosAntiArticulos.Location = New System.Drawing.Point(11, 315)
        Me.bnArribaPuntosAntiArticulos.Name = "bnArribaPuntosAntiArticulos"
        Me.bnArribaPuntosAntiArticulos.Size = New System.Drawing.Size(67, 23)
        Me.bnArribaPuntosAntiArticulos.TabIndex = 24
        Me.bnArribaPuntosAntiArticulos.Text = "&Arriba"
        Me.bnArribaPuntosAntiArticulos.UseVisualStyleBackColor = True
        '
        'Label120
        '
        Me.Label120.Location = New System.Drawing.Point(6, 267)
        Me.Label120.Name = "Label120"
        Me.Label120.Size = New System.Drawing.Size(82, 35)
        Me.Label120.TabIndex = 23
        Me.Label120.Text = "Desplazar Jerarquía"
        Me.Label120.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dgPuntosAntiArticulos
        '
        Me.dgPuntosAntiArticulos.AllowUserToAddRows = False
        Me.dgPuntosAntiArticulos.AllowUserToDeleteRows = False
        Me.dgPuntosAntiArticulos.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgPuntosAntiArticulos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgPuntosAntiArticulos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdPuntosPuntosAntiArticulos, Me.JerarquiaPuntosAntiArticulos, Me.DescripcionPuntosAntiArticulos, Me.PuntosPuntosAntiArticulos, Me.PuntosSegundaYTercerCaja, Me.PuntosCuartaCaja})
        Me.dgPuntosAntiArticulos.Location = New System.Drawing.Point(93, 213)
        Me.dgPuntosAntiArticulos.Name = "dgPuntosAntiArticulos"
        Me.dgPuntosAntiArticulos.ReadOnly = True
        Me.dgPuntosAntiArticulos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgPuntosAntiArticulos.Size = New System.Drawing.Size(587, 249)
        Me.dgPuntosAntiArticulos.TabIndex = 21
        '
        'IdPuntosPuntosAntiArticulos
        '
        Me.IdPuntosPuntosAntiArticulos.DataPropertyName = "IdPuntos"
        Me.IdPuntosPuntosAntiArticulos.HeaderText = "IdPuntosPuntosAntiArticulos"
        Me.IdPuntosPuntosAntiArticulos.Name = "IdPuntosPuntosAntiArticulos"
        Me.IdPuntosPuntosAntiArticulos.ReadOnly = True
        Me.IdPuntosPuntosAntiArticulos.Visible = False
        '
        'JerarquiaPuntosAntiArticulos
        '
        Me.JerarquiaPuntosAntiArticulos.DataPropertyName = "Jerarquia"
        Me.JerarquiaPuntosAntiArticulos.HeaderText = "Jerarquía"
        Me.JerarquiaPuntosAntiArticulos.Name = "JerarquiaPuntosAntiArticulos"
        Me.JerarquiaPuntosAntiArticulos.ReadOnly = True
        Me.JerarquiaPuntosAntiArticulos.Width = 80
        '
        'DescripcionPuntosAntiArticulos
        '
        Me.DescripcionPuntosAntiArticulos.DataPropertyName = "Descripcion"
        Me.DescripcionPuntosAntiArticulos.HeaderText = "Caja"
        Me.DescripcionPuntosAntiArticulos.Name = "DescripcionPuntosAntiArticulos"
        Me.DescripcionPuntosAntiArticulos.ReadOnly = True
        Me.DescripcionPuntosAntiArticulos.Width = 230
        '
        'PuntosPuntosAntiArticulos
        '
        Me.PuntosPuntosAntiArticulos.DataPropertyName = "Puntos"
        Me.PuntosPuntosAntiArticulos.HeaderText = "1er Caja"
        Me.PuntosPuntosAntiArticulos.Name = "PuntosPuntosAntiArticulos"
        Me.PuntosPuntosAntiArticulos.ReadOnly = True
        Me.PuntosPuntosAntiArticulos.Width = 60
        '
        'PuntosSegundaYTercerCaja
        '
        Me.PuntosSegundaYTercerCaja.DataPropertyName = "PuntosSegundaYTercerCaja"
        Me.PuntosSegundaYTercerCaja.HeaderText = "2da y 3er Caja"
        Me.PuntosSegundaYTercerCaja.Name = "PuntosSegundaYTercerCaja"
        Me.PuntosSegundaYTercerCaja.ReadOnly = True
        '
        'PuntosCuartaCaja
        '
        Me.PuntosCuartaCaja.DataPropertyName = "PuntosCuartaCaja"
        Me.PuntosCuartaCaja.HeaderText = "4ta Caja"
        Me.PuntosCuartaCaja.Name = "PuntosCuartaCaja"
        Me.PuntosCuartaCaja.ReadOnly = True
        Me.PuntosCuartaCaja.Width = 60
        '
        'Label119
        '
        Me.Label119.AutoSize = True
        Me.Label119.Location = New System.Drawing.Point(245, 54)
        Me.Label119.Name = "Label119"
        Me.Label119.Size = New System.Drawing.Size(83, 15)
        Me.Label119.TabIndex = 20
        Me.Label119.Text = "Antigüedad:"
        '
        'cbRangoPuntosAntiArticulos
        '
        Me.cbRangoPuntosAntiArticulos.DisplayMember = "Rango"
        Me.cbRangoPuntosAntiArticulos.FormattingEnabled = True
        Me.cbRangoPuntosAntiArticulos.Location = New System.Drawing.Point(334, 46)
        Me.cbRangoPuntosAntiArticulos.Name = "cbRangoPuntosAntiArticulos"
        Me.cbRangoPuntosAntiArticulos.Size = New System.Drawing.Size(310, 23)
        Me.cbRangoPuntosAntiArticulos.TabIndex = 0
        Me.cbRangoPuntosAntiArticulos.ValueMember = "IdRango"
        '
        'tbPromocionAntiguedad
        '
        Me.tbPromocionAntiguedad.Controls.Add(Me.bnGuardarTipoUsuario)
        Me.tbPromocionAntiguedad.Controls.Add(Me.Label131)
        Me.tbPromocionAntiguedad.Controls.Add(Me.dgvTipoUsuario)
        Me.tbPromocionAntiguedad.Controls.Add(Me.bnGuardarMeses)
        Me.tbPromocionAntiguedad.Controls.Add(Me.tbMesesAntiguedadPromocion)
        Me.tbPromocionAntiguedad.Controls.Add(Me.Label130)
        Me.tbPromocionAntiguedad.Controls.Add(Me.Label129)
        Me.tbPromocionAntiguedad.Location = New System.Drawing.Point(4, 84)
        Me.tbPromocionAntiguedad.Name = "tbPromocionAntiguedad"
        Me.tbPromocionAntiguedad.Size = New System.Drawing.Size(865, 572)
        Me.tbPromocionAntiguedad.TabIndex = 24
        Me.tbPromocionAntiguedad.Text = "Promoción de Antigüedad"
        Me.tbPromocionAntiguedad.UseVisualStyleBackColor = True
        '
        'bnGuardarTipoUsuario
        '
        Me.bnGuardarTipoUsuario.Location = New System.Drawing.Point(646, 169)
        Me.bnGuardarTipoUsuario.Name = "bnGuardarTipoUsuario"
        Me.bnGuardarTipoUsuario.Size = New System.Drawing.Size(75, 23)
        Me.bnGuardarTipoUsuario.TabIndex = 74
        Me.bnGuardarTipoUsuario.Text = "&Guardar"
        Me.bnGuardarTipoUsuario.UseVisualStyleBackColor = True
        '
        'Label131
        '
        Me.Label131.Location = New System.Drawing.Point(214, 128)
        Me.Label131.Name = "Label131"
        Me.Label131.Size = New System.Drawing.Size(401, 38)
        Me.Label131.TabIndex = 73
        Me.Label131.Text = "Tipos de Usuarios con acceso al módulo de Promoción de Antigüedad:"
        '
        'dgvTipoUsuario
        '
        Me.dgvTipoUsuario.AllowUserToAddRows = False
        Me.dgvTipoUsuario.AllowUserToDeleteRows = False
        Me.dgvTipoUsuario.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvTipoUsuario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTipoUsuario.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_TipoUsuario, Me.DescripcionTipoUsuario, Me.Acceso})
        Me.dgvTipoUsuario.GridColor = System.Drawing.Color.WhiteSmoke
        Me.dgvTipoUsuario.Location = New System.Drawing.Point(217, 169)
        Me.dgvTipoUsuario.Name = "dgvTipoUsuario"
        Me.dgvTipoUsuario.Size = New System.Drawing.Size(423, 279)
        Me.dgvTipoUsuario.TabIndex = 72
        '
        'Clv_TipoUsuario
        '
        Me.Clv_TipoUsuario.DataPropertyName = "Clv_TipoUsuario"
        Me.Clv_TipoUsuario.HeaderText = "Clv_TipoUsuario"
        Me.Clv_TipoUsuario.Name = "Clv_TipoUsuario"
        Me.Clv_TipoUsuario.Visible = False
        '
        'DescripcionTipoUsuario
        '
        Me.DescripcionTipoUsuario.DataPropertyName = "Descripcion"
        Me.DescripcionTipoUsuario.HeaderText = "Tipo Usuario"
        Me.DescripcionTipoUsuario.Name = "DescripcionTipoUsuario"
        Me.DescripcionTipoUsuario.Width = 250
        '
        'Acceso
        '
        Me.Acceso.DataPropertyName = "Acceso"
        Me.Acceso.HeaderText = "Acceso"
        Me.Acceso.Name = "Acceso"
        Me.Acceso.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Acceso.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'bnGuardarMeses
        '
        Me.bnGuardarMeses.Location = New System.Drawing.Point(502, 28)
        Me.bnGuardarMeses.Name = "bnGuardarMeses"
        Me.bnGuardarMeses.Size = New System.Drawing.Size(10, 13)
        Me.bnGuardarMeses.TabIndex = 71
        Me.bnGuardarMeses.Text = "&Guardar"
        Me.bnGuardarMeses.UseVisualStyleBackColor = True
        Me.bnGuardarMeses.Visible = False
        '
        'tbMesesAntiguedadPromocion
        '
        Me.tbMesesAntiguedadPromocion.Location = New System.Drawing.Point(474, 23)
        Me.tbMesesAntiguedadPromocion.Name = "tbMesesAntiguedadPromocion"
        Me.tbMesesAntiguedadPromocion.Size = New System.Drawing.Size(10, 21)
        Me.tbMesesAntiguedadPromocion.TabIndex = 70
        Me.tbMesesAntiguedadPromocion.Visible = False
        '
        'Label130
        '
        Me.Label130.Location = New System.Drawing.Point(441, 26)
        Me.Label130.Name = "Label130"
        Me.Label130.Size = New System.Drawing.Size(10, 15)
        Me.Label130.TabIndex = 69
        Me.Label130.Text = "Número de meses de Antigüedad para que aplique la promoción:"
        Me.Label130.Visible = False
        '
        'Label129
        '
        Me.Label129.AutoSize = True
        Me.Label129.BackColor = System.Drawing.Color.Black
        Me.Label129.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label129.ForeColor = System.Drawing.Color.White
        Me.Label129.Location = New System.Drawing.Point(43, 40)
        Me.Label129.Name = "Label129"
        Me.Label129.Size = New System.Drawing.Size(215, 20)
        Me.Label129.TabIndex = 68
        Me.Label129.Text = "Promoción de Antigüedad"
        '
        'tpPromPriMens
        '
        Me.tpPromPriMens.Controls.Add(Me.btnGuardarPM)
        Me.tpPromPriMens.Controls.Add(Me.btnAgregarPM)
        Me.tpPromPriMens.Controls.Add(Me.Label138)
        Me.tpPromPriMens.Controls.Add(Me.gbxMens0)
        Me.tpPromPriMens.Controls.Add(Me.gbxMensN)
        Me.tpPromPriMens.Controls.Add(Me.lblServicio)
        Me.tpPromPriMens.Controls.Add(Me.cbxServicio)
        Me.tpPromPriMens.Controls.Add(Me.lblTipServ)
        Me.tpPromPriMens.Controls.Add(Me.cbxTipoServ)
        Me.tpPromPriMens.Location = New System.Drawing.Point(4, 84)
        Me.tpPromPriMens.Name = "tpPromPriMens"
        Me.tpPromPriMens.Padding = New System.Windows.Forms.Padding(3)
        Me.tpPromPriMens.Size = New System.Drawing.Size(865, 572)
        Me.tpPromPriMens.TabIndex = 25
        Me.tpPromPriMens.Text = "Promoción Primera Mensualidad"
        Me.tpPromPriMens.UseVisualStyleBackColor = True
        '
        'btnGuardarPM
        '
        Me.btnGuardarPM.Location = New System.Drawing.Point(664, 216)
        Me.btnGuardarPM.Name = "btnGuardarPM"
        Me.btnGuardarPM.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardarPM.TabIndex = 71
        Me.btnGuardarPM.Text = "Guardar"
        Me.btnGuardarPM.UseVisualStyleBackColor = True
        '
        'btnAgregarPM
        '
        Me.btnAgregarPM.Location = New System.Drawing.Point(664, 187)
        Me.btnAgregarPM.Name = "btnAgregarPM"
        Me.btnAgregarPM.Size = New System.Drawing.Size(75, 23)
        Me.btnAgregarPM.TabIndex = 70
        Me.btnAgregarPM.Text = "Agregar"
        Me.btnAgregarPM.UseVisualStyleBackColor = True
        '
        'Label138
        '
        Me.Label138.AutoSize = True
        Me.Label138.BackColor = System.Drawing.Color.Black
        Me.Label138.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label138.ForeColor = System.Drawing.Color.White
        Me.Label138.Location = New System.Drawing.Point(73, 31)
        Me.Label138.Name = "Label138"
        Me.Label138.Size = New System.Drawing.Size(265, 20)
        Me.Label138.TabIndex = 69
        Me.Label138.Text = "Promoción Primera Mensualidad"
        '
        'gbxMens0
        '
        Me.gbxMens0.Controls.Add(Me.Label135)
        Me.gbxMens0.Controls.Add(Me.txtDescuentoC0)
        Me.gbxMens0.Controls.Add(Me.Label136)
        Me.gbxMens0.Controls.Add(Me.txtDescuento0)
        Me.gbxMens0.Controls.Add(Me.Label137)
        Me.gbxMens0.Controls.Add(Me.txtImporte0)
        Me.gbxMens0.Location = New System.Drawing.Point(363, 177)
        Me.gbxMens0.Name = "gbxMens0"
        Me.gbxMens0.Size = New System.Drawing.Size(268, 113)
        Me.gbxMens0.TabIndex = 5
        Me.gbxMens0.TabStop = False
        Me.gbxMens0.Text = "Contratación = 0"
        '
        'Label135
        '
        Me.Label135.AutoSize = True
        Me.Label135.Location = New System.Drawing.Point(9, 82)
        Me.Label135.Name = "Label135"
        Me.Label135.Size = New System.Drawing.Size(132, 15)
        Me.Label135.TabIndex = 11
        Me.Label135.Text = "Descuento Combo :"
        '
        'txtDescuentoC0
        '
        Me.txtDescuentoC0.Location = New System.Drawing.Point(147, 79)
        Me.txtDescuentoC0.Name = "txtDescuentoC0"
        Me.txtDescuentoC0.Size = New System.Drawing.Size(100, 21)
        Me.txtDescuentoC0.TabIndex = 10
        '
        'Label136
        '
        Me.Label136.AutoSize = True
        Me.Label136.Location = New System.Drawing.Point(58, 59)
        Me.Label136.Name = "Label136"
        Me.Label136.Size = New System.Drawing.Size(83, 15)
        Me.Label136.TabIndex = 9
        Me.Label136.Text = "Descuento :"
        '
        'txtDescuento0
        '
        Me.txtDescuento0.Location = New System.Drawing.Point(147, 53)
        Me.txtDescuento0.Name = "txtDescuento0"
        Me.txtDescuento0.Size = New System.Drawing.Size(100, 21)
        Me.txtDescuento0.TabIndex = 8
        '
        'Label137
        '
        Me.Label137.AutoSize = True
        Me.Label137.Location = New System.Drawing.Point(77, 32)
        Me.Label137.Name = "Label137"
        Me.Label137.Size = New System.Drawing.Size(64, 15)
        Me.Label137.TabIndex = 7
        Me.Label137.Text = "Importe :"
        '
        'txtImporte0
        '
        Me.txtImporte0.Location = New System.Drawing.Point(147, 26)
        Me.txtImporte0.Name = "txtImporte0"
        Me.txtImporte0.Size = New System.Drawing.Size(100, 21)
        Me.txtImporte0.TabIndex = 6
        '
        'gbxMensN
        '
        Me.gbxMensN.Controls.Add(Me.Label134)
        Me.gbxMensN.Controls.Add(Me.txtDescuentoC)
        Me.gbxMensN.Controls.Add(Me.Label133)
        Me.gbxMensN.Controls.Add(Me.txtDescuento)
        Me.gbxMensN.Controls.Add(Me.Label132)
        Me.gbxMensN.Controls.Add(Me.txtImporte)
        Me.gbxMensN.Location = New System.Drawing.Point(67, 177)
        Me.gbxMensN.Name = "gbxMensN"
        Me.gbxMensN.Size = New System.Drawing.Size(269, 113)
        Me.gbxMensN.TabIndex = 4
        Me.gbxMensN.TabStop = False
        Me.gbxMensN.Text = "Contratación > 0"
        '
        'Label134
        '
        Me.Label134.AutoSize = True
        Me.Label134.Location = New System.Drawing.Point(7, 82)
        Me.Label134.Name = "Label134"
        Me.Label134.Size = New System.Drawing.Size(132, 15)
        Me.Label134.TabIndex = 5
        Me.Label134.Text = "Descuento Combo :"
        '
        'txtDescuentoC
        '
        Me.txtDescuentoC.Location = New System.Drawing.Point(145, 79)
        Me.txtDescuentoC.Name = "txtDescuentoC"
        Me.txtDescuentoC.Size = New System.Drawing.Size(100, 21)
        Me.txtDescuentoC.TabIndex = 4
        '
        'Label133
        '
        Me.Label133.AutoSize = True
        Me.Label133.Location = New System.Drawing.Point(56, 59)
        Me.Label133.Name = "Label133"
        Me.Label133.Size = New System.Drawing.Size(83, 15)
        Me.Label133.TabIndex = 3
        Me.Label133.Text = "Descuento :"
        '
        'txtDescuento
        '
        Me.txtDescuento.Location = New System.Drawing.Point(145, 53)
        Me.txtDescuento.Name = "txtDescuento"
        Me.txtDescuento.Size = New System.Drawing.Size(100, 21)
        Me.txtDescuento.TabIndex = 2
        '
        'Label132
        '
        Me.Label132.AutoSize = True
        Me.Label132.Location = New System.Drawing.Point(75, 32)
        Me.Label132.Name = "Label132"
        Me.Label132.Size = New System.Drawing.Size(64, 15)
        Me.Label132.TabIndex = 1
        Me.Label132.Text = "Importe :"
        '
        'txtImporte
        '
        Me.txtImporte.Location = New System.Drawing.Point(145, 26)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(100, 21)
        Me.txtImporte.TabIndex = 0
        '
        'lblServicio
        '
        Me.lblServicio.AutoSize = True
        Me.lblServicio.Location = New System.Drawing.Point(157, 151)
        Me.lblServicio.Name = "lblServicio"
        Me.lblServicio.Size = New System.Drawing.Size(66, 15)
        Me.lblServicio.TabIndex = 3
        Me.lblServicio.Text = "Servicio :"
        '
        'cbxServicio
        '
        Me.cbxServicio.FormattingEnabled = True
        Me.cbxServicio.Location = New System.Drawing.Point(266, 148)
        Me.cbxServicio.Name = "cbxServicio"
        Me.cbxServicio.Size = New System.Drawing.Size(257, 23)
        Me.cbxServicio.TabIndex = 2
        '
        'lblTipServ
        '
        Me.lblTipServ.AutoSize = True
        Me.lblTipServ.Location = New System.Drawing.Point(157, 113)
        Me.lblTipServ.Name = "lblTipServ"
        Me.lblTipServ.Size = New System.Drawing.Size(98, 15)
        Me.lblTipServ.TabIndex = 1
        Me.lblTipServ.Text = "Tipo Servicio :"
        '
        'cbxTipoServ
        '
        Me.cbxTipoServ.FormattingEnabled = True
        Me.cbxTipoServ.Location = New System.Drawing.Point(266, 110)
        Me.cbxTipoServ.Name = "cbxTipoServ"
        Me.cbxTipoServ.Size = New System.Drawing.Size(202, 23)
        Me.cbxTipoServ.TabIndex = 0
        '
        'ConsultaRelTecnicosQuejasBindingSource
        '
        Me.ConsultaRelTecnicosQuejasBindingSource.DataMember = "ConsultaRel_Tecnicos_Quejas"
        Me.ConsultaRelTecnicosQuejasBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'ConsultaRelQuejasTecFamiliaBindingSource
        '
        Me.ConsultaRelQuejasTecFamiliaBindingSource.DataMember = "ConsultaRel_Quejas_Tec_Familia"
        Me.ConsultaRelQuejasTecFamiliaBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.Control
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(392, 693)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 74
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'ConGeneralTableAdapter1
        '
        Me.ConGeneralTableAdapter1.ClearBeforeFill = True
        '
        'ConGeneralAntiguedadTableAdapter1
        '
        Me.ConGeneralAntiguedadTableAdapter1.ClearBeforeFill = True
        '
        'MUESTRAPERIODOSTableAdapter1
        '
        Me.MUESTRAPERIODOSTableAdapter1.ClearBeforeFill = True
        '
        'CONSULTAGENERALESDESCTableAdapter1
        '
        Me.CONSULTAGENERALESDESCTableAdapter1.ClearBeforeFill = True
        '
        'ConsultaRel_Quejas_Tec_FamiliaBindingSource
        '
        Me.ConsultaRel_Quejas_Tec_FamiliaBindingSource.DataMember = "ConsultaRel_Quejas_Tec_Familia"
        Me.ConsultaRel_Quejas_Tec_FamiliaBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'ConsultaRel_Quejas_Tec_FamiliaTableAdapter
        '
        Me.ConsultaRel_Quejas_Tec_FamiliaTableAdapter.ClearBeforeFill = True
        '
        'ConsultaRel_Tecnicos_QuejasBindingSource
        '
        Me.ConsultaRel_Tecnicos_QuejasBindingSource.DataMember = "ConsultaRel_Tecnicos_Quejas"
        Me.ConsultaRel_Tecnicos_QuejasBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'ConsultaRel_Tecnicos_QuejasTableAdapter
        '
        Me.ConsultaRel_Tecnicos_QuejasTableAdapter.ClearBeforeFill = True
        '
        'Muestra_tecnicosDepartamentos_AlmacenTableAdapter
        '
        Me.Muestra_tecnicosDepartamentos_AlmacenTableAdapter.ClearBeforeFill = True
        '
        'Muestra_TecnicosByFamiliTableAdapter
        '
        Me.Muestra_TecnicosByFamiliTableAdapter.ClearBeforeFill = True
        '
        'NueGeneralMsjTicketsBindingSource
        '
        Me.NueGeneralMsjTicketsBindingSource.DataMember = "NueGeneralMsjTickets"
        '
        'MODIFCAGENERALESDESCBindingSource1
        '
        Me.MODIFCAGENERALESDESCBindingSource1.DataMember = "MODIFCAGENERALESDESC"
        '
        'NueGeneralBindingSource1
        '
        Me.NueGeneralBindingSource1.DataMember = "NueGeneral"
        '
        'NUEGeneralAntiguedadBindingSource1
        '
        Me.NUEGeneralAntiguedadBindingSource1.DataMember = "NUEGeneralAntiguedad"
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter5
        '
        Me.Muestra_ServiciosDigitalesTableAdapter5.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter6
        '
        Me.Muestra_ServiciosDigitalesTableAdapter6.ClearBeforeFill = True
        '
        'FrmGenerales_Sistema
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(901, 741)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TabControl1)
        Me.MaximizeBox = False
        Me.Name = "FrmGenerales_Sistema"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Generales del Sistema"
        CType(Me.ConsultaCpGeneBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_ImpresorasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConMesesCobroAdeudoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConGeneralCorreoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Mod_Cp_GenBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Generales_Filtros_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Generales_Filtros_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.COnsultaGralfacturaglobalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Gral_factura_globalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.DataGridViewCobro, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONSULTAGENERALESDESCBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAPERIODOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.PanelBasico.ResumeLayout(False)
        Me.PanelBasico.PerformLayout()
        Me.Panel13.ResumeLayout(False)
        Me.Panel13.PerformLayout()
        CType(Me.ConGeneralAntiguedadBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConGeneralBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.Consulta_cobrodepositoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        CType(Me.ConGeneralAlertaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel12.ResumeLayout(False)
        Me.Panel12.PerformLayout()
        CType(Me.CONSULTA_General_HsbcBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetyahve, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        CType(Me.CONSULTA_General_SantanderBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelDatosXml.ResumeLayout(False)
        Me.PanelDatosXml.PerformLayout()
        CType(Me.CONSULTA_General_XmlBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelDatos.ResumeLayout(False)
        Me.PanelDatos.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        CType(Me.ConGeneralMsjTicketsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        CType(Me.Consulta_cobrodepositobuenoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        CType(Me.NumericUpDown9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Rel_PaquetesDigTelDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Rel_PaquetesDigTelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueGeneralAlertaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_Direcciones_IpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage14.ResumeLayout(False)
        Me.TabPage13.ResumeLayout(False)
        Me.TabPage12.ResumeLayout(False)
        Me.TabPage11.ResumeLayout(False)
        Me.TabPage10.ResumeLayout(False)
        Me.TabPage9.ResumeLayout(False)
        Me.TabPage8.ResumeLayout(False)
        Me.TabPage7.ResumeLayout(False)
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage15.ResumeLayout(False)
        Me.PnQuejas.ResumeLayout(False)
        Me.PnQuejas.PerformLayout()
        CType(Me.MuestraTecnicosByFamiliBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestratecnicosDepartamentosAlmacenBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage16.ResumeLayout(False)
        Me.TabPage16.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPuntosPPE.ResumeLayout(False)
        Me.TabPuntosPPE.PerformLayout()
        Me.TabPage17.ResumeLayout(False)
        Me.TabPage17.PerformLayout()
        Me.TabPage18.ResumeLayout(False)
        Me.TabPage18.PerformLayout()
        CType(Me.NumericUpDownIeps, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDownIva, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpRecontratacion.ResumeLayout(False)
        Me.tpRecontratacion.PerformLayout()
        Me.tpDecodificadores.ResumeLayout(False)
        Me.tpDecodificadores.PerformLayout()
        CType(Me.dataGridDec, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpEstadosCuenta.ResumeLayout(False)
        Me.tpEstadosCuenta.PerformLayout()
        Me.gbMensaje.ResumeLayout(False)
        Me.gbMensaje.PerformLayout()
        Me.tpDatosFiscales.ResumeLayout(False)
        Me.tpDatosFiscales.PerformLayout()
        Me.tbPuntos.ResumeLayout(False)
        Me.tbPuntos.PerformLayout()
        Me.tcPuntos.ResumeLayout(False)
        Me.tbPuntosRangos.ResumeLayout(False)
        Me.tbPuntosRangos.PerformLayout()
        CType(Me.dgRangos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbPuntosServicios.ResumeLayout(False)
        Me.tbPuntosServicios.PerformLayout()
        CType(Me.dgPuntosServicios, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbPuntosServiciosAdic.ResumeLayout(False)
        Me.tbPuntosServiciosAdic.PerformLayout()
        CType(Me.dgPagosAntiServAdic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbPuntosCajas.ResumeLayout(False)
        Me.tbPuntosCajas.PerformLayout()
        CType(Me.dgPuntosAntiArticulos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbPromocionAntiguedad.ResumeLayout(False)
        Me.tbPromocionAntiguedad.PerformLayout()
        CType(Me.dgvTipoUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpPromPriMens.ResumeLayout(False)
        Me.tpPromPriMens.PerformLayout()
        Me.gbxMens0.ResumeLayout(False)
        Me.gbxMens0.PerformLayout()
        Me.gbxMensN.ResumeLayout(False)
        Me.gbxMensN.PerformLayout()
        CType(Me.ConsultaRelTecnicosQuejasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsultaRelQuejasTecFamiliaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsultaRel_Quejas_Tec_FamiliaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsultaRel_Tecnicos_QuejasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueGeneralMsjTicketsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MODIFCAGENERALESDESCBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NueGeneralBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUEGeneralAntiguedadBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents ConGeneralBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGeneralTableAdapter As sofTV.NewSofTvDataSetTableAdapters.ConGeneralTableAdapter
    Friend WithEvents NueGeneralBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueGeneralTableAdapter As sofTV.NewSofTvDataSetTableAdapters.NueGeneralTableAdapter
    Friend WithEvents ConGeneralAntiguedadBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGeneralAntiguedadTableAdapter As sofTV.NewSofTvDataSetTableAdapters.ConGeneralAntiguedadTableAdapter
    Friend WithEvents NUEGeneralAntiguedadBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NUEGeneralAntiguedadTableAdapter As sofTV.NewSofTvDataSetTableAdapters.NUEGeneralAntiguedadTableAdapter
    Friend WithEvents MUESTRAPERIODOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAPERIODOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRAPERIODOSTableAdapter
    Friend WithEvents CONSULTAGENERALESDESCBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTAGENERALESDESCTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONSULTAGENERALESDESCTableAdapter
    Friend WithEvents MODIFCAGENERALESDESCBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MODIFCAGENERALESDESCTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MODIFCAGENERALESDESCTableAdapter
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents MODIFCAPeriodosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MODIFCAPeriodosTableAdapter As New sofTV.DataSetLidiaTableAdapters.MODIFCAPeriodosTableAdapter
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents Consulta_ImpresorasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_ImpresorasTableAdapter As sofTV.DataSetarnoldoTableAdapters.Consulta_ImpresorasTableAdapter
    Friend WithEvents Inserta_Mod_Cp_GenBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Mod_Cp_GenTableAdapter As sofTV.DataSetarnoldoTableAdapters.Inserta_Mod_Cp_GenTableAdapter
    Friend WithEvents ConsultaCpGeneBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Cp_GeneTableAdapter As sofTV.DataSetarnoldoTableAdapters.Consulta_Cp_GeneTableAdapter
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConMesesCobroAdeudoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConMesesCobroAdeudoTableAdapter As sofTV.DataSetEricTableAdapters.ConMesesCobroAdeudoTableAdapter
    Friend WithEvents MuestraTipServEricBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipServEricTableAdapter As sofTV.DataSetEricTableAdapters.MuestraTipServEricTableAdapter
    Friend WithEvents ConGeneralCorreoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGeneralCorreoTableAdapter As sofTV.DataSetEricTableAdapters.ConGeneralCorreoTableAdapter
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Consulta_Generales_Filtros_OrdenesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Generales_Filtros_OrdenesTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Consulta_Generales_Filtros_OrdenesTableAdapter
    Friend WithEvents Inserta_Generales_Filtros_OrdenesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Generales_Filtros_OrdenesTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Generales_Filtros_OrdenesTableAdapter
    'Friend WithEvents COnsulta_Gral_factura_globalBindingSource As System.Windows.Forms.BindingSource
    'Friend WithEvents COnsulta_Gral_factura_globalTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.COnsulta_Gral_factura_globalTableAdapter
    Friend WithEvents COnsultaGralfacturaglobalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents COnsulta_Gral_factura_globalTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.COnsulta_Gral_factura_globalTableAdapter
    Friend WithEvents Inserta_Gral_factura_globalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Gral_factura_globalTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Gral_factura_globalTableAdapter
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents CuentaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PasswordTextBox As System.Windows.Forms.TextBox
    Friend WithEvents HostTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PortTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Clv_TipSerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ConceptoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents MesesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents txtDiaCorte As System.Windows.Forms.TextBox
    Friend WithEvents TextBox15 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox13 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents ImpresoraFiscalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ImpresoraTarjetasTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ImpresoraTicketsTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ImpresoraContratosTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PanelBasico As System.Windows.Forms.Panel
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown7 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown8 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown5 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown6 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown3 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown4 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown2 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Puntos10TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Puntos5TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Puntos2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Puntos1TextBox As System.Windows.Forms.TextBox
    Friend WithEvents ImpresoraOrdenesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents CostoExtTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NumeroExtTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Carga_trab_insTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CargaDeTrabajoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ImportePorExtrasTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Si_se_generaCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents DataSetEric2 As New sofTV.DataSetEric2
    Friend WithEvents ConGeneralMsjTicketsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGeneralMsjTicketsTableAdapter As sofTV.DataSetEric2TableAdapters.ConGeneralMsjTicketsTableAdapter
    Friend WithEvents MensajeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ActivoCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents NueGeneralMsjTicketsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueGeneralMsjTicketsTableAdapter As sofTV.DataSetEric2TableAdapters.NueGeneralMsjTicketsTableAdapter
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox4 As System.Windows.Forms.CheckBox
    Friend WithEvents Button19 As System.Windows.Forms.Button
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Button20 As System.Windows.Forms.Button
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents DataSetLidia2 As sofTV.DataSetLidia2
    Friend WithEvents ConGeneralAlertaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGeneralAlertaTableAdapter As sofTV.DataSetLidia2TableAdapters.ConGeneralAlertaTableAdapter
    Friend WithEvents NueGeneralAlertaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NueGeneralAlertaTableAdapter As sofTV.DataSetLidia2TableAdapters.NueGeneralAlertaTableAdapter
    Friend WithEvents Dame_Direcciones_IpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_Direcciones_IpTableAdapter As sofTV.DataSetLidia2TableAdapters.Dame_Direcciones_IpTableAdapter
    Friend WithEvents Button21 As System.Windows.Forms.Button
    Friend WithEvents Procedimientosarnoldo4 As sofTV.Procedimientosarnoldo4
    Friend WithEvents Consulta_cobrodepositoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_cobrodepositoTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Consulta_cobrodepositoTableAdapter
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Button23 As System.Windows.Forms.Button
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Consulta_cobrodepositobuenoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_cobrodepositobuenoTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Consulta_cobrodepositobuenoTableAdapter
    Friend WithEvents CablemodemsCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents PregcablTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DecodersCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents PregdecoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Consulta_Rel_PaquetesDigTelBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Rel_PaquetesDigTelTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Consulta_Rel_PaquetesDigTelTableAdapter
    Friend WithEvents Consulta_Rel_PaquetesDigTelDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Button26 As System.Windows.Forms.Button
    Friend WithEvents Button25 As System.Windows.Forms.Button
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents NumericUpDown9 As System.Windows.Forms.NumericUpDown
    Friend WithEvents ConsecutivoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PanelDatos As System.Windows.Forms.Panel
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TELefonosTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RfcTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CiudadTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ColoniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DireccionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Id_sucursalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PanelDatosXml As System.Windows.Forms.Panel
    Friend WithEvents Button28 As System.Windows.Forms.Button
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents DataSetyahve As sofTV.DataSetyahve
    Friend WithEvents CONSULTA_General_XmlBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTA_General_XmlTableAdapter As sofTV.DataSetyahveTableAdapters.CONSULTA_General_XmlTableAdapter
    Friend WithEvents Nom_ArchivoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_InicioDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Fecha_FacturasDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Periodo_inicialTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Periodo_finalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Numero_InicioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FormatoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RutaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Numero_LocalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Pais_LocalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clave044TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clave045TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clave01800TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clave01900TextBox As System.Windows.Forms.TextBox
    Friend WithEvents CONSULTA_General_SantanderBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTA_General_SantanderTableAdapter As sofTV.DataSetyahveTableAdapters.CONSULTA_General_SantanderTableAdapter
    Friend WithEvents Panel11 As System.Windows.Forms.Panel
    Friend WithEvents ReferenciaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ConstanteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ModuloTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button30 As System.Windows.Forms.Button
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Panel12 As System.Windows.Forms.Panel
    Friend WithEvents Button31 As System.Windows.Forms.Button
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents CONSULTA_General_HsbcBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTA_General_HsbcTableAdapter As sofTV.DataSetyahveTableAdapters.CONSULTA_General_HsbcTableAdapter
    Friend WithEvents ReferenciaTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ModuloTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents TextBoxfolio As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxserie As System.Windows.Forms.TextBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents TxtCdClvAdic As System.Windows.Forms.TextBox
    Friend WithEvents TxtEdoClvAdic As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Muestra_ciudadTableAdapter1 As sofTV.Procedimientosarnoldo4TableAdapters.Muestra_ciudadTableAdapter
    Friend WithEvents TabPage14 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage13 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage12 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage11 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage10 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage9 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage8 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage7 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents TextBoxNumeroExt As System.Windows.Forms.TextBox
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents TextBoxNumeroInt As System.Windows.Forms.TextBox
    Friend WithEvents ConGeneralTableAdapter1 As sofTV.Procedimientosarnoldo4TableAdapters.ConGeneralTableAdapter
    Friend WithEvents ConGeneralAntiguedadTableAdapter1 As sofTV.Procedimientosarnoldo4TableAdapters.ConGeneralAntiguedadTableAdapter
    Friend WithEvents MUESTRAPERIODOSTableAdapter1 As sofTV.Procedimientosarnoldo4TableAdapters.MUESTRAPERIODOSTableAdapter
    Friend WithEvents CONSULTAGENERALESDESCTableAdapter1 As sofTV.Procedimientosarnoldo4TableAdapters.CONSULTAGENERALESDESCTableAdapter
    Friend WithEvents MODIFCAGENERALESDESCBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents MODIFCAGENERALESDESCTableAdapter1 As New sofTV.DataSetEric2TableAdapters.MODIFCAGENERALESDESCTableAdapter
    Friend WithEvents NueGeneralBindingSource1 As New System.Windows.Forms.BindingSource
    Friend WithEvents NueGeneralTableAdapter1 As New sofTV.DataSetEric2TableAdapters.NueGeneralTableAdapter
    Friend WithEvents NUEGeneralAntiguedadBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents NUEGeneralAntiguedadTableAdapter1 As sofTV.DataSetEric2TableAdapters.NUEGeneralAntiguedadTableAdapter
    Friend WithEvents TabPage15 As System.Windows.Forms.TabPage
    Friend WithEvents PnQuejas As System.Windows.Forms.Panel
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents TreViewTecnicos As System.Windows.Forms.TreeView
    Friend WithEvents CmbTecnicos As System.Windows.Forms.ComboBox
    Friend WithEvents CmBDepto As System.Windows.Forms.ComboBox
    Friend WithEvents LblFamilia As System.Windows.Forms.Label
    Friend WithEvents LblTecnico As System.Windows.Forms.Label
    Friend WithEvents LblNotas As System.Windows.Forms.Label
    Friend WithEvents ConsultaRel_Quejas_Tec_FamiliaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConsultaRel_Quejas_Tec_FamiliaTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.ConsultaRel_Quejas_Tec_FamiliaTableAdapter
    Friend WithEvents ConsultaRel_Tecnicos_QuejasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConsultaRel_Tecnicos_QuejasTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.ConsultaRel_Tecnicos_QuejasTableAdapter
    Friend WithEvents ConsultaRelTecnicosQuejasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConsultaRelQuejasTecFamiliaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTecnicosByFamiliBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestratecnicosDepartamentosAlmacenBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_tecnicosDepartamentos_AlmacenTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Muestra_tecnicosDepartamentos_AlmacenTableAdapter
    Friend WithEvents Muestra_TecnicosByFamiliTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Muestra_TecnicosByFamiliTableAdapter
    Friend WithEvents TabPage16 As System.Windows.Forms.TabPage
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Clv_Grupo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Grupo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Porcentaje As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Activo As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents LabelNota As System.Windows.Forms.Label
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents TreeViewOrdenes As System.Windows.Forms.TreeView
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewCobro As System.Windows.Forms.DataGridView
    Friend WithEvents ComboBoxCobro As System.Windows.Forms.ComboBox
    Friend WithEvents Clv_TipoCliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Meses As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Habilitado As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents TabPuntosPPE As System.Windows.Forms.TabPage
    Friend WithEvents txtPtosMin As System.Windows.Forms.TextBox
    Friend WithEvents btnGuardaPPE As System.Windows.Forms.Button
    Friend WithEvents lblPPE1 As System.Windows.Forms.Label
    Friend WithEvents TabPage17 As System.Windows.Forms.TabPage
    Friend WithEvents DDescoSusp As System.Windows.Forms.TextBox
    Friend WithEvents DContrata As System.Windows.Forms.TextBox
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents TabPage18 As System.Windows.Forms.TabPage
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents LblPorIeps As System.Windows.Forms.Label
    Friend WithEvents NumericUpDownIeps As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDownIva As System.Windows.Forms.NumericUpDown
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents ChkCalculo1 As System.Windows.Forms.CheckBox
    Friend WithEvents LabelIEPS As System.Windows.Forms.Label
    Friend WithEvents CtaIepsText As System.Windows.Forms.TextBox
    Friend WithEvents LabelCtaIEPS As System.Windows.Forms.Label
    Friend WithEvents CheckIEPS As System.Windows.Forms.CheckBox
    Friend WithEvents TextIva As System.Windows.Forms.TextBox
    Friend WithEvents TextIEPS As System.Windows.Forms.TextBox
    Friend WithEvents tpRecontratacion As System.Windows.Forms.TabPage
    Friend WithEvents btnAceptarRecon As System.Windows.Forms.Button
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents cbAplica As System.Windows.Forms.CheckBox
    Friend WithEvents txtMeses As System.Windows.Forms.TextBox
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents rb30 As System.Windows.Forms.RadioButton
    Friend WithEvents rb15 As System.Windows.Forms.RadioButton
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents tpDecodificadores As System.Windows.Forms.TabPage
    Friend WithEvents txtAgregarDec As System.Windows.Forms.Button
    Friend WithEvents dataGridDec As System.Windows.Forms.DataGridView
    Friend WithEvents txtDescripcionDec As System.Windows.Forms.TextBox
    Friend WithEvents comboServicioDec As System.Windows.Forms.ComboBox
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents txtEliminarDec As System.Windows.Forms.Button
    Friend WithEvents Label75 As System.Windows.Forms.Label
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents txtPrecioPrincipal As System.Windows.Forms.TextBox
    Friend WithEvents Label76 As System.Windows.Forms.Label
    Friend WithEvents txtPrecioAdicional As System.Windows.Forms.TextBox
    Friend WithEvents IdDec As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescripcionDec As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_ServicioDec As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ServicioDec As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrecioPrincipalDec As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrecioAdicionalDec As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tpEstadosCuenta As System.Windows.Forms.TabPage
    Friend WithEvents Label80 As System.Windows.Forms.Label
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents txtDiaEdoCuenta As System.Windows.Forms.TextBox
    Friend WithEvents cbPeriodo As System.Windows.Forms.ComboBox
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents gbMensaje As System.Windows.Forms.GroupBox
    Friend WithEvents tbMensaje As System.Windows.Forms.TextBox
    Friend WithEvents Label82 As System.Windows.Forms.Label
    Friend WithEvents tbAsunto As System.Windows.Forms.TextBox
    Friend WithEvents Label81 As System.Windows.Forms.Label
    Friend WithEvents tbMensajeDeco As System.Windows.Forms.TextBox
    Friend WithEvents Label85 As System.Windows.Forms.Label
    Friend WithEvents Label84 As System.Windows.Forms.Label
    Friend WithEvents Label83 As System.Windows.Forms.Label
    Friend WithEvents dtHora As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents Label88 As System.Windows.Forms.Label
    Friend WithEvents Label87 As System.Windows.Forms.Label
    Friend WithEvents cbDeco As System.Windows.Forms.CheckBox
    Friend WithEvents cbCorreo As System.Windows.Forms.CheckBox
    Friend WithEvents tpDatosFiscales As System.Windows.Forms.TabPage
    Friend WithEvents tbTelefono As System.Windows.Forms.TextBox
    Friend WithEvents Label2000 As System.Windows.Forms.Label
    Friend WithEvents tbCodigoPostal As System.Windows.Forms.TextBox
    Friend WithEvents Label101 As System.Windows.Forms.Label
    Friend WithEvents tbPais As System.Windows.Forms.TextBox
    Friend WithEvents Label100 As System.Windows.Forms.Label
    Friend WithEvents tbEstado As System.Windows.Forms.TextBox
    Friend WithEvents Label99 As System.Windows.Forms.Label
    Friend WithEvents tbMunicipio As System.Windows.Forms.TextBox
    Friend WithEvents Label98 As System.Windows.Forms.Label
    Friend WithEvents tbReferencia As System.Windows.Forms.TextBox
    Friend WithEvents Label97 As System.Windows.Forms.Label
    Friend WithEvents tbrfc As System.Windows.Forms.TextBox
    Friend WithEvents Label96 As System.Windows.Forms.Label
    Friend WithEvents tbNombre As System.Windows.Forms.TextBox
    Friend WithEvents Label95 As System.Windows.Forms.Label
    Friend WithEvents tbCalle As System.Windows.Forms.TextBox
    Friend WithEvents Label94 As System.Windows.Forms.Label
    Friend WithEvents tbNoExt As System.Windows.Forms.TextBox
    Friend WithEvents Label93 As System.Windows.Forms.Label
    Friend WithEvents tbNoInt As System.Windows.Forms.TextBox
    Friend WithEvents Label92 As System.Windows.Forms.Label
    Friend WithEvents tbColonia As System.Windows.Forms.TextBox
    Friend WithEvents Label91 As System.Windows.Forms.Label
    Friend WithEvents tbLocalidad As System.Windows.Forms.TextBox
    Friend WithEvents Label90 As System.Windows.Forms.Label
    Friend WithEvents Button17 As System.Windows.Forms.Button
    Friend WithEvents tbEmail As System.Windows.Forms.TextBox
    Friend WithEvents Label89 As System.Windows.Forms.Label
    Friend WithEvents Label102 As System.Windows.Forms.Label
    Friend WithEvents tbRFCExtranjeros As System.Windows.Forms.TextBox
    Friend WithEvents tbRFCPublicoGeneral As System.Windows.Forms.TextBox
    Friend WithEvents tbCertificado As System.Windows.Forms.TextBox
    Friend WithEvents tbAnoAprobacion As System.Windows.Forms.TextBox
    Friend WithEvents Label107 As System.Windows.Forms.Label
    Friend WithEvents Label106 As System.Windows.Forms.Label
    Friend WithEvents Label105 As System.Windows.Forms.Label
    Friend WithEvents Label104 As System.Windows.Forms.Label
    Friend WithEvents tbNoAprobacion As System.Windows.Forms.TextBox
    Friend WithEvents Label103 As System.Windows.Forms.Label
    Friend WithEvents cbHabilitado As System.Windows.Forms.CheckBox
    Friend WithEvents tbPuntos As System.Windows.Forms.TabPage
    Friend WithEvents tcPuntos As System.Windows.Forms.TabControl
    Friend WithEvents tbPuntosRangos As System.Windows.Forms.TabPage
    Friend WithEvents bnEliminarRango As System.Windows.Forms.Button
    Friend WithEvents bnAgregarRango As System.Windows.Forms.Button
    Friend WithEvents Label109 As System.Windows.Forms.Label
    Friend WithEvents tbRangoFin As System.Windows.Forms.TextBox
    Friend WithEvents dgRangos As System.Windows.Forms.DataGridView
    Friend WithEvents Label108 As System.Windows.Forms.Label
    Friend WithEvents tbRangoIni As System.Windows.Forms.TextBox
    Friend WithEvents tbPuntosServicios As System.Windows.Forms.TabPage
    Friend WithEvents tbPuntosServiciosAdic As System.Windows.Forms.TabPage
    Friend WithEvents tbPuntosCajas As System.Windows.Forms.TabPage
    Friend WithEvents bnEliminarPuntosServicio As System.Windows.Forms.Button
    Friend WithEvents bnAgregarPuntosServicio As System.Windows.Forms.Button
    Friend WithEvents Label112 As System.Windows.Forms.Label
    Friend WithEvents dgPuntosServicios As System.Windows.Forms.DataGridView
    Friend WithEvents tboxPuntosAnti As System.Windows.Forms.TextBox
    Friend WithEvents cbRangosPuntosAnti As System.Windows.Forms.ComboBox
    Friend WithEvents cbServicioPuntosAnti As System.Windows.Forms.ComboBox
    Friend WithEvents Label113 As System.Windows.Forms.Label
    Friend WithEvents Label111 As System.Windows.Forms.Label
    Friend WithEvents cbTipSerPuntosAnti As System.Windows.Forms.ComboBox
    Friend WithEvents Label118 As System.Windows.Forms.Label
    Friend WithEvents tbPuntosAntiServicios As System.Windows.Forms.TextBox
    Friend WithEvents Label114 As System.Windows.Forms.Label
    Friend WithEvents Label115 As System.Windows.Forms.Label
    Friend WithEvents cbTipSerPuntosAntiServicios As System.Windows.Forms.ComboBox
    Friend WithEvents cbServicioPuntosAntiServicios As System.Windows.Forms.ComboBox
    Friend WithEvents bnEliminarPuntosAntiServicios As System.Windows.Forms.Button
    Friend WithEvents bnAgregarPuntosAntiServicios As System.Windows.Forms.Button
    Friend WithEvents Label116 As System.Windows.Forms.Label
    Friend WithEvents dgPagosAntiServAdic As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PagosPuntuales As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PuntosServicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tbPuntosAntiServiciosPagos As System.Windows.Forms.TextBox
    Friend WithEvents cbRangosPuntosAntiServicios As System.Windows.Forms.ComboBox
    Friend WithEvents dgPuntosAntiArticulos As System.Windows.Forms.DataGridView
    Friend WithEvents Label119 As System.Windows.Forms.Label
    Friend WithEvents cbRangoPuntosAntiArticulos As System.Windows.Forms.ComboBox
    Friend WithEvents bnAbajoPuntosAntiArticulos As System.Windows.Forms.Button
    Friend WithEvents bnArribaPuntosAntiArticulos As System.Windows.Forms.Button
    Friend WithEvents Label120 As System.Windows.Forms.Label
    Friend WithEvents bnEliminarPuntosAntiArticulos As System.Windows.Forms.Button
    Friend WithEvents bnAgregarPuntosAntiArticulos As System.Windows.Forms.Button
    Friend WithEvents Label121 As System.Windows.Forms.Label
    Friend WithEvents cbArticuloPuntosAntiArticulos As System.Windows.Forms.ComboBox
    Friend WithEvents Label122 As System.Windows.Forms.Label
    Friend WithEvents tbPuntosAntiArticulos As System.Windows.Forms.TextBox
    Friend WithEvents IdRango As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Rango As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdPuntos As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Puntos As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label123 As System.Windows.Forms.Label
    Friend WithEvents Label110 As System.Windows.Forms.Label
    Friend WithEvents Label117 As System.Windows.Forms.Label
    Friend WithEvents Label124 As System.Windows.Forms.Label
    Friend WithEvents tbPuntosAnti As System.Windows.Forms.TextBox
    Friend WithEvents Panel13 As System.Windows.Forms.Panel
    Friend WithEvents tbCantidad As System.Windows.Forms.TextBox
    Friend WithEvents bnGuardarNumero As System.Windows.Forms.Button
    Friend WithEvents Label126 As System.Windows.Forms.Label
    Friend WithEvents IdPuntosPuntosAntiArticulos As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents JerarquiaPuntosAntiArticulos As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescripcionPuntosAntiArticulos As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PuntosPuntosAntiArticulos As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PuntosSegundaYTercerCaja As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PuntosCuartaCaja As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents tbPuntosAntiArticulos4ta As System.Windows.Forms.TextBox
    Friend WithEvents Label128 As System.Windows.Forms.Label
    Friend WithEvents tbPuntosAntiArticulos2da3er As System.Windows.Forms.TextBox
    Friend WithEvents Label127 As System.Windows.Forms.Label
    Friend WithEvents tbPromocionAntiguedad As System.Windows.Forms.TabPage
    Friend WithEvents bnGuardarTipoUsuario As System.Windows.Forms.Button
    Friend WithEvents Label131 As System.Windows.Forms.Label
    Friend WithEvents dgvTipoUsuario As System.Windows.Forms.DataGridView
    Friend WithEvents bnGuardarMeses As System.Windows.Forms.Button
    Friend WithEvents tbMesesAntiguedadPromocion As System.Windows.Forms.TextBox
    Friend WithEvents Label130 As System.Windows.Forms.Label
    Friend WithEvents Label129 As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Clv_TipoUsuario As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescripcionTipoUsuario As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Acceso As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter5 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter6 As Softv.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents tpPromPriMens As System.Windows.Forms.TabPage
    Friend WithEvents lblServicio As System.Windows.Forms.Label
    Friend WithEvents cbxServicio As System.Windows.Forms.ComboBox
    Friend WithEvents lblTipServ As System.Windows.Forms.Label
    Friend WithEvents cbxTipoServ As System.Windows.Forms.ComboBox
    Friend WithEvents gbxMens0 As System.Windows.Forms.GroupBox
    Friend WithEvents gbxMensN As System.Windows.Forms.GroupBox
    Friend WithEvents Label134 As System.Windows.Forms.Label
    Friend WithEvents txtDescuentoC As System.Windows.Forms.TextBox
    Friend WithEvents Label133 As System.Windows.Forms.Label
    Friend WithEvents txtDescuento As System.Windows.Forms.TextBox
    Friend WithEvents Label132 As System.Windows.Forms.Label
    Friend WithEvents txtImporte As System.Windows.Forms.TextBox
    Friend WithEvents Label135 As System.Windows.Forms.Label
    Friend WithEvents txtDescuentoC0 As System.Windows.Forms.TextBox
    Friend WithEvents Label136 As System.Windows.Forms.Label
    Friend WithEvents txtDescuento0 As System.Windows.Forms.TextBox
    Friend WithEvents Label137 As System.Windows.Forms.Label
    Friend WithEvents txtImporte0 As System.Windows.Forms.TextBox
    Friend WithEvents Label138 As System.Windows.Forms.Label
    Friend WithEvents btnGuardarPM As System.Windows.Forms.Button
    Friend WithEvents btnAgregarPM As System.Windows.Forms.Button
    'Friend WithEvents Inserta_Gral_factura_globalBindingSource As System.Windows.Forms.BindingSource
    'Friend WithEvents Inserta_Gral_factura_globalTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Gral_factura_globalTableAdapter
End Class
