Imports System.Data.SqlClient
Imports System.Text
Public Class FrmPrefijoMsj

    Private Clv_Session As Long
    Private actualiza As Boolean = False
    Private ClvMensaje As Long
    Private TipoMsj As Integer
    Private Descripcion As String
    Private Mensaje As String

    Private Sub FrmPrefijoMsj_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If rSession = 0 Then
            Module1.clvSessionMensajes()
            Me.Clv_Session = rSession
        Else
            Me.Clv_Session = rSession
        End If
        MuestraTipoMensajes()
        If Me.actualiza = False Then
            Me.Inicio(1)
        Else
            Me.Inicio(2)
            Me.ComboBox1.SelectedValue = Me.TipoMsj
            Me.TextBox2.Text = Me.Descripcion
            Me.TextBox1.Text = Me.Mensaje
        End If
        Me.MuestraPrefijos(1, Me.Clv_Session)
        Me.MuestraPrefijos(2, Me.Clv_Session)
    End Sub

    Private Sub MuestraPrefijos(ByVal op As Integer, ByVal session As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim tabla As DataTable = New DataTable
        Dim com As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        Dim bs As BindingSource = New BindingSource
        Try
            com.CommandType = CommandType.Text
            com.CommandTimeout = 0
            com.CommandText = "Exec MuestraTablasPrefijos " + op.ToString + "," + session.ToString
            com.Connection = conexion
            com.Connection.Open()
            da.Fill(tabla)
            bs.DataSource = tabla
            If op = 1 Then
                Me.ListBox1.DataSource = bs
                Me.ListBox1.ValueMember = "Clv_Prefijo"
                Me.ListBox1.DisplayMember = "Prefijo"
            End If
            If op = 2 Then
                Me.ListBox2.DataSource = bs
                Me.ListBox2.ValueMember = "Clv_Prefijo"
                Me.ListBox2.DisplayMember = "Prefijo"
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            com.Connection.Close()
            com.Dispose()
            tabla.Dispose()
        End Try
    End Sub

    Private Sub MuestraTipoMensajes()
        Dim con As New SqlConnection(MiConexion)
        Dim str As New StringBuilder
        str.Append("Exec MuestraTipoMensajes ")
        Dim dataadapter As New SqlDataAdapter(str.ToString, con)
        Dim datatable As New DataTable
        Dim binding As New BindingSource
        Try
            con.Open()
            dataadapter.Fill(datatable)
            binding.DataSource = datatable
            Me.ComboBox1.DataSource = binding
            Me.ComboBox1.DisplayMember = "Tipo_Mensaje"
            Me.ComboBox1.ValueMember = "Clv_Tmensaje"
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            con.Close()
            con.Dispose()
        End Try
    End Sub

    Private Sub InsertaPrefijoTmp(ByVal op As Integer)

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("InsertaPrefijoTmp", con)
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.Add(New SqlParameter("@clv_prefijo", SqlDbType.BigInt))
        com.Parameters("@clv_prefijo").Value = 0
        If op = 1 Then
            If Me.ListBox1.Items.Count > 0 Then
                com.Parameters("@clv_prefijo").Value = Me.ListBox1.SelectedValue
            Else
                MsgBox("No hay elementos disponibles", MsgBoxStyle.Exclamation)
                Return
            End If
        End If
        If op = 3 Then
            If Me.ListBox2.Items.Count > 0 Then
                com.Parameters("@clv_prefijo").Value = Me.ListBox2.SelectedValue
            Else
                MsgBox("No hay elementos disponibles", MsgBoxStyle.Exclamation)
                Return
            End If
        End If
        com.Parameters.Add(New SqlParameter("@Clv_Session", Me.Clv_Session))
        com.Parameters.Add(New SqlParameter("@Op", op))
        Try
            con.Open()
            com.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            con.Close()
        End Try

    End Sub

    Private Sub Inicio(ByVal Op As Integer)
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("PrefijosInicio", con)
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.Add(New SqlParameter("@Op", Op))
        com.Parameters.Add(New SqlParameter("@ClvMensaje", Me.ClvMensaje))
        com.Parameters.Add(New SqlParameter("@Clv_Session", Me.Clv_Session))
        Try
            con.Open()
            com.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            con.Close()
        End Try
    End Sub

    Private Function Guarda(ByVal op As Integer, ByVal clvMensaje As Long, ByVal tipo As Integer, ByVal desc As String, ByVal mensaje As String, ByVal session As Long) As Integer
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("GuardaMensajePersonal", con)
        Dim res As Integer
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.Add(New SqlParameter("@Op", op))
        com.Parameters.Add(New SqlParameter("@ClvMensaje", clvMensaje))
        com.Parameters.Add(New SqlParameter("@Tmensaje", tipo))
        com.Parameters.Add(New SqlParameter("@Desc", desc))
        com.Parameters.Add(New SqlParameter("@Mensaje", mensaje))
        com.Parameters.Add(New SqlParameter("@Clv_Session", session))
        com.Parameters.Add(New SqlParameter("@Res", SqlDbType.Int))
        com.Parameters("@Res").Direction = ParameterDirection.Output
        Try
            con.Open()
            com.ExecuteNonQuery()
            res = com.Parameters("@Res").Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            con.Close()
        End Try
        Return res
    End Function

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.ListBox1.Items.Count = 0 Then
            MsgBox("No hay elementos disponibles", MsgBoxStyle.Exclamation)
        Else
            InsertaPrefijoTmp(1)
            Me.MuestraPrefijos(1, Me.Clv_Session)
            Me.MuestraPrefijos(2, Me.Clv_Session)
            Me.Refresh()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.ListBox1.Items.Count = 0 Then
            MsgBox("No hay elementos disponibles", MsgBoxStyle.Exclamation)
        Else
            InsertaPrefijoTmp(2)
            Me.MuestraPrefijos(1, Me.Clv_Session)
            Me.MuestraPrefijos(2, Me.Clv_Session)
            Me.Refresh()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.ListBox2.Items.Count = 0 Then
            MsgBox("No hay elementos disponibles", MsgBoxStyle.Exclamation)
        Else
            InsertaPrefijoTmp(3)
            Me.MuestraPrefijos(1, Me.Clv_Session)
            Me.MuestraPrefijos(2, Me.Clv_Session)
            Me.Refresh()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.ListBox2.Items.Count = 0 Then
            MsgBox("No hay elementos disponibles", MsgBoxStyle.Exclamation)
        Else
            InsertaPrefijoTmp(4)
            Me.MuestraPrefijos(1, Me.Clv_Session)
            Me.MuestraPrefijos(2, Me.Clv_Session)
            Me.Refresh()
        End If
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If Me.ListBox2.Items.Count > 0 Then
            If Me.actualiza = False Then
                If Me.Guarda(1, 0, Integer.Parse(Me.ComboBox1.SelectedValue.ToString), Me.TextBox2.Text.Trim.ToString, Me.TextBox1.Text.Trim.ToString, Me.Clv_Session) = 0 Then
                    MsgBox("Al parecer ya existe una programacion similar", MsgBoxStyle.Exclamation)
                Else
                    MsgBox("Datos Guardados", MsgBoxStyle.Information)
                    Me.Close()
                End If
            Else
                Me.Guarda(2, Me.ClvMensaje, Me.ComboBox1.SelectedValue, Me.TextBox2.Text, Me.TextBox1.Text, Me.Clv_Session)
                rSession = 0
                rMensajes = False
                Me.Close()
            End If
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Public Sub actualizar(ByVal ClvMensaje As Long)
        Me.ClvMensaje = ClvMensaje
        Me.actualiza = True
        Dim con As New SqlConnection(MiConexion)
        Dim query As String = "Select Clv_Tmensaje,Descripcion,Mensaje From MensajePersonal Where ClvMensaje=" + ClvMensaje.ToString
        Dim com As SqlCommand = New SqlCommand(query, con)
        com.CommandType = CommandType.Text
        Dim table As DataTable = New DataTable()
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        Try
            con.Open()
            da.Fill(table)
            If table.Rows.Count > 0 Then
                Me.TipoMsj = table.Rows(0)(0).ToString
                Me.Descripcion = table.Rows(0)(1).ToString
                Me.Mensaje = table.Rows(0)(2).ToString
            Else
                MsgBox("No existen datos para actualizar", MsgBoxStyle.Critical)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            con.Close()
            con.Dispose()
        End Try
    End Sub

    Private Sub btnNombre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNombre.Click
        Dim mensaje As String = Me.TextBox1.Text.ToString
        Me.TextBox1.Text = mensaje + "[NOMBRE]"
        Me.TextBox1.Select(Me.TextBox1.Text.Length - 8, 8)
        Me.Refresh()
    End Sub

    Private Sub btnPrefijo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrefijo.Click
        Dim prefijo As String = Me.TextBox1.Text.ToString
        Me.TextBox1.Text = prefijo + "[PREFIJO]"
        Me.TextBox2.Select(Me.TextBox2.Text.Length - 8, 8)
        Me.Refresh()
    End Sub
End Class