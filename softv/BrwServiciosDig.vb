Imports System.Data.SqlClient
Public Class BrwServiciosDig

    Private Sub Busca(ByVal opt As Integer)
        Dim cone As New SqlConnection(MiConexion)
        Dim par As String
        Dim par2 As String
        Try

            If Me.TextBox2.Text.Length = 0 Then
                par = ""
            End If
            If Me.MaskedTextBox1.Text.Length = 0 Then
                par2 = 0
            End If
            cone.Open()
            Me.BUSCASERVICIOS_DIGITALESTableAdapter.Connection = cone
            Me.BUSCASERVICIOS_DIGITALESTableAdapter.Fill(Me.DataSetLidia2.BUSCASERVICIOS_DIGITALES, 0, par, par2, opt)
            cone.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub BrwServiciosDig_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If bec_bnd = True Then
            bec_bnd = False
            Busca(3)
        End If
    End Sub

    Private Sub BrwServiciosDig_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Busca(3)
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(1)
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Busca(2)
    End Sub

    Private Sub MaskedTextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MaskedTextBox2.KeyPress
        If (Asc(e.KeyChar) = 13) Then
            Busca(0)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub

    Private Sub MaskedTextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MaskedTextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'If IsNumeric(Me.Clv_tipo_paquete_AdiocionalLabel1.Text) = True Then
        'gloclv_servicioDigital = Me.Clv_tipo_paquete_AdiocionalLabel1.Text
        opcion = "N"
        FrmServiciosDigitales.Show()
        'End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click

        If IsNumeric(Me.Clv_tipo_paquete_AdiocionalLabel1.Text) = True Then
            gloclv_servicioDigital = Me.Clv_tipo_paquete_AdiocionalLabel1.Text
            opcion = "C"
            FrmServiciosDigitales.Show()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If IsNumeric(Me.Clv_tipo_paquete_AdiocionalLabel1.Text) = True Then
            gloclv_servicioDigital = Me.Clv_tipo_paquete_AdiocionalLabel1.Text
            opcion = "M"
            FrmServiciosDigitales.Show()
        End If
    End Sub

End Class