Imports System.Data.SqlClient
Imports System.Data.Sql

Public Class FrmTipoMensajes

    Private agregar As Boolean = False
    Private modificar As Boolean = False

    Private Sub FrmTipoMensajes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.MuestraTipoMensajes()
    End Sub

    Private Sub MuestraTipoMensajes()
        Dim con As New SqlConnection(MiConexion)
        Dim str As String = "Exec MuestraTipoMensajes"
        Dim dataadapter As New SqlDataAdapter(str, con)
        Dim datatable As New DataTable
        Dim binding As New BindingSource
        Try
            con.Open()
            dataadapter.Fill(datatable)
            binding.DataSource = datatable
            Me.gvPrefijos.DataSource = binding
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            con.Close()
            con.Dispose()
        End Try
    End Sub

    Private Function Abc(ByVal op As Integer, ByVal clv As Integer, ByVal tipo As String) As String
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("ABCTipoMensaje", con)
        Dim mensaje As String = ""
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.Add(New SqlParameter("@Op", op))
        com.Parameters.Add(New SqlParameter("@Clv", clv))
        com.Parameters.Add(New SqlParameter("@Tipo", tipo))
        com.Parameters.Add(New SqlParameter("@Mensaje", SqlDbType.VarChar, 250))
        com.Parameters("@Mensaje").Direction = ParameterDirection.Output
        Try
            com.Connection.Open()
            com.ExecuteNonQuery()
            mensaje = com.Parameters("@Mensaje").Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            com.Connection.Close()
            com.Dispose()
        End Try
        Return mensaje
    End Function

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        Me.txtValores.Visible = True
        Me.btnAceptar.Visible = True
        Me.agregar = True
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Me.gvPrefijos.Enabled = False
        Me.txtValores.Visible = True
        Me.txtValores.Text = Me.gvPrefijos.Rows(Integer.Parse(Me.gvPrefijos.CurrentRow.Index.ToString)).Cells("Tipo_mensaje").Value.ToString
        Me.btnAceptar.Visible = True
        Me.modificar = True
    End Sub

    Private Sub btnQuitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuitar.Click
        If MsgBox("�Desea Eliminar este objeto?", MsgBoxStyle.OkCancel) = MsgBoxResult.Ok Then
            Dim clv As Integer = Integer.Parse(Me.gvPrefijos.Rows(Integer.Parse(Me.gvPrefijos.CurrentRow.Index.ToString)).Cells("Clv_Tmensaje").Value.ToString)
            MsgBox(Me.Abc(3, clv, ""), MsgBoxStyle.Information)
            Me.MuestraTipoMensajes()
        End If
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Dim clv As Integer = 0
        Dim tipo As String = ""
        If Me.agregar = True Then
            tipo = Me.txtValores.Text.ToString
            MsgBox(Me.Abc(1, clv, tipo), MsgBoxStyle.Information)
            Me.txtValores.Visible = False
            Me.btnAceptar.Visible = False
            Me.agregar = False
        End If
        If Me.modificar = True Then
            Me.gvPrefijos.Enabled = True
            clv = Integer.Parse(Me.gvPrefijos.Rows(Integer.Parse(Me.gvPrefijos.CurrentRow.Index.ToString)).Cells("Clv_Tmensaje").Value.ToString)
            tipo = Me.txtValores.Text.ToString
            MsgBox(Me.Abc(2, clv, tipo), MsgBoxStyle.Information)
            Me.txtValores.Visible = False
            Me.btnAceptar.Visible = False
            Me.modificar = False
        End If
        Me.txtValores.Text = ""
        Me.MuestraTipoMensajes()
    End Sub

    Private Sub gvPrefijos_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gvPrefijos.SelectionChanged
        
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
End Class