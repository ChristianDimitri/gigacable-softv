Imports System.Data.SqlClient
Public Class FrmTarifasEspeciales
    Dim eRes As Integer = 0
    Dim eMsg As String = ""

    Private Sub DameClasificacionLlamadas()
        Dim con As New SqlConnection(MiConexion)
        con.Open()
        Me.DameClasificacionLlamadasTableAdapter.Connection = con
        Me.DameClasificacionLlamadasTableAdapter.Fill(Me.DataSetEric2.DameClasificacionLlamadas, 0, 0)
        con.Close()
    End Sub

    Private Sub ConPaisOCodigo()
        Dim con As New SqlConnection(MiConexion)
        con.Open()
        Me.ConPaisOCodigoTableAdapter.Connection = con
        Me.ConPaisOCodigoTableAdapter.Fill(Me.DataSetEric2.ConPaisOCodigo, CInt(Me.DescripcionComboBox.SelectedValue))
        con.Close()
    End Sub

    Private Sub MuestraTarifasEspeciales()
        Dim con As New SqlConnection(MiConexion)
        con.Open()
        Me.MuestraTarifasEspecialesTableAdapter.Connection = con
        Me.MuestraTarifasEspecialesTableAdapter.Fill(Me.DataSetEric2.MuestraTarifasEspeciales)
        con.Close()
    End Sub

    Private Sub Agregar()
        If Me.NombreComboBox.SelectedValue.ToString.Length = 0 Then
            MsgBox("Selecciona una Lada.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If IsNumeric(Me.TextBoxTarifa.Text) = False Then
            MsgBox("Proporciona una Tarifa v�lida.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        eRes = 0
        Dim con As New SqlConnection(MiConexion)
        con.Open()
        Me.AgrTarifasEspecialesTableAdapter.Connection = con
        Me.AgrTarifasEspecialesTableAdapter.Fill(Me.DataSetEric2.AgrTarifasEspeciales, CInt(Me.DescripcionComboBox.SelectedValue), Me.NombreComboBox.SelectedValue, Me.TextBoxTarifa.Text, eRes, eMsg)
        con.Close()
        If eRes = 1 Then
            MsgBox(eMsg, MsgBoxStyle.Exclamation)
        Else
            MuestraTarifasEspeciales()
            Me.TextBoxTarifa.Text = ""
        End If
    End Sub

    Private Sub Eliminar()
        Dim con As New SqlConnection(MiConexion)
        con.Open()
        Me.EliTarifasEspecialesTableAdapter.Connection = con
        Me.EliTarifasEspecialesTableAdapter.Fill(Me.DataSetEric2.EliTarifasEspeciales, CInt(Me.Clasificacion_LDTextBox.Text), Me.LadaTextBox.Text)
        con.Close()
    End Sub

    Private Sub DescripcionComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DescripcionComboBox.SelectedIndexChanged
        ConPaisOCodigo()
        If CInt(Me.DescripcionComboBox.SelectedValue) = 1 Then
            Me.Label1.Text = "Nacional"
        ElseIf CInt(Me.DescripcionComboBox.SelectedValue) = 2 Then
            Me.Label1.Text = "E.U. y Canad�"
        ElseIf CInt(Me.DescripcionComboBox.SelectedValue) = 3 Then
            Me.Label1.Text = "Internacional"
        End If
    End Sub

    Private Sub ButtonAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAgregar.Click
        
        Agregar()

    End Sub

    Private Sub ButtonEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonEliminar.Click
        If Me.MuestraTarifasEspecialesDataGridView.RowCount = 0 Then
            Exit Sub
        End If
        Eliminar()
        MuestraTarifasEspeciales()
    End Sub

    Private Sub FrmTarifasEspeciales_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        DameClasificacionLlamadas()
        ConPaisOCodigo()
        MuestraTarifasEspeciales()
    End Sub

    Private Sub ButtonSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSalir.Click
        Me.Close()
    End Sub

    Private Sub TextBoxTarifa_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBoxTarifa.KeyDown
        If e.KeyData = Keys.Enter Then
            Agregar()
        End If
    End Sub

End Class