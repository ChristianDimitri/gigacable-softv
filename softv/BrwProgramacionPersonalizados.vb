Imports System.Data.SqlClient

Public Class BrwProgramacionPersonalizados


    Private Sub BrwProgramacionPersonalizados_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.DataGridView1.DataSource = Me.llenaGrid("", 0, 1)
    End Sub

    Private Sub btnBuscaNombre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscaNombre.Click
        Me.DataGridView1.DataSource = Me.llenaGrid(Me.TextBox1.Text, 0, 1)
    End Sub

    Private Sub btnFecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFecha.Click
        DateTimePicker1.Format = DateTimePickerFormat.Custom
        DateTimePicker1.CustomFormat = "yyyyMMdd"
        Me.DataGridView1.DataSource = Me.llenaGrid("", Integer.Parse(Me.DateTimePicker1.Text), 1)
        DateTimePicker1.Format = DateTimePickerFormat.Short
    End Sub

    Private Sub btnGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerar.Click
        Module1.clvSessionMensajes()
        FrmSelCiudad.Show()
    End Sub

    Private Sub btnModifica_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModifica.Click
        If Me.DataGridView1.Rows.Count > 0 Then
            Dim row As Integer = Integer.Parse(Me.DataGridView1.CurrentRow.Index.ToString)
            Dim nombre As String = Me.DataGridView1.Rows(row).Cells("NameJob").Value.ToString
            FrmProgramacionMsjPer.actualizar = True
            FrmProgramacionMsjPer.nombre = nombre
            FrmProgramacionMsjPer.Show()
        Else
            MsgBox("No existen registros", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub btnElimina_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnElimina.Click
        If Me.DataGridView1.Rows.Count > 0 Then
            If MsgBox("�Desea Eliminar esta Programacion?", MsgBoxStyle.OkCancel) = MsgBoxResult.Yes Then
                Dim row As Integer = Integer.Parse(Me.DataGridView1.CurrentRow.Index.ToString)
                Dim nombre As String = Me.DataGridView1.Rows(row).Cells("NameJob").Value.ToString
                Me.eliminaTrabajo(nombre)
            End If
        Else
            MsgBox("No existen registros", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Function llenaGrid(ByVal nombre As String, ByVal fecha As Integer, ByVal op As Integer) As BindingSource
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("BuscaProgPersonalizados", con)
        Dim tabla As DataTable = New DataTable
        Dim bs As BindingSource = New BindingSource
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.Add(New SqlParameter("@Nombre", nombre))
        com.Parameters.Add(New SqlParameter("@fecha", fecha))
        com.Parameters.Add(New SqlParameter("@op", op))
        Try
            con.Open()
            da.Fill(tabla)
            bs.DataSource = tabla
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            con.Close()
        End Try
        Return bs
    End Function

    Private Sub eliminaTrabajo(ByVal nombre As String)
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("EliminarProgramacionPersonalizada", con)
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.Add(New SqlParameter("@Name", nombre))
        Try
            con.Open()
            com.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        Dim row As Integer = Integer.Parse(Me.DataGridView1.CurrentRow.Index.ToString)
        Me.lblNombre.Text = Me.DataGridView1.Rows(row).Cells("NameJob").Value.ToString
        Me.lblFecha.Text = Me.DataGridView1.Rows(row).Cells("Date_Created").Value.ToString
        Dim habilitado As Boolean = Integer.Parse(Me.DataGridView1.Rows(row).Cells(1).Value.ToString)
        Me.CheckBox1.Checked = habilitado
    End Sub
End Class