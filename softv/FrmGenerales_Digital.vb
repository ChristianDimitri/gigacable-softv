Imports System.Data.SqlClient
Public Class FrmGenerales_Digital

    Private IpServer As String = Nothing
    Private PuertoServer As String = Nothing
    Private NombreConexion As String = Nothing
    Private NumeroDeDias As String = Nothing
    Private PaquetesAsignados As String = Nothing
    Private Sub DameDatosBitacora()
        Try
            IpServer = Me.IpServerTextBox.Text
            PuertoServer = Me.PuertoServerTextBox.Text
            NombreConexion = Me.Nombre_ConexionTextBox.Text
            NumeroDeDias = Me.Num_dia_habilitarTextBox.Text
            PaquetesAsignados = Me.Paq_defaultTextBox.Text
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub GuardaDatosBitacora(ByVal Op As Integer)
        Try
            If Op = 0 Then
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Ip Server", IpServer, Me.IpServerTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Puerto Server", PuertoServer, Me.PuertoServerTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Nombre Conexi�n", NombreConexion, Me.Nombre_ConexionTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "N�mero De D�as De Prueba", NumeroDeDias, Me.Num_dia_habilitarTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Paquete(s) Asignado(s) Por Default", PaquetesAsignados, Me.Paq_defaultTextBox.Text, LocClv_Ciudad)
            ElseIf Op = 1 Then
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Elimin� Registro", "IpServer: " + IpServer + " / PuertoServer: " + PuertoServer + " /  NombreConexi�n: " + NombreConexion + " / N�meroDeD�as: " + NumeroDeDias + " / Paquete(s)Asignado(s): " + PaquetesAsignados, "", LocClv_Ciudad)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub CONGenerales_DigitalBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONGenerales_DigitalBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONGenerales_DigitalBindingSource.EndEdit()
        Me.CONGenerales_DigitalTableAdapter.Connection = CON
        Me.CONGenerales_DigitalTableAdapter.Update(Me.DataSetEDGAR.CONGenerales_Digital)
        MsgBox(mensaje5)
        CON.Close()
        GuardaDatosBitacora(0)
        Me.Close()

    End Sub

    Private Sub Busca()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.CONGenerales_DigitalTableAdapter.Connection = CON
            Me.CONGenerales_DigitalTableAdapter.Fill(Me.DataSetEDGAR.CONGenerales_Digital, New System.Nullable(Of Integer)(CType(1, Integer)))
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
        DameDatosBitacora()
    End Sub

    Private Sub FrmGenerales_Digital_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Busca()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        GuardaDatosBitacora(1)
        CON.Open()
        Me.CONGenerales_DigitalTableAdapter.Connection = CON
        Me.CONGenerales_DigitalTableAdapter.Delete(Me.Clv_IdTextBox.Text)
        MsgBox(mensaje6)
        CON.Close()
        Me.Close()
    End Sub
End Class