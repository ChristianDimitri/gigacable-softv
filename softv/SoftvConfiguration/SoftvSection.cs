﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SoftvConfiguration
{
    public class SoftvSection : ConfigurationSection
    {
        /// <summary>
        /// Gets default connection String. If it doesn't exist then
        /// returns global connection string
        /// </summary>
        [ConfigurationProperty("DefaultConnectionString")]
        public String ConnectionString
        {
            get
            {
                string connectionString = (string)base["DefaultConnectionString"];
                connectionString = String.IsNullOrEmpty(connectionString) ?
                    Globals.DataAccess.GlobalConectionString :
                    (string)base["DefaultConnectionString"];
                return connectionString;
            }
        }

        /// <summary>
        /// Gets default assembly name for TestSimulator data clases
        /// </summary>
        [ConfigurationProperty("DefaultAssembly", DefaultValue = "SoftvSQL")]
        public String Assembly
        {
            get { return (string)base["DefaultAssembly"]; }
        }

        /// <summary>
        /// Gets ClienteApellidos configuration data
        /// </summary>
        [ConfigurationProperty("ClienteApellidos")]
        public ClienteApellidosElement ClienteApellidos
        {
            get { return (ClienteApellidosElement)base["ClienteApellidos"]; }
        }

        /// <summary>
        /// Gets ClienteBuroConfiguracion configuration data
        /// </summary>
        [ConfigurationProperty("ClienteBuroConfiguracion")]
        public ClienteBuroConfiguracionElement ClienteBuroConfiguracion
        {
            get { return (ClienteBuroConfiguracionElement)base["ClienteBuroConfiguracion"]; }
        }
        /// <summary>
        /// Gets RelMTATelefono configuration data
        /// </summary>
        [ConfigurationProperty("RelMTATelefono")]
        public RelMTATelefonoElement RelMTATelefono
        {
            get { return (RelMTATelefonoElement)base["RelMTATelefono"]; }
        }

        /// <summary>
        /// Gets RelMTATelefono configuration data
        /// </summary>
        [ConfigurationProperty("DescargaMaterial")]
        public DescargaMaterialElement DescargaMaterial
        {
            get { return (DescargaMaterialElement)base["DescargaMaterial"]; }
        }

        /// <summary>
        /// Gets RelMTATelefono configuration data
        /// </summary>
        [ConfigurationProperty("RelMaterialTrabajo")]
        public RelMaterialTrabajoElement RelMaterialTrabajo
        {
            get { return (RelMaterialTrabajoElement)base["RelMaterialTrabajo"]; }
        }


        /// <summary>
        /// Gets RelMTATelefono configuration data
        /// </summary>
        [ConfigurationProperty("Trabajo")]
        public TrabajoElement Trabajo
        {
            get { return (TrabajoElement)base["Trabajo"]; }
        }


    }
}
