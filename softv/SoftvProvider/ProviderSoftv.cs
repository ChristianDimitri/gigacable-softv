﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Softv.Providers
{
    public class ProviderSoftv
    {
        public static ClienteApellidosProvider ClienteApellidos
        {
            get { return ClienteApellidosProvider.Instance; }
        }
        public static ClienteBuroConfiguracionProvider ClienteBuroConfiguracion
        {
            get { return ClienteBuroConfiguracionProvider.Instance; }
        }
        public static RelMTATelefonoProvider RelMTATelefono
        {
            get { return RelMTATelefonoProvider.Instance; }
        }
        public static RelMaterialTrabajoProvider RelMaterialTrabajo
        {
            get { return RelMaterialTrabajoProvider.Instance; }
        }
        public static TrabajoProvider Trabajo
        {
            get { return TrabajoProvider.Instance; }
        }
        public static DescargaMaterialProvider DescargaMaterial
        {
            get { return DescargaMaterialProvider.Instance; }
        }
    }
}
